import {
	resizeModalConfig,
	resizeModal,
} from "./modals.js";
import {
	renderConfirmVersion
} from './renders/renderModals.js';
import {
	resizeWorkspace
} from "./nav.js";
import {
	loadingDefault
} from "./renders/render.js";



localStorage.setItem("bearerToken", _BEARER_TOKEN);
localStorage.setItem("clientId", _CLIENT_ID);
localStorage.setItem("clientSecret", _CLIENT_SECRET);

var xd = 0;
var porc = 0;
var listCountries = "";


document.cookie = "SameSite=Lax";

export const removeInnerForm = (vars) => {
	////console.log('removeInnerForm');
	$(".innerForm").removeClass("on");
	setTimeout(function () {
		$(".innerForm").remove();
		$(".bodyModule").removeClass("backOff");
		resizeWorkspace();
	}, 500);
}

//convert string to camelcase
export const camelize = (str) => {
	return str.replace(/\s(.)/g, function ($1) {
		return $1.toUpperCase();
	}).replace(/\s/g, '').replace(/^(.)/, function ($1) {
		return $1.toLowerCase();
	});
}

export const activateBtn = (selector = '') => {
	//console.log('activateBtn',selector);
	if (selector != '') {
		$(selector).addClass('on');
		$(selector).removeClass('error');
		$(selector).removeClass('disabled');
	}
}

export const deactivateBtn = (selector = '') => {
	//console.log('deactivateBtn',selector);
	if (selector != '') {
		$(selector).removeClass('on');
	}
}

export const disabledBtn = (selector = '') => {
	//console.log('deactivateBtn',selector);
	if (selector != '') {
		$(selector).addClass('disabled');
	}
}
export const unDisabledBtn = (selector = '') => {
	//console.log('deactivateBtn',selector);
	if (selector != '') {
		$(selector).removeClass('disabled');
	}
}

export const formatCamelCase = (text) => {
	const formatCase = (word, index) => {
		const formattedNonFirstWord = word.charAt(0).toUpperCase() + word.slice(1);
		return index === 0 ? word.toLowerCase() : formattedNonFirstWord
	};

	return text
		.replace(NON_WORD_CHARS_REGEX, ' ')
		.split(WHITE_SPACE_REGEX)
		.map((word, index) => formatCase(word, index))
		.join('')
};



export const createBtnSlit = (vars = []) => {
	let list = "";
	let btnId = [];
	let btnLabel = [];
	let btnIdLabel = [];
	let clsBtnPrimary = vars.clsBtnPrimary || "btnPrimary";
	for (let i = 0; i < vars.data.length; i++) {
		let elem = vars.data[i];
		btnId[i] = elem.id;
		btnLabel[i] = elem.name;
		btnIdLabel[i] = "btn" + capitalize(camelize(elem.name));

		if (i > 0) {
			list += `<a class="btn btnInfo btnSlitItem" id="${btnIdLabel[i]}" data-id="${btnId[i]}" >${vars.preLabel} ${btnLabel[i]}</a>`;
		}
	}
	if (vars.data == 0) {
		return `<a class="btn ${clsBtnPrimary}" id="${vars.id}0" data-id="" >${vars.preLabel}</a>`;
	} else {
		return `<div class="boxBtnSlit" id="${vars.id}">
					 
					<a class="btn btnPrimary btnSlitItem" id="${btnIdLabel[0]}" data-id="${btnId[0]}" >${vars.preLabel} ${btnLabel[0]}</a>
					<a class="btn btnSlit btnInfo" for="down${vars.id}"><i class="icon icon-chevron-down"></i></a>
					 
					<div class="slitMenu" id="down${vars.id}">
						<ul>
							${list}
						</ul>
					</div>
				</div>`;
	}
}

function checkInternetConnection() {
	let idNavigatorConnection = document.getElementById("alertNavigatorOnline");

	if (navigator.onLine) {
		//console.log('The browser is online');
		if (navigator.connection) {
			const connectionType = navigator.connection.effectiveType;
			if (connectionType === 'slow-2g' || connectionType === '2g') {
				console.log('Internet speed is slow');
				if (!idNavigatorConnection) {
					alertMessageError({
						id: 'alertNavigatorOnline',
						stop: 1,
						message: 'El internet esta muy lento para trabajar correctamente.',
						icon: 'icon icon-wifi'
					});
				}
			} else {
				//console.log('Internet speed is fast enough');
				$("#alertNavigatorOnline").remove();
			}
		} else {
			console.log('The connection API is not supported by this browser.');
			if (!idNavigatorConnection) {
				alertMessageError({
					id: 'alertNavigatorOnline',
					message: 'The connection API is not supported by this browser.',
					icon: 'icon icon-wifi'
				});
			}
		}
	} else {
		console.log('Browser is offline');
		if (!idNavigatorConnection) {
			alertMessageError({
				id: 'alertNavigatorOnline',
				stop: 1,
				message: 'El internet esta offline.',
				icon: 'icon icon-wifi'
			});
		}
	}
}

const intervalCheckInternet = 5000;
if (_STATE_CONNECT != 0) {
	console.log("STATE", _STATE_CONNECT);
	setInterval(checkInternetConnection, intervalCheckInternet);
}

document.addEventListener("DOMContentLoaded", function () {

	$("body").on("click", ".btnSlit", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let id = $(this).attr("for");
		//console.log('btnSlit', );
		$("#" + id).addClass("on");
		$(this).addClass("on");
		//position of bottom
		let slitMenu = $(this);
		let slitMenuP = slitMenu.offset();
		let slitMenuH = slitMenu.outerHeight();
		let slitMenuW = slitMenu.outerWidth();
		let slitMenuT = slitMenuP.top;
		let slitMenuL = slitMenuP.left;

		console.log("btnSlit:", slitMenuH, slitMenuT, slitMenuL);
		$("#" + id).css("width", slitMenuW);
		$("#" + id).css("top", slitMenuT + slitMenuH);
		$("#" + id).css("left", slitMenuL);
		
	})

	$("body").on("mouseleave", ".boxBtnSlit", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		$(".slitMenu").removeClass("on");
		$(".btnSlit").removeClass("on");
	})

	$("body").on("click", ".btnStep", function (e) {
		e.preventDefault();
		e.stopPropagation();
		var module = $(this).parent().attr("module");
		var step = $(this).attr("step");
		//console.log(module + ":" + step);
		$(".steps[module='" + module + "'] .btnStep").removeClass("active");
		$(
			".steps[module='" + module + "'] .btnStep[step='" + step + "']"
		).addClass("active");
		$(".contentSteps[module='" + module + "'] .content").removeClass(
			"active"
		);
		$(
			".contentSteps[module='" +
			module +
			"'] .content[for='" +
			step +
			"']"
		).addClass("active");
	});



	$("body").on("click", ".btnNextStep", function (e) {
		e.preventDefault();
		e.stopPropagation();
		var step = $(this).attr("step");
		var module = $(this).attr("module");
		//console.log(module + ":" + step);
		$(".steps[module='" + module + "'] .btnStep").removeClass("active");
		$(
			".steps[module='" + module + "'] .btnStep[step='" + step + "']"
		).addClass("active");
		$(".contentSteps[module='" + module + "'] .content").removeClass(
			"active"
		);
		$(
			".contentSteps[module='" +
			module +
			"'] .content[for='" +
			step +
			"']"
		).addClass("active");
	});

	$("body").on("click", ".btnPreviusStep", function (e) {
		e.preventDefault();
		e.stopPropagation();
		var step = $(this).attr("step");
		var module = $(this).attr("module");
		//console.log(module + ":" + step);
		$(".steps[module='" + module + "'] .btnStep").removeClass("active");
		$(
			".steps[module='" + module + "'] .btnStep[step='" + step + "']"
		).addClass("active");
		$(".contentSteps[module='" + module + "'] .content").removeClass(
			"active"
		);
		$(
			".contentSteps[module='" +
			module +
			"'] .content[for='" +
			step +
			"']"
		).addClass("active");
	});

	//bts country
	$("body").on("keyup", ".inputBuscarCountry", function (e) {
		e.preventDefault();
		e.stopPropagation();
		let id = $(this).attr("for");
		let q = $(this).val();
		////console.log(box);
		var rex = new RegExp($(this).val(), "i");
		$(".boxCountries .btnCodeCountry").hide();
		$(".boxCountries .btnCodeCountry")
			.filter(function () {
				return rex.test($(this).text());
			})
			.show();
	});

	$("body").on("click", ".btnSelectCountry", function (e) {
		e.preventDefault();
		e.stopPropagation();
		let id = $(this).attr("for");
		let forms = $(this).attr("form");
		loadCountryBox({
			id: id,
			form: forms,
			mode: "country"
		});
	});

	$("body").on("mouseleave", ".boxCountries", function (e) {
		e.preventDefault();
		e.stopPropagation();
		let id = $(this).attr("for");
		$(".boxCountries").removeClass("on");
		$(".boxCountries").attr("state", 0);
	});

	$("body").on("click", ".btnCodeCountry", function (e) {
		e.preventDefault();
		e.stopPropagation();
		let ac = $(this);
		let id = $(this).parent().attr("for");
		let mode = $(this).parent().attr("mode");
		//console.log(id);
		//console.log(mode);
		let item = $(this).attr("item");
		let dialcode = $(this).attr("dialcode");
		let name = $(".btnCodeCountry[item='" + item + "'] .name").html();
		/* $(".btnCodeCountry").removeClass("active")
			$(this).addClass("active") */

		item = item.toLowerCase();

		if (mode == "") {
			$(".formSelectCountry[for='" + id + "'] .country .cp").html(
				`<img src="${_PATH_WEB_NUCLEO}vendor/svg/flags/${item}.svg"><span>${name}</span>`
			);
			$("#" + id).val(item);
		}

		if (mode == "dialcelular") {
			$(".btnDialCelular[for='" + id + "']").html(
				`<img src="${_PATH_WEB_NUCLEO}vendor/svg/flags/${item}.svg"><span>${dialcode}</span><i class="icon-chevron-down"></i>`
			);
			$("#" + id + "Dial").val(dialcode);
		}

		$(".boxCountries").removeClass("on");
		$(".boxCountries").attr("state", 0);
		////console.log(id)
	});

	//btn dialCountry
	$("body").on("click", ".btnDialCelular", function (event) {
		event.preventDefault();
		event.stopPropagation();
		//console.log("btnDialCelular");
		let id = $(this).attr("for");
		let forms = $(this).attr("form");
		loadCountryBox({
			id: id,
			form: forms,
			mode: "dialCelular"
		});
	});

	//tables



	$("body").on("click", ".btnBlockPw", function (e) {
		var state = $(this).attr("state");
		var id = $(this).attr("for");

		////console.log("btnBlockPw")

		$("i", this).attr("class", "");

		if (state == 0) {
			$("i", this).addClass("icon icon-eye-open");
			$(this).attr("state", "1");
			$("#" + id).attr("type", "text");
		} else {
			$("i", this).addClass("icon icon-eye-close");
			$(this).attr("state", "0");
			$("#" + id).attr("type", "password");
		}
	});

	$("body").on("click", ".btnSearchClear", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let id = $(this).attr("for");
		let restoreId = $(this).attr("restore");
		$("#" + id).val("");
		$("#" + restoreId + " div").show();
		$("#" + restoreId + " li").show();
	});


	$("body").on("click", `.btnQuantityPlus`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let id = $(this).attr("for");
		let value = parseInt($("#" + id).val());
		//console.log(``,data);
		$("#" + id).val(value + 1);
	});

	$("body").on("click", `.btnQuantityMinus`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let id = $(this).attr("for");
		let value = parseInt($("#" + id).val());
		//console.log(``,data);
		if (value > 1) {
			$("#" + id).val(value - 1);
		}
	});

	$("body").on("click", `.btnDropdown`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let id = $(this).attr("for");

		$(`.btnDropdown`).removeClass("on");
		$(`.dropdown`).removeClass("on");
		$(`.dropdown[for="${id}"]`).addClass("on");
		$(this).addClass("on");
	});

	$("body").on("mouseleave", `.dropdown`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();

		$(`.dropdown`).removeClass("on");
		$(`.btnDropdown`).removeClass("on");
	});

	$("body").on("click", "#btnUpdateVersionSystem", function (e) {
		e.preventDefault();
		//console.log('btnUpdateVersionSystem');
		//console.log('eventoTeclado', eventoTeclado);
		location.reload(true);
	});


});


export const validateEmail = (valor) => {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(valor)) {
		//alert("La dirección de email " + valor + " es correcta.");
		return 1;
	} else {
		//alert("La dirección de email es incorrecta.");
		return 0;
	}
};

export function getOffset(el) {
	var _x = 0;
	var _y = 0;
	while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
		_x += el.offsetLeft - el.scrollLeft;
		_y += el.offsetTop - el.scrollTop;
		el = el.offsetParent;
	}
	return {
		top: _y,
		left: _x
	};
}

export function listJsonToString(json, row) {
	let str = "";
	for (let i = 0; i < json.length; i++) {
		const obj = json[i];
		let j = 0;
		let aux = [];

		for (const key in obj) {
			////console.log(key + ":" + obj[key])
			if (row != undefined) {
				if (key == row) {
					j++;
					aux[j] = obj[key];
				}
			} else {
				j++;
				aux[j] = obj[key];
			}
		}

		str += aux.join();
	}
	str = str.substring(1);
	str = str.replace(",", ", ");

	return str;
}

export const dataModule = (module, str) => {
	////console.log('colorModule',vars);
	return $('.btnModule[module = "' + module + '"]').attr(str);
}


export const loadingBarId = (id) => {
	////console.log('loadingDivId',vars);
	$("#" + id).html(`<div class="loadingPageModule"><div class="loadingBar"></div></div></div>`);
	loadingBar();
}

export const loadingBarIdReturn = (id) => {
	////console.log('loadingDivId',vars);
	loadingBar();
	return `<div class="loadingPageModule"><div class="loadingBar"></div></div></div>`;

}

export const empty = (str) => {
	if (str != "" && str != null && str != undefined) {
		return 1;
	} else {
		return 0;
	}
}

export function loadingBtnIcon(elm, str = "") {
	$(elm).addClass("btnIconLeft disabled");
	$(elm + " i").removeClass();
	if (str != "") {
		$(elm + " span").html(str);
	}
	$(elm + " i").addClass("icon icon-refresh icon-loading");
}

export function removeLoadingBtnIcon(elm, str) {
	$(elm).removeClass("btnIconLeft disabled");
	$(elm + " i").removeClass();
	if (str != "") {
		$(elm + " span").html(str);
	}
}

export const removeModalPage = (type) => {
	if (type == undefined) {
		$(".modalPage").remove();
	}

	if (type == "animated") {
		$(".modalPage").addClass("animated fadeOut");
		setInterval(() => {
			$(".modalPage").remove();
		}, 100);
	}
};

export const loadingSelector = (vars = []) => {
	//console.log('loadingSelector', vars);
	let selector = vars.selector ? vars.selector : "";

	$(selector).html(`<div class="loading"></div>`);

}

export function btnLoading(id, str = "") {
	$("#" + id).addClass("btnIconLeft disabled");
	$("#" + id + " i").removeClass();
	$("#" + id + " i").addClass("icon icon-loading icon-refresh");
	if (str != "") {
		$("#" + id + " span").html(str);
	}
}

export function btnLoadingRemove(id, str = "", icon = "") {
	$("#" + id + " i").removeClass();
	$("#" + id).removeClass("btnIconLeft");
	$("#" + id).removeClass("disabled");
	if (str != "") {
		$("#" + id + " span").html(str);
	}
	if (icon != "") {
		$("#" + id + " i").addClass(icon);
	}
}

export function changeBtn(vars = []) {
	console.log("changeBtn", vars);
	let type = vars.type;
	let id = vars.id;
	let text = vars.text ? vars.text : "";
	let icon = vars.icon ? vars.icon : "";
	let btnCls = "";
	let action = vars.action ? vars.action : "";
	let selector = vars.selector ? vars.selector : "";
	let resultadoAction = vars.resultadoAction ? vars.resultadoAction : -1;

	if (id != undefined && id != "") {
		$("#" + id).removeClass("btnIconLeft disabled btnIconRight btnPrimary btnSuccess btnWarning btnDanger btnInfo btnDefault");
		$("#" + id + " i").removeClass();
	}

	if (selector != undefined && selector != "") {
		$(selector).removeClass("btnIconLeft disabled btnIconRight btnPrimary btnSuccess btnWarning btnDanger btnInfo btnDefault");
		$(selector + " i").removeClass();
	}

	if (type == "success") {
		text = vars.text ? vars.text : "Guardado";
		icon = "icon icon-circle-checkmark";
		action = vars.action ? vars.action : "btnSuccess";
	}

	if (type == "primary") {
		text = vars.text ? vars.text : "Guardar";
		icon = vars.icon ? vars.icon : "";
		action = vars.action ? vars.action : "btnPrimary";
	}

	if (type == "loading") {
		text = vars.text ? vars.text : "Actualizando...";
		icon = vars.icon ? vars.icon : "icon icon-loading icon-refresh";
		action = " disabled btnPrimary " + action;
	}

	if (type == "save") {
		btnCls = "btnIconLeft " + action;
		text = vars.text ? vars.text : "Guardando...";
		icon = vars.icon ? vars.icon : "icon icon-loading icon-refresh";
		action = " btnPrimary disabled  " + action;
	}

	if (type == "danger") {
		text = vars.text ? vars.text : "Error. por favor contactarse con soporte.";
		icon = "icon icon-alert-warning";
		action = "btnDanger";
	}


	// buscar dentro de action si existe iconRight

	if (icon != "" && resultadoAction != -1) {
		btnCls = "btnIconLeft " + action;
	} else {
		btnCls = "" + action;
	}

	if (type == "custom") {
		text = vars.text ? vars.text : "Sin texto";
		icon = vars.icon ? vars.icon : "";
		action = vars.action ? vars.action : "";
		btnCls = vars.btnCls ? vars.btnCls : "";
	}

	////console.log("btnCls", btnCls , "action", action, "icon", icon, "text", text);

	if (id != undefined && id != "") {
		$("#" + id).addClass(btnCls);
		$("#" + id + " i").addClass(icon);
		$("#" + id + " span").html(text);
	}

	if (selector != undefined && selector != "") {
		$(selector).addClass(btnCls);
		$(selector + " i").addClass(icon);
		$(selector + " div").html(text);
		$(selector + " span").html(text);

	}

}

export function convertBtn({
	text,
	icon,
	selector,
	cls
}) {
	if (selector != undefined && selector != "") {
		$(selector).attr('class', cls);
		$(selector + " i").attr('class', icon);
		$(selector + " div").html(text);
		$(selector + " span").html(text);
	}
}

export function emptyReturn(str, put) {
	if (str == "" || str == undefined) {
		return put;
	} else {
		return str;
	}
}

export function getScript(path, tag) {
	var newNode = document.createElement("script"); // create a script tag
	let referenceNode = document.querySelector(tag);
	newNode.src = path; // set the source of the script to your script
	newNode.setAttribute("type", "module");
	referenceNode.append(newNode); // append the script to the DOM

	////console.log('scritpCargado:' + path)
}

export function isJson(str) {
	//GET
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}

export function replacePath(str) {
	////console.log("replacePath " + str)
	str = str.replace(/{{_PATH_WEB}}/g, _PATH_WEB);
	str = str.replace(/{{_PATH_WEB_NUCLEO}}/g, _PATH_WEB_NUCLEO);
	str = str.replace(/{{_PATH_FILES}}/g, _PATH_FILES);
	str = str.replace(/{{_PATH_DASHBOARD}}/g, _PATH_DASHBOARD);

	return str;
}

export function replaceEssentials(obj) {
	////console.log("replacePath " , obj)
	let str = emptyReturn(obj.str, "");
	str = str.replace(/{{_MODULE}}/g, obj.module);
	str = str.replace(/{{_SYSTEM}}/g, obj.system);
	str = str.replace(/{{_PATHURL}}/g, obj.pathurl);
	str = str.replace(/{{_COLOR}}/g, obj.color);
	str = str.replace(/{{_FN}}/g, obj.fn);
	str = str.replace(/{{_TITLE}}/g, obj.title);
	str = str.replace(/{{_ICON}}/g, obj.icon);
	str = str.replace(/{{_NAME}}/g, obj.name);
	str = str.replace(/{{_BODY}}/g, obj.body);
	str = str.replace(/{{_CONTENT}}/g, obj.content);
	str = str.replace(/{{_ITEM}}/g, obj.item);
	str = str.replace(/{{_FORM}}/g, obj.form);
	str = str.replace(/{{_FORM_ID}}/g, obj.formId);
	str = str.replace(/{{_BTN_ACTION}}/g, obj.btnAction);
	str = str.replace(/{{_TASK}}/g, obj.task);
	str = str.replace(/{{_CLS}}/g, obj.cls);
	str = str.replace(/{{_CATEGORYS}}/g, obj.categorys);
	str = str.replace(/{{_BTN_TASK}}/g, obj.btnTask);
	str = str.replace(/{{_BTN_ICON}}/g, obj.btnIcon);
	str = str.replace(/{{_BTN_ID}}/g, obj.btnId);
	str = str.replace(/{{_BTN_NAME}}/g, obj.btnName);
	str = str.replace(/{{_BTN_ATTR}}/g, obj.btnAttr);
	str = str.replace(/{{_BTN_NAME_ACTION}}/g, obj.btnNameAction);
	str = str.replace(/{{_BTN_LABEL_ACTION}}/g, obj.btnLabelAction);
	str = str.replace(/[*]/g, '<span class="required">*</span>');
	str = replacePath(str);
	return str;
}

export function returnAccessToken(access_token) {
	let dataToken = atob(access_token);
	let varsData = dataToken.split(".");
	return `{userId : ${varsData[1]} , rolId: ${varsData[2]} }`;
}

export function loadingBar() {
	displayLoadingBar = setInterval(loadBar, 200);
	////console.log(displayLoadingBar)
}

export function loadBtnLoading(e) {
	////console.log(e)
	let id = e.currentTarget.id;
	let dat = e.currentTarget.innerHTML;
	$("#" + id).addClass("btnIconLeft disabled");
	e.currentTarget.innerHTML =
		'<i class="icon icon-refresh icon-loading"></i>' + dat;
}

export function removeBtnLoading(e) {
	//console.log(e);
	let id = e.currentTarget.id;
	let dat = e.currentTarget.innerHTML;
	$("#" + id).removeClass("btnIconLeft");
	let html = dat.replace(
		'<i class="icon icon-refresh icon-loading"></i>',
		""
	);
	e.currentTarget.innerHTML = html;
}

export function unDisableId(id) {
	$("#" + id).removeClass("disabled");
}

export function disableId(id) {
	$("#" + id).addClass("disabled");
	$("#" + id).blur();
}

export function unDisableElem(elem) {
	$(elem).removeClass("disabled");
}

export function disableElem(elem) {
	$(elem).addClass("disabled");
}

export function loadBar() {
	porc = porc + 1;
	////console.log(porc)
	$(".loadingBar").width(porc + "%");
	if (porc > 99) {
		porc = 0;
	}
}

export function stopLoadingBar(tion) {
	////console.log("stopLoadingBar")
	clearInterval(displayLoadingBar);
	//handle = 0;
}

export async function selectOptionsCountries(vars = []) {
	let codeSelect = vars.selected;
	let url = _PATH_WEB_NUCLEO + "vendor/json/countries.json";
	let html = "";
	let req = new Request(url, {
		async: true,
		method: "GET",
		mode: "cors",
	});


	try {
		let contentAwait = await fetch(req);
		let data = await contentAwait.json();
		for (let i = 0; i < data.length; i++) {
			let selected = "";
			if (data[i].code == codeSelect) {
				selected = "selected";
			}
			html += `<option value="${data[i].code}" ${selected}>${data[i].es}</option>`;
		}
		$("#" + vars.id).html(html);
		if (codeSelect == "") {
			$("#" + vars.id).val("BO");
		}

		//console.log("html", html, data);

	} catch (err) {
		console.log("loadAdvised Error:" + err);
	}
}

async function selectListCountries(id, codeSelect, idParent) {
	let url = _PATH_WEB_NUCLEO + "vendor/json/countries.json";
	let req = new Request(url, {
		async: true,
		method: "GET",
		mode: "cors",
	});

	try {
		let contentAwait = await fetch(req);
		let data = await contentAwait.json();
		let list = "";
		let cls = "";
		for (let i = 0; i < data.length; i++) {
			////console.log(data[i].code + ":" + data.length)
			let cod = data[i].code;
			let name = data[i].es;
			let dialCode = data[i].dialCode;
			//let img = `<img src="https://www.countryflags.io/${cod}/flat/24.png">`;
			let img = data[i].flag;
			if (cod == codeSelect) {
				cls = "active";
				/* $(".formSelectCountry[for='" + id + "'] .country").html(`<div class="cp">${img}<span>${name}</span></div>`)
					$("#" + id).val(cod) */
			} else {
				cls = "";
			}
			list += `<a class="btnCodeCountry ${cls}" item="${cod}" dialcode="${dialCode}" >${img}<span class="dialCode">${dialCode}</span><span class="name">${name}</span></a>`;
		}
		return list;
	} catch (err) {
		//console.log("loadAdvised Error:" + err);
	}

}

export function loadCountryBox(vars) {
	let id = vars.id;
	let forms = vars.form;
	let mode = vars.mode;
	let boxCountries = $(".boxCountries");
	let state = $(".boxCountries").attr("state");
	let iForm = $(".formInput[for='" + id + "']");

	//console.log(".btnSelectCountry:" + id);
	////console.log(iForm)

	//boxCountries.offset({ left: coordLeft }     ////console.log(listCountries[id])

	if (mode == "dialCelular") {
		$(".boxCountries .listCountries").addClass("on");
		$(".boxCountries .listCountries").attr("mode", "dialcelular");
	} else {
		$(".boxCountries .listCountries").removeClass("on");
		$(".boxCountries .listCountries").attr("mode", "");
	}

	let wForm = iForm.width();
	let hForm = iForm.outerHeight();
	let coordForm = iForm.offset();
	let coordFormx = $("#" + forms).offset();
	// let coordTop = coordForm.top - coordFormx.top + hForm
	let coordTop = coordForm.top - coordFormx.top + hForm;
	let coordLeft = coordForm.left - coordFormx.left;

	//console.log(coordForm);

	boxCountries.width(wForm);
	$(".boxCountries .listCountries").attr("for", id);

	if (listCountries == "" || listCountries == undefined) {
		boxCountries.attr("state", 1);
		boxCountries.css("top", coordTop);
		boxCountries.css("left", coordLeft);
		$(".boxCountries .listCountries").html(loadingDefault());
		boxCountries.addClass("on");

		selectListCountries(id, "BO").then((list) => {
			listCountries = list;
			$(".boxCountries .listCountries").html(list);
		});
	} else {
		boxCountries.css("top", coordTop);
		boxCountries.css("left", coordLeft);
		if (state == 1) {
			boxCountries.removeClass("on");
			boxCountries.attr("state", 0);
		} else {
			boxCountries.addClass("on");
			boxCountries.attr("state", 1);
		}
	}
}

/* export const addHtml = (vars) => {
	
} */


export const addHtml = async (vars = []) => {
	//console.log("addHtml", vars);
	try {
		let selector = vars.elem ? vars.elem : vars.selector;
		let content = vars.content ? vars.content : vars.html;
		if (vars.type == "append") {
			$(selector).append(content);
		} else if (vars.type == "prepend") {
			$(selector).prepend(content);
		} else if (vars.type == "insert") {
			$(selector).html(content);
		}

		resizeModal()

	} catch (error) {
		console.log("Error: addHtml")
		console.log(error)
	}
}

export function removeHtml(vars = []) {
	let timer = vars.timer ? vars.timer : "500";
	let animation = vars.animation ? vars.animation : "animation bounceOut";
	let selector = vars.selector;
	$(selector).removeClass("bounceIn fadeIn zoomIn");

	if (vars.animation != "") {
		$(selector).addClass(animation);
		setTimeout(function () {
			$(selector).remove();
		}, timer);
	} else {
		$(selector).remove();
	}


}

export function timer(time, resolve, reject) {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			resolve();
		}, time);
	});
}

export const loadView = async (url) => {
	let req = new Request(url, {
		async: true,
		method: "POST",
		mode: "cors",
	});

	try {
		let contentAwait = await fetch(req);
		let str = await contentAwait.text();
		return str;
	} catch (err) {
		console.log("loadView Error:", err);
	}
};

export async function loadViewJson(url) {
	let req = new Request(url, {
		method: "POST",
		mode: "cors",
		async: true,
	});

	try {
		let contentAwait = await fetch(req);
		let str = await contentAwait.json();
		return str;
	} catch (err) {
		//console.log("loadViewJson Error:" + err);
	}
}

export function timeLeft(param) {
	var endDate = param.endDate;
	var startDate = param.startDate;
	var mode = emptyReturn(param.mode, "");
	var dat = "";

	var url = _PATH_WEB_NUCLEO + "controllers/apis/modules/modules.php";
	let access_token = localStorage.getItem("access_token");
	let refresh_token = localStorage.getItem("refresh_token");

	////console.log(endDate + ":" + startDate)

	const dataForm = new FormData();
	dataForm.append("access_token", access_token);
	dataForm.append("refresh_token", refresh_token);
	dataForm.append("action", "timeLeft");
	dataForm.append("endDate", endDate);
	dataForm.append("startDate", startDate);
	dataForm.append("mode ", mode);

	let req = new Request(url, {
		method: "POST",
		mode: "cors",
		body: dataForm,
	});

	fetch(req).then(function (response) {
		response.text().then(function (text) {
			////console.log(text)
			return text;
		});
	});
}

export function todaysDate(param = "", locale = "es-Es") {
	////console.log(Date().split(' '))
	let f = new Date();
	let h = (f.getHours() < 10 ? "0" : "") + f.getHours();
	let min = (f.getMinutes() < 10 ? "0" : "") + f.getMinutes();
	let day = (f.getDate() < 10 ? "0" : "") + f.getDate();
	let mes = f.getMonth() + 1;
	let mt = (mes < 10 ? "0" : "") + mes;
	let y = f.getFullYear();
	////console.log(f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear() + " " + f.getHours() + ":" + f.getMinutes())

	// let Array = Date()
	if (param == "") {
		return day + "-" + mt + "-" + y + " " + h + ":" + min;
	}

	if (param == "dbDateTime") {
		return y + "-" + mt + "-" + day + " " + h + ":" + min;
	}

	if (param == "dbDate") {
		return y + "-" + mt + "-" + day;
	}

	if (param == "full") {
		var options = {
			weekday: "long",
			year: "numeric",
			month: "long",
			day: "numeric",
		};
		let dates = f.toLocaleDateString(locale, options);
		return capitalize(dates) + " " + h + ":" + min;
	}
}

export function btnFn(vars) {
	////console.log(vars)
	let id = emptyReturn(vars.id, "");
	let btnType = emptyReturn(vars.btnType, "");
	let clss = emptyReturn(vars.clss, "");
	let cls = emptyReturn(vars.cls, "");
	let item = emptyReturn(vars.item, "");
	let module = emptyReturn(vars.module, "");
	let system = emptyReturn(vars.system, "");
	let fn = emptyReturn(vars.fn, "");
	let vari = emptyReturn(vars.vars, "");
	let attr = emptyReturn(vars.attr, "");
	let icon = emptyReturn(vars.icon, "");

	if (btnType == "btnEdit") {
		return (
			'<a class="btnEdit ' +
			clss +
			'" id="' +
			id +
			'" item="' +
			item +
			'" fn="' +
			fn +
			'"  vars="' +
			vari +
			'" ' +
			attr +
			'" module="' +
			module +
			'" system="' +
			system +
			'" ' + attr + ' ><i class="icon icon-pencil"></i></a>'
		);
	}

	if (btnType == "btnHistory") {
		return (
			'<a class="btnHistory ' +
			clss +
			'" id="' +
			id +
			'" item="' +
			item +
			'" fn="' +
			fn +
			'"  vars="' +
			vari +
			'"  module="' +
			module +
			'" system="' +
			system +
			'" ' + attr + ' ><i class="icon icon-list"></i></a>'
		);
	}
	if (btnType == "btnDelete") {
		return (
			'<a class="btnDelete ' +
			clss + ' ' + cls +
			'" id="' +
			id +
			'" item="' +
			item +
			'" fn="' +
			fn +
			'"  vars="' +
			vari +
			'"  module="' +
			module +
			'" system="' +
			system +
			'" ' + attr + ' ><i class="icon icon-trash"></i></a>'
		);
	}

	if (btnType == "btnDeleteFn") {
		return (
			`<a class="btnDeleteFn ${cls}" ${attr} ><i class="icon icon-trash"></i></a>`
		);
	}

	if (btnType == "btnViewFn") {
		return (
			`<a class="btnViewFn ${cls}" ${attr} ><i class="icon icon-search"></i></a>`
		);
	}

	if (btnType == "") {
		return (
			`<a class="${cls}" ${attr} data-id="${id}" ><i class="${icon}"></i></a>`
		);
	}
}

export function capitalize(s) {
	if (typeof s !== "string") return "";
	return s.charAt(0).toUpperCase() + s.slice(1);
}

export function resizeSteps() {
	let h = $(window).outerHeight();
	let hContent = h - 220;
	$(".contentSteps").outerHeight(hContent);
}

export function replaceAll(text, search, replace) {
	while (text.toString().indexOf(search) != -1)
		text = text.toString().replace(search, replace);
	return text;
}

export function dateFormat(vars, locale = "es-Es") {
	//console.log("vars", vars);
	let date = emptyReturn(vars.date, "");
	let modal = emptyReturn(vars.modal, "full");
	let f = new Date(date);
	let h = (f.getHours() < 10 ? "0" : "") + f.getHours();
	let min = (f.getMinutes() < 10 ? "0" : "") + f.getMinutes();
	let day = (f.getDate() < 10 ? "0" : "") + f.getDate();
	let mes = f.getMonth() + 1;
	let mt = (mes < 10 ? "0" : "") + mes;
	let y = f.getFullYear();
	////console.log(f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear() + " " + f.getHours() + ":" + f.getMinutes())

	if (modal == "") {
		return day + "-" + mt + "-" + y + " " + h + ":" + min;
	}

	if (modal == "dbDateTime") {
		return y + "-" + mt + "-" + day + " " + h + ":" + min;
	}

	if (modal == "dbDate") {
		return y + "-" + mt + "-" + day;
	}

	if (modal == "full") {
		var options = {
			weekday: "long",
			year: "numeric",
			month: "long",
			day: "numeric",
		};
		let dates = f.toLocaleDateString("es-ES", options);

		return capitalize(dates) + " " + f.getHours() + ":" + min;
	}
}

export function formatDate(vars) {
	let date = emptyReturn(vars.date);
	let modal = emptyReturn(vars.modal, "db");
	let str = "";
	let datArray = date.split(" ");

	let datLine = datArray[0];
	let datLineArray = datLine.split("-");
	let datTime = datArray[1];

	if (modal == "db") {
		str =
			datLineArray[2] +
			"-" +
			datLineArray[1] +
			"-" +
			datLineArray[0] +
			" " +
			datTime;
	}
	return str;
}

export function alertPage(array) {
	//console.info(array);
	var type = array.type;
	var id = array.id;
	var text = array.text;
	var icon = array.icon;
	var num = empty(array.num, 1);
	var stop = array.stop;
	var animation_in = array.animation_in;
	var animation_out = array.animation_out;
	var time = array.time;
	var position = array.position;
	var contAlert = 0;
	$(".boxAlert").each(function () {
		contAlert++;
	});
	if (contAlert == 0) {
		$("body").prepend('<div class="boxAlert ' + position + '"></div>');
	}
	$("body .boxAlert").append(
		"<div num='" + num + "' class='alert alertNum" + num + " " + type + "  animated " + animation_in + "  fast' id='" + id + "'><i class='icon " + icon + "'></i><span class='text'>" + text + "</span></div>"
	);


	if (stop != 1) {
		setTimeout(function () {
			////console.log(time);
			$(".alert").removeClass(animation_in);
			$(".alert").addClass(animation_out);
			setTimeout(function () {
				$(".boxAlert").html("");
				$(".boxAlert").remove();
			}, 450);
		}, time);
	}
}

export const alertResponseMessageError = (vars) => {
	let message = vars.message ? vars.message : "Error. por favor contactarse con soporte.";
	let text = vars.text != "" ? vars.text : message;
	let time = vars.time ? vars.time : "3500";


	alertPage({
		text: message,
		icon: "icn icon-alert-warning",
		animation_in: "bounceInRight",
		animation_out: "bounceOutRight",
		type: "alertDanger",
		time,
		position: "top-left",
	});
};


export const alertMessageError = (vars) => {
	let message = vars.message ? vars.message : "Error. por favor contactarse con soporte.";
	let icon = vars.icon ? vars.icon : "icn icon-alert-warning";
	let stop = vars.stop ? vars.stop : 0;
	alertPage({
		text: message,
		icon,
		id: vars.id,
		stop,
		animation_in: "fadeInRight",
		animation_out: "fadeOutRight",
		type: "alertDanger",
		time: "3500",
		position: "top-left",
	});
};

export const alertMessageSuccess = (vars) => {
	let message = vars.message ? vars.message : "Acción exitosa!.";
	alertPage({
		text: message,
		icon: "icn icon-alert-warning",
		animation_in: "fadeInRight",
		animation_out: "fadeOutRight",
		type: "alertSuccess",
		time: "3500",
		position: "top-left",
	});
};


export function loadingBtnIco(elm) {
	$(elm).addClass("btnIconLeft disabled");
	$(elm + " i").removeClass();
	$(elm + " i").addClass("icon icon-refresh icon-loading");
}

export function removeLoadingIcon(elm) {
	$(elm).removeClass("btnIconLeft");
	$(elm).removeClass("disabled");
	$(elm + " i").removeClass();
}

export const removeSelector = (selector = null) => {
	//console.log('removeSelector', selector);
	$(selector).remove();
}

export const activeSelector = (selector = null) => {
	//console.log('activeSelector',selector);
	$(selector).addClass("active");
}

export const unactiveSelector = (selector = null) => {
	//console.log('unactiveSelector ',selector);
	$(selector).removeClass("active");
}

export function listCategorys(vars) {
	////console.log(vars)
	let str = "";
	let lastItem = "";
	for (let i = 0; i < vars.length; i++) {
		const id = vars[i].cat_id;
		const name = vars[i].cat_name;
		const level = vars[i].level;
		const children = vars[i].children;
		let actChild = "";

		var numLast = vars.length - 1;
		if (i == numLast) {
			lastItem = "level-last";
		} else {
			lastItem = "";
		}

		if (children == 0) {
			actChild = "";
		} else {
			actChild = "haveChild";
		}

		str += `<div class="checkbox level level${level} ${lastItem}" child="0" parent="0" level="0" item="${id}" >
                    <span class="box ${actChild}"><input name="inputCategorys[]" id="inputCat${id}" type="checkbox" value="${id}"></span>
					<span class="name">${name}</span>
    	        </div>`;
		////console.log("children : " + children)
		////console.log("children.length: " + children.length)
		if (children != 0) {
			if (children.length != 0) {
				////console.log("tiene hijos" + children)
				children.parent = id;
				str += listCategorysChildren(children);
			}
			////console.log(str)
		}
	}
	return str;
}

export function listCategorysChildren(vars) {
	//console.log(vars)
	////console.log(vars.length)
	var str = "";
	let lastItem = "";
	for (let i = 0; i < vars.length; i++) {
		var levelMargin = "";
		const typeLine = "";
		const id = vars[i].cat_id;
		const name = vars[i].cat_name;
		const level = vars[i].level;
		const children = vars[i].children;
		const parent = vars.parent;

		var numLast = vars.length - 1;

		if (i == numLast) {
			lastItem = "level-last";
		} else {
			lastItem = "";
		}
		////console.log("listCategorysChildren children.length: " + children.length)

		var levelStatus = "";
		let mleft = 24 * level;
		if (children.length >= 1) {
			levelStatus = "childrenActive";
		} else {
			levelStatus = "";
		}
		if (children.length == 1) {
			levelStatus = "childActive";
		}

		str += `<div class="checkbox level level${level} ${lastItem}" level="${level}" child="${levelStatus}" id="item${id}" item="${id}" parent="${parent}" style ="margin-left:${mleft}px" >
                    <span class="box ${levelStatus}"><input name="inputCategorys[]" id="inputCatTree${id}" type="checkbox" value="${id}"></span>
                    <span class="name">${name}</span>`;
		str += `</div>`;

		if (children != 0) {
			if (children.length != 0) {
				str += `<div class="children  ${levelStatus}" for="${id}" >`;
				children.parent = id;
				str += listCategorysChildren(children);
				str += "</div>";
			}
		}
	}
	return str;
}

export function checkCategorys(categorys, name = "inputCategorys[]") {
	//console.log(categorys, name);
	if (categorys != 0) {
		for (let j = 0; j < categorys.length; j++) {
			const id = categorys[j].id;
			$("input[name='" + name + "'][value='" + id + "']").attr(
				"checked",
				"checked"
			);
		}
	}
}

export const loadLoading = (str) => {
	$(str).prepend(renderLoading(""));
};

export const setTokenSession = () => {
	localStorage.setItem("tokenSession", JSON.stringify({
		bearerToken: _BEARER_TOKEN,
		clientId: _CLIENT_ID,
		clientSecret: _CLIENT_SECRET,
		page: _PATH_PAGE,
	}));
}

export function getAccessToken() {
	let tokenSession = localStorage.getItem("tokenSession");
	const token = {
		access_token: tokenSession.AccessToken,
		refresh_token: tokenSession.RefreshToken,
		entitieId: tokenSession.entitieId,
		page: _PATH_PAGE,
	};

	////console.log(token);
	return token;
}

export function accessToken() {
	const token = {
		access_token: localStorage.getItem("access_token"),
		refresh_token: localStorage.getItem("refresh_token"),
		entitieId: localStorage.getItem("idEntitie"),
		page: _PATH_PAGE,
		vs: localStorage.getItem("_VS")
	};

	////console.log(token);
	return token;
}

export function jointActions(vars = null) {
	///console.log('jointActions index', vars);
	if (vars.Error == 1) {
		if (vars.status == "version") {
			addHtml({
				selector: 'body',
				type: 'prepend',
				content: renderConfirmVersion({
					vs: vars.vs
				})
			}) //type: html, append, prepend, before, after
		}
	}
}

export function createInnerForm(vars) {
	const system = vars.system;
	const module = vars.module;
	const fn = vars.fn;
	$(".bodyModule[system='" + system + "'][module='" + module + "']").prepend(
		'<div class="innerForm"  module="' +
		module +
		'" system="' +
		system +
		'" fn="' +
		fn +
		'"><div class="loadingPageModule"><div class="loadingBar"></div></div></div>'
	);
	$(
		".bodyModule[system='" + system + "'][module='" + module + "'] .inner"
	).addClass("on");
	$(
		".bodyModule[system='" +
		system +
		"'][module='" +
		module +
		"'] .innerForm"
	).addClass("on");
	$(".bodyModule[system='" + system + "'][module='" + module + "']").addClass(
		"backOff"
	);
}


export function deleteDiacritics(texto) {
	return texto.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

export const replaceAccents = function (cadena) {
	var chars = {
		á: "a",
		é: "e",
		í: "i",
		ó: "o",
		ú: "u",
		à: "a",
		è: "e",
		ì: "i",
		ò: "o",
		ù: "u",
		ñ: "n",
		Á: "A",
		É: "E",
		Í: "I",
		Ó: "O",
		Ú: "U",
		À: "A",
		È: "E",
		Ì: "I",
		Ò: "O",
		Ù: "U",
		Ñ: "N",
	};
	var expr = /[áàéèíìóòúùñ]/gi;
	var res = cadena.replace(expr, function (e) {
		return chars[e];
	});
	return res;
};

/* export const convertUrlPath = (text) => {

	//console.log("url");
	var text = deleteDiacritics(text);
	text = text.toLowerCase(); // a minusculas
	text = text.replace(/ /g, "-");

	text = text.replace("!", "");
	text = text.replace("'", "");
	text = text.replace("(", "");
	text = text.replace(")", "");
	text = text.replace("*", "");
	text = text.replace('"', "");
	// text = text.replace(';', '-');
	// text = text.replace('; ', '-');
	// text = text.replace(':', '-');
	// text = text.replace(': ', '-');
	text = text.replace('"', "");
	text = text.replace("'", "");
	text = text.replace(",", "");
	text = text.replace(/ [{()}:,;*"'”“` ] /g, "");

	// text = text.replace(/[ýÿ]/, 'y');
	// text = text.replace(/[ñ]/, 'n');
	// text = text.replace(/[ç]/, 'c');
	// text = text.replace(/[&]/, 'y');
	// text = text.replace(/[ó]/, 'o')

	// text = text.replace(/[áàäâå]/, 'a');
	// text = text.replace(/[éèëê]/, 'e');
	// text = text.replace(/[íìïî]/, 'i');
	// text = text.replace(/[óòöô]/, 'o');
	// text = text.replace(/[úùüû]/, 'u');

	// // text = text.replace(/[^a-zA-Z0-9]/, '');

	// text = text.replace(/[,]/, '-');
	// text = text.replace(/['"`''""]/, '');
	text = text.replace(/['"`''"":;“”]/, "");

	// // text = text.replace(/(')s+/, 's');
	// // text = text.replace(/...+/, '-');
	// // text = text.replace(/(_)$/, '-');
	// // text = text.replace(/[?¡¿!(),:]/, '');
	// // text = text.replace(/(')$/, '');
	text = text.replace(/[.]$/, "");
	// text = text.replace(/[..]$/, '');
	// text = text.replace(/[...]$/, '');
	// text = text.replace(/[....]$/, '');
	// // text = text.replace(/(')$/, '');
	// text = text.replace(/(')$/, '');
	text = text.replace(/(“)$/, '')
	text = text.replace(/(”)$/, '')
	// text = text.replace(/[']$/, '');
	// text = text.replace(/["]$/, '');
	// text = text.replace(/(,)$/, '-');
	// text = text.replace(/(_)/, '');
	// text = text.replace(/^(:)/, '-');
	// text = text.replace(/ +/g, '-');
	// text = text.replace(/-+/g, '-');
	// text = text.replace(/&.,+?;/g, '-');

	let texto = text.split("");

	////console.log(texto[ texto.length -1 ]);

	if (texto[texto.length - 1] == "-" || texto[texto.length - 1] == " ") {
		text = text.substring(0, texto.length - 1);
	}

	return text;
} */

export const convertUrlPath = (str) => {
	let text = deleteDiacritics(str);
	text = text.toLowerCase().replace(/ /g, "-");


	// Reemplazar caracteres especiales y otros no deseados
	text = text.replace(/[!¡&*@~%`'"|#^+()*:;,.\[\]¿?=–"]/g, "");

	// Eliminar caracteres especiales al final si es un guion o espacio
	text = text.replace(/[- ]+$/, "");

	// Reemplazar puntos y barras
	text = text.replace(/[.\/]/g, "-");

	let texto = text.split("");

	if (texto[texto.length - 1] == "-" || texto[texto.length - 1] == " ") {
		text = text.substring(0, texto.length - 1);
	}

	return text;
}