export function translate() {
    //console.log("Translate")
    const lang = [
        { es: "Ingresar", en: "Sign In" },
        { es: "Ingresa con tu cuenta", en: "Login with your account" },
        { es: "¿ Olvidaste tu contraseña ?", en: "forgot your password?" },
        { es: "Mi cuenta", en: "My account" },
        { es: "Ver equipo y grupos", en: "View team and groups" },
        { es: "Configuraciones", en: "Configurations" },
        { es: "Cerrar Sessión", en: "Close Session" },
        { es: "Super Administrador", en: "Super Manager" },
    ]

    if (_LOCALE == "en") {
        var obj = document.querySelectorAll("[lang='es']")

        obj.forEach(function (node, index) {
            //console.log(node.text)
            let str = node.innerHTML
            //console.log(index, node.text, node.innerHTML)
            for (var valor of lang) {
                //console.log("Valor: " + valor.es, valor.en)
                if (valor.es == str) {
                    node.innerHTML = valor.en
                }
            }
        })
    }
}
