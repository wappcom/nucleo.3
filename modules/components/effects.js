export const buttonsEffects = (vars) => {
    console.log('buttonsEffects');
    let id = vars.id;
    let innerHtml = vars.innerHtml ? vars.innerHtml : '';
    let cls = vars.cls ? vars.cls :'';
    let effect = vars.effect ? vars.effect :'';
    let icon = vars.icon? vars.icon:'';

    $("#"+id).addClass(cls);
    $("#"+id).addClass(effect);
    $("#"+id+" i").removeClass();
    $("#"+id+" i").addClass(icon);
    $("#"+id).html(innerHtml);
}