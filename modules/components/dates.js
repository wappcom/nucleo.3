import { capitalize } from "./functions.js";

export const literalDay = (vars) => {
	let today = vars.dateInput;
	let type = vars.type ? vars.type : "default";
	let mode = vars.mode ? vars.mode : "complete";
	let array = today.split("/");
	//console.log(array, type);
	if (type == "default") {
		var objFecha = new Date(array[0], array[1], array[2]);
		console.log(objFecha);
		return objFecha.toLocaleDateString();
	}
	if (type == "inverse") {
		if (mode == "complete") {
			var options = {
				weekday: "long",
				year: "numeric",
				month: "long",
				day: "numeric",
			};
			//console.log(array[2] + "-" + array[1] + "-" + array[0]);
			return capitalize(
				new Date(
					array[2] + "-" + array[1] + "-" + array[0] + "T00:00:00"
				).toLocaleDateString("es-Es", options)
			);
		}
		if (mode == "bd") {
			return new Date(
				array[2] + "-" + array[1] + "-" + array[0]
			).toLocaleDateString("es-Es", {
				year: "numeric",
				month: "2-digit",
				day: "2-digit",
			});
		}
	}
};

export const dayLiteral = (vars) => {
	//console.log("dayLiteral", vars);
	let today = vars.dateInput;
	if (today == "" || today == null) {
		return "vacio";
	} else {
		//let type = vars.type ? vars.type : "default";
		let mode = vars.mode ? vars.mode : "default";
		let array = today.split("-");
		/* if (type == "default") {
			var objFecha = new Date(today);
			return objFecha.toLocaleDateString();
		} */
		//console.log(type, mode, array)

		if (mode == "default") {
			var options = {
				weekday: "long",
				year: "numeric",
				month: "long",
				day: "numeric",
			};
			//const event = new Date(array[2] + "-" + array[1] + "-" + array[0]);
			today = today.replace(/-/g, "/");
			const event = new Date(today);
			return capitalize(event.toLocaleDateString("es-Es", options));
		}
		if (mode == "compact") {
			var options = {
				weekday: "short",
				year: "numeric",
				month: "short",
				day: "numeric",
			};
			//const event = new Date(array[2] + "-" + array[1] + "-" + array[0]);
			today = today.replace(/-/g, "/");
			const event = new Date(today);
			return capitalize(event.toLocaleDateString("es-Es", options));
		}
		if (mode == "complete") {
			var options = {
				weekday: "long",
				year: "numeric",
				month: "long",
				day: "numeric",
				hour: "numeric",
				minute: "numeric",
			};
			//const event = new Date(array[2] + "-" + array[1] + "-" + array[0]);
			today = today.replace(/-/g, "/");
			const event = new Date(today);
			return capitalize(event.toLocaleDateString("es-Es", options));
		}
		if (mode == "bd") {
			return new Date(
				array[2] + "-" + array[1] + "-" + array[0]
			).toLocaleDateString("es-Es", {
				year: "numeric",
				month: "2-digit",
				day: "2-digit",
			});
		}
	}
};

export const financial = (num) => {
	const x = num.replace(",", ".");
	const nenNum = Number.parseFloat(x).toFixed(2);
	return nenNum.replace(".", ",");
};

export const unDbDate = (dateStr) => {
	//console.log("unDbDate",dateStr);

	if (dateStr == "" || dateStr == null) {
		return "";
	}
	// saber si hay espacio antes
	let str = dateStr;
	let obj = str.split(" ");
	let dat = obj[0].split("-");
	let hour = [];
	let hr = "";
	
	if (obj.length > 1) {
		
		if (obj[1]) {
			hour = obj[1].split(":");
			hr = " " + hour[0] + ":" + hour[1];
		} else {
			hr = "";
		}
	}
	//console.log(dat, hour);
	if (dat[0].length == 4) {
		return dat[0] + "-" + dat[1] + "-" + dat[2] + hr;
	}
	if (dat[0].length == 2) {
		return dat[2] + "-" + dat[1] + "-" + dat[0] + hr;
	}
};

export const convertLiteralToDbDate = (dateStr) => {
	let obj = dateStr.split(", ");
	let dat = obj[1].split(" ");
	let monthStr = "01";
	//console.log(dat[0], dat[2], dat[4]);
	const day = (dat[0] < 10 ? "0" : "") + dat[0];

	var d = Date.parse(dat[2] + "1, 2012");
	if (!isNaN(d)) {
		monthStr = new Date(d).getMonth() + 1;
	}

	return dat[4] + "-" + monthStr + "-" + day;
};


// today 
export const today = () => {
	const today = new Date();
	const dd = String(today.getDate()).padStart(2, "0");
	const mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
	const yyyy = today.getFullYear();
	return yyyy + "-" + mm + "-" + dd;
}

// today and time
export const todayTime = (vars = []) => {
	let today = new Date();
	let dd = String(today.getDate()).padStart(2, "0");
	let mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
	let yyyy = today.getFullYear();
	let hh = String(today.getHours()).padStart(2, "0");
	let min = String(today.getMinutes()).padStart(2, "0");
	let addHours = vars.addHours ? vars.addHours : 0;

	// add hours
	if (addHours > 0) {
		// add hour to todaydStart(2, "0");
		if (today.getHours() < 24) {
			hh = today.getHours() + 1;
		} else {
			hh = '00';
		}
	}

	return yyyy + "-" + mm + "-" + dd + " " + hh + ":" + min;
}
