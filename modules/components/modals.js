import {
    resizeWorkspace
} from "./nav.js";
import {
    renderModalDelete,
    renderTooltip,
    renderDeleteModal
} from "./renders/renderModals.js";
import {
    errorInput,
    focusInput,
} from './forms.js';
import {
    deleteItemModule,
    removeItemTable
} from './tables.js';
import * as nav from './nav.js';


export const removeUIJS = () => {
    var scripts = document.getElementsByTagName('script');
    // Recorrer todos los elementos script
    for (var i = 0; i < scripts.length; i++) {
        var script = scripts[i];

        // Verificar si la extensión es ".ui.js"
        if (script.src && script.src.endsWith('.ui.js')) {
        // Eliminar el script
        script.parentNode.removeChild(script);
        }
    } 
}


document.addEventListener("DOMContentLoaded", function () {

    $(".bodyModulo").bind('resize', function (e) {
        //console.log('.bodyModule', e);
    })

    $("body").on("click", ".btnBack", function (e) {
        $(".innerForm").removeClass("on");
        removeInnerForm();
    });

    $("body").on("click", ".btnCancel", function (e) {
        removeInnerForm();
        let id = $(this).attr("for");
        if (id != "") {
            $("#" + id).remove();
        } else {
            $(".modal").remove();
        }
        $(".formModal").remove();
        $(".bodyModule").removeClass("backOff");
        resizeWorkspace();
    });

    $("body").on("click", ".btnCloseModal", function (e) {
        removeInnerForm();
        let id = $(this).attr("for");
        console.log("btnCloseModal", id);
        if (id != "") {
            $("#" + id).remove();
        } else {
            $(".modal").remove();
            $(".formModal").remove();
            $(".bodyModule").removeClass("backOff");
        }

        resizeWorkspace();
    });

    $("body").on("click", `.btnCloseInnerWindow`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let id = $(this).attr('for');
        console.log(`.btnCloseInnerWindow`, id);
        $('#' + id).remove();
    });

    $("body").on("click", ".btnCancelModalRender", function (e) {
        e.preventDefault()
        e.stopPropagation();
        let id = $(this).attr("for");
        if (id == "" || id == undefined) {
            $(".modalRender").remove();
        } else {
            $("#" + id).remove();
        }

        removeUIJS();

    })
    $("body").on("click", ".btnCancelModal", function (e) {
        e.preventDefault()
        e.stopPropagation()
        let id = $(this).attr("for");
        if (id == "" || id == undefined) {
            $(".modalRender").remove();
        } else {
            $("#" + id).remove();
        }

        removeUIJS();
    })

    $("body").on("click", `.modalBgBack`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        console.log(`.modalBgBack`);
        let id = $(this).attr("for");

        if (id == "" || id == undefined) {
            $(".modalRender").remove();
        } else {
            $("#" + id).remove();
        }

        removeUIJS();
    });

    /*$("body").on("click", `.modalConfig .modalInner`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
    }); */

    $("body").on("click", ".btnModalClose", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        ////console.log(".btnModalClose");
        const id = $(this).attr("for")
        const fn = $(this).attr("fn")
        $("#" + id).remove();
        if (fn != "" && fn != undefined) {
            eval(fn + "()");
        }
    });


    $("body").on("click", ".btnActionModal", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let id = $(this).parent().attr("for");
        let content = $(this).attr("content");
        let module = $(this).attr("module");
        let fn = $(this).attr("fn");
        let vars = $(this).attr("vars");
        let funcName = "nav.router." + module + '.' + fn;
        ////console.log(id)
        //console.log(fn, typeof fn)

        $(".btnActionModal").removeClass("active");
        $(".contents .content").removeClass("active");
        $("#" + content).addClass("active");
        $(".btnActionModal[content='" + content + "']").addClass("active");

        if (fn != "" && fn != null) {
            ////console.log(fn + '(' + vars + ')');
            eval(funcName + "(" + vars + ")");
        }
    });


    $("body").on("click", ".btnDeleteConfirm", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        let fn = $(this).attr("fn");
        let fnReturn = $(this).attr("fnreturn");
        let varsReturn = $(this).attr("varsreturn");
        let vars = $(this).attr("vars");
        let module = $(this).attr("module");

        console.log('.btnDeleteConfirm', fn, fnReturn, varsReturn, vars, module);

        if (fn != "fnDeleteModuleItem") {
            ////console.log("otro")

            if (module != "" && fn == "") {
                let funcName = "nav." + module + '.' + fn;
                const va = vars != "" ? vars : "";
                if (fn != "" && fn != null) {
                    //console.log(fn + '(' + vars + ')');
                    eval(funcName + "('" + vars + "','" + fnReturn + "','" + varsReturn + "')");
                }
            }
        }

        if (fn == "deleteItemModule") {
            console.log("deleteItemModule", item, module);
            const item = vars;

            deleteItemModule({
                item,
                module
            }).then((response) => {
                console.log('deleteItemModule response', response);
                if (response.Error == 0) {
                    $(".modalConfirm").remove();
                    removeItemTable(item);
                    //advisedIndex()
                } else {
                    alertPage({
                        text: "Error. por favor contactarse con soporte. " +
                            response.message,
                        icon: "icn icon-alert-warning",
                        animation_in: "bounceInRight",
                        animation_out: "bounceOutRight",
                        tipe: "danger",
                        time: "3500",
                        position: "top-left",
                    });
                }
            });
        }

        if (fn == "deleteItemsMedia") {
            if (module != "" && fn != "") {
                let funcName = "nav." + module + '.' + fn;
                const va = vars != "" ? vars : "";
                if (fn != "" && fn != null) {
                    console.log(funcName + "('" + vars + "','" + fnReturn + "','" + varsReturn + "')");
                    eval(funcName + "('" + vars + "','" + fnReturn + "','" + varsReturn + "')");
                }
            }
        }
    })

    $("body").on("click", `.btnInfoModal`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        // let data = $(this).data();
        let forAttr = $(this).attr("for");
        console.log(`.btnInfoModal`, forAttr);

        if (forAttr != "" && forAttr != undefined) {
            $(".boxInfoModal").appendTo("body");
            $(".boxInfoModal").toggleClass("on");
            $(this).toggleClass("on");
        }

    });

    $("body").on("mouseleave", `.boxInfoModal`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        $(this).toggleClass("on");
        $(".btnInfoModal").toggleClass("on");
    });

    $("body").on("change", `.formInput.error`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        $(this).removeClass("error");
    });

    document.querySelectorAll(".btnCloseModal").forEach(function (e) {
        e.addEventListener("click", function () {
            //console.log("Has hecho click")
        })
    })
})

export const deleteModal = (vars = []) => {
    //console.log('deleteModal', vars);

    let classBtnAction = vars.classBtnAction ? vars.classBtnAction : 'deleteItem';

    $(".workspace").prepend(
        renderModalDelete({
            id: vars.id,
            name: vars.name,
            attr: vars.attr,
            cls: vars.cls,
            classBtnAction,
        })
    )
}

export const modalDelete = (vars = []) => {
    console.log('modalDelete', vars);
    $(".workspace").prepend(
        renderDeleteModal({
            id: vars.id,
            name: vars.name,
            attr: vars.attr,
            cls: vars.cls
        })
    )
}

export const removeInnerForm = (vars) => {
    ////console.log('removeInnerForm');
    $(".innerForm").removeClass("on");
    setTimeout(function () {
        $(".innerForm").remove();
        $(".bodyModule").removeClass("backOff");
        resizeWorkspace();
    }, 500);
}

export function deleteModalItem(vars) {
    $(".workspace").prepend(
        renderModalDelete({
            id: vars.item,
            name: vars.name,
            fn: vars.fn,
            classBtnAction: vars.classBtnAction,
            attr: vars.attr,
            cls: vars.cls,
            fnReturn: vars.fnReturn,
            varsReturn: vars.varsReturn,
            vars: vars.vars,
            module: vars.module,
        })
    )
    /*
        nameFile + "," + returnaction + "," + id + "-" + x
        $("body").on("click", ".btnConfirmRemoveFile", function(e) {
            e.preventDefault()
            e.stopPropagation()
            let id = $(this).attr("for")
            $("#" + id).remove()
            loadItemMedia({ img: varsArray, index: x, id: id, name: nameFile, type: type })
            if (xd <= 1) {
                loadInitChargeMedia(id)
            }
        })

        $("body").on("click", ".btnCancelModalRenameFile", function(e) {
            e.preventDefault()
            e.stopPropagation()
            let id = $(this).attr("for")
            let item = varsArray[1]
            removeFileId(item)
            $("#" + id).remove()
            if ($(".boxInputs").html() == "") {
                $("." + returnitem).html(eval("render" + returnitem + "()"));
                $(".inputFileBtn").removeClass("on");
            }
        })*/
}

export const tooltip = (vars = []) => {
    //console.log('tooltip', vars);
    let selector = vars.selector ? vars.selector : "";
    let text = vars.text ? vars.text : "";
    let position = vars.position ? vars.position : "top";
    let animation_in = vars.animation_in ? vars.animation_in : "bounceIn";
    let animation_out = vars.animation_out ? vars.animation_out : "bounceOut";

    $("body").prepend(renderTooltip({
        selector,
        text,
        position
    }));

    let positionSelector = $(selector).offset();
    let wSelector = $(selector).outerWidth();
    let hSelector = $(selector).outerHeight();
    let positionSelectorTop = positionSelector.top;
    let positionSelectorLeft = positionSelector.left;
    let wTooltip = $(".tooltip[for='" + selector + "']").outerWidth();
    let hTooltip = $(".tooltip[for='" + selector + "']").outerHeight();

    ////console.log( selector, positionSelectorTop, positionSelectorLeft, wSelector, hSelector);

    let pTop = positionSelectorTop - hTooltip;
    let pLeft = positionSelectorLeft + (wSelector / 2) - (wTooltip / 2);

    $(".tooltip[for='" + selector + "']").css("top", pTop + "px");
    $(".tooltip[for='" + selector + "']").css("left", pLeft + "px");
}

export function removeModal(vars = []) {
    let timer = vars.timer ? vars.timer : "500";
    let id = vars.id ? vars.id : "";
    let animation = vars.animation ? vars.animation : "bounceOut";
    let aux = '';
    $(".modalConfirm .modalInner").removeClass("bounceIn");
    $(".modalRender .modalInner").removeClass("bounceIn");
    $(".modalRender .modalInner").removeClass("enlarge");

    if (id == "") {

        if (vars.animation != "" && vars.timer == "500") {
            $(".modalConfirm .modalInner").addClass(animation);
            $(".modalRender .modalInner").addClass(animation);
        } else {
            $(".modalConfirm").remove();
            $(".modalRender").remove();
        }

        setTimeout(() => {
            $(".modalConfirm").remove();
            $(".modalRender").remove();
        }, timer);
    } else {
        aux = '#' + id;
        if (vars.animation != "" && vars.timer == "500") {
            $(aux + " .modalInner").addClass(animation);
            $(aux + " .modalInner").addClass(animation);
        } else {
            $(aux + " .modalConfirm").remove();
            $(aux + " .modalRender").remove();
        }

        setTimeout(() => {
            $(aux).remove();
        }, timer);
    }

}

export const closeModal = (vars = []) => {
    ////console.log('closeModal',vars);
    $(".modal").remove();
    $(".modalRender").remove();
}

export function removeModalId(id) {
    $("#" + id).remove()
}

export function draggable(element) {
    var isMouseDown = false

    // initial mouse X and Y for `mousedown`
    var mouseX
    var mouseY

    // element X and Y before and after move
    var elementX = 0
    var elementY = 0

    // mouse button down over the element
    element.addEventListener("mousedown", onMouseDown)

    /**
     * Listens to `mousedown` event.
     *
     * @param {Object} event - The event.
     */
    function onMouseDown(event) {
        mouseX = event.clientX
        mouseY = event.clientY
        isMouseDown = true
    }

    // mouse button released
    element.addEventListener("mouseup", onMouseUp)

    /**
     * Listens to `mouseup` event.
     *
     * @param {Object} event - The event.
     */
    function onMouseUp(event) {
        isMouseDown = false
        elementX = parseInt(element.style.left) || 0
        elementY = parseInt(element.style.top) || 0
    }

    // need to attach to the entire document
    // in order to take full width and height
    // this ensures the element keeps up with the mouse
    document.addEventListener("mousemove", onMouseMove)

    /**
     * Listens to `mousemove` event.
     *
     * @param {Object} event - The event.
     */
    function onMouseMove(event) {
        if (!isMouseDown) return
        var deltaX = event.clientX - mouseX
        var deltaY = event.clientY - mouseY
        element.style.left = elementX + deltaX + "px"
        element.style.top = elementY + deltaY + "px"
    }
}

export function resizeModalConfig(system) {
    const w = $(window).outerWidth();
    const h = $(window).outerHeight();
    let wModalConfig = $(".modalConfig[system='" + system + "'] .modalInner").outerWidth();
    let hModalConfig = $(".modalConfig[system='" + system + "'] .modalInner").outerHeight();
    let hModalConfigHead = $(".modalConfig[system='" + system + "'] .modalInner .head").outerHeight();
    const hModalConfigBody = hModalConfig - hModalConfigHead;
    const wSidebar = $(".modalConfig[system='" + system + "'] .modalInner .sidebarMenu").outerWidth();
    const wAside = wModalConfig - wSidebar - 10;
    const hAside = hModalConfigBody - 30;
    ////console.log(hModalConfigHead,system, w, h)
    $(".modalConfig[system='" + system + "']  .modalInner >.tbody").outerHeight(hModalConfigBody);
    $(".modalConfig[system='" + system + "']  .modalInner .contents").outerWidth(wAside);
    //$(".modalConfig[system='" + system + "']  .modalInner .content").outerWidth(wAside);
    $(".modalConfig[system='" + system + "']  .modalInner .content").outerHeight(hModalConfigBody);
    $(".modalConfig[system='" + system + "']  .modalInner .content .tbody").outerHeight(hAside);
}

export const resizeModal = (vars = []) => {
    //console.log('resizeModal', vars);
    const h = $(window).outerHeight();
    let wModalConfig = $(".modalConfig .modalInner").outerWidth();
    let hModalConfig = $(".modalConfig .modalInner").outerHeight();
    let hModalConfigHead = $(".modalConfig .modalInner .head").outerHeight();
    const wAside = wModalConfig - 10;
    const hAside = hModalConfig - 30;
    const hModalConfigBody = hModalConfig - hModalConfigHead - 1;

    $(".modalConfig .modalInner >.tbody").outerHeight(hModalConfigBody);
    $(".modalConfig .modalInner .contents").outerWidth(wAside);
    //$(".modalConfig .modalInner .content").outerWidth(wAside);
    $(".modalConfig .modalInner .content").outerHeight(hModalConfigBody);
    $(".modalConfig .modalInner .content .tbody").outerHeight(hAside);
}
// test