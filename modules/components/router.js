/*init*/
import * as worksheets from "../websites/components/worksheets.js";
export {
    worksheets
};
import * as blocks from "../websites/components/blocks.js";
export {
    blocks
};
import * as publications from "../websites/components/publications.js";
export {
    publications
};
import * as websites from "../websites/components/websites.js";
export {
    websites
};
import * as contents from "../websites/components/contents.js";
export {
    contents
};
import * as categorys from "../websites/components/categorys.js";
export {
    categorys
};
import * as media from "../websites/components/media.js";
export {
    media
};
import * as docs from "../websites/components/docs.js";
export {
    docs
};
import * as posts from "../websites/components/posts.js";
export {
    posts
};
import * as links from "../websites/components/links.js";
export {
    links
};
import * as sliders from "../websites/components/sliders.js";
export {
    sliders
};
import * as inventory from "../inventory/components/inventory.js";
export {
    inventory
};
import * as products from "../inventory/components/products.js";
export {
    products
};
import * as stock from "../inventory/components/stock.js";
export {
    stock
};
import * as pricelist from "../inventory/components/pricelist.js";
export {
    pricelist
};
import * as accounting from "../accounting/components/accounting.js";
export {
    accounting
};
import * as counseling from "../counseling/components/counseling.js";
export {
    counseling
};
import * as advised from "../counseling/components/advised.js";
export {
    advised
};
import * as calendarAdvised from "../counseling/components/calendarAdvised.js";
export {
    calendarAdvised
};
import * as reportsCounseling from "../counseling/components/reportCounseling.js";
export {
    reportsCounseling
};
import * as sales from "../sales/components/sales.js";
export {
    sales
};
import * as salesPointTickets from "../sales/components/salesPointTickets.js";
export {
    salesPointTickets
};
import * as brokers from "../brokers/components/brokers.js";
export {
    brokers
};
import * as brokersCustomers from "../brokers/components/brokersCustomers.js";
export {
    brokersCustomers
};
import * as suscriptions from "../accounts/components/subscriptions.js";
export {
    suscriptions
};
import * as accountsUsers from "../accounts/components/accountsUsers.js";
export {
    accountsUsers
};
import * as customers from "../accounts/components/customers.js";
export {
    customers
};
import * as fls from "../fls/components/fls.js";
export {
    fls
};
import * as flsCustomers from "../fls/components/flsCustomers.js";
export {
    flsCustomers
};
import * as tickets from "../tickets/components/tickets.js";
export {
    tickets
};
import * as ticketingEvents from "../tickets/components/ticketingEvents.js";
export {
    ticketingEvents
};
import * as rehabilitation from "../rehabilitation/components/rehabilitation.js";
export {
    rehabilitation
};
import * as rehabilitationRegistry from "../rehabilitation/components/rehabilitationRegistry.js";
export {
    rehabilitationRegistry
};
import * as ads from "../ads/components/ads.js";
export {
    ads
};
import * as displayAds from "../ads/components/displayAds.js";
export {
    displayAds
};
import * as lms from "../lms/components/lms.js";
export {
    lms
};
import * as syllabus from "../lms/components/syllabus.js";
export {
    syllabus
};
import * as fixedassets from "../fixedassets/components/fixedassets.js";
export {
    fixedassets
};
import * as assets from "../fixedassets/components/assets.js";
export {
    assets
};
import * as operations from "../operations/components/operations.js";
export {
    operations
};
import * as crm from "../crm/components/crm.js";
export {
    crm
};
import * as customersCrm from "../crm/components/customersCrm.js";
export {
    customersCrm
};
import * as suscriptionsCrm from "../crm/components/suscriptionsCrm.js";
export {
    suscriptionsCrm
};
import * as rrhh from "../rrhh/components/rrhh.js";
export {
    rrhh
};
import * as organizationChart from "../rrhh/components/organizationChart.js";
export {
    organizationChart
};
import * as externaldocuments from "../externaldocuments/components/externaldocuments.js";
export {
    externaldocuments
};
import * as inboxEd from "../externaldocuments/components/inboxEd.js";
export {
    inboxEd
};
import * as templatesEd from "../externaldocuments/components/templatesEd.js";
export {
    templatesEd
};
import * as trakingEd from "../externaldocuments/components/trakingEd.js";
export {
    trakingEd
};
import * as geolocation from "../geolocation/components/geolocation.js";
export {
    geolocation
};
import * as places from "../geolocation/components/places.js";
export {
    places
};
import * as ganadorapp from "../ganadorapp/components/ganadorapp.js";
export {
    ganadorapp
};
import * as proyectosGA from "../ganadorapp/components/proyectosGA.js";
export {
    proyectosGA
};
import * as neoganadores from "../ganadorapp/components/neoganadores.js";
export {
    neoganadores
};
import * as ganaderos from "../ganadorapp/components/ganaderos.js";
export {
    ganaderos
};
import * as sports from "../sports/components/sports.js";
export {
    sports
};
import * as futbol from "../sports/components/futbol.js";
export {
    futbol
};
import * as competitions from "../competitions/components/competitions.js";
export {
    competitions
}
import * as app_tributo from '../app_tributo/components/app_tributo.js';
export {
    app_tributo
}
import * as app_tributo_selection from '../app_tributo/components/app_tributo_selection.js';
export {
    app_tributo_selection
}
import * as app_candire from '../app_candire/components/app_candire.js';
export {
    app_candire
}
import * as app_candire_campaigns from '../app_candire/components/app_candire_campaigns.js';
export {
    app_candire_campaigns
}
import * as appointments from '../appointments/components/appointments.js';
export {
    appointments
}
import * as agenda from '../appointments/components/agenda.js';
export {
    agenda
}
import * as calendars from '../appointments/components/calendars.js';
export {
    calendars
}
import * as appointmentsServices from '../appointments/components/appointmentsServices.js';
export {
    appointmentsServices
}
/*end*/