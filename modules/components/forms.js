import {
	emptyReturn,
	alertResponseMessageError,
	accessToken,
	convertUrlPath,
	empty,
	addHtml,
	removeSelector,
	activeSelector,
	unactiveSelector,
	capitalize
} from './functions.js';

import {
	rendermultipleFiles,
	rendermultipleFilesHover,
	rendersimpleFile,
	rendersimpleFileHover,
	rendersimpleFileMini,
	renderBoxMedia,
	rendermultipleFilesMini,
	renderNumItemsHead,
	renderBoxImage,
	renderEditMedia,
	renderItemInputContent,
	renderItemVideo,
	renderItemImage,
	renderItemSvg,
} from "./renders/renderForms.js";
import {
	deleteModalItem,
	removeModal,
	deleteModal
} from "./modals.js";
import {
	renderModal,
	renderModalRenameFile,
	renderModalDelete,
	renderDeleteModal
} from "./renders/renderModals.js";

import {
	finder
} from "./finder.js";

import * as nav from "./nav.js";

var porc = 0;
var xd = 0;

document.addEventListener("DOMContentLoaded", function () {
	$("body").on("focus", ".formInput[type='text']", function (e) {
		e.preventDefault();
		e.stopPropagation();
		let id = $(this).attr("id");

		//console.log("focus", id);

		let name = $(this).attr("name");
		if (id != "" && id != undefined) {
			if (id.indexOf("[]") !== -1) {
				id = name;
			}
			////console.log("focus:" + id)
			$("label[for='" + id + "']").addClass("on");
			$('.message[for="' + id + '"]').html('');
		}
	});

	$("body").on("change", ".form .formInput[type='checkbox']", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		var id = $(this).attr("id");
		console.log("change:", e.currentTarget.checked);
		if (e.currentTarget.checked) {
			$(this).val(1);
		} else {
			$(this).val(0);
		}
		//$(this).val($(this).checked)  	
	})

	$("body").on("click", ".btnCerrarInputGroup", function (e) {
		e.preventDefault();
		e.stopPropagation();
		var item = $(this).attr("item");
		$("#inputGroup" + item).remove();
	});

	$("body").on("blur", ".form .formInput[type='text']", function (e) {
		$('.formInput[type="text"]').each(function (i, obj) {
			var id = $(this).attr("id");
			var name = $(this).attr("name");
			var value = "";
			//console.log(id + ":" + id.indexOf("[]"));
			//console.log("blur", id);
			if (id != "" && id != undefined) {
				if (id.indexOf("[]") !== -1) {
					////console.log("name:" + name)
					value = $("*[name='" + name + "']").val();
					id = name;
				} else {
					value = $("#" + id).val();
				}
				//           //console.log(i + ":" + obj + $(this).attr("id"))
				if (value == "") {
					$("label[for='" + id + "']").removeClass("on");
				}
			}


		});
	});

	$("body").on("click", ".btnSelectMedia", function (e) {
		e.preventDefault();
		e.stopPropagation();
		let item = $(this).attr("item");
		let forId = $(this).parents().eq(2).attr("for");
		let contentId = $(this).parents().eq(2).attr("item");
		let state = $(this).attr("state"); 
		let data = $(this).data();

		console.log("btnSelectMedia", item, forId, state, data, contentId  );

		$(".formChargeMedia[for='" + forId + "'] .item[item='" + item + "'] .btnSelectMedia i").removeClass();
		if (state == 0) {
			$(this).attr("state", 1);
			// $(".formChargeMedia[for='" + id + "'] .btnSelectMedia i").removeClass()
			$(".formChargeMedia[for='" + forId + "'] .item[item='" + item + "']").addClass("on");
			$(".formChargeMedia[for='" + forId + "'] .item[item='" + item + "'] .btnSelectMedia i").addClass("icon icon-unchecked");
			$(".formChargeMedia[for='" + forId + "'] .item[item='" + item + "']").attr("select", "on");
			loadTrashCountMedia(1, forId, data.module, contentId);
		} else {
			$(this).attr("state", 0);
			$(".formChargeMedia[for='" + forId + "'] .item[item='" + item + "']").removeClass("on");
			$(".formChargeMedia[for='" + forId + "'] .item[item='" + item + "'] .btnSelectMedia i").addClass("icon icon-checked-o");
			$(".formChargeMedia[for='" + forId + "'] .item[item='" + item + "']").attr("select", "");
			loadTrashCountMedia(0, forId, data.module, contentId);
		}
		////console.log(item)
		////console.log(forId)
	});

	$("body").on("click", ".btnTrashImage", function (e) {
		e.preventDefault();
		e.stopPropagation();
		let item = $(this).attr("item");
		let forId = $(this).parents().eq(2).attr("for");
		let name = $(this).attr("name");
		let module = $(this).attr("module");
		let fn = $(this).attr("fn");
		let state = $(this).attr("state");
		console.log(".btnTrashImage", item)
		//console.log(forId)
		deleteModalItem({
			item,
			name,
			vars: item,
			module,
			fn,
			fnReturn: 'loadChargeImageInit',
			varsReturn: forId
		});

		//$(".formChargeImage[for='" +forId +"']").html(loadInitChargeSimple(forId));

	});

	$("body").on("click", ".btnTrashItems", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let id = $(this).attr("for");
		let idItemForm = $(".formChargeMedia[for='" + id + "']").attr("data-id");
		let module = $("#" + id).attr("module");
		let data = $(this).data();
		//console.log('.btnTrashItems', id);
		let array = [];
		let arrayName = [];

		$(".formChargeMedia[for='" + id + "'] .itemMedia[select='on']").each(function (e) {
			////console.log(e);
			////console.log($(this).attr("item"));
			const item = $(this).attr("item");
			const name = $(this).attr("name");
			array[e] = item;
			arrayName[e] = name;
		});

		////console.log(array, arrayName.join(", "));

		/* deleteModalItem({
			item: array,
			name: arrayName.join(", "),
			vars: array + ":" + idItemForm,
			module,
			cls: "formChargeMedia",
			fn: 'deleteItemsMedia',
			fnReturn: 'loadChargeMediaInit',
			varsReturn: id
		}); */

		/* deleteModalItem({
			name: arrayName.join(", "),
			item: 'ModalDelete' + id,
			fn: 'deleteItemsMedia',
			classBtnAction: 'btnConfirmDelete',
			attr: `data-id="${idItemForm}" data-item="${data.item}" data-module="${module}"  data-array= "${array.join(',')}"`
		}); */


		deleteModal({
			id: "modalDelete" + id,
			name: "Items "+array.join(','),
			classBtnAction: 'btnConfirmDeleteMedia'+capitalize(data.module),
			attr: `data-id="${idItemForm}" data-item="${data.item}" data-module="${module}"  data-array= "${array.join(',')}"`,
		})
		

	})

	$("body").on("keyup", ".formControlSearch input", function () {
		let id = $(this).attr("id");
		let str = $(this).val();

		if (str.length > 1) {
			$(".formControlSearch .iconClear").addClass("on");
		} else {
			$(".formControlSearch .iconClear").removeClass("on");
		}
	});

	$("body").on("click", `.btnClearInputForm`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let id = $(this).attr("for");
		console.log(`.btnClearInputForm`, data);
		let input = document.getElementById(id);
		input.value = "";
		input.focus();
		// eliminar las clase .boxResults con javascript vainilla
		unactiveSelector('.boxResults');
	});

	$("body").on("mouseleave", `.boxResults`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		console.log(`.boxResults`, data);
		$(".boxResults .boxResultsInner").html("");
		unactiveSelector('.boxResults');
	});

	$("body").on("click", `.btnClosePill`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let fnReturn = "nav.router." + data.module + "." + data.fn;
		console.log(`.btnClosePill`, data, fnReturn);
		$(this).parent().remove();
		eval(fnReturn + "(data)");
	});

	$("body").on("click", ".btnChangeDate", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();

		let id = $(this).attr("for");
		let type = $(this).attr("type");
		//console.log("btnChangeDate:" + id + ":" + type);
		$("#" + id).removeClass("animated flash");
		$("#" + id).html(todaysDate(type));
		setTimeout(() => {
			$("#" + id).addClass("animated flash");
		}, 200);
	});

	$("body").on("dragover", ".inputFileBtn", function (e) {
		////console.log("dragover:")
		e.preventDefault();
		e.stopPropagation();
		let mode = $(this).attr("mode");
		let id = $(this).attr("id");
		$(".formChargeMedia[for='" + id + "'] .boxInputFile .boxInner").html(
			eval("render" + mode + "Hover()")
		);
		$(".formChargeImage[for='" + id + "'] .boxInputFile .boxInner").html(
			eval("render" + mode + "Hover()")
		);
		$(this).addClass("on");
	});

	$("body").on("dragover", `.inputUpFile`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let id = $(this).attr("id");
		console.log(`dragover .inputUpFile`,id);
		$(".inner[for='" + id + "']").addClass("on");
	});

	$("body").on("dragleave", `.inputUpFile`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let id = $(this).attr("id");
		console.log(`dragleave .inputUpFile`,id);
		$(".inner[for='" + id + "']").removeClass("on");
	});

	$("body").on("dragleave", ".inputFileBtn", function (e) {
		//console.log("dragoleave");
		e.preventDefault();
		e.stopPropagation();
		let mode = $(this).attr("mode");
		let id = $(this).attr("id");
		//console.log(xd);
		if (xd == 1) {
			$(
				".formChargeMedia[for='" + id + "'] .boxInputFile .boxInner"
			).html(eval("render" + mode + "()"));
			$(
				".formChargeImage[for='" + id + "'] .boxInputFile .boxInner"
			).html(eval("render" + mode + "()"));
		} else {
			$(
				".formChargeMedia[for='" + id + "'] .boxInputFile .boxInner"
			).html(eval("render" + mode + "Mini()"));
		}
		$(this).removeClass("on");
	});

	$("body").on("change", ".inputFileBtn", function (e) {
		e.preventDefault();
		e.stopPropagation();
		//resize_item();
		let id = $(this).attr("id");
		let folder = $(this).attr("folder");
		let validfiles = $(this).attr("validfiles");
		let mode = $(this).attr("mode");
		let arc = document.getElementById(id);
		let files = arc.files;
		let module = $(this).attr("module");
		let system = $(this).attr("system");
		saveFileFn({
			id,
			folder,
			validfiles,
			mode,
			module,
			system,
			files,
		});
	}); // fin inputArchivoFinder

	$("body").on("change", ".inputUpFile", function (e) {
		e.preventDefault();
		e.stopPropagation();
		//resize_item();

		let data = $(this).data();
		let id = $(this).attr("id");
		let inputName = data.name
		let folder = data.folder;
		let returnId = data.return;
		let validfiles = data.validfiles;
		let mode = data.mode;
		let arc = document.getElementById(id);
		let files = arc.files;
		let module = data.module
		let system = data.system
		let form = data.form
		let last = data.last
		let thumb = data.thumb
		saveFile({
			id,
			folder,
			validfiles,
			mode,
			module,
			system,
			cls: 'inputUpFile',
			inputName,
			returnId,
			files,
			form,
			last,
			thumb
		});
	}); // fin inputArchivoFinder


	//Selection List

	$("body").on("focus", ".inputSelectList", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		////console.log("inputSelectList")
		let id = $(this).attr("for");
		$(this).removeClass("error");
		$(".boxSelectList[for='" + id + "']").addClass("on");
		$(".inputSearch[for='" + id + "']").val("");
		$("#inputSearchAccountUsers[for='" + id + "']").val("");
		$(".otherResults").html("");
		$(".boxSelectList[for='" + id + "'] input[for='" + id + "']").focus();
	});

	$("body").on("keyup", ".inputSearch", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		////console.log('inputSearch: ' + $(this).val());

		let id = $(this).attr("for");
		var rex = new RegExp($(this).val(), "i");
		$(".btnSearchOut[for='" + id + "']").removeClass("disabled");
		$(".boxSelectList[for='" + id + "'] .list .btn").hide();
		$(".boxSelectList[for='" + id + "'] .list .btn")
			.filter(function () {
				return rex.test($(this).text());
			})
			.show();
	});

	$("body").on("click", `.btnSwitch`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let id = data.for;
		let form = data.form;
		let value = $(this).attr("value");
		let desactive = data.desactive;

		console.log(`.btnSwitch`, data, id, value, desactive);
		if (value == "1") {
			this.setAttribute("value", "0");
			$(`#${form} #${id}`).val("0");
		}
		if (value == "0") {
			this.setAttribute("value", "1");
			$(`#${form} #${id}`).val("1");
		}

		if (desactive != "" && desactive != undefined && desactive != null) {
			let arrayDesactive = desactive.split(",");
			if (arrayDesactive.length > 0) {
				for (let i = 0; i < arrayDesactive.length; i++) {
					$(`#${form} #${arrayDesactive[i]}`).val("0");
					$(`.btnSwitch[data-for='${arrayDesactive[i]}']`).attr("value", "0");
				}
			} else {
				$(`#${form} #${desactive}`).val("0");
				$(`.btnSwitch[data-for='${desactive}']`).attr("value", "0");
			}
		}
	});

	$("body").on("mouseleave", ".boxSelectList", function (e) {
		let id = $(this).attr("for");
		////console.log('boxSelectList');
		$(".boxSelectList[for='" + id + "']").removeClass("on");
	});

	$("body").on("click", ".btnListSelect", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let id = $(this).parent().attr("for");
		let value = $(this).attr("value");
		let select = $("span", this).html();
		////console.log(id)
		////console.log(select)
		$(".inputSelectList[for='" + id + "']").val(select);
		$("#" + id).val(value);
		$(".boxSelectList[for='" + id + "']").removeClass("on");
		$(".boxSelectList[for='" + id + "'] .list .btn").show();
		$(".inputSearch[for='" + id + "']").val("");
	});

	// Selection Items 

	$("body").on("click", ".boxInputContent", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let id = $(this).attr("for");
		$(this).removeClass("error");
		//console.log("boxInputContent:" + id);
		$(".boxSelectList[for='" + id + "']").addClass("on");
		$(".boxSelectList[for='" + id + "'] input[for='" + id + "']").focus();
	});

	$("body").on("click", ".btnInputContent", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let id = $(this).attr("for");
		$(this).removeClass("error");
		//console.log("btnInputContent:" + id);
		$(".boxSelectList[for='" + id + "']").addClass("on");
		$(".boxSelectList[for='" + id + "'] input[for='" + id + "']").focus();
	});

	$("body").on("click", ".btnItemSelect", function (e) {
		//e.preventDefault();
		//e.stopPropagation();

		let id = $(this).parent().attr("for");
		let value = $(this).attr("value");
		let name = $(this).attr("name");
		let select = $("#" + id).val();
		let str = value;
		if (select != "") {
			str = select + "," + value;
		}
		////console.log("btnItemSelect:" , id, value, name);
		$("#" + id).val(str)
		$(".boxSelectList[for='" + id + "']").removeClass("on");
		$(".boxSelectList[for='" + id + "'] .list .btn").show();

		$(".boxInputContent[for='" + id + "']").append(
			renderItemInputContent({
				id,
				value,
				name
			})
		);

		////console.log("btnItemSelect:", $("#"+id).val());

	});

	$("body").on("click", ".btnItemSelectPathUrl", function (e) {
		//e.preventDefault();
		//e.stopPropagation();

		let id = $(this).parent().attr("for");
		let value = $(this).attr("value");
		let name = $(this).attr("name");
		let path = $(this).attr("path");
		let inputPathUrl = $("#" + id).val();
		let text = path + "/";
		//console.log("btnItemSelect:", id, value, name);

		$(".boxSelectList[for='" + id + "']").removeClass("on");
		$(".boxSelectList[for='" + id + "'] .list .btn").show();

		insertAtCaret(id, text)

	});

	$("body").on("click", ".btnRemoveItemSelect", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let id = $(this).attr("for");
		//console.log("btnRemoveItemSelect:", id);
		//aqui quitar el valor del input
		$(this).parent().remove();
	});

	$("body").on("click", `.btnGroupCollapse`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let elem = $(this).attr("for");
		console.log(`.btnGroupCollapse`, elem);
		$(".formCollapse[for='" + elem + "']").toggleClass('active');
		$(this).toggleClass('active');
	});

	// Lits hours

	$("body").on("click", ".listHours .btnListHours", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let id = $(this).attr("for");
		let hourSelect = $(this).html();
		////console.log(hourSelect);

		$(".btnListHours").removeClass("on");
		$(".list").removeClass("on");
		$(".btnListHours[for='" + id + "']").addClass("on");
		$(".list[for='" + id + "']").addClass("on");

		if (hourSelect != "00:00" && hourSelect != "01:00") {
			////console.log ( $(".list[for='" + id+"'] .btnHour[value='"+hourSelect+"']").offset().top);
			const btnHour = $(".list[for='" + id + "'] .btnHour[value='" + hourSelect + "']");
			const x = $(".list[for='" + id + "'] .btnHour[value='" + hourSelect + "']").offset();
			//const scrollElement = document.querySelector(`[id-div="${scrollTarget}"]`);
			$(".list[for='" + id + "'] .btnHour[value='" + hourSelect + "']").scrollTop(x.top);
			$(".list[for='" + id + "'] .btnHour").removeClass("active");
			$(".list[for='" + id + "'] .btnHour[value='" + hourSelect + "']").addClass("active");
			btnHour[0].scrollIntoView({
				block: "center" // or "end"
			});
		}

		$("body").on("click", ".listHours .btnHour", function (e) {
			e.preventDefault();
			e.stopPropagation();
			e.stopImmediatePropagation();
			let id = $(this).parent().attr("for");
			let type = $(this).parent().attr("type");
			let idForm = $(this).parent().attr("item");
			let value = $(this).attr("value");
			////console.log( id, type, value);
			let count = 0;

			if (type == "end") {
				let valueStart = $(".selectHoursStartEnd[item='" + idForm + "'] input[name='inputHourStart']").val();
				////console.log(valueStart, idForm )
				const hStart = parseFloat(valueStart.split(":")[0]);
				const mStart = parseFloat(valueStart.split(":")[1]);
				const hNow = parseFloat(value.split(":")[0]);
				const mNow = parseFloat(value.split(":")[1]);

				////console.log("hnow:",hNow, mNow, "hstart:",hStart, mStart);

				if (hNow < hStart) {
					count++;
				}

				if (hNow == hStart && mNow < mStart) {
					count++;
				}
			}

			if (count == 0) {
				$(".btnListHours[for='" + id + "']").html(value);
				$("#" + id).val(value).trigger("change");
			} else {
				alertResponseMessageError({
					message: "Hay un error de horario. Intentalo denuevo por favor."
				});
			}

			$(".btnListHours").removeClass("on");
			$(".list").removeClass("on");

		});





	});

	$("body").on("mouseleave", ".listHours .list", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		$(".btnListHours").removeClass("on");
		$(".list").removeClass("on");
	});


	$("body").on("click", ".btnEditMedia", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		//console.log('btnEditMedia');
		let item = $(this).attr("item");
		let imgSrc = $(this).attr("data-src");
		let id = $(this).attr("data-id");
		let module = $("#" + id).attr("module");
		let inputId = $("#" + id).attr("input-id");
		let fn = "nav." + module + "." + "formEditMedia";
		//console.log(fn, inputId);
		modalEditMedia({
			item,
			imgSrc,
			fn,
			inputId
		});

	})

	$("body").on("change", ".formControlSelectAndOthers select", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let value = $(this).val();
		let id = $(this).attr("for");
		if (value == "others") {
			$("#" + id).addClass("on");
			$("#" + id).focus();
		} else {
			$("#" + id).removeClass("on");
		}
	});

	//btns iconSelect
	$("body").on("click", ".btnSelectIcon", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let id = $(this).attr("for");
		//console.log('.btnSelectIcon', id);
		loadBoxSelectIcons({
			id
		});

		$("body").on("click", ".boxFormSelectIcon", function (e) {
			e.preventDefault();
			e.stopPropagation();
			e.stopImmediatePropagation();
			//console.log('.boxFormSelectIcon');
			const data = $(this).attr("data");
			const id = $(this).parent().attr("for");
			$("#" + id).val(data).trigger("change");
			$(".formControlIconSelect[for='" + id + "'] .iconSelect").addClass("icon " + data);
			$(".boxSelectIcons").remove();
		})
	})


	$("body").on("keyup", ".formInputPathurl", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let id = $(this).attr("id");
		var input = $(this).val();
		let val = convertUrlPath(input);
		//console.log (`.formInputPathurl`, id, val);
		$(this).removeClass("error");
		$('.formInput[for="' + id + '"]').removeClass("error");
		$('.message[for="' + id + '"]').html('');
		$('.formInput[for="' + id + '"]').val(val);
		$('.formInput[data-for="' + id + '"]').val(val);
	})

	$("body").on("change", `.boxInputFile input[type="file"]`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let id = $(this).attr('id');
		let arc = document.getElementById(id);
		let files = arc.files;
		let numFiles = files.length;

		//console.log(`.boxInputFile input[type="file"]`, data, id, arc);

		uploadFile({
			id,
			files,
			numFiles,
			module: data.module,
			system: data.system,
			validfiles: data.validfiles,
			folder: data.folder,
			mode: data.mode
		}).then(function (response) {
			console.log('uploadFile:', response);
		}).catch(function (error) {
			console.log('Error uploadFile,', error);
		});
	});


	$("body").on("click", `.btnRemoveFileItem`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		console.log(`.btnRemoveFileItem`, data);
		$(".formPreview .item-video[item='" + data.for+"']").remove();
		$(".formPreview .item-imagen[item='" + data.for+"']").remove();
		$(".formUpFile .inner[for='" + data.id + "']").removeClass("off");
		$(".formUpFile input[data-id='" + data.id + "']").html("");

	});

	$("body").on("click",`.btnFormOn`, function(e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let id = $(this).attr("for");
		console.log(`.btnFormOn`, data, id);
		
		
		$(`#${id}`).removeClass("formOff");
		$(`#${id}`).addClass("formOn");
		//limpiar todos los campos del formulario
		cleanFormId(id);
		focusSelector(`#${id} .formInput:first`);
		
	});

	$("body").on("click",`.btnFormCancel`, function(e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		console.log(`.btnFormCancel`, data);
		
		let id = $(this).attr("for");
		
		$(`#${id}`).removeClass("formOn");
		$(`#${id}`).addClass("formOff");

		cleanFormId(id);
		
	});

});


export const removeFileItem = (vars =[]) => {
	console.log('removeFileItem',vars);
	$(".formPreview .item-video").html("");
	$(".formPreview .item-imagen").html("");
	$(".formUpFile .inner").removeClass("off");
}

export const stateBtnSwitch = (vars = []) => {
	//console.log('stateBtnSwitch', vars);
	let id = vars.id;
	let value = vars.value;
	let form = vars.form;
	//change valor input
	$(`#${form} #${id}`).val(value);

	//change state btn
	$(`.btnSwitch[data-form="${form}"][data-for="${id}"]`).attr("value", value);

}

export const uploadFile = async (vars = []) => {
	//console.log('uploadFile', vars);
	const url = _PATH_WEB_NUCLEO + "controllers/apis/v1/upload-file.php";
	let data = JSON.stringify({
		accessToken: accessToken(),
		vars
	});

	let formData = new FormData();
	let files = vars.files;
	let numFiles = vars.numFiles;

	formData.append("data", data);

	for (let i = 0; i < numFiles; i++) {
		formData.append("file-" + i, files[i]);
	}

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: formData
		})
		//console.log(res.data);
		return res.data
	} catch (error) {
		console.log("Error: uploadFile")
		console.log(error)
	}
}

export function insertAtCaret(areaId, text) {
	var txtarea = document.getElementById(areaId);
	var scrollPos = txtarea.scrollTop;
	var strPos = 0;
	var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
		"ff" : (document.selection ? "ie" : false));
	if (br == "ie") {
		txtarea.focus();
		var range = document.selection.createRange();
		range.moveStart('character', -txtarea.value.length);
		strPos = range.text.length;
	} else if (br == "ff") strPos = txtarea.selectionStart;

	var front = (txtarea.value).substring(0, strPos);
	var back = (txtarea.value).substring(strPos, txtarea.value.length);
	txtarea.value = front + text + back;
	strPos = strPos + text.length;
	if (br == "ie") {
		txtarea.focus();
		var range = document.selection.createRange();
		range.moveStart('character', -txtarea.value.length);
		range.moveStart('character', strPos);
		range.moveEnd('character', 0);
		range.select();
	} else if (br == "ff") {
		txtarea.selectionStart = strPos;
		txtarea.selectionEnd = strPos;
		txtarea.focus();
	}
	txtarea.scrollTop = scrollPos;
}

export const moveCursorToEnd = (id) => {
	//console.log('moveCursorToEnd', id);
	var el = document.getElementById(id);
	el.focus()
	if (typeof el.selectionStart == "number") {
		el.selectionStart = el.selectionEnd = el.value.length;
	} else if (typeof el.createTextRange != "undefined") {
		var range = el.createTextRange();
		range.collapse(false);
		range.select();
	}

}

export const selectTextInput = (vars = []) => {
	let id = vars['id'];
	var el = document.getElementById(id);

	////console.log('selectTextInput', id , el);

	let start = vars['start'] ? vars['start'] : 0;
	let endEl = el.value.length;
	let end = vars['end'] ? vars['end'] : endEl;
	////console.log("selectTextInput", el, el.value.length );
	el.setSelectionRange(start, end)
}

export const optionsForm = (vars = []) => {
	//console.log('optionsForm', vars);
	let options = vars.options;
	let initialLabel = vars.initialLabel ? vars.initialLabel : "Seleccione una opción";
	let idSelected = vars.idSelected ? vars.idSelected : 0;
	let des = vars.des ? vars.des : 0;
	let html = "";
	if (initialLabel == 0) {
		html = `<option value="0">${initialLabel}</option>`;
	}
	for (let i = 0; i < options.length; i++) {
		let item = options[i];
		let selected = "";
		let aux = "";
		if (item.id == idSelected) {
			selected = "selected";
		}
		if (des == 1) {
			if (empty(item.description)) {
				aux = `data-description="${item.description}"`;
			}
		}

		html += `<option value="${item.id}" ${aux} ${selected}>${item.name}</option>`;
	}
	return html;
}

export const loadBoxSelectIcons = (vars = []) => {
	//console.log('loadBoxSelectIcons', vars);
	let id = vars.id;
	filesIcon().then((response) => {
		////console.log('filesIcon', response);
		let html = '<div class="boxSelectIcons" for="' + id + '">';
		let array = response;
		for (let i = 0; i < array.length; i++) {
			const elem = array[i];
			html += `<a class="boxFormSelectIcon" data="icon ${elem.name}">
				<i class="icon ${elem.name}"></i> <span>${elem.name}</span>
			</a>`;
		}
		html += '</div>';
		$(".formControlIconSelect[for='" + id + "']").prepend(html);
		////console.log(html);
	}).catch(console.warn());
	//$("#" +  id).add
}

export const filesIcon = async (vars = []) => {
	//console.log('filesIcon', vars);
	const url = _ICONS_LINK_FILES;
	let data = JSON.stringify({
		accessToken: accessToken(),
		action: "filesIcon",
		vars: JSON.stringify(vars)
	});

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: data
		})
		////console.log(res.data);
		return res.data
	} catch (error) {
		//console.log("Error: filesIcon")
		//console.log(error)
	}
}

export const modalEditMedia = (vars = []) => {
	//console.log('modalEditMedia', vars);
	let item = vars.item;
	let imgSrc = vars.imgSrc;
	let fn = vars.fn;
	let inputId = vars.inputId;

	dataFile(vars.item).then((response) => {
		//console.log('dataFile', response);
		response.data["id"] = "modalFormEditMedia";


		let body = renderEditMedia(response.data);

		$("#root").prepend(renderModal({
			id: "modalEditMedia",
			cls: "modalExtendedMedia",
			item: vars.item,
			body
		}));
		//console.log('eval fn', fn, inputId);

		if (inputId != "") {

			eval(fn + `({item:${item},inputId:${inputId}})`);
		}

	}).catch(console.warn());

}

export const dataFile = async (item = []) => {
	//console.log('dataFile', item);
	const url = _PATH_WEB_NUCLEO + "controllers/apis/v1/files.php";
	const data = JSON.stringify({
		action: "dataFile",
		accessToken: accessToken(),
		vars: item,
	});

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			headers: {},
			url,
			data
		})
		////console.log(res.data);
		return res.data
	} catch (error) {
		//console.log("Error: dataFile")
		//console.log(error)
	}
}

export function addValueField(vars = []) {
	if (vars.type == "hidden") {
		$("#" + vars.id).val(vars.value);
	}
	if (vars.type == "text") {
		$("#" + vars.id).val(vars.value);
	}
	if (vars.type == "textarea") {
		$("#" + vars.id).html(vars.content);
	}
}

export function errorInput(id, str = null) {
	$("#" + id).addClass("error");
	if (str != null) {
		$("#" + id).val("");
		$("#" + id).attr("placeholder", str);
	}

	$("label[for='" + id + "']").addClass("on");

	$("body").on("keyup", "#" + id, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		$("#" + id).removeClass("error");
	});
}

export function errorInputSelector(selector, str = null) {
	$(selector).addClass("error");
	if (str != null) {
		$(selector).val("");
		$(selector).attr("placeholder", str);
	}

	//$("label[for='" + selector + "']").addClass("on");

	$("body").on("keyup", selector, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		$(selector).removeClass("error");
	});
}

export function errorFormInput(vars = null) {
	let id = vars.id;
	let message = vars.message;
	let messageId = vars.messageId;

	$("#" + id).addClass("error");
	if (message != null && messageId == "") {
		$("#" + id).val("");
		$("#" + id).attr("placeholder", message);
	}

	if (messageId != "") {
		$("#" + messageId).html(message);
	}

	$("label[for='" + id + "']").addClass("on");

	$("body").on("keyup", "#" + id, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		$("#" + id).removeClass("error");
	});
}

export function focusInput(id) {
	//console.log(id);
	$("#" + id).focus();
}

export function editorText(id) {
	$("#" + id).trumbowyg({
		lang: "es",

		autogrow: true,
		btns: [
			['viewHTML'],
			['formatting'],
			['strong', 'em', 'del'],
			['superscript', 'subscript'],
			['link'],
			['insertImage'],
			['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
			['horizontalRule'],
			['unorderedList', 'orderedList'],
			['removeformat'],
			['table'],
			['fullscreen']
		],
	});
}

export function saveFileFn(vars) {
	//console.log("saveFileFn", vars);
	let id = vars.id;
	let folder = vars.folder;
	let validfiles = vars.validfiles;
	let mode = vars.mode;
	let files = vars.files;
	let module = vars.module;
	let system = vars.system;
	let cls = vars.cls ? vars.cls : "";
	let returnId = vars.returnId ? vars.returnId : "";
	let inputName = vars.inputName ? vars.inputName : "";
	let last = vars.last ? vars.last : "";
	let numFiles = files.length;
	let url = _PATH_WEB_NUCLEO + "controllers/apis/v1/upFile.php";

	let formData = new FormData();
	////console.log("cantidad archivos:" + numFiles)
	formData.append("numFile", numFiles);
	formData.append("id", id);
	formData.append("validfiles", validfiles);
	formData.append("folder", folder);
	formData.append("page", _PATH_PAGE);
	formData.append("last", last);

	for (let i = 0; i < numFiles; i++) {
		formData.append("file-" + i, files[i]);
	}

	$.ajax({
		url: url,
		type: "POST",
		data: formData,
		contentType: false,
		processData: false,
		xhr: function () {
			let xhr = $.ajaxSettings.xhr();
			xhr.upload.onprogress = function (e) {
				let dat = Math.floor((e.loaded / e.total) * 100);
				////console.log(Math.floor(e.loaded / e.total *100) + '%');
				for (let i = 0; i < numFiles; i++) {
					// $(".boxIcon[for='" + id + "']").html(dat + "%");
					if (dat != 100) {
						$(
							".formChargeMedia[for='" +
							id +
							"'] .boxInputFile .boxInner"
						).html(dat + "%");
						$(
							".formChargeImage[for='" +
							id +
							"'] .boxInputFile .boxInner"
						).html(dat + "%");
					} else {
						$(
							".formChargeMedia[for='" +
							id +
							"'] .boxInputFile .boxInner"
						).html("");
						$(
							".formChargeImage[for='" +
							id +
							"'] .boxInputFile .boxInner"
						).html("");
					}
				}
			};
			return xhr;
		},
		success: function (data) {
			//console.log("data", data, JSON.parse(data));
			let dataTrim = data.trim();
			let dat = dataTrim.split(",");

			let arrayData = JSON.parse(data);
			//let Object.keys(arrayData[0])
			if (arrayData != null && arrayData.resp != false) {
				for (let x = 0; x < arrayData.length; x++) {
					xd = xd + 1;
					const key = arrayData[x].type;
					if (key == "error") {
						let action = arrayData[x].action;
						let vars = arrayData[x].vars;
						console.log("action", action, vars);
						let varsArray = vars.split(",");

						if (action == "exist-file") {
							let nameFile = varsArray[0];
							let imgFile = _PATH_FILES + varsArray[2];
							let returnaction = action;
							let type = varsArray[3];
							let item = varsArray[1];
							$(
								".boxModule[module='" +
								module +
								"'][system='" +
								system +
								"']"
							).prepend(
								renderModalRenameFile({
									text: "Se removera el documento cargado. Esta seguro de esta acción?",
									icon: "icon icon-alert-info iconInfo",
									item: item,
									classBtnAction: "btnPrimary btnConfirmRenameFile bgInfo",
									labelBtnAction: "Guardar",
									vars: nameFile +
										"," +
										returnaction +
										"," +
										id,
								})
							);



							$("body").on(
								"click",
								".btnConfirmRenameFile",
								function (e) {
									e.preventDefault();
									e.stopPropagation();
									let id = $(this).attr("for");
									//$("#" + id).remove()
									//console.log()
									loadItemMedia({
										img: imgFile,
										index: xd,
										id: id,
										name: nameFile,
										type: type,
									});
									loadInitChargeMedia(id);

									$("#" + id).remove();
								}
							);

							$("body").on(
								"click",
								".btnCancelModalRenameFile",
								function (e) {
									e.preventDefault();
									e.stopPropagation();
									let id = $(this).attr("for");
									let item = $(this).attr("item");
									removeFileId(item);
									$("#" + id).remove();
									if ($(".boxInputs").html() == "") {
										//$("." + returnitem).html(eval("render" + returnitem + "()"));
										loadChargMediaInit(id);
									}
								}
							);
						}
					}
					if (key == "return") {
						const elem = arrayData[x].return;
						const arrayReturn = arrayData[x].vars;
						const arry = arrayReturn.split(",");
						const name = arry[0];
						const item = arry[1];
						const img = _PATH_FILES + arry[2];
						const type = arry[3];

						if (mode == "imgPerfil") {
							$("#" + returnidinput).val(dat[0]);
							$("#" + returnitem)
								.parent()
								.prepend(
									'<a class="btnRemoveImg" item="' +
									dat[0] +
									'" for="' +
									returnidinput +
									'"><i class="icon icon-circle-close"></i></a>'
								);
							$("#" + returnitem).html(
								'<img src="' + _PATH_FILES + dat[1] + '">'
							);

							$("body").on(
								"click",
								".btnRemoveImg",
								function (e) {
									e.preventDefault();
									e.stopPropagation();
									var item = $(this).attr("item");
									$(".boxModule").prepend(
										renderModalConfirm({
											text: "Se removera el elemento cargado. Esta seguro de esta acción?",
											icon: "icon icon-alert-warning iconDanger",
											classBtnAction: "btnPrimary btnConfirmRemove bgDanger",
											labelBtnAction: "Aceptar",
											vars: item +
												"," +
												returnaction +
												"," +
												id,
										})
									);
								}
							);
						}
						if (mode == "singleDoc") {
							let item = dat[0];
							let urlItem = dat[1];
							let typeFile = dat[2];
							let nameFile = dat[3];
							$(
								"#" +
								returnidinput +
								"[for='" +
								returnidinput +
								"']"
							).val(dat[0]);
							$(
									"#" +
									returnitem +
									"[for='" +
									returnidinput +
									"']"
								)
								.parent()
								.prepend(
									'<a class="btnRemoveDoc" item="' +
									dat[0] +
									'" for="' +
									returnidinput +
									'"><i class="icon icon-circle-close"></i></a>'
								);
							$(
								"#" +
								returnitem +
								"[for='" +
								returnidinput +
								"']"
							).html(nameFile + "." + typeFile);

							$(".boxIcon[for='" + id + "']")
								.parent()
								.css("padding", "10px 10px");

							$("body").on(
								"click",
								".btnRemoveDoc",
								function (e) {
									e.preventDefault();
									e.stopPropagation();
									let item = $(this).attr("item");
									$(".boxModule").prepend(
										renderModalConfirm({
											text: "Se removera el documento cargado. Esta seguro de esta acción?",
											icon: "icon icon-alert-warning",
											classBtnAction: "btnPrimary btnConfirmRemove",
											labelBtnAction: "Aceptar",
											vars: item +
												"," +
												returnaction +
												"," +
												id,
										})
									);
								}
							);
						}

						if (mode == "multipleFiles") {

							console.log("multipleFiles", id);

							loadItemMedia({
								item: item,
								img: img,
								index: xd,
								id: id,
								name: name,
								type: type,
							});
							loadInitChargeMedia(id);

							/* $(".formChargeMedia[for = '" + id + "'] ").sortable({
								handle: ".handle .ui-state-default",
								placeholder: "portlet-placeholder ui-corner-all",
								update: function (event, ui) {
									console.log("update", event, ui);
									let i = 0;
									$(".formChargeMedia[for = '" +id +"'] .item").each(function () {
										var item = $(this).attr("item");
										i++;
										$(".formChargeMedia[for = '" +id +"'] .boxInputFile .boxInputs input[value='" +item +"']").attr("data-sort", i);
									});
								},
							}); */
							$(".formChargeMedia[for = '" + id + "']").sortable({
							
								activate: function (event, ui) {
									console.log("update", event, ui);
									
								}, change: function (event, ui) {
									console.log("change", event, ui);
								}, sort: function (event, ui) {
									console.log("sort", event, ui);
								}, update: function (event, ui) {
									console.log("update", event, ui);

									//sorteable_item
									$(".formChargeMedia[for = '" +id +"'] .boxInputFile .boxInputs").html("");
									$(".formChargeMedia[for = '" +id +"'] .item").each(function () {
										var item = $(this).attr("item");
										console.log("item", item , id);
										i++;
										$(".formChargeMedia[for = '" +id +"'] .boxInputFile .boxInputs").append(`<input type="text" name="inputFilesMedia[]" id="input-media-${item}" value="${item}">`);
									});
								}
							});

						}
						if (mode == "simpleFile") {
							////console.log("simpleFile");
							loadItemSimple({
								item: item,
								img: img,
								index: xd,
								id: id,
								name: name,
								type: type,
								module : vars.module
							});
							//loadInitChargeSimple(id);
						}
					}
				}
			} else {
				alertMessageError({
					message: response.message
				});
			}

			if (arrayData != "") {
				//console.log("arrayData", arrayData, arrayData.length);
				for (let x = 0; x < arrayData.length; x++) {
					let elem = arrayData[x].return;
					//console.log ("elem", elem, "cls", cls, elem.status);
					if (elem.status == "success") {
						// console.log("sussess", arrayData[x].return.data);
						if (cls == "inputUpFile") {
							console.log("inputUpFile", elem.data, returnId);
							let dataReturn = elem.data;

							if (dataReturn.type == "mp4") {
								/* let video = `
								<div class="item item-video" item="${dataReturn.fileId}">
									<a class="btnRemoveFile" data-for="${dataReturn.fileId}"><i class="icon icon-circle-close"></i></a>
									<video width="100%" height="180" controls>
									<source src="${_PATH_FILES}${dataReturn.file}" type="video/mp4">
										Tu navegador no soporta el tag de video.
									</video>
									<input type="hidden" name="${inputName}"  value="${dataReturn.fileId}">
								</div>
								`; */

								let video = renderItemVideo({
									item: dataReturn.fileId,
									pathFile: _PATH_FILES + dataReturn.file,
									inputName: inputName,
									id
								});
								$("#" + returnId).html(video);
								/* $(".btnRemoveFile[data-for='" + dataReturn.fileId + "']").on("click", function (e) {
									console.log("remove");
									e.preventDefault();
									e.stopPropagation();
									let item = $(this).attr("data-for");
									$(".item-video[item='" + item + "']").remove();
									$(".inner[for='" + id + "']").removeClass("off");
								}); */
								$(".inner[for='" + id + "']").addClass("off");
							}
						}
					}
				}
			}

		},
	});
}

export function saveFile(vars) {
	console.log("saveFile", vars);
	let id = vars.id;
	let folder = vars.folder;
	let validfiles = vars.validfiles;
	let mode = vars.mode;
	let files = vars.files;
	let module = vars.module;
	let system = vars.system;
	let form = vars.form ? vars.form : "";
	let thumb = vars.thumb ? vars.thumb : "";
	let last = vars.last ? vars.last : "";
	let cls = vars.cls ? vars.cls : "";
	let returnId = vars.returnId ? vars.returnId : "";
	let inputName = vars.inputName ? vars.inputName : "";
	let numFiles = files.length;
	let url = _PATH_WEB_NUCLEO + "controllers/apis/v1/up-file.php";

	let formData = new FormData();
	////console.log("cantidad archivos:" + numFiles)
	formData.append("numFile", numFiles);
	formData.append("id", id);
	formData.append("validfiles", validfiles);
	formData.append("folder", folder);
	formData.append("page", _PATH_PAGE);
	formData.append("thumb", thumb);
	formData.append("last", last);

	for (let i = 0; i < numFiles; i++) {
		formData.append("file-" + i, files[i]);
	}

	$.ajax({
		url: url,
		type: "POST",
		data: formData,
		contentType: false,
		processData: false,
		dataType: "json",
		xhr: function () {
			let xhr = $.ajaxSettings.xhr();
			xhr.upload.onprogress = function (e) {
				let dat = Math.floor((e.loaded / e.total) * 100);
				//console.log(Math.floor(e.loaded / e.total *100) + '%');
				$(".inner[for='" + id + "']").addClass("off");


				for (let i = 0; i < numFiles; i++) {
					$("#" + returnId).html("<div class='loader'><div class='dat'></div><div class='barLoader'></div>");
				}

				$("#" + returnId + " .loader .barLoader").html("<div class='bar' style='width:" + dat + "%;'></div>");
				$("#" + returnId + " .loader .dat").html(dat + "%");
			};
			return xhr;
		},
		success: function (data) {
			console.log("data SaveFile -1477", data);

			if (data != "") {
				let arrayData = data;
				//console.log("arrayData", arrayData, arrayData.length);
				for (let x = 0; x < arrayData.length; x++) {
					let elem = arrayData[x].return;
					//console.log ("elem", elem, "cls", cls, elem.status);
					if (elem.status == "success") {
						// console.log("sussess", arrayData[x].return.data);
						if (cls == "inputUpFile") {
							//console.log("inputUpFile", elem.data, returnId);
							let dataReturn = elem.data;

							if (dataReturn.type == "mp4") {

								let video = renderItemVideo({
									item: dataReturn.fileId,
									pathFile: _PATH_FILES + dataReturn.file,
									id
								});
								$("#" + returnId).html(video);
								//$(".inner[for='" + id + "']").addClass("off");
								$(`#${form} input[name='${inputName}']`).val(dataReturn.fileId);
							}

							if (dataReturn.typeFile == "image") {

								

								if (thumb != "no") {

									let img = renderItemImage({
										item: dataReturn.fileId,
										pathFile: _PATH_FILES + dataReturn.thumb,
										inputName: inputName,
										id
									});

									$("#" + returnId).html(img);
									$(`#${form} input[name='${inputName}']`).val(dataReturn.fileId);
								}else{
									$("#" + returnId).html(dataReturn + ".pdf fue subido exitosamente");
									$(`#${form} input[name='${inputName}']`).val(dataReturn.fileId);
								}


							}

							if (dataReturn.typeFile == "svg") {
								let img = renderItemSvg({
									item: dataReturn.fileId,
									pathFile: _PATH_FILES + dataReturn.file,
									inputName: inputName,
									id
								});
								$("#" + returnId).html(img);
								$(`#${form} input[name='${inputName}']`).val(dataReturn.fileId);
							}

							if (dataReturn.typeFile == "pdf") {
								console.log("thumb", thumb);
								if (thumb != "no") {
									let img = renderItemImage({
										item: dataReturn.fileId,
										pathFile: _PATH_FILES + dataReturn.thumb,
										inputName: inputName,
										id
									});
									$("#" + returnId).html(img);
									$(`#${form} input[name='${inputName}']`).val(dataReturn.fileId);
									$(`#${form} #inputTitle`).val(dataReturn.name);
								}else{
									$("#" + returnId).html(dataReturn.name + ".pdf fue subido exitosamente");
									$(`#${form} input[name='${inputName}']`).val(dataReturn.fileId);
								}
							}
						}
					}
				}
			}

		},
	});
}

export function loadItemMedia(vars) {
	////console.log("loadItemMedia", vars);
	let img = vars.img;
	let i = vars.index;
	let id = vars.id;
	let item = vars.item;
	let name = vars.name;
	let type = vars.type;
	let module = vars.module;

	////console.log(id)
	$(".formChargeMedia[for='" + id + "'] .boxInputFile").before(
		renderBoxMedia({
			item: item,
			img: img,
			index: i,
			id: id,
			name: name,
			type: type,
			module
		})
	);
	$(".formChargeMedia[for='" + id + "'] .boxInputs").append(
		'<input type="text" name="' +
		id +
		'[]" id="input-media-' +
		item +
		'" value="' +
		item +
		'" >'
	);
}

export function loadItemSimple(vars) {
	////console.log("loadItemMedia", vars);
	let img = vars.img;
	let i = vars.index;
	let id = vars.id;
	let item = vars.item;
	let name = vars.name;
	let type = vars.type;
	let module = vars.module;
	////console.log(id)

	$(".formChargeImage[for='" + id + "'] .boxInputFile").before(
		renderBoxImage({
			item: item,
			img: img,
			index: i,
			id: id,
			name: name,
			type: type,
			module
		})
	);
	$("input[for='" + id + "'] ").val(item);
	$(".formChargeImage[for='" + id + "'] .boxInputFile").addClass("block hide");
}

export function loadChargeMediaInit(id = null) {
	$(".actions[for='" + id + "']").html("");
	$(".formChargeMedia[for='" + id + "'] .itemMedia").remove();
	$(".formChargeMedia[for='" + id + "'] .boxInputFile").removeClass("block");
	$(".formChargeMedia[for='" + id + "'] .inputFileBtn").addClass("on");
	$(".formChargeMedia[for='" + id + "'] .boxInner").html(
		rendermultipleFiles()
	);
}

export const removalMediaItems = (vars) => {
	////console.log( removalMediaItems',vars);
	const items = vars.items;
	const varsReturn = vars.id;

	items.forEach(element => {

		let numItem = $(".actions[for='" + varsReturn + "'] .numItems").attr("numitems");
		let num = numItem - 1;
		$(".formChargeMedia[for='" + varsReturn + "'] .item[item='" + element + "']").remove();
		$(".actions[for='" + varsReturn + "'] .numItems").attr("numitems", num);
		$(".actions[for='" + varsReturn + "'] .numItems").html(num);
		$("#" + varsReturn).removeClass("on");
		$("#" + varsReturn).prop("value", "");

	});
	let numItems = 0;
	$(".formChargeMedia[for='" + varsReturn + "'] .itemMedia").each(function (e) {
		numItems++;
	});

	if (numItems == 0) {
		loadChargeMediaInit(varsReturn);
	}

	removeModal();
}

export const loadFilesFormLoader = (vars) => {
	//console.log('loadFilesFormLoader ', vars);
	let files = emptyReturn(vars.files, 0);
	let id = emptyReturn(vars.id, 'inputFile');
	if (files != 0) {
		loadInitChargeMedia(vars.id)
		for (let i = 0; i < files.length; i++) {
			const element = files[i];
			let index = i + 1;
			////console.log(element,index);
			loadItemMedia({
				id,
				index: index,
				img: _PATH_FILES + element.pathurl,
				name: element.title,
				item: element.id,
				module : vars.module
			})
		}

		$(`.formChargeMedia[for='${id}']`).sortable({
			connectWith: ".item",
			update: function (event, ui) {
				console.log("update loadFilesFormLoader", event, ui);
				let i = 0;
				$(".formChargeMedia[for = '" +id +"'] .boxInputFile .boxInputs").html("");
				$(".formChargeMedia[for = '" +id +"'] .item").each(function () {
					var item = $(this).attr("item");
					console.log("item", item , id);
					i++;
					$(".formChargeMedia[for = '" +id +"'] .boxInputFile .boxInputs").append(`<input type="text" name="inputFilesMedia[]" id="input-media-${item}" value="${item}">`);
				});
			}
		}).disableSelection();
	}

}

export const loadSingleFileFormLoader = (vars) => {
	//console.log('loadSingleFileFormLoader', vars);
	let file = emptyReturn(vars.file, 0);
	let id = emptyReturn(vars.id, 'inputFile');
	if (file != 0 && file.pathurl != '' && file.pathurl != null) {
		loadItemSimple({
			item: file.id,
			img: file.pathurl,
			id: id,
			name: file.name,
			type: file.type,
			module : vars.module
		});
	}
}

export function loadChargeImageInit(id = null) {
	////console.log("loadChargeImageInit", id);
	$("input[for='" + id + "'] ").val("");
	$(".formChargeImage[for='" + id + "'] .boxInputFile").removeClass("block hide");
	$(".formChargeImage[for='" + id + "'] .itemMedia").remove();
	$(".formChargeImage[for='" + id + "'] .boxInner").html(
		rendermultipleFiles()
	);
}

export function loadInitChargeMedia(id = null) {
	$(".formChargeMedia[for='" + id + "'] .boxInputFile").addClass("block");
	$(".formChargeMedia[for='" + id + "'] .inputFileBtn").removeClass("on");
	$(".formChargeMedia[for='" + id + "'] .boxInner").html(
		rendermultipleFilesMini()
	);
}

export function loadTrashCountMedia(acc, forId, module, item) {
	let numItems = $(".actions[for='" + forId + "'] .boxNumsItems .numItems").attr("numitems");

	//console.log("acc:" + acc + ":" + forId + ":" + numItems);

	if (acc == 1) {
		if (numItems == 0 || numItems == undefined) {
			////console.log("aqui 0");
			$(".actions[for='" + forId + "']").html(renderNumItemsHead({
				for: forId,
				item : item,
				module
			}));
			$(".actions[for='" + forId + "'] .numItems").html("1");
			$(".actions[for='" + forId + "'] .numItems").attr("numitems", "1");
		} else {
			numItems++;
			$(".actions[for='" + forId + "'] .numItems").html(numItems);
			$(".actions[for='" + forId + "'] .numItems").attr("numitems", numItems);
		}
	}
	if (acc == 0) {
		numItems--;
		if (numItems > 0) {
			////console.log("aqui2", numItems, numItems);
			$(".actions[for='" + forId + "'] .numItems").html(numItems);
			$(".actions[for='" + forId + "'] .numItems").attr("numitems", numItems);
		} else {
			$(".actions[for='" + forId + "']").html("");
		}
	}

}

export const removeFileId = async (idFile, fnReturn, varsReturn) => {
	let url = _PATH_WEB_NUCLEO + "controllers/apis/v1/deleteFile.php";
	let access_token = localStorage.getItem("access_token");
	let refresh_token = localStorage.getItem("refresh_token");
	let idEntitie = localStorage.getItem("idEntitie");

	const dataForm = new FormData();
	dataForm.append("access_token", access_token);
	dataForm.append("refresh_token", refresh_token);
	dataForm.append("idEntitie", idEntitie);
	dataForm.append("idFile", idFile);
	dataForm.append("page", _PATH_PAGE);

	//console.log(idFile);

	let req = new Request(url, {
		method: "POST",
		mode: "cors",
		body: dataForm,
	});

	try {
		let contentAwait = await fetch(req);
		let str = await contentAwait.text();
		//console.log(str);
		if (str) {
			removeModal();
			eval(fnReturn + "('" + varsReturn + "')");
		} else {
			alertPage({
				text: "Error. por favor contactarse con soporte. " +
					response.message,
				icon: "icn icon-alert-warning",
				animation_in: "bounceInRight",
				animation_out: "bounceOutRight",
				tipe: "danger",
				time: "3500",
				position: "top-left",
			})
		}
		return str;
	} catch (err) {
		//console.log("loadView Error:" + err);
	}
};

export function sortJSON(data, key, orden) {
	return data.sort(function (a, b) {
		var x = a[key],
			y = b[key];

		if (orden === "asc") {
			return x < y ? -1 : x > y ? 1 : 0;
		}

		if (orden === "desc") {
			return x > y ? -1 : x < y ? 1 : 0;
		}
	});
}

export const listHours = (id, hour = "00:00") => {
	let str = '<div class="listHours">';
	str += ` <a class="btnListHours" for="${id}">${hour}</a>`;
	str += ` <div class="list scrollBar" for="${id}" >`;
	for (let i = 0; i < 24; i++) {
		let min = 0;
		for (let j = 0; j < 12; j++) {
			const h = (i < 10 ? "0" : "") + i;
			const m = (min < 10 ? "0" : "") + min;
			const time = h + ":" + m;
			let aux = "";
			if (hour == time) {
				aux = "active";
			}
			str += `<a class="btnHour ${aux}" value="${h}:${m}">${h}:${m}</a>`;
			min = min + 5;
		}
	}

	str += "    </div>";
	str += `    <input type="hidden" name="${id}" id="${id}" value="${hour}" />`;
	str += "</div>";
	return str;
	////console.log(str)
};

//validations
export const validateEmail = (email) => {
	let rtn = String(email)
		.toLowerCase()
		.match(
			/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
		);

	if (rtn == null) {
		return false;
	} else {
		return true;
	}
};

export const validateNum = (str) => {
	/* if (typeof str == "string") return false; // we only process strings!
	return (
		!isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
		!isNaN(parseFloat(str))
	); // ...and ensure strings of whitespace fail */

	if (/^[0-9]+$/.test(str)) {
		return true;
	} else {
		return false;
	}
};

export function validateUrl(str) {
	var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
		'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
		'((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
		'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
		'(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
		'(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
	return !!pattern.test(str);
}

export function validatePathUrlNode(str) {
	var pattern = new RegExp('^([-a-z\\d%_.~+&?0123456789\\/]*)' +
		'([;&a-z\\d%_.~+=-]*)' + // query string
		'(#[-a-z\\d_]*)?$'); // fragment locator
	return !!pattern.test(str);
}

export const validateDate = (testDate) => {
	////console.log(testDate);
	var date_regex = /^\d{1,2}-\d{1,2}-\d{2,4}\s\d{1,2}:\d{1,2}$/;
	var date_regex_inv = /^\d{2,4}-\d{1,2}-\d{1,2}\s\d{1,2}:\d{1,2}$/;
	var date_reg = /^\d{2,4}-\d{1,2}-\d{1,2}$/;
	var date_reg_inv = /^\d{1,2}-\d{1,2}-\d{2,4}$/;
	////console.log(date_regex.test(testDate));
	if (
		date_regex.test(testDate) ||
		date_regex_inv.test(testDate) ||
		date_reg.test(testDate) ||
		date_reg_inv.test(testDate)
	) {
		return 1;
	} else {
		return 0;
	}
};

export const addMessageToForm = (vars = []) => {
	//console.log('renderMessage',vars);
	$(vars.selector).html(`<div class="message ${vars.cls}">${vars.message}</div>`);
	if (vars.timer > 0 && vars.timer != undefined) {
		setTimeout(() => {
			$(vars.selector).html("");
		}, vars.timer);
	}
}

export const listElemSelectables = (vars = []) => {
	////console.log('listElemSelectables', vars);
	let str = "";
	let dataLength = 0;
	let btn = vars.btn ? vars.btn : "btnListSelect";

	if (vars.data != 0) {
		let dataList = sortJSON(vars.data, "id", "asc");
		dataLength = vars.data.length;

		for (let i = 0; i < dataLength; i++) {
			const element = dataList[i];
			const id = element.id;
			const name = element.name;
			const path = element.path;
			const children = element.children;
			str += `<buttom class="btn item-${id} ${btn}" path="${path}"  title="${id}" name="${name}" value="${id}">
                    <span>${name}</span>
                </buttom>`;

			if (children != 0) {
				str += listElemSelectables({
					data: children,
					btn
				});
			}
		}
	}

	return str;
}

export const cleanFormId = (id = "") => {
	$(`.form[for="${id}"] input`).val("");
		$(`.form[for="${id}"] textarea`).val("");
		$(`.form[for="${id}"] select`).val("");
		$(`.form[for="${id}"] select`).trigger("change");
		$(`.form[for="${id}"] .formInput`).removeClass("error");
		$(`.form[for="${id}"] .message`).html("");
}

//reset clear form
export const resetForm = (form) => {
	document.getElementById(form).reset();
	$("#" + form + " input").removeClass("error");
	$("#" + form + " input").removeClass("disabled");
};

export const clearForm = (oForm) => {
	let elements = oForm.elements;

	oForm.reset();

	for (let i = 0; i < elements.length; i++) {
		let field_type = elements[i].type.toLowerCase();

		switch (field_type) {
			case "text":
			case "email":
			case "password":
			case "textarea":
			case "hidden":
				elements[i].value = "";
				break;

			case "radio":
			case "checkbox":
				if (elements[i].checked) {
					elements[i].checked = false;
				}
				break;

			case "select-one":
			case "select-multi":
				elements[i].selectedIndex = -1;
				break;

			default:
				break;
		}
	}
};

export const mountForm = (vars) => {
	////console.log('mountForm',vars);
	const id = vars.id;
	const actions = vars.actions ? vars.actions : "";
	const title = vars.title ? vars.title : "Form";
	const module = vars.module;
	const system = vars.system;

}

export const focusId = (elem) => {
	////console.log('focusId',vars);
	document.getElementById(elem).focus();
}

export const focusSelector = (elem) => {
	////console.log('focusSelector',vars);
	$(elem).focus();
}

export const ckeckboxChecked = (vars = []) => {
	//console.log('ckeckboxChecked', vars);
	for (let i = 0; i < vars.array.length; i++) {
		let elem = vars.array[i];
		$(`input[name='${vars.name}'][value='${elem.id}']`).prop('checked', true);
	}
}