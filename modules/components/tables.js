import {
	emptyReturn,
	accessToken,
	alertPage,
	alertResponseMessageError,
	addHtml,
	alertMessageError,
	capitalize
} from "./functions.js";

import {
	renderColumns,
	renderActions,
	renderMacroTable
} from "./renders/renderTables.js";
import {
	deleteModalItem,
	removeModalId
} from "./modals.js";

import {
	renderModalClean,
	renderModalDelete
} from "./renders/renderModals.js";

import * as router from "./router.js";

//actions
document.addEventListener("DOMContentLoaded", function () {

	$("body").on("click", ".btnDelete[fn='deleteModuleItem']", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		const module = $(this).attr("module");
		const item = $(this).attr("item");
		let vari = $(this).attr("vars");
		let variArray = vari.split(",");
		//console.log("deleteModuleItem", module, item);
		deleteModalItem({
			name: variArray[0],
			vars: item,
			fn: "fnDeleteModuleItem",
			module
		});
	});

	/* $("body").on(
		"click",
		".btnDeleteConfirm[fn='fnDeleteModuleItem']",
		function (e) {
			e.preventDefault();
			e.stopPropagation();
			e.stopImmediatePropagation();
			const item = $(this).attr("vars");
			console.log(".btnDeleteConfirm", item, module);

			deleteItemModule({
				item,
				module
			}).then((response) => {
				console.log(response);
				if (response.Error == 0) {
					$(".modalConfirm").remove();
					removeItemTable(item);
					//advisedIndex()
				} else {
					alertPage({
						text: "Error. por favor contactarse con soporte. " +
							response.message,
						icon: "icn icon-alert-warning",
						animation_in: "bounceInRight",
						animation_out: "bounceOutRight",
						tipe: "danger",
						time: "3500",
						position: "top-left",
					});
				}
			});
		}
	); */

	$("body").on("click", ".btnStateModuleItem", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		const state = $(this).attr("state");
		const item = $(this).attr("item");
		const module = $(this).attr("module");
		const fn = $(this).attr("fn");

		if (fn != "changeStateItem"){
			let func = 'router.' + module + '.' + fn;
			eval (`${func}({item, state, module})`);
		}else{
			changeStateModuleItem({
				item,
				state,
				module
			}).then((response) => {
				console.log(response);
				if (response.Error == 0) {
					if (response.response == 1) {
						$(".btnStateModuleItem[item='" + item + "']").attr(
							"class",
							"btnStateModuleItem state state-1"
						);
						$(".btnStateModuleItem[item='" + item + "']").attr(
							"state",
							"1"
						);
					}
					if (response.response == 0) {
						$(".btnStateModuleItem[item='" + item + "']").attr(
							"class",
							"btnStateModuleItem state state-0"
						);
						$(".btnStateModuleItem[item='" + item + "']").attr(
							"state",
							"0"
						);
					}
				} else {
					alertResponseMessageError({
						message: response.message
					});
				}
			});
		}
	});

	$("body").on("click", ".btnDelete[std='1']", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let id = $(this).attr("item");
		let name = $(this).attr("name");
		let fn = $(this).attr("fn");
		let module = $(this).attr("module");
		console.log('.btnDelete[std=1]', id);

		addHtml({
			selector: `#root`,
			type: 'prepend',
			content: renderModalDelete({
				id: "formDeleteStd",
				text: "¿Estás seguro de eliminar <strong>" + name + "</strong>? <br/> Esta acción no se puede deshacer.",
				classBtnAction: fn,
				module,
				attr: `data-id="${id}"`
			})
		}) //type: html, append, prepend, before, after

	});

});

export const downloadXLSTable = (vars = []) => {
	const name = vars.name;
	const table = vars.tableId;

	var uri = 'data:application/vnd.ms-excel;base64,',
		template = `<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>`,
		base64 = function (s) {
			return window.btoa(unescape(encodeURIComponent(s)))
		},
		format = function (s, c) {
			return s.replace(/{(\w+)}/g, function (m, p) {
				return c[p];
			})
		}
	var toExcel = document.getElementById(table).innerHTML;
	var ctx = {
		worksheet: name || '',
		table: toExcel
	};
	var link = document.createElement("a");
	link.download = name + ".xls";
	link.href = uri + base64(format(template, ctx))
	link.click();
}

//functions
export const changeStateModuleItem = async (vars) => {
	//console.log("changeStateModuleItem");
	const url = _PATH_WEB_NUCLEO + "controllers/apis/v1/modules.php";
	const dataForm = new FormData();
	dataForm.append("accessToken", JSON.stringify(accessToken()));
	dataForm.append("action", "changeStateModuleItem");
	dataForm.append("vars", JSON.stringify(vars));

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: dataForm,
		});
		//console.log(res.data);
		return res.data;
	} catch (error) {
		console.log("Error: changeStateModuleItem");
		console.log(error);
	}
};

export const deleteItemModule = async (vars) => {
	console.log("deleteItemModule", vars);
	const url = _PATH_WEB_NUCLEO + "controllers/apis/v1/modules.php";
	const dataForm = new FormData();
	dataForm.append("accessToken", JSON.stringify(accessToken()));
	dataForm.append("action", "deleteItemModule");
	dataForm.append("vars", JSON.stringify(vars));

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: dataForm,
		});
		//console.log(res.data);
		return res.data;
	} catch (error) {
		console.log("Error deleteItemModule");
		console.log(error);
	}
};

export function removeItemTable(item, tableId = "") {
	console.log("removeItemTable", item, tableId);
	if (tableId == "") {
		$(".rowItem[item='" + item + "']").addClass("animated bounceOutRight");
		setTimeout(() => {
			$(".rowItem[item='" + item + "']").remove();
		}, 700);
	} else {
		$(`#${tableId} .rowItem[item='${item}']`).addClass("animated bounceOutRight");
		setTimeout(() => {
			$(`#${tableId} .rowItem[item='${item}']`).remove();
		}, 700);
	}

}

export function activeBtnStateItem(item) {
	$(".btnStateItem[item='" + item + "']").attr(
		"class",
		"btnStateItem state state-1"
	);
	$(".btnStateItem[item='" + item + "']").attr("state", "1");
}

export function inactiveBtnStateItem(item) {
	$(".btnStateItem[item='" + item + "']").attr(
		"class",
		"btnStateItem state state-0"
	);
	$(".btnStateItem[item='" + item + "']").attr("state", "0");
}

export function dataTable(params) {
	//console.log ("dataTable", params)
	var elem = params.elem;
	var pageLength = emptyReturn(params.pageLength, 25);
	var orderCol = emptyReturn(params.orderCol, 0);
	var typeOrder = emptyReturn(params.typeOrder, "ASC");

	// console.log(params)
	// console.log(params.orderCol)
	// console.log(emptyReturn(params.orderCol, 0))
	// console.log(orderCol)
	//console.log(params.pageLength)
	var isDrawCallbackExecuted = false;
    var table = $(elem).DataTable({
        dom: '<"top"fl<"topActions">p<"clear">>rt<"bottom"ip<"clear">>',
        language: {
            sProcessing: "Procesando...",
            sLengthMenu: "<span class='view'>Mostrar</span> _MENU_ <span class='reg'>reg.</span>",
            sZeroRecords: "No se encontraron resultados",
            sEmptyTable: "Ningún dato disponible en esta tabla",
            sInfo: "<span class='m1'>Mostrando registros del</span> _START_ al _END_, <span class='m2'>de un total de</span> _TOTAL_ registros",
            sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
            sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
            sInfoPostFix: "",
            sSearch: "<i class='icon icon-search'></i>",
            sUrl: "",
            sInfoThousands: ",",
            sLoadingRecords: "Cargando...",
            oPaginate: {
                sFirst: "Primero",
                sLast: "Último",
                sNext: "<i class='icon icon-chevron-right'></i>",
                sPrevious: "<i class='icon icon-chevron-left'></i>",
            },
            oAria: {
                sSortAscending: ": Activar para ordenar la columna de manera ascendente",
                sSortDescending: ": Activar para ordenar la columna de manera descendente",
            },
        },
        "stripeClasses": [],
        pagingType: $(window).width() < 1300 ? "simple" : "simple_numbers",
        lengthMenu: [10, 25, 50, 75, 100, "All"],
        bSortable: true,
        pageLength: pageLength,
        order: [
            [orderCol, typeOrder]
        ],
		drawCallback: function (settings) {
			if (!isDrawCallbackExecuted) {
				var api = this.api();
				var rowsVisible = api.rows({ page: 'current' }).nodes().length;

				//console.log("rowsVisible", rowsVisible);

				if (rowsVisible === 1) {
					noResultsFound(settings);
					isDrawCallbackExecuted = true;
				}
			}
        },
        initComplete: function(settings) {
            //console.log("initComplete", settings);
            // Agrega un ID al campo de búsqueda
            var searchBox = $('div.dataTables_filter input');
			searchBox.attr('id', 'customSearchBoxId');
			
			$('#customSearchBoxId').on('input', function() {
                isDrawCallbackExecuted = false; // Restablece la bandera cuando se escribe en el campo de búsqueda
            });
        }
    });

    function noResultsFound(settings) {
        //console.log("No se encontraron resultados", settings, settings.sTableId, settings.oPreviousSearch.sSearch,params.module);
		if (params.module != "" && params.module != undefined) {
			let nav = `router.${params.module}.search${capitalize(settings.sTableId)}({search:${JSON.stringify(settings.oPreviousSearch.sSearch)}})`;
			eval(nav);
		}
        
        return false;
    }
	
	$(elem + "_wrapper .topActions").html(renderActions({
		form: elem,
		cls: "disabled"
	}));
	/* $(elem + " .check").html(
		'<input type="checkbox" class="btnCheckboxTable" for="' + elem + '" />'
	); */

	//applications
	$("body").on("click", ".btnCheckboxTable", function (e) {
		let form = $(this).attr("for");
		let check = $(this).prop("checked");
		console.log(form)
		if (check) {
			$(form + " input[name='inputCheck[]']").prop("checked", true);
			$(".selectActionsTable[for='" + elem + "']").removeClass("disabled");
		} else {
			$(form + " input[name='inputCheck[]']").prop("checked", false);
			$(".selectActionsTable[for='" + elem + "']").addClass("disabled");
		}
		console.log("check", check)

	});

	$("body").on("change", elem + "  input[name='inputCheck[]']", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		//console.log("input[name='inputCheck[]']");
		//let check = $(this).prop("checked");

		let check = [];

		$(elem + " input[name='inputCheck[]']").each(function (i) {
			let auxCheck = $(this).prop("checked");
			let auxValue = $(this).val();
			if (auxCheck) {
				check[i] = auxValue;
			}
		});
		//console.log(check, check.length);
		if (check.length > 0) {
			$(".selectActionsTable[for='" + elem + "']").removeClass("disabled");
		} else {
			$(".selectActionsTable[for='" + elem + "']").addClass("disabled");
		}
	})




}

export function createTable(vars) {
	//console.log("createTable", vars);
	var id = vars.id;
	var clss = vars.class;
	var body = vars.body;
	var thead = vars.thead.split(",");

	var str = "";
	str += '<div class="table-responsive" data-id="' + id + '">';
	str += '<div class="table-wrapper-inner"></div>';
	str +=
		'<table class="table table-hover display ' +
		clss +
		'" id="' +
		id +
		'">';
	str += "<thead>";
	str += "<tr>";
	for (let i = 0; i < thead.length; i++) {
		const elem = thead[i].split(":");
		str += '<th class = "' + elem[1] + '" >' + elem[0] + "</th>";
	}
	str += "</tr>";
	str += "</thead>";
	str += "<tbody>";
	str += body;
	str += "</tbody>";
	str += "</table>";
	str += "</div>";

	return str;
}

export function categorysListTable(list) {
	let listCategorys = [];

	for (let j = 0; j < list.length; j++) {
		const cat = list[j];
		listCategorys[j] =
			'<a class="btnCat" item="' + cat.id + '" >' + cat.name + " </a>";
	}

	return listCategorys.join(",");
}

export const mountTable = (vars) => {
	//console.log("mountTable", vars);
	const tableId = vars.id;
	const data = emptyReturn(
		vars.rows,
		"<td></td><td></td><td></td><td></td><td></td>"
	);
	const container = vars.container;
	const module = vars.module;
	const system = vars.system;
	const orderCol = emptyReturn(vars.orderCol, 1);
	let thead = "";

	let columnsInit = [
		...colCheck,
		...colId,
		...colTitle,
		...colState,
		...colActionsBase,
	];

	//console.log(columnsInit);

	let columns = "";
	if (vars.columns.length != 0) {
		columns = vars.columns;
	} else {
		columns = columnsInit;
	}
	//columns = columnsInit;

	let strTable = "";
	let tbody = "";
	let arrayCols = [];

	for (let i = 0; i < columns.length; i++) {
		const col = columns[i];
		let aux = ",";
		if (i == columns.length - 1) {
			aux = "";
		}
		thead += col.label + ":" + col.cls + aux;
	}

	//console.log(data, columns, thead);

	/* for (let i = 0; i < data.length; i++) {
		const elem = data[i];
		/* 		elem["table"] = tableId;
		elem["module"] = module;
		elem["system"] = system;
		tbody += renderColumns(elem); */

	strTable = createTable({
		id: tableId,
		thead: thead,
		body: data,
	});

	$(container).html(strTable);

	dataTable({
		elem: "#" + tableId,
		orderCol: orderCol,
		module,
		system
	});
};

export const colCheck = [{
	label: "",
	cls: "check",
}, ];

export const colId = [{
	label: "id",
	cls: "colId",
}, ];

export const colState = [{
	label: "Estado",
	cls: "colState",
}, ];

export const colTitle = [{
	label: "Titulo",
	cls: "colTitle",
}, ];

export const colName = [{
	label: "Nombre",
	cls: "colName",
}, ];

export const colCategorys = [{
	label: "Nombre",
	cls: "colName",
}, ];

export const colActionsBase = [{
	label: "Acciones",
	cls: "colActions",
}, ];


export function renderEmpty(vars = []) {
	let message = vars.message ? vars.message : "No hay registros para mostrar.";
	let cls = vars.cls ? vars.cls : "messageCenter";
	return `
        <div class="inner">
            <div class="message ${cls} " lang="es">
                ${message}
            </div>
        </div>
    `;
}

export function setupTable(vars = []) {
	//console.log("setupTable", vars);

	let cols = vars.cols;
	let tableId = vars.tableId;
	let attrBtnDelete = vars.attrBtnDelete ?? "";
	let attrBtnEdit = vars.attrBtnEdit ?? "";

	//console.log(cols);

	let thead = cols.map(function (obj, index) {
		let cls = "col" + capitalize(obj.elem);
		let label = obj.label;
		if (obj.elem == "check") {
			cls = "check";
			label = `<input type="checkbox" class="btnCheckboxTable" for="#${tableId}">`;
		}
		return `<th class="col ${cls}">${label}</th>`;
	});

	//console.log("thead", thead);

	let tbody = vars.data.map(function (obj) {

		let check = `<input name="inputCheck[]" type="checkbox" value="${obj.id}" />`;
		let col = cols.map(function (objCol) {
			let elem = objCol.elem;
			let cont = obj[elem];

			//console.log("elem", elem, cont);

			if (elem == "check") {
				return `<td class="col colCheck">${check}</td>`;
			}

			if (elem == "type") {
				return `<td class="col colId colType">${obj.type.data.name}</td>`;
			}
			if (elem == "state") {
				cont = `<a class="btnState state" 
							data-id="${obj.id}" 
							data-for = "${tableId}"
							state = "${obj.state}" >
								<i class="icon icon-eye-close"></i>
								<i class="icon icon-eye-open"></i>
						</a>`
			}

			if (elem == "actions") {
				//console.log("action", objCol.btns, obj.type);
				let btnsArray = objCol.btns;

				let dataType = obj.type ? obj.type.id : "";
				cont = btnsArray.map(function (objBtn) {
					if (objBtn == "btnEdit") {
						return `<a class="btnEdit" data-for="${tableId}" data-type="${dataType}" data-id="${obj.id}" ${attrBtnEdit} >
									<i class="icon icon-pencil"></i>
						</a>`;
					}

					if (objBtn == "btnDelete") {
						return `<a class="btnDelete" data-name="${obj.name}" data-fn="delete${capitalize(tableId)}" ${attrBtnDelete}  data-id="${obj.id}" >
									<i class="icon icon-trash"></i>
						</a>`;
					}

					if (objBtn == "btnDeleteFn") {
						return `<a class="btnDeleteFn" data-name="${obj.name}" data-fn="delete${capitalize(tableId)}" ${attrBtnDelete}  data-id="${obj.id}" >
									<i class="icon icon-trash"></i>
						</a>`;
					}
				});
				cont = `<div class="btns">${cont.join("")}</div>`;

			}
			return `<td class="col col${capitalize(elem)}">${cont}</td>`;
		});

		//console.log("cols", cols.join(""));

		return `<tr class="row rowItem" item="${obj.id}">${col.join("")}</tr>`;
	});

	//console.log("tbody", tbody);

	return `<div class="table-responsive"><table class="table table-hover display" id="${vars.tableId}">
		<thead>${thead.join("")}</thead>
		<tbody>${tbody.join("")}</tbody>
	</table></div>`;
}

export const mountMacroTable = (vars = []) => {
	//console.log('mountMacroTable', vars);

	let cols = vars.cols;
	let dataInner = vars.data;
	let module = vars.module;
	let tableId = vars.tableId;
	let fiedsMacroInner = "";
	let tableHead = "";
	let tableElements = "";
	let fn = vars.fn ? vars.fn : "";

	let table = `<table class="macroTableInner macroTableInnerHead">`;
	for (let index = 0; index < cols.length; index++) {
		const obj = cols[index];
		//console.log("colsArray", obj, index);
		let attrName = obj.attrName ? obj.attrName : "";
		let id = obj.id ? obj.id : "";
		let label = obj.label ? obj.label : "";
		label = label.replace("*", '<span class="required">*</span>');

		tableHead += `<th class="col col-${index} ${obj.cls}">${label}</th>`;

		if (obj.type == 'id') {
			fiedsMacroInner += `<td class="col col-${index} ${obj.cls}"></td>`;
		}
		if (obj.type == 'text') {
			fiedsMacroInner += `<td class="col col-${index} ${obj.cls}"> <input type="text" class="tableInput" name="${attrName}" tabindex="${index}" id="${id}" /></td>`;
		}
		if (obj.type == 'bit') {
			fiedsMacroInner += `<td class="col col-${index} ${obj.cls}"> <input type="text" class="tableInput" name="${attrName}" tabindex="${index}" id="${id}" /></td>`;
		}
		if (obj.type == 'int') {
			fiedsMacroInner += `<td class="col col-${index} ${obj.cls}"> <input type="text" class="tableInput" name="${attrName}" tabindex="${index}" id="${id}" /></td>`;
		}
		if (obj.type == 'state') {
			fiedsMacroInner += `<td class="col col-${index} ${obj.cls}"> <input type="checkbox" class="tableInput" name="${attrName}" tabindex="${index}" id="${id}" value="1" checked /></td>`;
		}

		if (obj.type == 'json') {
			fiedsMacroInner += `<td class="col col-${index} ${obj.cls}">
					<div class="boxJson">
						<span></span>
						<a class="btn btnHiperMini btnDefault btnBoxTableJson" for="${id}"><i class="icon icon-point-line"></i></a>
						<textarea hidden name="${attrName}" tabindex="${index}" id="${id}"></textarea>
					</div>
				</td>`;
		}

		if (obj.type == 'textarea') {
			fiedsMacroInner += `<td class="col col-${index} ${obj.cls}">
					<div class="boxTextarea">
						<span></span>
						<a class="btn btnHiperMini btnDefault btnBoxTableTextarea" for="${id}" title="${obj.label}"><i class="icon icon-point-line"></i></a>
						<textarea hidden name="${attrName}" tabindex="${index}" id="${id}"></textarea>
					</div>
				</td>`;
		}		


		if (obj.type == 'select') {

			let options = obj.options.map(function (obj1) {
				//console.log("select", obj1);
				let selected = "";
				if (obj.default == obj1.value) {
					selected = "selected";
				}
				return `<option value="${obj1.value}" ${selected}>${obj1.label}</option>`;
			});
			fiedsMacroInner += `<td class="col col-${index} ${obj.cls}"> <select class="tableInput" name="${attrName}" tabindex="${index}" id="${id}">${options.join("")}</select></td>`;
		}
	}


	/* 	for (let j = 0; j < dataInner.length; j++) {
			let object = dataInner[j];
			let itemId = object.id;
			let w = 0;
			console.log("object", object);
			tableElements += `<tr rowId="${itemId}">`;
			tableElements += `<td class="col colCheck"><input type="checkbox" data-id="${itemId}" class="tableInput" id="rowChecked${itemId}" value="0" /></td>`;
	
			for (const key in object) {
				w++;
				//console.log(`${key}: ${object[key]}`);
				let value = object[key] ? object[key] : "";
				if (key != "state") {
					tableElements += `<td data-id="${itemId}" class="col col${capitalize(key)}">${value}</td>`;
				}
	
				if (key == "state") {
					let ck = value == 1 ? "checked" : "";
					tableElements += `<td class="col col${capitalize(key)}"> 
						<input type="checkbox" class="tableInput" name="${key}" tabindex="${w}" id="row${w}" data-id="${itemId}" value="${value}" ${ck} />
					</td>`;
				}
			}
			tableElements += `</tr>`;
		}; */

	for (let j = 0; j < dataInner.length; j++) {
		let object = dataInner[j];
		let itemId = object.id;
		let num = j + 1;
		console.log("object", object);
		tableElements += `<tr rowId="${itemId}">`;
		tableElements += `<td class="col colCheck"><input type="checkbox" data-module="${module}" data-name="${Object.values(object)[1]}" data-action="checkselect" data-id="${itemId}" data-table="${tableId}" class="tableInput" id="rowChecked${itemId}" value="${itemId}" /></td>`;

		for (let index = 0; index < cols.length; index++) {
			const obj = cols[index];
			// key obj
			const key = Object.keys(obj);
			const render = obj.render;
			let value = object[render] ? object[render] : "";

			if (obj.type == 'id') {
				tableElements += `<td class="col col-${num} ${obj.cls}">${value}</td>`;
			}

			if (obj.type == 'text') {
				tableElements += `<td class="col col-${num} ${obj.cls}">${value}</td>`;
			}

			if (obj.type == 'textarea') {
				tableElements += `<td class="col col-${num} ${obj.cls}">${value}</td>`;
			}

			if (obj.type == 'bit' || obj.type == "int") {
				value = object[render];
				tableElements += `<td class="col col-${num} ${obj.cls}">${value}</td>`;
			}

			if (obj.type == 'select') {
				let option = obj.options ? obj.options : [];
				//console.log("option", option, obj.out);
				/* let options = option.map(function (obj1) {
					//console.log("select", obj1);
					if (value == obj1.value) {
						return `<option value="${obj1.value}" selected>${obj1.label}</option>`;
					}
					return `<option value="${obj.value}">${obj.label}</option>`;
				}); */

				let out = obj.out;
				let outInner = "";
				if (out == "label") {
					const labelOption = option.find(obj1 => obj1.value == value);
					const label = labelOption ? labelOption.label : "";
					const clsOut = labelOption ? labelOption.clsOut : "";
					//console.log("labelOption", labelOption)
					outInner = `<span class="out ${clsOut}">${label}</span>`;
				}

				tableElements += `<td class="col col-${num} ${obj.cls}">${outInner}</td>`;
			}

			if (obj.type == "state") {
				let ck = value == 1 ? `checked="checked"` : "";
				tableElements += `<td class="col col${capitalize(key)}"> 
						<input type="checkbox" class="tableInput" 
							data-name="state" 
							data-table="${tableId}" 
							data-module="${module}"
							data-action = "${obj.action}"
							data-fn = "${fn}"
							tabindex="${index}"  
							id="row${num}" 
							data-id="${itemId}" 
							value="${value}" ${ck} >
					</td>`;
			}

			if (obj.type == "json") {
				let json = value;
				// reducir cadena a 20 caracteres
				let jsonInner = json.length > 18 ? json.substring(0, 18) + "..." : json;

				tableElements += `<td class="col col-${num} ${obj.cls}">
					<div class="boxJson btnBoxJson">
						<span>${jsonInner}</span> 
						<a class="btn btnHiperMini btnDefault"><i class="icon icon-point-line"></i></a>
					</div>
				</td>`;
			}

		}
		tableElements += `</tr>`;
	}


	table += `<thead>
				<th class="btnActionCheckbox">#</th>
				${tableHead}
			</thead>
			<tbody>
				<tr class="rowInputs">
					<td class="col colCheck active"></td>
					${fiedsMacroInner}
					${tableElements}
				</tr>
			</tbody>
	</table>`;

	return renderMacroTable({
		tableId,
		module,
		fn,
		body: table,
		cls: ''
	});
}

export const clearMacroTable = (selector = null) => {
	console.log('clearMacroTable', selector);
	$(`${selector} .rowInputs input`).val("");
}


export function updateCode(text) {
	let result_element = document.querySelector("#highlighting-content");
	// Handle final newlines (see article)
	if (text[text.length - 1] == "\n") {
		text += " ";
	}
	// Update code
	result_element.innerHTML = text.replace(new RegExp("&", "g"), "&amp;").replace(new RegExp("<", "g"), "&lt;"); /* Global RegExp */
	// Syntax Highlight
	//Prism.highlightElement(result_element);
}

export function syncScrollCode(element) {
	/* Scroll result to scroll coords of event - sync with textarea */
	let result_element = document.querySelector("#highlighting");
	// Get and set x and y
	result_element.scrollTop = element.scrollTop;
	result_element.scrollLeft = element.scrollLeft;
}

export function checkTabCode(element, event) {
	//console.log("checkTabCode", event.key);
	let code = element.value;
	if (event.key == "Tab") {
		/* Tab key pressed */
		event.preventDefault(); // stop normal
		let before_tab = code.slice(0, element.selectionStart); // text before tab
		let after_tab = code.slice(element.selectionEnd, element.value.length); // text after tab
		let cursor_pos = element.selectionStart + 1; // where cursor moves after tab - moving forward by 1 char to after tab
		element.value = before_tab + "\t" + after_tab; // add tab char
		// move cursor
		element.selectionStart = cursor_pos;
		element.selectionEnd = cursor_pos;
		//updateCode(element.value); // Update text to include indent
	}
}

export const updateItemMacroTable = (vars = []) => {
	console.log('updateItemMacroTable', vars);


};

document.addEventListener("change", (e) => {
	//console.log("change", e , e.srcElement.classList[0]);
	
	/* if (e.target.matches(".selectActionsTable")) {
		let id = e.target.getAttribute("for");
		//console.log("selectActionsTable", id);

		let action = e.target.value;
		console.log("action", action);
		
		if (action == "delete-selected") {
			deleteSelectedItems(id);
		}
	} */

	if (e.target.matches(".selectActions")){
		//let id = e.target.getAttribute("for");
		let data = e.target.dataset;
		let value = e.target.value;
		//console.log("selectActions", data, value);
		if (value != 0) {
			eval(`router.${data.module}.${value}(data)`);
		}
	}

	/* if (e.srcElement.classList[0] == "tableInput") {
		let value = e.target.value;
		let data = e.target.dataset;
		console.log("tableInput", data, value);
		//updateItemMacroTable({data, value})
		//eval(`router..updateItemMacroTable({${data}, ${value}})`);
	}  */
})

document.addEventListener('DOMContentLoaded', function () {
	$("body").on("click", `.btnActionMobile`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let state = $(this).attr("state");
		console.log(`.btnActionMobile`, data, state);
		$(".btns").removeClass("on");
		$(".btnActionMobile").removeClass("on");

		if (state == 0) {
			$(`.btns[data-id=${data.id}]`).addClass("on");
			$(this).attr("state", 1);
		} else {
			$(`.btns[data-id=${data.id}]`).removeClass("on");
			$(this).attr("state", 0);
		}
	});

	$("body").on("click", `.btnBoxTableJson`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let id = $(this).attr("for");
		console.log(`.btnBoxTableJson`, data);

		addHtml({
			selector: `.ws`,
			type: 'prepend',
			content: renderModalClean({
				id: "modalJson",
				title: "Json",
				body: `<div class="form">
					<textarea placeholder="Enter Code Json" id="editingCode" spellcheck="false"></textarea>
					<!--<pre id="highlighting" aria-hidden="true">
						<code class="language-html" id="highlighting-content"></code>
					</pre>-->
					<div class="formControl formActions min">
						<button class="btn btnCancel" for="modalJson">Cancel</button>
						<button class="btn btnPrimary btnSaveInputJson" >Guardar</button>
					</div>
				</div>`
			})
		}) //type: html, append, prepend, before, after

	});

	$("body").on("click", `.btnBoxTableTextarea`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let id = $(this).attr("for");
		let title = $(this).attr("title");
		let textValue = $("#" + id).val();
		console.log(`.btnBoxTableTextArea`, data);

		addHtml({
			selector: `body`,
			type: 'prepend',
			content: renderModalClean({
				id: "modalTextarea",
				title,
				body: `<div class="form">
					<textarea placeholder="Enter text" id="editingText" spellcheck="false">${textValue}</textarea>
					<div class="formControl formActions min">
						<button class="btn btnCancel" for="modalTextarea">Cancel</button>
						<button class="btn btnPrimary btnSaveTextareaTable" data-for-id="${id}" data-textarea-id="editingText" >Guardar</button>
					</div>
				</div>`
			})
		}) //type: html, append, prepend, before, after

	});

	$("body").on("click",`.btnSaveTextareaTable`, function(e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		console.log(`btnSaveTextareaTable`,data);
		let textValue = $("#"+data.textareaId).val();
		$("#"+data.forId).val(textValue);
		removeModalId("modalTextarea");
		if (textValue != ""){
			$(`.btnBoxTableTextarea[for="${data.forId}"]`).addClass("active");
		}else{
			$(`.btnBoxTableTextarea[for="${data.forId}"]`).removeClass("active");
		}
	});

	/* $("body").on("input", `#editingCode`, function (e) {
		e.preventDefault();
		console.log(`#editingCode`, this.value)
		updateCode(this.value);
		syncScrollCode(this);

	});

	$("#editingCode").on("scroll", function (e) {
		e.preventDefault();
		syncScrollCode(this);
	});*/

	//keydown 
	$("body").on("keydown", "#editingCode", function (e) {
		//e.preventDefault();
		checkTabCode(this, e);
	});
})