import {
    addHtml,
    accessToken,
} from './functions.js';
import { renderFinder } from './renders/renderFinder.js';

export const finder = (vars = []) => {
    //console.log('finder', vars);
    getData({
        task: 'getFiles',
        return: 'returnArray', // returnId, returnState, returnArray
    }).then((response) => {
        console.log('getData getFiles', response);
        if (response.status == 'success') {
            vars["data"] = response.data;
            addHtml({
                selector: 'body',
                type: 'prepend',
                content: renderFinder(vars)
            }) //type: html, append, prepend, before, after
            $(".finder .tabs .tab:first-child").addClass("active");
            $(".finder .tabsContents .tabContent:first-child").addClass("active");
        } else {
            alertMessageError({ message: response.message })
        }
    }).catch(console.warn());
}

export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "controllers/apis/v1/files.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        //console.log("Error: getDataPlaces", error);
    }
}


document.addEventListener("DOMContentLoaded", function () {
    $("body").on("click", `.btnFinder`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`.btnFinder`, data);
        finder(data);
    });

    $("body").on("click", `.btnCloseFinder`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        //let data = $(this).data();
        //console.log(`.btnCloseFinder`, data);
        $(`.finder`).remove();
    });
});