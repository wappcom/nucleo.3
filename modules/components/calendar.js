export function Calendar(Month, Year, fn) {
    var output = '';

    let firstDay = new Date(Year, Month, 1);
    //El primer día de la semana ha de ser Lunes
    let startDay = firstDay.getDay() - 1;
    if ((startDay < 0))
        startDay = 6;
    else

        if (((Year % 4 == 0) && (Year % 100 != 0)) || (Year % 400 == 0))
            days[1] = 29;
        else
            days[1] = 28;

    output += '<div class="monthInner">';
    output += '     <div class="head"><label>' + names[Month] + '</label>';
    output += `     <div class="lineDay">
                        <span>L</span>
                        <span>M</span>
                        <span>M</span>
                        <span>J</span>
                        <span>V</span>
                        <span>S</span>
                        <span>D</span>
                    </div></div>`;
    output += ' <div class="row">';

    var column = 0;
    for (let j = 0; j < startDay; j++) {
        output += "<span class='day'>&nbsp</span>";
        column++;
    }

    for (let i = 1; i <= days[Month]; i++) {

        //Día actual sombreado
        if ((i == thisDay) && (Month == thisMonth) && (Year == thisYear))
            output += "<span class='day today' state='0'  allday='0' fn='" + fn + "' date='" + thisDay + "/" + (thisMonth + 1).toString().padStart(2, "0") + "/" + thisYear + "'>" + i + "</span>";
        else

            //Domingos en rojo
            if ((column == 6))
                output += "<span class='day endWeek' state='0' allday='0' fn='" + fn + "' date='" + i + "/" + (Month + 1).toString().padStart(2, "0") + "/" + Year + "'>" + i + "</span>";
            else

                output += "<span class='day normal' state='0' allday='0' fn='" + fn + "' date='" + i + "/" + (Month + 1).toString().padStart(2, "0") + "/" + Year + "'>" + i + "</span>";
        column++;
        if (column == 7) {
            output += '</div><div class="row">';
            column = 0;
        }
    }
    output += "</div></div>";

    return output;
}

function array(m0, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11) {
    this[0] = m0; this[1] = m1; this[2] = m2; this[3] = m3;
    this[4] = m4; this[5] = m5; this[6] = m6; this[7] = m7;
    this[8] = m8; this[9] = m9; this[10] = m10; this[11] = m11;
}

var names = new array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
var days = new array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);


export function y2k(number) { return (number < 1000) ? number + 1900 : number; }

var today = new Date();
var thisDay = today.getDate();
var thisMonth = today.getMonth();
var thisYear = y2k(today.getYear());


export const calendarYear = (vars) => {

    let year = vars.year ? vars.year : thisDay;
    let fn = vars.fn

    var output = '<div class="calendar calendarYear">';
    output += '       <div class="head" lang="es">Año ' + year + "</div>";
    output += '       <div class="months">';
    output += '             <div class="month">' + Calendar(1 - 1, year, fn) + '</div>';
    output += '             <div class="month">' + Calendar(2 - 1, year, fn) + '</div>';
    output += '             <div class="month">' + Calendar(3 - 1, year, fn) + '</div>';
    output += '             <div class="month">' + Calendar(4 - 1, year, fn) + '</div>';
    output += '             <div class="month">' + Calendar(5 - 1, year, fn) + '</div>';
    output += '             <div class="month">' + Calendar(6 - 1, year, fn) + '</div>';
    output += '             <div class="month">' + Calendar(7 - 1, year, fn) + '</div>';
    output += '             <div class="month">' + Calendar(8 - 1, year, fn) + '</div>';
    output += '             <div class="month">' + Calendar(9 - 1, year, fn) + '</div>';
    output += '             <div class="month">' + Calendar(10 - 1, year, fn) + '</div>';
    output += '             <div class="month">' + Calendar(11 - 1, year, fn) + '</div>';
    output += '             <div class="month">' + Calendar(12 - 1, year, fn) + '</div>';
    output += '         </div>';
    output += '</div>';

    return output;
}

export function checkInRange($date_start, $date_end, $date_now) {
    $date_start = strtotime($date_start);
    $date_end = strtotime($date_end);
    $date_now = strtotime($date_now);
    if (($date_now >= $date_start) && ($date_now <= $date_end))
        return true;
    return false;
}