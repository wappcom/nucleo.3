import {
    replacePath,
    replaceAll,
    loadingBar,
    loadView,
    loadLoading,
    addHtml,
    capitalize,
    loadingBarIdReturn,
    accessToken,
    alertMessageError
} from "./functions.js"

import {
    translate
} from "./lang.js"

import {
    getPageConfig,
    loadConfigAdmin
} from "../dashboard/components/configurations.js"

///import * as settings from pathRouterHost + 'components/settings.js';
//export { settings }
import * as router from "./router.js";
export {
    router
};

import * as forms from "./forms.js";
export {
    forms
}

import * as modals from "./modals.js";
export {
    modals
}

import * as tables from "./tables.js";
export {
    tables
}

import * as lang from "./lang.js";
export {
    lang
}

import * as finder from "./finder.js";
export {
    finder
}

import * as effects from "./effects.js";
export {
    effects
}

import * as renderModals from "./renders/renderModals.js";
export {
    renderModals
}

import * as renderForms from "./renders/renderForms.js";
export {
    renderForms
}

import * as renderTables from "./renders/renderTables.js";
export {
    renderTables
}

import * as renderFinder from "./renders/renderFinder.js";
export {
    renderFinder
}

import * as renderNav from "./renders/renderNav.js";
export {
    renderNav
}

import * as renderDashboard from "./renders/renderDashboard.js";
export {
    renderDashboard
}


document.addEventListener("DOMContentLoaded", function () {
    $(window).resize(function () {
        resizeNav();
        resizeWorkspace()
    })

    state();

    $("body").on("click", ".menu", function (e) {
        e.preventDefault()
        e.stopPropagation()
        //console.log("clickMenu")
        let state = $(this).attr("state")
        //console.log("state:" + state)
        closeProfileMenu()

        if (state == 0) {
            openSidebar()
        } else {
            closeSidebar()
        }
    })

    $("body").on("click", ".btnStateItem", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        const state = $(this).attr("state")
        const item = $(this).attr("item")
        const module = $(this).attr("module")
        const fn = module + "." + $(this).attr("fn")
        eval(fn + "(" + item + "," + state + ")")
    })

    $("body").on("click", ".sidebar .bg", function (e) {
        e.preventDefault()
        e.stopPropagation()
        //console.log("sidebar .bg")

        closeProfileMenu()

        let state = $(".menu").attr("state")
        if (state == 0) {
            openSidebar()
        } else {
            closeSidebar()
        }
    })

    $("body").on("click", ".colapseSidebarMenu", function (e) {
        e.preventDefault()
        e.stopPropagation()
        let system = $(this).attr("for")
        let state = $(this).attr("state");

        //console.log("collapse", cl, state)
        if (state == 0) {
            collapseSidebar(system)
        } else {
            unCollapseSidebar(system)
        }
        /* setTimeout(function () {
            let wBodyModule = $(`.bodyModule[system='${cl}']`).outerWidth();
            //console.log("estado", wBodyModule)
            $(".innerForm").outerWidth(wBodyModule);
        }, 120) */

    })

    $("body").on("click", ".btnItemTab", function (e) {
        e.preventDefault()
        e.stopPropagation()
        //console.log("tab item")
        let path = $(this).attr("path");
        let sysId = $(this).attr("item");
        let modId = 0;

        $(`.btnModule[path="${path}"]`).each(function (index, e) {
            let array = [...e.classList];
            let modItem = e.dataset.id;
            // buscar dentro de una lista 
            let ind = array.indexOf("active");
            if (ind != -1) {
                modId = modItem;
            }
        });

        getData({
            task: 'setActiveTab',
            return: 'returnState', // returnId, returnState, returnArray, returnMessage, returnObject
            inputs: {
                modId,
                sysId
            }
        }).then((response) => {
            //console.log('getData setActiveTab', response);
            if (response.status == 'success') {

            } else {
                alertMessageError({
                    message: response.message
                })
            }
        }).catch(console.warn());

        activeTabWs({
            path,
            sysId
        })
    })

    $("body").on("click", ".boxEntities .item-select", function (e) {
        e.preventDefault()
        e.stopPropagation()
        //console.log(".item-select")
        $(".innerEntities").toggleClass("on")
    })

    $("body").on("click", ".innerEntities .item", function (e) {
        e.preventDefault()
        e.stopPropagation()
        let brand = $(this).attr("brand")
        let item = $(this).attr("item")
        let name = $(this).attr("name")
        $(".boxEntities .item-select img").attr("src", brand)
        $(".boxEntities .item-select span").html(name)
        $(".innerEntities").removeClass("on")
        localStorage.setItem("idEntitie", item)
        // Aqui para función de cambio por entidad
    })

    $("body").on("click", ".numTabMobile", function (e) {
        e.preventDefault()
        e.stopPropagation()
        $(".boxTabMobil").addClass("on")
    })

    $("body").on("click", ".btnCerrarTabMobil", function (e) {
        e.preventDefault()
        e.stopPropagation()
        $(".boxTabMobil").removeClass("on")
    })

    $("body").on("click", ".boxTabMobil .btnItemTab", function (e) {
        e.preventDefault()
        e.stopPropagation()
        //console.log("tab item boxTabMobil")
        let path = $(this).attr("path")
        $(".navTabs .item").attr("active", "0")
        $('.navTabs .item[path="' + path + '"]').attr("active", "1")
        reorderTabMobile()
        $(".boxTabMobil").removeClass("on")
        $(".numTabMobile").removeClass("on")
    })

    //modulos
    $("body").on("click", ".btnModule", function (e) {
        e.preventDefault()
        e.stopPropagation()
        let path = $(this).attr("path")
        let pathurl = $(this).attr("pathurl")
        let system = $(this).attr("system")
        let module = $(this).attr("module")
        let name = $(this).attr("name")
        let color = $(this).attr("color")
        let icon = $(this).attr("icon")
        let data = $(this).data();

        actionBtnModule({
            pathurl,
            path,
            system,
            module,
            name,
            color,
            icon,
            id: data.id,
            sysId: data.sys,
        })
        //console.log("btnMod:" + path + ":" + module)
        /* let funcName = module + "." + module + "Index"

        

        $(".btnModule").removeClass("active")
        $(this).addClass("active")
        // $(".titleMenuTop[system='" + system + "']").html(name)

        $("div[system='" + system + "']").attr("module", module)
        $(
            ".bodyModule[system='" + system + "'][module = '" + module + "']"
        ).html("")
        $(
            '.bodyModule[system="' + system + '"][module = "' + module + '"]'
        ).html(
            '<div class="loadingPageModule"><div class="loadingBar"></div></div>'
        )
        loadingBar()
        eval(funcName + "()")
        resizeWorkspace()
        $(this).addClass("active") */
        resizeWorkspace();
    })

    $("body").on("click", ".btnConfirmRemove", function (e) {
        e.preventDefault()
        e.stopPropagation()
        let vars = $(this).attr("vars")
        let arrays = vars.split(",")
        // //console.log(arrays)
        removeFileId(arrays[0]).then((str) => {
            if (str == 1) {
                $(".modalRender").remove()
                $(".btnRemoveImg").remove()
                $(".btnRemoveDoc").remove()
                let funcName = arrays[1]
                let varId = arrays[2]

                eval(funcName + '("' + varId + '")')
            } else {
                alert("algo Salio Mal...")
                $(".modalRender").remove()
            }
        })
    })

    $("body").on("click", ".btnProfileNav", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        let state = $(this).attr("state")
        closeSidebar()

        if (state == 0) {
            openProfileMenu()
        } else {
            closeProfileMenu()
        }
    })

    $("body").on("click", ".tab", function (e) {
        e.preventDefault()
        e.stopPropagation()
        let item = $(this).attr("item")
        let id = $(this).parent().attr("for")
        //console.log(item + ":" + id)

        //tabs
        $(".tabs[for='" + id + "'] .tab").removeClass("active")
        $(this).addClass("active")

        //contents
        $(".tabsContents[id='" + id + "'] .tabContent").removeClass("active")
        $(".tabsContents[id='" + id + "'] .tabContent[item='" + item + "']").addClass("active")
    })

    $("body").on("click", ".btnCloseSession", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()

        closeProfileMenu()

        logout().then((back) => {
            //console.log(back)
            if (back.Error == 0) {
                localStorage.clear()
                window.location.href = _PATH_LOGOUT
            }
        })
    })

    $("body").on("click", ".btnAddForm", function (e) {
        e.preventDefault()
        e.stopPropagation()
        let system = $(this).attr("system")
        let module = $(this).attr("module")
        let fn = $(this).attr("fn")
        let pathurl = $(this).attr("pathurl")
        let vars = $(this).attr("vars")
        let item = $(this).attr("item")
        let data = JSON.stringify($(this).data());

        $(".boxModule[system='" + system + "'][module='" + module + "']").prepend('<div class="innerForm" pathurl="' + pathurl + '" module="' + module + '" system="' + system + '" fn="' + fn + '"><div class="loadingPageModule"><div class="loadingBar"></div></div></div>')
        $(".bodyModule[system='" + system + "'][module='" + module + "'] .inner").addClass("on")
        $(".bodyModule[system='" + system + "'][module='" + module + "'] .innerForm").addClass("on")
        $(".bodyModule[system='" + system + "'][module='" + module + "']").addClass("backOff")

        let funcName = "router." + module + "." + fn;
        //console.log("🚀 ~ file: nav.js:410 ~ funcName:", funcName + '({system:"' + system + '",module:"' + module + '",fn:"' + fn + '",pathurl:"' + pathurl + '",vars:"' + vars + '",item:"' + item + '", data:"' + data + '"})')

        eval(
            funcName + '({system:"' + system + '",module:"' + module + '",fn:"' + fn + '",pathurl:"' + pathurl + '",vars:"' + vars + '",item:"' + item + '", data:' + data + '})'
        )
        resizeWorkspace()
    })

    $("body").on("click", `.btnActionFn`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`.btnActionFn`,data);
        let funcName = "router." + data.module + "." + data.fn;
        eval(funcName + '(' + JSON.stringify(data) + ')')
        resizeWorkspace();
    });

    $("body").on("click", ".btnItemMenu", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        //console.log('.btnItemMenu[path=users]');
        const idModal = 'modalConfigAdmin';
        const path = $(this).attr('path');
        const system = $(this).attr('system');
        const type = $(this).attr('type');
        const id = $(this).attr('item');
        const modDefault = $(this).attr('moddefault');

        closeProfileMenu();
        closeSidebar();

        if (type == 'admin') {
            addHtml({
                selector: "#root",
                type: 'prepend',
                content: renderModals.renderModal({
                    cls: "modalConfig animated fadeUp",
                    btnClose: 1,
                    body: loadingBarIdReturn(),
                    id: idModal,
                })
            }) //type: html, append, prepend, before, after

            loadConfigAdmin({
                id: idModal,
                path,
            });

        }

        if (type == 'system') {
            let tabSis = $(".btnItemTab[path='" + path + "']");
            let numTabs = $(".btnItemTab").length;
            console.log('tabSis', tabSis.length, numTabs);
            if (tabSis.length == 0) {
                let str = elemTab({
                    item: numTabs++,
                    id: id,
                    path: path,
                    modDefault,
                    classItem: 1,
                    name: $(".btnItemMenu[item=" + id + "] .name").html(),
                    type: "",
                    system,
                    color: "",
                    icon: $(".btnItemMenu[item=" + id + "] .icon").attr("class"),
                });

                $(".tabsNavList").append(str);
                $(".itemTabNav").removeClass("active");
                $(".itemTabNav[path='" + path + "']").attr("active", 1);
                $(".itemTabNav[path='" + path + "']").addClass("active");
                $(".ws").removeClass("active");
                loadWorkspace(id, "active", path, system, 0, modDefault); //id, active, path, system, modPathUrlActive = 0, modIdActive = 0

            }
            if (tabSis.length == 1) {

                $(".itemTabNav").attr("active", 0);
                $(".itemTabNav").removeClass("active");
                $(".itemTabNav[path='" + path + "']").attr("active", 1);
                $(".itemTabNav[path='" + path + "']").addClass("active");

                $(".ws").removeClass("active");
                $(".ws[path='" + path + "']").addClass("active");

            }



            getData({
                task: 'setItemTab',
                return: 'returnState', // returnId, returnState, returnArray, returnMessage, returnObject
                inputs: {
                    sysId: id,
                    modId: modDefault,
                }
            }).then((response) => {
                if (response.status == 'success') {
                    console.log('getData setItemTab', response);
                } else {
                    alertMessageError({
                        message: response.message
                    })
                }
            }).catch(console.warn());

            resizeNav()
        }


    })

    $("body").on("click", ".btnMenuModal", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log("btnMenuModal", data);
        $(`#${data.modalId} .btnMenuModal`).removeClass('active');
        $(`#${data.modalId} .btnMenuModal[data-content='${data.content}']`).addClass('active');
        eval(`router.${data.module}.${data.content}Index(` + JSON.stringify(data) + `)`);
    })

    $("body").on("click", `.btnItemTabClose`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let sysId = $(this).attr('item');
        let path = $(`.itemTabNav[id='item-${sysId}']`).attr('path');
        let modId = 0;
        //console.log(`.btnItemTabClose`, data);


        $(`.btnModule[path="${path}"]`).each(function (index, e) {
            let array = [...e.classList];
            let modItem = e.dataset.id;
            // buscar dentro de una lista 
            let ind = array.indexOf("active");
            if (ind != -1) {
                modId = modItem;
            }
        });

        console.log("modId", modId, "sysId", sysId);

        getData({
            task: 'removeItemTab',
            return: 'returnState', // returnId, returnState, returnArray, returnMessage, returnObject
            inputs: {
                sysId,
                modId
            }
        }).then((response) => {
            console.log('getData removeItemTab', response, sysId);
            if (response.status == 'success') {

                $(`.itemTabNav[id='item-${sysId}']`).remove();
                $(`.ws[path='${path}']`).remove();

                let lastItem = $('.tabsNavList .itemTabNav:last-child').attr('item');
                $('.tabsNavList .itemTabNav:last-child').addClass('active');
                $(`.ws-${lastItem}`).addClass('active');

            } else {
                alertMessageError({
                    message: response.message
                })
            }
        }).catch(console.warn());

        resizeNav();
    });
})

export const activeTabWs = (vars = []) => {
    //console.log('activeTabWs', vars);
    let path = vars.path;
    $(".navTabs .item").removeClass("active")
    $(".workspace .ws").removeClass("active")
    $('.navTabs .item[path="' + path + '"]').addClass("active")
    $(".navTabs .item").attr("active", "0")
    $('.navTabs .item[path="' + path + '"]').attr("active", "1")
    $('.workspace .ws[path="' + path + '"]').addClass("active")
}

export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/dashboard/controllers/apis/v1/dashboard.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        //console.log("Error: getDataPlaces", error);
    }
}

export const collapseSidebar = (system, selector = ".colapseSidebarMenu") => {
    console.log("collapseSidebar")
    $(selector).attr("state", "1")
    if (system != undefined && system != "") {
        $(".sidebarMenuModule[system='" + system + "']").addClass("on")
    } else {
        $(".sidebarMenuModule").addClass("on")
    }
    $(".bodyModule").removeClass("on");
    $(".menuTopHeader").removeClass("on");
    $(selector).removeClass("on");
    $(".innerForm").removeClass('collapse');

}

export const unCollapseSidebar = (system, selector = ".colapseSidebarMenu") => {
    $(selector).attr("state", "0")
    console.log("unCollapseSidebar", system, selector)
    if (system != undefined && system != "") {
        $(".sidebarMenuModule[system='" + system + "']").removeClass("on")
    } else {
        $(".sidebarMenuModule").removeClass("on");
    }
    $(".menuTopHeader").addClass("on")
    $(".bodyModule").addClass("on")
    $(selector).addClass("on");
    $(".innerForm").addClass('collapse');
}

export const btnStateToogle = (btnId) => {
    //console.log('btnStatus',vars);
    const id = $("#" + btnId);
    if (id.attr("state") == 0) {
        id.addClass("active");
        id.attr("state", 1);
    } else {
        id.removeClass("active");
        id.attr("state", 0);
    }
}

export const actionBtnModule = (vars) => {
    //console.log('actionBtnModule', vars);
    let system = vars.system
    let module = vars.module
    let pathurl = vars.pathurl
    let color = vars.color
    let icon = vars.icon
    let name = vars.name
    let sysId = vars.sysId
    let modId = vars.id

    let funcName = "router." + module + "." + module + "Index"

    $(".btnModule[data-sys='" + sysId + "']").removeClass("active");
    $(".innerForm").remove();
    // $(".titleMenuTop[system='" + system + "']").html(name)

    $("div[system='" + system + "']").attr("module", module)
    $(".bodyModule[system='" + system + "'][module = '" + module + "']").html("")
    $('.bodyModule[system="' + system + '"][module = "' + module + '"]').html(
        '<div class="loadingPageModule"><div class="loadingBar"></div></div>'
    )
    $('.bodyModule[system="' + system + '"][module = "' + module + '"]').attr("color", color)
    $('.bodyModule[system="' + system + '"][module = "' + module + '"]').attr("icon", icon)
    $('.bodyModule[system="' + system + '"][module = "' + module + '"]').attr("name", name)

    getData({
        task: 'setModuleUserTab',
        return: 'returnState', // returnId, returnState, returnArray, returnMessage, returnObject
        inputs: {
            sysId,
            modId
        }
    }).then((response) => {
        //console.log('getData setModuleUserTab', response);
        if (response.status == 'success') {

        } else {
            alertMessageError({
                message: response.message
            })
        }
    }).catch(console.warn());

    loadingBar()
    eval(funcName + "()")
    resizeWorkspace()
    $(".btnModule[pathurl='" + pathurl + "']").addClass("active");
    //leer el titulo de la pagina y añadir el nombre del modulo
    let titlePage = document.title;
    let titleModule = name;
    if (titlePage.indexOf(name) == -1) {
        document.title = titleModule + " - " + titlePage;
    }

}

export function loadTabs(menusJson) {

    var menuJson = menusJson
    var menuGeneral = menuJson.menu
    var tabs = menuGeneral.tabs
    //console.log(menuGeneral)
    //console.log(menuGeneral.administrator)
    //console.log("menuJson:" + menusJson)
    //console.log("tabs:", tabs)
    var j = 0
    let str = ""

    //$(".navTabs").html("")

    if (menuGeneral.tabs != 0 && menuGeneral.tabs != undefined) {
        for (let key in tabs) {
            //console.log("tabs[key]", tabs[key])
            let id = tabs[key].id
            let active = tabs[key].active
            let path = $(".btnItemMenu[type=system][item='" + id + "']").attr(
                "path"
            )
            let pathUrl = $(
                ".btnItemMenu[type=system][item='" + id + "']"
            ).attr("system")
            //console.log("id:" + tabs[key].id)
            let modPathUrlActive = tabs[key].modPathUrlActive ? tabs[key].modPathUrlActive : 0;
            let modIdActive = tabs[key].modIdActive ? tabs[key].modIdActive : 0;

            str = elemTab({
                item: j++,
                id: id,
                path: path,
                classItem: active,
                name: $(".btnItemMenu[item=" + id + "] .name").html(),
                type: "",
                system: pathUrl,
                color: "",
                icon: "icon " +
                    $(".btnItemMenu[item=" + id + "] i").attr("class"),
                modDefault: modIdActive,
            })
            loadWorkspace(id, active, path, pathUrl, modPathUrlActive, modIdActive);
            $(".navTabs .tabsNavList").append(str)
        }
    }
    resizeNav()
}

export function loadWorkspace(id, active, path, system, modPathUrlActive = 0, modIdActive = 0) {
    let str = ""
    //console.log("id:" + id + ",active:" + active + "path:" + path, "system:" + system, "modPathUrlActive:" + modPathUrlActive, "modIdActive:" + modIdActive)
    //console.log("system:" + system)
    loadModule(path, modIdActive).then((dataModule) => {
        //console.log(dataModule)
        str += '<div class="ws ws-' + id + "  " + active + '"  system="' + system + '"  path="' + path + '"  id="ws-' + id + '">'
        str += dataModule
        str += "</div>"
        $(".workspace").append(str);

        let funcName = '';
        if (modPathUrlActive == '0') {
            //console.log(system + "." + system + "Index");
            funcName = system + "." + system + "Index"
        } else {
            //console.log(modPathUrlActive + "." + modPathUrlActive + "Index");
            funcName = "router." + modPathUrlActive + "." + modPathUrlActive + "Index"
            //$(".btnModule[module='" + system + "']").remove("active");
            //$(".btnModule[pathurl='" + modPathUrlActive + "']").addClass("active")
        }
        eval(funcName + "()")

        resizeWorkspace()
    })
}

export const forceLogout = () => {
    console.log('forceLogout');
    localStorage.clear()
    window.location.href = _PATH_LOGOUT
}

async function logout() {
    var url =
        _PATH_WEB_NUCLEO + "modules/dashboard/controllers/apis/v1/logout.php"
    const dataForm = new FormData()
    dataForm.append("access_token", localStorage.getItem("access_token"))
    dataForm.append("refresh_token", localStorage.getItem("refresh_token"))
    dataForm.append("entitieId", localStorage.getItem("idEntitie"))
    dataForm.append("page", _PATH_PAGE)

    let req = new Request(url, {
        async: true,
        method: "POST",
        mode: "cors",
        body: dataForm,
    })
    try {
        let contentAwait = await fetch(req)
        let str = await contentAwait.json()
        //console.log(str)
        //console.log('logout: ' + str)
        return str
    } catch (err) {
        //console.log("logout Error: " + err)
    }
}

async function loadModule(path, modIdActive) {
    //console.log("loadModule:" + _PATH_WEB + path + "index.php")
    var url = _PATH_WEB_NUCLEO + path + "index.php"
    let access_token = localStorage.getItem("access_token")
    let refresh_token = localStorage.getItem("refresh_token")
    let idEntitie = localStorage.getItem("idEntitie")
    //var userDataVars = JSON.parse(userData)
    var userDataVars = userData

    const dataForm = new FormData()
    dataForm.append("access_token", access_token)
    dataForm.append("refresh_token", refresh_token)
    dataForm.append("page", _PATH_PAGE)
    dataForm.append("idEntitie", idEntitie)
    dataForm.append("idRol", userDataVars.rolId)
    dataForm.append("path", path)
    dataForm.append("modIdActive", modIdActive)

    let req = new Request(url, {
        method: "POST",
        mode: "cors",
        body: dataForm,
    })

    try {
        let contentAwait = await fetch(req)
        var str = await contentAwait.text()
        //str = str.replace("{{_PATH_MODULE}}", _PATH_WEB + "modules/" + path + "/");
        //console.log("loadModule:" + str);
        if (str != "undefined") {
            return str
        }
    } catch (err) {
        //console.log(path + " Error:" + err)
    }
}

export function loadBody() {
    //console.log("body")
    $("#root").append('<div class="workspace"></div>')
}

export function resizeNav() {

    var w = $(window).outerWidth()
    var h = $(window).outerHeight()
    var hNav = $("nav").outerHeight()
    var innerWidth = w - 48;
    var wInnerEntities = $(".innerEntities").outerWidth();

    //$("nav").outerWidth(w)
    $(".innerNav").outerWidth(innerWidth)
    $(".sidebar").outerHeight(h - 40)
    $(".workspace").outerHeight(h)
    $(".workspace .ws").outerHeight(h);

    if (w < 470) {
        $(".innerEntities").css("left", (w - wInnerEntities) / 2)
        $(".boxEntities").prependTo(".sidebar .innerMenu")
        $(".innerMenuNav").prependTo(".sidebar .innerMenu")
        unCollapseSidebar()
        reorderTabMobile()
    } else {
        $(".innerEntities").css("left", "auto")
        reorderTabDesktop();
    }

    if (w < 850 && h < 470) {
        unCollapseSidebar()
        reorderTabMobile()
    }
    translate();
    resizeMenuTop();
}

export function resizeWorkspace() {
    //console.log("resizeWorkspace")
    var w = $(window).outerWidth()
    var h = $(window).outerHeight()
    var wSidebarModule = $(".sidebarMenuModule").outerWidth()
    var hSidebarMod = $(".sidebarMenuModule").outerHeight()
    var hSidebarModule = $(".sidebarMenuModule").outerHeight()
    var hBoxModule = $(".boxModule").outerHeight()
    var wW = w - wSidebarModule
    //var hBodyModule = h - $("nav").outerHeight() - $(".menuTopModule").outerHeight();
    var hBodyModule = h - $("nav").outerHeight();
    var hSidebarModule = h - $("nav").outerHeight() - 4;
    //$(".bodyModule").outerWidth(wW);
    $(".ws .bodyModule").outerWidth(wW);
    $(".ws .boxModule").outerHeight(hBodyModule);
    $(".ws .bodyModule").outerHeight(hBodyModule);
    $(".ws .sidebarMenuModule").outerHeight(hSidebarModule);
    //console.log("w", w, "h", h, "hSidebarMod ", hSidebarMod, "hSidebarModule", hSidebarModule, "hBoxModule", hBoxModule, "hBodyModule", hBodyModule)
    //$(".menuTopLeft").outerWidth(wSidebarModule)
    $(".ws .bodyModule .innerForm").outerHeight(hSidebarMod);
    $(".ws .bodyModule .innerForm").outerWidth(wW);
    resizeMenuTop()
}

export function resizeMenuTop() {
    let w = $(window).outerWidth()
    let wMenuTopLeft = $(".menuTopLeft").outerWidth()
    let wMenuTopRight = $(".menuTopRight").outerWidth()
    let wM = w - wMenuTopLeft;

    $(".menuTopRight").outerWidth(wM);
}

export function reorderTabMobile() {
    //console.log("reorderTabMobile ")
    $(".itemTabNav[active='0']").prependTo(".boxTabMobil .inner")
    $(".itemTabNav[active='1']").prependTo(".tabsNavList")
}

export function reorderTabDesktop() {
    //console.log("reorderTabDesktop")
    $(".itemTabNav").prependTo(".tabsNavList");

    let numTabs = $(".tabsNavList .itemTabNav").length;
    let wTabs = $(".tabsNavList").outerWidth();
    let wItemTab = (wTabs / numTabs);
    $(".tabsNavList .itemTabNav").outerWidth(wItemTab);
    $(".tabsNavList .itemTabNav button").css("width", "75%");
    //console.log("numTabs: " + numTabs);
}

export const loadSidebar = async (menusJson) => {
    //console.log("loadSidebar", menusJson);

    if (menusJson.Error == 1) {
        console.log("loadSidebar Error: " + menusJson.ErrorMessage);

        forceLogout()

    }

    var menuJson = menusJson
    var menuGeneral = menuJson.menu

    //var menuGeneral = JSON.parse(menuJson.menu)
    var menuSidebarSystems = menuGeneral.sidebar.systems
    var menuSidebarPersonal = menuGeneral.sidebar.personal
    var menuSidebarAdministrator = menuGeneral.sidebar.administrator
    //console.log(menuJson)
    //console.log("menuSidebarSystems", menuSidebarSystems)
    //console.log(menuGeneral.sidebar.tabs)
    //console.log(menuGeneral)
    //console.log(menuGeneral.sidebar.systems)
    //console.log(menuSidebarAdministrator)
    //console.log("menuGeneral.tabs", menuGeneral.tabs)
    //Systems

    let str = ""
    if (
        menuSidebarSystems != 0 &&
        menuSidebarSystems != undefined &&
        menuGeneral.tabs != 0
    ) {
        let name = menuGeneral.sidebar.systems.label
        let data = menuGeneral.sidebar.systems.data
        //console.log("name" + name);
        //console.log(data);
        str += '<div class="elem elem-0 elem-systems">'
        str += "    <label>" + name + "</label>"
        str += '    <div class="inner">'

        var j = 0
        for (let keyItem in data) {
            let id = data[keyItem]["id"]
            let path = data[keyItem]["path"]
            let pathUrl = data[keyItem]["pathUrl"]
            let name = data[keyItem]["name"]
            let type = data[keyItem]["type"]
            let color = data[keyItem]["color"]
            let icon = data[keyItem]["icon"]
            let modDefault = data[keyItem]["modDefault"];
            str += elemMenu({
                item: j++,
                id: id,
                path: path,
                pathUrl: pathUrl,
                type: type,
                name: name,
                color: color,
                icon: icon,
                modDefault
            })
        }

        str += "    </div>"
        str += "</div>"
    }

    //Personal
    if (menuSidebarPersonal != 0) {
        let name = menuGeneral.sidebar.personal.label
        let data = menuGeneral.sidebar.personal.data
        str += '<div class="elem elem-0 elem-list elem-personal">'
        str += "    <label>" + name + "</label>"
        str += '    <div class="inner">'

        var j = 0
        for (let keyItem in data) {
            let id = data[keyItem]["id"]
            let path = data[keyItem]["path"]
            let name = data[keyItem]["name"]
            let type = data[keyItem]["type"]
            let color = data[keyItem]["color"]
            let icon = data[keyItem]["icon"]
            str += elemMenu({
                item: j++,
                id: id,
                path: path,
                type: type,
                name: name,
                color: color,
                icon: icon,
            })
        }

        str += "    </div>"
        str += "</div>"
    }

    //Administrator
    if (menuSidebarAdministrator != 0) {
        let name = menuGeneral.sidebar.administrator.label
        let data = menuGeneral.sidebar.administrator.data
        str += '<div class="elem elem-0 elem-list elem-administrator">'
        str += "    <label>" + name + "</label>"
        str += '    <div class="inner">'

        var j = 0
        for (let keyItem in data) {
            let id = data[keyItem]["id"]
            let path = data[keyItem]["path"]
            let name = data[keyItem]["name"]
            let type = data[keyItem]["type"]
            let color = data[keyItem]["color"]
            let icon = data[keyItem]["icon"]
            let modDefault = data[keyItem]["modDefault"]
            str += elemMenu({
                item: j++,
                id: id,
                path: path,
                name: name,
                type: type,
                color: color,
                icon: icon,
                modDefault
            })
        }

        str += "    </div>"
        str += "</div>"
    }

    $(".sidebar .innerMenu").append(str)

    //console.log(JSON.parse(menuJson.sidebar));
}

export function elemMenu(vars) {
    var str = ""
    let id = vars.id
    let path = vars.path
    let name = vars.name
    let type = vars.type
    let color = vars.color
    let icon = vars.icon
    let pathUrl = vars.pathUrl
    let classItem = vars.classItem
    let item = vars.item
    let modDefault = vars.modDefault ? vars.modDefault : 0
    str +=
        '    <div class = "item item-' +
        item +
        " " +
        classItem +
        '" type="' +
        type +
        '" id = "item-' +
        id +
        '" > '
    str += '    <button class="btnItemMenu" moddefault="' + modDefault + '" type="' + type + '" system="' + pathUrl + '" color="' + color + '" item="' + id + '" path="' + path + '">'
    str += '        <i class="' + icon + '" style="color:' + color + '" ></i>'
    str += '        <span class="name">' + name + "</span>"
    str += "    </button>"
    str += "    </div>"

    return str
}

export function elemTab(vars) {
    var str = ""
    let id = vars.id
    let path = vars.path
    let name = vars.name
    let type = vars.type
    let color = vars.color
    let icon = vars.icon
    let classItem = vars.classItem
    let item = vars.item
    let modDefault = vars.modDefault ? vars.modDefault : 0
    let active = 0
    if (classItem == "active") {
        active = 1
    }

    str += '    <div class = "itemTabNav item item-' + item + " " + classItem + '" moddefault="' + modDefault + '" active="' + active + '"   item="' + id + '" path="' + path + '" type="' + type + '" id = "item-' + id + '" > '
    str += '        <button class="btnItemTab"   item="' + id + '" path="' + path + '">'
    str += '            <i class="' + icon + '"></i>'
    str += '            <span class="name">' + name + "</span>"
    str += "        </button>"
    str += '        <button class="btnItemTabClose" data-path="' + path + '"   item="' + id + '">'
    str += '            <i class="icon icon-close"></i>'
    str += "        </button>"
    str += "    </div>"

    return str
}

export const loadMenu = async () => {
    var url =
        _PATH_WEB_NUCLEO +
        "modules/dashboard/controllers/apis/v1/menu.php?" +
        _VS
    let access_token = localStorage.getItem("access_token")
    let refresh_token = localStorage.getItem("refresh_token")
    let idEntitie = localStorage.getItem("idEntitie")

    //console.log("idEntitie:" + idEntitie)

    const dataForm = new FormData()
    dataForm.append("access_token", access_token)
    dataForm.append("refresh_token", refresh_token)
    dataForm.append("idEntitie", idEntitie)
    dataForm.append("page", _PATH_PAGE)

    let req = new Request(url, {
        method: "POST",
        mode: "cors",
        body: dataForm,
    })

    try {
        let contentAwait = await fetch(req)
        let str = await contentAwait.json();
        //console.log("loadMenu:", str);
        return str
    } catch (err) {
        //console.log("loadMenu Error:" + err)
    }
}

export const loadNav = async (userData) => {
    // var userData = JSON.parse(userData)
    //console.log("loadNav",userData)

    var url = _PATH_WEB_NUCLEO + _PATH_DASHBOARD + "views/nav.html?" + _VS
    //console.log(url)
    try {
        let contentAwait = await fetch(url)
        var str = await contentAwait.text()

        str = replacePath(str)
        str = str.replace("{{_VS}}", _VS)
        str = str.replace(
            "{{_PATH_DASHBOARD_FAVICON}}",
            _PATH_DASHBOARD_FAVICON
        )
        str = str.replace(
            "{{_PERFIL_NAME}}",
            userData.userName + " " + userData.userLastname
        )
        str = str.replace("{{_PERFIL_EMAIL}}", userData.userEmail)
        str = str.replace("{{_PERFIL_ROL}}", userData.rolName)

        if (userData.userImg) {
            //str = str.replace("{{_PROFILE}}", '<img src="' + userData.userImg + '" />')
            str = replaceAll(
                str,
                "{{_PROFILE}}",
                '<img src="' + userData.userImg + '" />'
            )
        } else {
            //str = str.replace("{{_PROFILE}}", userData.initial)
            str = replaceAll(str, "{{_PROFILE}}", userData.initial)
        }

        //console.log("str:" + str)

        if (userData.entities == 0) {
            str = str.replace("{{selectEntities}}", "")
        } else {
            //console.log("userData.entities:" + userData.entities)
            var contEnt = userData.entities
            //var contEnt = JSON.parse(userData.entities)
            var contStr = contEnt.length

            //console.log("contEnt", contEnt, "contStr", contStr)
            //console.log("contStr:" + contStr)

            if (contStr == 1) {
                // //console.log(contEnt)
                //console.log(contEnt[0].id)
                localStorage.setItem("idEntitie", contEnt[0].id)
                str = str.replace(
                    "{{selectEntities}}",
                    '<div class="boxEntities"><span>' +
                    contEnt[0].name +
                    "</span></div>"
                )
            } else {
                var str1 = '<div class="boxEntities">'
                for (let index = 0; index < contStr; index++) {
                    const element = contEnt[index]
                    var onItem = ""
                    var onIcon = "icon-circle"
                    if (index == 0) {
                        localStorage.setItem("idEntitie", element.id)
                        str1 += '<a class="item-select">'
                        if (element.brand != "") {
                            str1 += '<img src="' + _PATH_WEB + element.brand + '"/>'
                        }
                        str1 += " <span>" + element.name + "</span>"
                        str1 += ' <i class="icon  icon-arrow-o-donw"></i>'
                        str1 += "</a>"
                        str1 += '<div class="inner innerEntities">'
                        onItem = "on"
                        onIcon = "icon-circle-point"
                    }
                    str1 +=
                        '<a class="item item-' +
                        index +
                        "  " +
                        onItem +
                        '" item="' +
                        element.id +
                        '" name="' +
                        element.name +
                        '" brand="' +
                        _PATH_WEB +
                        element.brand +
                        '" >'
                    str1 += ' <i class="icon ' + onIcon + '"></i>'
                    str1 += ' <img src="' + _PATH_WEB + element.brand + '"/>'
                    str1 += " <span>" + element.name + "</span>"
                    str1 += "</a>"
                    if (index == contStr - 1) {
                        str1 += "</div>"
                    }
                }
                str1 += "</div>"
                str = str.replace("{{selectEntities}}", str1)
            }
        }

        //console.log(str)
        return str
    } catch (err) {
        //console.log("loadLoginRoot Error:" + err)
    }
}

export function structureMenus(menusJson) {
    //var dates = JSON.parse(menusJson)
    var dates = menusJson
    //cargando Nav Menu
    userData = dates.user
    //console.log("userData:" + userData)
    loadNav(dates.user).then((str) => {
        $("#root").html(str);
        loadBody() //estructura de cuerpo
        resizeNav();
        loadSidebar(menusJson)
        loadTabs(menusJson)
    })
}

export function closeProfileMenu() {
    $(".profileMenu").removeClass("on")
    $(".btnProfileNav").attr("state", "0")
}

export function openProfileMenu() {
    $(".profileMenu").addClass("on")
    $(".btnProfileNav").attr("state", "1")
}

export function closeSidebar() {
    $(".menu").attr("state", 0)
    $(".sidebar").removeClass("on")
}

export function openSidebar() {
    $(".menu").attr("state", 1)
    $(".sidebar").addClass("on")
}

export const state = (vars = []) => {
    let display = setInterval(stateActions, 2000);
}

export const stateActions = (vars = []) => {
    //console.log('stateActions', vars);
    let nowDate = new Date().toLocaleDateString('es-es', {
        weekday: "long",
        year: "numeric",
        month: "long",
        day: "numeric",
        hour: "numeric",
        minute: "2-digit"
    });

    $('.dateDisplay').html(`<span class="dayLiteral">${capitalize(nowDate)}</span>`);
}

//export const state = (vars = []) => {