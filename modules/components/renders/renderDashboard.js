export const renderDashboardSection = (vars = []) => {
    //console.log('section', vars);
    let cls = vars.cls || "general sectionfluid";
    let title = vars.title || "General";
    let content = vars.content || "content";
    let clsBody = vars.clsBody || "";
    return `<section class="${cls}">
                <div class="head">
                    <div class="left">
                        <i class="icon icon-gril-compac"></i>
                        <h3>${title}</h3>
                    </div>
                    <div class="right">
                        <a class="btn btnIcon">
                            <i class="icon icon-points-v"></i>
                        </a>
                    </div>
                </div>
                <div class="body ${clsBody}">
                    ${content}
                </div>
            </section>`;
}

export const renderGeneralBlock = (vars = []) => {
    //console.log('renderGeneralBloc', vars);
    let numToday = vars.data.general.report.todayReport.num || 0;
    let numLast = vars.data.general.report.lastReport.num || 0;
    return `<div class="col col1 col20w colColumn">
                    ${blockSimpleData({ label: "Hoy", value: numToday })}
                    ${blockSimpleData({ label: "Ultima Semana", value: numLast })}
                </div>
                <div class="col col80w col2">
                    <canvas id="${vars.id}"></canvas>
                </div>`;
}

export const blockSimpleData = (vars = []) => {
    //console.log('blockSimpleData', vars);
    let cls = vars.cls || "";
    let label = vars.label || "";
    let value = vars.value || "";
    let clsBlock = vars.clsBlock || "";

    return `<section class="${cls}">
                <div class="head">
                    <div class="left">
                        <i class="icon icon-gril-compac"></i>
                    </div>
                    <div class="right">
                        <a class="btn btnIcon">
                            <i class="icon icon-points-v"></i>
                        </a>
                    </div>
                </div>
               <div class="block ${clsBlock}">
                    <div class="value">${value}</div>
                    <label>${label}</label>
                </div>
            </section>`;
}