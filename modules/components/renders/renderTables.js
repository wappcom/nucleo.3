import {
	btnFn,
	capitalize,
	emptyReturn
} from "./../functions.js";
import * as nav from "../nav.js";

export const renderNoDataTable = () => {
	return /*html*/ `
        <div class="message messageCenter" lang="es">No hay datos en la lista para mostrar.</div>
    `;
};

export function renderBtnState(vars) {
	let cls = vars.cls ? vars.cls : "btnStateModuleItem";
	return `<a class="${cls} state " 
                item="${vars.item}" 
                module="${vars.module}" 
                system="${vars.system}" 
				fn = "${vars.fn}"
                state="${vars.state}">
					<i class="icon icon-eye-close"></i>
					<i class="icon icon-eye-open"></i>
			</a>`;
}

export function renderColumns(vars) {
	const rows = emptyReturn(vars.rows, "");
	return (
		`
    <tr class="rowItem" item="${vars.id}">
        <td class="colCheck"><input name="inputCheck[]"  type="checkbox" value="${vars.id}"></td>
        <td class="colId">${vars.id}</td>
        <td class="colTitle">${vars.title}</td>
        ${rows}
        <td class="colState">` +
		renderBtnState({
			item: vars.id,
			fn: "changeStateItem",
			state: vars.state,
			module: vars.module,
			system: vars.system,
		}) +
		`</td>
        <td class="colActions">
            <div class="btns">
                 ` +
		btnFn({
			btnType: "btnEdit",
			clss: "btnActionModal",
			attr: "content='configFormHalls' module='" + vars.module + "'",
			item: vars.id,
			module: vars.module,
			system: vars.system,
			fn: "editItemHall",
			vars: vars.id,
			attr: `data-id="${vars.id}" data-module="${vars.module}" data-system="${vars.system}"`,
		}) +
		` ` +
		btnFn({
			btnType: "btnDelete",
			item: vars.id,
			module: vars.module,
			system: vars.system,
			fn: "deleteItemHall",
			vars: vars.name + "",
			attr: `data-id="${vars.id}" data-module="${vars.module}" data-system="${vars.system}" data-name="${vars.name}"`,
		}) +
		`
            </div>
        </td>
     </tr>
   `
	);
}

export const renderRowsTable = (vars) => {
	//console.log('renderRowTable');
	const id = emptyReturn(vars.id, "");
	const cls = emptyReturn(vars.cls, "");
	const attr = emptyReturn(vars.attr, "");
	const content = emptyReturn(vars.content, "");
	return /*html*/ `
        <tr class="rowItem ${cls}" ${attr} item="${id}" >
            ${content}
        </tr>
    `;
};

export const renderColCheck = (vars) => {
	//onsole.log('renderColCheck');
	const id = emptyReturn(vars.id, "");
	const cls = emptyReturn(vars.cls, "");
	const attr = emptyReturn(vars.attr, "");
	return /*html*/ `
        <td class="colCheck ${cls}" ${attr} item="${id}" ><input name="inputCheck[]"  type="checkbox" value="${id}"></td>
    `;
};

export const renderColId = (vars) => {
	//onsole.log('renderColId');
	const id = emptyReturn(vars.id, "");
	const cls = emptyReturn(vars.cls, "");
	const attr = emptyReturn(vars.attr, "");
	return /*html*/ `
        <td class="colId ${cls}" ${attr} item="${id}" >${id}</td>
    `;
};

export const renderColTitle = (vars) => {
	//onsole.log('renderColTitle');
	const id = emptyReturn(vars.id, "");
	const cls = emptyReturn(vars.cls, "");
	const attr = emptyReturn(vars.attr, "");

	return /*html*/ `
        <td class="colTitle ${cls}" ${attr} item="${id}" >${vars.title}</td>
    `;
};

export const renderColTitlePath = (vars) => {
	//onsole.log('renderColTitle');
	const id = emptyReturn(vars.id, "");
	const cls = emptyReturn(vars.cls, "");
	const attr = emptyReturn(vars.attr, "");

	return /*html*/ `
        <td class="colTitle ${cls}" ${attr} item="${id}" ><a target="_blank" href="${vars.link}">${vars.title}</a></td>
    `;
};


export const renderCol = (vars) => {
	//console.log('renderCol', vars);
	const id = emptyReturn(vars.id, "");
	const cls = emptyReturn(vars.cls, "");
	const attr = emptyReturn(vars.attr, "");
	let data = vars.data ? vars.data : "---";
	const type = vars.type ? vars.type : "text";
	//console.log('data', data)

	if (type == "text") {
		return /*html*/ `
			<td class="col ${cls}" ${attr} item="${id}" >${data}</td>
		`;
	}
	if (type == "array" && data != '---') {

		let returnHtml = '';
		for (let i = 0; i < data.length; i++) {
			let obj = data[i];
			var pairs = [];

			for (var [key, value] of Object.entries(obj)) {
				pairs.push(value);
			}

			returnHtml += pairs.join(":") + '<br>';
		}

		return `<td class="col ${cls}" ${attr} item = "${id}" > ${returnHtml}</td>`;
	}

	if (type == "array" && data == '---') {
		return /*html*/ `
			<td class="col ${cls}" ${attr} item="${id}" >---</td>
		`;
	}
}

//saber el numero de hijos de cada elemento de un objeto
export const numColumnsObj = (array) => {
	let obj = array[0];
	let linear = [];
	for (var [key, value] of Object.entries(obj)) {
		// Procesar la clave y el valor aquí
		linear.push({
			key: key,
			value: value
		});
	}
	return linear;
}

export const renderColCategorys = (vars) => {
	//console.log('renderColCategorys', vars);
	const id = emptyReturn(vars.id, "");
	const cls = emptyReturn(vars.cls, "colCats");
	const attr = emptyReturn(vars.attr, "");
	const fn = emptyReturn(vars.fn, "");
	const array = emptyReturn(vars.array, "");

	let data = [];

	if (array != "") {
		for (let i = 0; i < array.length; i++) {
			const elem = array[i];
			const name = elem["name"];
			const id = elem["id"];
			const pathurl = elem["pathurl"] ? elem["pathurl"] : "";
			const state = elem["state"] ? elem["state"] : "";
			data[i] = /*html*/ ` <a pathurl="${pathurl}" data-state='${state}' data-fn="${fn}" data-id="${id}" data-item="${id}">${name}</a> `;
		}
	}


	return /*html*/ `
        <td class="col ${cls}" ${attr} data-item="${id}" ><div class="content">${data.join(",&nbsp;")}</div></td>
    `;
};

export const renderColState = (vars) => {
	//onsole.log('renderColState');
	const id = emptyReturn(vars.id, "");
	const cls = emptyReturn(vars.cls, "");
	const attr = emptyReturn(vars.attr, "");
	const fn = vars.fn ? vars.fn : "changeStateItem";
	const colState = renderBtnState({
		item: vars.id,
		fn,
		cls,
		state: vars.state,
		module: vars.module,
		system: vars.system,
	});
	return /*html*/ `
        <td class="colState" ${cls}" ${attr} item="${id}" >${colState}</td>
    `;
};

export const renderColActions = (vars) => {
	const id = emptyReturn(vars.id, "");
	const cls = emptyReturn(vars.cls, "");
	const attr = emptyReturn(vars.attr, "");
	const icon = emptyReturn(vars.icon, "icon icon-pencil,icon icon-trash");
	const type = emptyReturn(vars.type, "btnEdit,btnDelete");
	const fnType = emptyReturn(vars.fnType, "formEdit,deleteModuleItem");
	const arrayType = type.split(",");
	const arrayFnType = fnType.split(",");
	const arrayIcon = icon.split(",");

	let btns = "";
	for (let i = 0; i < arrayType.length; i++) {
		let elm = arrayType[i];
		let fn = arrayFnType[i];
		let icon = arrayIcon[i];
		if (elm == "btnEdit") {
			btns += btnFn({
				btnType: "btnEdit",
				clss: "btnActionModal",
				attr: "content='configFormHalls' module='" + vars.module + "'",
				item: vars.id,
				module: vars.module,
				system: vars.system,
				fn: fn,
				vars: vars.id,
				attr: `data-id="${vars.id}" data-module="${vars.module}" data-system="${vars.system}"`,
			});
		}
		if (elm == "btnEditFn") {
			btns += btnFn({
				btnType: "btnEdit",
				clss: "btnEditFn",
				attr: `data-id="${vars.id}" data-module="${vars.module}" data-system="${vars.system}"`,
			});
		}
		if (elm == `btnDelete`) {
			btns += btnFn({
				btnType: "btnDelete",
				item: vars.id,
				module: vars.module,
				system: vars.system,
				fn: fn,
				vars: vars.name + ",",
				attr: `data-id="${vars.id}" data-module="${vars.module}" data-system="${vars.system}" data-name="${vars.name}"`,
			});
		}

		if (elm == `btnDeleteFn`) {
			btns += btnFn({
				btnType: "btnDeleteFn",
				cls: fn,
				attr: `data-id="${vars.id}"  data-name="${vars.name}" data-module="${vars.module}" data-system="${vars.system}"`,
			});
		}

		if (elm == `btnViewFn`) {
			btns += btnFn({
				btnType: "btnViewFn",
				cls: fn,
				attr: `data-id="${vars.id}"  data-name="${vars.name}" data-module="${vars.module}" data-system="${vars.system}"`,
			});
		}

		if (elm == `btnDeleteStd`) {
			btns += btnFn({
				btnType: "btnDelete",
				item: vars.id,
				module: vars.module,
				system: vars.system,
				fn: fn,
				attr: ` std ='1' name='${vars.name}' `,
				vars: "",
				attr: `data-id="${vars.id}" data-module="${vars.module}" data-system="${vars.system}" data-name="${vars.name}"`,
			});
		}

		if (elm == 'btn') {
			btns += `<a class="btn btnIcon ${fn}" ${attr}><i class="${icon}"></i></a>`;
		}
	}
	return /*html*/ `
        <td class="colActions" ${cls}" ${attr} item="${id}" >
            <div class="btns">
                ${btns}
            </div>
        </td>
    `;
};


export const renderTableForm = (vars) => {
	console.log('renderGetTable', vars);
	let tableId = vars.id;
	let data = vars.data;
	let container = emptyReturn(vars.container, 0);
	let module = vars.module;
	let system = vars.system;
	let orderCol = emptyReturn(vars.orderCol, 1);
	let thead = emptyReturn(vars.thead, [{
		label: "id",
		col: "id",
		"cls": "id"
	}, {
		label: "name",
		col: "name",
		cls: "name"
	}]);
	let strThead = '';
	let strTbody = '';
	let strTbodyInitial = '<tr class="rowInit">';

	for (let index = 0; index < thead.length; index++) {
		const element = thead[index];
		strThead += `<td class="col col-${index} ${element.cls}">${element.label}</td>`;
		let strInput = '';
		if (element.type != "") {
			if (element.type == "none") {
				strInput = '---';
			} else if (element.type == "input") {
				strInput = `<input 
								tabindex="${index}" 
								type="text" 
								autocomplete="disabled"  
								autocomplete="off"
								autocomplete="off"
								class="formControl" 
								name="${element.id}" 
								id="${element.id}" value="">`;
			} else if (element.type == "action") {
				if (element.action == "add") {}
				if (element.action == "returnFn") {
					strInput = `<buttom 
									module = "${module}"
									system = "${system}"
									item = "${element.id}"
									tabindex="${index}"
									type="buttom"
									class="btn btnPrimary btnTableReturnFn" 
									fn="${element.fnInit}" 
									vars="${tableId}" >
									<i class="icon icon-arrow-line-right"></i>
								</buttom>`;
				}
			}
		}
		strTbodyInitial += `<td class="col col-${index} ${element.cls}">${strInput}</td>`;
	}

	strTbodyInitial += '</tr>';

	for (let i = 0; i < data.length; i++) {
		const element = data[i];
		let strRow = '';
		for (let j = 0; j < thead.length; j++) {
			const elem = thead[j];
			strRow += `<td class="col col-${j} ${elem.cls}">
							${elem[element.col]}
						</td>`;
		}
		strTbody += `<tr class="rowItem" item="${element.id}">
						${strRow}
					</tr>`;
	}

	let html = /*html*/ `
			
		<table class="tableForm" id="${tableId}" method>
				<thead>
					<tr>${strThead}</tr>
				</thead>
				<tbody>
					${strTbodyInitial + strTbody}
				</tbody>
				<tfoot></tfoot>
		</table>`;

	if (container) {
		//insert container
	} else {
		//return
		//console.log(thead);
		return html;
	}



}

document.addEventListener("DOMContentLoaded", function () {
	$("body").on("click", ".btnTableReturnFn", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		//console.log('.btnReturnFn1');	
		const module = $(this).attr("module");
		const item = $(this).attr("item");
		const fn = $(this).attr("fn");
		const vars = $(this).attr("vars");
		const system = $(this).attr("system");

		eval(fn + "({module,item,system,vars})");
	})

	$("body").on("keypress", ".btnTableReturnFn", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		//console.log('.btnReturnFn2');
		const module = $(this).attr("module");
		const item = $(this).attr("item");
		const fn = $(this).attr("fn");
		const vars = $(this).attr("vars");
		const system = $(this).attr("system");

		eval(fn + "({module,item,system,vars})");
	})

});

export function renderActions(vars) {
	//console.log('renderActions', vars);

	let form = vars.form.slice(1);

	return /*html*/ `<select class="formInput selectActionsTable selectActions${capitalize(form)} ${vars.cls}"   for="${vars.form}">
                <option value="0" lang="es">Acciones</option>
                <option value="delete_selected" lang="es">Eliminar seleccionados</option>
            </select>`
}

export const renderMacroTable = (vars = []) => {
	//console.log('renderMacroTable', vars);
	let cls = vars.cls ? vars.cls : '';
	let body = vars.body ? vars.body : '';
	let module = vars.module ? vars.module : '';
	let fn = vars.fn ? vars.fn : '';

	return /*html*/ `<div class="macroTable ${cls}" data-module="${module}"  id="${vars.tableId}">
						<div class="macroTableHeader">
							<div class="left">
								<div class="search">
									<i class="icon icon-search"></i>
									<input type="text" class="formInput searchInput" placeholder="Buscar...">
								</div>
								<div class="pages">
									<select class="formInput selectPages">
										<option value="25" lang="es">25</option>
										<option value="50" lang="es">50</option>
										<option value="100" lang="es">100</option>
										<option value="200" lang="es">200</option>
										<option value="Alls" lang="es">Todas</option>
									</select>
								</div>
								<div class = "actions" >
									<select class="formInput selectActions" data-module="${module}" data-table-id="${vars.tableId}" data-fn="${fn}" >
										<option value="0" lang="es">Acciones</option>
										<option value="delete_selected" lang="es">Eliminar</option>
										<option value="edit_selected" lang="es">Editar</option>
										<option value="all_selected" lang="es">Seleccionar todos</option>
										<option value="all_deselected" lang="es">Deseleccionar todos</option>
									</select>
								</div>
							</div>
							<div class="middle"></div>
							<div class="right">
								<div class="actions">
									<a class="btn btnIcon btnFull btnMini btnFields "><i class="icon icon-column"></i></a>
									<a class="btn btnIcon btnFull btnMini btnFilters "><i class="icon icon-filter"></i></a>
									<div class="navigation">
									</div>
								</div>
							</div>
						</div>

						<div class="macroTableBody">
							${body}
						</div>
					</div>`
}

export const renderColCheckboxState = (vars = []) => {
	console.log('renderColCheckboxState', vars);
	let inputState = vars.inputState || 0;
	let fn = vars.fn ? vars.fn : '';
	let action = vars.action ? vars.action : '';
	let id = vars.id;
	let state = "";
	if (inputState == 1) {
		state = `checked="checked"`;
	}
	return /*html*/ `
		<input type="checkbox" 
			class="tableInput" 

			name="inputStateItem[]" 
			tabindex="4" 
			id="inputStateItem${id}" 
			value="${inputState}" 
			data-action="${action}"
			data-fn="${fn}"
			data-id="${id}" ${state}>
	`;

}