export function renderLoading(Cls) {
    return `<div class="loading ${Cls}"></div>`
}

export function renderContenido(vars) {
    var html = vars.html;
    var id = vars.id;
    return `
		<div class="content-inner  animated fadeIn" id="` + id + `">` + html + `</div>
	`;
}

export function renderFinderTabs(vars) {
    var upload = vars.upload;
    return `
		<div class="finder-head">   
			<div class="group-tabs">     
				<div class="tab">       
					<label class="title title-finder">FINDER</label>       
					<span class="group">         
						<a class="category active" id="tab-todos" idtab="todos" style="display: inline-block;">Todos</a>         
						<a class="category" id="tab-imagenes" idtab="imagenes" style="display: inline-block;">Imagenes</a>         
						<a class="category" id="tab-albums" idtab="albums" style="display: none;">Albums</a>         
						<a class="category" id="tab-videos" idtab="videos" style="display: none;">Videos</a>         
						<a class="category" id="tab-audio" idtab="audio" style="display: none;">Audio</a>         
						<a class="category" id="tab-documentos" idtab="documentos" style="display: none;">Documentos</a>       
					</span>     
				</div>   
			</div>
		</div>
	`;
}


export function renderRootModulo(vars) {
    var id = vars.id;
    var fnRetorno = vars.fnRetorno;
    var fnRetornoNombre = vars.fnRetornoNombre;
    var clase = vars.clase;
    var title = vars.title;
    var tbody = vars.tbody;
    var botones = vars.botones;
    return `
		<div class="rootModulo container-fluid animated fadeIn ` + clase + `" id="${ id }">
			<div class="container">
				<div class="header">
					<a fn="${ fnRetorno }" class="btn btnVolver"><i class="icn-arrow-line-left"></i><span>${ fnRetornoNombre }</span></a>
					<div class="inner">
						<label class="title"><h1>${ title }</h1></label>
						${botones}
					</div>
				</div>
				<div class="tbody">
				` + tbody + `
				</div>
			</div>
		</div>
	`;
}

export function renderNav() {
    return `
    <div class="nav">
    	<div class="navBrand">
			<a class="brand" style="background:url(${ sitio + jsonConf.conf_favicon })no-repeat center center" href="${ sitio }dashboard"></a>
			<a class="btnNavActionBrand">
				<i class="icn icn-arrow-o-donw"></i>
			</a>
    	</div>
		<div class="navMenu">
			<a href="${ sitio }" target="_blank" class="btnNavMenu">${ sitio }</a>
			<a href="${ sitio }dashboard" target="_self" class="btnNavMenu">Dashboard</a>
		</div>
		<div class="navMenuSidebar">
			<div class="menuSidebar"></div>
			<div class="bg"></div>
		</div>
		<ul class="navInner">
			<li class="perfil">
				<img src="${ sitioImages + datosUsuario.usu_imagen }" alt="" /> 
				<span>${ datosUsuario.usu_nombre }</span>
			</li>
			<li class="menuBar">		
            	<a class="btnMenu btnMenuNav">
              		<i class="icn icn-reorder"></i>
            	</a>
			</li>
		</ul>
    </div>
  `;
}

export function renderMenuSidebar(vars) {
    var vars = JSON.parse(vars);
    var listSistem = '';
    var listSistemNode = '';

    for (var i = 0; i < vars.length; i++) {
        var sisId = vars[i].sis_id;
        var sisName = vars[i].sis_name;
        var sisIcon = vars[i].sis_icon;
        var sisColor = vars[i].sis_color;
        var sisUrl = vars[i].sis_url;
        var sisNode = vars[i].sis_node;
        var attrVars = '';

        if (sisNode == 0) {
            attrVars = 'class="btnNav btnNavItem" href="' + setUrlNucleo(sisUrl) + '"';
        } else {
            attrVars = 'class="btnNav btnNavItemNode" node="' + i + '"';

            var varsNode = JSON.parse(sisNode);
            listSistemNode += '<div class="sistemsNode" node="' + i + '" sis="' + sisId + '">';
            listSistemNode += '	<label style="background-color:' + sisColor + '" ><i class="' + sisIcon + '"></i><span>' + sisName + '</span></label>';
            listSistemNode += '	<ul>';
            for (var j = 0; j < varsNode.length; j++) {
                var modId = varsNode[j].mod_id;
                var modIcon = varsNode[j].mod_icon;
                var modColor = varsNode[j].mod_color;
                var modName = varsNode[j].mod_name;
                var modUrlPath = varsNode[j].mod_url_path;


                var attrVarsMod = 'href="' + sitio + modUrlPath + '" item="' + modId + '" ';

                listSistemNode += '<li>';
                listSistemNode += '<a class="btnNav" ' + attrVarsMod + '   ><i class="icn ' + modIcon + '" style="color:' + modColor + '"></i><span>' + modName + '</span></a>';
                listSistemNode += '<a class="btnSkip" ' + attrVarsMod + ' target="_blank"  id="btnMod' + modId + '" ><i class="icn icn-skip"></i></a>';
                listSistemNode += '</li>';

            }
            listSistemNode += '</ul>';
            listSistemNode += '</div>';
        }


        listSistem += '<a ' + attrVars + '  item="' + sisId + '" ><i class="' + sisIcon + '" style="background-color:' + sisColor + '"></i><span>' + sisName + '</span></a>';
    }

    return `		
	    <div class="boxSistem">
	    	<div class="boxSistemActives">
				<label class="title" lang="es">Sistemas Activos</label>
				<div class="sistems">
					${ listSistem }
				</div>
			</div>
			${ renderMenuMain() }
			${ renderMenuFooter() }
	    </div>
	    <div class="sistemsNodes">
			${ listSistemNode }
		</div>
  	`;
}

export function renderMenuMain() {
    var listModules = '';
    var varsModules = '';

    if (datosUsuario.rol_id == 1 || datosUsuario.rol_id == 2) {
        //console.log(modulesEssential[0])
        listModules += '<div class="boxMain">';
        listModules += '	<label class="title" lang="es">Administración de Sitios</label>';
        listModules += '	<ul>';
        for (var i = 0; i < modulesEssential.length; i++) {
            var modId = modulesEssential[i].mod_id;
            var modIcon = modulesEssential[i].mod_icon;
            var modColor = modulesEssential[i].mod_color;
            var modName = modulesEssential[i].mod_name;
            var modUrlPath = modulesEssential[i].mod_url_path;
            var attrVarsMod = 'href="' + sitio + "dashboard/" + modUrlPath + '" item="' + modId + '" ';

            listModules += '<li>';
            listModules += '<a class="btnNav" ' + attrVarsMod + '   ><i class="icn ' + modIcon + '" style="color:' + modColor + '"></i><span>' + modName + '</span></a>';
            listModules += '<a class="btnSkip" ' + attrVarsMod + ' target="_blank"  id="btnMod' + modId + '" ><i class="icn icn-skip"></i></a>';
            listModules += '</li>';
        }
        listModules += '	</ul>';
        listModules += '</div>';

        return listModules;
    }
}

export function renderMenuFooter() {
    var iconConf = '';
    if (datosUsuario.rol_id == 1) {
        iconConf = '<a href="' + sitio + 'dashboard/configuracion" class="btnNavFooter btnEditConfig"><i class="icn icn-conf"></i><span>Configuracion</span></a>';
    }
    return `
		<div class="boxMenuFooter">
			<div class="btnNavFooter perfil">
				<i class="icn icn-credential"></i>
				<span class="dataUser">
					<label>` + datosUsuario.usu_nombre + ` ` + datosUsuario.usu_apellidos + `</label>
					<div class="rol">` + datosUsuario.rol_nombre + `</div>
				</span>
				<a class="btnEditPerfil"><i class="icn icn-pencil"></i></a>
			</div>
			${ iconConf }
			<a href="` + sitio + `logout" class="btnNavFooter btnCerrarSession"><i class="icn icn-off"></i><span lang="es">Cerrar Sesión</span></a>
		</div>
    `;
}

export function loadingBtn() {
    return `
		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto;  display: block;" width="21px" height="21px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
		<circle cx="50" cy="50" fill="none" stroke="#ffffff" stroke-width="13" r="36" stroke-dasharray="169.64600329384882 58.548667764616276" transform="rotate(217.396 50 50)">
		  <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="0.704225352112676s" values="0 50 50;360 50 50" keyTimes="0;1"></animateTransform>
		</circle>
		</svg>
    `
}

export function loadingDefault() {

    return `<div class="loadingDefault" style="width=100%; display:flex; justify-content:center"><svg version="1.1" style="  width: 28px; padding:7px; height: auto;"
		xmlns="http://www.w3.org/2000/svg"
		xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="25 25 50 50">
		<circle cx="50" cy="50" r="20" fill="none" stroke-width="5" stroke="#11db76" stroke-linecap="round" stroke-dashoffset="0" stroke-dasharray="100, 200">
			<animateTransform attributeName="transform" attributeType="XML" type="rotate" from="0 50 50" to="360 50 50" dur="2.5s" repeatCount="indefinite"/>
			<animate attributeName="stroke-dashoffset" values="0;-30;-124" dur="1.25s" repeatCount="indefinite"/>
			<animate attributeName="stroke-dasharray" values="0,200;110,200;110,200" dur="1.25s" repeatCount="indefinite"/>
		</circle>
		</svg>
		</div>
    `
}

export function emptyReturn(str, put) {
    if (str == "" || str == undefined) {
        return put
    } else {
        return str
    }
}






export function merge() {
    var obj, name, copy,
        target = arguments[0] || {},
        i = 1,
        length = arguments.length;

    for (; i < length; i++) {
        if ((obj = arguments[i]) != null) {
            for (name in obj) {
                copy = obj[name];

                if (target === copy) {
                    continue;
                } else if (copy !== undefined) {
                    target[name] = copy;
                }
            }
        }
    }

    return target;
}


//tables
export function renderBtnState(vars) {
    return `<a class="btnStateItem state state-${vars.state}" 
                fn="${vars.fn}" 
                item="${vars.item}" 
                module="${vars.module}" 
                system="${vars.system}" 
                state="${vars.state}"><i class="icon icon-circle-point"></i></a>`
}

export function renderActions(vars) {
    return `<select class="formInput">
                <option value="0">Acciones</option>
                
            </select>`
}