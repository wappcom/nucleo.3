export const renderListElements = ({id,module,data}) => {
    console.log('renderListElements',id, module, data);
    let str = `<div class="listElements" id="${id}" data-module="${module}">`;
    str += `<div class="header">
        <div class="search">
            <i class="icon icon-search"></i>
            <input type="text" data-for="${id}" placeholder="Buscar...">
        </div>
        <div class="pages">
        </div>
    </div>
    <div class="lists">`;
    data.forEach(element => {
        //console.log('element',element);
        str += `<div class="element" data-for="${id}" data-item="${element.id}" >
                <div class="head">
                    <a class="btnChecked" data-for="${id}" data-item="${element.id}" data-module ="${module}" ><i class="icon icon-checked"></i></a>
                    <span>${element.name}</span>
                </div>
                <div class="actions">
                    <a class="btnEdit btnEditListElement" data-for="${id}" data-module="${module}" data-item="${element.id}"><i class="icon icon-pencil"></i></a>
                    <a class="btnDeleteStd btnDeleteFormListElement"  data-name="${element.name}" data-id="${element.id}" data-module="${module}" ><i class="icon icon-trash"></i></a>
                </div>
            </div>`;
    });

    str += `</div></div>`;

    return str;
}