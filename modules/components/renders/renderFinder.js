export const renderFinder = (vars = []) => {
    console.log('renderFinder', vars);
    let validfiles = vars.validfiles ? vars.validfiles : `No files`;
    let id = vars.for || "";
    let folders = vars.data.folders.sort();
    let primaryFolder = vars.data.primaryFolder;
    let max = vars.data.max || "3MB";

    validfiles = validfiles.split(',');

    let tabsSectors = validfiles.map((file) => {
        if (file == 'jpeg' || file == 'jpg' || file == 'png' || file == 'gif') {
            return `<a class="tab" item="images" lang="en">Images</a>`;
        }
        if (file == 'pdf' || file == 'xdoc' || file == 'doc' || file == 'docx' || file == 'xls' || file == "xlsx" || file == "ppt" || file == "pptx") {
            return `<a class="tab" item="documents" lang="en">Documents</a>`;
        }
        if (file == 'mp3' || file == 'wav' || file == 'ogg' || file == 'flac') {
            return `<a class="tab" item="audios" lang="en">Audios</a>`;
        }
        if (file == 'mp4' || file == 'embed') {
            return `<a class="tab" item="videos" lang="en">Videos</a>`;
        }
    });

    const result = tabsSectors.reduce((acc, item) => {
        if (!acc.includes(item)) {
            acc.push(item);
        }
        return acc;
    }, []).join('');

    let menuFolders = folders.map((folder) => {
        let aux = "";
        if (folder == primaryFolder) {
            aux = `active`;
        }
        return `<a class="btnFolderFinder ${aux}" data-file="${folder}"><i class="icon icon-folder"></i><span>${folder}</span></a>`
    }).join('');


    let items = vars.data.files.map((item) => {
        let ext = item.ext;
        let pathurl = item.pathurl;
        let typeFile = item.typeFile || "";
        let thumb = item.thumb || "";
        let fileThumb = _PATH_FILES + thumb;
        let image;
        if (typeFile == "image") {
            image = `<img src="${fileThumb}" alt="${item.name}" />`
        }

        return `<div class="item" data-select="no-select" data-type="${item.typeFile}" data-id="${item.id}" data-ext="${ext}">
            <div class="image">${image}</div>
            <div class="info">
                <span class="name" title="${item.name}">${item.name}</span>
                <span class="ext">${ext}</span>
            </div>
        </div>`
    }).join('');

    return `
        <div class="finder">
            <div class="inner">
                <div class="header">
                    <div class="head">
                        <div class="title">Finder</div>
                        <div class="tabs" for="finderContent">
                            ${result}
                        </div>
                    </div>
                    <div class="actions">
                        <div class="search disabled">
                            <i class="icon icon-search"></i>
                            <input type="text" placeholder="Search" />
                        </div>
                        <button class="btnCloseFinder"><i class="icon icon-close"></i></button>
                    </div>
                </div>
                <div class="tbody">
                    <div class="sidebar">
                        ${menuFolders}
                    </div>
                    <div class="tabsContents" id="listItems">
                     
                            <div class="boxCharge item" data-select="no-select">
                                <div class="boxChargeInner">
                                    <i class="icon icon-upload"></i>
                                    <span class="btn btnSmall btnFull" lang="es">Subir File</span>
                                    <span class="message" lang="es">O arrastra archivos [jpeg,jpg,png,svg,webp] max:${max}</span>
                                </div>
                                <input type="file" data-folder="${primaryFolder}"  class="inputFileFinder" multiple   />
                            </div>
                            ${items}
                    </div>
                </div>
                <div class="tfooter">
                    <div class="actions">
                        <button class="btn btnCloseFinder">Cancel</button>
                        <button class="btn btnPrimary btnInsert disabled" data-for="${id}">Insert</button>
                    </div>
                </div>
            </div>
        </div>
    `;
}