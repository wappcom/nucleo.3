import {
	capitalize,
	emptyReturn
} from "../functions.js";
import {
	resizeWorkspace
} from "../nav.js"

export function renderModalConfirm(vars) {
	let text = emptyReturn(vars.text, "");
	let vari = emptyReturn(vars.vars, "");
	let title = emptyReturn(vars.title, "");
	let icon = emptyReturn(vars.icon, "");
	let fn = emptyReturn(vars.fn, "");
	let fnReturn = emptyReturn(vars.fnReturn, "");
	let fnCancel = emptyReturn(vars.fnCancel, "");
	let varsReturn = emptyReturn(vars.varsReturn, "");
	let id = emptyReturn(vars.id, "");
	let attr = emptyReturn(vars.attr, "");
	let module = emptyReturn(vars.module, "");
	let body = emptyReturn(vars.body, "");
	let classBtnAction = emptyReturn(vars.classBtnAction, "btnPrimary");
	let labelBtnAction = emptyReturn(vars.labelBtnAction, "Confirmar");
	let iconBtnAction = emptyReturn(vars.iconBtnAction, "");
	let cls = vars.cls || "";
	let tbody = "";
	let strTitle = "";

	if (icon != "" && text != "") {
		tbody =
			`<div class="modalBody"> 
					<i class="` +
			icon +
			`"></i> 
					<span>` +
			text +
			`</span> 
				</div>`;
	}

	if (title != "") {
		strTitle = `<div class="modalTitle">` + title + `</div>`;
	}

	return (
		`<div class="modalRender modalConfirm" id="` +
		id +
		`" >
				<div class="modalInner animated bounceIn fast">
					${strTitle}
					${tbody}
					<div class="body">${body}</div>
					<div class="modalFooter">
						<a class="btn btnCancelModal btnSmall btnFull" fn-cancel="${fnCancel}" for="${id}" > Cancelar </a>
						<a class="btn ${cls} ` +
		classBtnAction +
		`  "  module="` +
		module +
		`" fn="` +
		fn +
		`"  fnreturn="` +
		fnReturn +
		`"  varsreturn="` +
		varsReturn +
		`" vars="` +
		vari +
		`" ` +
		attr +
		` >
							<i class=""></i>
							<span>` +
		labelBtnAction +
		` </span>
						</a>
					</div>
				</div>
			</div>`
	);
}

export function renderModalRenameFile(vars) {
	let text = emptyReturn(vars.text, "");
	let vari = emptyReturn(vars.vars, "");
	let title = emptyReturn(vars.title, "");
	let icon = emptyReturn(vars.icon, "");
	let action = emptyReturn(vars.action, "");
	let item = emptyReturn(vars.item, "");

	let classBtnAction = emptyReturn(vars.classBtnAction, "btnPrimary");
	let labelBtnAction = emptyReturn(vars.labelBtnAction, "Volver a Guardar");
	let iconBtnAction = emptyReturn(vars.iconBtnAction, "");
	let arrayVars = vari.split(",");
	let nameFile = arrayVars[0];
	let returnaction = arrayVars[1];
	let id = arrayVars[2];

	$(".modalRender").remove();

	return (
		`<div class="modalRender modalConfirm" id="` +
		id +
		`" >
				<div class="modalInner animated bounceIn fast">
					<div class="modalTitle">` +
		title +
		`</div>
					<div class="modalBody"> 
						<i class="` +
		icon +
		` info"></i> 
						<span>` +
		text +
		`</span> 
					</div>
					<div class="modalControl">
						<input type="text" id="input` +
		id +
		`" value="` +
		nameFile +
		`" />
					</div>
					<div class="modalFooter">
						<a class="btn btnCancelModalRenameFile  btnSmall btnFull" for="` +
		id +
		`" item="` +
		item +
		`" >Cancelar</a>
						<a accion="` +
		action +
		`" class="btn ` +
		classBtnAction +
		`  " vars="` +
		vari +
		`"   for="` +
		id +
		`">
						<i class="${iconBtnAction}"></i><span>` +
		labelBtnAction +
		`</span></a>
					</div>
				</div>
			</div>`
	);
}

export function renderModalRename(vars) {
	let text = emptyReturn(vars.text, "");
	let attr = emptyReturn(vars.attr, "");
	let title = emptyReturn(vars.title, "Renombrar");
	let icon = emptyReturn(vars.icon, "");
	let action = emptyReturn(vars.action, "");
	let item = emptyReturn(vars.item, "");
	let id = emptyReturn(vars.id, "");
	let fn = emptyReturn(vars.fn, "");
	let body = emptyReturn(vars.body, "");

	let classBtnAction = emptyReturn(vars.classBtnAction, "btnPrimary");
	let labelBtnAction = emptyReturn(vars.labelBtnAction, "Renombrar");
	let iconBtnAction = emptyReturn(vars.iconBtnAction, "");

	$(".modalRender").remove();

	return (
		`<div class="modalRender modalConfirm modalRename" id="` +
		id +
		`" >
				<div class="modalInner animated bounceIn fast">
					<div class="modalTitle">` +
		title +
		`</div>
					<div class="modalBody"> 
						<i class="` +
		icon +
		` info"></i> 
						<span>` +
		text +
		`</span> 
					</div>
					<div class="modalControl form">
						<div class="formControl">
							<input class="formInput" type="text" id="input` +
		id +
		`" value="` +
		vars.input +
		`" />
						</div>
						${body}
					</div>
					<div class="modalFooter">
						<a class="btn btnLink btnCancelModalRender  btnSmall btnFull" for="` +
		id +
		`" item="` +
		item +
		`" >Cancelar</a>
						<a class="btn btnModalRename ${classBtnAction}" item="${item}"  fn="${fn}" ` +
		attr +
		`>
							<i class="${iconBtnAction}"></i>
							<span>${labelBtnAction}</span>
						</a>
					</div>
				</div>
			</div>`
	);
}

export function renderModalAlert(vars) {
	var label = vars.label;
	var icono = vars.icono;
	var id = vars.id;
	var classBtnAccion = vars.classBtnAccion;

	return (
		`<div class="modalRender modalAlert" id="` +
		id +
		`" >
				<div class="inner animated bounceIn fast">
					<div class="modal-title"></div>
					<div class="modal-body"> 
						<i class="` +
		icono +
		`"></i> 
						` +
		label +
		` 
					</div>
					<div class="modal-footer">
						<a id="btn-` +
		id +
		`" class="btn btn-cancelar btn-info btn-small" >Aceptar</a>
					</div>
				</div>
			</div>`
	);
}

export function renderModalDelete(vars) {
	//console.log(vars);

	var label =
		`<label><b>"` +
		vars.name +
		`"</b> se eliminará. <br> ¿Estás seguro de eliminarlo? </label><br><span>No podrás deshacer esta acción.</span>`;

	let text = emptyReturn(vars.text, label);

	let classBtnAction = vars.classBtnAction ?
		vars.classBtnAction :
		"btnDeleteConfirm";
	classBtnAction = classBtnAction + " btnDanger";

	let labelBtnAction = vars.labelBtnAction ? vars.labelBtnAction : "Eliminar";

	return renderModalConfirm({
		text,
		id: vars.id,
		vars: vars.vars,
		attr: vars.attr,
		fn: vars.fn,
		fnReturn: vars.fnReturn,
		varsReturn: vars.varsReturn,
		module: vars.module,
		cls: vars.cls,
		labelBtnAction,
		classBtnAction,
		icon: "icon icon-trash iconDanger",
	});
}

export const renderTooltip = (vars = []) => {
	console.log("renderTooltip", vars);
	let selector = vars.selector;
	let type = vars.type ? vars.type : "default";
	let position = vars.position ? vars.position : "top";
	let cls = vars.cls ? vars.cls : "default";
	let arrow = "arrow" + capitalize(position);
	let tp = "tooltip" + capitalize(position);

	return /*html*/ `
		<div class="tooltip ${tp} ${cls}" for="${selector}" type="${type}">
			<div class="tooltipInner">
				${vars.text}
			</div>
			<i class="arrow ${arrow}"></i>
		</div>	
	`;
};

export function renderModalTrash(vars) {
	var label =
		`<label>"` +
		vars.title +
		`" se eliminará, estas seguro de eliminarlo. </label><span>No podrás deshacer esta acción.<span>`;
	return (
		`<div class="modalRender modalConfirm " id="` +
		vars.id +
		`" >
			<div class="inner animated bounceIn fast">
				<div class="modal-title"></div>
				<div class="modal-body"> 
					<i class="icn icn-trash icn-danger"></i> 
					` +
		label +
		` 
				</div>
				<div class="modal-footer">
					<a class="btn btnCancelTrash btn-small btn-full">Cancelar</a>
					<a class="btn btnDeleteTrash btn-danger btn-small" idmod="` +
		vars.idMod +
		`" item="` +
		vars.idItem +
		`"  vars="` +
		vars.vars +
		`" >Eliminar</a>
				</div>
			</div>
		</div>`
	);
}

export function renderModal(vars = []) {
	//console.log("renderModal", vars);
	if (vars == undefined) {
		vars = {
			cls: "",
			item: "",
			classInner: "",
			body: "",
			id: "",
		};
	}

	var cls = vars.cls ? vars.cls : "";
	var item = vars.item ? vars.item : "";
	var id = vars.id ? vars.id : "";
	var classInner = vars.classInner ? vars.classInner : "";
	var body = vars.body ? vars.body : "";
	var btnClose = vars.btnClose ? vars.btnClose : 0;
	let btnCloseStr = `<a class="btn btnCloseModal btnIcon" for="${id}" >
		<i class="icon icon-circle-close"></i>				
	</a>`;
	if (btnClose != 0) {
		btnCloseStr = "";
	}

	return (
		`<div class="modal modalRender ` +
		cls +
		`" item="` +
		item +
		`" id="` +
		id +
		`">
				<div class="modalInner  ${classInner}">${btnCloseStr}${body}</div>
			</div>`
	);
}

export function renderModalConfig(vars) {
	let module = vars.module ? vars.module : "";
	let item = vars.item ? vars.item : "";
	let system = vars.system ? vars.system : "";
	let innerCls = vars.innerCls ? vars.innerCls : "";
	let attr = vars.attr ? vars.attr : "";

	return ` <div class="modalRender modalConfig" module="${module}" item="${item}" ${attr} system="${system}" id="${vars.id}">
                <div class="modalInner ${innerCls}">${vars.body}</div>
				<div class="modalBgBack" for="${vars.id}"></div>
            </div>`;
}

export function renderModalConfigBody(vars) {
	return /*html*/ `
		 <div class="head">
			 <div class="title">
				 <i class="icon icon-conf"></i><h2 class="">${vars.title}</h2>
			 </div>
			 <div class="actions">
				 <button type="button" class="btnModalClose btnIcon btn" for="${vars.id}" system="${vars.system}"><i class="icon icon-close"></i></button>
			 </div>
		 </div>
		 <div class="tbody" for="${vars.id}" system="${vars.system}" >
			 <div class="sidebarMenu" for="${vars.id}">
				  ${vars.sidebar}
			 </div>
			 <div class="contents" id="content${vars.id}">
				 ${vars.body}
			 </div>
		 </div>
	 `;
}

export function renderModalConf(vars) {
	let contentId = "content" + capitalize(vars.id);
	return /*html*/ `
	<div class="modalRender modalConfig" module="${vars.module}" item="${vars.item}" ${vars.attr} system="${vars.system}" id="${vars.id}">
		<div class="modalInner ${vars.innerCls}">
			<div class="head">
				<div class="title">
					<h3 class="">${vars.title}</h3>
				</div>
				<div class="actions">
					<button type="button" class="btnModalClose btnIcon btn" for="${vars.id}" system="${vars.system}"><i class="icon icon-close"></i></button>
				</div>
			</div>
			<div class="tbody" for="${vars.id}" system="${vars.system}" >
				<div class="sidebarMenu" for="${vars.id}">
					<ul class="inner">
					${vars.sidebar}
					</ul>
				</div>
				<div class="content" id="${contentId}">
					${vars.body}
				</div>
			</div>
		</div>
	</div>	
	`;
}

export const innerForm = (vars) => {
	//console.log('innerForm', vars);
	let id = vars.id ? vars.id : "";
	let item = vars.item ? vars.item : "";
	let cls = vars.cls ? vars.cls : "";
	let attr = vars.attr ? vars.attr : "";
	let module = vars.module ? vars.module : "";

	let inner = $(".innerForm");
	if (inner.length) {
		$(".innerForm").html(vars.body);
		// $(".innerForm").addClass("on animated bounceRight fast");
		//$(".innerForm").addClass("on");
	} else {
		if (module) {
			$(".boxModule[module='" + vars.module + "']").prepend(
				`<div class="innerForm" id="${id}" ${attr} item="${item}" >${vars.body}</div>`
			);
		} else {
			$(".ws").prepend(
				`<div class="innerForm" id="${id}" ${attr} item="${item}" >${vars.body}</div>`
			);
		}
		// $(".bodyModule[module='" + vars.module + "'] >.tbody").addClass("off");
	}

	const hBodyModule = $(
		".bodyModule[module='" + vars.module + "']"
	).outerHeight();
	const w = $(window).width();
	const wSidebar = $(
		".boxModule[module='" + vars.module + "'] .sidebarMenuModule"
	).outerWidth();
	const wInnerForm = w - wSidebar;
	//console.log ("wInnerForm ", wInnerForm)
	$(".boxModule[module='" + vars.module + "'] .innerForm").css(
		"height",
		hBodyModule + "px"
	);
	$(".boxModule[module='" + vars.module + "'] .innerForm").css(
		"width",
		wInnerForm + "px"
	);
	$(".boxModule[module='" + vars.module + "'] .innerForm").addClass("on");
	$(".boxModule[module='" + vars.module + "'] .innerForm").addClass(cls);
};

export const removeInnerForm = (vars) => {
	////console.log('removeInnerForm');
	$(".innerForm").removeClass("on");
	setTimeout(function () {
		$(".innerForm").remove();
		$(".bodyModule").removeClass("backOff");
		resizeWorkspace();
	}, 500);
}

export const renderInnerWindow = (vars = []) => {
	//console.log('renderInnerWindow', vars);
	let id = vars.id ? vars.id : "innerWindow";
	let cls = vars.cls ? vars.cls : "";
	let attr = vars.attr ? vars.attr : "";
	let selector = vars.selector ? vars.selector : "body";

	$(selector).prepend(
		`<div class="innerWindow on ${cls}" id="${id}" ${attr}>${vars.content}</div>`
	);
};

export const removeInnerWindow = (selector = "") => {
	$(selector + " .innerWindow").remove();
}

export function renderDeleteModal(vars) {
	var label =
		`<label><b>"` +
		vars.name +
		`"</b> se eliminará. <br> ¿Estás seguro de eliminarlo? </label><br><span>No podrás deshacer esta acción.</span>`;
	let title = vars.title ? vars.title : label;
	let vari = emptyReturn(vars.vars, "");
	let fnCancel = emptyReturn(vars.fnCancel, "");
	let id = emptyReturn(vars.id, "");
	let icon = emptyReturn(vars.icon, "icon icon-trash iconDanger");
	let attr = emptyReturn(vars.attr, "");
	let module = emptyReturn(vars.module, "");
	let body = emptyReturn(vars.body, "");
	let classBtnAction = emptyReturn(
		vars.classBtnAction,
		"btnDeleteConfirmation"
	);
	let cls = emptyReturn(vars.cls, "");
	let labelBtnAction = emptyReturn(vars.labelBtnAction, "Eliminar");
	let tbody = "";
	let strTitle = "";

	if (cls != "") {
		classBtnAction = cls;
	}

	return (
		`<div class="modalRender modalConfirm modalDelete" id="` +
		id +
		`" >
				<div class="modalInner animated bounceIn fast">
					<div class="modalBody"> 
						<i class="${icon}"></i> 
						<div class="title">` +
		title +
		`</div> 
					</div>
					<div class="body">${body}</div>
					<div class="modalFooter">
						<a class="btn btnCancelModalRender btnSmall btnFull" fn-cancel="${fnCancel}">Cancelar</a>
						<a  class="btn btnDanger ` +
		classBtnAction +
		`  "  module="` +
		module +
		`" vars="` +
		vari +
		`" ` +
		attr +
		` >` +
		labelBtnAction +
		`</a>
					</div>
				</div>
			</div>`
	);
}

export function renderDeleteModalFn(vars) {
	let icon = emptyReturn(vars.icon, "icon icon-trash iconDanger");
	let label = `<label><b>${vars.name}</b> se eliminará. <br> ¿Estás seguro de eliminarlo? </label><br><span>No podrás deshacer esta acción.</span>`;
	let title = vars.title ? vars.title : label;
	let body = vars.body ? `<div class="body">${vars.body}</div>` : "";
	let titleBtn = vars.titleBtn ? vars.titleBtn : "Eliminar";
	return (
		`<div class="modalRender modalConfirm modalDelete" id="${vars.id}" >
				<div class="modalInner animated bounceIn fast">
					<div class="modalBody"> 
						<i class="${icon}"></i> 
						<div class="title">${title}</div> 
					</div>
					${body}
					<div class="modalFooter">
						<a class="btn btnCancelModalRender btnSmall btnFull" ${vars.attr}>Cancelar</a>
						<a  class="btn btnDanger btnDeleteConfirmation${capitalize(vars.module)}" ${vars.attr}>${titleBtn}</a>
					</div>
				</div>
			</div>`
	);
}

export function renderDeleteOrRemove(vars) {
	var label =
		`<label><b>"` +
		vars.name +
		`"</b><br> ¿Estás seguro de eliminarlo? </label><br><span>No podrás deshacer esta acción. Remover solo quitara la imagen sin eliminarla.</span>`;
	let title = vars.title ? vars.title : label;
	let vari = emptyReturn(vars.vars, "");
	let fnCancel = emptyReturn(vars.fnCancel, "");
	let id = emptyReturn(vars.id, "");
	let icon = emptyReturn(vars.icon, "icon icon-trash iconDanger");
	let attr = emptyReturn(vars.attr, "");
	let module = emptyReturn(vars.module, "");
	let body = emptyReturn(vars.body, "");
	let classBtnAction = emptyReturn(vars.classBtnAction, "btnDeleteAction");
	let classBtnRemove = emptyReturn(vars.classBtnRemove, "btnRemove");
	let labelBtnAction = emptyReturn(vars.labelBtnAction, "Eliminar");
	let labelBtnRemove = emptyReturn(vars.labelBtnRemove, "Remover");
	let tbody = "";
	let strTitle = "";

	return (
		`<div class="modalRender modalConfirm modalDelete" id="` +
		id +
		`" >
				<div class="modalInner animated bounceIn fast">
					<div class="modalBody"> 
						<i class="${icon}"></i> 
						<div class="title">` +
		title +
		`</div> 
					</div>
					<div class="body">${body}</div>
					<div class="modalFooter">
						<a class="btn btnCancelModalRender btnSmall btnFull" fn-cancel="${fnCancel}">Cancelar</a>
						<a class="btn btnPrimary  ${classBtnRemove}"` +
		attr +
		`>` +
		labelBtnRemove +
		` </a>
						<a class="btn btnDanger  ${classBtnAction}"` +
		attr +
		`>` +
		labelBtnAction +
		` </a>
					</div>
				</div>
			</div>`
	);
}

export const renderModalClean = (vars = []) => {
	let title = vars.title || "Title";
	let attr = vars.attr || "";
	let cls = vars.cls || "";
	let clsHeader = vars.clsHeader || "";
	let btns = vars.btns || "";
	return /*html*/ `
		<div class="modal modalRender ${cls}" ${attr} id="${vars.id}">
            <div class="modalInner">
                <div class="modalHeader ${clsHeader}">
                    <h3 class="title">${title}</h3>
                    <div class="actions">
						${btns}
                        <a class="btn btnIcon btnModalClose" for="${vars.id}">
                            <i class="icon icon-close"></i>
                        </a>
                    </div>
                </div>
                <div class="modalBody">
                    ${vars.body}
                </div>
            </div>
        </div>
	`;
};

export const renderConfirmVersion = (vars = []) => {
	//console.log('renderConfirmVersion',vars);
	let modalRender = $("#confirmVersionModal");
	// revisar si existe un id
	if (modalRender.length == 0) {
		return /*html*/ `
		<div class="modal modalRender" id="confirmVersionModal">
			<div class="modalInner modalInnerMini">
				<div class="modalBody">
					<i class="icon icon-refresh"></i>
					<span>Existe una nueva versión del sistema. v${vars.vs}</span>
				</div>
				<div class="modalFooter">
					<button class="btn btnPrimary btnSmall" id="btnUpdateVersionSystem">Actualizar</button>
				</div>
			</div>
		</div>	
		`;
	}
};

export const btnSidebarModalBtn = (vars = []) => {
	//console.log('btnSidebarModalBtn',vars);
	let cls = vars.cls ? vars.cls : "btnMenuModal";
	let icon = vars.icon ? vars.icon : "icon icon-conf";
	let label = vars.label ? vars.label : "Label";
	let state = vars.state ? vars.state : "";
	let module = vars.module ? vars.module : "";
	let modalId = vars.modalId ? vars.modalId : "";
	let dataFor = vars.dataFor ? vars.dataFor : "";
	let dataContent = vars.dataContent ? vars.dataContent : "";
	let attr = vars.attr ? vars.attr : "";

	return /*html*/ `
		<a class="btn btnIconLeft ${cls} ${state}" ${attr} data-module="${module}" data-modal-id="${modalId}" data-for="${dataFor}" data-content="${dataContent}">
        	<i class="${icon}"></i>
        	<span lang="es">${label}</span>
        </a>
	`;
}

export const renderContextMenu = (vars =[]) => {
	console.log('renderContextMenu',vars);
	let body = vars.body ? vars.body : "";
	let array = vars.array ? vars.array : [];
	let module = vars.module ? vars.module : "";

	if (array.length > 0) {
		array.forEach((item) => {
			console.log('item',item);
			if (item.type == "btn"){
				body += `<a class="btn btnIconLeft ${item.cls}" ${item.attr} data-module="${module}" >
					<i class="${item.icon}"></i>
					<span lang="es">${item.label}</span>
				</a>`;
			}

			if (item.type == "link"){
				body += `<a class="btn btnIconLeft ${item.cls}" ${item.attr} data-module="${module}" >
					<i class="${item.icon}"></i>
					<span lang="es">${item.label}</span>
				</a>`;
			}

			if (item.type == "separator"){
				body += `<div class="separator"></div>`;
			}

		});
	}
	return /*html*/`
		<div class="modalContextMenu ${vars.cls}" id="${vars.id}">
			<div class="inner">
				${body}
			</div>
		</div>
	`;
}