import {
	emptyReturn
} from '../functions.js';

export const renderInputFileMultiUpload = (vars = []) => {
	let cls = vars.cls || '';
	let title = vars.title || 'Media';
	let validfiles = vars.validfiles || 'jpeg,jpg,png,mp4,gif,mp3,svg,webp';
	let max = vars.max || 'max. 2MB';
	let id = vars.id || '';
	let folder = vars.folder || '';
	let mode = vars.mode || 'multiple';
	let system = vars.system || '';
	let module = vars.module || '';

	return `
	<div class="formControl ${cls}">
		<div class="head">
			<label for="">${title}</label>
			<div class="actions" data-for="${id}">
				<a class="btn btnMini btnFull btnFinder" data-folder="${folder}" data-mode="${mode}" data-validfiles="${validfiles}" data-for="${id}">Finder</a>
			</div>
		</div>
		<div class="formUploadMedia">
			<div class="boxMedia" data-for="${id}" date-mode="${mode}"></div>
			<div class="boxCharge">
				<div class="charge">
					<i class="icon icon-upload"></i>
					<span class="btn btnFull" lang="es">Agrega Archivos</span>
					<span class="message" lang="es">O arrastra archivos [${validfiles}] ${max}</span>
				</div>
				<div class="boxInputFile ui-state-disabled">
					<input type="file" id="${id}" class="btnUploadFile" multiple="multiple"
					data-mode="${mode}" data-folder="${folder}" data-validfiles="${validfiles}"
					data-system="${system}" data-module="${module}" />
				</div>
			</div>
		</div>
	</div>
	`;
}

export const renderItemVideo = (vars =[]) => {
	//console.log('renderItemVideo',vars);
	let fileId = vars.item || '';
	let pathFile = vars.pathFile || '';
	let inputName = vars.inputName || '';
	let id = vars.id || '';

	return /*html*/`
	<div class="item item-video" item="${fileId}">
		<a class="btnRemoveFileItem" data-for="${fileId}" data-id="${id}" >
			<i class="icon icon-circle-close"></i>
		</a>
		<video width="100%" height="180" controls>
		<source src="${pathFile}" type="video/mp4">
			Tu navegador no soporta el tag de video.
		</video>
	</div>
	`;
}

export const renderItemImage = (vars =[]) => {
	//console.log('renderItemVideo',vars);
	let fileId = vars.item || '';
	let pathFile = vars.pathFile || '';
	let inputName = vars.inputName || '';
	let id = vars.id || '';

	let inputFileId = $(`input[data-name="${id}"]`).attr('id');
	console.log('inputFileId',inputFileId);
	$(`.inner[for="${inputFileId}"]`).addClass('off');

	return /*html*/`
	<div class="item item-imagen" item="${fileId}">
		<a class="btnRemoveFileItem" data-for="${inputName}" data-id="${id}" >
			<i class="icon icon-circle-close"></i>
		</a>
		<img src="${pathFile}" width="100%"/>
	</div>
	`;
}

export const renderItemSvg = (vars =[]) => {
	//console.log('renderItemVideo',vars);
	let fileId = vars.item || '';
	let pathFile = vars.pathFile || '';
	let inputName = vars.inputName || '';
	let id = vars.id || '';

	return /*html*/`
	<div class="item item-imagen item-svg" item="${fileId}">
		<a class="btnRemoveFileItem" data-for="${fileId}" data-id="${id}" >
			<i class="icon icon-circle-close"></i>
		</a>
		<img src="${pathFile}" width="100%"/>
	</div>
	`;
}

export function rendermultipleFiles() {
	return `<i class="icon icon-upfile"></i>
			<span class="btn btnFull">Agrega Archivos</span>
			<span class="message">O arrastra archivos [jpg,png,gif,svg] max.2MB</span>`;
}
export function rendersimpleFile() {
	return `<i class="icon icon-upfile"></i>
			<span class="btn btnFull">Agrega Archivos</span>
			<span class="message">O arrastra archivos [jpg,png,gif,svg] max.2MB</span>`;
}

export function rendermultipleFilesHover() {
	return `<i class="icon icon-drag"></i>
			<span class="message on" style="">Suelta el Archivo para cargar</span>`;
}

export function rendersimpleFileHover() {
	return `<i class="icon icon-drag"></i>
			<span class="message on" style="">Suelta el Archivo para cargar</span>`;
}

export function renderHelperText(vars = []) {
	let message = vars.message ? vars.message : '';
	let cls = vars.cls ? vars.cls : 'messageAlert';
	$(vars.elem).html(`<div class="message  ${cls}">${message}</div>`);
}

export function renderBoxMedia(vars) {
	console.log("renderBoxMedia", vars);
	return (
		`<div class="item item${vars.index} itemMedia ui-state-default" name="${vars.name}" select="" item="${vars.item}">
				<div class="actions">
					<a class="btnOrderMedia handle" item="${vars.item}"><i class="icon icon-grid-compac"></i></a>
					<a class="btnSelectMedia" data-module="${vars.module}"  item="${vars.item}" state="0"><i class="icon icon-checked-o"></i></a>
					<a class="btnEditMedia"
					data-src = "${vars.img}"
					data-id = "${vars.id}"
					item="${vars.item}"> <i class ="icon icon-pencil"></i></a>
				</div>
				<div class="bg"></div>
				<div class="img"><img src='` +
		vars.img +
		`' /></div>
			</div>`
	);
}

export function renderBoxImage(vars) {
	//console.log("renderBoxImage", vars);
	let img = emptyReturn(vars.img, 0);
	if (img == 0) {
		img = "no hay imagen para mostrar";
	} else {
		img = img.replaceAll(_PATH_FILES, "");
		img = `<img src='` + _PATH_FILES + img + `' />`;
	}
	return (
		`<div class="item item${vars.index} itemMedia ui-state-default" select="" name="${vars.name}"  item="${vars.item}">
				<div class="actions">
					<a class="btnTrashImg" module="forms" fn="removeFileId"  vars="${vars.item}" data-module="${vars.module}" data-name="${vars.name}" data-id="${vars.item}"><i class="icon icon-trash"></i></a>
					<a class="btnEditImage" item="${vars.item}"><i class="icon icon-pencil"></i></a>
				</div>
				<div class="bg"></div>
				<div class="img">${img}</div>
			</div>`
	);
}

export function renderNumItemsHead(vars) {
	console.log("renderNumItemsHead", vars);
	return `<div class="box boxAllSelectedItems">
				<a class="btnSelectedAllItems" for="${vars.for}">Seleccionar todos</a>
			</div>
			<div class="box boxNumsItems">
				<a class="btnRemoveItems"  title="Remote items" for="${vars.for}"><i class="icon icon-square-minus"></i></a>
				<div class="numItems" numitems="0"></div>
			</div>
			<div class="box boxNumsItems">
				<a class="btnTrashItems" title="Delete items" data-item ="${vars.item}" data-module="${vars.module}"  for="${vars.for}"><i class="icon icon-trash"></i></a>
				<div class="numItems" numitems="0"></div>
			</div>`
}

export function rendermultipleFilesMini(vars) {
	return `<div class="boxMediaMini">
				<span class="btn btnLink">Add Media</span>
				<span class="message">o arrastra los archivos a cargar</span>
			</div>`;
}

export function rendersimpleFileMini(vars) {
	return `<div class="boxMediaMini">
				<span class="btn btnLink">Add Media</span>
				<span class="message">o arrastra los archivos a cargar</span>
			</div>`;
}

export function renderBtnStateSwitch(vars) {
	////console.log(vars);

	const title = vars.title ? vars.title : "label";
	const id = vars.id ? vars.id : "default";
	const fn = vars.fn ? vars.fn : "default";
	const state = vars.state ? vars.state : "0";
	const stateBody = vars.stateBody ? vars.stateBody : "";

	return /*html*/ `
			<div class="boxBtnStateSwitch">
				<label class="title">${title}</label>
				<a class="btnStateSwitch" item="${id}" state="${state}" fn="${fn}">
					<div class="circle"></div>
					<div class="light"></div>
					<div class="base"></div>
				</a>
				<input type="hidden" value="0" name="inputState${id}" id="inputState${id}" />
				<div class="state" item="${id}">${stateBody}</div>
			</div>
		`;
}

export function renderSelectOptions(values = []) {
	let optionInitialValue = values.optionInitialValue ?
		values.optionInitialValue :
		0;
	let optionInitialLabel = values.optionInitialLabel ?
		values.optionInitialLabel :
		"Seleccionar";
	let options = values.options ? values.options : [];
	let selectedDefaultValue = values.selectedDefaultValue ?
		values.selectedDefaultValue :
		0;

	let html = `<option value="${optionInitialValue}">${optionInitialLabel}</option>`;

	for (let i = 0; i < options.length; i++) {
		const name = options[i].name;
		const id = options[i].id;
		let aux = "";
		if (selectedDefaultValue == id) {
			aux = "selected";
		}
		html += `<option value="${id}" ${aux}>${name}</option>`;
	}

	////console.log(html)
	return html;
}

export const renderSelectHoursStartEnd = (vars) => {
	var id = vars.id ? vars.id : "";
	var attr = vars.attr ? vars.attr : "";
	var hourStart = vars.hourStart ? vars.hourStart : "00:00";
	var hourEnd = vars.hourEnd ? vars.hourEnd : "01:00";
	return /*html*/ `
		<div class="selectHoursStartEnd" item="${id}" ${attr} >
			<div class="hourStart">
				<div class="listHours">
					<a
						class="btnListHours"
						for="inputHourStart${id}"
						>${hourStart}</a
					>
					<div
						class="list scrollBar"
						type = "start"
						item="${id}"
						for="inputHourStart${id}"
					>
						<a
							class="btnHour active"
							value="00:00"
							>00:00</a
						><a
							class="btnHour"
							value="00:15"
							>00:15</a
						><a
							class="btnHour"
							value="00:30"
							>00:30</a
						><a
							class="btnHour"
							value="00:45"
							>00:45</a
						><a
							class="btnHour"
							value="01:00"
							>01:00</a
						><a
							class="btnHour"
							value="01:15"
							>01:15</a
						><a
							class="btnHour"
							value="01:30"
							>01:30</a
						><a
							class="btnHour"
							value="01:45"
							>01:45</a
						><a
							class="btnHour"
							value="02:00"
							>02:00</a
						><a
							class="btnHour"
							value="02:15"
							>02:15</a
						><a
							class="btnHour"
							value="02:30"
							>02:30</a
						><a
							class="btnHour"
							value="02:45"
							>02:45</a
						><a
							class="btnHour"
							value="03:00"
							>03:00</a
						><a
							class="btnHour"
							value="03:15"
							>03:15</a
						><a
							class="btnHour"
							value="03:30"
							>03:30</a
						><a
							class="btnHour"
							value="03:45"
							>03:45</a
						><a
							class="btnHour"
							value="04:00"
							>04:00</a
						><a
							class="btnHour"
							value="04:15"
							>04:15</a
						><a
							class="btnHour"
							value="04:30"
							>04:30</a
						><a
							class="btnHour"
							value="04:45"
							>04:45</a
						><a
							class="btnHour"
							value="05:00"
							>05:00</a
						><a
							class="btnHour"
							value="05:15"
							>05:15</a
						><a
							class="btnHour"
							value="05:30"
							>05:30</a
						><a
							class="btnHour"
							value="05:45"
							>05:45</a
						><a
							class="btnHour"
							value="06:00"
							>06:00</a
						><a
							class="btnHour"
							value="06:15"
							>06:15</a
						><a
							class="btnHour"
							value="06:30"
							>06:30</a
						><a
							class="btnHour"
							value="06:45"
							>06:45</a
						><a
							class="btnHour"
							value="07:00"
							>07:00</a
						><a
							class="btnHour"
							value="07:15"
							>07:15</a
						><a
							class="btnHour"
							value="07:30"
							>07:30</a
						><a
							class="btnHour"
							value="07:45"
							>07:45</a
						><a
							class="btnHour"
							value="08:00"
							>08:00</a
						><a
							class="btnHour"
							value="08:15"
							>08:15</a
						><a
							class="btnHour"
							value="08:30"
							>08:30</a
						><a
							class="btnHour"
							value="08:45"
							>08:45</a
						><a
							class="btnHour"
							value="09:00"
							>09:00</a
						><a
							class="btnHour"
							value="09:15"
							>09:15</a
						><a
							class="btnHour"
							value="09:30"
							>09:30</a
						><a
							class="btnHour"
							value="09:45"
							>09:45</a
						><a
							class="btnHour"
							value="10:00"
							>10:00</a
						><a
							class="btnHour"
							value="10:15"
							>10:15</a
						><a
							class="btnHour"
							value="10:30"
							>10:30</a
						><a
							class="btnHour"
							value="10:45"
							>10:45</a
						><a
							class="btnHour"
							value="11:00"
							>11:00</a
						><a
							class="btnHour"
							value="11:15"
							>11:15</a
						><a
							class="btnHour"
							value="11:30"
							>11:30</a
						><a
							class="btnHour"
							value="11:45"
							>11:45</a
						><a
							class="btnHour"
							value="12:00"
							>12:00</a
						><a
							class="btnHour"
							value="12:15"
							>12:15</a
						><a
							class="btnHour"
							value="12:30"
							>12:30</a
						><a
							class="btnHour"
							value="12:45"
							>12:45</a
						><a
							class="btnHour"
							value="13:00"
							>13:00</a
						><a
							class="btnHour"
							value="13:15"
							>13:15</a
						><a
							class="btnHour"
							value="13:30"
							>13:30</a
						><a
							class="btnHour"
							value="13:45"
							>13:45</a
						><a
							class="btnHour"
							value="14:00"
							>14:00</a
						><a
							class="btnHour"
							value="14:15"
							>14:15</a
						><a
							class="btnHour"
							value="14:30"
							>14:30</a
						><a
							class="btnHour"
							value="14:45"
							>14:45</a
						><a
							class="btnHour"
							value="15:00"
							>15:00</a
						><a
							class="btnHour"
							value="15:15"
							>15:15</a
						><a
							class="btnHour"
							value="15:30"
							>15:30</a
						><a
							class="btnHour"
							value="15:45"
							>15:45</a
						><a
							class="btnHour"
							value="16:00"
							>16:00</a
						><a
							class="btnHour"
							value="16:15"
							>16:15</a
						><a
							class="btnHour"
							value="16:30"
							>16:30</a
						><a
							class="btnHour"
							value="16:45"
							>16:45</a
						><a
							class="btnHour"
							value="17:00"
							>17:00</a
						><a
							class="btnHour"
							value="17:15"
							>17:15</a
						><a
							class="btnHour"
							value="17:30"
							>17:30</a
						><a
							class="btnHour"
							value="17:45"
							>17:45</a
						><a
							class="btnHour"
							value="18:00"
							>18:00</a
						><a
							class="btnHour"
							value="18:15"
							>18:15</a
						><a
							class="btnHour"
							value="18:30"
							>18:30</a
						><a
							class="btnHour"
							value="18:45"
							>18:45</a
						><a
							class="btnHour"
							value="19:00"
							>19:00</a
						><a
							class="btnHour"
							value="19:15"
							>19:15</a
						><a
							class="btnHour"
							value="19:30"
							>19:30</a
						><a
							class="btnHour"
							value="19:45"
							>19:45</a
						><a
							class="btnHour"
							value="20:00"
							>20:00</a
						><a
							class="btnHour"
							value="20:15"
							>20:15</a
						><a
							class="btnHour"
							value="20:30"
							>20:30</a
						><a
							class="btnHour"
							value="20:45"
							>20:45</a
						><a
							class="btnHour"
							value="21:00"
							>21:00</a
						><a
							class="btnHour"
							value="21:15"
							>21:15</a
						><a
							class="btnHour"
							value="21:30"
							>21:30</a
						><a
							class="btnHour"
							value="21:45"
							>21:45</a
						><a
							class="btnHour"
							value="22:00"
							>22:00</a
						><a
							class="btnHour"
							value="22:15"
							>22:15</a
						><a
							class="btnHour"
							value="22:30"
							>22:30</a
						><a
							class="btnHour"
							value="22:45"
							>22:45</a
						><a
							class="btnHour"
							value="23:00"
							>23:00</a
						><a
							class="btnHour"
							value="23:15"
							>23:15</a
						><a
							class="btnHour"
							value="23:30"
							>23:30</a
						><a
							class="btnHour"
							value="23:45"
							>23:45</a
						>
						 
					</div>
					<input
						${attr} 
						type="hidden"
						name="inputHourStart"
						id="inputHourStart${id}"
						value="${hourStart}"
					/>
				</div>
			</div>
			<span class="bar">-</span>
			<div class="hourEnd">
				<div class="listHours">
					<a
						class="btnListHours"
						for="inputHourEnd${id}"
						>${hourEnd}</a
					>
					<div
						class="list scrollBar"
						type="end"
						item="${id}"
						for="inputHourEnd${id}"
					>
						<a
							class="btnHour"
							value="00:00"
							>00:00</a
						><a
							class="btnHour"
							value="00:15"
							>00:15</a
						><a
							class="btnHour"
							value="00:30"
							>00:30</a
						><a
							class="btnHour"
							value="00:45"
							>00:45</a
						><a
							class="btnHour"
							value="01:00"
							>01:00</a
						><a
							class="btnHour"
							value="01:15"
							>01:15</a
						><a
							class="btnHour"
							value="01:30"
							>01:30</a
						><a
							class="btnHour"
							value="01:45"
							>01:45</a
						><a
							class="btnHour"
							value="02:00"
							>02:00</a
						><a
							class="btnHour"
							value="02:15"
							>02:15</a
						><a
							class="btnHour"
							value="02:30"
							>02:30</a
						><a
							class="btnHour"
							value="02:45"
							>02:45</a
						><a
							class="btnHour"
							value="03:00"
							>03:00</a
						><a
							class="btnHour"
							value="03:15"
							>03:15</a
						><a
							class="btnHour"
							value="03:30"
							>03:30</a
						><a
							class="btnHour"
							value="03:45"
							>03:45</a
						><a
							class="btnHour"
							value="04:00"
							>04:00</a
						><a
							class="btnHour"
							value="04:15"
							>04:15</a
						><a
							class="btnHour"
							value="04:30"
							>04:30</a
						><a
							class="btnHour"
							value="04:45"
							>04:45</a
						><a
							class="btnHour"
							value="05:00"
							>05:00</a
						><a
							class="btnHour"
							value="05:15"
							>05:15</a
						><a
							class="btnHour"
							value="05:30"
							>05:30</a
						><a
							class="btnHour"
							value="05:45"
							>05:45</a
						><a
							class="btnHour"
							value="06:00"
							>06:00</a
						><a
							class="btnHour"
							value="06:15"
							>06:15</a
						><a
							class="btnHour"
							value="06:30"
							>06:30</a
						><a
							class="btnHour"
							value="06:45"
							>06:45</a
						><a
							class="btnHour"
							value="07:00"
							>07:00</a
						><a
							class="btnHour"
							value="07:15"
							>07:15</a
						><a
							class="btnHour"
							value="07:30"
							>07:30</a
						><a
							class="btnHour"
							value="07:45"
							>07:45</a
						><a
							class="btnHour"
							value="08:00"
							>08:00</a
						><a
							class="btnHour"
							value="08:15"
							>08:15</a
						><a
							class="btnHour"
							value="08:30"
							>08:30</a
						><a
							class="btnHour"
							value="08:45"
							>08:45</a
						><a
							class="btnHour"
							value="09:00"
							>09:00</a
						><a
							class="btnHour"
							value="09:15"
							>09:15</a
						><a
							class="btnHour"
							value="09:30"
							>09:30</a
						><a
							class="btnHour"
							value="09:45"
							>09:45</a
						><a
							class="btnHour"
							value="10:00"
							>10:00</a
						><a
							class="btnHour"
							value="10:15"
							>10:15</a
						><a
							class="btnHour"
							value="10:30"
							>10:30</a
						><a
							class="btnHour"
							value="10:45"
							>10:45</a
						><a
							class="btnHour"
							value="11:00"
							>11:00</a
						><a
							class="btnHour"
							value="11:15"
							>11:15</a
						><a
							class="btnHour"
							value="11:30"
							>11:30</a
						><a
							class="btnHour"
							value="11:45"
							>11:45</a
						><a
							class="btnHour"
							value="12:00"
							>12:00</a
						><a
							class="btnHour"
							value="12:15"
							>12:15</a
						><a
							class="btnHour"
							value="12:30"
							>12:30</a
						><a
							class="btnHour"
							value="12:45"
							>12:45</a
						><a
							class="btnHour"
							value="13:00"
							>13:00</a
						><a
							class="btnHour"
							value="13:15"
							>13:15</a
						><a
							class="btnHour"
							value="13:30"
							>13:30</a
						><a
							class="btnHour"
							value="13:45"
							>13:45</a
						><a
							class="btnHour"
							value="14:00"
							>14:00</a
						><a
							class="btnHour"
							value="14:15"
							>14:15</a
						><a
							class="btnHour"
							value="14:30"
							>14:30</a
						><a
							class="btnHour"
							value="14:45"
							>14:45</a
						><a
							class="btnHour"
							value="15:00"
							>15:00</a
						><a
							class="btnHour"
							value="15:15"
							>15:15</a
						><a
							class="btnHour"
							value="15:30"
							>15:30</a
						><a
							class="btnHour"
							value="15:45"
							>15:45</a
						><a
							class="btnHour"
							value="16:00"
							>16:00</a
						><a
							class="btnHour"
							value="16:15"
							>16:15</a
						><a
							class="btnHour"
							value="16:30"
							>16:30</a
						><a
							class="btnHour"
							value="16:45"
							>16:45</a
						><a
							class="btnHour"
							value="17:00"
							>17:00</a
						><a
							class="btnHour"
							value="17:15"
							>17:15</a
						><a
							class="btnHour"
							value="17:30"
							>17:30</a
						><a
							class="btnHour"
							value="17:45"
							>17:45</a
						><a
							class="btnHour"
							value="18:00"
							>18:00</a
						><a
							class="btnHour"
							value="18:15"
							>18:15</a
						><a
							class="btnHour"
							value="18:30"
							>18:30</a
						><a
							class="btnHour"
							value="18:45"
							>18:45</a
						><a
							class="btnHour"
							value="19:00"
							>19:00</a
						><a
							class="btnHour"
							value="19:15"
							>19:15</a
						><a
							class="btnHour"
							value="19:30"
							>19:30</a
						><a
							class="btnHour"
							value="19:45"
							>19:45</a
						><a
							class="btnHour"
							value="20:00"
							>20:00</a
						><a
							class="btnHour"
							value="20:15"
							>20:15</a
						><a
							class="btnHour"
							value="20:30"
							>20:30</a
						><a
							class="btnHour"
							value="20:45"
							>20:45</a
						><a
							class="btnHour"
							value="21:00"
							>21:00</a
						><a
							class="btnHour"
							value="21:15"
							>21:15</a
						><a
							class="btnHour"
							value="21:30"
							>21:30</a
						><a
							class="btnHour"
							value="21:45"
							>21:45</a
						><a
							class="btnHour"
							value="22:00"
							>22:00</a
						><a
							class="btnHour"
							value="22:15"
							>22:15</a
						><a
							class="btnHour"
							value="22:30"
							>22:30</a
						><a
							class="btnHour"
							value="22:45"
							>22:45</a
						><a
							class="btnHour"
							value="23:00"
							>23:00</a
						><a
							class="btnHour"
							value="23:15"
							>23:15</a
						><a
							class="btnHour"
							value="23:30"
							>23:30</a
						><a
							class="btnHour"
							value="23:45"
							>23:45</a
						>
						
					</div>
					<input
						${attr} 
						type="hidden"
						name="inputHourEnd"
						id="inputHourEnd${id}"
						value="${hourEnd}"
					/>
				</div>
			</div>
		</div>
	`;
};

export const renderSelect = (vars) => {
	////console.log(vars);
	const array = vars.array;
	const id = vars.id;
	const cls = vars.cls ? vars.cls : "";
	const selectedValue = vars.selected ? vars.selected : "0";
	const labelInit = vars.labelInit ? vars.labelInit : "Seleccionar";
	const attr = vars.attr ? vars.attr : "";

	let selected = "";

	let str = `<select class="formInput ${cls}" ${attr} id="${id}" name="${id}">`;
	if (selectedValue == 0) {
		selected = "selected";
	}
	str += `<option value="0" ${selected} >${labelInit}</option>`;
	for (let i = 0; i < array.length; i++) {
		const item = array[i];
		const value = item.id;
		const label = item.name;
		if (selectedValue == value) {
			selected = "selected";
		} else {
			selected = "";
		}
		str += `<option value="${value}" ${selected} >${label}</option>`;
	}
	str += `</select>`;
	return str;
};

export const renderCheckBox = (vars) => {
	//console.log("renderCheckBox", vars);
	const array = vars.array === undefined ? "" : vars.array;
	const attr = vars.attr === undefined ? "" : vars.attr;

	const cls = vars.cls ? vars.cls : "";
	let str = "";
	let checked = "";

	//console.log(array);

	if (array !== "") {
		for (let i = 0; i < array.length; i++) {
			const item = array[i];
			const id = item.id;
			const name = item.name;
			const value = item.value;
			const label = item.label ? item.label : "";
			const checked = item.checked === undefined ? "" : item.checked;

			str += `<div class="checkbox checkboxControl">
							<input
								${attr} 
								${checked} 
								class="${vars.cls}"
								type="checkbox"
								id="${vars.id}-${i}"
								data-id="${id}"
								name="${vars.id}[]"
								data-name="${name}"
								data-value="${value}"
								value="${id}"
							/>
							<label class="checkboxControlLabel" for="${id}">
								${label}
								${name}
							</label>
					</div>`;
		}
	}

	return str;
};

export const renderRadioButton = (vars) => {
	//console.log("renderRadioButton = (vars", vars);
	const array = vars.array === undefined ? "" : vars.array;
	const attr = vars.attr === undefined ? "" : vars.attr;

	const cls = vars.cls ? vars.cls : "";
	let str = "";
	let checked = "";
	const id = vars.id === undefined ? "" : vars.id;

	let checkedValue = vars.checkedValue;

	//console.log(array, checkedValue);

	if (array !== "") {
		for (let i = 0; i < array.length; i++) {
			const item = array[i];
			//console.log("item",item, item.value, item.label);
			const value = item.value ? item.value : item.id;
			const label = item.label ? item.label : item.name;
			const checked = item.checked === undefined ? "" : item.checked;
			let checkedVal = "";
			if (checkedValue == value) {
				checkedVal = "checked";
			}
			str += `<div class="formCheck">
							<input
								${attr} 
								${checked} 
								${checkedVal} 
								class="formCheckInput"
								type="radio"
								id="${id + i}"
								name="${id}"
								value="${value}"
							/>
							<label class="formCheckLabel" for="${id + i}">
								${label}
							</label>
					</div>`;
		}
	}

	return str;
};

export const renderEditMedia = (vars = []) => {
	//console.log('renderEditMediaForm', vars);
	const id = vars.id;
	const name = vars.name;
	const description = vars.description;
	const pathurl = vars.pathurl;
	const urlFile = _PATH_FILES + vars.pathurl;
	const ext = vars.ext;
	const filename = vars.filename;
	const datatime = vars.datatime;

	let strMedia = "";

	if (ext == "jpeg" || ext == "png" || ext == "gif" || ext == "jpg" || ext == "svg") {
		strMedia = `<img src="${urlFile}" alt="${name}" class="img-fluid" />`;
	}

	return /*html*/ `
		<div class="modalFormEditMedia" id="${id}">
			
			<div class="body">
				<div class="col col-1 col-media">
					<div class="header">
						<h2>Editar Media</h2>
					</div>
					<div class="media">
						${strMedia}
					</div>
					<div class="data">
						<p><b lang="es">Nombre:</b> ${name}</p>
						<p><b lang="es">Descripción:</b> ${description}</p>
						<p><b lang="es">PathUrl:</b> ${pathurl}</p>
						<p><b lang="es">Filename:</b> ${filename}</p>
						<p><b lang="es">Ext:</b> ${ext}</p>
						<p><b lang="es">Id:</b> ${id}</p>
						<p><b lang="es">Fecha de registro:</b> ${datatime}</p>
					</div>
				</div>
				<div class= "col col-2 col-form">
				</div>
			</div>	
		</div>	
	`;
}

export const renderItemInputContent = (vars) => {
	////console.log('renderItemInputContent', vars);

	return `
	<div class="item" value="${vars.value}">
		<span>${vars.name}</span>
		<a class="btnRemoveItemSelect" item="${vars.value}" for="${vars.id}">
			<i class="icon icon-circle-close"></i>
		</a>
	</div>
	`;
}

export const renderInputMedia = (vars) => {
	let id = vars.id;
	let title = vars.title || "Media";
	let validfiles = vars.validfiles || "jpg,jpeg,webp,png,mp4";
	let folder = vars.folder || "inventory/media";
	let module = vars.module || "";
	let system = vars.system || "";
	let cls = vars.cls || "";
	return `
	<div class="formControl ${cls}" for="${id}">
		<div class="head">
			<label for="">${title}</label>
			<div class="actions" for="${id}"></div>
		</div>
		<div class="formGroup">
			<div class="formChargeMedia" for="${id}">
				<div class="boxInputFile ui-state-disabled">
					<div class="boxInner">
						<i class="icon icon-upfile"></i>
						<span class="btn btnFull">Agrega Archivos</span>
						<span class="message">O arrastra archivos [${validfiles}] max.2MB</span>
					</div>
					<input type="file" title="" class="inputFileBtn" multiple="multiple"
						mode="multipleFiles" folder="${folder}" validfiles="${validfiles}"
						name="${id}" id="${id}" system="${system}"
						module="${module}" />
					<div class="boxInputs"></div>
				</div>
			</div>
		</div>
	</div>
	`;
}

export const renderInputQuantity = (vars) => {
	let id = vars.id || "";
	let value = vars.value || 0;
	return `<div class="formControl formControlQuantity" for="${id}">
			<label for="">${vars.title}</label>
			<div class="formGroup">
				<input class="formInput" id="${id}" name="${id}" type="text" value="${value}" placeholder="">
				<button class="btn btnQuantityPlus" for="${id}">+</button>
				<button class="btn btnQuantityMinus" for="${id}">-</button>
			</div>
		</div>`;
}

export const renderInputDate = (vars = []) => {
	let required = vars.required || '';
	let label = vars.label || "Fecha";
	let cls = vars.cls ? vars.cls : 'form33w';
	let value = vars.value || '';
	let placeholder = vars.placeholder || '';

	if (required == true) {
		required = `<span class="required">*</span>`
	}

	return /*html*/ `
	<div class="formControl ${cls} formInputCalendar">
		<label for="inputBirthday" lang="es" class="on">${label}:</label>
		<i class="icon icon-calendar"></i>
		<input type="text" class="formInput" name="${vars.id}" id="${vars.id}" autocomplete="new-date" #date="${value}" value="${value}" placeholder="${placeholder}">
	</div>
	`;


}

export const renderPill = (vars = []) => {
	console.log('renderPill', vars);
	let fn = vars.fn || '';
	let module = vars.module || '';
	let cls = vars.cls || '';
	let clsName = vars.clsName || '';
	return /*html*/ `
        <div class="pill ${cls}" id="pill${vars.id}" data-id="${vars.id}" data-module="${vars.module}">
            <span class="${clsName}" data-id="${vars.id}" >${vars.name}</span>
			<a class="btn btnSmall btnIcon btnClosePill" data-module="${vars.module}" data-fn="${fn}" data-id="${vars.id}">
				<i class="icon icon-circle-close"> </i>
			</a>
        </div>
    `;
}

export const renderCountrySelect = (vars = []) => {
	console.log('renderCountrySelect', vars);

	let selected = vars.selected || "BO";
	let id = vars.id || "inputCountry";
	let selectedOptions = ["AF", "AL", "DZ", "AS", "AD", "AO", "AI", "AR", "AM", "AW", "AU", "AT", "AZ", "BS", "BH", "BD", "BB", "BY", "BE", "BZ", "BJ", "BM", "BT", "BO", "BA", "BW", "BV", "BR", "IO", "BN", "BG", "BF", "BI", "KH", "CM", "CA", "CV", "KY", "CF", "TD", "CL", "CN", "CX", "CC", "CO", "KM", "CG", "CD", "CK", "CR", "CI", "HR", "CU", "CY", "CZ", "DK", "DJ", "DM", "DO", "EC", "EG", "SV", "GQ", "ER", "EE", "ET", "FK", "FO", "FJ", "FI", "FR", "GF", "PF", "TF", "GA", "GM", "GE", "DE", "GH", "GI", "GR", "GL", "GD", "GP", "GU", "GT", "GN", "GW", "GY", "HT", "HM", "VA", "HN", "HK", "HU", "IS", "IN", "ID", "IR", "IQ", "IE", "IL", "IT", "JM", "JP", "JO", "KZ", "KE", "KI", "KP", "KR", "KW", "KG", "LA", "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MO", "MK", "MG", "MW", "MY", "MV", "ML", "MT", "MH", "MQ", "MR", "MU", "YT", "MX", "FM", "MD", "MC", "MN", "MS", "MA", "MZ", "MM", "NA", "NR", "NP", "NL", "AN", "NC", "NZ", "NI", "NE", "NG", "NU", "NF", "MP", "NO", "OM", "PK", "PW", "PA", "PG", "PY", "PE", "PH", "PN", "PL", "PT", "PR", "QA", "RE", "RO", "RU", "RW", "KN", "LC", "VC", "WS", "SM", "ST", "SA", "SN", "SC", "SL", "SG", "SK", "SI", "SB", "SO", "ZA", "GS", "ES", "LK", "SH", "PM", "SD", "SR", "SJ", "SZ", "SE", "CH", "SY", "TW", "TJ", "TZ", "TH", "TG", "TK", "TO", "TT", "TN", "TR", "TM", "TC", "TV", "UG", "UA", "AE", "GB", "US", "UM", "UY", "UZ", "VU", "VE", "VN", "VG", "VI", "WF", "EH", "YE", "ZM", "ZW"];
	
	let labelOptions = ["Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "the Democratic Republic of the Congo", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint LUCIA", "Saint Vincent and the Grenadines</option >", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Viet Nam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Zambia", "Zimbabwe"];

	let options = selectedOptions.map(option => {
		let isSelected = option === selected ? "selected" : "";
		let label = labelOptions[selectedOptions.indexOf(option)];
		return `<option value="${option}" ${isSelected} >${label}</option>`;
	});

	return `
		<select class="formInput" name="${id}" id="${id}">
			<option>Seleccionar País</option>
			${options.join("")}
		</select>
	`;
}