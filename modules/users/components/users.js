import {
    replacePath,
    replaceAll,
    loadingBar,
    loadView,
    loadLoading,
    addHtml,
    replaceEssentials,
    accessToken,
    loadingBarIdReturn,
    alertMessageError
} from "../../components/functions.js";

import {
    categorysListTable,
    createTable,
    dataTable,
    removeItemTable,
    inactiveBtnStateItem,
    activeBtnStateItem,
    mountTable,
    colCheck,
    colId,
    colName,
    colTitle,
    colState,
    colActionsBase,
    deleteItemModule,
    renderEmpty
} from "../../components/tables.js";
import {
    renderCol,
    renderColCheck,
    renderColId,
    renderColTitle,
    renderRowsTable,
    renderColState,
    renderColActions,
    renderColCategorys,
} from "../../components/renders/renderTables.js";
import {
    innerForm,
    renderInnerWindow,
    renderDeleteModal
} from "../../components/renders/renderModals.js";
import {
    removeModal
} from "../../components/modals.js";
import {
    renderRadioButton
} from "../../components/renders/renderForms.js";



let tableId = "tableUsers";
let module = "users";
let system = "users";
let _USERS = [];

export const usersIndex = (vars = []) => {
    console.log('usersIndex', vars);
    loadView(_PATH_WEB_NUCLEO + `modules/users/views/users.html?` + _VS).then((htmlView) => {
        //console.log('loadView', responsePath);
        let str = replaceEssentials({
            str: htmlView,
            name: "Users",
            fn: "formNewUser",
            color: $(".btnItemMenu[path='users']").attr("color"),
            icon: 'icon icon-users',
            btnLabelAction: "Nuevo Usuario",
            module,
            system,
        })

        addHtml({
            selector: '#' + vars.id,
            type: 'insert',
            content: str
        }) //type: html, append, prepend, before, after

        getUsers().then((response) => {
            _USERS = response.data;
            if (response.Error == 0 && response.data != 0) {
                console.log('getUser', response);
                let rows = "";
                for (let i in response.data) {
                    let item = response.data[i];
                    //console.log("item", item);
                    rows += renderRowsTable({
                        id: item.id,
                        content: renderColCheck({
                            id: item.id,
                        }) +
                            renderColId({
                                id: item.id,
                            }) +
                            renderCol({
                                id: item.id,
                                cls: '',
                                attr: '',
                                data: item.name,
                            }) +
                            renderCol({
                                id: item.id,
                                cls: '',
                                attr: '',
                                data: item.lastname,
                            }) +
                            renderCol({
                                id: item.id,
                                cls: '',
                                attr: `rol="${item.rolId}"`,
                                data: item.rolName,
                            }) +
                            renderCol({
                                id: item.id,
                                cls: '',
                                attr: '',
                                data: item.user,
                            }) +
                            renderColState({
                                id: item.id,
                                state: item.state,
                                cls: 'btnChangeStateUsers',
                                module,
                                system,
                            }) +

                            renderColActions({
                                id: item.id,
                                name: item.title,
                                type: "btnEdit,btnDeleteStd",
                                name: item.nameFull,
                                fnType: "formEditUser,btnDeleteUserItem",
                                module,
                                system,
                            }),
                    });
                }

                mountTable({
                    id: tableId,
                    columns: [
                        ...colCheck,
                        ...colId,
                        {
                            label: "Nombre(s)",
                            cls: "colName",
                        },

                        {
                            label: "Apellido(s)",
                            cls: "colLastname",
                        },
                        {
                            label: "Rol",
                            cls: "",
                        },
                        {
                            label: "Usuario",
                            cls: "colDareRegister",
                        },
                        ...colState,
                        ...colActionsBase,
                    ],
                    rows,
                    module,
                    system,
                    container: "#" + vars.id + " .tbody",
                });
            } else if (response === 0 || response.items == 0) {
                $("#" + vars.id).html(renderEmpty());
            } else {
                alertMessageError({
                    title: 'Error',
                    text: responsePath.message
                });
            }

        }).catch(console.warn());
    }).catch(console.warn());
}

// axios async
export const getUsers = async (vars = []) => {
    console.log('getUsers', vars);
    const url = _PATH_WEB_NUCLEO + "modules/users/controllers/apis/v1/users.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "getUsers",
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("Error: getUsers")
        console.log(error)
    }
}

export const formNewUser = (vars = []) =>{
    console.log('click formNewUser');
    loadView(_PATH_WEB_NUCLEO + "modules/users/views/formUser.html?" +_VS).then((responseView)=>{
        let str = responseView;
        str = str.replace(/{{_TITLE}}/g, "Crear Usuario");
        str = str.replace(/{{_BTN_NAME_ACTION}}/g,"Guardar");
        str = str.replace(/{{_FORM_ID}}/g, "no");
        str = str.replace(/{{_MODULE}}/g, module);

        getData({
            task: 'getRoles',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
        }).then((response) => {
            console.log('getData getRoles', response.data);
            if(response.status=='success'){

                
                renderInnerWindow({
                    id: "formNewUser",
                    selector: `#contentConfigAdmin`,
                    content: str,
                });

                const radioButtonsHtml = renderRadioButton({
                    array: response.data,
                    id: "userRole",
                    checkedValue: "admin",
                    cls: "custom-radio",
                });
                str = str.replace("{{_RADIO_BUTTONS}}", radioButtonsHtml);

                document.querySelector(".roleOptions").innerHTML = radioButtonsHtml;

                
            }else{
                 alertMessageError({message: response.message})
            }
        }).catch(console.warn());

    })
}

export const formEditUser = (vars = []) => {
    console.log('click formEditUser');
    loadView(_PATH_WEB_NUCLEO + "modules/users/views/formUser.html?" +_VS).then((responseView)=>{
        let str = responseView;
        str = str.replace(/{{_TITLE}}/g, "Crear Usuario");
        str = str.replace(/{{_BTN_NAME_ACTION}}/g,"Guardar");
        str = str.replace(/{{_FORM_ID}}/g, "no");
        str = str.replace(/{{_MODULE}}/g, module);

        renderInnerWindow({
            id: "formEditUser",
            selector: `#contentConfigAdmin`,
            content: str,
        });
    })
}

export const changeStateId = async (vars = []) => {
    console.log('changeStateId', vars);
    const url = _PATH_WEB_NUCLEO + "modules/users/controllers/apis/v1/users.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "changeStateId",
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("Error: changeStateId")
        console.log(error)
    }
}

export const deleteItems = async (vars = []) => {
    console.log('deleteItems', vars);
    const url = _PATH_WEB_NUCLEO + "modules/users/controllers/apis/v1/users.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "deleteItems",
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("Error: deleteItems")
        console.log(error)
    }
}

export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/users/controllers/apis/v1/users.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("Error: getDataPlaces", error);
    }
}

document.addEventListener("DOMContentLoaded", function () {
    $("body").on("click", ".btn[fn='formNewUser']", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        formNewUser();
    })

    $("body").on("click", ".btn[fn='formEditUser']", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();

        formEditUser(data);
    })


    $("body").on("click", ".btnChangeStateUsers", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let state = $(this).attr("state");
        console.log('btnChangeStateUsers', state);
        let id = $(this).attr("item");
        if (state == 1) {
            state = 0;
        } else {
            state = 1;
        }
        changeStateId({
            id,
            state
        }).then((response) => {
            console.log('changeStateId', response);
            if (response.status == "success") {
                $("#" + tableId + " .btnChangeStateUsers[item='" + id + "']").attr("state", state);
            } else {
                alertMessageError({
                    title: 'Error',
                    text: response.message
                });
            }
        }).catch(console.warn());
    })

    $("body").on("click", ".btnDeleteUserItem", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let id = $(this).attr("data-id");
        console.log(`.btnDeleteUserItem`,);

        deleteItems({
            items: [id]
        }).then((response) => {
            console.log('deleteItems', response);
            if (response.status == "success") {
                removeItemTable(id, tableId);
                removeModal({
                    id: 'formDeleteStd'
                });
            } else {
                alertMessageError({
                    title: 'Error',
                    text: response.message
                });
            }
        }).catch(console.warn());


    })



});

document.addEventListener('click', function handleClick(event) {
    //console.log('user clicked: ', event.target);

    //event.target.classList.add('bg-yellow');
});

/*

 $("body").on("click", `.btn[module="${module}"]`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btn[user]`, data);
    });

*/

/* document.addEventListener("click", function (e) {
    console.log(e)
}); */