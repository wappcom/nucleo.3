import {
    replacePath,
    replaceAll,
    loadingBar,
    loadView,
    loadLoading,
    addHtml,
    replaceEssentials,
    accessToken,
    loadingBarIdReturn,
    alertMessageError
} from "../../components/functions.js";

import {
    categorysListTable,
    createTable,
    dataTable,
    removeItemTable,
    inactiveBtnStateItem,
    activeBtnStateItem,
    mountTable,
    colCheck,
    colId,
    colName,
    colTitle,
    colState,
    colActionsBase,
    deleteItemModule,
    renderEmpty
} from "../../components/tables.js";
import {
    renderCol,
    renderColCheck,
    renderColId,
    renderColTitle,
    renderRowsTable,
    renderColState,
    renderColActions,
    renderColCategorys,
} from "../../components/renders/renderTables.js";
import {
    innerForm,
    renderInnerWindow,
    renderDeleteModal
} from "../../components/renders/renderModals.js";

import {
    listCategorys,
    checkCategorys
}
    from "./../../websites/components/categorys.js";

import {
    removeModal,
    deleteModal,
} from "../../components/modals.js";

let tableId = "tableRoles";
let module = "roles";
let system = "users";
let _ROLES = [];

export const rolesIndex = (vars = []) => {
    //console.log('rolesIndex', vars);
    getData({
        task: 'getRoles',
        return: 'returnArray',
    }).then((response) => {
        //console.log('getRoles', response);
        if (response.status == 'success') {
            _ROLES = response.data;
            loadView(_PATH_WEB_NUCLEO + `modules/users/views/roles.html?` + _VS).then((htmlView) => {
                //console.log('loadView', responsePath);
                let str = replaceEssentials({
                    str: htmlView,
                    name: "Roles",
                    fn: "rolesCreate",
                    color: $(".btnItemMenu[path='roles']").attr("color"),
                    icon: 'icon icon-credential',
                    btnLabelAction: "Nuevo Rol",
                })
                str = replaceAll(str, "{{_CLS_ADD}}", "btnAddRol");
                addHtml({
                    selector: '#' + vars.id,
                    type: 'insert',
                    content: str
                })

                if (response.Error == 0 && response.data != 0) {
                    console.log('getRoles', response);
                    let rows = "";
                    for (let i in response.data) {
                        let item = response.data[i];
                        //console.log("item", item);
                        rows += renderRowsTable({
                            id: item.id,
                            content: renderColCheck({
                                id: item.id,
                            }) +
                                renderColId({
                                    id: item.id,
                                }) +
                                renderCol({
                                    id: item.id,
                                    cls: '',
                                    attr: '',
                                    data: item.name,
                                }) +
                                renderCol({
                                    id: item.id,
                                    cls: '',
                                    attr: '',
                                    data: item.description,
                                }) +
                                renderCol({
                                    id: item.id,
                                    cls: '',
                                    attr: '',
                                    data: item.parentId,
                                }) +
                                renderCol({
                                    id: item.id,
                                    cls: '',
                                    attr: '',
                                    data: item.redirectionUrl,
                                }) +
                                renderColState({
                                    id: item.id,
                                    state: item.state,
                                    cls: 'btnChangeStateUsers',
                                    module,
                                    system,
                                }) +

                                renderColActions({
                                    id: item.id,
                                    type: "btnEditFn,btnDeleteFn",
                                    name: item.name,
                                    fnType: "rolesUpdate,btnDeleteRol",
                                    module,
                                    system,
                                }),
                        });
                    }

                    mountTable({
                        id: tableId,
                        columns: [
                            ...colCheck,
                            ...colId,
                            {
                                label: "Nombre Rol",
                                cls: "colName",
                            },
                            {
                                label: "Descripción",
                                cls: "",
                            },
                            {
                                label: "Padre",
                                cls: "",
                            },
                            {
                                label: "Redirección",
                                cls: "",
                            },

                            ...colState,
                            ...colActionsBase,
                        ],
                        rows,
                        module,
                        system,
                        container: "#" + vars.id + " .tbody",
                    });
                } else if (response === 0 || response.items == 0) {
                    $("#" + vars.id).html(renderEmpty());
                } else {
                    alertMessageError({
                        title: 'Error',
                        text: responsePath.message
                    });
                }
            }).catch(console.warn());
        } else {
            alertMessageError({
                message: response.message
            })
        }
    }).catch(console.warn());
}

export const rolesCreate = (vars = []) => {
    console.log('rolesCreate', vars);

    loadView(_PATH_WEB_NUCLEO + "modules/users/views/rolesForm.html?" + _VS).then((responseView) => {
        console.log('loadView', responseView);
        let str = responseView;
        renderInnerWindow({
            id: "formRolesCreate",
            selector: `#contentConfigAdmin`,
            content: str,
        });
    });

}

export const rolesUpdate = (vars = []) => {
    //console.log('rolesUpdate', vars, _CATEGORYS);
    loadView(_PATH_WEB_NUCLEO + "modules/users/views/rolesForm.html?" + _VS).then((responseView) => {
        //console.log('loadView', responseView);
        let str = responseView;
        let formId = "formRolesUpdate";
        let item = _ROLES.find(item => item.id == vars.id);

        str = replaceAll(str, "{{_ITEM}}", vars.id);
        str = replaceAll(str, "{{_FORM_ID}}", formId);
        str = replaceAll(str, "{{_TITLE}}", "Editar Rol");
        str = replaceAll(str, "{{_BTN_NAME_ACTION}}", "Actualizar");
        str = replaceAll(str, "{{_CATEGORYS}}", listCategorys(_CATEGORYS));

        renderInnerWindow({
            id: formId,
            selector: `#contentConfigAdmin`,
            content: str,
        });

        $(`#${formId} #inputId`).val(item.id);
        $(`#${formId} #inputName`).val(item.name);
        $(`#${formId} #inputDescription`).val(item.description);
        $(`#${formId} #inputRedirectionUrl`).val(item.redirectionUrl);
        $(`#${formId} #inputJson`).val(item.json);
        $(`#${formId} #inputParent`).append(optionRoles({
            selectId: item.parentId
        }));
    });
}


export const optionRoles = (vars = []) => {
    //console.log('optionRoles', vars);
    let string = "";
    _ROLES.forEach(element => {
        let aux = "";
        if (element.id == vars.selectId) {
            aux = "selected";
        }
        string += `<option value="${element.id}" ${aux}>${element.name}</option>`;
    });

    return string;
}

//async

export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/users/controllers/apis/v1/users.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        //console.log("Error: getDataPlaces")
        //console.log(error)
    }
}


document.addEventListener("DOMContentLoaded", function () {
    $("body").on("click", `.btnAddRol`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data()
        console.log(`.btnAddRol`, data);
        rolesCreate();
    })

    $("body").on("click", `.btnDeleteRol`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data()
        let id = $(this).attr("data-id");
        let name = $(this).attr("data-name");

        console.log(`.btnDeleteRol`, data);

        deleteModal({
            id: "deleteModal",
            name,
            attr: `data-name="${name}" data-fn="deleteRol" data-id="${id}"`,
        })
    })

    $("body").on("click", `.deleteItem[data-fn="deleteRol"]`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data()
        console.log(`.deleteItem[data-fn="deleteRol"]`, data);

        getData({
            task: 'deleteRol',
            return: 'returnState', // returnId, returnState, returnArray
            id: data.id,
        }).then((response) => {
            console.log('deleteRol', response);
            if (response.status == 'success') {
                removeItemTable(id, tableId);
                removeModalId("deleteModal");
            } else {
                alertMessageError({
                    message: response.message
                })
            }
        }).catch(console.warn());
    });

    /* $("body").on("click", `.btnEditFn`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data()
        //console.log(`.btnEditFn`, data);
        rolesUpdate(data)
    }) */


});