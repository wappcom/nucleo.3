import {
    replacePath,
    replaceAll,
    loadingBar,
    loadView,
    loadLoading,
    addHtml,
    replaceEssentials,
    accessToken,
    loadingBarIdReturn,
    alertMessageError
} from "../../components/functions.js";

import {
    categorysListTable,
    createTable,
    dataTable,
    removeItemTable,
    inactiveBtnStateItem,
    activeBtnStateItem,
    mountTable,
    colCheck,
    colId,
    colName,
    colTitle,
    colState,
    colActionsBase,
    deleteItemModule,
    renderEmpty
} from "../../components/tables.js";
import {
    renderCol,
    renderColCheck,
    renderColId,
    renderColTitle,
    renderRowsTable,
    renderColState,
    renderColActions,
    renderColCategorys,
} from "../../components/renders/renderTables.js";
import {
    innerForm,
    renderInnerWindow,
    renderDeleteModal
} from "../../components/renders/renderModals.js";

let module = "Systemas";

export const systemsIndex = (vars = []) => {
    loadView(_PATH_WEB_NUCLEO + "modules/users/views/systems.html?" + _VS).then((responseView) => {
        //console.log('loadView', responseView);
        let str = responseView;
        str = str.replace(/{{_COLOR}}/g, $(".btnItemMenu[path='systems']").attr("color"));
        str = str.replace(/{{_ICON}}/g, "icon icon-system");
        str = str.replace(/{{_NAME}}/g, "Systemas");
        str = str.replace(/{{_FN}}/g, "formNewSystem");
        str = str.replace(/{{_BTN_LABEL_ACTION}}/g, "Nuevo Sistema");
        addHtml({
            selector: `#contentConfigAdmin`,
            type: 'insert', // insert, append, prepend, replace
            content: str
        })

        getData({
            task: 'getSystems',
            return: 'returnArray', // returnId, returnState, returnArray

        }).then((response) => {
            console.log('getSystems', response);
            if (response.status == 'success') {
                for (let i in response.data) {
                    let item = response.data[i];
                    //console.log("item", item);
                    rows += renderRowsTable({
                        id: item.id,
                        content: renderColCheck({
                            id: item.id,
                        }) +
                            renderColId({
                                id: item.id,
                            }) +
                            renderCol({
                                id: item.id,
                                cls: '',
                                attr: '',
                                data: item.name,
                            }) +
                            renderCol({
                                id: item.id,
                                cls: '',
                                attr: '',
                                data: item.type,
                            }) +
                            renderCol({
                                id: item.id,
                                cls: '',
                                attr: ``,
                                data: item.module,
                            }) + 
                            renderColState({
                                id: item.id,
                                state: item.state,
                                cls: 'btnChangeStateUsers',
                                module,
                                system,
                            }) +

                            renderColActions({
                                id: item.id,
                                name: item.title,
                                type: "btnEdit,btnDeleteStd",
                                name: item.nameFull,
                                fnType: "formEditUser,btnDeleteUserItem",
                                module,
                                system,
                            }),
                    });
                }

                mountTable({
                    id: tableId,
                    columns: [
                        ...colCheck,
                        ...colId,
                        {
                            label: "Nombre",
                            cls: "colName",
                        },

                        {
                            label: "Tipo",
                            cls: "colType",
                        },
                        {
                            label: "Modulos",
                            cls: "",
                        },
                        ...colState,
                        ...colActionsBase,
                    ],
                    rows,
                    module,
                    system,
                    container: "#" + vars.id + " .tbody",
                });
            } else if (response === 0 || response.items == 0) {
                $("#" + vars.id).html(renderEmpty());
            } else {
                alertMessageError({
                    message: response.message
                })
            }
        }).catch(console.warn());
    }).catch(console.warn());
}

export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/users/controllers/apis/v1/users.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        //console.log("Error: getDataPlaces")
        //console.log(error)
    }
}

export const formNewSystem = (vars = []) => {
    console.log('click formNewSystem');

    loadView(_PATH_WEB_NUCLEO + "modules/users/views/formSystem.html?" +_VS).then((responseView)=>{
        let str = responseView;
        str = str.replace(/{{_TITLE}}/g, "Crear Usuario");
        str = str.replace(/{{_BTN_NAME_ACTION}}/g,"Guardar");
        str = str.replace(/{{_FORM_ID}}/g, "no");
        str = str.replace(/{{_MODULE}}/g, module);

        renderInnerWindow({
            id: "formNewSystem",
            selector: `#contentConfigAdmin`,
            content: str,
        });
    })
}

document.addEventListener("DOMContentLoaded", function(){
    $("body").on("click", ".btn[fn='formNewSystem']", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        formNewSystem();
    })
});