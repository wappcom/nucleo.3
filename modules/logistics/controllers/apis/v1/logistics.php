<?php
$access = json_decode($_POST["accessToken"], true);
$page = $_POST["page"];

if (empty($access)) {
    $pageRoot = $page;
} else {
    $pageRoot = $access["page"];
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $pageRoot  . "config.php");
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

require_once(_PATH_NUCLEO . "modules/logistics/controllers/class/class.logistics.php");
$logistics = new LOGISTICS($fmt);

require_once(_PATH_NUCLEO . "modules/accounts/controllers/class/class.accounts.php");
$accounts = new ACCOUNTS($fmt);

header("Content-Type: application/json");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: JSON");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

switch ($_SERVER["REQUEST_METHOD"]) {
    case 'POST':
        if (empty($access)) {
            $access_token = $fmt->auth->getInput('access_token');
            $refresh_token = $fmt->auth->getInput('refresh_token');
            $entitieId = $fmt->auth->getInput('entitieId');
        } else {
            $access_token = $access['access_token'];
            $refresh_token = $access['refresh_token'];
            $entitieId = $access['entitieId'];
            $action = $fmt->auth->getInput('action');
        }
        $actions = $fmt->auth->getInput('actions');

        $acuId = $accounts->validateUserToken(array("access_token" => $access_token, "refresh_token" => $refresh_token, "idEntitie" => $entitieId));

        //echo json_encode($refresh_token); exit(0);
        //echo json_encode("user: ".$userId); exit(0);

        if ($acuId) {
            //echo json_encode($entitieId); exit(0);
            //echo json_encode("llegamos");

            if ($actions == "loadPerimeters") {
                echo json_encode($logistics->listPerimeters());
            }

            if ($actions == "addAddressPlace") {
                $vars = json_decode($_POST['vars'], true);
                $response = $logistics->addAddressPlace(array("acuId" => $acuId, "vars" => $vars));

                if ($response != 0) {
                    $response["Error"] = 0;
                    echo json_encode($response);
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Error create Address",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
            }

            if ($action == "updateListAddress") {
                $addresses = json_decode($_POST['addresses'], true);
                $response = $logistics->updateListAddress(array("acuId" => $acuId, "addresses" => $addresses));

                if ($response != 0) {
                    $res["Error"] = 0;
                    echo json_encode($res);
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Error updateListAddress",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
            }

            if ($actions == "deleteAddress") {
                $vars = json_decode($_POST['vars'], true);
                $response = $logistics->deleteAddress(array("acuId" => $acuId, "vars" => $vars));

                //echo json_encode($response);

                if ($response != 0) {
                    $return["Error"] = 0;
                    $return["response"] = $response;
                    echo json_encode($return);
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Error delete Address",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
            }
        } else {
            echo $fmt->errors->errorJson([
                "description" => "Access Auth. invalid user",
                "code" => "",
                "lang" => "es"
            ]);
        }

        break;

    default:
        echo $fmt->errors->errorJson([
            "description" => "Access Auth. Metod request. " . $pathMod,
            "code" => "",
            "lang" => "es"
        ]);
        break;
}
