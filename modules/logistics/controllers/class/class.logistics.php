<?php
header('Content-Type: text/html; charset=utf-8');
class LOGISTICS
{

    var $fmt;
    var $pla;
    var $cat;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function listPerimeters()
    {
        $sql = "SELECT * FROM mod_logistics_distribution_zone WHERE  mod_log_dtz_state > 0";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return[$i]["id"] = $row["mod_log_dtz_id"];
                $return[$i]["name"] = $row["mod_log_dtz_name"];
                $return[$i]["code"] = $row["mod_log_dtz_code"];
                $return[$i]["perimetrer"] = $row["mod_log_dtz_perimetrer"];
                $return[$i]["cost"] = $row["mod_log_dtz_cost"];
                $return[$i]["state"] = $row["mod_log_dtz_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function dataPerimeter(Int $id = null)
    {
        $sql = "SELECT * FROM mod_logistics_distribution_zone WHERE  mod_log_dtz_id = '" . $id . "' AND  mod_log_dtz_state > 0";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {

            $row = $this->fmt->querys->row($rs);
            $return["id"] = $row["mod_log_dtz_id"];
            $return["name"] = $row["mod_log_dtz_name"];
            $return["code"] = $row["mod_log_dtz_code"];
            $return["perimetrer"] = $row["mod_log_dtz_perimetrer"];
            $return["cost"] = $row["mod_log_dtz_cost"];
            $return["state"] = $row["mod_log_dtz_state"];

            return $return;
        } else {
            return 0;
        }
    }

    public function addAddress($vars = null)
    {
        $acuId = $vars["acuId"];
        $coord = $this->fmt->emptyReturn($vars["coord"], "");
        $address = $this->fmt->emptyReturn($vars["address"], "");
        $alias = $this->fmt->emptyReturn($vars["alias"], "");
        $description = $this->fmt->emptyReturn($vars["description"], "");

        $insert = 'mod_add_name,mod_add_alias,mod_add_description,mod_add_coord,mod_add_state';
        $values  = "'" . $address . "','" . $alias . "','" . $description . "','" . $coord . "','1'";
        $sql = "insert into mod_addresses (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);


        $sql = "select max(mod_add_id) as id from mod_addresses";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        return $row["id"];
    }

    public function addAddressPlace($vars = null)
    {
        $acuId = $vars["acuId"];
        $var = $vars["vars"];
        $coord = $var["coord"];
        $inputAddress = $var["inputAddress"];
        $inputPerimeter = $var["inputPerimeter"];

        if (is_numeric($acuId)) {
            $addId = $this->addAddress(array(
                'acuId' => $acuId,
                'coord' => $coord,
                'address' => $inputAddress
            ));
        } else {
            return 0;
        }

        if (is_numeric($addId)) {

            $order = $this->relationAddressAccount($acuId, $addId);
            $delileryId = $this->relationAddressDelivery($addId, $inputPerimeter);
            $return["addId"] =  $addId;
            $return["deliveryId"] = $delileryId;
            $return["order"] =   $order;
            return $return;
        } else {
            return 0;
        }
    }
    public function relationAddressDelivery($addId, $delileryId)
    {

        $rel = $this->exitRelationAddressDelivery($addId);
        $order = 0;
        if ($rel != 0) {
            $order = $rel;
        }

        $insert = 'mod_add_ldz_add_id,mod_add_ldz_ldz_id,mod_add_ldz_order';
        $values  = "'" . $addId . "','" . $delileryId . "','" . $order . "'";
        $sql = "insert into mod_addresses_logistics_distribution_zone (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        return $delileryId;
    }

    public function updateListAddress(array $var = null)
    {
        $acuId = $var['acuId'];
        $addresses = $var['addresses'];
        $count = count($addresses);

        for ($i = 0; $i < $count; $i++) {
            $sql = "UPDATE mod_accounts_addresses SET
            mod_acu_add_order='" . $i . "'
            WHERE mod_acu_add_acu_id ='" . $acuId   . "' AND mod_acu_add_add_id= '" . $addresses[$i] . "'";
            $this->fmt->querys->consult($sql);
        }
        return 1;
    }

    public function relationAddressAccount($acuId, $addId)
    {

        $rel = $this->exitRelationAddressRelation($acuId);
        $order = 0;
        if ($rel != 0) {
            $order = $rel;
        }

        $insert = 'mod_acu_add_acu_id,mod_acu_add_add_id,mod_acu_add_order';
        $values  = "'" . $acuId . "','" . $addId . "','" . $order . "'";
        $sql = "insert into mod_accounts_addresses (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        return $order;
    }

    public function exitRelationAddressRelation($acuId)
    {
        $sql = "SELECT mod_acu_add_order FROM mod_accounts_addresses  WHERE mod_acu_add_acu_id='" . $acuId . "' ORDER BY mod_acu_add_order DESC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $aux = intval($row["mod_acu_add_order"]) + 1;
            return $aux;
        } else {
            return 0;
        }
    }

    public function getRelationAddressDelivery(Int $addId = null)
    {
        $sql = "SELECT mod_add_ldz_ldz_id FROM mod_addresses_logistics_distribution_zone  WHERE mod_add_ldz_add_id='" . $addId . "' ORDER BY mod_add_ldz_order ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_add_ldz_ldz_id"];
        } else {
            return 0;
        }
    }

    public function exitRelationAddressDelivery($addId)
    {
        $sql = "SELECT mod_add_ldz_order FROM mod_addresses_logistics_distribution_zone  WHERE mod_add_ldz_add_id='" . $addId . "' ORDER BY mod_add_ldz_order ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $aux = intval($row["mod_add_ldz_order"]) + 1;
            return $aux;
        } else {
            return 0;
        }
    }

    public function deleteAddress($vars = null)
    {
        $acuId = $vars["acuId"];
        $var = $vars["vars"];
        $item = $var["item"];

        $sqle = "DELETE FROM mod_addresses WHERE mod_add_id='" . $item . "' ";
        $this->fmt->querys->consult($sqle, __METHOD__);
        $up_sqr6 = "ALTER TABLE mod_addresses AUTO_INCREMENT=1";
        $this->fmt->querys->consult($up_sqr6, __METHOD__);

        $sqle1 = "DELETE FROM mod_accounts_addresses WHERE  mod_acu_add_add_id='" . $item . "'";
        $this->fmt->querys->consult($sqle1, __METHOD__);
        $up_sqr61 = "ALTER TABLE mod_accounts_addresses AUTO_INCREMENT=1";
        $this->fmt->querys->consult($up_sqr61, __METHOD__);

        return $item;
    }


    public function listByAddresses(array $addresses = null)
    {
        # code...
    }
}