<?php
header('Content-Type: text/html; charset=utf-8');
class CUSTOMERS
{

    var $fmt;
    var $brokers;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
        require_once(_PATH_NUCLEO . "modules/brokers/controllers/class/class.brokers.php");
        $this->brokers = new BROKERS($fmt);
    }

    public function loadCustomersCompanys(Array $var = null)
    {
        $userId = $var["userId"];
        $rolId = $var["rolId"];
        $entitieId =  $var["entitieId"];

        $sql = "SELECT * FROM mod_customers_companys WHERE mod_cco_ent_id='".$entitieId."'";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                //mod_cco_id	mod_cco_name	mod_cco_description	mod_cco_img	mod_cco_acu_id	mod_cco_register_date	mod_cco_user_id	mod_cco_state	
                $return[$i]["id"] = $row["mod_cco_id"];
                $return[$i]["name"] = $row["mod_cco_name"];
                $return[$i]["description"] = $row["mod_cco_description"];
                $return[$i]["img"] = $this->fmt->files->imgId($row["mod_cco_img"]);
                $return[$i]["acuId"] = $row["mod_cco_acu_id"];
                $return[$i]["registerDate"] = $row["mod_cco_register_date"];
                $return[$i]["userId"] = $row["mod_cco_user_id"];
                $return[$i]["state"] = $row["mod_cco_state"];
            }
            return $return;
        }else{
            return 0;
        }
    }
}