import {
    stopLoadingBar,
    loadView,
    accessToken,
    alertPage,
    replacePath,
    createInnerForm,
    todaysDate,
    formatDate,
} from "../../components/functions.js"
import { resizeWorkspace } from "../../components/nav.js"
import { renderTr, renderEmpty } from "./renderBrokers.js"

const module = "brokersCustomers"
const system = "brokers"
const pathurl = "modules/brokers/"

export function brokersCustomersIndex(vars) {
    loadView(
        _PATH_WEB_NUCLEO + "modules/brokers/views/brokersCustomers.html?" + _VS
    ).then((str) => {
        loadCustomersCompanys().then((response) => {
            str = str.replace(/{{_TITLE_MODULE}}/g, "Clientes")
            str = str.replace(/{{_ICON_MODULE}}/g, "icon-users")
            str = str.replace(/{{_MODULE}}/g, module)
            str = str.replace(/{{_SYSTEM}}/g, system)
            str = replacePath(str)

            if (response.Error == 0) {
                const tableId = "tableCustomersCompanys"
                const data = response.items
                let str = htmlView
                let strTable = ""
                let tbody = ""

                //console.log(data)

                for (let i = 0; i < data.length; i++) {
                    const elem = data[i]
                    elem["table"] = tableId
                    elem["module"] = module
                    elem["system"] = system
                    tbody += renderTr(elem)
                }
                strTable = createTable({
                    id: tableId,
                    thead: ":check,id:colId,Nombre,Estado:colState,Acciones:colActions",
                    body: tbody,
                })

                $(".bodyModule[module='" + module + "']").html(str)
                $(
                    ".bodyModule[module='" + module + "'] .tabContent[item=1]"
                ).html(strTable)

                dataTable({
                    elem: "#" + tableId,
                    orderCol: 1,
                })
                resizeWorkspace()
            } else if (response === 0) {
                $(".bodyModule[module='" + module + "']").html(str)
                $(
                    ".bodyModule[module='" +
                        module +
                        "'] .tabContent[item=1] .tbody"
                ).html(renderEmpty())
            } else {
                alertPage({
                    text:
                        "Error. por favor contactarse con soporte. " +
                        response.message,
                    icon: "icn icon-alert-warning",
                    animation_in: "bounceInRight",
                    animation_out: "bounceOutRight",
                    tipe: "danger",
                    time: "3500",
                    position: "top-left",
                })
            }
        })
    })
}

async function loadCustomersCompanys() {
    const url =
        _PATH_WEB_NUCLEO + "modules/brokers/controllers/apis/v1/customers.php"
    const dataForm = new FormData()
    dataForm.append("accessToken", JSON.stringify(accessToken))
    dataForm.append("action", "loadCustomersCompanys")

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: dataForm,
        })
        console.log(res.data)
        return res.data
    } catch (error) {
        console.log(error)
    }
}

document.addEventListener("DOMContentLoaded", function () {
   /*  $("body").on("click", "module[] .btnAddCustomerCompany", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        loadView(
            _PATH_WEB_NUCLEO + "modules/brokers/views/formCompany.html?" + _VS
        ).then((str) => {
            str = str.replace(/{{_MODULE}}/g, module)
            str = str.replace(/{{_SYSTEM}}/g, system)
            str = str.replace(/{{_TITLE_HEAD}}/g, "Agregar Empresa")
            str = str.replace(/{{_FORM_ID}}/g, "formCompany")
            str = replacePath(str)
            $(".bodyModule[module='" + module + "']").prepend(str)
            $("#formCompany").draggable()
            $("#inputName").focus()
        })
    }) */
})
