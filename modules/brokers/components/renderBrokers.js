import { empty, btnFn } from '../../components/functions.js'
import { renderBtnState } from '../../components/renders/render.js'

export function renderTr(vars) {
    let img = '';
    if ((vars.img != "") && (vars.img != null)) {
        img = '<img id="imgId' + vars.id + '" src="' + _PATH_FILES + vars.img + '">'
    } else {
        img = '<img id="imgId' + vars.id + '" src="' + _PATH_WEB +'">'
    }
    return `
    <tr class="rowItem" item="${vars.id}">
        <td class="colCheck colId"><input name="inputCheck[]"  type="checkbox" value="${vars.id}"></td>
        <td class="colId">${vars.id}</td>
        <td class="colImg"><a>${img}<span>${vars.name}<span></a></td>
        <td class="colState"></td>
        <td class="colActions">
            <div class="btns">
                
            </div>
        </td>
     </tr>
   `
}

export function renderEmpty() {
    return `
        <div class="inner">
            <div class="message messageCenter" lang="es">
                No hay Empresas registrados, Agrega una Empresa.
            </div>
        </div>
    `;
}