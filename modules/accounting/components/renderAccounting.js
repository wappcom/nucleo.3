

export function renderSidebar(vars){
    return /*html*/`
        <button
            type="button"
            class="btnActionModal btnIconLeft btn active"
            module="${vars.module}"
            fn=""
            content="contentAccountinPlan"
        >
            <i class="icon icon-list"></i>
            <span>Plan de Cuentas</span>
        </button>
        <button
            type="button"
            class="btnActionModal btnIconLeft btn"
            module="${vars.module}"
            fn=""
            content="contentCoins"
        >
            <i class="icon icon-coin"></i>
            <span>Monedas</span>
        </button>
    `;
}