import {
    stopLoadingBar,
    loadView,
    accessToken,
    alertPage,
    replacePath,
    createInnerForm,
    todaysDate
} from '../../components/functions.js';
import {
    renderModalConfig,
    renderModalConfigBody
} from '../../components/renders/renderModals.js';
import {
    resizeModalConfig
} from '../../components/modals.js';
import {
    renderSidebar,
} from './renderAccounting.js';

const system = 'accounting';
const module = 'accounting';


export function accountingIndex() {
    //console.log("ACC:", system, module);
    $(".bodyModule[system='" + system + "'][module='" + module + "']").html("Dashboard Contabilidad");
}

async function accountingPlan() {
    const url = _PATH_WEB_NUCLEO + 'modules/accounting/controllers/apis/v1/accounting.php?' + _VS;
    const dataForm = new FormData();
    dataForm.append("accessToken", JSON.stringify(accessToken));
    dataForm.append("action", "accountingPlan");

    try {
        let res = await axios({
            async: true,
            method: 'post',
            responseType: 'json',
            url: url,
            headers: {},
            data: dataForm
        });
        console.log(res.data);
        return res.data;
    } catch (error) {
        console.log(error);
    }
}

export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/accounting/controllers/apis/v1/accounting.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        //console.log("Error: getDataPlaces")
        //console.log(error)
    }
}

document.addEventListener("DOMContentLoaded", function () {

    $("body").on("click", ".btnConfig[module='" + module + "']", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let module = this.getAttribute('module');
        let system = this.getAttribute('system');
        const idModal = "modalConfigAccounting";
        console.log('btnConfig', module, system);
        loadView(_PATH_WEB_NUCLEO + "modules/accounting/views/accountingPlan.html?" + _VS).then((str) => {
            accountingPlan().then((response) => {
                let body = renderModalConfigBody({
                    title: 'Configuración',
                    system,
                    id: 'modalConfigAccounting',
                    sidebar: renderSidebar({
                        module
                    }),
                    body: str
                })
                $(".ws[system='" + system + "']").append(renderModalConfig({
                    body,
                    system,
                    id: idModal
                }));
                resizeModalConfig(system);
            })
        })

    })

    $("body").on("click", ".btnAddAccountPlan", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        $("#formAccountingPlan").addClass("on");
        $("#inputParentName").val("root");
        $("#inputParent").val("0");
    })

})