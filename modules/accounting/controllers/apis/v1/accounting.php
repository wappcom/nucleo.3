<?php
$access = json_decode($_POST["accessToken"], true);
require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $access["page"] . "config.php");
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

require_once(_PATH_NUCLEO."modules/accounting/controllers/class/class.accounting.php");
$accounting = new ACCOUNTING($fmt);

header("Content-Type: application/json");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

switch ($_SERVER["REQUEST_METHOD"]) {
    case 'POST':
        $access_token = $access['access_token'];
        $refresh_token = $access['refresh_token'];
        $entitieId = $access['entitieId'];
        $action = $fmt->auth->getInput('action');

        $userId = $fmt->users->validateUserToken(array("access_token" => $access_token, "refresh_token" => $refresh_token, "idEntitie" => $entitieId));

        if ($userId) {
            //echo "'usario validado";
            //echo json_encode("user: ".$userId.": actions: ".$actions); exit(0);

            $return["user"] = $fmt->users->userDataAuth(array("access_token" => $access_token, "idEntitie" => $entitieId));
            $userId = $return["user"]["userId"];
            $rolId = $return["user"]["rolId"];

            if ($action== "accountingPlan"){
                ///echo json_encode(1);
                // $vars = json_decode($_POST['vars'], true);
                echo json_encode($accounting->accountingPlan(array( "rolId" => $rolId, "entitieId" => $entitieId)));
                exit(0);
            }
        } else {
            echo $fmt->errors->errorJson([
                "description" => "Access Auth. invalid user",
                "code" => "",
                "lang" => "es"
            ]);
            exit(0);
        }

        break;

    default:
        echo $fmt->errors->errorJson([
            "description" => "Access Auth. Metod request.",
            "code" => "",
            "lang" => "es"
        ]);
        exit(0);
        break;
}