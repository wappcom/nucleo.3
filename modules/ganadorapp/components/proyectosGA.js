
import {
    empty,
    replacePath,
    replaceEssentials,
    replaceAll,
    loadView,
    stopLoadingBar,
    emptyReturn,
    loadingBtnIcon,
    btnLoadingRemove,
    unDisableId,
    accessToken,
    removeBtnLoading,
    alertMessageError,
    dataModule,
    jointActions,
    addHtml,
    capitalize,
    activeSelector,
    unactiveSelector
} from "../../components/functions.js";


import {
    renderTr,
} from "./renders/renderProyectosGA.js";


import {
    createTable,
    dataTable,
    removeItemTable,
} from "../../components/tables.js";

import {
    renderNoDataTable
} from "../../components/renders/renderTables.js";


const module = "proyectosGA";
const system = "ganadorapp";
const formId = "formProyectosGA";
const tableId = "tableProyectosGA";

export const proyectosGAIndex = (vars =[]) => {
    console.log('proyectosGAIndex',vars);
    loadView(_PATH_WEB_NUCLEO + "modules/ganadorapp/views/proyectosGA.html?" + _VS).then((responseView) => {
        //console.log('loadView', responseView);

        let content = replaceEssentials({
            str: responseView,
            module,
            system,
            fn: "formNewProyectoGA",
            color: dataModule(module, "color"),
            icon : dataModule(module, "icon"),
        })

        addHtml({
           selector:`.bodyModule[module="${module}"]`,
           type: 'insert', //insert, append, prepend, replace
           content 
        })

        getData({
            task: 'getProyectos',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
        }).then((response) => {
            console.log('getData getProyectos', response);
            if (response.status == "success") {
                if (response != null && response.Error == 0) {
                    const data = response.data;
                    let strTable = "";
                    let tbody = "";
                    //console.log(data);

                    for (let i = 0; i < data.length; i++) {
                        const elem = data[i];
                        elem["table"] = tableId;
                        elem["module"] = module;
                        elem["system"] = system;
                        elem["path"] = capitalize(module);
                        tbody += renderTr(elem);
                    }

                    strTable = createTable({
                        id: tableId,
                        thead: ":check,id:colId,Proyecto,Fecha F.Captación,Ganadero,Usuario,Estado:colState,Acciones:colActions",
                        body: tbody,
                    });

                    $( ".bodyModule[module='" + module + "'] >.tbody").html(strTable);

                    dataTable({
                        elem: "#" + tableId,
                        orderCol: 1,
                    });

                } else if (response.data == 0) {
                    $(".bodyModule[module='" + module + "'] >.tbody").html(renderNoDataTable());
                }
            } else {
                alertMessageError({
                    message: response.message,
                });
            }
        }).catch(console.warn());
        
    }).catch(console.warn());
}

export const getData = async (vars = []) => {
   //console.log('getData', vars);
   const url = _PATH_WEB_NUCLEO + "modules/ganadorapp/controllers/apis/v1/proyectosGA.php";
   let data = JSON.stringify({
       accessToken: accessToken(),
       vars: JSON.stringify(vars)
   });
   try {
       let res = await axios({
           async: true,
           method: "post",
           responseType: "json",
           url: url,
           headers: {},
           data: data
        })
       //console.log(res.data);
       jointActions(res.data)
       return res.data
   } catch (error) {
      console.log("Error: getData ", error);
   }
}


