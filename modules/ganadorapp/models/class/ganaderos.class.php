<?php
header('Content-Type: text/html; charset=utf-8');
class GANADEROS
{
    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function dataId(int $id = null, int $state = 0)
    {
        if ($state != 0) {
            $state = "AND mod_ggd_state".$state;
         }else{
            $state = "";
        }

        $sql = "SELECT * FROM mod_ganador_ganaderos WHERE mod_ggd_id = '$id' ". $state;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_ggd_id"];
            $return["id"] = $id;
            $return["name"] = $row["mod_ggd_name"];
            $return["acuId"] = $row["mod_ggd_acu_id"];
            $return["perfil"] = $row["mod_ggd_perfil"];
            $return["imgId"] = $row["mod_ggd_img_id"];
            $return["dial"] = $row["mod_ggd_dial"];
            $return["celular"] = $row["mod_ggd_celular"];
            $userId = $row["mod_ggd_user_id"];
            $return["user"] = $this->fmt->users->fullName($userId);
            $return["date_register"] = $row["mod_ggd_date_register"];
            $return["state"] = $row["mod_ggd_state"];
            return $return;
        } else {
            return 0;
        }
        
    }
    //creame una funcion que me llame a todos los proyectos en ejecución de la base de datos ganador_1 en la tabla mod_ganador_proyectos
    public function getGanaderos($var = [])
    {
        //return $var;
 
        $order = $this->fmt->emptyReturn($var["order"], "");
        $limit = $this->fmt->emptyReturn($var["limit"], "");
        $state = $this->fmt->emptyReturn($var["state"], "");
        $return = [];

        if (!empty($order)) {
            $order = "ORDER BY " . $order;
        }else{
            $order = "ORDER BY mod_ggd_id DESC";
        }

        if (!empty($limit)) {
            $limit = "LIMIT " . $limit;
        }else{
            $limit = "LIMIT 0,250";
        }

        if (!empty($state)) {
            $state = "mod_ggd_state".$state;
        }else{
            $state = "mod_ggd_state='1'";
        }


        $sql = "SELECT * FROM mod_ganador_ganaderos WHERE ". $state ." ". $order ." ". $limit;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_ggd_id"];
                $return[$i] = $this->dataId($id);
            }
            return $return;
        } else {
            return 0;
        }
    }
}