<?php
header('Content-Type: text/html; charset=utf-8');
class PROYECTOSGA
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function dataId(int $id = null, int $state = 0)
    {
        if ($state != 0) {
            $state = "AND mod_gp_state".$state;
         }else{
            $state = "";
        }

        $sql = "SELECT * FROM mod_ganador_proyectos WHERE mod_gp_id = '$id' ". $state;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_gp_id"];
            $return["id"] = $id;
            $return["name"] = $row["mod_gp_name"];
            $return["description"] = $row["mod_gp_description"];
            $return["details"] = $row["mod_gp_details"];
            $return["ggdId"] = $row["mod_gp_ggd_id"];
            $return["rentabilidad"] = $row["mod_gp_rentabilidad"];
            $return["periodo"] = $row["mod_gp_periodo"];
            $return["dateEndCaptacion"] = $row["mod_gp_date_end_captacion"];
            $return["ticketInversion"] = $row["mod_gp_ticket_inversion"];
            $return["sizeLote"] = $row["mod_gp_size_lote"];
            $return["recaudacionPrevista"] = $row["mod_gp_recaudacion_prevista"];
            $return["gprId"] = $row["mod_gp_gpr_id"];
            $return["dateRegister"] = $row["mod_gp_date_register"];
            $userId = $row["mod_gp_user_id"];
            //$return["userId"] = $row["mod_gp_user_id"];
            $return["user"] = $this->fmt->users->fullName($userId);
            $return["state"] = $row["mod_gp_state"];
            return $return;
        } else {
            return 0;
        }
        
    }
    //creame una funcion que me llame a todos los proyectos en ejecución de la base de datos ganador_1 en la tabla mod_ganador_proyectos
    public function getProyectos($var = [])
    {
        //return $var;
 
        $order = $this->fmt->emptyReturn($var["order"], "");
        $limit = $this->fmt->emptyReturn($var["limit"], "");
        $state = $this->fmt->emptyReturn($var["state"], "");
        $return = [];

        if (!empty($order)) {
            $order = "ORDER BY " . $order;
        }else{
            $order = "ORDER BY mod_gp_id DESC";
        }

        if (!empty($limit)) {
            $limit = "LIMIT " . $limit;
        }else{
            $limit = "LIMIT 0,250";
        }

        if (!empty($state)) {
            $state = "mod_gp_state".$state;
        }else{
            $state = "mod_gp_state='1'";
        }


        $sql = "SELECT * FROM mod_ganador_proyectos WHERE ". $state ." ". $order ." ". $limit;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_gp_id"];
                $return[$i] = $this->dataId($id);
            }
            return $return;
        } else {
            return 0;
        }
    }
}