<?php
$access = json_decode($_POST["accessToken"], true);
require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $access["page"] . "config.php");
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

require_once(_PATH_NUCLEO . "modules/accounts/controllers/class/class.accounts.php");
$accounts = new ACCOUNTS($fmt);

require_once(_PATH_NUCLEO . "modules/ecommerce/controllers/class/class.carts.php");
$carts = new CARTS($fmt);


header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");



switch ($_SERVER["REQUEST_METHOD"]) {
 
    case 'POST':
        $access_token = $access['access_token'];
        $refresh_token = $access['refresh_token'];
        $entitieId = $access['entitieId'];
        $action = $fmt->auth->getInput('action');

        //echo json_encode("1:". $action); exit(0);

        $acuId = $accounts->validateUserToken(array("access_token" => $access_token, "refresh_token" => $refresh_token, "idEntitie" => $entitieId));
        if ($acuId) {
            //echo "'usario validado";
            //echo json_encode("user: ".$userId.": actions: ".$actions); exit(0);

            $return["user"] = $fmt->users->userDataAuth(array("access_token" => $access_token, "idEntitie" => $entitieId));
            $userId = $return["user"]["userId"];
            $rolId = $return["user"]["rolId"];

            if ($action == "createCart") {
                //echo json_encode(1); exit(0);
                $vars = json_decode($_POST['vars'], true);
                //echo json_encode($vars);
                echo json_encode($carts->createCart(array("acuId" => $acuId, "rolId" => $rolId, "entitieId" => $entitieId, "vars" => $vars)));
                exit(0);
            }

            if ($action == "updateCartGateway") {
                $vars = json_decode($_POST['vars'], true);
                //echo json_encode($vars);
                echo json_encode($carts->updateCartGateway(array("acuId" => $acuId, "rolId" => $rolId, "entitieId" => $entitieId, "vars" => $vars)));
                exit(0);
            }
        } else {
            echo $fmt->errors->errorJson([
                "description" => "Access Auth. invalid user",
                "code" => "",
                "lang" => "es"
            ]);
            exit(0);
        }

        break;

    default:
        echo $fmt->errors->errorJson([
            "description" => "Access Auth. Metod request.",
            "code" => "",
            "lang" => "es"
        ]);
        exit(0);
        break;
}
