<?php
header('Content-Type: text/html; charset=utf-8');
class CARTS
{

    var $fmt;
    var $pla;
    var $cat;
    var $accounts;

    function __construct($fmt)
    {
        $this->fmt = $fmt;

        require_once(_PATH_NUCLEO . "modules/accounts/controllers/class/class.accounts.php");
        $this->accounts = new ACCOUNTS($fmt);
    }

    public function haveCartId(Int $cartId = null)
    {
        $sql = "SELECT mod_cart_id FROM mod_carts WHERE mod_cart_id='" . $cartId  . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_cart_id"];
        } else {
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function haveProductCartId(Int $cartId = null)
    {
        $sql = "SELECT mod_prod_id FROM mod_carts_products,mod_carts,mod_products  WHERE mod_cart_prod_cart_id='" . $cartId . "' AND mod_cart_prod_prod_id = mod_prod_id AND mod_prod_state > 1";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function updateCartGateway(array  $vars = null)
    {
        $acuId = $vars["acuId"];
        $var = $vars["vars"];
        $cartId = $var["cartId"];
        $codeReceipts = $var["codigo_recaudacion"]; //mod_cart_code_receipts
        $transactionId = $var["id_transaccion"]; //mod_cart_transaction_id
        $urlGatewayPayment = $var["url_pasarela_pagos"]; //mod_cart_url_gateway_payment	
        $gatewayPayment = $var["gatewayPayment"]; //mod_cart_gateway_payment	

        $sql = "UPDATE mod_carts SET
           mod_cart_code_receipts='" . $codeReceipts . "',
           mod_cart_transaction_id='" . $transactionId . "',
           mod_cart_gateway_url_payment	='" . $urlGatewayPayment . "',
           mod_cart_gateway_payment='" . $gatewayPayment . "'
           WHERE mod_cart_id ='" . $cartId . "' AND mod_cart_acu_id='" . $acuId . "'";
        $this->fmt->querys->consult($sql);

        return $var;
    }

    public function updateCallback(array $var = null)
    {
        //return $var;
        $code = $var["code"];
        $gatewayTransactionId = $var["gatewayTransactionId"];
        $gatewayJSON = $var["gatewayJSON"];
        $gatewayInvoiceUrl = $var["gatewayInvoiceUrl"];

        $sql = "UPDATE mod_carts SET
           mod_cart_gateway_json ='" . $gatewayJSON. "',
           mod_cart_gateway_url_invoice='" . $gatewayInvoiceUrl . "',
           mod_cart_state='1'
           WHERE mod_cart_code ='" . $code . "' AND mod_cart_transaction_id='" . $gatewayTransactionId . "' ";
        $this->fmt->querys->consult($sql);
        $return["Error"]=0;
        return $return;
    }

    public function createCart(array $vars = null)
    {
        //return $vars;
        /* addId: "1"
        addName: "Ballivián 153, Santa Cruz de la Sierra, Bolivia"
        celular: "75313126"
        concept: "Delivery Dento del Segundo Anillo"
        deliveryId: "1"
        details: "Detalles"
        nit: "75313126"
        paymentMethod: "multipay"
        price: "10.00"
        quantity: 1
        razonSocial: "Wappcom Srl"
        typeMoney: "Bs" */

        $acuId = $vars['acuId'];
        $var = $vars['vars'];
        $addId = $var['addId'];
        $celular = $var['celular'];
        $razonSocial = $var['razonSocial'];
        $nit = $var['nit'];
        $details = $var['details'];
        $paymentMethod = $var['paymentMethod'];
        $deliveryId = $var['deliveryId'];
        $products = $var['products'];
        $concept = $var['concept'];
        $price = $var['price'];
        $quantity = $var['quantity'];

        //Update data account
        if (!$this->accounts->updateData($acuId, "mod_acu_celular", $celular)) {
            return 0;
        }

        $invoiceDataId = $this->accounts->createInvoiceData(array('acuId' => $acuId, 'razonSocial' => $razonSocial, 'nit' => $nit));

        if ($invoiceDataId == 0) {
            return 0;
        }

        $code = $this->keyCode();

        $today = $this->fmt->modules->dateFormat();

        /* mod_cart_id	
            mod_cart_date	
            mod_cart_acu_id	
            mod_cart_code_receipts	
            mod_cart_transaction_id	
            mod_cart_url_gateway_payment	
            mod_cart_add_id	
            mod_cart_ldz_id	
            mod_cart_details	
            mod_cart_payment_method	
            mod_cart_invoice_id	
            mod_cart_state */

        $ingresar = "mod_cart_date,mod_cart_acu_id,mod_cart_code,mod_cart_add_id,mod_cart_details,mod_cart_payment_method,mod_cart_ldz_id,mod_cart_invoice_id,mod_cart_state";
        $valores  = "'" . $today . "','"
            . $acuId . "','"
            . $code . "','"
            . $addId . "','"
            . $details . "','"
            . $paymentMethod . "','"
            . $deliveryId . "','"
            . $invoiceDataId  . "','0'";
        $sql = "insert into mod_carts (" . $ingresar . ") values (" . $valores . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_cart_id) as id from mod_carts";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $cartId = $row["id"];

        $this->createRelationCartProducts($products, $cartId);

        $return["Error"] = 0;
        $return["cartId"] = $cartId;
        $return["code"] = $code;
        return $return;
    }

    public function keyCode(string  $code = null)
    {
        $code = base64_encode(uniqid(microtime(), true));

        $sql = "SELECT mod_cart_id FROM mod_carts WHERE mod_cart_code='" . $code . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $this->keyCode();
        } else {
            return $code;
        }
    }

    public function createRelationCartProducts(array  $vars = null, int $cartId = null)
    {
        //return $vars;
        $count = count($vars);
        $i = 0;
        foreach ($vars as $indice => $valor) {
            $i++;
            $ingresar = "mod_cart_prod_cart_id,mod_cart_prod_prod_id,mod_cart_prod_price,mod_cart_prod_order";
            $valores  = "'" . $cartId . "','" . $indice . "','" . $vars[$indice]["price"]  . "','" . $i . "'";
            $sql = "insert into mod_carts_products (" . $ingresar . ") values (" . $valores . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }
        return 1;
    }
}