<?php
header('Content-Type: text/html; charset=utf-8');
class COMPETITIONS
{

    var $fmt;
    var $accounts;

    function __construct($fmt)
    {

        require_once (_PATH_NUCLEO . "modules/accounts/models/class/class.accounts.php");
        $this->fmt = $fmt;
        $this->accounts = new ACCOUNTS($fmt);

    }

    public function dataId(int $id = null, int $state = 0)
    {
        // Prepare the SQL query
        $sql = "SELECT * FROM mod_competitions WHERE mod_cmp_id = '" . $id . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $return = [];
            $return["id"] = $row["mod_cmp_id"];
            $return["name"] = $row["mod_cmp_name"];
            $return["path"] = $row["mod_cmp_path"];
            $return["description"] = $row["mod_cmp_description"];
            $return["date_init"] = $row["mod_cmp_date_init"];
            $return["date_end"] = $row["mod_cmp_date_end"];
            $return["ent_id"] = $row["mod_cmp_ent_id"];
            $return["status"] = $row["mod_cmp_status"];
            
            return $return;
        } else {
            return 0;
        }
    }

    public function getCompetitions(array $var = null)
    {
        //return $var;

        $entitieId = $var["entitieId"];

        $sql = "SELECT * FROM mod_competitions WHERE mod_cmp_ent_id = '" . $entitieId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_cmp_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_cmp_name"];
                $return[$i]["path"] = $row["mod_cmp_path"];
                $return[$i]["description"] = $row["mod_cmp_description"];
                $return[$i]["date_init"] = $row["mod_cmp_date_init"];
                $return[$i]["date_end"] = $row["mod_cmp_date_end"];
                $return[$i]["ent_id"] = $row["mod_cmp_ent_id"];
                $return[$i]["status"] = $row["mod_cmp_status"];
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function getDataCompetition(array $var = null)
    {
        //return $var;

        $entitieId = $var["entitieId"];
        $path = $var["vars"]["inputs"]["path"];


        $sql = "SELECT * FROM mod_competitions_" . $path . " ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_cmp_mira_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_cmp_mira_name"];
                $return[$i]["lastname"] = $row["mod_cmp_mira_lastname"];
                $return[$i]["acu_id"] = $row["mod_cmp_mira_acu_id"];
                $return[$i]["dataAccount"] = $this->accounts->dataAccount($row["mod_cmp_mira_acu_id"]);
                $return[$i]["cumple"] = $row["mod_cmp_mira_cumple"];
                $return[$i]["typeid"] = $row["mod_cmp_mira_typeid"];
                $return[$i]["num_id"] = $row["mod_cmp_mira_num_id"];
                $return[$i]["nacionalidad"] = $row["mod_cmp_mira_nacionalidad"];
                $return[$i]["direccion"] = $row["mod_cmp_mira_direccion"];
                $return[$i]["ciudad"] = $row["mod_cmp_mira_ciudad"];
                $return[$i]["pais"] = $row["mod_cmp_mira_pais"];
                $return[$i]["websocial"] = $row["mod_cmp_mira_websocial"];
                $return[$i]["type"] = $row["mod_cmp_mira_type"];
                $return[$i]["nombre"] = $row["mod_cmp_mira_nombre"];
                $return[$i]["incluye"] = $row["mod_cmp_mira_incluye"];
                $return[$i]["link_portafolio"] = $row["mod_cmp_mira_link_portafolio"];

                $file_portafolio = $row["mod_cmp_mira_file_portafolio"];

                if ($file_portafolio != 0) {
                    $file_portafolio = $this->fmt->files->urlFileId($file_portafolio);
                }

                $return[$i]["file_portafolio"] = $file_portafolio;


                $return[$i]["titulo"] = $row["mod_cmp_mira_titulo"];
                $return[$i]["tipo_proyecto"] = $row["mod_cmp_mira_tipo_proyecto"];
                $return[$i]["sintesis"] = $row["mod_cmp_mira_sintesis"];
                $return[$i]["espacio"] = $row["mod_cmp_mira_espacio"];

                $fileId = $row["mod_cmp_mira_file_id"];

                if ($fileId != 0) {
                    $fileId = $this->fmt->files->urlFileId($fileId);
                }
                $return[$i]["file_id"] = $fileId;

                $return[$i]["link"] = $row["mod_cmp_mira_link"];

                $file_declaracion = $row["mod_cmp_mira_file_declaracion"];
                if ($file_declaracion != 0) {
                    $file_declaracion = $this->fmt->files->urlFileId($file_declaracion);
                }

                $return[$i]["file_declaracion"] = $file_declaracion;

                $file_ci = $row["mod_cmp_mira_file_ci"];
                if ($file_ci != 0) {
                    $file_ci = $this->fmt->files->urlFileId($file_ci);
                }
                $return[$i]["file_ci"] = $file_ci;
                $return[$i]["informacion"] = $row["mod_cmp_mira_informacion"];
                $return[$i]["date_register"] = $row["mod_cmp_mira_date_register"];
                $return[$i]["state"] = $row["mod_cmp_mira_state"];
            }

            return $return;
        } else {
            return 0;
        }

    }


}