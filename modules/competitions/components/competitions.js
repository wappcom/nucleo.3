import {
    addHtml,
    loadView,
    jointActions,
    accessToken,
    alertMessageError
} from '../../components/functions.js';

let system = "competitions";
let module = "competitions";
let dataCompetition = "";

export const competitionsIndex = (vars = []) => {
    console.log('comptitionsIndex', vars);
    loadView(_PATH_WEB_NUCLEO + "modules/competitions/views/competitions.html?" + _VS).then((responseView) => {
        //console.log('loadView', responseView);
        let content = responseView;

        getData({
            task: 'getCompetitions',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
        }).then((response) => {
            console.log('getData getCompetitions', response);
            if (response.status == 'success') {

                let data = response.data;
                let list = '';
                
                addHtml({
                    selector: `.bodyModule[module="${module}"]`,
                    type: 'append', //insert, append, prepend, replace
                    content
                });

                data.forEach(element => {
                    list += `<a class="btnList btnListCompetition" data-path="${element.path}" data-id="${element.id}">${element.name}</a>`;
                });

                addHtml({
                   selector:`#listCompetitions`,
                   type: 'append', //insert, append, prepend, replace
                    content: list
                })
                
            }else{
                 alertMessageError({message: response.message})
            }
        }).catch(console.warn());

       
        
    }).catch(console.warn());
   
}

export const getData = async (vars = []) => {
   //console.log('getData', vars);
   const url = _PATH_WEB_NUCLEO + "modules/competitions/controllers/apis/v1/competitions.php";
   let data = JSON.stringify({
       accessToken: accessToken(),
       vars: JSON.stringify(vars)
   });
   try {
       let res = await axios({
           async: true,
           method: "post",
           responseType: "json",
           url: url,
           headers: {},
           data: data
        })
       //console.log(res.data);
       jointActions(res.data)
       return res.data
   } catch (error) {
      console.log("Error: getData ", error);
   }
}

document.addEventListener("DOMContentLoaded", function () {
    $("body").on("click",`.btnListCompetition`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnListCompetition`, data);
        $(".btnListCompetition").removeClass('active');
        $(this).addClass('active');
        
        getData({
            task: 'getDataCompetition',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
            inputs: {
                path : data.path
            }
        }).then((response) => {
            console.log('getData getDataCompetition', response);
            if(response.status=='success'){
                let data = response.data;
                let item = '';
                dataCompetition = data;


                data.forEach(element => {
                    item += `<div class="item itemDataCompetition" data-id="${element.id}">
                                <span class="name">${element.name} ${element.lastname} </span> 
                                <span class="date">${element.date_register} </span>
                            </div>`;
                });

                addHtml({
                   selector:`#listCompetitionsForm`,
                   type: 'append', //insert, append, prepend, replace
                   content : item
                })

            }else{
                 alertMessageError({message: response.message})
            }
        }).catch(console.warn());
        
    }); 

    $("body").on("click",`.itemDataCompetition`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.itemDataCompetition`, data);
         $(".itemDataCompetition").removeClass('active');
         $(this).addClass('active');
        
        let id = data.id;
        dataCompetition.find(element => {
            if (element.id == id) {
                console.log(element);

                let file_portafolio = element.file_portafolio;
                let fileId = element.file_id;
                let file_declaracion = element.file_declaracion;
                let file_ci = element.file_ci;

                if (file_portafolio != 0) {
                    file_portafolio = '<a href="'+_PATH_FILES + file_portafolio + '" target="_blank">Ver Portafolio</a>';
                }

                if (fileId != 0) {
                    fileId = '<a href="'+_PATH_FILES + fileId + '" target="_blank">Ver Documento</a>';
                }
                
                if (file_declaracion != 0) {
                    file_declaracion = '<a href="'+_PATH_FILES + file_declaracion + '" target="_blank">Ver Declaración</a>';
                }

                if (file_ci != 0) {
                    file_ci = '<a href="'+_PATH_FILES + file_ci + '" target="_blank">Ver C.I.</a>';
                }

                addHtml({
                    selector: `.boxCompetitionsFormItem`,
                    type: 'insert', //insert, append, prepend, replace
                    content: `
                        <h2>FORMULARIO DE POSTULACIÓN: ${element.name} ${element.lastname}</h2>
                        <p>Cuenta Id: ${element.acu_id}</p>
                        <p>Fecha de Registro: ${element.date_register}</p>
                        <p>Fecha de Cumpleaños: ${element.cumple}</p></br>
                        <p>Tipo de Identificación: ${element.typeid}</p>
                        <p>Número: ${element.num_id}</p>
                        <p>Correo Electronico: ${element.dataAccount.email}</p>
                        <p>Whatsapp: ${element.dataAccount.celular}</p>
                        <p>Dirección: ${element.direccion}</p>
                        <p>Nacionalidad: ${element.nacionalidad}</p>
                        <p>Ciudad: ${element.ciudad}</p>
                        <p>Pais: ${element.pais}</p></br>
                        <p>Web/Red Social: ${element.websocial}</p></br>

                        <p>Tipo: ${element.type}</p>
                        <p>Nombre Artístico: ${element.nombre}</p>
                        <p>Incluye: ${element.incluye}</p>
                        <p>Link portafolio: ${element.link_portafolio}</p></br>
                        <p>File_portafolio: ${ file_portafolio } </p></br>
                        <p><h3>Información del Proyecto/Obra </h3></p></br>
                        <p><b>Titulo:</b> ${element.titulo}</p>
                        <p><b>Tipo del proyecto:</b> ${element.type}</p>
                        <p><b>Sintesis:</b> ${element.sintesis}</p>
                        <p><b>Espacio expositivo sugerido:</b> ${element.espacio}</p></br>
                        <p><b>Imagenes relacionadas:</b> ${fileId}</p></br>
                        <p><b>Link Video:</b> ${element.link}</p></br>
                        <p><b>File_declaración:</b> ${file_declaracion}</p></br>
                        <p><b>File_CI:</b> ${file_ci}</p></br>

                        <p><b>Información:</b> ${element.informacion}</p></br>
                    `
                });
            }
        });
        
    });

});