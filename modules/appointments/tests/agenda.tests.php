<?php

class AGENDA_TESTS {
    private $fmt;
    private $tables = [
        'mod_persons',
    ];

    public function __construct($fmt) {
        $this->fmt = $fmt;
    }

    /**
     * Limpia todas las tablas de prueba
     * @return array Resultado de la operación
     */
    public function cleanTestTables() {
        try {
            foreach ($this->tables as $table) {
                $sql = "TRUNCATE TABLE $table";
                $this->fmt->querys->consult($sql, __METHOD__);
            }
            
            return [
                'status' => 'success',
                'message' => [
                    'es' => 'Tablas de prueba limpiadas correctamente',
                    'en' => 'Test tables cleaned successfully'
                ]
            ];
        } catch (Exception $e) {
            return [
                'status' => 'error',
                'message' => [
                    'es' => 'Error al limpiar tablas de prueba: ' . $e->getMessage(),
                    'en' => 'Error cleaning test tables: ' . $e->getMessage()
                ]
            ];
        }
    }

    /**
     * Inserta datos de prueba en las tablas
     * @return array Resultado de la operación
     */
    public function insertTestData() {
        try {
            // Insertar tipos de persona de prueba
            $personTypes = [
                ['LEAD', 'Lead Test', 'Lead for testing'],
                ['CUSTOMER', 'Customer Test', 'Customer for testing'],
                ['PROVIDER', 'Provider Test', 'Provider for testing']
            ];

            foreach ($personTypes as $type) {
                $sql = "INSERT INTO mod_persons_types (mod_person_type_code, mod_person_type_name, mod_person_type_description) 
                        VALUES ('{$type[0]}', '{$type[1]}', '{$type[2]}')";
                $this->fmt->querys->consult($sql, __METHOD__);
            }

            // Insertar personas de prueba
            $persons = [
                [
                    'name' => 'John',
                    'lastname_father' => 'Doe',
                    'lastname_mother' => 'Smith',
                    'email' => 'john.doe@test.com',
                    'phone' => '1234567890',
                    'type' => 'LEAD'
                ],
                [
                    'name' => 'Jane',
                    'lastname_father' => 'Doe',
                    'lastname_mother' => 'Johnson',
                    'email' => 'jane.doe@test.com',
                    'phone' => '0987654321',
                    'type' => 'CUSTOMER'
                ]
            ];

            foreach ($persons as $person) {
                $sql = "INSERT INTO mod_persons (
                            mod_person_name, 
                            mod_person_lastname_father,
                            mod_person_lastname_mother,
                            mod_person_email,
                            mod_person_phone,
                            mod_person_type,
                            mod_person_ent_id,
                            mod_person_state
                        ) VALUES (
                            '{$person['name']}',
                            '{$person['lastname_father']}',
                            '{$person['lastname_mother']}',
                            '{$person['email']}',
                            '{$person['phone']}',
                            '{$person['type']}',
                            1,
                            1
                        )";
                $this->fmt->querys->consult($sql, __METHOD__);
            }

            return [
                'status' => 'success',
                'message' => [
                    'es' => 'Datos de prueba insertados correctamente',
                    'en' => 'Test data inserted successfully'
                ]
            ];
        } catch (Exception $e) {
            return [
                'status' => 'error',
                'message' => [
                    'es' => 'Error al insertar datos de prueba: ' . $e->getMessage(),
                    'en' => 'Error inserting test data: ' . $e->getMessage()
                ]
            ];
        }
    }

    /**
     * Verifica la existencia de una persona
     * @param array $data Datos de la persona a verificar
     * @return array Resultado de la verificación
     */
    public function testPersonExists($data) {
        try {
            $sql = "SELECT COUNT(*) as count 
                    FROM mod_persons 
                    WHERE mod_person_email = '{$data['email']}'
                    OR (
                        mod_person_name = '{$data['name']}'
                        AND mod_person_lastname_father = '{$data['lastname_father']}'
                        AND mod_person_lastname_mother = '{$data['lastname_mother']}'
                    )";
            
            $rs = $this->fmt->querys->consult($sql, __METHOD__);
            $row = $this->fmt->querys->row($rs);
            
            return [
                'status' => 'success',
                'exists' => $row['count'] > 0,
                'message' => [
                    'es' => $row['count'] > 0 ? 'La persona existe' : 'La persona no existe',
                    'en' => $row['count'] > 0 ? 'Person exists' : 'Person does not exist'
                ]
            ];
        } catch (Exception $e) {
            return [
                'status' => 'error',
                'message' => [
                    'es' => 'Error al verificar existencia: ' . $e->getMessage(),
                    'en' => 'Error checking existence: ' . $e->getMessage()
                ]
            ];
        }
    }

    /**
     * Ejecuta todos los tests
     * @return array Resultado de todos los tests
     */
    public function runAllTests() {
        $results = [];
        
        // Limpiar tablas
        $results['clean'] = $this->cleanTestTables();
        
        // Insertar datos de prueba
        $results['insert'] = $this->insertTestData();
        
        // Probar existencia de persona
        $testPerson = [
            'name' => 'John',
            'lastname_father' => 'Doe',
            'lastname_mother' => 'Smith',
            'email' => 'john.doe@test.com'
        ];
        $results['exists'] = $this->testPersonExists($testPerson);
        
        return $results;
    }
}
