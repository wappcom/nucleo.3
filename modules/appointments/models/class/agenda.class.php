<?php
header( 'Content-Type: text/html; charset=utf-8' );

class AGENDA {
    private $fmt;
    private $agendaTests;

    public function __construct($fmt) {
        $this->fmt = $fmt;
        require_once _PATH_NUCLEO . 'modules/appointments/tests/agenda.tests.php';
        $this->agendaTests = new AGENDA_TESTS($this->fmt);
    }

    public function getPersonsList(array $var = null)
    {
        //return $var;
        // Validación de parámetros
        $vars = $var['vars'] ?? [];
        $test = isset($vars['test']) ? $vars['test'] : 0;
        $entitieId = $var['entitieId'] ?? 1;

        // Limpiar tabla si se solicita (modo prueba)
        if ($test === 'clean') {
            $this->agendaTests->cleanTestTables();
        }

        $return = [];
        $sql = "SELECT * FROM mod_persons WHERE mod_person_ent_id='" . $entitieId . "'";
        
        // Filtro de estado
        if (isset($vars['state'])) {
            $sql .= " AND mod_person_state = '" . $vars['state'] . "'";
        } else {
            $sql .= " "; // Por defecto solo activos
        }
        
        if (isset($vars['search'])) {
            $sql .= " AND (mod_person_name LIKE '%" . $vars['search'] . "%' 
                     OR mod_person_lastname_father LIKE '%" . $vars['search'] . "%'
                     OR mod_person_lastname_mother LIKE '%" . $vars['search'] . "%'
                     OR mod_person_email LIKE '%" . $vars['search'] . "%'
                     OR mod_person_phone LIKE '%" . $vars['search'] . "%')";
        }

        if (isset($vars['type'])) {
            $sql .= " AND mod_person_type = '" . $vars['type'] . "'";
        }

        try {
            $rs = $this->fmt->querys->consult($sql, __METHOD__);
            $num = $this->fmt->querys->num($rs);

            if ($num > 0) {
                for ($i = 0; $i < $num; $i++) {
                    $row = $this->fmt->querys->row($rs);
                    $id = $row["mod_person_id"];
                    $return[$i]["id"] = $id;
                    $return[$i]["name"] = $row["mod_person_name"];
                    $return[$i]["lastnameFather"] = $row["mod_person_lastname_father"];
                    $return[$i]["lastnameMother"] = $row["mod_person_lastname_mother"];
                    $return[$i]["email"] = $row["mod_person_email"];
                    $return[$i]["phoneDial"] = $row["mod_person_phone_dial"];
                    $return[$i]["phone"] = $row["mod_person_phone"];
                    $return[$i]["entitieId"] = $row["mod_person_ent_id"];
                    $return[$i]["type"] = $row["mod_person_type"];
                    $return[$i]["state"] = $row["mod_person_state"];
                    $return[$i]["comments"] = $row["mod_person_comments"];
                    $return[$i]["metadata"] = json_decode($row["mod_person_metadata"], true);
                    $return[$i]["customClass"] = $row["mod_person_custom_class"];
                    $return[$i]["customId"] = $row["mod_person_custom_id"];
                    $return[$i]["createdAt"] = $row["mod_person_created_at"];
                    $return[$i]["updatedAt"] = $row["mod_person_updated_at"];
                }
                return [
                    'Error' => 0,
                    'status' => 'success',
                    'data' => $return
                ];
            } else {
                return [
                    'Error' => 0,
                    'status' => 'success',
                    'data' => 0
                ];
            }
        } catch (Exception $e) {
            return [
                'status' => 'error',
                'message' => $e->getMessage(),
                'Error' => 1
            ];
        }
    }

    public function getParameters(array $var = null)
    {
        //return $var;
        $rtn["personsTypes"] =  $this->getTypes();
        return $rtn;
        
    }

    public function getTypes()
    {
        $sql = "SELECT * FROM mod_persons_types ORDER BY mod_person_type_id ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_person_type_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_person_type_name"];
                $return[$i]["code"] = $row["mod_person_type_code"];
                $return[$i]["description"] = $row["mod_person_type_description"];
                $return[$i]["metadata"] = json_decode($row["mod_person_type_metadata"], true);
                $return[$i]["state"] = $row["mod_person_type_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function reviewPerson(array $var = null)
    {
        $name = $var["vars"]["inputs"]["inputName"];
        $lastNameFather = $var["vars"]["inputs"]["inputLastnameFather"];
        $lastNameMother = $var["vars"]["inputs"]["inputLastnameMother"];
        $email = $var["vars"]["inputs"]["inputEmail"];

        $sql = "SELECT COUNT(*) as total FROM mod_persons 
                WHERE (mod_person_name = '$name' 
                AND mod_person_lastname_father = '$lastNameFather' 
                AND mod_person_lastname_mother = '$lastNameMother')
                OR mod_person_email = '$email'";
        
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        
        return ($row['total'] > 0) ? 1 : 0;
    }

    public function addPerson(array $var = null)
    {
        //return $var;
        // Verificar si la persona ya existe
        $exists = $this->reviewPerson($var);
        if ($exists) {
            return [
                'status' => 'error',
                'message' => 'La persona ya existe en el sistema',
                'Error' => 1
            ];
        }

        $entitieId = $var["entitieId"];
        $name = $var["vars"]["inputs"]["inputName"];
        $lastNameFather = $var["vars"]["inputs"]["inputLastnameFather"];
        $lastNameMother = $var["vars"]["inputs"]["inputLastnameMother"];
        $email = $var["vars"]["inputs"]["inputEmail"];
        $phoneDial = $var["vars"]["inputs"]["inputPhoneDial"]; 
        $phone = $var["vars"]["inputs"]["inputPhone"];
        $type = $var["vars"]["inputs"]["inputType"];
        $state = $var["vars"]["inputs"]["inputState"] ? 0 : 1;
        $comments = $var["vars"]["inputs"]["inputComments"];
        $metadata = $var["vars"]["inputs"]["inputMetadata"];
        $customClass = $var["vars"]["inputs"]["inputCustomClass"];
        $customId = $var["vars"]["inputs"]["inputCustomId"];

        $insert = "mod_person_name,mod_person_lastname_father,mod_person_lastname_mother,mod_person_email,mod_person_phone_dial,mod_person_phone,mod_person_ent_id,mod_person_type,mod_person_state,mod_person_comments,mod_person_metadata,mod_person_custom_class,mod_person_custom_id,mod_person_created_at,mod_person_updated_at";
        $values ="'" . $name . "','" . $lastNameFather . "','" . $lastNameMother . "','" . $email . "','".$phoneDial."','" . $phone . "','" . $entitieId . "','" . $type . "','" . $state . "','" . $comments . "','" . $metadata . "','" . $customClass . "','" . $customId . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "'";
        $sql= "insert into  mod_persons (".$insert.") values (".$values.")";
        $this->fmt->querys->consult($sql,__METHOD__);

        $sql = "select max(mod_person_id) as id from mod_persons";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);    
        $id = $row["id"];

        $rtn["Error"] = 0;
        $rtn["status"] = "success";
        $rtn["id"] = $id;
                
        return $rtn;
    }

    public function getDataPerson(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $item = $var["vars"]["item"];

        $sql = "SELECT * FROM mod_persons WHERE mod_person_id='" . $item . "'";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_person_id"];
            $return["id"] = $id;
            $return["name"] = $row["mod_person_name"];
            $return["lastnameFather"] = $row["mod_person_lastname_father"];
            $return["lastnameMother"] = $row["mod_person_lastname_mother"];
            $return["email"] = $row["mod_person_email"];
            $return["phoneDial"] = $row["mod_person_phone_dial"];
            $return["phone"] = $row["mod_person_phone"];
            $return["entitieId"] = $row["mod_person_ent_id"];
            $return["type"] = $row["mod_person_type"];
            $return["types"] = $this->getTypes();
            $return["state"] = $row["mod_person_state"];
            $return["comments"] = $row["mod_person_comments"];
            $return["metadata"] = json_decode($row["mod_person_metadata"], true);
            $return["customClass"] = $row["mod_person_custom_class"];
            $return["customId"] = $row["mod_person_custom_id"];
            $return["createdAt"] = $row["mod_person_created_at"];
            $return["updatedAt"] = $row["mod_person_updated_at"];
            
            return $return;
        } else {
            return 0;
        }
        
    }

    public function deletePerson(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $inputs = $var["vars"]["inputs"];
        $inputId = $inputs["inputId"];

        $sql = "DELETE FROM mod_persons WHERE mod_person_id = '" .  $inputId . "'";
        $this->fmt->querys->consult($sql, __METHOD__);
        $sql = "ALTER TABLE mod_persons AUTO_INCREMENT = 1";
        $this->fmt->querys->consult($sql, __METHOD__);

        return 1;
        
    }

}
