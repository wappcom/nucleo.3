<?php

header( 'Content-Type: text/html; charset=utf-8' );

class CALENDARS 
{

    var $services = '';
    var $accounts;
    var $agenda = '';
    var $fmt = '';

    function __construct($fmt)
    {
        $this->fmt = $fmt;
        require_once (_PATH_NUCLEO . "modules/services/models/class/services.class.php");
        require_once (_PATH_NUCLEO . "modules/accounts/models/class/class.accounts.php");
        require_once (_PATH_NUCLEO . "modules/appointments/models/class/agenda.class.php");
        $this->agenda = new AGENDA($fmt);
        $this->services = new SERVICES($fmt);
        $this->accounts = new ACCOUNTS($fmt);
    }

    public function dataId($id = null)
    {
        $sql = "SELECT * FROM mod_appointments_calendars WHERE mod_apt_cal_id = '" . $id . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $return = [
                'id' => $row['mod_apt_cal_id'],
                'cpeId' => $row['mod_apt_cal_cpe_id'],
                'personId' => $row['mod_apt_cal_person_id'],
                'acuId' => $row['mod_apt_cal_acu_id'],
                'providerId' => $row['mod_apt_cal_provider_id'],
                'srvId' => $row['mod_apt_cal_srv_id'],
                'schId' => $row['mod_apt_cal_sch_id'],
                'dateStart' => $row['mod_apt_cal_date_start'],
                'dateEnd' => $row['mod_apt_cal_date_end'],
                'reschedule' => $row['mod_apt_cal_reschedule'],
                'reasonCancel' => $row['mod_apt_cal_reason_cancel'],
                'title' => $row['mod_apt_cal_title'],
                'allDay' => $row['mod_apt_cal_all_day'],
                'place' => $row['mod_apt_cal_place'],
                'guests' => $row['mod_apt_cal_guests'],
                'status' => $row['mod_apt_cal_status'],
                'type' => $row['mod_apt_cal_type'],
                'entId' => $row['mod_apt_cal_ent_id'],
                'userId' => $row['mod_apt_cal_user_id'],
                'flags' => $row['mod_apt_cal_flags'],
                'details' => $row['mod_apt_cal_details'],
                'createdAt' => $row['mod_apt_cal_created_at'],
                'updatedAt' => $row['mod_apt_cal_updated_at'],
                'state' => $row['mod_apt_cal_state']
            ];
            return $return;
        } else {
            return 0;
        }
    }

    public function getDataCalendars(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $arrayServices = $this->services->getServicesAll($var);
        $rolId = $var["user"]["rolId"];
        $userId = $var["user"]["userId"];
        $numAgents = 0;
        $agents = [];
        $flatAgents = [];


        for ($i=0; $i < count($arrayServices); $i++) { 
            $id= $arrayServices[$i]["id"];
            $arrayServices[$i]["id"] = $id;
            $agent = $this->getUsersRolesToServices($id, $entitieId);
            $numAgent = count($agent);
            $arrayServices[$i]["agents"] = $agent;
            $numAgents += $numAgent;
            $agents[$i] = $agent;
            
            // Add agents to flat list
            if (is_array($agent)) {
                foreach ($agent as $user) {
                    $flatAgents[] = $user; // Add each user in order without using their id as the key
                }
            }
        }
        

        // Convert associative array back to indexed array
        //$flatAgents = array_values($flatAgents);
        if ($rolId == 1){
            $rtn['services'] = $arrayServices;
            $rtn['numAgents'] = count($flatAgents);
            $rtn['agents'] = $flatAgents;
            $rtn['agenda'] = $this->agenda->getPersonsList($var); // $listAccounts;
            $rtn['typesAppointments'] = $this->getTypesAppointments($var);
        }

        #arreglar
        if ($rolId == 11 || $rolId == 12){
            $rtn['services'] = $arrayServices;
            $rtn['numAgents'] = 1;
            //$rtn['agents'] = [$var["user"]];
            $user = [];
            $flatAgents = [];
            $user = $this->fmt->users->getDataId($userId);
            $user['rolId'] = $rolId;
            $user['dataRol'] = $this->fmt->users->dataRolId($rolId);
            $user['serviceId'] = $this->getServiceIdToRolId($rolId);
            $flatAgents[0] = $user; // Add each user in order without using their id as the key
            $rtn['agents'] = $flatAgents;
            $rtn['agenda'] = $this->agenda->getPersonsList($var); // $listAccounts;
            $rtn['typesAppointments'] = $this->getTypesAppointments($var);
        }
        return $rtn; 
    }

 
    public function getUsersRolesToServices(int $serviceId = null, int $entitieId = null)
    {
        //return $var;

        $sql = "SELECT mod_srv_rol_id FROM mod_services_roles WHERE mod_srv_id='" . $serviceId . "'";
        $rs = $this->fmt->querys->consult($sql,__METHOD__);
        $num = $this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row = $this->fmt->querys->row($rs);
                $rolId = $row["mod_srv_rol_id"];
                $users = $this->fmt->users->usersRol($rolId, $entitieId);
                $dataRol = $this->fmt->users->dataRolId($rolId);
                
                if (is_array($users)) {
                    foreach ($users as &$user) {
                        $user['rolId'] = $rolId;
                        $user['dataRol'] = $dataRol;
                        $user['serviceId'] = $serviceId;
                        $return[] = $user;
                    }
                }
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function getServiceIdToRolId($rolId = 0)
    {
        //return $var;
        $sql = "SELECT mod_srv_id FROM mod_services_roles WHERE mod_srv_rol_id='". $rolId. "'";
        $rs = $this->fmt->querys->consult($sql,__METHOD__);
        $num= $this->fmt->querys->num($rs);
        if($num>0){
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_srv_id"];
            return $id;
        } else {
            return 0;
        }
    }


    public function getValue($name)
    {
        $sql = "SELECT mod_apt_opt_value FROM mod_appointments_options WHERE mod_apt_opt_name='" . $name . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        return $row["mod_apt_opt_value"];
    }

    public function getName($id)
    {
        $sql = "SELECT mod_apt_opt_name FROM mod_appointments_options WHERE mod_apt_opt_id='" . $id . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        return $row["mod_adv_opt_name"];
    }


    public function getDataDaysBlocked(array $var = null)
    {

        //return $var;
        $entitieId = $var["entitieId"];

        $sql = "SELECT * FROM mod_appointments_calendars_blocked WHERE mod_apt_cb_state = '1' AND mod_apt_cb_ent_id = '" . $entitieId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_apt_cb_id"];
                $return[$i]["id"] = $id;
                $dateArray = explode(" ", $row["mod_apt_cb_date_start"]);
                $dateArrayEnd = explode(" ", $row["mod_apt_cb_date_end"]);
                $dateExplode = explode("-", $dateArray[0]);
                $hourEnd = substr($dateArrayEnd[1], 0, -3);
                $hourStart = substr($dateArray[1], 0, -3);
                $return[$i]["date"] = $dateArray[0];
                $return[$i]["dateYear"] = $dateExplode[0];
                $return[$i]["dateMonth"] = $dateExplode[1];
                $return[$i]["dateDay"] = $dateExplode[2];
                $return[$i]["hourEnd"] = $hourEnd;
                $return[$i]["hourStart"] = $hourStart;
                $return[$i]["dateStart"] = $row["mod_apt_cb_date_start"];
                $return[$i]["dateEnd"] = $row["mod_apt_cb_date_end"];
                if ($hourStart == '00:00' && $hourEnd == '23:45') {
                    $allday = 1;
                } else {
                    $allday = 0;
                }
                $return[$i]["allday"] =   $allday;
            }
            return $return;
        } else {
            return 0;
        }

        return 0;
        
    }


    public function getDataSchedulesAgent(array $var = null)
    {
        //return $var;
        $userId = $var["user_id"];
        $entitieId = $var["entitieId"];
        $agentId = $var["vars"]["item"];

        $now = $this->fmt->modules->dateFormat();
        $lastMonth = date("Y-m-d", strtotime($now . "-1 month"));

        $sql = "SELECT *
                FROM mod_appointments_calendars
                WHERE mod_apt_cal_user_id = '" . $agentId . "' 
                AND mod_apt_cal_state = 1 
                AND mod_apt_cal_date_start > '" . $lastMonth . "'";

        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $return = [];
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return[$i]["id"] = $row["mod_apt_cal_id"];
                $return[$i]["acuId"] = $row["mod_apt_cal_acu_id"];
                $return[$i]["personId"] = $row["mod_apt_cal_person_id"];
                $return[$i]["cpeId"] = $row["mod_apt_cal_cpe_id"];

                $return[$i]["providerId"] = $row["mod_apt_cal_provider_id"];
                
                $return[$i]["dateStart"] = $row["mod_apt_cal_date_start"];
                $return[$i]["dateEnd"] = $row["mod_apt_cal_date_end"];
                $return[$i]["title"] = $row["mod_apt_cal_title"];
                $return[$i]["allDay"] = $row["mod_apt_cal_all_day"];
                $return[$i]["place"] = $row["mod_apt_cal_place"];
                $return[$i]["status"] = $row["mod_apt_cal_status"];
                $type = $row["mod_apt_cal_type"];
                $arrayType = $this->getTypesAppointmentsByPath(["path" => $type, "entitieId" => $entitieId]);
                $return[$i]["dataType"] = $arrayType;
                $return[$i]["type"] = $type;
                $return[$i]["details"] = $row["mod_apt_cal_details"];
                $return[$i]["guests"] = $row["mod_apt_cal_guests"];
                $return[$i]["hourStart"] = $row["mod_apt_sch_hour_start"];
                $return[$i]["hourEnd"] = $row["mod_apt_sch_hour_end"];
                $return[$i]["agentName"] = $this->fmt->users->fullName($row["mod_apt_cal_provider_id"], 1);

                //switch
                $return[$i]["name"] = $this->accounts->fullName($row["mod_apt_cal_acu_id"]);
                $return[$i]["dataAcuId"] = $this->accounts->dataAccount($account);
                $return[$i]["linkCelular"] = $this->fmt->options->getValue("link_celular");
            }
            return $return;
        } else {
            return 0;
        }
    }


    public function getTypesAppointments(array $var = null)
    {
        $vars = $var["vars"];
        $userId = $var["user"]["userId"];
        $entitieId = $var["entitieId"];

        $sql = "SELECT * FROM mod_appointments_types WHERE mod_apt_type_ent_id='" . $entitieId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_apt_type_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_apt_type_name"];
                $return[$i]["summary"] = $row["mod_apt_type_summary"];
                $return[$i]["type"] = $row["mod_apt_type_type"];
                $return[$i]["path"] = $row["mod_apt_type_path"];
                $return[$i]["color"] = $row["mod_apt_type_color"] ? $row["mod_apt_type_color"] : "#a1c0fd";
                $return[$i]["json"] = $row["mod_apt_type_json"];
                $return[$i]["state"] = $row["mod_apt_type_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function getTypesAppointmentsById(array $var = null)
    {
        $vars = $var["vars"];
        $userId = $var["user"]["userId"];
        $entitieId = $var["entitieId"];
        $id = $var["id"];

        $sql = "SELECT * FROM mod_appointments_types WHERE mod_apt_type_ent_id='" . $entitieId . "' and mod_apt_type_id='" . $id . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $return["id"] = $id;
            $return["name"] = $row["mod_apt_type_name"];
            $return["summary"] = $row["mod_apt_type_summary"];
            $return["type"] = $row["mod_apt_type_type"];
            $return["path"] = $row["mod_apt_type_path"];
            $return["color"] = $row["mod_apt_type_color"] ? $row["mod_apt_type_color"] : "#a1c0fd";
            $return["json"] = $row["mod_apt_type_json"];
            $return["state"] = $row["mod_apt_type_state"];
            return $return;
        } else {
            return 0;
        }
    }

    public function getTypesAppointmentsByPath(array $var = null)
    {
        //return $var;
        $vars = $var["vars"];
        $userId = $var["user"]["userId"];
        $entitieId = $var["entitieId"];
        $path = $var["path"];

        $sql = "SELECT * FROM mod_appointments_types WHERE mod_apt_type_ent_id='" . $entitieId . "' and mod_apt_type_path='" . $path . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_apt_type_id"];
            $return["id"] = $id;
            $return["name"] = $row["mod_apt_type_name"];
            $return["summary"] = $row["mod_apt_type_summary"];
            $return["kind"] = $row["mod_apt_type_kind"];
            $return["path"] = $row["mod_apt_type_path"];
            $return["bgColor"] = $row["mod_apt_type_bg_color"] ? $row["mod_apt_type_bg_color"] : "#a1c0fd";
            $return["textColor"] = $row["mod_apt_type_text_color"] ? $row["mod_apt_type_text_color"] : "#000000";
            $return["borderColor"] = $row["mod_apt_type_border_color"] ? $row["mod_apt_type_border_color"] : "#000000";
            $return["json"] = $row["mod_apt_type_json"];
            $return["state"] = $row["mod_apt_type_state"];
            return $return;
        } else {
            return 0;
        }
    }

    public function addSchedule(array $var = null)
    {
        //return $var;
        $userId = $var["userId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];

        $providerId = $vars["inputProviderId"] ?? '';
        $userId = $vars["inputUserId"] ?? '';
        $acuId = $vars["inputAccount"] ?? '';
        $acuLabel = $vars["inputAccountLabel"] ?? '';
        $hourStart = $vars["inputHourStart"] ?? '';
        $hourEnd = $vars["inputHourEnd"] ?? '';

        $dateStart = $vars["inputDateStart"];
        $dateEnd = $vars["inputDateEnd"];
        $place = $vars["inputPlace"];
        $others = $vars["inputOthers"];
        $comments = $vars["inputComments"];
        $typeAppointment = $vars["inputTypeAppointment"] ?? 'appointment';
        $flag = $vars["flag"] ?? '';

        if ($dateEnd == "") {
            $dateEnd = $dateStart;
        } 

        $start = $dateStart . " " . $hourStart;
        $end = $dateEnd . " " . $hourEnd;

        if (!empty($others)) {
            $place = $others;
        }

        if ($start == "" || $end == "" || $start=="0000-00-00 00:00:00" || $end=="0000-00-00 00:00:00") {
            $rtn["Error"] = 1;
            $rtn["status"] = 'error';
            $rtn["message"] = "La fecha no esta definida, revisar fecha por favor, intenta denuevo";
            return $rtn;
        }

        $responseDaysBlocked = $this->verifyDaysBlocked(array(
            "entitieId" => $entitieId,
            "dateStart" => $start,
            "dateEnd" => $end,
            "providerId" => $providerId,
            "userId" => $userId,
            "acuId" => $acuId
        ));

        if ($responseDaysBlocked != 0) {
            $rtn["Error"] = 1;
            $rtn["message"] = "La fecha seleccionada no se encuentra disponible, intenta denuevo";
            return $rtn;
        }

        $responseVerify = $this->verifyDate(array(
            "entitieId" => $entitieId,
            "dateStart" => $start,
            "dateEnd" => $end,
            "providerId" => $providerId,
            "userId" => $userId,
            "acuId" => $acuId
        ));

        if ($responseVerify != 0) {
            $rtn["Error"] = 1;
            $message = "";
            for ($i = 0; $i < count($responseVerify); $i++) {
                $message .= $responseVerify[$i]['title'] . " - " . $responseVerify[$i]['dateStart'] . " / " . $responseVerify[$i]['dateEnd'] . "\n";
            }

            $rtn["message"] = "Ya existe una cita en ese horario, intenta denuevo - ID ". "\n"."\n" .$message;
            $rtn["send"] = $responseVerify;
            return $rtn;
        }

        $responseAvailability = $this->verifyAvailability(array(
            "entitieId" => $entitieId,
            "dateStart" => $start,
            "dateEnd" => $end,
            "providerId" => $providerId,
            "userId" => $userId 
        ));

        if ($responseAvailability != 0) {
            $rtn["Error"] = 1;
            $rtn["message"] = "La fecha no coincide con la disponibilidad del Consejero en horas: " . $responseAvailability . ", intenta denuevo";
            return $rtn;
        }

        $today = $this->fmt->modules->dateFormat();

        $insert = "mod_apt_cal_acu_id,
                mod_apt_cal_provider_id,
                mod_apt_cal_type,
                mod_apt_cal_date_start,
                mod_apt_cal_date_end,
                mod_apt_cal_title,
                mod_apt_cal_details,
                mod_apt_cal_place,
                mod_apt_cal_flags,
                mod_apt_cal_user_id,
                mod_apt_cal_created_at,
                mod_apt_cal_ent_id,
                mod_apt_cal_state";
        $values = "'" . $acuId . "','" .
            $providerId . "','" .
            $typeAppointment . "','" .
            $start . "','" .
            $end . "','Cita " .
            $acuLabel . "','" .
            $comments . "','" .
            $place . "','" .
            $flag . "','" .
            $userId . "','" .
            $today. "','" .
            $entitieId . "','1'";

        $sql = "insert into mod_appointments_calendars (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_apt_cal_id) as id from mod_appointments_calendars";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $id  = $row["id"];

        $rtn["Error"] = 0;
        
        $rtn["status"] = 'success';
        $rtn["data"]["calId"] = $id;
        $rtn["data"]["dataCalId"] = $this->dataId($id);
        $rtn["data"]["providerId"] = $providerId;
        $rtn["data"]["counselorName"] = $this->fmt->users->fullName($providerId);
        $rtn["data"]["dateStart"] = $dateStart;
        $rtn["data"]["dateEnd"] = $dateEnd;
        $rtn["data"]["hourStart"] = $hourStart;
        $rtn["data"]["typeAppointment"] = $typeAppointment;
        $rtn["data"]["hourEnd"] = $hourEnd;
        $rtn["data"]["flag"] = $flag;
        $rtn["data"]["accountDataId"] = $this->accounts->dataAccount($acuId);
        $rtn["data"]["linkCelular"] = $this->fmt->options->getValue("link_celular");

        return $rtn;
    }

    /**
     * Verifies appointment dates and returns matching calendar events
     * 
     * Checks for overlapping appointments in the given date range for a specific provider
     * Returns array of matching events with details or 0 if none found
     * 
     * @param array $var Contains date range, provider ID and ACU ID
     * @return array|int Returns array of matching events or 0 if none found
     */
    public function verifyDate(array $var = null)
    {
        $dateStart = $var["dateStart"];
        $dateEnd = $var["dateEnd"];
        $userId = $var["userId"];
        $acuId = $var["acuId"];

        $sql = "SELECT 
            mod_apt_cal_id,
            mod_apt_cal_title,
            mod_apt_cal_date_start,
            mod_apt_cal_date_end,
            mod_apt_cal_status,
            mod_apt_cal_state,
            mod_apt_cal_all_day,
            mod_apt_cal_place,
            mod_apt_cal_type,
            mod_apt_cal_details
        FROM mod_appointments_calendars
        WHERE mod_apt_cal_state = '1'
          AND mod_apt_cal_provider_id = '" . $userId . "'
          AND mod_apt_cal_status NOT IN ('cancelled')
          AND (
            ('" . $dateStart . "' BETWEEN mod_apt_cal_date_start AND mod_apt_cal_date_end)
            OR ('" . $dateEnd . "' BETWEEN mod_apt_cal_date_start AND mod_apt_cal_date_end)
            OR (mod_apt_cal_date_start BETWEEN '" . $dateStart . "' AND '" . $dateEnd . "')
            OR (mod_apt_cal_date_end BETWEEN '" . $dateStart . "' AND '" . $dateEnd . "')
          )";

        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $rtn = [];
            for ($i = 0; $i < $num; $i++) { 
                $row = $this->fmt->querys->row($rs);
                $rtn[$i] = [
                    'id' => $row['mod_apt_cal_id'],
                    'title' => $row['mod_apt_cal_title'],
                    'dateStart' => $row['mod_apt_cal_date_start'],
                    'dateEnd' => $row['mod_apt_cal_date_end'],
                    'status' => $row['mod_apt_cal_status'],
                    'state' => $row['mod_apt_cal_state'],
                    'allDay' => (bool)$row['mod_apt_cal_all_day'],
                    'place' => $row['mod_apt_cal_place'],
                    'type' => $row['mod_apt_cal_type'],
                    'details' => $row['mod_apt_cal_details']
                ];
            }
            return $rtn;
        } else {
            return 0;
        }
    }

    /**
     * Verifies availability of appointments based on provider/user, date and time
     * 
     * @param array $var Contains date range, provider and user IDs
     * @return string|int Returns available time range as string if found, otherwise returns 0
     */
    public function verifyAvailability(array $var = null)
    {
        $dateStart = $var["dateStart"];
        $dateEnd = $var["dateEnd"];
        $providerId = $var["providerId"];
        $userId = $var["userId"];

        $timeStart = $this->fmt->modules->timeToDate($dateStart);
        $timeEnd = $this->fmt->modules->timeToDate($dateEnd);

        $day = $this->fmt->modules->dayLiteralNum($dateStart);

        $sql = "SELECT mod_apt_sch_hour_start, mod_apt_sch_hour_end
                FROM mod_appointments_schedules
                WHERE mod_apt_sch_state='1' AND mod_apt_sch_day = '" . $day . "' AND
                '" . $timeStart . "' >= mod_apt_sch_hour_start and  '" . $timeEnd . "' >= mod_apt_sch_hour_end";

        if ($providerId != 0) {
            $sql .= " AND mod_apt_sch_provider_id='" . $providerId . "'";
        }
        if ($userId != 0) {
            $sql .= " AND mod_apt_sch_user_id='" . $userId . "'";
        }

        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return substr($row["mod_apt_sch_hour_start"], 0, -9) . " - " . substr($row["mod_apt_sch_hour_end"], 0, -9);
        } else {
            return 0;
        }
    }

    public function verifyDaysBlocked(array $var = null)
    {
        $dateStart = $var["dateStart"];
        $dateEnd = $var["dateEnd"];
        $providerId = $var["providerId"];
        $acuId = $var["acuId"];

        $timeStart = $dateStart;
        $timeEnd = $dateEnd;

        $sql = "SELECT mod_apt_cb_id FROM mod_appointments_calendars_blocked WHERE mod_apt_cb_state = '1' AND mod_apt_cb_ent_id = '1' AND ('" . $timeStart . "' BETWEEN mod_apt_cb_date_start AND mod_apt_cb_date_end) AND ('" . $timeEnd . "' BETWEEN mod_apt_cb_date_start AND mod_apt_cb_date_end)";

        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_apt_cb_id"];
        } else {
            return 0;
        }
    }

}