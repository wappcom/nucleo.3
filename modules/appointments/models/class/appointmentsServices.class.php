<?php 

header('Content-Type: text/html; charset=utf-8');

class APPOINTMENTS_SERVICES
{

    var $services = '';
    var $fmt = '';

    function __construct($fmt)
    {
        $this->fmt = $fmt;
        require_once (_PATH_NUCLEO . "modules/services/models/class/services.class.php");
        $this->services = new SERVICES($fmt);
    }

    public function getAppointmentsServicesData(array $var = null)
    {
        //return $var;

        $rtn['services'] = $this->services->getServicesAll($var);

        return $rtn;   
    }

}