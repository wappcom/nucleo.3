

import {
    alertMessageError,
    replacePath,
    addHtml,
    alertPage,
    loadView,
    replaceEssentials,
    emptyReturn,
    jointActions,
    accessToken,
    changeBtn,
    stopLoadingBar,
    replaceAll,
    dataModule
} from '../../components/functions.js';

import {
    focusSelector
} from '../../components/forms.js';

import {
	categorysListTable,
	createTable,
	dataTable,
	removeItemTable,
	inactiveBtnStateItem,
	activeBtnStateItem,
	mountTable,
	colCheck,
	colId,
	colName,
	colTitle,
	colState,
	colActionsBase,
	deleteItemModule,
	renderEmpty,
	mountMacroTable,
	clearMacroTable
} from "../../components/tables.js";
import {
	renderCol,
	renderColCheck,
	renderColId,
	renderColTitle,
	renderRowsTable,
	renderColState,
	renderColActions,
	renderColCategorys,
} from "../../components/renders/renderTables.js";

let module = 'appointmentsServices';
let system = 'appointments';
let formId = "formAppointmentsServices";
let tableId = "tableAppointmentsServices";
let pathurl = "modules/appointments/";

export const appointmentsServicesIndex = (vars = { module: module, system: system }) => {
        let moduleReturn = vars.moduleReturn ? vars.moduleReturn : module;
        //console.log("appointmentsServicesIndex", module, system, pathurl, moduleReturn);
        loadView(_PATH_WEB_NUCLEO +  "modules/appointments/views/appointmentsServices.html?" + _VS).then(
            (htmlView) => {
                //console.log(htmlView, module, system);
                getData({
                    task: 'getAppointmentsServicesData',
                    return: 'returnArray', // returnId, returnState, returnArray
                }).then((response) => {
                    console.log('getData getAppointmentsServicesData', response);
                    if (response.status == 'success') {
                        stopLoadingBar();
    
                        let str = htmlView;
                        let table = mountMacroTable({
                            tableId,
                            module,
                            data: response.data.services,
                            cols: [{
                                label: "id",
                                cls: 'colId',
                                type: "id",
                                render: 'id',
                            }, {
                                label: "*Nombre",
                                cls: 'colName',
                                type: "text",
                                render: 'name',
                                attrName: 'inputName',
                                id: 'inputName',
                            },  {
                                label: "Description",
                                cls: 'colDescription',
                                type: "text",
                                render: 'description',
                                attrName: 'inputDescription',
                                id: 'inputDescription',
                            },  {
                                label: "Activo",
                                cls: 'colText',
                                type: "text",
                                render: 'active',
                                attrName: 'inputActive',
                                id: 'inputActive',
                            }, {
                                label: "Precio",
                                cls: 'colText',
                                type: "text",
                                render: 'price',
                                attrName: 'inputPrice',
                                id: 'inputPrice',
                            }, {
                                label: "Estado",
                                cls: 'colState',
                                type: "state",
                                render: 'state',
                                attrName: 'inputState',
                                id: 'inputState',
                            }]
                        })
                        str = replaceAll(str, "{{_TABLE_INNER}}", table);
    
                        $(".innerForm").remove();
                        //console.log("str", module)
                        addHtml({
                            selector: `.bodyModule[module='${moduleReturn}']`,
                            type: 'insert', // insert, append, prepend, replace
                            content: replaceEssentials({
                                str,
                                module,
                                system,
                                pathurl,
                                name: dataModule(module, "name"),
                                fn: "formNewCustomerCompany",
                                btnNameAction: "Nuevo Cliente Empresa",
                                icon: dataModule(module, "icon"),
                                color: dataModule(module, "color"),
                            })
                        })
    
                        $(`#${tableId} #inputName`).focus();
                    } else if (response.status == 'success' && response.data == 0) {
                        $(".bodyModule[module='" + moduleReturn + "'] .tbody").html(renderEmpty());
                    } else {
                        alertMessageError({
                            message: response.message
                        })
                    }
                }).catch(console.warn());
            }
        );
}


export const getData = async (vars = []) => {
   //console.log('getData', vars);
   const url = _PATH_WEB_NUCLEO + "modules/appointments/controllers/apis/v1/appointmentsServices.php";
   let data = JSON.stringify({
       accessToken: accessToken(),
       vars: JSON.stringify(vars)
   });
   try {
       let res = await axios({
           async: true,
           method: "post",
           responseType: "json",
           url: url,
           headers: {},
           data: data
        })
       //console.log(res.data);
       jointActions(res.data)
       return res.data
   } catch (error) {
      console.log("Error: getData ", error);
   }
}


document.addEventListener("DOMContentLoaded", function () {
	$("body").on("keyup", `#${tableId} input`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		//console.log(e.key);

		if (e.key == "Enter") {
			let name = $(`#${tableId} #inputName`).val();
            let description = $(`#${tableId} #inputDescription`).val() || "";
            let providerId = $(`#${tableId} #inputProviderId`).val();
			let duration = $(`#${tableId} #inputDuration`).val();
			let price = $(`#${tableId} #inputPrice`).val();
			let state = $(`#${tableId} #inputState`).is(':checked') ? 1 : 0;


			//let state = $(`#${tableId} #inputState`).val() ;
			let count = 0;

			if (name == "") {
				alertMessageError({
					message: "Debe ingresar al menos un Nombre"
				})
				count++;
			}


			if (count == 0) {
				let inputs = {
					name,
					description,
					providerId,
					duration,
					price,
					state
				}

				getData({
					task: 'addAppointmentsService',
					return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
					inputs
				}).then((responseData) => {
					console.log('getData addAppointmentsService', responseData);
					if (responseData.status == 'success') {
						let id = responseData.data;
						$(`#${tableId} table tbody tr:nth-child(2)`).append(`<tr rowId="${id}">
							<td class="colCheck"><input type="checkbox" class="inputCheckBox" data-id="${id}"></td>
							<td class="colId">${id}</td>
							<td class="colName">${name}</td>
                            <td class="colText">${description}</td>
                            <td class="colText">${providerId}</td>
							<td class="colText">${duration}</td>
							<td class="colText">${price}</td>
							<td class="colState">${state}</td>
						</tr>`);
						clearMacroTable('#' + tableId);
					} else {
						alertMessageError({
							message: response.message
						})
					}
				}).catch(console.warn());
			}
		}

	})
});