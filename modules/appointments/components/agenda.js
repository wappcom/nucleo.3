// Importaciones necesarias
import {
    alertMessageError,
    alertMessageSuccess,
    replacePath,
    addHtml,
    alertPage,
    loadView,
    replaceEssentials,
    emptyReturn,
    jointActions,
    accessToken,
    changeBtn,
    stopLoadingBar,
    replaceAll,
    dataModule,
} from '../../components/functions.js';

import {
    editorText,
    errorInput,
    errorInputSelector,
    loadInitChargeMedia,
    loadItemMedia,
    loadFilesFormLoader,
    loadSingleFileFormLoader,
    loadChargeMediaInit,
    removalMediaItems,
    ckeckboxChecked,
    stateBtnSwitch
} from "../../components/forms.js";

import {
    deleteModalItem,
    removeModal,
    removeModalId,
    closeModal,
    resizeModalConfig,
    deleteModal,
} from "../../components/modals.js";

import {
	categorysListTable,
	createTable,
	dataTable,
	removeItemTable,
	inactiveBtnStateItem,
	activeBtnStateItem,
	mountTable,
	colCheck,
	colId,
	colName,
	colTitle,
	colState,
	colActionsBase,
	deleteItemModule,
	renderEmpty,
	mountMacroTable,
	clearMacroTable
} from "../../components/tables.js";

import {
    renderCol,
    renderColCheck,
    renderColId,
    renderColTitle,
    renderRowsTable,
    renderColState,
    renderColActions,
    renderColCategorys,
    renderColCheckboxState,
    renderColTitlePath
} from "../../components/renders/renderTables.js";

import {
    innerForm,
    renderDeleteModal,
    renderDeleteModalFn,
    renderModalConfig,
    renderModalConfigBody,
    renderModalClean,
    removeInnerForm
} from "../../components/renders/renderModals.js";


import {
    renderCheckBox,
    renderPill,
    renderItemVideo,
    renderItemImage
} from "../../components/renders/renderForms.js";


let module = 'agenda';
let system = 'appointments';
let formId = "formAgenda";
let tableId = "tableAgenda";
let pathurl = "modules/appointments/";

// Funciones principales
export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/appointments/controllers/apis/v1/agenda.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
         })
        //console.log(res.data);
        jointActions(res.data)
        return res.data
    } catch (error) {
       console.log("Error: getData ", error);
    }
}

export const agendaIndex = (vars = { module: module, system: system }) => {
    let moduleReturn = vars.moduleReturn ? vars.moduleReturn : module;
    
    loadView(_PATH_WEB_NUCLEO + "modules/appointments/views/agenda.html?" + _VS).then(
        (htmlView) => {
            let content = htmlView;
            console.log("userData", userData);
            content = content.replace(/{{_BTN_ID}}/g, "btnFormNewPerson");
            if (userData.rolId == 1) {
                content = content.replace(/{{_BTNS_RIGTH}}/g, `<button class="btn btnIcon" data-toggle="modal" data-target="" id="btnConfigEd"><i class="icon icon-conf"></i></button>`);
            } else {
                content = content.replace(/{{_BTNS_RIGTH}}/g, "");
            }

            $(".bodyModule[module='" + moduleReturn + "']").html(
                replaceEssentials({
                    str: content,
                    module,
                    system,
                    pathurl,
                    btnNameAction: "Nueva Persona",
                    name: dataModule(module, "name"),
                    icon: dataModule(module, "icon"),
                    color: dataModule(module, "color"),
                })
            );

            
            $(".bodyModule[module='" + moduleReturn + "'] .tbody").html('<div class="loading loadingFluidTop"></div>');

            getData({
                task: 'getPersonsList',
                return: 'returnObject',
            }).then((response) => {
                console.log('getData getPersonsList', response);
                if (response.status == 'success') {
                    if (response.Error == 0 && response.data != 0) {
                        let rows = "";
                        for (let i in response.data) {
                            let item = response.data[i];
                            let name = item.name + ' ' + item.lastnameFather + ' ' + (item.lastnameMother || '');
                            const phone = item.phoneDial + " " + item.phone;
                            rows += renderRowsTable({
                                id: item.id,
                                content: renderColCheck({
                                        id: item.id,
                                    }) +
                                    renderColId({
                                        id: item.id,
                                    }) +
                                    renderCol({
                                        id: item.id,
                                        data: name,
                                    }) +
                                    renderCol({
                                        id: item.id,
                                        data: item.email || '',
                                    }) +
                                    renderCol({
                                        id: item.id,
                                        data: phone || '',
                                    }) +
                                    renderCol({
                                        id: item.id,
                                        data: item.type,
                                    }) +
                                    renderColState({
                                        id: item.id,
                                        state: item.state,
                                        fn: "changeStatePerson",
                                        module,
                                        system,
                                    }) +
                                    renderColActions({
                                        id: item.id,
                                        name: item.name,
                                        type: "btn,btn",
                                        fnType: "btnEditItemPerson,btnDeleteItemPerson",
                                        attr: `data-id="${item.id}" data-module="${module}" data-system="${system}" data-name="${name}"`
                                    }),
                            });
                        }

                        mountTable({
                            id: 'tablePersons',
                            columns: [
                                ...colCheck,
                                ...colId,
                                {
                                    label: "Nombre Completo",
                                    cls: "colName",
                                },
                                {
                                    label: "Email",
                                    cls: "colEmail",
                                },
                                {
                                    label: "Teléfono",
                                    cls: "colPhone",
                                },
                                {
                                    label: "Tipo",
                                    cls: "colType",
                                },
                                ...colState,
                                ...colActionsBase,
                            ],
                            rows,
                            module,
                            system,
                            container: ".bodyModule[module='" + moduleReturn + "'] .tbody",
                        });

                    } else if (response === 0 || response.data == 0) {
                        $(".bodyModule[module='" + moduleReturn + "'] .tbody").html(renderEmpty())
                    } else {
                        alertPage({
                            text: "Error. por favor contactarse con soporte. " + response.message,
                            icon: "icn icon-alert-warning",
                            animation_in: "bounceInRight",
                            animation_out: "bounceOutRight",
                            tipe: "danger",
                            time: "3500",
                            position: "top-left",
                        })
                    }
                } else {
                    alertMessageError({
                        message: response.message
                    })
                }
            }).catch(console.warn());
            
            stopLoadingBar();
            $(".innerForm").remove();
        }
    ).catch(console.warn());
}

export const formNewPerson = (vars = []) => {
    getData({
        task: 'getParameters',
        return: 'returnArray',
    }).then((response) => {
        console.log('getParameters', response);
        if (response.status == 'success') {

            loadView(_PATH_WEB_NUCLEO + "modules/appointments/views/formPerson.html?" + _VS).then((responseView) => {
                let str = responseView;
                str = str.replace(/{{_MODULE}}/g, module);
                str = str.replace(/{{_SYSTEM}}/g, system);
                str = str.replace(/{{_FORM_ID}}/g, "newFormPerson");
                str = str.replace(/{{_ITEM}}/g, "");
                str = str.replace(/{{_TITLE}}/g, "Nueva Persona");
                str = str.replace(/{{_BTNS_TOP}}/g, "<a class='btn btnPrimary btnSaveNewPerson' id='btnSaveNewPerson' data-form='newFormPerson' ><span>Guardar</span></a>");

                // Reemplazar datos específicos del formulario de persona
                if (response.data) {
                    str = str.replace(/{{_PERSON_TYPES}}/g, renderPersonsTypes(response.data.personsTypes));
                }

                innerForm({
                    module,
                    body: str,
                });

                // Inicializar datepickers o cualquier otro plugin necesario
                jQuery.datetimepicker.setLocale("es");
                $("#newFormPerson .datepicker").datetimepicker({
                    timepicker: false,
                    datepicker: true,
                    format: "Y-m-d",
                    lang: "es",
                });

                // Enfocar el primer input
                $("#newFormPerson #inputName").focus();

            }).catch(console.warn());
        } else {
            alertMessageError({
                message: response.message
            });
        }
    }).catch(console.warn());
};

export const newPerson = (vars = []) => {
    console.log('newPerson', vars);
    let form = vars.form;
    let count = 0;
    let inputreturn = vars.inputreturn || '';

    // Obtener valores del formulario
    const inputName = $(`#${form} #inputName`).val();
    const inputLastnameFather = $(`#${form} #inputLastnameFather`).val(); 
    const inputLastnameMother = $(`#${form} #inputLastnameMother`).val();
    const inputEmail = $(`#${form} #inputEmail`).val();
    const inputPhoneDial = $(`#${form} #inputPhoneDial`).val();
    const inputPhone = $(`#${form} #inputPhone`).val();
    const inputType = $(`#${form} #inputType`).val();
    const inputCustomClass = $(`#${form} #inputCustomClass`).val();
    const inputCustomId = $(`#${form} #inputCustomId`).val();
    const inputComments = $(`#${form} #inputComments`).val();
    let inputMetadata = $(`#${form} #inputMetadata`).val();
    const name = inputName + ' ' + inputLastnameFather + ' ' + inputLastnameMother;

    // Validaciones
    if (!inputName) {
        errorInput("inputName", "Debe ingresar un nombre");
        count++;
    }

    if (!inputLastnameFather) {
        errorInput("inputLastnameFather", "Debe ingresar el apellido paterno");
        count++;
    }

    if (!inputEmail) {
        errorInput("inputEmail", "Debe ingresar un correo");
        count++;
    }

    // Validar que inputMetadata sea un JSON válido si tiene contenido
    if (inputMetadata) {
        try {
            JSON.parse(inputMetadata);
        } catch (e) {
            errorInput("inputMetadata", "El formato JSON no es válido");
            count++;
        }
    } else {
        inputMetadata = '{}';
    }

    console.log('inputMetadata', inputMetadata, count);

    if (count === 0) {
        getData({
            task: 'addPerson',
            return: 'returnObject',
            inputs: {
                inputName,
                inputLastnameFather,
                inputLastnameMother,
                inputEmail,
                inputPhoneDial,
                inputPhone,
                inputType,
                inputCustomClass,
                inputCustomId,
                inputComments,
                inputMetadata
            }
        }).then((response) => {
            console.log('addPerson response:', response);

            if (response.Error === 0) {
                alertMessageSuccess({ message: response.message });
                if (inputreturn == '') {
                    closeModal('modalNewPerson');
                    agendaIndex();
                } else {
                    removeModal()
                    addAccountList({ personId: response.id, name : response.id + ".- " + name , inputreturn })
                }
               
            } else {
                alertMessageError({ message: response.message });
            }
        }).catch((error) => {
            console.error('Error in addPerson:', error);
            alertMessageError({ message: 'Error al guardar la persona' });
        });
    }
}

export const addAccountList = (vars = []) => {
	//console.log('addAccountList', vars);
	$(`#${vars.inputreturn}`).val(vars.personId);
	$(`input[for="${vars.inputreturn}"]`).val(vars.name);

}

export const formEditPerson = (vars = []) => {
    console.log('formEditPerson', vars);
    if (!vars || !vars.id) {
        console.error('formEditPerson: ID de persona no proporcionado');
        alertMessageError({ message: 'ID de persona no proporcionado' });
        return;
    }

    const { module, system } = vars;
    const personId = vars.id;
    const form = 'editFormPerson'; // Usar ID fijo del formulario

    getData({
        task: 'getDataPerson',
        return: 'returnArray',
        item: personId
    }).then((response) => {
        console.log('getData getDataPerson', response);

        if (response.status === 'success') {
            const person = response.data;
            
            loadView(_PATH_WEB_NUCLEO + "modules/appointments/views/formPerson.html?" + _VS).then((responseView) => {
                let str = responseView;
                const typesData = person.types || [];

                str = str.replace(/{{_MODULE}}/g, module);
                str = str.replace(/{{_SYSTEM}}/g, system);
                str = str.replace(/{{_FORM_ID}}/g, form);
                str = str.replace(/{{_ITEM}}/g, personId);
                str = str.replace(/{{_TITLE}}/g, "Editar Persona");
                str = str.replace(/{{_BTNS_TOP}}/g, `<a class='btn btnPrimary btnUpdateEditPerson' id='btnUpdateEditPerson' data-form='${form}' data-id='${personId}'><span>Actualizar</span></a>`);

                str = str.replace(/{{_PERSON_TYPES}}/g, renderPersonsTypes(typesData, person.type));

                innerForm({
                    module,
                    body: str,
                });

                const fields = {
                    inputName: person.name,
                    inputLastnameFather: person.lastnameFather,
                    inputLastnameMother: person.lastnameMother,
                    inputEmail: person.email,
                    inputPhone: person.phone,
                    inputCustomClass: person.customClass,
                    inputCustomId: person.customId,
                    inputComments: person.comments,
                    inputMetadata: person.metadata
                };

                Object.entries(fields).forEach(([id, value]) => {
                    if (value !== undefined) {
                        $(`#${form} #${id}`).val(value);
                    }
                });

                jQuery.datetimepicker.setLocale("es");
                $(`#${form} .datepicker`).datetimepicker({
                    timepicker: false,
                    datepicker: true,
                    format: "Y-m-d",
                    lang: "es",
                });
            }).catch((viewError) => {
                console.error('Error cargando vista:', viewError);
                alertMessageError({ message: 'Error al cargar el formulario' });
            });
        } else {
            alertMessageError({ message: response.message || 'Error al obtener datos de la persona' });
        }
    }).catch((error) => {
        console.error('Error en formEditPerson:', error);
        alertMessageError({ message: 'Error al procesar la solicitud' });
    });
};

export const updatePerson = (vars = []) => {
    console.log('updatePerson', vars);
    let form = vars.form;
    let count = 0;

    // Establecer el ID en el campo oculto
    $(`#${form} #inputId`).val(vars.id);

    const inputId = vars.id;
    const inputName = $(`#${form} #inputName`).val();
    const inputLastnameFather = $(`#${form} #inputLastnameFather`).val();
    const inputLastnameMother = $(`#${form} #inputLastnameMother`).val();
    const inputEmail = $(`#${form} #inputEmail`).val();
    const inputPhone = $(`#${form} #inputPhone`).val();
    const inputType = $(`#${form} #inputType`).val();
    const inputCustomClass = $(`#${form} #inputCustomClass`).val();
    const inputCustomId = $(`#${form} #inputCustomId`).val();
    const inputComments = $(`#${form} #inputComments`).val();
    let inputMetadata = $(`#${form} #inputMetadata`).val();

    if (!inputId) {
        console.error('No ID provided for update');
        alertMessageError({ message: 'Error: No se proporcionó ID' });
        return;
    }

    if (!inputName) {
        errorInput("inputName", "Debe ingresar un nombre");
        count++;
    }

    if (!inputLastnameFather) {
        errorInput("inputLastnameFather", "Debe ingresar el apellido paterno");
        count++;
    }

    if (!inputEmail) {
        errorInput("inputEmail", "Debe ingresar un correo");
        count++;
    }

    // Validar que inputMetadata sea un JSON válido si tiene contenido
    if (inputMetadata) {
        try {
            JSON.parse(inputMetadata);
        } catch (e) {
            errorInput("inputMetadata", "El formato JSON no es válido");
            count++;
        }
    } else {
        inputMetadata = '{}';
    }

    if (count === 0) {
        getData({
            task: 'updatePerson',
            return: 'returnState',
            inputs: {
                inputId,
                inputName,
                inputLastnameFather,
                inputLastnameMother,
                inputEmail,
                inputPhone,
                inputType,
                inputCustomClass,
                inputCustomId,
                inputComments,
                inputMetadata
            }
        }).then((response) => {
            console.log('updatePerson response:', response);

            if (response.status === 'success') {
                    agendaIndex();
                    closeModal('modalEditPerson');
            } else {
                alertMessageError({ message: response.message || 'Error al actualizar la persona' });
            }
        }).catch((error) => {
            console.error('Error in updatePerson:', error);
            alertMessageError({ message: 'Error al actualizar la persona' });
        });
    }
}

export const searchTablePersons = (vars =[]) => {
    console.log('searchTablePersons',vars);
}

export const deletePerson = (vars = []) => {
    console.log('deletePerson', vars);
    const inputId = vars.id;

    if (!inputId) {
        console.error('No ID provided for deletion');
        alertMessageError({ message: 'Error: No se proporcionó ID' });
        return;
    }

    getData({
        task: 'deletePerson',
        return: 'returnState',
        inputs: {
            inputId
        }
    }).then((response) => {
        console.log('deletePerson response:', response);

        if (response.status === 'success') {
            agendaIndex();
            closeModal('modalDelete');
        } else {
            alertMessageError({ message: response.message || 'Error al eliminar la persona' });
        }
    }).catch((error) => {
        console.error('Error in deletePerson:', error);
        alertMessageError({ message: 'Error al eliminar la persona' });
    });
}

// Funciones auxiliares
const renderPersonsTypes = (types, selectedCode = null) => {
    if (!Array.isArray(types) || types.length === 0) {
        return '<option value="">No hay tipos disponibles</option>';
    }

    let options = '';
    
    if (!selectedCode) {
        options = '<option value="">Seleccionar tipo</option>';
    }

    types.forEach((type, index) => {
        const isSelected = (selectedCode && type.code === selectedCode) || (!selectedCode && index === 0);
        options += `<option value="${type.code}" ${isSelected ? 'selected' : ''}>${type.name}</option>`;
    });

    return options;
};

// Event listener for delete button
document.addEventListener("DOMContentLoaded", function() {
    $("body").on("click", ".btnDeletePerson", function(e) {
        e.preventDefault();
        const id = $(this).data("id");
        
        // Show confirmation dialog
        Swal.fire({
            title: '¿Está seguro?',
            text: "¡Esta acción no se puede deshacer!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {
                deletePerson({ id: id });
            }
        });
    });

    $("body").on("click",`#btnFormNewPerson`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`#btnFormNewPerson`,data);
        formNewPerson(data);
    });

    $("body").on("click", `.btnSaveNewPerson`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let form = data.form;
        console.log(`.btnSaveNewPerson`, data, form);
        newPerson({
            form,
            state: '0'
        });
    });

    $("body").on("click", `.btnEditItemPerson`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnEditItemPerson`, data);
        formEditPerson(data);
    });

    $("body").on("click", `.btnDeleteItemPerson`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnDeleteItemPerson`, data);
        addHtml({
            selector: "body",
            type: "append",  
            content: renderDeleteModalFn({
                name: data.name,
                id: "modalDelete",
                module,
                attr: `data-module="${module}" data-id="${data.id}"`,
            }),
        });
    });

    $("body").on("click",`.btnDeleteConfirmationAgenda`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnDeleteConfirmationAgenda`,data);
        deletePerson(data);
    });

    $("body").on("click", `.btnUpdateEditPerson`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnSaveEditPerson`, data);
        updatePerson(data);
    });


    $("body").on("click", `.btnSavePersonExpress`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`.btnSavePersonExpress`, data);
        data["btnId"] = e;
        data["form"] = data.id;
        let ci = $(`#${data.id} #inputCI`).val();
        let ext = $(`#${data.id} #inputExt`).val();
        data["inputreturn"] = data.inputreturn;
        data["inputMetadata"] = `{ "ci": "${ci}", "ext": "${ext}"}`;
        newPerson(data);
            
            /* .then((dataOut) => {
            console.log("newPersonExpress", dataOut);

            if (dataOut.status == "success") {
                removeModal()

                addAccountList({ acuId: dataOut.data.idAccount, name: dataOut.data.name, inputreturn: data.inputreturn })
            }
            if (dataOut.Error == "error") {
                $(`.message[for="${data.id}"]`).html(`<div class="message messageDanger">${dataOut.message}</div>`);
            }
        }).catch((err) => {
            //console.log(err)
        }); */
    });

});

