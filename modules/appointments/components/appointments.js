import {
	listJsonToString,
	btnFn,
	loadView,
	replaceAll,
	replacePath,
	accessToken,
	stopLoadingBar,
	loadBtnLoading,
	loadingBtnIcon,
	alertResponseMessageError,
	alertPage,
	disableId,
	alertMessageError,
	addHtml,
	alertMessageSuccess,
	changeBtn,
	timer,
	jointActions,
	capitalize,
	disabledBtn,
	unDisabledBtn,
	loadingBar
} from "../../components/functions.js";
import {
	draggable,
	removeModalId
} from "../../components/modals.js";

import {
	checkInRange
} from "../../components/calendar.js";

import {
	renderModalConfirm,
	renderDeleteModal,
	renderModal
} from "../../components/renders/renderModals.js";
import {
	sortJSON,
	validateDate,
	listHours
} from "../../components/forms.js";
import {
	literalDay,
	dayLiteral,
	convertLiteralToDbDate,
	unDbDate,
} from "../../components/dates.js";

import {
	resizeNav
} from "../../components/nav.js";

/*import {
	buttonsEffects
} from "../../components/effects.js";

 import {
	loadCounselors,
	loadDashboardAdvised,
	createUserExpress
} from "./advised.js"; 

import {
	formHistoryAdvised
} from "./historyAdvised.js";

export {
	formHistoryAdvised
};
/* import {
	renderListCounselors,
	renderCalendarAdvised,
	renderFormCalendar,
	renderOpenFormCalendar,
	renderModalConfigBodyCalendar,
	renderModalConfigContentBodyCalendar,
	renderFormBlockContent,
	renderBtnAddHours,
	renderModalConfigSidebar,
	renderHeadCalendars,
	renderHeadActionsCalendars,
	renderSearchUsersAdvised,
	renderEditDataAccount,
	renderWindowCalendarList,
} from "./renders/renderCalendarAdvised.js"; */
import {
	resizeModalConfig,
	deleteModalItem
} from "../../components/modals.js";
import {
	renderModalConfig,
	renderModalConfigBody,
} from "../../components/renders/renderModals.js";
import {
	renderBtnStateSwitch,
	renderSelectHoursStartEnd,
	renderCheckBox,
	renderRadioButton,
} from "../../components/renders/renderForms.js";

import {
	renderLoading
} from "../../components/renders/render.js";

import {
	mountMacroTable,
} from "../../components/tables.js";

import {
    focusSelector
} from '../../components/forms.js';

let module = 'appointments';
let system = 'appointments';
let formId = "formAppointments";

export const appointmentsIndex = (vars = { module: module, system: system }) => {
    console.log('appointmentsIndex', vars);
    $(`.bodyModule[module='${module}]`).html(`<div class="loadingPageModule"><div class="loadingBar"></div></div>`);
}