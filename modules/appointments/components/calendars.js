import {
	listJsonToString,
	btnFn,
	loadView,
	replaceAll,
	replacePath,
	accessToken,
	stopLoadingBar,
	loadBtnLoading,
	loadingBtnIcon,
	alertResponseMessageError,
	alertPage,
	disableId,
	alertMessageError,
	addHtml,
	alertMessageSuccess,
	changeBtn,
	timer,
	jointActions,
	capitalize,
	disabledBtn,
	unDisabledBtn,
	loadingBar
} from "../../components/functions.js";
import {
	draggable,
	removeModalId
} from "../../components/modals.js";


import {
	renderModalConfirm,
	renderDeleteModal,
	renderModal
} from "../../components/renders/renderModals.js";
import {
	sortJSON,
	validateDate,
	listHours
} from "../../components/forms.js";
import {
	literalDay,
	dayLiteral,
	convertLiteralToDbDate,
	unDbDate,
} from "../../components/dates.js";

import {
	resizeNav
} from "../../components/nav.js";

/*import {
	buttonsEffects
} from "../../components/effects.js";

 import {
	loadCounselors,
	loadDashboardAdvised,
	CreatePersonExpress
} from "./advised.js"; 

import {
	formHistoryAdvised
} from "./historyAdvised.js";

export {
	formHistoryAdvised
};

import {
	renderListCounselors,
	renderCalendarAdvised,
	renderFormCalendar,
	renderOpenFormCalendar,
	renderModalConfigBodyCalendar,
	renderModalConfigContentBodyCalendar,
	renderFormBlockContent,
	renderBtnAddHours,
	renderModalConfigSidebar,
	renderHeadCalendars,
	renderHeadActionsCalendars,
	renderSearchUsersAdvised,
	renderEditDataAccount,
	renderWindowCalendarList,
} from "./renders/renderCalendars.js"; */

import {
	renderListAgents,
	renderCalendarAgents,
	renderFormCalendar,
	renderOpenFormCalendar,	
	renderFormNewPersonExpress
} from "./renders/renderCalendars.js";

import {
	resizeModalConfig,
	deleteModalItem
} from "../../components/modals.js";
import {
	renderModalConfig,
	renderModalConfigBody,
} from "../../components/renders/renderModals.js";
import {
	renderBtnStateSwitch,
	renderSelectHoursStartEnd,
	renderCheckBox,
	renderRadioButton,
} from "../../components/renders/renderForms.js";

import {
	renderLoading
} from "../../components/renders/render.js";

import {
	mountMacroTable,
} from "../../components/tables.js";

import {
	focusSelector
} from '../../components/forms.js';


let module = 'calendars';
let system = 'appointments';
let formId = "formCalendars";
let listAccounts = "";
let numAgents = 0;

export const calendarsIndex = (vars = { module: module, system: system }) => {
	console.log('calendarsIndex', vars, module, system);
	$(`.bodyModule[module='${module}'][system='${system}']`).html(`<div class="loadingPageModule"><div class="loadingBar"></div></div>`);
	loadingBar();

	getData({
		task: 'getDataCalendars',
		return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
	}).then((response) => {
		console.log('getData getDataCalendars', response);
		let dataCalendars = response.data;
		numAgents = dataCalendars.numAgents;
		let agents = dataCalendars.agents;
		listAccounts = dataCalendars.agenda;
		let typesAppointments = dataCalendars.typesAppointments;
		let agentsId = dataCalendars.agentsId;

		localStorage.setItem('typesAppointments', JSON.stringify(typesAppointments));
		localStorage.setItem('agents', JSON.stringify(agents));
		localStorage.setItem('agentsId', JSON.stringify(agentsId));


		if (response.status == 'success') {
			loadView(_PATH_WEB_NUCLEO + "modules/appointments/views/calendars.html?" + _VS).then((responseView) => {
				//console.log('loadView', responseView);
				let str = responseView;

				str = str.replace(/{{_MODULE}}/g, module);
				str = str.replace(/{{_SYSTEM}}/g, system);
				str = str.replace(/{{_NUM}}/g, numAgents);

				str = str.replace(
					/{{_LIST_AGENTS}}/g,
					renderListAgents(agents)
				);

				str = str.replace(
					/{{_CALENDARS}}/g,
					renderCalendarAgents(agents)
				);

				str = replacePath(str);

				$(".bodyModule[module='" + module + "'][system='" + system + "']").html(str);

				let hSidebar = $(".sidebarAppointmentsCalendars").outerHeight();
				// let hCalendar = hSidebar - 100;
				let hCalendar = hSidebar;
				calendars(agents, "calendarAgent", {
					height: hCalendar,
					fn: "addScheduleForm",
				});

				resizeCalendar();

				//console.log(data);
			})
		} else {
			alertMessageError({
				message: response.message
			})
		}
	}).catch(console.warn());

}

export function resizeCalendar() {
	let wMod = $(".bodyModule[module='" + module + "']").outerWidth();
	let wSidebar = $(".sidebarCalendar").outerWidth();
	let hSidebar = $(".sidebarCalendar").outerHeight();
	let hCalBody = $(".bodyModule[module='" + module + "'] .appointmentsCalendars .tbody").outerHeight();
	let wBody = wMod - wSidebar;
	let w = $(window).outerWidth();


	resizeNav();
	console.log("resizeCalendar", w, module, wMod, wSidebar, wBody)
	//$(".bodyModule[module='" + module + "'] .bodyCalendar").outerWidth(wBody);
}

export function openScheduleForm(date) {
	console.log(listAccounts)
	date['rolId'] = userData.rolId;
	date['canCreateUser'] = listAccounts.canCreateUser;
	date['canEditHistory'] = listAccounts.canEditHistory;
	//console.log("openScheduleForm", date);

	$(".bodyModule[module='" + module + "']").prepend(
		renderOpenFormCalendar(date)
	);

	$("#formCalendar").draggable();
	$("#inputDate").datetimepicker({
		timepicker: false,
		format: "d-m-Y",
		lang: "es",
	});
	let str = "";
}

export function calendars(agents = [], id, options) {
	//console.log("calendars:", agents, id, options);

	getData({
		task: 'getDataDaysBlocked',
		return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
	}).then((response) => {
		//console.log('getData getDataDaysBlocked', response);
		if (response.status == 'success') {
			let dataDateBlock = "";
			let dataBlock = localStorage.getItem("daysBlocked");
			//console.log("dataBlock:", dataBlock);
			if (dataBlock && dataBlock != "" && dataBlock != "undefined") {
				dataDateBlock = JSON.parse(dataBlock);
			} else {
				dataDateBlock = "";
			}

			for (let i = 0; i < agents.length; i++) {
				const element = agents[i];
				const name = element.name + " " + element.lastname;
				console.log ("name", element);
				getData({
					task: 'getDataSchedulesAgent',
					return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
					item: element.id
				}).then((response) => {

					console.log("getDataSchedulesAgent", response);

					let events = [];

					//console.log("dataDateBlock:", dataDateBlock);

					let ote = [];

					if (dataDateBlock != null && dataDateBlock != "" && dataDateBlock != "undefined") {
						let data = '';
						for (let i = 0; i < dataDateBlock.length; i++) {
							const element = dataDateBlock[i];
							const data = {
								start: element.dateStart,
								end: element.dateEnd,
								rendering: 'background',
								color: 'disabled',
							};
							ote[i] = {
								...data
							};
						}
					}

					//console.log("ote:", ote);

					if (response.Error == 0) {
						const array = response.data;
						let eventsLength = array.length;
						

						for (let i = 0; i < array.length; i++) {
							const elem = array[i];
							//console.log("elem:", elem);
							const item = {
								id: elem.id,
								item: elem.id,
								title: elem.title,
								module: module,
								system: system,
								details: elem.details,
								place: elem.place,
								state: elem.state,
								agentId: element.id,
								hourStart: elem.hourStart,
								hourEnd: elem.hourEnd,
								start: elem.dateStart,
								end: elem.dateEnd,
								color: elem.dataType.bgColor,
								textColor: elem.dataType.textColor,
								borderColor : elem.dataType.borderColor,
								type: elem.type,
								kind: elem.dataType.kind,
								state: elem.state,
								email: elem.dataAcuId.email,
								agentName: elem.agentName,
								acuId: elem.acuId,
								dataAcuId: elem.dataAcuId,
								linkCelular: elem.linkCelular,
							};

							events[i] = {
								...item
							};
						}

						for (let j = 0; j < ote.length; j++) {
							events[eventsLength++] = ote[j];
						}
						//console.log(events);
					} else {
						for (let j = 0; j < ote.length; j++) {
							events[j] = ote[j];
						}
					}

					//console.log("events", events);

					$("#" + id + element.id).fullCalendar({
						dayNames: [
							"Domingo",
							"Lunes",
							"Martes",
							"Miercoles",
							"Jueves",
							"Viernes",
							"Sabado",
						],
						dayNamesShort: [
							"Dom",
							"Lun",
							"Mar",
							"Mie",
							"Jue",
							"Vie",
							"Sáb",
						],
						monthNamesShort: [
							"Ene",
							"Feb",
							"Mar",
							"Abr",
							"May",
							"Jun",
							"Jul",
							"Ago",
							"Sep",
							"Oct",
							"Nov",
							"Dic",
						],
						monthNames: [
							"Enero",
							"Febrero",
							"Marzo",
							"Abril",
							"Mayo",
							"Junio",
							"Julio",
							"Agosto",
							"Septiembre",
							"Octubre",
							"Noviembre",
							"Diciembre",
						],
						buttonText: {
							today: "Hoy",
							month: "Mes",
							week: "Semana",
							day: "Día",
							list: "Lista",
						},
						header: {
							left: "prev today",
							center: "title",
							right: "month,agendaWeek,agendaDay,listMonth,next",
						},
						views: {
							timelineFourDays: {
								type: "timeline",
								duration: {
									days: 1
								},
							},
						},
						defaultView: "agendaWeek",
						businessHours: {
							dw: [1, 2, 3, 4, 5, 6],
							start: "8:00",
							end: "18:00",
						},
						firstDay: 1,
						firstDayOfWeek: 1,
						timeZone: 'UTC',
						slotDuration: "00:20:00",
						minTime: "07:30",
						slotLabelFormat: "HH:mm",
						timeFormat: "HH:mm",
						height: options.height,
						contentHeight: 'auto',
						eventLimit: true,
						nowIndicator: true,
						navLinks: true,
						events: events,
						dayClick: function (date, jsEvent, view) {
							// let inputDate = moment(date).format('YYYY-MM-DD HH:mm');
							// var moment2 = $("#" + id + element.id).fullCalendar('getDate');
							// if (date <= moment2){
							//     return false;
							// }
							//alert('Clicked on: ' + date.format());
							//if (moment().format('YYYY-MM-DD') === date.format('YYYY-MM-DD') || date.isAfter(moment())) {
							// This allows today and future date
							//} else {
							// Else part is for past dates
							//}

							//console.log("dayClick date",date);
							////console.log("dayClick jsEvent", jsEvent);
							//console.log("dayClick View", view);
							let count = 0;
							var clickedTime =
								"{id:'" +
								id +
								"'," +
								"dayLiteral:'" +
								date.format("dddd") +
								"',date:'" +
								date.format("YYYY-MM-DD") +
								"',monthLiteral:'" +
								date.format("MMMM") +
								"',day:'" +
								date.format("DD") +
								"',year:'" +
								date.format("YYYY") +
								"',hour:'" +
								date.format("HH:mm") +
								"',hourSelected:'" +
								date.format("HH:mm") +
								"',agentId:'" +
								element.id +
								"',counselorName:'" +
								name +
								"',view:'" +
								view.name +
								"'}";

							let dateField = date.format("YYYY-MM-DD HH:mm");
							//console.log(dateField, options.fn) //addScheduleForm

							/* if (dateField != "2022-02-02 10:00") {
								eval(options.fn + "(" + clickedTime + ")");
							} */
							if (dataDateBlock != null) {
								for (let u = 0; u < dataDateBlock.length; u++) {
									const elem = dataDateBlock[u];
									/* let start = elem.start;
									let end = elem.end;
									let dateStart = start.format("YYYY-MM-DD HH:mm");
									let dateEnd = end.format("YYYY-MM-DD HH:mm"); */
									let dateNow = new Date(dateField + ":00");
									let dateStart = new Date(elem.dateStart);
									let dateEnd = new Date(elem.dateEnd);

									/* //console.log(elem);
									//console.log(dateNow);
									//console.log(dateStart);
									//console.log(dateEnd); */

									if (dateNow >= dateStart && dateNow <= dateEnd) {
										count++;
										//console.log("error", elem);
									}
								}
							}

							if (count == 0) {
								eval(options.fn + "(" + clickedTime + ")");
							}

						},
						eventClick: function (date, jsEvent, view) {
							//console.log("eventClick date", date, jsEvent, view);
							const dateItem = date.start._i;
							const dateItemEnd = date.end._i;
							const array = dateItem.split(" ");
							const arrayEnd = dateItemEnd.split(" ");

							const data = {
								dayLiteral: dayLiteral({
									dateInput: array[0],
								}),
								dateStart: array[0],
								dateStartHours: array[1],
								dateEnd: arrayEnd[0],
								dateEndHours: arrayEnd[1]
							};

							date["data"] = {
								...data
							};
							//console.log("date", date);
							openScheduleForm(date);
						},

						eventRender: function (info) {

						},
						select: function (start, end) {
							//console.log("select", start, end);
						},
						selectAllow: function (select) { },
						dayCellContent: function (info, create) {
							//console.log("dayCellContent", info, create);
						},
						dayRender: function (date, cell) {
							/* 	// //console.log("dayRender date",date.format("YYYY-MM-DD HH:mm"));
								// //console.log("dayRender cell", cell) */
							//console.log("dataDateBlock", dataDateBlock);

							var dateItem = date.format("YYYY-MM-DD");
							//console.log(dateItem, date, cell);

							if (dataDateBlock != null) {
								for (let u = 0; u < dataDateBlock.length; u++) {
									const elem = dataDateBlock[u];
									let date = elem.date;
									if (date == dateItem) {
										cell.css("background-color", "#FEE2E2");
									}
								}
							}

						},

						bindSegHandlersToEl: function (info) {
							//console.log("handleSegClick", info);
						}
					});

					/* 				$("#" + id + element.id+ " *[data='disabled']").each(function (e) {
										//console.log("disabled", e);
									});
					 */

					/* $("#" + id + element.id).dblclick(function(e) {
					//console.log(element.id);
					//console.log(e);
					//console.log(e.target.getAttribute("data-date"));
					let id = element.id;
					let date = $.fullCalendar.moment(e.target.getAttribute("data-date"));
					let clickedTime ="{id:'"+id+"'," +
							"dayLiteral:'" +
							date.format("dddd") +
							"',date:'" +
							date.format("YYYY-MM-DD") +
							"',monthLiteral:'" +
							date.format("MMMM") +
							"',day:'" +
							date.format("DD") +
							"',year:'" +
							date.format("YYYY") +
							"',hour:'" +
							date.format("hh:mm") +
							"',hourSelected:'" +
							date.format("hh:mm") +
							"',agentId:'" +
							element.id +
							"',counselorName:'" +
							name +
							"',view:'" +
							view.name +
							"'}"
						//console.log(clickedTime);
				}); */

					$("#" + id + element.id + " .fc-left").append(
						`<div class="title">
						<button class="btnCalendarList" counselor="${element.id}"   name="${name}"><i class="icon icon-list"></i></button>
						<span class="nameCounselor">${name}</span>
					</div>`
					);

					let w = $(window).width();
					let h = $(window).height();

					if (w < 850 && h < 850) {
						$("#" + id + element.id + " .nameCounselor").detach().prependTo("#" + id + element.id + " .fc-center");
					}


				})
					.catch(console.warn());
			}
		} else {
			alertMessageError({ message: response.message })
		}
	}).catch(console.warn());
}



export function addScheduleForm(date) {
	//console.log("addScheduleForm", date, listAccounts);
	let data = date;
	let formCal = $("#formCalendar");
	//console.log("addScheduleForm Data", data)

	if (formCal.length == 0) {

		getData({
			task: 'getTypesAppointments',
			return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
		}).then((responseTypes) => {
			console.log('getData getTypesAppointments', responseTypes, listAccounts);
			if (responseTypes.status == 'success') {
				let array = responseTypes.data;
				let html = "";
				let strTypes = "";

				if (array != 0) {
					html = array.map(element => {
						let id = element.id;
						let name = element.name;
						let type = element.type;
						let path = element.path;
						let json = element.json;
						let color = element.color;

						return /*html*/ `<option value="${path}" data-type="${type}" data-color="${color}" >${name}</option>`;

					});
					strTypes = html.join("");
				}



				data['typesFormCalendar'] = strTypes;

				$(".bodyModule[module='" + module + "']").prepend(renderFormCalendar(data));
				//$("#inputSearchAccount").focus();
				$("#inputSearchAccountUsers").focus();

				$("#formCalendar").draggable();
				let str = "";
				let strBtns = "";
				let usersLength = 0;
				let usersList = sortJSON(listAccounts.data, "id", "asc");

				if (listAccounts.data != 0) {
					usersLength = listAccounts.data.length;
				}
				console.log('usersList', usersList, usersLength);
				for (let i = 0; i < usersLength; i++) {
					const element = usersList[i];
					const name = element.name + " " + element.lastnameFather;
					str += `<buttom class="btn item-${element.id} btnListSelect" value="${element.id
						}" data-ci="${element.ci}"><span>${element.id}.- ${element.name +
						" " +
						element.lastnameFather +
						" " +
						element.lastnameMother
						} </span><i class="icon icon-chevron-right"></i></buttom>`;
				}

				strBtns = `<buttom class="btn btnIconLeft btnFull btnSmall btnSearchUserNotAssigned btnSearchOut disabled" for="inputAccount" data-agent="${date.agentId}"><i class="icon icon-search"></i><span>Buscar no asignados</span></buttom>`;
				strBtns += `<buttom class="btn btnIconLeft btnFull btnSmall btnCreatePersonAgenda" data-agent="${date.agentId}"><i class="icon icon-user"></i><span>Crear Aconsejado</span></buttom>`;
				//console.log('strBtns', strBtns, str);
				$(".otherResultsPerson[for='inputAccount']").html(str);
				$(".btnsPerson[for='inputAccount']").html(strBtns);

				//$(`.bodyModule[module='${module}'] #selectTypeFormCalendar`).html(html.join(""));
				jQuery.datetimepicker.setLocale("es")

				$("#inputDateTime").datetimepicker({
					timepicker: false,
					datepicker: true,
					format: "Y-m-d",
					lang: "es",
				});

				$("#inputDateInit").datetimepicker({
					timepicker: false,
					datepicker: true,
					format: "Y-m-d",
					lang: "es",
				})

				$("#inputDateStart").datetimepicker({
					timepicker: false,
					datepicker: true,
					format: "Y-m-d",
					lang: "es",
				})

				$("#inputDateEnd").datetimepicker({
					timepicker: false,
					datepicker: true,
					format: "Y-m-d",
					lang: "es",
				})
			} else {
				alertMessageError({
					message: response.message
				})
			}
		}).catch(console.warn());


	}

}

export function createPersonExpress(vars) {
	//console.log("createPersonExpress", vars);
	addHtml({
		selector: "body",
		type: 'prepend',
		content: renderModal({
			id: "modalCreateAdviser",
			body: renderFormNewPersonExpress({
				id: 'formCreatePersonExpress',
				agentId: vars.counselor,
				inputReturn: vars.inputReturn
			})
		})
	}) //type: html, append, prepend, before, after

	$("#inputBirthday").datetimepicker({
		timepicker: false,
		datepicker: true,
		format: "Y-m-d",
		lang: "es",
	})
}



export const getData = async (vars = []) => {
   //console.log('getData', vars);
   const url = _PATH_WEB_NUCLEO + "modules/appointments/controllers/apis/v1/calendars.php";
   let data = JSON.stringify({
	   accessToken: accessToken(),
	   vars: JSON.stringify(vars)
   });
   try {
	   let res = await axios({
		   async: true,
		   method: "post",
		   responseType: "json",
		   url: url,
		   headers: {},
		   data: data
		})
	   //console.log(res.data);
	   jointActions(res.data)
	   return res.data
   } catch (error) {
	  console.log("Error: getData ", error);
   }
}

document.addEventListener("DOMContentLoaded", function () {

	$("body").on("click", `.btnCalendarList`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let couId = $(this).attr("counselor");
		let name = $(this).attr("name");
		//console.log(`.btnCalendarList`, data);
		windowCalendarList({
			couId,
			name
		})
	});

	$("body").on("click", ".btnCalendarAgent", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let item = $(this).attr("item");
		$(".btnCalendarAgent").removeClass("active");
		$(this).addClass("active");
		$(".bodyModule[module='" + module + "'] .calendar").removeClass(
			"active"
		);
		$(
			".bodyModule[module='" +
			module +
			"'] .calendar[id='calendarAdvised" +
			item +
			"']"
		).addClass("active");
	});

	$("body").on("click", ".btnConfig[module='" + module + "']", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		//console.log("btnConfig", "calendarAdvised");
		let module = this.getAttribute("module");
		let system = this.getAttribute("system");
		const idModal = "modalConfigCalendarAdvised";

		//accountingPlan().then((response) => {
		let body = renderModalConfigBody({
			title: "Configuración",
			system,
			id: idModal,
			sidebar: renderModalConfigSidebar({
				module
			}),
			body: renderModalConfigContentBodyCalendar(),
		});

		$(".ws[system='" + system + "']").append(
			renderModalConfig({
				body,
				system,
				id: idModal
			})
		);



		const counselors = JSON.parse(localStorage.getItem("counselors"));
		let str = "";
		if (counselors != 0) {
			for (let i = 0; i < counselors.length; i++) {
				const name = counselors[i].name;
				const lastname = counselors[i].lastname;
				const agentId = counselors[i].id;
				const state = counselors[i].stateRRHH.state;
				const stateBody = renderBtnAddHours({
					id: agentId
				});
				str += renderBtnStateSwitch({
					title: name + " " + lastname,
					id: agentId,
					state,
					stateBody,
				});
			}
			$(".ws[system='" + system + "'] #" + idModal + " .listConunselorsHours").append(str);
			$(".ws[system='" + system + "'] #" + idModal + ".boxHours").html(renderLoading());
			loadHoursCounselors().then((response) => {
				$(".ws[system='" + system + "'] #" + idModal + ".boxHours").html();
				for (let i = 0; i < response.length; i++) {
					let id = response[i].id;
					let count = response[i].count;
					let day = response[i].day;
					let hourStart = response[i].hourStart;
					let hourEnd = response[i].hourEnd;
					let item = "hour" + id + count + day;
					$(".boxHours[item='" + id + "'][day='" + day + "']").append(
						" " +
						renderSelectHoursStartEnd({
							id: item,
							hourStart,
							hourEnd,
							attr: "item=" +
								id +
								" count=" +
								count +
								" day=" +
								day,
						}) +
						" <a class='btnRemoveHours'  item='" + id + "' count='" + count + "' day='" + day + "' ><i class='icon icon-close'></i></a>"
					);
					$("#btnAddHours" + id + "[day='" + day + "']").attr("count", count);
				}
			});
		}

		resizeModalConfig(system);
		//})
	});

	$("body").on("click", "#blockedDatesCalendar .day", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let date = $(this).attr("date");
		let state = $(this).attr("state");
		let item = $(this).attr("item");
		let allday = $(this).attr("allday");

		$("#formBlockDateAdvised").remove();
		//console.log(date);
		if (state == 0) {
			addBlockedDates({
				date
			});
		}
		if (state == 1) {
			//console.log("editDateBlocked");
			editBlockedDates({
				date,
				item,
				allday
			});
		}
	});

	$("body").on("click", "#btnRegister", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		const dateInput = $(this).attr("date");
		const id = $(this).attr("for");
		const stateInputAllDay = $("#inputAllDay").val();
		let hourStartInput = "00:00";
		let hourEndInput = "23:45";

		if (stateInputAllDay == 0) {
			hourStartInput = $("#inputHourStart" + id).val();
			hourEndInput = $("#inputHourEnd" + id).val();
		}
		//console.log('btnRegister', id, stateInputAllDay, dateInput, hourStartInput, hourEndInput);
		//disableId("btnRegister");

		saveHoursBlocked({
			dateInput,
			hourStartInput,
			hourEndInput,
			inputAllDay: stateInputAllDay
		}).then((response) => {
			//console.log('saveHoursBlocked', response, response.state, localStorage.getItem("daysBlocked"));
			if (response.Error == "0") {

				let item = response.state;
				let dayBlockedLocal = localStorage.getItem("daysBlocked");
				let daysBlocked = 0;
				if (dayBlockedLocal != null && dayBlockedLocal != "" && dayBlockedLocal != "undefined") {
					let daysBlocked = JSON.parse(localStorage.getItem("daysBlocked"));
				}

				if (daysBlocked == 0) {

					daysBlocked = [];
					daysBlocked.push(item);
				} else {
					daysBlocked.push(item);

				}
				localStorage.setItem("daysBlocked", JSON.stringify(daysBlocked));


				$("#formBlockDateAdvised").remove();
				//console.log("date", dateInput);
				$(".day[date='" + dateInput + "']").attr("state", 1);
				$(".day[date='" + dateInput + "']").attr("item", item.id);
				$(".day[date='" + dateInput + "']").addClass("blocked");

				calendarAdvisedIndex();

			} else {
				alertResponseMessageError({
					message: response.message
				});
			}

		}).catch(console.warn());
	})

	$("body").on("click", "#btnUpdateBlocked", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();

		let item = $(this).attr("item");
		//console.log('#btnUpdateBlocked', item);

		const dateInput = $(this).attr("date");
		const id = $(this).attr("for");
		const stateInputAllDay = $("#inputAllDay").val();
		let hourStartInput = "00:00";
		let hourEndInput = "23:45";

		if (stateInputAllDay == 0) {
			hourStartInput = $("#inputHourStart" + id).val();
			hourEndInput = $("#inputHourEnd" + id).val();
		}

		//console.log('btnUpdateBlocked', id, stateInputAllDay, dateInput, hourStartInput, hourEndInput);

		updateHoursBlocked({
			item,
			dateInput,
			hourStartInput,
			hourEndInput
		}).then((response) => {
			//console.log('updateHoursBlocked', response);
			if (response.Error == "0") {
				updateDayBlockedCalendar({
					item,
					dateInput,
					hourStartInput,
					hourEndInput
				})
			} else {
				alertMessageError({
					message: response.message
				});
			}
		}).catch(console.warn());
	})

	$("body").on("click", "#btnDeleteBlocked", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();

		let item = $(this).attr("item");
		//console.log('#btnDeleteBlocked', item);
		deleteDateBloked({
			item
		}).then((response) => {
			//console.log('deleteDateBloked', response);
			if (response.Error == "0") {
				deleteDayBlockedCalendar({
					item
				})

			} else {
				alertMessageError({
					message: response.message
				});
			}

		}).catch(console.warn());
	})

	$("body").on("change", "#inputAllDay", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		if (this.checked) {
			//console.log("seleccionado");
			$(this).val("1");
			$("#formBlockDateAdvised .others").html("");
		} else {
			$(this).val("0");
			//console.log("no seleccionado", parentId);
			loadFormHoursBlocked();
		}
	});

	$("body").on("click", ".btnAddHours", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let id = $(this).attr("item");
		let day = $(this).attr("day");
		let count = $(this).attr("count");
		count++;
		//console.log(".btnAddHours", id, day, count);

		$(".boxHours[item='" + id + "'][day='" + day + "']").append(
			" " +
			renderSelectHoursStartEnd({
				id: 'hour' + id + count + day,
				attr: "item=" + id + " count=" + count + " day=" + day,
			}) +
			" <a class='btnRemoveHours' item='" + id + "' count='" + count + "'  day='" + day + "' ><i class='icon icon-close'></i></a>"
		);

		$(".btnAddHours[item='" + id + "'][day='" + day + "']").attr("count", count);

	});

	$("body").on("click", ".btnRemoveHours", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let id = $(this).attr("item");
		let count = $(this).attr("count");
		let day = $(this).attr("day");
		//console.log("btnRemoveHours", id, count);
		let item = id + count + day;
		//$(".selectHoursStartEnd[item='" + item + "'][count='" + count + "'][day='" + day + "']").remove();
		$(".selectHoursStartEnd[item='hour" + item + "']").remove();
		$(this).remove();
		//Aqui fn que remueve los datos
		deteleHours({
			id,
			count,
			day
		}).then((response) => {
			//console.log('deteleHours', response);

		}).catch(console.warn());
	});

	$("body").on("change", "input[name='inputHourStart']", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let valueStart = $(this).val();
		let id = $(this).attr("item");
		let day = $(this).attr("day");
		let count = $(this).attr("count");
		let valueEnd = $(
			"input[name='inputHourEnd'][item='" +
			id +
			"'][count='" +
			count +
			"']"
		).val();

		//console.log(valueStart,valueEnd, id, count);
		modifyHours({
			hourStart: valueStart,
			hourEnd: valueEnd,
			item: id,
			count,
			day,
		});
	});

	$("body").on("change", "input[name='inputHourEnd']", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let valueEnd = $(this).val();
		let id = $(this).attr("item");
		let day = $(this).attr("day");
		let count = $(this).attr("count");
		let valueStart = $(
			"input[name='inputHourStart'][item='" +
			id +
			"'][count='" +
			count +
			"']"
		).val();

		//console.log(valueStart,valueEnd, id, count);
		modifyHours({
			hourStart: valueStart,
			hourEnd: valueEnd,
			item: id,
			count,
			day,
		});
	});

	$("body").on("click", ".btnRegisterSheduleCalendar", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		const btn = "btnRegisterSheduleCalendar";
		const form = $(this).attr("form");
		const acu = $("#" + form + " #inputAccount").val();
		console.log("btnRegister", form, acu);
		const type = $("#selectTypeFormCalendar").val();
		const dataType = $("#selectTypeFormCalendar option:selected").attr('data-type');
		console.log("fomr", JSON.stringify($("#" + form).serializeArray()));

		let vars ={
			inputProviderId: $(`#formcalendarAgent #selectProvider option:selected`).attr('data-id'),
			inputCounselorId: 4,
			inputAccount: acu,
			inputType: type,
			inputDataType: dataType,
			inputDate: $(`#formcalendarAgent #inputDate`).val(),
			inputDateEnd: $(`#formcalendarAgent #inputDateEnd`).val(),
			inputPlace: $(`#formcalendarAgent #inputPlace`).val(),
			inputOthers: $(`#formcalendarAgent #inputOthers`).val(),
			inputComments: $(`#formcalendarAgent #inputComments`).val(),
			inputName: $(`#formcalendarAgent #inputName`).val(),
			inputCouId: $(`#formcalendarAgent #inputCounselors`).val(),
			inputHourStart: $(`#formcalendarAgent #inputHourStart`).val(),
			inputHourEnd: $(`#formcalendarAgent #inputHourEnd`).val(),
			inputCount: $(`#formcalendarAgent #inputCount`).val(),
		};
		console.log("vars", vars);

		if (acu != "") {

		 
			getData({
				task: 'addShedule',
				return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
				vars,
			}).then((response) => {
				console.log('getData addShedule', response);
				if (response.status == 'success') {
					console.log("🚀 ~ file: calendar.js:1160 ~ addSchedule ~ response:", response, response.data)
					let data = response.data;
					if (response.Error == 0) {
						//efect btn success
						buttonsEffects({
							id: "btnRegisterSheduleCalendar",
							innerHtml: "¡Registro exitoso!",
							effect: "animated flash",
							cls: "btnSuccess disabled",
						});

						setTimeout(() => {
							removeModalId("formCalendar");
							const typeAppointment = data.typeAppointment;
							let listTypesAppointmentsLS = JSON.parse(localStorage.getItem('typesAppointments'));

							let typeAppointmentSelect = listTypesAppointmentsLS.find(element => element.path == typeAppointment);
							//console.log("🚀 ~ file: calendarAdvised.js:1827 ~ setTimeout ~ $typeAppointmentDOM:", typeAppointmentSelect)


							$('#calendarAdvised' + data.couId).fullCalendar('renderEvent', {
								title: data.accountDataId.name + ' ' + data.accountDataId.lastname,
								id: data.dataCalId.id,
								item: data.dataCalId.id,
								type: data.typeAppointment,
								module,
								system,
								details: data.dataCalId.details,
								place: data.dataCalId.place,
								state: data.dataCalId.state,
								agentId: data.couId,
								hourStart: data.hourStart,
								hourEnd: data.hourEnd,
								start: data.dataCalId.dateStart,
								end: data.dataCalId.dateEnd,
								color: typeAppointmentSelect.color,
								dial: data.accountDataId.dial,
								celular: data.accountDataId.celular,
								textColor: "#555",
								email: data.accountDataId.email,
								counselorName: data.counselorName,
								acuId: data.accountDataId.id,
								dataAcuId: data.accountDataId,
								linkCelular: data.linkCelular
							});

							//calendarAdvisedIndex();
						}, 1000);
					} else {
						/* alertResponseMessageError({
							message: response.message
						}); */
						const wModalComment = $("#inputComments").width();
						$("#" + form + " #messageRegisterShedule").html("<div class='messageAlert' style='max-width:" + wModalComment + "px'>" + response.message + "</div>");

						setTimeout(() => {
							$("#" + form + " #messageRegisterShedule").html("");
						}, 3500);
					}
				} else {
					alertMessageError({
						message: response.message
					})
				}
			}).catch(console.warn());


			/* addSchedule({
				form,
				type,
				dataType
			}).then((response) => {
				console.log("🚀 ~ file: calendar.js:1160 ~ addSchedule ~ response:", response, response.data)
				let data = response.data;
				if (response.Error == 0) {
					//efect btn success
					buttonsEffects({
						id: "btnRegisterSheduleCalendar",
						innerHtml: "¡Registro exitoso!",
						effect: "animated flash",
						cls: "btnSuccess disabled",
					});

					setTimeout(() => {
						removeModalId("formCalendar");
						const typeAppointment = data.typeAppointment;
						let listTypesAppointmentsLS = JSON.parse(localStorage.getItem('typesAppointments'));

						let typeAppointmentSelect = listTypesAppointmentsLS.find(element => element.path == typeAppointment);
						//console.log("🚀 ~ file: calendarAdvised.js:1827 ~ setTimeout ~ $typeAppointmentDOM:", typeAppointmentSelect)


						$('#calendarAdvised' + data.couId).fullCalendar('renderEvent', {
							title: data.accountDataId.name + ' ' + data.accountDataId.lastname,
							id: data.dataCalId.id,
							item: data.dataCalId.id,
							type: data.typeAppointment,
							module,
							system,
							details: data.dataCalId.details,
							place: data.dataCalId.place,
							state: data.dataCalId.state,
							agentId: data.couId,
							hourStart: data.hourStart,
							hourEnd: data.hourEnd,
							start: data.dataCalId.dateStart,
							end: data.dataCalId.dateEnd,
							color: typeAppointmentSelect.color,
							dial: data.accountDataId.dial,
							celular: data.accountDataId.celular,
							textColor: "#555",
							email: data.accountDataId.email,
							counselorName: data.counselorName,
							acuId: data.accountDataId.id,
							dataAcuId: data.accountDataId,
							linkCelular: data.linkCelular
						});

						//calendarAdvisedIndex();
					}, 1000);
				} else {
					/* alertResponseMessageError({
						message: response.message
					}); 
					const wModalComment = $("#inputComments").width();
					$("#" + form + " #messageRegisterShedule").html("<div class='messageAlert' style='max-width:" + wModalComment + "px'>" + response.message + "</div>");

					setTimeout(() => {
						$("#" + form + " #messageRegisterShedule").html("");
					}, 3500);
				}
			}); */
		} else {
			$("#" + form + " #inputAccountLabel").addClass("error");
			$("#" + form + " #inputAccountLabel").attr(
				"placeholder",
				"Adiciona a un usuario. Por Favor."
			);
		}
	});

	$("body").on("click", ".btnTransfer", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		//console.log(".btnTransfer");
	});

	$("body").on("click", ".btnStateCancel", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		//console.log(".btnStateCancel");
		let item = $(this).attr("item");
		let name = $(this).attr("name");
		let counselor = $(this).attr("counselor");
		let vars = item + "," + counselor;
		//console.log(vars);

		addHtml({
			selector: ".bodyModule[module='" + module + "']",
			type: 'prepend',
			content: renderDeleteModal({
				name: "Cita de " + name,
				id: "modalDeleteSchedule",
				cls: 'btnDeleteSchedule',
				attr: `data-fn="deleteFormSchedule" data-id="${item}" data-agent="${counselor}"`,
			})
		})
		/* 		changeStateItem(item)
			.then((response) => {
				//console.log(response);
			})
			.catch(console.warn()); */
	});

	$("body").on("click", ".btnCancelShedule", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		//console.log("btnCancelShedule");
		let item = $(this).attr("item");
		let name = $(this).attr("name");
		let counselor = $(this).attr("counselor");
		cancelShedule({
			item,
			name,
			counselor
		});
	});

	$("body").on("click", ".btnConfirmCancelShedule", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let value = $("input[name='cancelShedule']:checked").val();
		const item = $(this).attr("vars");
		const couId = $(this).attr("counselor");
		//console.log(".btnConfirmCancelShedule");
		if (value == "other") {
			value = $("#inputOtherCancel").val();
		}
		//console.log( $("input[name='cancelShedule']:checked").val() );
		//console.log(value, item);
		stateCancelShedule({
			item,
			value
		})
			.then((response) => {
				//console.log(response);
				if (response.Error == 0) {
					//efect btn success
					$(".modalConfirm").remove();
					$(".modalFormCalendar").addClass("animated fadeOut ");
					setTimeout(() => {
						$(".modalFormCalendar").remove();
						$("#calendarAdvised" + couId).fullCalendar(
							"removeEvents",
							item
						);
					}, 600);

				} else {
					alertResponseMessageError({
						message: response.message
					});
				}
			})
			.catch(console.warn());
	});

	$("body").on("click", ".btnReshedule", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		const item = $(this).attr("item");
		const couId = $(this).attr("counselor");
		//console.log(".btnReshedule", item);
		const data = $(this).data();
		const inputDate = $("#formCalendar #inputDate").val();
		const inputHourEnd = $("#formCalendar #inputHourEnd").val();
		const inputHourStart = $("#formCalendar #inputHourStart").val();
		let count = 0;
		disabledBtn("#btnResheduleCalAdvised");
		changeBtn({
			selector: ".btnReshedule",
			type: "loading"
		})

		if (inputHourEnd == data.hourend || inputHourStart == data.hourstart) {
			if (inputDate == data.start) {
				alertResponseMessageError({
					message: "Mínimo necesita hacer un cambio de hora para reagendar la cita."
				});
				count++;
				unDisabledBtn("#btnResheduleCalAdvised");
			}
		}

		if (count == 0) {
			reshedule(item)
				.then((response) => {
					console.log("reshedule", response);
					if (response.Error == 0) {
						let data = response.data;
						//efect btn success


						//console.log("🚀 ~ file: calendarAdvised.js:1827 ~ setTimeout ~ $typeAppointmentDOM:", typeAppointmentSelect)

						calendarAdvisedIndex({
							couIdActive: couId
						});
						/* console.log("🚀 ~ calendario reagendado")
						 */

						removeModalId("formCalendar");




						/* $("#calendarAdvised" + couId).fullCalendar("removeEvents", response.item);

						const typeAppointment = data.typeAppointment;
						let listTypesAppointmentsLS = JSON.parse(localStorage.getItem('typesAppointments'));

						let typeAppointmentSelect = listTypesAppointmentsLS.find(element => element.path == typeAppointment);
						$('#calendarAdvised' + data.couId).fullCalendar('renderEvent', {
							title: data.accountDataId.name + ' ' + data.accountDataId.lastname,
							id: data.dataCalId.id,
							item: data.dataCalId.id,
							type: data.typeAppointment,
							module,
							system,
							details: data.dataCalId.details,
							place: data.dataCalId.place,
							state: data.dataCalId.state,
							agentId: data.couId,
							hourStart: data.hourStart,
							hourEnd: data.hourEnd,
							start: data.dataCalId.dateStart,
							end: data.dataCalId.dateEnd,
							color: "#88cff5",
							dial: data.accountDataId.dial,
							celular: data.accountDataId.celular,
							textColor: "#555",
							email: data.accountDataId.email,
							counselorName: data.counselorName,
							acuId: data.accountDataId.id,
							dataAcuId: data.accountDataId,
							linkCelular: data.linkCelular
						}); */

						//calendarAdvisedIndex();
					} else {
						alertResponseMessageError({
							message: response.message,
							time: "6500"
						});
						//timerout 6500
						setTimeout(() => {
							unDisabledBtn("#btnResheduleCalAdvised");
							changeBtn({
								selector: ".btnReshedule",
								text: "Reagendar",
								btnCls: "btn btnPrimary",
								type: "custom"
							})
						}, 6500);

					}
				})
				.catch(console.warn());
		}
	});

	$("body").on("click", `.btnDeleteSchedule[data-fn='deleteFormSchedule']`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		const data = $(this).data();
		//console.log(`.btnDeleteConfirmation[data-fn='deleteFormSchedule']`, data);
		deleteSchedule({
			item: data.id
		}).then((response) => {
			if (response.Error == 0) {
				deleteScheduleCalendar({
					item: data.id,
					couId: data.counselor
				});
			} else {
				alertPage({
					text: "Error. por favor contactarse con soporte. " +
						response.message,
					icon: "icn icon-alert-warning",
					animation_in: "bounceInRight",
					animation_out: "bounceOutRight",
					tipe: "danger",
					time: "3500",
					position: "top-left",
				});
			}
		}).catch(console.warn());
	})

	$("body").on("click", `.btnCreatePersonAgenda`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let inputReturn = 'inputAccount';
		data["inputReturn"] = inputReturn;
		//console.log(`.btnCreatePersonAgenda`, data);
		$(".inputSearch[for='" + inputReturn + "']").val("");
		$(".boxSelectList[for='" + inputReturn + "'] .list .btn").show();
		$(".boxSelectList[for='" + inputReturn + "'] .list").scrollTop();
		createPersonExpress(data);


	});

	$("body").on("click", `.btnSearchUsersAdvised[content='searchUsersAdvised']`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		//console.log(`.btnSearchUsersAdvised[content='searchUsersAdvised']`, data);
		searchUsersAdvised(data);

	});

	$("body").on("keyup", `#inputSearchAccountCalendarAdvised`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let input = $(this).val();
		//console.log(`#inputSearchAccountCalendarAdvised`, data);

		if (input.length > 3) {
			getData({
				task: 'searchUsersAccounts',
				return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
				input: {
					couId: data.conunselor,
					search: input
				}
			}).then((response) => {
				console.log('getData searchUsersAccounts', response);
				if (response.status == 'success') {

				} else {
					alertMessageError({
						message: response.message
					})
				}
			}).catch(console.warn());
		}
	})

	$("body").on("keyup", `#inputSearchAccountUsers`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let input = $(this).val();
		let id = $(this).attr("for");
		console.log(`#inputSearchAccountUsers`, data, input);

		if (input.length > 3) {
			getData({
				task: 'searchUsersAccounts',
				return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
				input: {
					couId: data.conunselor,
					search: input
				}
			}).then((response) => {
				console.log('getData inputSearchAccountUsers', response);
				if (response.status == 'success') {
					let counselorsList = response.data.counselors;
					let counselorsAsigned = response.data.result;
					const rolEdit = response.data.rolEdit;

					console.log("counselorsAsigned", counselorsAsigned);

					$(".boxSelectList[for='" + id + "'] .otherResultsPerson").html("");

					if (counselorsAsigned != 0 && counselorsAsigned != undefined) {
						counselorsAsigned.forEach(element => {
							//console.log(element);
							let listCounselors = element.counselors.counselor;
							let numCounselors = element.counselors.numCounselors;
							let counselors = "";
							let auxBtnAsigned = "";
							if ((numCounselors <= 3) && (rolEdit == 1)) {
								auxBtnAsigned = `<button class="btn btnMini btnPrimary btnAsignedAdvised" data-acuid="${element.id}" data-name="${element.name}" for="${id}">Asignar</button>`;
							}
							if (numCounselors > 0) {
								listCounselors.forEach(cou => {
									counselors += `<span class="badge badgePill badgeDefault">${cou.name}</span> `;
								})
							}

							$(".boxSelectList[for='" + id + "'] .otherResultsPerson").append(`<div class="item" data-id="${element.id}" data-name="${element.name}"> 
												<div class="row">
													<label>${element.id} - ${element.name} </label>
													${auxBtnAsigned}
												</div>
												
												<div class="listCounselors">
													${counselors}
												</div>
												
										</div>`);
						});
					} else {
						$(".boxSelectList[for='" + id + "'] .otherResultsPerson").html("Sin resultados");
					}
				} else {
					alertMessageError({
						message: response.message
					})
				}
			}).catch(console.warn());
		}
	});

	$("body").on("click", `.otherResultsPerson .item`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		console.log(`.otherResultsPerson .item`, data);
		$("#formcalendarAdvised #inputAccount").val(data.id);
		$("#formcalendarAdvised #inputAccountLabel").val(data.id + " - " + data.name);

		$("#formcalendarAdvised .otherResultsPerson").html("");
		$("#formcalendarAdvised .btnSearchUserNotAssigned").addClass("disabled");
		$("#formcalendarAdvised #inputSearchAccount").val("");
		$("#formcalendarAdvised .boxSelectList").removeClass("on");
	});

	$("body").on("click", `.btnSearchUserNotAssigned`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let id = $(this).attr("for");
		//console.log(`.btnSearchUserNotAssigned`, data, id);

		getData({
			task: 'searchUserNotAssigned',
			return: 'returnArray', // returnId, returnState, returnArray
			search: $(".inputSearch[for='" + id + "']").val(),
		}).then((response) => {
			console.log('getData searchUserNotAssigned', response);
			if (response.status == 'success') {
				let counselorsList = response.data.counselors;
				let counselorsAsigned = response.data.result;

				//console.log("counselorsAsigned", counselorsAsigned);

				$(".boxSelectList[for='" + id + "'] .otherResultsPerson").html("");

				if (counselorsAsigned != 0 && counselorsAsigned != undefined) {
					counselorsAsigned.forEach(element => {
						//console.log(element);
						let listCounselors = element.counselors.counselor;
						let numCounselors = element.counselors.numCounselors;
						let counselors = "";
						let auxBtnAsigned = "";
						if (numCounselors <= 3) {
							auxBtnAsigned = `<button class="btn btnMini btnPrimary btnAsignedAdvised" data-acuid="${element.id}" data-name="${element.name}" for="${id}">Asignar</button>`;
						}
						if (numCounselors > 0) {
							listCounselors.forEach(cou => {
								counselors += `<span class="badge badgePill badgeDefault">${cou.name}</span> `;
							})
						}

						$(".boxSelectList[for='" + id + "'] .otherResultsPerson").append(`<div class="item"> 
												<div class="row">
													<label>${element.id} - ${element.name} </label>
													${auxBtnAsigned}
												</div>
												
												<div class="listCounselors">
													${counselors}
												</div>
												
										</div>`);
					});
				} else {
					$(".boxSelectList[for='" + id + "'] .otherResultsPerson").html("Sin resultados");
				}

			} else {
				alertMessageError({
					message: response.message
				})
			}
		}).catch(console.warn());


	});

	$("body").on("click", `.btnAsignedAdvised`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		//console.log(`.btnAsignedAdvised`, data);

		getData({
			task: 'asignedAdvised',
			return: 'returnState', // returnId, returnState, returnArray
			acuId: data.acuid,
		}).then((response) => {
			//console.log('getData asignedAdvised', response);
			if (response.status == 'success') {
				alertMessageSuccess({
					message: 'Asignación exitosa!'
				})
				$("#formcalendarAdvised .otherResultsPerson").html("");
				$("#formcalendarAdvised .btnSearchUserNotAssigned").addClass("disabled");
				$("#formcalendarAdvised #inputSearchAccount").val("");
				$("#formcalendarAdvised .boxSelectList").removeClass("on");

				$("#formcalendarAdvised #inputCouId").val(userData.userId);
				$("#formcalendarAdvised #inputAccount").val(data.acuid);
				$("#formcalendarAdvised #inputAccountLabel").val(data.acuid + " - " + data.name);

			} else {
				alertMessageError({
					message: response.message
				})
			}
		}).catch(console.warn());

	});

	//doble click
	$("body").on("dblclick", `.btnEditDataAccount`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let id = 'modalEditDateAccount';
		let name = data.name;
		let celular = data.celular;
		let email = data.email;
		//console.log(`.btnEditDateAccount`, data);

		addHtml({
			selector: 'body',
			type: 'prepend',
			content: renderModal({
				id,
				body: renderEditDataAccount({
					id,
					name,
					celular,
					email,
					item: data.id
				})
			})
		}) //type: html, append, prepend, before, after
	});


	$("body").on("click", `.btnSaveEditDataAccount`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let form = $(this).attr("for");
		let id = $(`#${form} #inputId`).val();
		let name = $(`#${form} #inputName`).val();
		let celular = $(`#${form} #inputCelular`).val();
		let email = $(`#${form} #inputEmail`).val();
		let data = {
			id,
			name,
			celular,
			email
		};
		//console.log(`.btnSaveEditDataAccount`, data);
		changeBtn({
			selector: ".btnSaveEditDataAccount",
			type: "loading"
		})
		getData({
			task: 'saveEditDataAccountCalendar',
			return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
			inputs: data
		}).then((response) => {
			//console.log('getData aveEditDataAccountCalendar', response);
			if (response.status == 'success') {
				changeBtn({
					selector: ".btnSaveEditDataAccount",
					type: "success"
				});
				timer(1500).then(() => {
					$("#modalEditDateAccount").remove();
				});
			} else {
				alertMessageError({
					message: response.message
				})
			}
		}).catch(console.warn());



	});

	$("body").on("change", `.bodyModule[module="${module}"] #selectTypeFormCalendar`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let value = $(this).val();
		let data = $(this).find('option:selected').data();
		let dataBase = $(`.bodyModule[module="${module}"] #inputDateBase`).val();
		let hStart = $(`.bodyModule[module="${module}"] #inputHourStart`).val();
		let hEnd = $(`.bodyModule[module="${module}"] #inputHourEnd`).val();
		console.log(`.bodyModule[module="${module}"] #selectTypeFormCalendar`, value, data);

		$("#formCalendar .boxColorFormCalendar").css("background-color", data.color);

		$(`.bodyModule[module='${module}'] #inputTypeAppointment`).val(value);
		$(`.bodyModule[module='${module}'] #inputTypeDate`).val(data.type);

		addHtml({
			selector: '.bodyModule[module="' + module + '"] #spaceDateFormCalendar',
			type: 'insert',
			content: selectTypesAppointments({
				type: data.type,
				dataBase,
				hStart,
				hEnd
			})
		}) //type: html, append, prepend, before, after

		$("#inputDateStart").datetimepicker({
			timepicker: false,
			datepicker: true,
			format: "Y-m-d",
			lang: "es",
		})

		$("#inputDateEnd").datetimepicker({
			timepicker: false,
			datepicker: true,
			format: "Y-m-d",
			lang: "es",
		})

	});



});