import {
    alertMessageError,
    replacePath,
    addHtml,
    alertPage,
    loadView,
    replaceEssentials,
    emptyReturn,
    jointActions,
    accessToken,
    changeBtn
} from '../../components/functions.js';

import {
    focusSelector
} from '../../components/forms.js';

let module = 'app_candire_campaigns';
let system = 'app_candire';
let formId = "formCampaignAppCandire";

export const app_candire_campaignsIndex = (vars = { module: module, system: system }) => {
    console.log('app_candire_campaignsIndex', vars);
    loadView(_PATH_WEB_NUCLEO + "modules/app_candire/views/campaigns.html?" + _VS).then((responseView) => {
        //console.log('loadView', responseView);
        let content = responseView;

        content = content.replace(/{{_BTN_TITLE}}/g, "Guardar");
        content = content.replace(/{{_BTN_CLS}}/g, "btnPrimary");
        content = content.replace(/{{_BTN_ID}}/g, "btnAdd" + formId);
        content = content.replace(/{{_FORM_ID}}/g, formId);

        getData({
            task: 'getCampaigns',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
        }).then((response) => {
            console.log('getData getCampaigns', response);
            if (response.status == 'success') {
                const data = response.data;
                const now = data.now;
                const nowLast = data.nowLast;

                addHtml({
                    selector:`.bodyModule[module="${vars.module}"] `,
                    type: 'append', //insert, append, prepend, replace
                    content 
                })
                
                focusSelector(`#${formId} #inputName`);
                $(`#${formId} #inputDateInit`).val(now);
                $(`#${formId} #inputDateEnd`).val(nowLast);
                
            }else{
                 alertMessageError({message: response.message})
            }
        }).catch(console.warn());

        
        
    })
   
}

export const addCampaign = (vars =[]) => {
    console.log('addCampaign',vars);
    let inputName = $(`#${formId} #inputName`).val();
    let inputDescription = $(`#${formId} #inputDescription`).val();
    let inputTypeCustomer = $(`#${formId} #inputTypeCustomer`).val();
    let inputCode = $(`#${formId} #inputCode`).val();
    let inputAgeInit = $(`#${formId} #inputAgeInit`).val();
    let inputAgeEnd = $(`#${formId} #inputAgeEnd`).val();
    let inputGender = $(`#${formId} #inputGender`).val();
    let inputDateInit = $(`#${formId} #inputDateInit`).val();
    let inputDateEnd = $(`#${formId} #inputDateEnd`).val();
    let inputJson = $(`#${formId} #inputJson`).val();

    let count = 0;

    if(!inputName) {
        alertMessageError({
            message: 'El nombre es requerido'
        })
        count++;
    }
    if (!inputCode){
        alertMessageError({
            message: 'El Código de cliente es requerido'
        })
        count++;
    }

    if(count == 0) {
        let inputs = {
            inputName,
            inputDescription,
            inputTypeCustomer,
            inputCode,
            inputAgeInit,
            inputAgeEnd,
            inputGender,
            inputDateInit,
            inputDateEnd,
            inputJson
        }

        getData({
            task: 'addCampaign',
            return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
            inputs
        }).then((response) => {
            console.log('getData addCampaign',response);
            if(response.status=='success'){
                
            }else{
                 alertMessageError({message: response.message})
            }
        }).catch(console.warn());
    }

}

export const getData = async (vars = []) => {
   //console.log('getData', vars);
   const url = _PATH_WEB_NUCLEO + "modules/app_candire/controllers/apis/v1/app_candire.php";
   let data = JSON.stringify({
       accessToken: accessToken(),
       vars: JSON.stringify(vars)
   });
   try {
       let res = await axios({
           async: true,
           method: "post",
           responseType: "json",
           url: url,
           headers: {},
           data: data
        })
       //console.log(res.data);
       jointActions(res.data)
       return res.data
   } catch (error) {
      console.log("Error: getData ", error);
   }
}

document.addEventListener("DOMContentLoaded", function () {
    $("body").on("click",`#btnAddformCampaignAppCandire`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`#btnAddformCampaignAppCandire`, data);
        //changeBtn({id: `btnAddformCampaignAppCandire`, type: 'save'});
        addCampaign(data);
    }); 

    $("body").on("change",`#inputGender`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let value = $(this).val();
        console.log(`#inputGender`, data, value);
        
        if (value == 'custom') {
            $(`#${formId} #inputGenderCustom`).attr('type', 'text');
            focusSelector(`#${formId} #inputGenderCustom`);
        } else {
            $(`#${formId} #inputGenderCustom`).val('');
            $(`#${formId} #inputGenderCustom`).attr('type', 'hidden');
        }
        
    });
});