<?php
header('Content-Type: text/html; charset=utf-8');
class APP_CANDIRE
{
    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function getCampaigns(array $var = null)
    {

        $now = $this->fmt->data->dateFormat();
        $nowLast = $this->fmt->data->dateCustom(["modification" => "+12 hours"]);
        $rtn["now"] = $now;
        $rtn["nowLast"] = $nowLast;
        //$rtn["campaings"] = $this->getCampaignsData();
        return $rtn;
    }

    public function addCampaign(array $var = null)
    {
        //return $var;
        $usuId = $var['usuId'];
        $entitieId = $var["entitieId"];
        $inputs = $var["vars"]["inputs"];
        $inputName = $inputs["inputName"];
        $inputDescription = $inputs["inputDescription"];
        $inputTags = $inputs["inputTags"];
        $inputTypeCustomer = $inputs["inputTypeCustomer"];
        $inputCode = $inputs["inputCode"];
        $inputAgeInit = $inputs["inputAgeInit"];
        $inputAgeEnd = $inputs["inputAgeEnd"];
        $inputGender = $inputs["inputGender"];
        $inputGenderCustom = $inputs["inputGenderCustom"];
        $inputDateInit = $inputs["inputDateInit"];
        $inputDateEnd = $inputs["inputDateEnd"];
        $inputJson = $input["inputJson"];

        $now = $this->fmt->data->now();

        $insert = "mod_cnd_cp_name,mod_cnd_cp_description,mod_cnd_cp_tags,mod_cnd_cp_acu_id,mod_cnd_cp_type_customer,mod_cnd_cp_cpe_id,mod_cnd_cp_cen_id,mod_cnd_cp_target_init_age,mod_cnd_cp_target_end_age,mod_cnd_cp_target_gender,mod_cnd_cp_init_date,mod_cnd_cp_end_date,mod_cnd_cp_register_date,mod_cnd_cp_json,mod_cnd_cp_state";
        $values ="'" .$inputName. "','".$inputDescription."','".$inputTags."','".$acuId."','".$typeCustomer."','".$cpeId."','".$cenId."','".$inputAgeInit."','".$inputAgeEnd."','".$inputGender."','".$inputDateInit."','".$inputDateEnd."','".$now."','".$inputJson."','1'";
        $sql= "insert into mod_candire_campaigns (".$insert.") values (".$values.")";
        $this->fmt->querys->consult($sql,__METHOD__);     

        $sql = "select max(mod_cnd_cp_id) as id from mod_candire_campaigns";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        return $row['id'];
    }

    public function getCampaignsData(array $var = null)
    {
        //return $var;
        $sql = "SELECT * FROM mod_candire_campaigns";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_cnd_cp_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_cnd_cp_name"];
                $return[$i]["description"] = $row["mod_cnd_cp_description"];
                $return[$i]["tags"] = $row["mod_cnd_cp_tags"];
                $return[$i]["typeCustomer"] = $row["mod_cnd_cp_type_customer"];
                $return[$i]["code"] = $row["mod_cnd_cp_code"];
                $return[$i]["cepId"] = $row["mod_cnd_cp_cpe_id"];
                $return[$i]["cenId"] = $row["mod_cnd_cp_cen_id"];
                $return[$i]["ageInit"] = $row["mod_cnd_cp_target_init_age"];
                $return[$i]["ageEnd"] = $row["mod_cnd_cp_target_init_age"];
                $return[$i]["gender"] = $row["mod_cnd_cp_target_gender"];
                $return[$i]["dateInit"] = $row["mod_cnd_cp_init_date"];
                $return[$i]["dateEnd"] = $row["mod_cnd_cp_end_date"];
                $return[$i]["registerDate"] = $row["mod_cnd_cp_register_date"];
                $return[$i]["json"] = $row["mod_cnd_cp_json"];
                $return[$i]["state"] = $row["mod_cnd_cp_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }
}