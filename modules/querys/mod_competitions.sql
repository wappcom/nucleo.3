CREATE TABLE `mod_competitions` (
  `mod_cmp_id` int NOT NULL AUTO_INCREMENT,
  `mod_cmp_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_cmp_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cmp_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_cmp_date_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `mod_cmp_date_end` datetime DEFAULT NULL,
  `mod_cmp_ent_id` int DEFAULT NULL,
  `mod_cmp_status` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_cmp_id`) USING BTREE,
  INDEX (`mod_cmp_name`),
  INDEX (`mod_cmp_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;