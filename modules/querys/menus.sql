DROP TABLE IF EXISTS menus;
DROP TABLE IF EXISTS menus_items;

CREATE TABLE `menus` (
  `menu_id` int(11) NOT NULL,
  `menu_name` varchar(255) NOT NULL,
  `menu_description` tinytext,
  `menu_ent_id` int(11) DEFAULT '1',
  `menu_state` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`menu_id`);

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menus`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;


CREATE TABLE `menus_items` (
  `menu_item_id` int(11) NOT NULL,
  `menu_item_name` varchar(255) NOT NULL,
  `menu_item_description` tinytext,
  `menu_item_menu_id` int(11) DEFAULT NULL,
  `menu_item_cat_id` int(11) DEFAULT NULL,
  `menu_item_cat_active` int(11) DEFAULT '0',
  `menu_item_pathurl` varchar(400) DEFAULT NULL,
  `menu_item_target` varchar(12) NOT NULL DEFAULT '_self',
  `menu_item_parent_id` int(11) DEFAULT NULL,
  `menu_item_icon` int(11) DEFAULT NULL,
  `menu_item_img` int(11) DEFAULT NULL,
  `menu_item_order` int(11) DEFAULT NULL,
  `menu_item_state` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Indices de la tabla `menu_items`
--
ALTER TABLE `menus_items`
  ADD PRIMARY KEY (`menu_items_id`);

--
-- AUTO_INCREMENT de la tabla `menu_items`
--
ALTER TABLE `menus_items`
  MODIFY `menu_items_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;