-- Eliminar tablas nuevas (si es necesario)

DROP TABLE IF EXISTS `mod_ads`;
DROP TABLE IF EXISTS `mod_ads_campaigns`;
DROP TABLE IF EXISTS `mod_ads_performance_metrics`;
DROP TABLE IF EXISTS `mod_ads_locations`;
DROP TABLE IF EXISTS `mod_ads_devices`;
DROP TABLE IF EXISTS `mod_ads_types`;
DROP TABLE IF EXISTS `mod_ads_display`;

CREATE TABLE `mod_ads` (
    `mod_ads_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único del anuncio',
    `mod_ads_name` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Nombre del anuncio',
    `mod_ads_details` TEXT NULL DEFAULT NULL COMMENT 'Detalles o descripción del anuncio',
    `mod_ads_code` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Código único del anuncio',
    `mod_ads_file_desktop` INT NULL DEFAULT NULL COMMENT 'ID del archivo multimedia asociado (relacionado con mod_ads_media)',
    `mod_ads_file_tablet` INT NULL DEFAULT NULL COMMENT 'ID del archivo multimedia asociado (relacionado con mod_ads_media)',
    `mod_ads_file_mobile` INT NULL DEFAULT NULL COMMENT 'ID del archivo multimedia asociado (relacionado con mod_ads_media)',
    `mod_ads_position` INT NULL DEFAULT NULL COMMENT 'Posición del anuncio en la página',
    `mod_ads_json` TEXT NULL DEFAULT NULL COMMENT 'JSON con los datos del anuncio',
    `mod_ads_user_id` INT NULL DEFAULT NULL COMMENT 'ID del usuario que creó el anuncio',
    `mod_ads_state` INT NOT NULL DEFAULT '0' COMMENT 'Estado del anuncio (0: inactivo, 1: activo)',
    PRIMARY KEY (`mod_ads_id`)
) ENGINE = MyISAM DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- Tabla de Campañas (mod_campaigns)
CREATE TABLE `mod_ads_campaigns` (
    `mod_ads_campaign_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único de la campaña',
    `mod_ads_campaign_cen_id` INT NOT NULL COMMENT 'Id de la empresa que crea la campaña (relacionado con mod_customers_enterprises)',
    `mod_ads_campaign_name` VARCHAR(255) NOT NULL COMMENT 'Nombre de la campaña',
    `mod_ads_campaign_objective` VARCHAR(255) NOT NULL COMMENT 'Objetivo de la campaña (ejemplo: leads, ventas)',
    `mod_ads_campaign_status` VARCHAR(50) DEFAULT 'active' COMMENT 'Estado de la campaña (active, paused, completed)',
    `mod_ads_campaign_start_date` DATE NOT NULL COMMENT 'Fecha de inicio de la campaña',
    `mod_ads_campaign_end_date` DATE DEFAULT NULL COMMENT 'Fecha de finalización de la campaña',
    `mod_ads_campaign_budget_id` INT NOT NULL COMMENT 'Id del presupuesto asociado',
    `mod_ads_campaign_ent_id` INT NOT NULL COMMENT 'Id de la entidad asociada (relacionado con mod_customers_enterprises)',
    `mod_ads_campaign_state` INT NOT NULL DEFAULT '1' COMMENT 'Estado del registro (1: activo, 0: inactivo)',
    `mod_ads_campaign_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación de la campaña',
    `mod_ads_campaign_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización de la campaña',
    PRIMARY KEY (`mod_ads_campaign_id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- Tabla de Métricas de Rendimiento (mod_ads_performance_metrics)
CREATE TABLE `mod_ads_performance_metrics` (
    `mod_ads_metric_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único de la métrica',
    `mod_ads_metric_ads_id` INT NOT NULL COMMENT 'Id del anuncio asociado (relacionado con mod_ads)',
    `mod_ads_metric_site_id` INT NOT NULL COMMENT 'Id del sitio donde se mostró el anuncio (relacionado con mod_ads_sites)',
    `mod_ads_metric_clicks` INT DEFAULT 0 COMMENT 'Número de clics',
    `mod_ads_metric_impressions` INT DEFAULT 0 COMMENT 'Número de impresiones',
    `mod_ads_metric_conversions` INT DEFAULT 0 COMMENT 'Número de conversiones',
    `mod_ads_metric_cost_per_click` DECIMAL(10, 2) DEFAULT NULL COMMENT 'Costo por clic',
    `mod_ads_metric_cost_per_conversion` DECIMAL(10, 2) DEFAULT NULL COMMENT 'Costo por conversión',
    `mod_ads_metric_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación de la métrica',
    `mod_ads_metric_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización de la métrica',
    PRIMARY KEY (`mod_ads_metric_id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;



CREATE TABLE `mod_ads_display` (
    `mod_ads_dsp_id` INT NOT NULL AUTO_INCREMENT, -- Identificador único para la posición del anuncio
    `mod_ads_dsp_name` VARCHAR(255) NULL DEFAULT NULL, -- Nombre de la posición del anuncio
    `mod_ads_dsp_description` TEXT NULL DEFAULT NULL, -- Descripción de la posición del anuncio
    `mod_ads_dsp_site_id` INT NULL DEFAULT NULL, -- ID del sitio asociado con la posición del anuncio
    `mod_ads_dsp_pub_id` INT NULL DEFAULT NULL, -- ID del publicador asociado con la posición del anuncio
    `mod_ads_dsp_type` INT NULL DEFAULT NULL COMMENT 'Tipo de ubicación (1: encabezado, 2: pie de página, 3: lateral)', -- Tipo de ubicación
    `mod_ads_dsp_state` INT NOT NULL DEFAULT '0', -- Estado de la posición del anuncio (0: inactivo, 1: activo)
    PRIMARY KEY (`mod_ads_dsp_id`) -- Clave primaria para la tabla
) ENGINE = MyISAM DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci; -- Motor de almacenamiento y conjunto de caracteres de la tabla

-- Tabla de Ubicaciones (mod_ads_locations)
CREATE TABLE `mod_ads_locations` (
    `mod_ads_location_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único de la ubicación',
    `mod_ads_location_name` VARCHAR(255) NOT NULL COMMENT 'Nombre de la ubicación',
    `mod_ads_location_country` VARCHAR(255) NOT NULL COMMENT 'País de la ubicación',
    `mod_ads_location_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación de la ubicación',
    `mod_ads_location_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización de la ubicación',
    PRIMARY KEY (`mod_ads_location_id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- Tabla de Dispositivos (mod_ads_devices)
CREATE TABLE `mod_ads_devices` (
    `mod_ads_device_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único del dispositivo',
    `mod_ads_device_name` VARCHAR(255) NOT NULL COMMENT 'Nombre del dispositivo (ejemplo: móvil, desktop)',
    `mod_ads_device_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación del dispositivo',
    `mod_ads_device_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización del dispositivo',
    PRIMARY KEY (`mod_ads_device_id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- Tabla de Tipos de Anuncios (mod_ads_types)
CREATE TABLE `mod_ads_types` (
    `mod_ads_type_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único del tipo de anuncio',
    `mod_ads_type_name` VARCHAR(255) NOT NULL COMMENT 'Nombre del tipo de anuncio (ejemplo: display, video)',
    `mod_ads_type_description` TEXT DEFAULT NULL COMMENT 'Descripción del tipo de anuncio',
    `mod_ads_type_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación del tipo de anuncio',
    `mod_ads_type_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización del tipo de anuncio',
    PRIMARY KEY (`mod_ads_type_id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;