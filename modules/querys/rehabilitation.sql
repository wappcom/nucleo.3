DROP TABLE IF EXISTS mod_rehabilitation_user;


CREATE TABLE `mod_rehabilitation_user` (
  `mod_rhb_id` int(11) NOT NULL,
  `mod_rhb_name` varchar(255) NOT NULL,
  `mod_rhb_lastname` varchar(255) NOT NULL,
  `mod_rhb_acu_id` int(11) DEFAULT NULL,
  `mod_rhb_fds_id` int(11) DEFAULT NULL,
  `mod_rhb_birthday` date DEFAULT NULL,
  `mod_rhb_civil_state` int(11) DEFAULT NULL COMMENT '1.Soltero/a 2.Casado/a 3.Divorciado 4.Concuvinato 5. Unión Libre  6. Otro',
  `mod_rhb_dependents` int(11) NOT NULL,
  `mod_rhb_telf_fax` varchar(255) DEFAULT NULL,
  `mod_rhb_celular` varchar(250) DEFAULT NULL,
  `mod_rhb_address` varchar(550) DEFAULT NULL,
  `mod_rhb_city` varchar(400) DEFAULT NULL,
  `mod_rhb_academic_level` varchar(255) DEFAULT NULL,
  `mod_rhb_age_first_use` int(11) DEFAULT NULL,
  `mod_rhb_num_internment` int(11) DEFAULT NULL,
  `mod_rhb_labor_status` int(11) DEFAULT NULL COMMENT '1. Dependiente 2.Independiente. 3. Jubilado/rentista. 4.Estudiante 5. Desempleado',
  `mod_rhb_last_work` varchar(255) DEFAULT NULL,
  `mod_rhb_nationality` varchar(550) DEFAULT NULL,
  `mod_rhb_profession` varchar(50) DEFAULT NULL,
  `mod_rhb_primary_consumption` varchar(150) DEFAULT NULL,
  `mod_rhb_reason_addiction` varchar(255) DEFAULT NULL,
  `mod_rhb_internment_date` date DEFAULT NULL,
  `mod_rhb_register_date` date DEFAULT NULL,
  `mod_rhb_gradation_date` date DEFAULT NULL,
  `mod_rhb_leaving_date` date DEFAULT NULL,
  `mod_rhb_observations` tinytext,
  `mod_rhb_user_id` int(10) DEFAULT NULL,
  `mod_rhb_reason_leaving` varchar(450) DEFAULT NULL,
  `mod_rhb_tutor_name` varchar(50) DEFAULT NULL,
  `mod_rhb_tutor_lastname` varchar(250) DEFAULT NULL,
  `mod_rhb_tutor_gender` varchar(50) DEFAULT NULL,
  `mod_rhb_tutor_ci` int(10) DEFAULT NULL,
  `mod_rhb_tutor_ext` varchar(2) DEFAULT NULL,
  `mod_rhb_tutor_birthday` date DEFAULT NULL,
  `mod_rhb_tutor_nationality` varchar(50) DEFAULT NULL,
  `mod_rhb_tutor_telf_fax` varchar(50) DEFAULT NULL,
  `mod_rhb_tutor_cellphone` int(11) DEFAULT NULL,
  `mod_rhb_tutor_labor_status` int(11) DEFAULT NULL,
  `mod_rhb_tutor_company_work` varchar(50) DEFAULT NULL,
  `mod_rhb_tutor_address_work` varchar(50) DEFAULT NULL,
  `mod_rhb_tutor_work_area` varchar(50) DEFAULT NULL,
  `mod_rhb_tutor_profession` varchar(50) DEFAULT NULL,
  `mod_rhb_tutor_relationship` varchar(50) DEFAULT NULL,
  `mod_rhb_state` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


ALTER TABLE `mod_rehabilitation_user`
  ADD PRIMARY KEY (`mod_rhb_id`) USING BTREE;

ALTER TABLE `mod_rehabilitation_user`
  MODIFY `mod_rhb_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;