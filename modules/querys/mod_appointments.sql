DROP TABLE IF EXISTS mod_appointments;
DROP TABLE IF EXISTS mod_appointments_calendars;
DROP TABLE IF EXISTS mod_appointments_reviews;
DROP TABLE IF EXISTS mod_appointments_schedules;
DROP TABLE IF EXISTS mod_appointments_payments;
DROP TABLE IF EXISTS mod_appointments_options;
DROP TABLE IF EXISTS mod_appointments_calendars_blocked;
DROP TABLE IF EXISTS mod_appointments_types;
DROP TABLE IF EXISTS mod_appointments_exceptions;
DROP TABLE IF EXISTS mod_providers;
DROP TABLE IF EXISTS mod_services;
DROP TABLE IF EXISTS mod_services_roles;

DROP TABLE IF EXISTS mod_appointments_logs;
DROP TABLE IF EXISTS mod_invoices;

DROP TABLE IF EXISTS mod_persons;
DROP TABLE IF EXISTS mod_persons_types;


CREATE TABLE `mod_appointments_calendars` (
  `mod_apt_cal_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único de la cita',
  `mod_apt_cal_cpe_id` INT NOT NULL COMMENT 'Id customer persona (cliente)',
  `mod_apt_cal_person_id` INT NOT NULL COMMENT 'Id de la persona asociada a la cita',
  `mod_apt_cal_acu_id` INT NOT NULL COMMENT 'Id persona que se registra',
  `mod_apt_cal_provider_id` INT NOT NULL COMMENT 'Id proveedor (cuenta relacionada con el servicio)',
  `mod_apt_cal_srv_id` INT NOT NULL COMMENT 'Id del servicio relacionado',
  `mod_apt_cal_sch_id` INT NOT NULL COMMENT 'Id del horario asociado',
  `mod_apt_cal_date_start` datetime DEFAULT NULL,
  `mod_apt_cal_date_end` datetime DEFAULT NULL,
  `mod_apt_cal_reschedule` int DEFAULT NULL,
  `mod_apt_cal_reason_cancel` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_apt_cal_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_apt_cal_all_day` TINYINT(1) DEFAULT 0 COMMENT 'Indica si la cita es todo el día (0: No, 1: Sí)',
  `mod_apt_cal_place` VARCHAR(255) DEFAULT NULL COMMENT 'Lugar donde se llevará a cabo la cita',
  `mod_apt_cal_guests` TEXT DEFAULT NULL COMMENT 'Lista de invitados (almacenados como JSON)',
  `mod_apt_cal_status` VARCHAR(20) DEFAULT 'pending' COMMENT 'Estado de la cita (pending, confirmed, completed, cancelled)',
  `mod_apt_cal_type` VARCHAR(50) DEFAULT NULL COMMENT 'Tipo de cita (ejemplo: consulta, revisión, seguimiento)',
  `mod_apt_cal_ent_id` int DEFAULT NULL,
  `mod_apt_cal_user_id` int DEFAULT NULL,
  `mod_apt_cal_flags` VARCHAR(50) DEFAULT NULL COMMENT 'Banderas o indicadores especiales asociados a la cita',
  `mod_apt_cal_details` TEXT DEFAULT NULL COMMENT 'Comentarios o descripción adicional del evento',
  `mod_apt_cal_created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación de la cita',
  `mod_apt_cal_updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización de la cita',
  `mod_apt_cal_state` INT DEFAULT '0' COMMENT 'Estado lógico de la cita (0: inactivo, 1: activo)',
  PRIMARY KEY (`mod_apt_cal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



CREATE TABLE `mod_appointments_reviews` (
  `mod_apt_rev_id` int NOT NULL AUTO_INCREMENT COMMENT 'Id único de la reseña',
  `mod_apt_rev_apt_id` int NOT NULL COMMENT 'Id de la cita asociada',
  `mod_apt_rev_cpe_id` int NOT NULL COMMENT 'Id del cliente que realiza la reseña',
  `mod_apt_rev_provider_id` int NOT NULL COMMENT 'Id del proveedor asociado a la reseña',
  `mod_apt_rev_rating` int DEFAULT NULL COMMENT 'Calificación dada por el cliente (1-5)',
  `mod_apt_rev_comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT 'Comentario opcional del cliente',
  `mod_apt_rev_register_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de registro de la reseña',
  `mod_apt_rev_state` int DEFAULT '0' COMMENT 'Estado de la reseña (activo/inactivo)',
  PRIMARY KEY (`mod_apt_rev_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Tabla de reseñas y valoraciones de los servicios por parte de los clientes';


CREATE TABLE `mod_appointments_schedules` (
  `mod_apt_sch_id` int NOT NULL AUTO_INCREMENT,
  `mod_apt_sch_provider_id` int NOT NULL COMMENT 'Id del proveedor',
  `mod_apt_sch_user_id` int NOT NULL COMMENT 'Id del usuario',
  `mod_apt_sch_day` TINYINT(1) NOT NULL COMMENT 'Día de la semana (1: Lunes, 2: Martes, ..., 7: Domingo)',
  `mod_apt_sch_hour_start` time NOT NULL COMMENT 'Hora de inicio',
  `mod_apt_sch_hour_end` time NOT NULL COMMENT 'Hora de finalización',
  `mod_apt_sch_state` int DEFAULT '0' COMMENT 'Estado (0: Inactivo, 1: Activo)',
  `mod_apt_sch_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación',
  `mod_apt_sch_updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización',
  PRIMARY KEY (`mod_apt_sch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Disponibilidad de proveedores por día y hora';


CREATE TABLE `mod_appointments_exceptions` (
  `mod_apt_exc_id` int NOT NULL AUTO_INCREMENT,
  `mod_apt_exc_provider_id` int NOT NULL COMMENT 'Id del proveedor',
  `mod_apt_exc_user_id` int NOT NULL COMMENT 'Id del usuario',
  `mod_apt_exc_date` date NOT NULL COMMENT 'Fecha de la excepción',
  `mod_apt_exc_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Razón de la excepción',
  `mod_apt_exc_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación',
  `mod_apt_exc_updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización',
  PRIMARY KEY (`mod_apt_exc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Excepciones en la disponibilidad de los proveedores (vacaciones, días festivos, etc.)';

CREATE TABLE `mod_appointments_logs` (
  `mod_apt_log_id` int NOT NULL AUTO_INCREMENT,
  `mod_apt_log_apt_id` int NOT NULL COMMENT 'Id de la cita',
  `mod_apt_log_action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Acción realizada (crear, modificar, cancelar, etc.)',
  `mod_apt_log_changed_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha y hora del cambio',
  `mod_apt_log_changed_by` int NOT NULL COMMENT 'Id del usuario o administrador que realizó el cambio',
  PRIMARY KEY (`mod_apt_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Historial de cambios realizados en las citas';


CREATE TABLE `mod_appointments_payments` (
  `mod_apt_pay_id` int NOT NULL AUTO_INCREMENT COMMENT 'Id del pago',
  `mod_apt_pay_apt_id` int NOT NULL,
  `mod_apt_pay_amount` decimal(10,2) NOT NULL,
  `mod_apt_pay_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `mod_apt_pay_method` varchar(255) NOT NULL COMMENT 'Método de pago credit_card,paypal,stripe,efectivo,qr..',
  `mod_apt_pay_status` varchar(255) DEFAULT 'pending,failed,completed',
  `mod_apt_pay_state` int DEFAULT '0',
  PRIMARY KEY (`mod_apt_pay_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `mod_providers` (
  `mod_prov_id` int NOT NULL AUTO_INCREMENT,
  `mod_prov_code` varchar(255) NOT NULL,
  `mod_prov_user_id` int NOT NULL,
  `mod_prov_name` varchar(255) NOT NULL,
  `mod_prov_email` varchar(255) NOT NULL,
  `mod_prov_phone` varchar(255) NOT NULL,
  `mod_prov_address` varchar(255) NOT NULL,
  `mod_prov_rating` DECIMAL(3,2) DEFAULT NULL,
  `mod_prov_city` varchar(255) NOT NULL,
  `mod_prov_zip` varchar(255) NOT NULL,
  `mod_prov_country` varchar(255) NOT NULL,
  `mod_prov_website` varchar(255) NOT NULL,
  `mod_prov_logo` varchar(255) NOT NULL,
  `mod_prov_description` varchar(255) NOT NULL,
  `mod_prov_type` varchar(255) NOT NULL,
  `mod_prov_created` datetime NOT NULL,
  `mod_prov_modified` datetime NOT NULL,
  `mod_prov_deleted` datetime DEFAULT NULL COMMENT 'Fecha de eliminación (si aplica)',
  `mod_prov_state` int DEFAULT '0' COMMENT 'Estado (0: Inactivo, 1: Activo)',
  PRIMARY KEY (`mod_prov_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Tabla de proveedores';


 
CREATE TABLE `mod_services` (
  `mod_srv_id` int NOT NULL AUTO_INCREMENT,
  `mod_srv_name` varchar(255) NOT NULL,
  `mod_srv_description` varchar(255) NOT NULL,
  `mod_srv_active` int NOT NULL,
  `mod_srv_duration` varchar(255) NOT NULL,
  `mod_srv_type` varchar(255) NOT NULL,
  `mod_srv_price` DECIMAL(10,2) NOT NULL,
  `mod_srv_created` datetime NOT NULL,
  `mod_srv_modified` datetime NOT NULL,
  `mod_srv_deleted` datetime DEFAULT NULL,
  `mod_srv_ent_id` int NOT NULL,
  `mod_srv_state` int DEFAULT '0',
  PRIMARY KEY (`mod_srv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Tabla de servicios';

CREATE TABLE `mod_services_roles` (
  `mod_srv_id` int NOT NULL,
  `mod_srv_rol_id` int NOT NULL,
  `mod_srv_rol_ent_id` int NOT NULL,
  `mod_srv_rol_state` int DEFAULT '0',
  PRIMARY KEY (`mod_srv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Tabla de servicios roles';





CREATE TABLE `mod_invoices` (
  `mod_inv_id` int NOT NULL AUTO_INCREMENT,
  `mod_inv_px_id` int NOT NULL COMMENT 'Id del pago relacionado',
  `mod_inv_date` timestamp DEFAULT CURRENT_TIMESTAMP,
  `mod_inv_total` decimal(10,2) NOT NULL,
  `mod_inv_status` varchar(255) NOT NULL DEFAULT 'pending',
  `mod_inv_register_date` timestamp DEFAULT CURRENT_TIMESTAMP,
  `mod_inv_user_id` int NOT NULL,
  `mod_inv_ent_id` int NOT NULL,
  `mod_inv_custom_data` json DEFAULT NULL,
  `mod_inv_class` varchar(255) DEFAULT NULL,
  `mod_inv_custom_id` varchar(255) DEFAULT NULL,
  `mod_inv_state` int DEFAULT '0',
  PRIMARY KEY (`mod_inv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `mod_persons` (
  `mod_person_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único de la persona',
  `mod_person_name` VARCHAR(255) NOT NULL COMMENT 'Nombre de la persona',
  `mod_person_lastname_father` VARCHAR(255) NOT NULL COMMENT 'Apellido paterno de la persona',
  `mod_person_lastname_mother` VARCHAR(255) DEFAULT NULL COMMENT 'Apellido materno de la persona',
  `mod_person_email` VARCHAR(255) DEFAULT NULL COMMENT 'Correo electrónico de la persona',
  `mod_person_phone` VARCHAR(20) DEFAULT NULL COMMENT 'Teléfono de contacto',
  `mod_person_ent_id` INT NOT NULL COMMENT 'Id de la entidad asociada',
  `mod_person_type` VARCHAR(20) DEFAULT NULL  COMMENT 'Id del tipo de persona',
  `mod_person_state` INT DEFAULT 0 COMMENT 'Estado lógico de la persona (0: inactivo, 1: activo)',
  `mod_person_comments` TEXT DEFAULT NULL COMMENT 'Campo para comentarios adicionales sobre la persona',
  `mod_person_metadata` JSON DEFAULT NULL COMMENT 'Campo para datos adicionales en formato JSON',
  `mod_person_custom_class` VARCHAR(255) DEFAULT NULL COMMENT 'Clase CSS personalizada asociada a la persona',
  `mod_person_custom_id` VARCHAR(255) DEFAULT NULL COMMENT 'ID personalizado asociado a la persona',
  `mod_person_created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación',
  `mod_person_updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización',
  PRIMARY KEY (`mod_person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `mod_persons_types` (
  `mod_person_type_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único del tipo de persona',
  `mod_person_type_code` VARCHAR(50) NOT NULL COMMENT 'Código único para identificar el tipo',
  `mod_person_type_name` VARCHAR(100) NOT NULL COMMENT 'Nombre descriptivo del tipo',
  `mod_person_type_description` TEXT DEFAULT NULL COMMENT 'Descripción detallada del tipo',
  `mod_person_type_metadata` JSON DEFAULT NULL COMMENT 'Datos adicionales en formato JSON',
  `mod_person_type_state` INT DEFAULT 1 COMMENT 'Estado lógico del tipo (0: inactivo, 1: activo)',
  PRIMARY KEY (`mod_person_type_id`),
  UNIQUE KEY `mod_person_type_code_UNIQUE` (`mod_person_type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Insertar tipos base
INSERT INTO `mod_persons_types` (
  `mod_person_type_code`, 
  `mod_person_type_name`, 
  `mod_person_type_description`
) VALUES
('LEAD', 'Lead', 'Posible cliente potencial'),
('CUSTOMER', 'Cliente', 'Cliente activo'),
('PROVIDER', 'Proveedor', 'Proveedor de servicios o productos');

CREATE TABLE `mod_appointments_options` (
  `mod_apt_opt_id` int NOT NULL AUTO_INCREMENT,
  `mod_apt_opt_name` varchar(255) NOT NULL,
  `mod_apt_opt_value` varchar(255) NOT NULL,
  `mod_apt_opt_autoload` varchar(5) DEFAULT 'yes',
  `mod_apt_opt_state` int DEFAULT '0',
  PRIMARY KEY (`mod_apt_opt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `mod_appointments_calendars_blocked` (
  `mod_apt_cb_id` int NOT NULL AUTO_INCREMENT,
  `mod_apt_cb_date_start` datetime NOT NULL,
  `mod_apt_cb_date_end` datetime DEFAULT NULL,
  `mod_apt_cb_ent_id` int DEFAULT NULL,
  `mod_apt_cb_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_apt_cb_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


/**
 * Almacena los diferentes tipos de citas y sus configuraciones
 * Se utiliza para categorizar y gestionar varios tipos de citas en el sistema
 */
CREATE TABLE `mod_appointments_types` (
  `mod_apt_type_id` int NOT NULL COMMENT 'ID único para el tipo de cita',
  `mod_apt_type_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Nombre del tipo de cita',
  `mod_apt_type_summary` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Descripción breve del tipo de cita',
  `mod_apt_type_kind` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Categoría/clasificación del tipo de cita',
  `mod_apt_type_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Ruta asociada a este tipo de cita',
  `mod_apt_type_bg_color` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Código de color de fondo para la representación visual de este tipo',
  `mod_apt_type_text_color` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Código de color del texto para la representación visual de este tipo',
  `mod_apt_type_border_color` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Código de color del borde para la representación visual de este tipo',
  `mod_apt_type_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT 'Configuración adicional en formato JSON',
  `mod_apt_type_ent_id` int DEFAULT NULL COMMENT 'ID de entidad asociada a este tipo de cita',
  `mod_apt_type_state` int DEFAULT '0' COMMENT 'Estado del tipo de cita (0: inactivo, 1: activo)',
  PRIMARY KEY (`mod_apt_type_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;