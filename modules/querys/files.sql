DROP TABLE IF EXISTS files;


CREATE TABLE `files` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_description` varchar(255) DEFAULT NULL,
  `file_pathurl` varchar(1000) DEFAULT NULL,
  `file_embed` varchar(500) DEFAULT NULL,
  `file_btn_title` varchar(255) DEFAULT NULL,
  `file_ext` varchar(11) DEFAULT NULL,
  `file_alt` varchar(250) DEFAULT NULL,
  `file_title` varchar(255) DEFAULT NULL,
  `file_filename_md5` binary(255) DEFAULT NULL,
  `file_datatime` datetime DEFAULT NULL,
  `file_state` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



ALTER TABLE `files`
  ADD PRIMARY KEY (`file_id`) USING BTREE;

--
-- AUTO_INCREMENT de la tabla `files`
--
ALTER TABLE `files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
COMMIT;
