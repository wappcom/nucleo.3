CREATE TABLE `mod_accounts_users` (
  `mod_acu_id` int(11) NOT NULL,
  `mod_acu_name` varchar(200) DEFAULT NULL,
  `mod_acu_fathers_lastname` varchar(255) NOT NULL,
  `mod_acu_mothers_lastname` varchar(255) NOT NULL,
  `mod_acu_age_range` varchar(11) DEFAULT NULL,
  `mod_acu_email` varchar(255) NOT NULL,
  `mod_acu_birthday_date` date DEFAULT NULL,
  `mod_acu_password` varchar(120) DEFAULT NULL,
  `mod_acu_imagen` varchar(400) DEFAULT NULL,
  `mod_acu_level` int(11) DEFAULT NULL,
  `mod_acu_gender` varchar(255) DEFAULT NULL COMMENT 'no-gender,male,female',
  `mod_acu_ci` int(10) DEFAULT NULL,
  `mod_acu_ci_ext` varchar(2) DEFAULT NULL,
  `mod_acu_celular` int(11) DEFAULT NULL,
  `mod_acu_record_date` datetime DEFAULT NULL,
  `mod_acu_type` int(11) DEFAULT NULL,
  `mod_acu_timezone` varchar(120) DEFAULT NULL,
  `mod_acu_locale` varchar(120) DEFAULT NULL,
  `mod_acu_token` varchar(255) DEFAULT NULL,
  `mod_acu_code` varchar(255) NOT NULL,
  `mod_acu_referred` varchar(255) DEFAULT NULL,
  `mod_acu_ent_id` int(11) DEFAULT NULL,
  `mod_acu_state` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


--
-- Indices de la tabla `mod_accounts_users`
--
ALTER TABLE `mod_accounts_users`
  ADD PRIMARY KEY (`mod_acu_id`) USING BTREE;
--
-- AUTO_INCREMENT de la tabla `mod_accounts_users`
--
ALTER TABLE `mod_accounts_users`
  MODIFY `mod_acu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;
