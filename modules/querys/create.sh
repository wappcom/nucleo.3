#!/bin/bash

# Nombre del proyecto
proyecto="services"
proyectoCapitalizado="$(tr '[:lower:]' '[:upper:]' <<< ${proyecto:0:1})${proyecto:1}"
# Directorio base del proyecto
directorio_base="/home/wappcom/www/nucleo/nucleo.3/modules/"

# Crear directorio base con permisos para tu usuario
mkdir -p "$directorio_base/$proyecto"
chown -R $(whoami):$(id -g -n $(whoami)) "$directorio_base/$proyecto"
cd "$directorio_base/$proyecto" || exit

# Crear subdirectorios con permisos para tu usuario
directorios=("assets" "assets/css" "components" "components/renders" "controllers" "controllers/apis" "controllers/apis/v1" "models" "models/class" "views")
for dir in "${directorios[@]}"; do
    mkdir -p "$dir"
    chown -R $(whoami):$(id -g -n $(whoami)) "$dir"
done

# Crear archivos de ejemplo con permisos para tu usuario
touch "assets/css/$proyecto.less"
touch "components/$proyecto.js"
touch "components/renders/render$proyectoCapitalizado.js"
touch "controllers/apis/v1/$proyecto.php"
touch "models/class/$proyecto.class.php"
touch "views/index.html"
touch "index.php"
chown -R $(whoami):$(id -g -n $(whoami)) *

# Mensaje de finalización
echo "Proyecto $proyecto creado en $directorio_base con permisos de tu usuario"
