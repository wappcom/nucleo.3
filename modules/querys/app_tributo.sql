

DROP TABLE IF EXISTS `mod_tributes_contribuyente_form`;
CREATE TABLE `mod_tributes_contribuyente_form` (
  `mod_tb_cf_typc_id` int NOT NULL,
  `mod_tb_cf_type_form_id` int NOT NULL,
  `mod_tb_cf_presentacion` varchar(445) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_tributes_contribuyente_form
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_tributes_facturas
-- ----------------------------
DROP TABLE IF EXISTS `mod_tributes_facturas`;
CREATE TABLE `mod_tributes_facturas` (
  `mod_tb_fac_id` int NOT NULL AUTO_INCREMENT,
  `mod_tb_fac_nit` int NOT NULL,
  `mod_tb_fac_img` int DEFAULT NULL,
  `mod_tb_fac_type` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'manual',
  `mod_tb_fac_periodo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fac_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_tb_fac_id`,`mod_tb_fac_nit`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_tributes_facturas
-- ----------------------------
BEGIN;
INSERT INTO `mod_tributes_facturas` VALUES (5, 2147483647, 57, 'trimestral', '1', 1);
INSERT INTO `mod_tributes_facturas` VALUES (6, 2147483647, 58, 'manual', '1', 1);
INSERT INTO `mod_tributes_facturas` VALUES (7, 0, 59, 'manual', '1', 1);
INSERT INTO `mod_tributes_facturas` VALUES (8, 0, 60, 'manual', '1', 1);
COMMIT;

-- ----------------------------
-- Table structure for mod_tributes_options
-- ----------------------------
DROP TABLE IF EXISTS `mod_tributes_options`;
CREATE TABLE `mod_tributes_options` (
  `mod_tb_op_id` int NOT NULL AUTO_INCREMENT,
  `mod_tb_op_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_tb_op_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_tb_op_autoload` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'yes',
  `mod_tb_op_date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `mod_tb_op_date_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`mod_tb_op_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_tributes_options
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_tributes_periods
-- ----------------------------
DROP TABLE IF EXISTS `mod_tributes_periods`;
CREATE TABLE `mod_tributes_periods` (
  `mod_tb_pr_id` int NOT NULL AUTO_INCREMENT COMMENT 'ej:202409 se refiere al periodo 2024-09 año/mes siempre seran 6 digitos', 
  `mod_tb_pr_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_pr_year_fiscal` YEAR  NULL COMMENT 'ej:2024',
  `mod_tb_pr_init_monthly` DATE DEFAULT NULL COMMENT 'ej:2024-01-01',
  `mod_tb_pr_end_monthly` DATE DEFAULT NULL COMMENT 'ej:2024-12-31',
  `mod_tb_pr_init_quarterly` DATE DEFAULT NULL COMMENT 'ej:2024-01-01',
  `mod_tb_pr_end_quarterly` DATE DEFAULT NULL COMMENT 'ej:2024-12-31',
  `mod_tb_pr_type` VARCHAR(25) DEFAULT NULL COMMENT 'ej: M M,T.. M: mensual, M,T: mensual y trimestral',
  `mod_tb_pr_active_user` INT DEFAULT NULL COMMENT 'si este periodo esta activo para el usuario manejado por el cron de sistema para cerrar periodos',
  `mod_tb_pr_active_operator` INT DEFAULT NULL COMMENT 'si este periodo esta activo para el operador manejado por el cron de sistema para cerrar periodos',
  `mod_tb_pr_message_register` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'es el campo texto para gestionar mensajes en el registro, es html hecho en markdown',
  `mod_tb_pr_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_tb_pr_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_tb_pr_status` int DEFAULT NULL,
  PRIMARY KEY (`mod_tb_pr_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=202412 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_tributes_periods
-- ----------------------------
BEGIN;
INSERT INTO `mod_tributes_periods` VALUES (202412, 'Periodo 12-2024', '2024',"2024-12-01","2024-12-31","2024-10-01","2024-12-31","M,T",1,1,'- Facturas Mensuales desde el 2024/Dic/01 al 2024/Dic/31
- Facturas Trimestrales desde el 2024/Oct/01 al 2024/Dic/31',NULL,NULL,1);
INSERT INTO `mod_tributes_periods` VALUES (202501, 'Periodo 01-2025', '2025',"2025-01-01","2024-01-31","2025-01-01","2025-03-31","M",0,0,'- Facturas Mensuales desde el 2025/Ene/01 al 2025/Ene/31',NULL,NULL,0);
INSERT INTO `mod_tributes_periods` VALUES (202502, 'Periodo 02-2025', '2025',"2025-02-01","2024-02-28","2025-01-01","2025-03-31","M",0,0,'- Facturas Mensuales desde el 2025/Feb/01 al 2025/Feb/28',NULL,NULL,0);
INSERT INTO `mod_tributes_periods` VALUES (202503, 'Periodo 03-2025', '2025',"2025-03-01","2024-03-31","2025-01-01","2025-03-31","M,T",0,0,'- Facturas Mensuales desde el 2025/Mar/01 al 2025/Mar/31 - Facturas Trimestrales desde el 2025/Ene/01 al 2025/Mar/31',NULL,NULL,0);
INSERT INTO `mod_tributes_periods` VALUES (202504, 'Periodo 04-2025', '2025',"2025-04-01","2024-04-30","2025-04-01","2025-06-30","M",0,0,'- Facturas Mensuales desde el 2025/Abr/01 al 2025/Abr/30',NULL,NULL,0);
INSERT INTO `mod_tributes_periods` VALUES (202505, 'Periodo 05-2025', '2025',"2025-05-01","2024-05-31","2025-04-01","2025-06-30","M",0,0,'- Facturas Mensuales desde el 2025/May/01 al 2025/May/31',NULL,NULL,0);
INSERT INTO `mod_tributes_periods` VALUES (202506, 'Periodo 06-2025', '2025',"2025-06-01","2024-06-30","2025-04-01","2025-06-30","M,T",0,0,'- Facturas Mensuales desde el 2025/Jun/01 al 2025/Jun/30 - Facturas Trimestrales desde el 2025/Abr/01 al 2025/Jun/30',NULL,NULL,0);
INSERT INTO `mod_tributes_periods` VALUES (202507, 'Periodo 07-2025', '2025',"2025-07-01","2024-07-31","2025-07-01","2025-09-30","M",0,0,'- Facturas Mensuales desde el 2025/Jul/01 al 2025/Jul/31',NULL,NULL,0);
INSERT INTO `mod_tributes_periods` VALUES (202508, 'Periodo 08-2025', '2025',"2025-08-01","2024-08-31","2025-07-01","2025-09-30","M",0,0,'- Facturas Mensuales desde el 2025/Ago/01 al 2025/Ago/31',NULL,NULL,0);
INSERT INTO `mod_tributes_periods` VALUES (202509, 'Periodo 09-2025', '2025',"2025-09-01","2024-09-30","2025-07-01","2025-09-30","M,T",0,0,'- Facturas Mensuales desde el 2025/Sep/01 al 2025/Sep/30 - Facturas Trimestrales desde el 2025/Jul/01 al 2025/Sep/30',NULL,NULL,0);
INSERT INTO `mod_tributes_periods` VALUES (202510, 'Periodo 10-2025', '2025',"2025-10-01","2024-10-31","2025-10-01","2025-12-31","M",0,0,'- Facturas Mensuales desde el 2025/Oct/01 al 2025/Oct/31',NULL,NULL,0);
INSERT INTO `mod_tributes_periods` VALUES (202511, 'Periodo 11-2025', '2025',"2025-11-01","2024-11-30","2025-10-01","2025-12-31","M",0,0,'- Facturas Mensuales desde el 2025/Nov/01 al 2025/Nov/30',NULL,NULL,0);
INSERT INTO `mod_tributes_periods` VALUES (202512, 'Periodo 12-2025', '2025',"2025-12-01","2024-12-31","2025-10-01","2025-12-31","M,T",0,0,'- Facturas Mensuales desde el 2025/Dic/01 al 2025/Dic/31 - Facturas Trimestrales desde el 2025/Oct/01 al 2025/Dic/31',NULL,NULL,0);

COMMIT;

-- ----------------------------
-- Table structure for mod_tributes_plans_periods
-- ----------------------------
DROP TABLE IF EXISTS `mod_tributes_plans_periods`;
-- Esta tabla almacena los períodos de los planes de tributos
CREATE TABLE `mod_tributes_plans_periods` (
  `mod_tb_plpr_acu_id` int NOT NULL COMMENT 'id account',
  `mod_tb_plpr_cep_id` int NOT NULL COMMENT 'id customer_person',
  `mod_tb_plpr_pl_id` int NOT NULL COMMENT 'id plan',
  `mod_tb_plpr_pr_id` int NOT NULL COMMENT 'id period',
  `mod_tb_plpr_status` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_tributes_plans_periods
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_tributes_relations_periods
-- ----------------------------
DROP TABLE IF EXISTS `mod_tributes_relations_periods`;
CREATE TABLE `mod_tributes_relations_periods` (
  `mod_tb_rpd_id` int NOT NULL AUTO_INCREMENT,
  `mod_tb_rpd_pr_id` int DEFAULT NULL,
  `mod_tb_rpd_group_nit` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'g1,g2',
  `mod_tb_rpd_date_max_reception` date DEFAULT NULL,
  `mod_tb_rpd_date_max_preliquidation` date DEFAULT NULL,
  `mod_tb_rpd_date_ok_preliquidation` date DEFAULT NULL,
  `mod_tb_rpd_date_declaration` date DEFAULT NULL,
  PRIMARY KEY (`mod_tb_rpd_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_tributes_relations_periods
-- ----------------------------
BEGIN;
INSERT INTO `mod_tributes_relations_periods` VALUES (1, 202409, 'G1', '2024-10-05', '2024-10-09', '2024-10-11', '2024-10-12');
INSERT INTO `mod_tributes_relations_periods` VALUES (2, 202409, 'G2', '2024-10-05', '2024-10-10', '2024-10-14', '2024-10-17');
INSERT INTO `mod_tributes_relations_periods` VALUES (3, 202410, 'G1', '2024-11-05', '2024-11-09', '2024-11-11', '2024-11-12');
INSERT INTO `mod_tributes_relations_periods` VALUES (4, 202410, 'G2', '2024-11-05', '2024-11-10', '2024-11-14', '2024-11-17');
INSERT INTO `mod_tributes_relations_periods` VALUES (5, 202411, 'G1', '2024-12-05', '2024-12-09', '2024-12-11', '2024-12-12');
INSERT INTO `mod_tributes_relations_periods` VALUES (6, 202411, 'G2', '2024-12-05', '2024-12-10', '2024-12-14', '2024-12-17');
INSERT INTO `mod_tributes_relations_periods` VALUES (7, 202412, 'G1', '2025-01-05', '2025-01-09', '2025-01-11', '2025-01-12');
INSERT INTO `mod_tributes_relations_periods` VALUES (8, 202409, 'G2', '2025-01-05', '2025-01-10', '2025-01-14', '2025-01-17');
COMMIT;

-- ----------------------------
-- Table structure for mod_tributos_facturas_filter
-- ----------------------------
DROP TABLE IF EXISTS `mod_tributos_facturas_filter`;
CREATE TABLE `mod_tributos_facturas_filter` (
  `mod_tb_fcf_id` int NOT NULL AUTO_INCREMENT,
  `mod_tb_fcf_nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_fac_id` int DEFAULT NULL,
  `mod_tb_fcf_fac_nit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_fac_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_fac_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_fac_periodo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_fac_state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_nitProveedor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_razonSocialProveedor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_codigoAutorizacion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_numeroFactura` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_numeroDuiDim` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_fechaFacturaDuiDim` date DEFAULT NULL,
  `mod_tb_fcf_importeTotalCompra` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_importeIce` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_importeIehd` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_importeIpj` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_tasas` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_otroNoSujetoACreditoFiscal` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_importesExentos` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_importeComprasGravadasATasaCero` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_descuentos` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_importeGifCard` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_user_id` int DEFAULT NULL,
  `mod_tb_fcf_date_register` date DEFAULT NULL,
  `mod_tb_fcf_state` int DEFAULT NULL,
  PRIMARY KEY (`mod_tb_fcf_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_tributos_facturas_filter
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_types_contribuyentes
-- ----------------------------
DROP TABLE IF EXISTS `mod_types_contribuyentes`;
CREATE TABLE `mod_types_contribuyentes` (
  `mod_typc_id` int NOT NULL AUTO_INCREMENT,
  `mod_typc_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_typc_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_typc_system` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_typc_state` int DEFAULT NULL,
  PRIMARY KEY (`mod_typc_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_types_contribuyentes
-- ----------------------------
BEGIN;
INSERT INTO `mod_types_contribuyentes` VALUES (1, 'Alquiler de Bienes', NULL, 'tributo_v1', 1);
INSERT INTO `mod_types_contribuyentes` VALUES (2, 'Consultor en Linea', NULL, 'tributo_v1', 1);
INSERT INTO `mod_types_contribuyentes` VALUES (3, 'Oficio Independiente', NULL, 'tributo_v1', 1);
INSERT INTO `mod_types_contribuyentes` VALUES (4, 'Profesional Independiente', NULL, 'tributo_v1', 1);
COMMIT;

-- ----------------------------
-- Table structure for mod_types_forms
-- ----------------------------
DROP TABLE IF EXISTS `mod_types_forms`;
CREATE TABLE `mod_types_forms` (
  `mod_type_form_id` int NOT NULL AUTO_INCREMENT,
  `mod_type_form_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_type_form_declaracion` varchar(24) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_type_form_mode` varchar(44) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_type_form_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_type_form_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_type_form_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=611 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_types_forms
-- ----------------------------
BEGIN;
INSERT INTO `mod_types_forms` VALUES (110, 'Formulario 110', 'trimestral', 'impuestos', '0', 0);
INSERT INTO `mod_types_forms` VALUES (200, 'Formulario 200', 'mensual', 'impuestos', '0', 1);
INSERT INTO `mod_types_forms` VALUES (400, 'Formulario 400', 'mensual', 'impuestos', '0', 0);
INSERT INTO `mod_types_forms` VALUES (610, 'Formulario 610', 'trimestral', 'impuestos', '0', 1);
COMMIT;

-- ----------------------------
-- Table structure for mod_types_oficios
-- ----------------------------
DROP TABLE IF EXISTS `mod_types_oficios`;
CREATE TABLE `mod_types_oficios` (
  `mod_tb_tof_id` int NOT NULL AUTO_INCREMENT,
  `mod_tb_tof_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_tof_descrition` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_tof_code` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_tof_state` int NOT NULL,
  PRIMARY KEY (`mod_tb_tof_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_types_oficios
-- ----------------------------
BEGIN;
INSERT INTO `mod_types_oficios` VALUES (1, 'Alquiler de Bienes', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (2, 'Consultor en Linea', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (3, 'Instaladores', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (4, 'Chóferes', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (5, 'Carpiteros', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (6, 'Mecánicos', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (7, 'Consultores Independientes', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (8, 'Arquitectos', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (9, 'Abogados', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (10, 'Médicos', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (11, 'Catedráticos', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (12, 'Ingenieros', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (13, 'Veterinarios', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (14, 'Psicólogos', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (15, 'Bioquimicas', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (16, 'Economistas', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (17, 'Otros Profesionales Independientes', '0', NULL, 1);
COMMIT;


