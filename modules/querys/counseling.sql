DROP TABLE IF EXISTS mod_advised_schedules;

CREATE TABLE `mod_advised_schedules` (
  `mod_adv_sch_cou_id` int(11) NOT NULL,
  `mod_adv_sch_day` int(11) NOT NULL,
  `mod_adv_sch_hour_start` varchar(5) DEFAULT NULL,
  `mod_adv_sch_hour_end` varchar(5) DEFAULT NULL,
  `mod_adv_sch_count` int(11) DEFAULT NULL,
  `mod_adv_sch_state` int(11) NOT NULL DEFAULT '0'
) ENGINE = MyISAM DEFAULT CHARSET = latin1;
--
-- Índices para tablas volcadas
--
--
-- Indices de la tabla `mod_advised_schedules`
--
ALTER TABLE
  `mod_advised_schedules` DROP PRIMARY KEY,
ADD
  PRIMARY KEY(
    `mod_adv_sch_cou_id`,
    `mod_adv_sch_day`,
    `mod_adv_sch_count`
  );