DROP TABLE IF EXISTS mod_events;
DROP TABLE IF EXISTS mod_events_halls;
CREATE TABLE `mod_events` (
  `mod_eve_id` int(11) NOT NULL,
  `mod_eve_name` varchar(255) DEFAULT NULL,
  `mod_eve_description` tinytext,
  `mod_eve_register_date` datetime DEFAULT NULL,
  `mod_eve_user_id` int(11) DEFAULT NULL,
  `mod_eve_init_date` datetime DEFAULT NULL,
  `mod_eve_end_date` datetime DEFAULT NULL,
  `mod_eve_seats` int(21) DEFAULT NULL,
  `mod_eve_hall` int(11) DEFAULT NULL,
  `mod_eve_cost_seat` decimal(21, 2) DEFAULT NULL,
  `mod_eve_coin` varchar(10) NOT NULL DEFAULT 'Bs',
  `mod_eve_ent_id` int(11) DEFAULT NULL,
  `mod_eve_state` int(11) NOT NULL DEFAULT '0'
) ENGINE = MyISAM DEFAULT CHARSET = utf8mb4;
--
-- Indices de la tabla `mod_events`
--
ALTER TABLE
  `mod_events`
ADD
  PRIMARY KEY (`mod_eve_id`);
-- AUTO_INCREMENT de la tabla `mod_events`
  --
ALTER TABLE
  `mod_events`
MODIFY
  `mod_eve_id` int(11) NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 1;
--
  -- Estructura de tabla para la tabla `mod_events_halls`
  --
  CREATE TABLE `mod_events_halls` (
    `mod_eve_hall_id` int(11) NOT NULL,
    `mod_eve_hall_ent_id` int(11) NOT NULL,
    `mod_eve_hall_name` varchar(255) DEFAULT NULL,
    `mod_eve_hall_details` tinytext,
    `mod_eve_hall_url_draw_id` varchar(255) DEFAULT NULL,
    `mod_eve_hall_num_tables` int(11) DEFAULT NULL,
    `mod_eve_hall_num_seats` int(11) DEFAULT NULL,
    `mod_eve_hall_address` varchar(255) DEFAULT NULL,
    `mod_eve_hall_city` varchar(255) DEFAULT NULL,
    `mod_eve_hall_country` varchar(255) DEFAULT NULL,
    `mod_eve_hall_coord` varchar(440) DEFAULT NULL,
    `mod_eve_hall_user_id` int(11) DEFAULT NULL,
    `mod_eve_hall_register_date` datetime DEFAULT NULL,
    `mod_eve_hall_state` int(11) NOT NULL DEFAULT '0'
  ) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;
--
  -- Indices de la tabla `mod_events_modes`
  --
ALTER TABLE
  `mod_events_halls`
ADD
  PRIMARY KEY (`mod_eve_hall_id`, `mod_eve_hall_ent_id`);
ALTER TABLE
  `mod_events_halls`
MODIFY
  `mod_eve_hall_id` int(11) NOT NULL AUTO_INCREMENT;