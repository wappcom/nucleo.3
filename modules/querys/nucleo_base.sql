/*
 Navicat Premium Data Transfer

 Source Server         : dev-wappcom
 Source Server Type    : MySQL
 Source Server Version : 80040
 Source Host           : 192.168.0.7:3306
 Source Schema         : nucleo_base

 Target Server Type    : MySQL
 Target Server Version : 80040
 File Encoding         : 65001

 Date: 30/12/2024 07:15:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for blocks
-- ----------------------------
DROP TABLE IF EXISTS `blocks`;
CREATE TABLE `blocks` (
  `block_id` int NOT NULL AUTO_INCREMENT,
  `block_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `block_class` varchar(44) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `block_order` int DEFAULT NULL,
  `block_parent_id` int NOT NULL,
  `block_ent_id` int NOT NULL DEFAULT '0',
  `block_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`block_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of blocks
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for categorys
-- ----------------------------
DROP TABLE IF EXISTS `categorys`;
CREATE TABLE `categorys` (
  `cat_id` int NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cat_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cat_pathurl` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cat_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `cat_order` int DEFAULT NULL,
  `cat_parent_id` int DEFAULT '0',
  `cat_related` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cat_icon` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cat_color` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cat_img` int DEFAULT NULL,
  `cat_banner` int DEFAULT NULL,
  `cat_ent_id` int DEFAULT NULL,
  `cat_configpath` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cat_cls` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cat_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `cat_state_collapse` int NOT NULL DEFAULT '1',
  `cat_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`cat_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of categorys
-- ----------------------------
BEGIN;
INSERT INTO `categorys` VALUES (16, 'home', 'home', 'home', '', 0, 0, '', '', '', 0, 0, 1, '', '', '', 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for categorys_files
-- ----------------------------
DROP TABLE IF EXISTS `categorys_files`;
CREATE TABLE `categorys_files` (
  `cat_file_file_id` int NOT NULL,
  `cat_file_cat_id` int NOT NULL,
  `cat_file_order` int NOT NULL,
  PRIMARY KEY (`cat_file_file_id`,`cat_file_cat_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of categorys_files
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for contents
-- ----------------------------
DROP TABLE IF EXISTS `contents`;
CREATE TABLE `contents` (
  `cont_id` int NOT NULL AUTO_INCREMENT,
  `cont_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `cont_pathurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `cont_summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `cont_tags` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cont_img` int DEFAULT NULL,
  `cont_body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `cont_author` int DEFAULT NULL,
  `cont_register_date` datetime DEFAULT NULL,
  `cont_user_id` int DEFAULT NULL COMMENT 'user register',
  `cont_notitle` int DEFAULT '0',
  `cont_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cont_btn_title` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cont_icon` varchar(21) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cont_target` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '_blank',
  `cont_cls` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'css class',
  `cont_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `cont_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`cont_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of contents
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for contents_categorys
-- ----------------------------
DROP TABLE IF EXISTS `contents_categorys`;
CREATE TABLE `contents_categorys` (
  `cont_cat_cont_id` int NOT NULL,
  `cont_cat_cat_id` int NOT NULL,
  `cont_cat_ent_id` int DEFAULT NULL,
  `cont_cat_order` int NOT NULL,
  PRIMARY KEY (`cont_cat_cont_id`,`cont_cat_cat_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of contents_categorys
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for contents_files
-- ----------------------------
DROP TABLE IF EXISTS `contents_files`;
CREATE TABLE `contents_files` (
  `cont_file_cont_id` int NOT NULL,
  `cont_file_file_id` int NOT NULL,
  `cont_file_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `cont_file_summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `cont_file_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `cont_file_btn_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cont_file_icon` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cont_file_target` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '_blank',
  `cont_file_order` int NOT NULL,
  PRIMARY KEY (`cont_file_cont_id`,`cont_file_file_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of contents_files
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for contents_pubs
-- ----------------------------
DROP TABLE IF EXISTS `contents_pubs`;
CREATE TABLE `contents_pubs` (
  `cont_pub_cont_id` int NOT NULL,
  `cont_pub_pub_id` int NOT NULL,
  `cont_pub_order` int NOT NULL,
  PRIMARY KEY (`cont_pub_cont_id`,`cont_pub_pub_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of contents_pubs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for docs
-- ----------------------------
DROP TABLE IF EXISTS `docs`;
CREATE TABLE `docs` (
  `doc_id` int NOT NULL AUTO_INCREMENT,
  `doc_file_id` int DEFAULT NULL,
  `doc_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `doc_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `doc_tags` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `doc_img` int NOT NULL DEFAULT '0' COMMENT 'customized',
  `doc_date` datetime DEFAULT NULL,
  `doc_register_date` datetime DEFAULT NULL,
  `doc_user_id` int NOT NULL DEFAULT '0',
  `doc_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `doc_ent_id` int DEFAULT NULL,
  `doc_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`doc_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of docs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for docs_categorys
-- ----------------------------
DROP TABLE IF EXISTS `docs_categorys`;
CREATE TABLE `docs_categorys` (
  `doc_cat_doc_id` int NOT NULL,
  `doc_cat_cat_id` int NOT NULL,
  `doc_cat_order` int NOT NULL,
  `doc_cat_ent_id` int DEFAULT NULL,
  PRIMARY KEY (`doc_cat_doc_id`,`doc_cat_cat_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of docs_categorys
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for entities
-- ----------------------------
DROP TABLE IF EXISTS `entities`;
CREATE TABLE `entities` (
  `ent_id` int NOT NULL AUTO_INCREMENT,
  `ent_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_brand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_code` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ent_record_date` datetime DEFAULT NULL,
  `ent_type` int DEFAULT NULL,
  `ent_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_state` int DEFAULT NULL,
  PRIMARY KEY (`ent_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of entities
-- ----------------------------
BEGIN;
INSERT INTO `entities` VALUES (1, 'Wappcom', 'wappcom', 'modules/assets/img/logo.svg', 'CO1', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3IiwibmFtZSI6IkNhc2EgZGUgT3JhY2lvbiIsImlhdCI6MTIzNDU2Nzg5fQ.y1fa_y48leQNYbZGDlvJewtehVj78zMhkJ73B-8MGyE', '2020-10-20 14:55:39', 1, '', 1);
COMMIT;

-- ----------------------------
-- Table structure for files
-- ----------------------------
DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `file_id` int NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_pathurl` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_embed` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_btn_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_target` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '_self',
  `file_ext` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_alt` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_filename_md5` binary(255) DEFAULT NULL,
  `file_datatime` datetime DEFAULT NULL,
  `file_ent_id` int DEFAULT NULL,
  `file_state` int DEFAULT '0',
  PRIMARY KEY (`file_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of files
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `group_id` int NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `group_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `group_state` int DEFAULT NULL,
  PRIMARY KEY (`group_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of groups
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for groups_files
-- ----------------------------
DROP TABLE IF EXISTS `groups_files`;
CREATE TABLE `groups_files` (
  `group_file_group_id` int NOT NULL,
  `group_file_file_id` int NOT NULL,
  `group_file_order` int NOT NULL,
  PRIMARY KEY (`group_file_group_id`,`group_file_file_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of groups_files
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for links
-- ----------------------------
DROP TABLE IF EXISTS `links`;
CREATE TABLE `links` (
  `lnk_id` int NOT NULL AUTO_INCREMENT,
  `lnk_title` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lnk_description` varchar(445) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lnk_tags` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lnk_url` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '#',
  `lnk_target` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '_blank',
  `lnk_icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lnk_attr` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lnk_cls` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lnk_alt` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lnk_img` int DEFAULT NULL,
  `lnk_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `lnk_ent_id` int DEFAULT NULL,
  `lnk_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`lnk_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of links
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for links_categorys
-- ----------------------------
DROP TABLE IF EXISTS `links_categorys`;
CREATE TABLE `links_categorys` (
  `lnk_cat_cat_id` int NOT NULL,
  `lnk_cat_lnk_id` int NOT NULL,
  `lnk_cat_order` int DEFAULT NULL,
  PRIMARY KEY (`lnk_cat_cat_id`,`lnk_cat_lnk_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of links_categorys
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for links_pubs
-- ----------------------------
DROP TABLE IF EXISTS `links_pubs`;
CREATE TABLE `links_pubs` (
  `lnk_pub_pub_id` int NOT NULL,
  `lnk_pub_lnk_id` int NOT NULL,
  `lnk_pub_order` int NOT NULL,
  PRIMARY KEY (`lnk_pub_pub_id`,`lnk_pub_lnk_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of links_pubs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `menu_id` int NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `menu_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `menu_ent_id` int DEFAULT NULL,
  `menu_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of menus
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for menus_items
-- ----------------------------
DROP TABLE IF EXISTS `menus_items`;
CREATE TABLE `menus_items` (
  `menu_item_id` int NOT NULL AUTO_INCREMENT,
  `menu_item_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `menu_item_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `menu_item_menu_id` int DEFAULT NULL,
  `menu_item_cat_id` int DEFAULT NULL,
  `menu_item_cat_active` int DEFAULT '0',
  `menu_item_pathurl` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `menu_item_target` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '_self',
  `menu_item_parent_id` int DEFAULT '0',
  `menu_item_icon` int DEFAULT NULL,
  `menu_item_img` int DEFAULT NULL,
  `menu_item_order` int DEFAULT NULL,
  `menu_item_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_item_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of menus_items
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_accounting_plan
-- ----------------------------
DROP TABLE IF EXISTS `mod_accounting_plan`;
CREATE TABLE `mod_accounting_plan` (
  `mod_acp_id` int NOT NULL AUTO_INCREMENT,
  `mod_acp_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acp_descripcion` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acp_parent_id` int DEFAULT NULL,
  `mod_acp_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acp_level` int DEFAULT NULL,
  `mod_acp_coin` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acp_has_referral_code` int NOT NULL DEFAULT '0' COMMENT 'tiene codigo de referido',
  `mod_acp_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_acp_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_accounting_plan
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_accounts_addresses
-- ----------------------------
DROP TABLE IF EXISTS `mod_accounts_addresses`;
CREATE TABLE `mod_accounts_addresses` (
  `mod_acu_add_acu_id` int NOT NULL,
  `mod_acu_add_add_id` int NOT NULL,
  `mod_acu_add_order` int NOT NULL,
  PRIMARY KEY (`mod_acu_add_acu_id`,`mod_acu_add_add_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_accounts_addresses
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_accounts_ads
-- ----------------------------
DROP TABLE IF EXISTS `mod_accounts_ads`;
CREATE TABLE `mod_accounts_ads` (
  `mod_aca_id` int NOT NULL AUTO_INCREMENT,
  `mod_aca_cen_id` int NOT NULL,
  `mod_aca_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_aca_description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_aca_code` int DEFAULT NULL,
  `mod_aca_ent_id` int DEFAULT NULL,
  `mod_aca_state` int DEFAULT NULL,
  PRIMARY KEY (`mod_aca_id`,`mod_aca_cen_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_accounts_ads
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_accounts_invoices_data
-- ----------------------------
DROP TABLE IF EXISTS `mod_accounts_invoices_data`;
CREATE TABLE `mod_accounts_invoices_data` (
  `mod_acu_invd_id` int NOT NULL AUTO_INCREMENT,
  `mod_acu_invd_acu_id` int NOT NULL,
  `mod_acu_invd_razon_social` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_invd_nit` int NOT NULL,
  `mod_acu_invd_order` int NOT NULL,
  `mod_acu_invd_state` int DEFAULT '0',
  PRIMARY KEY (`mod_acu_invd_id`,`mod_acu_invd_acu_id`,`mod_acu_invd_nit`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_accounts_invoices_data
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_accounts_plans
-- ----------------------------
DROP TABLE IF EXISTS `mod_accounts_plans`;
CREATE TABLE `mod_accounts_plans` (
  `mod_ap_id` int NOT NULL AUTO_INCREMENT,
  `mod_ap_name` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_ap_description` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_ap_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_ap_level` int DEFAULT NULL,
  `mod_ap_details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_ap_cost` decimal(10,2) DEFAULT NULL,
  `mod_ap_coin` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Bs',
  `mod_ap_mode` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'monthly',
  `mod_ap_last` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_ap_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_ap_ent_id` int DEFAULT NULL,
  `mod_ap_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_ap_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_accounts_plans
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_accounts_roles
-- ----------------------------
DROP TABLE IF EXISTS `mod_accounts_roles`;
CREATE TABLE `mod_accounts_roles` (
  `mod_acu_rol_id` int NOT NULL AUTO_INCREMENT,
  `mod_acu_rol_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_rol_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_rol_parent_id` int DEFAULT NULL,
  `mod_acu_rol_redirection_url` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_rol_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_acu_rol_state` int DEFAULT '0',
  PRIMARY KEY (`mod_acu_rol_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_accounts_roles
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_accounts_services
-- ----------------------------
DROP TABLE IF EXISTS `mod_accounts_services`;
CREATE TABLE `mod_accounts_services` (
  `mod_acu_sv_sv_id` int NOT NULL,
  `mod_acu_sv_acu_id` int NOT NULL,
  `mod_acu_sv_order` int NOT NULL,
  PRIMARY KEY (`mod_acu_sv_sv_id`,`mod_acu_sv_acu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_accounts_services
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_accounts_token
-- ----------------------------
DROP TABLE IF EXISTS `mod_accounts_token`;
CREATE TABLE `mod_accounts_token` (
  `mod_atk_user_id` bigint NOT NULL,
  `mod_atk_type` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_atk_token` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_atk_expires_in` datetime NOT NULL,
  `mod_atk_date` datetime NOT NULL,
  `mod_atk_dates_browser` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`mod_atk_type`,`mod_atk_token`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_accounts_token
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_accounts_users
-- ----------------------------
DROP TABLE IF EXISTS `mod_accounts_users`;
CREATE TABLE `mod_accounts_users` (
  `mod_acu_id` int NOT NULL AUTO_INCREMENT,
  `mod_acu_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_fathers_lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_acu_mothers_lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_acu_age_range` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_acu_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_acu_birthday_date` date DEFAULT NULL,
  `mod_acu_password` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_imagen` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_level` int DEFAULT '0',
  `mod_acu_plan_id` int DEFAULT NULL,
  `mod_acu_gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'no-gender,male,female',
  `mod_acu_ci` int DEFAULT NULL,
  `mod_acu_ci_ext` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_dial` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_celular` int DEFAULT NULL,
  `mod_acu_record_date` datetime DEFAULT NULL,
  `mod_acu_type` varchar(44) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_timezone` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_locale` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_referred` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_ent_id` int DEFAULT NULL,
  `mod_acu_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_acu_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_accounts_users
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_accounts_users_plans
-- ----------------------------
DROP TABLE IF EXISTS `mod_accounts_users_plans`;
CREATE TABLE `mod_accounts_users_plans` (
  `mod_acu_ap_acu_id` int NOT NULL,
  `mod_acu_ap_ap_id` int NOT NULL,
  `mod_acu_ap_mode` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_acu_ap_date_register` datetime NOT NULL,
  PRIMARY KEY (`mod_acu_ap_acu_id`,`mod_acu_ap_ap_id`,`mod_acu_ap_mode`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_accounts_users_plans
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_accounts_users_roles
-- ----------------------------
DROP TABLE IF EXISTS `mod_accounts_users_roles`;
CREATE TABLE `mod_accounts_users_roles` (
  `mod_acu_user_rol_acu_id` int NOT NULL,
  `mod_acu_user_rol_rol_id` int NOT NULL,
  `mod_acu_user_rol_ent_id` int NOT NULL,
  `mod_acu_user_rol_order` int NOT NULL,
  PRIMARY KEY (`mod_acu_user_rol_acu_id`,`mod_acu_user_rol_rol_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_accounts_users_roles
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_actions
-- ----------------------------
DROP TABLE IF EXISTS `mod_actions`;
CREATE TABLE `mod_actions` (
  `mod_act_id` int NOT NULL AUTO_INCREMENT,
  `mod_act_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_act_summary` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_act_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_act_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_act_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_actions
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_actions_register
-- ----------------------------
DROP TABLE IF EXISTS `mod_actions_register`;
CREATE TABLE `mod_actions_register` (
  `mod_act_reg_act_id` int NOT NULL COMMENT 'mod_actions',
  `mod_act_reg_cpa_id` int NOT NULL COMMENT 'mod_campaigns_ads',
  `mod_act_reg_aca_id` int NOT NULL COMMENT 'mod_accounts_ads',
  `mod_act_reg_acu_id` int NOT NULL COMMENT 'mod_accounts_user',
  `mod_act_reg_plc_id` int NOT NULL COMMENT 'mod_places',
  `mod_act_reg_value` int NOT NULL,
  `mod_act_reg_register_date` datetime NOT NULL,
  `mod_act_reg_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_act_reg_act_id`,`mod_act_reg_cpa_id`,`mod_act_reg_aca_id`,`mod_act_reg_acu_id`,`mod_act_reg_plc_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_actions_register
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_acuse
-- ----------------------------
DROP TABLE IF EXISTS `mod_acuse`;
CREATE TABLE `mod_acuse` (
  `mod_ac_id` int NOT NULL AUTO_INCREMENT,
  `mod_ac_date` datetime NOT NULL,
  `mod_ac_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`mod_ac_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_acuse
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_addresses
-- ----------------------------
DROP TABLE IF EXISTS `mod_addresses`;
CREATE TABLE `mod_addresses` (
  `mod_add_id` int NOT NULL AUTO_INCREMENT,
  `mod_add_name` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_add_alias` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_add_description` varchar(700) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_add_coord` varchar(700) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_add_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_add_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_addresses
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_addresses_logistics_distribution_zone
-- ----------------------------
DROP TABLE IF EXISTS `mod_addresses_logistics_distribution_zone`;
CREATE TABLE `mod_addresses_logistics_distribution_zone` (
  `mod_add_ldz_add_id` int NOT NULL,
  `mod_add_ldz_ldz_id` int NOT NULL,
  `mod_add_ldz_order` int NOT NULL,
  PRIMARY KEY (`mod_add_ldz_add_id`,`mod_add_ldz_ldz_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_addresses_logistics_distribution_zone
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_ads
-- ----------------------------
DROP TABLE IF EXISTS `mod_ads`;
CREATE TABLE `mod_ads` (
  `mod_ads_id` int NOT NULL AUTO_INCREMENT,
  `mod_ads_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_ads_details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_ads_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_ads_img` int DEFAULT NULL,
  `mod_ads_position` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_ads_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_ads_user_id` int DEFAULT NULL,
  `mod_ads_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_ads_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_ads
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_ads_customers
-- ----------------------------
DROP TABLE IF EXISTS `mod_ads_customers`;
CREATE TABLE `mod_ads_customers` (
  `mod_ads_cus_id` int NOT NULL AUTO_INCREMENT,
  `mod_ads_cus_ads_id` int NOT NULL COMMENT 'mod_ads',
  `mod_ads_cus_cen_id` int NOT NULL COMMENT 'mod_customers_enterprise',
  `mod_ads_cus_date_init` datetime NOT NULL,
  `mod_ads_cus_date_end` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mod_ads_cus_img` int NOT NULL,
  `mod_ads_cus_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_ads_cus_state` int NOT NULL DEFAULT '0' COMMENT '0: Inactivo, 1: Activo, 2: Desactivado por tiempo, 3: Desactivado por cantidad de clicks, 4: Desactivado por cantidad de impresiones',
  PRIMARY KEY (`mod_ads_cus_id`,`mod_ads_cus_ads_id`,`mod_ads_cus_cen_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_ads_customers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_ads_position
-- ----------------------------
DROP TABLE IF EXISTS `mod_ads_position`;
CREATE TABLE `mod_ads_position` (
  `mod_ads_pos_id` int NOT NULL AUTO_INCREMENT,
  `mod_ads_pos_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_ads_pos_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_ads_pos_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_ads_pos_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_ads_position
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_advised_calendars
-- ----------------------------
DROP TABLE IF EXISTS `mod_advised_calendars`;
CREATE TABLE `mod_advised_calendars` (
  `mod_adv_cal_id` int NOT NULL AUTO_INCREMENT,
  `mod_adv_cal_acu_id` int DEFAULT NULL,
  `mod_adv_cal_cou_id` int DEFAULT NULL,
  `mod_adv_cal_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'appointment',
  `mod_adv_cal_date_start` datetime DEFAULT NULL,
  `mod_adv_cal_date_end` datetime DEFAULT NULL,
  `mod_adv_cal_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_cal_details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_adv_cal_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_cal_reason_cancel` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_cal_reschedule` int DEFAULT NULL,
  `mod_adv_cal_flags` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_cal_ent_id` int DEFAULT NULL,
  `mod_adv_cal_user_id` int DEFAULT NULL,
  `mod_adv_cal_date_register` datetime DEFAULT NULL,
  `mod_adv_cal_state` int NOT NULL DEFAULT '0' COMMENT '0. Eliminado, 1. Activo, 2. cancelado, 3. reagendado, 4. Atendido',
  PRIMARY KEY (`mod_adv_cal_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_advised_calendars
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_advised_calendars_blocked
-- ----------------------------
DROP TABLE IF EXISTS `mod_advised_calendars_blocked`;
CREATE TABLE `mod_advised_calendars_blocked` (
  `mod_adv_cb_id` int NOT NULL AUTO_INCREMENT,
  `mod_adv_cb_date_start` datetime NOT NULL,
  `mod_adv_cb_date_end` datetime DEFAULT NULL,
  `mod_adv_cb_ent_id` int DEFAULT NULL,
  `mod_adv_cb_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_adv_cb_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_advised_calendars_blocked
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_advised_counselors
-- ----------------------------
DROP TABLE IF EXISTS `mod_advised_counselors`;
CREATE TABLE `mod_advised_counselors` (
  `mod_adv_cou_acu_id` int NOT NULL,
  `mod_adv_cou_cou_id` int NOT NULL,
  `mod_adv_cou_order` int NOT NULL,
  PRIMARY KEY (`mod_adv_cou_acu_id`,`mod_adv_cou_cou_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_advised_counselors
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_advised_counselors_fields
-- ----------------------------
DROP TABLE IF EXISTS `mod_advised_counselors_fields`;
CREATE TABLE `mod_advised_counselors_fields` (
  `mod_adv_fds_user_id` int NOT NULL AUTO_INCREMENT,
  `mod_adv_fds_fds_id` int NOT NULL,
  `mod_adv_fds_order` int NOT NULL,
  PRIMARY KEY (`mod_adv_fds_user_id`,`mod_adv_fds_fds_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_advised_counselors_fields
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_advised_history
-- ----------------------------
DROP TABLE IF EXISTS `mod_advised_history`;
CREATE TABLE `mod_advised_history` (
  `mod_adv_his_id` int NOT NULL AUTO_INCREMENT,
  `mod_adv_his_acu_id` int DEFAULT NULL,
  `mod_adv_his_user_id` int DEFAULT NULL,
  `mod_adv_his_fds_id` int DEFAULT NULL,
  `mod_adv_his_comments` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_adv_his_date_registration` datetime DEFAULT NULL,
  `mod_adv_his_date_init` datetime DEFAULT NULL,
  `mod_adv_his_date_end` datetime DEFAULT NULL,
  `mod_adv_his_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_his_type` int NOT NULL DEFAULT '0' COMMENT '0. normal 1.Fuera de Hora',
  `mod_adv_his_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_adv_his_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_advised_history
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_advised_history_calendars
-- ----------------------------
DROP TABLE IF EXISTS `mod_advised_history_calendars`;
CREATE TABLE `mod_advised_history_calendars` (
  `mod_adv_his_cal_cal_id` int NOT NULL,
  `mod_adv_his_cal_his_id` int NOT NULL,
  `mod_adv_his_cal_order` int NOT NULL,
  PRIMARY KEY (`mod_adv_his_cal_cal_id`,`mod_adv_his_cal_his_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_advised_history_calendars
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_advised_options
-- ----------------------------
DROP TABLE IF EXISTS `mod_advised_options`;
CREATE TABLE `mod_advised_options` (
  `mod_adv_opt_id` int NOT NULL AUTO_INCREMENT,
  `mod_adv_opt_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_adv_opt_value` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_adv_opt_autoload` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`mod_adv_opt_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_advised_options
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_advised_schedules
-- ----------------------------
DROP TABLE IF EXISTS `mod_advised_schedules`;
CREATE TABLE `mod_advised_schedules` (
  `mod_adv_sch_id` int NOT NULL AUTO_INCREMENT,
  `mod_adv_sch_cou_id` int NOT NULL,
  `mod_adv_sch_day` int NOT NULL,
  `mod_adv_sch_hour_start` time NOT NULL,
  `mod_adv_sch_hour_end` time NOT NULL,
  `mod_adv_sch_count` int NOT NULL,
  `mod_adv_sch_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_adv_sch_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_advised_schedules
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_advised_types_appointments
-- ----------------------------
DROP TABLE IF EXISTS `mod_advised_types_appointments`;
CREATE TABLE `mod_advised_types_appointments` (
  `mod_adv_tya_id` int NOT NULL AUTO_INCREMENT,
  `mod_adv_tya_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_tya_summary` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_tya_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_tya_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_tya_color` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_tya_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_adv_tya_ent_id` int DEFAULT NULL,
  `mod_adv_tya_state` int DEFAULT '0',
  PRIMARY KEY (`mod_adv_tya_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_advised_types_appointments
-- ----------------------------
BEGIN;
INSERT INTO `mod_advised_types_appointments` VALUES (1, 'Consejeria', NULL, NULL, NULL, NULL, '{\r\n \"listUsers\",\"selectDay\"\r\n}', 1, 1);
INSERT INTO `mod_advised_types_appointments` VALUES (2, 'Otra Actividad', NULL, NULL, NULL, NULL, '{\r\n \"title\",\"selectDates\"\r\n}', 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for mod_advised_user
-- ----------------------------
DROP TABLE IF EXISTS `mod_advised_user`;
CREATE TABLE `mod_advised_user` (
  `mod_adv_id` int NOT NULL AUTO_INCREMENT,
  `mod_adv_fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_adv_comments` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_adv_acu_id` int DEFAULT NULL,
  `mod_adv_fds_id` int DEFAULT NULL,
  `mod_adv_civil_state` int DEFAULT NULL COMMENT '1.Soltero/a 2.Casado/a 3.Divorciado 4.Concuvinato 5. Unión Libre  6. Otro',
  `mod_adv_dependents` int DEFAULT NULL,
  `mod_adv_telf_fax` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_address` varchar(550) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_coordinates` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_labor_status` int DEFAULT NULL COMMENT '1. Dependiente 2.Independiente. 3. Jubilado/rentista. 4.Estudiante 5. Desempleado',
  `mod_adv_company_work` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_address_work` varchar(550) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_work_area` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_profession` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_position` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_fullname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_gender` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_ci` int DEFAULT NULL,
  `mod_adv_spouse_ext` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_birthday` date DEFAULT NULL,
  `mod_adv_spouse_nationality` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_telf_fax` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_cellphone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_labor_status` int DEFAULT NULL,
  `mod_adv_spouse_company_work` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_address_work` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_work_area` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_profession` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_position` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`mod_adv_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_advised_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_appointments
-- ----------------------------
DROP TABLE IF EXISTS `mod_appointments`;
CREATE TABLE `mod_appointments` (
  `mod_apt_id` int NOT NULL AUTO_INCREMENT COMMENT 'Id único de la cita',
  `mod_apt_cpe_id` int NOT NULL COMMENT 'Id customer persona (cliente)',
  `mod_apt_person_id` int NOT NULL COMMENT 'Id de la persona asociada a la cita',
  `mod_apt_acu_id` int NOT NULL COMMENT 'Id persona que se registra',
  `mod_apt_provider_id` int NOT NULL COMMENT 'Id proveedor (cuenta relacionada con el servicio)',
  `mod_apt_srv_id` int NOT NULL COMMENT 'Id del servicio relacionado',
  `mod_apt_sch_id` int NOT NULL COMMENT 'Id del horario asociado',
  `mod_apt_date` date NOT NULL COMMENT 'Fecha de la cita',
  `mod_apt_time` time DEFAULT NULL COMMENT 'Hora de la cita (NULL si es todo el día)',
  `mod_apt_all_day` tinyint(1) DEFAULT '0' COMMENT 'Indica si la cita es todo el día (0: No, 1: Sí)',
  `mod_apt_place` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Lugar donde se llevará a cabo la cita',
  `mod_apt_guests` text COLLATE utf8mb4_general_ci COMMENT 'Lista de invitados (almacenados como JSON)',
  `mod_apt_status` varchar(20) COLLATE utf8mb4_general_ci DEFAULT 'pending' COMMENT 'Estado de la cita (pending, confirmed, completed, cancelled)',
  `mod_apt_type` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Tipo de cita (ejemplo: consulta, revisión, seguimiento)',
  `mod_apt_flags` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Banderas o indicadores especiales asociados a la cita',
  `mod_apt_comments` text COLLATE utf8mb4_general_ci COMMENT 'Comentarios o descripción adicional del evento',
  `mod_apt_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación de la cita',
  `mod_apt_updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización de la cita',
  `mod_apt_state` int DEFAULT '0' COMMENT 'Estado lógico de la cita (0: inactivo, 1: activo)',
  PRIMARY KEY (`mod_apt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_appointments
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_appointments_exceptions
-- ----------------------------
DROP TABLE IF EXISTS `mod_appointments_exceptions`;
CREATE TABLE `mod_appointments_exceptions` (
  `mod_apt_exc_id` int NOT NULL AUTO_INCREMENT,
  `mod_apt_exc_provider_id` int NOT NULL COMMENT 'Id del proveedor',
  `mod_apt_exc_date` date NOT NULL COMMENT 'Fecha de la excepción',
  `mod_apt_exc_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Razón de la excepción',
  `mod_apt_exc_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación',
  `mod_apt_exc_updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización',
  PRIMARY KEY (`mod_apt_exc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Excepciones en la disponibilidad de los proveedores (vacaciones, días festivos, etc.)';

-- ----------------------------
-- Records of mod_appointments_exceptions
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_appointments_logs
-- ----------------------------
DROP TABLE IF EXISTS `mod_appointments_logs`;
CREATE TABLE `mod_appointments_logs` (
  `mod_apt_log_id` int NOT NULL AUTO_INCREMENT,
  `mod_apt_log_apt_id` int NOT NULL COMMENT 'Id de la cita',
  `mod_apt_log_action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Acción realizada (crear, modificar, cancelar, etc.)',
  `mod_apt_log_changed_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha y hora del cambio',
  `mod_apt_log_changed_by` int NOT NULL COMMENT 'Id del usuario o administrador que realizó el cambio',
  PRIMARY KEY (`mod_apt_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Historial de cambios realizados en las citas';

-- ----------------------------
-- Records of mod_appointments_logs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_appointments_payments
-- ----------------------------
DROP TABLE IF EXISTS `mod_appointments_payments`;
CREATE TABLE `mod_appointments_payments` (
  `mod_apt_pay_id` int NOT NULL AUTO_INCREMENT COMMENT 'Id del pago',
  `mod_apt_pay_apt_id` int NOT NULL,
  `mod_apt_pay_amount` decimal(10,2) NOT NULL,
  `mod_apt_pay_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `mod_apt_pay_method` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Método de pago credit_card,paypal,stripe,efectivo,qr..',
  `mod_apt_pay_status` varchar(255) COLLATE utf8mb4_general_ci DEFAULT 'pending,failed,completed',
  `mod_apt_pay_state` int DEFAULT '0',
  PRIMARY KEY (`mod_apt_pay_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_appointments_payments
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_appointments_reviews
-- ----------------------------
DROP TABLE IF EXISTS `mod_appointments_reviews`;
CREATE TABLE `mod_appointments_reviews` (
  `mod_apt_rev_id` int NOT NULL AUTO_INCREMENT COMMENT 'Id único de la reseña',
  `mod_apt_rev_apt_id` int NOT NULL COMMENT 'Id de la cita asociada',
  `mod_apt_rev_client_id` int NOT NULL COMMENT 'Id del cliente que realiza la reseña',
  `mod_apt_rev_provider_id` int NOT NULL COMMENT 'Id del proveedor asociado a la reseña',
  `mod_apt_rev_rating` int DEFAULT NULL COMMENT 'Calificación dada por el cliente (1-5)',
  `mod_apt_rev_comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT 'Comentario opcional del cliente',
  `mod_apt_rev_register_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de registro de la reseña',
  `mod_apt_rev_state` int DEFAULT '0' COMMENT 'Estado de la reseña (activo/inactivo)',
  PRIMARY KEY (`mod_apt_rev_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Tabla de reseñas y valoraciones de los servicios por parte de los clientes';

-- ----------------------------
-- Records of mod_appointments_reviews
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_appointments_schedules
-- ----------------------------
DROP TABLE IF EXISTS `mod_appointments_schedules`;
CREATE TABLE `mod_appointments_schedules` (
  `mod_apt_sch_id` int NOT NULL AUTO_INCREMENT,
  `mod_apt_sch_provider_id` int NOT NULL COMMENT 'Id del proveedor',
  `mod_apt_sch_day` enum('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Día de la semana',
  `mod_apt_sch_hour_start` time NOT NULL COMMENT 'Hora de inicio',
  `mod_apt_sch_hour_end` time NOT NULL COMMENT 'Hora de finalización',
  `mod_apt_sch_state` int DEFAULT '0' COMMENT 'Estado (0: Inactivo, 1: Activo)',
  `mod_apt_sch_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación',
  `mod_apt_sch_updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización',
  PRIMARY KEY (`mod_apt_sch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Disponibilidad de proveedores por día y hora';

-- ----------------------------
-- Records of mod_appointments_schedules
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_brands
-- ----------------------------
DROP TABLE IF EXISTS `mod_brands`;
CREATE TABLE `mod_brands` (
  `mod_brd_id` int NOT NULL AUTO_INCREMENT,
  `mod_brd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_brd_img` int DEFAULT NULL,
  `mod_brd_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_brd_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_brands
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_campaigns_actions
-- ----------------------------
DROP TABLE IF EXISTS `mod_campaigns_actions`;
CREATE TABLE `mod_campaigns_actions` (
  `mod_cpa_act_cpa_id` int NOT NULL,
  `mod_cpa_act_act_id` int NOT NULL,
  `mod_cpa_act_rew` int DEFAULT NULL,
  `mod_cpa_act_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_cpa_act_cpa_id`,`mod_cpa_act_act_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_campaigns_actions
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_campaigns_ads
-- ----------------------------
DROP TABLE IF EXISTS `mod_campaigns_ads`;
CREATE TABLE `mod_campaigns_ads` (
  `mod_cpa_id` int NOT NULL AUTO_INCREMENT,
  `mod_cpa_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpa_description` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_cpa_acu_id` int DEFAULT NULL COMMENT 'mod_account_users',
  `mod_cpa_aca_id` int DEFAULT NULL COMMENT '//mod_accounts_ads',
  `mod_cpa_type_customer` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpa_cpe_id` int DEFAULT NULL,
  `mod_cpa_cen_id` int NOT NULL,
  `mod_cpa_target_init_age` int DEFAULT NULL,
  `mod_cpa_target_end_age` int DEFAULT NULL,
  `mod_cpa_target_gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpa_init_date` datetime DEFAULT NULL,
  `mod_cpa_end_date` datetime DEFAULT NULL,
  `mod_cpa_register_date` datetime DEFAULT NULL,
  `mod_cpa_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_cpa_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_cpa_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_campaigns_ads
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_candire_campaigns
-- ----------------------------
DROP TABLE IF EXISTS `mod_candire_campaigns`;
CREATE TABLE `mod_candire_campaigns` (
  `mod_cnd_cp_id` int NOT NULL AUTO_INCREMENT,
  `mod_cnd_cp_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cnd_cp_description` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_cnd_cp_tags` varchar(445) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cnd_cp_acu_id` int DEFAULT NULL COMMENT 'mod_account_users',
  `mod_cnd_cp_type_customer` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cnd_cp_cpe_id` int DEFAULT NULL,
  `mod_cnd_cp_cen_id` int NOT NULL,
  `mod_cnd_cp_target_init_age` int DEFAULT NULL,
  `mod_cnd_cp_target_end_age` int DEFAULT NULL,
  `mod_cnd_cp_target_gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cnd_cp_init_date` datetime DEFAULT NULL,
  `mod_cnd_cp_end_date` datetime DEFAULT NULL,
  `mod_cnd_cp_register_date` datetime DEFAULT NULL,
  `mod_cnd_cp_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_cnd_cp_user_id` int DEFAULT NULL,
  `mod_cnd_cp_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_cnd_cp_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_candire_campaigns
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_carts
-- ----------------------------
DROP TABLE IF EXISTS `mod_carts`;
CREATE TABLE `mod_carts` (
  `mod_cart_id` int NOT NULL AUTO_INCREMENT,
  `mod_cart_date` datetime DEFAULT NULL,
  `mod_cart_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cart_acu_id` int NOT NULL,
  `mod_cart_add_id` int DEFAULT NULL,
  `mod_cart_ldz_id` int DEFAULT NULL,
  `mod_cart_details` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cart_payment_method` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cart_invoice_id` int DEFAULT NULL,
  `mod_cart_code_receipts` int DEFAULT NULL COMMENT 'Codigo de Recaudación\r\n',
  `mod_cart_transaction_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cart_gateway_url_payment` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cart_gateway_payment` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cart_gateway_url_invoice` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cart_gateway_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_cart_state` int NOT NULL DEFAULT '0' COMMENT '0 Creada 1 Registrada 2. Pagada 3. Revertida 4. Anulada 4.Eliminada\r\n',
  PRIMARY KEY (`mod_cart_id`,`mod_cart_acu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_carts
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_carts_products
-- ----------------------------
DROP TABLE IF EXISTS `mod_carts_products`;
CREATE TABLE `mod_carts_products` (
  `mod_cart_prod_cart_id` int NOT NULL,
  `mod_cart_prod_prod_id` int NOT NULL,
  `mod_cart_prod_price` decimal(20,2) DEFAULT NULL,
  `mod_cart_prod_order` int DEFAULT NULL,
  PRIMARY KEY (`mod_cart_prod_cart_id`,`mod_cart_prod_prod_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_carts_products
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_classification_info
-- ----------------------------
DROP TABLE IF EXISTS `mod_classification_info`;
CREATE TABLE `mod_classification_info` (
  `mod_clsi_id` int NOT NULL AUTO_INCREMENT,
  `mod_clsi_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_clsi_description` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_clsi_color` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_clsi_file_id` int DEFAULT NULL,
  `mod_clsi_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_clsi_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_classification_info
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_competitions
-- ----------------------------
DROP TABLE IF EXISTS `mod_competitions`;
CREATE TABLE `mod_competitions` (
  `mod_cmp_id` int NOT NULL AUTO_INCREMENT,
  `mod_cmp_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_cmp_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cmp_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_cmp_date_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `mod_cmp_date_end` datetime DEFAULT CURRENT_TIMESTAMP,
  `mod_cmp_ent_id` int DEFAULT NULL,
  `mod_cmp_status` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_cmp_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_competitions
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_competitions_accounts
-- ----------------------------
DROP TABLE IF EXISTS `mod_competitions_accounts`;
CREATE TABLE `mod_competitions_accounts` (
  `mod_cmp_cmp_id` int NOT NULL,
  `mod_cmp_acu_id` int NOT NULL,
  `mod_cmp_rel` int DEFAULT NULL,
  `mod_cmp_date_register` datetime NOT NULL,
  PRIMARY KEY (`mod_cmp_cmp_id`,`mod_cmp_acu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_competitions_accounts
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_competitions_mira
-- ----------------------------
DROP TABLE IF EXISTS `mod_competitions_mira`;
CREATE TABLE `mod_competitions_mira` (
  `mod_cmp_mira_id` int NOT NULL AUTO_INCREMENT,
  `mod_cmp_mira_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cmp_mira_lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cmp_mira_acu_id` int NOT NULL,
  `mod_cmp_mira_cumple` date DEFAULT NULL,
  `mod_cmp_mira_typeid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Tipo de identificación: CI/DNI/Otro',
  `mod_cmp_mira_num_id` varchar(44) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Número de identificación',
  `mod_cmp_mira_nacionalidad` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cmp_mira_direccion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cmp_mira_ciudad` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cmp_mira_pais` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cmp_mira_websocial` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_cmp_mira_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'individual,grupo/colectiva',
  `mod_cmp_mira_nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Nombre artístico / Nombre del colectivo y participante',
  `mod_cmp_mira_incluye` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'si-link\r\nsi-file\r\nno\r\nartista-emergente\r\n',
  `mod_cmp_mira_link_portafolio` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cmp_mira_file_portafolio` int DEFAULT NULL,
  `mod_cmp_mira_titulo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cmp_mira_tipo_proyecto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '(Fotografía, video, performance, instalación, objeto, escultura, pintura, etc.)',
  `mod_cmp_mira_sintesis` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_cmp_mira_espacio` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '(Tipología, características y dimensiones. Max. 500 caracteres)',
  `mod_cmp_mira_file_id` int DEFAULT NULL COMMENT 'Imágenes, videos y pdf complementario',
  `mod_cmp_mira_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'link video',
  `mod_cmp_mira_file_declaracion` int DEFAULT NULL,
  `mod_cmp_mira_file_ci` int DEFAULT NULL,
  `mod_cmp_mira_informacion` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_cmp_mira_date_register` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mod_cmp_mira_state` int DEFAULT NULL,
  PRIMARY KEY (`mod_cmp_mira_id`,`mod_cmp_mira_acu_id`,`mod_cmp_mira_num_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_competitions_mira
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_customers_enterprises
-- ----------------------------
DROP TABLE IF EXISTS `mod_customers_enterprises`;
CREATE TABLE `mod_customers_enterprises` (
  `mod_cen_id` int NOT NULL AUTO_INCREMENT,
  `mod_cen_cpe_id` int DEFAULT NULL,
  `mod_cen_ced_id` int DEFAULT NULL COMMENT 'customer data',
  `mod_cen_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cen_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cen_code` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cen_description` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cen_img` int DEFAULT NULL,
  `mod_cen_register_date` datetime DEFAULT NULL,
  `mod_cen_user_id` int DEFAULT NULL,
  `mod_cen_ent_id` int DEFAULT NULL,
  `mod_cen_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_cen_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_customers_enterprises
-- ----------------------------
BEGIN;
INSERT INTO `mod_customers_enterprises` VALUES (1, 0, 1, 'Wappcom', 'wappcom', 'wapp', NULL, 0, '2023-01-03 15:50:12', 1, 1, 1);
INSERT INTO `mod_customers_enterprises` VALUES (2, 0, 2, 'Eurochronos', 'eurochronos', 'euro', 'Paola Andrea Abudinen	77801315	marketing@eurochronos.com.bo', 0, '2023-01-03 18:08:14', 1, 1, 1);
INSERT INTO `mod_customers_enterprises` VALUES (3, 0, 3, 'Previsión', 'prevision', 'prevision', 'Magali Justiniano	70844811	mjustiniano@prevision.com.bo', 0, '2023-01-03 19:55:00', 1, 1, 1);
INSERT INTO `mod_customers_enterprises` VALUES (4, 0, 4, 'Cenace', 'cenace', 'cenace', 'Gabriela Roca Aguilera	76600069	gabrielaroca@upsa.edu.bo', 0, '2023-01-03 19:56:14', 1, 1, 1);
INSERT INTO `mod_customers_enterprises` VALUES (5, 0, 5, 'Casa de Oración', 'casadeoracion', 'casaoracion', 'Gabriel Rosales Jándula	75001599	gabriel.rosales.j@gmail.com', 0, '2023-01-03 20:07:04', 1, 1, 1);
INSERT INTO `mod_customers_enterprises` VALUES (6, 0, 6, 'Cabruja Films', 'cabruja', 'cabruja', 'Jose Luis Cabruja	72121870	jlcabruja@gmail.com	Daniela Gutierrez		dgcabrujafilms@gmail.com', 0, '2023-01-03 20:10:03', 1, 1, 1);
INSERT INTO `mod_customers_enterprises` VALUES (7, 0, 7, 'Francisco Arce', 'franciscoarce', 'FA001', 'Francisco Arce 76342483 farcime@gmail.com', 0, '2023-01-03 20:24:58', 1, 1, 1);
INSERT INTO `mod_customers_enterprises` VALUES (8, 0, 8, 'Chaplin Show', 'chaplinshow', 'CHS-001', '', 0, '2023-01-03 20:57:24', 1, 1, 1);
INSERT INTO `mod_customers_enterprises` VALUES (9, 0, 9, 'Candire', 'candire', 'CAN-001', 'Bernardo Daza +591 67702930 bdaza@candire.net', 0, '2023-06-01 14:53:39', 1, 1, 1);
INSERT INTO `mod_customers_enterprises` VALUES (10, 0, 10, 'Edadsa', 'edadsa', 'EDA-001', 'Carlos Lopez clopez@eldia.com.bo  +591 65060735', 0, '2023-09-06 16:45:41', 1, 1, 1);
INSERT INTO `mod_customers_enterprises` VALUES (11, 0, 11, 'Victoria', 'victoria', 'VIC-001', 'Luis Fernando Villa	3 348-7070 int 1610 jefesistemas@panaderiavictoria.com', 0, '2023-09-19 09:05:47', 1, 1, 1);
INSERT INTO `mod_customers_enterprises` VALUES (12, 0, 12, 'Networks Solitions', 'ns', 'ns-1', '', 0, '2024-09-18 15:26:46', 1, 1, 1);
INSERT INTO `mod_customers_enterprises` VALUES (13, 0, NULL, 'Prueba', 'prueba', 'pru2', '', 0, '2024-11-14 12:29:12', 1, 1, 1);
INSERT INTO `mod_customers_enterprises` VALUES (14, 0, NULL, 'prueba 2', 'prueba2', '913864', '', 0, '2024-11-14 12:31:49', 1, 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for mod_customers_enterprises_data
-- ----------------------------
DROP TABLE IF EXISTS `mod_customers_enterprises_data`;
CREATE TABLE `mod_customers_enterprises_data` (
  `mod_cen_data_id` int NOT NULL AUTO_INCREMENT,
  `mod_cen_data_cen_id` int NOT NULL,
  `mod_cen_data_business_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cen_data_nit` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cen_data_priority` int DEFAULT NULL,
  `mod_cen_data_type_contract` int DEFAULT NULL,
  `mod_cen_data_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`mod_cen_data_id`,`mod_cen_data_cen_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_customers_enterprises_data
-- ----------------------------
BEGIN;
INSERT INTO `mod_customers_enterprises_data` VALUES (14, 14, 'pruienba123', '12312321312', NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for mod_customers_persons
-- ----------------------------
DROP TABLE IF EXISTS `mod_customers_persons`;
CREATE TABLE `mod_customers_persons` (
  `mod_cpe_id` int NOT NULL AUTO_INCREMENT,
  `mod_cpe_acu_id` int DEFAULT NULL,
  `mod_cpe_plan_id` int DEFAULT NULL,
  `mod_cpe_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpe_lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpe_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpe_code` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpe_phone` int DEFAULT NULL,
  `mod_cpe_ci` int DEFAULT NULL,
  `mod_cpe_ci_complemento` int DEFAULT NULL,
  `mod_cpe_adder` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpe_register_date` datetime NOT NULL,
  `mod_cpe_user_id` int DEFAULT NULL COMMENT 'usuario que lo registro\r\n',
  `mod_cpe_ent_id` int DEFAULT NULL,
  `mod_cpe_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_cpe_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_customers_persons
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_customers_persons_data
-- ----------------------------
DROP TABLE IF EXISTS `mod_customers_persons_data`;
CREATE TABLE `mod_customers_persons_data` (
  `mod_cpdt_id` int NOT NULL AUTO_INCREMENT,
  `mod_cpdt_cep_id` int NOT NULL,
  `mod_cpdt_cep_nit` bigint NOT NULL,
  `mod_cpdt_cep_business_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpdt_cep_type_contribuyente` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpdt_cep_type_oficio` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpdt_cep_priority` int DEFAULT NULL,
  `mod_cpdt_cep_type_contract` int DEFAULT NULL,
  `mod_cpdt_cep_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_cpdt_cep_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpdt_cep_pw` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`mod_cpdt_id`,`mod_cpdt_cep_id`,`mod_cpdt_cep_nit`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_customers_persons_data
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_departments
-- ----------------------------
DROP TABLE IF EXISTS `mod_departments`;
CREATE TABLE `mod_departments` (
  `mod_dep_id` int NOT NULL AUTO_INCREMENT,
  `mod_dep_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_dep_parent_id` int NOT NULL,
  `mod_dep_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_dep_ent_id` int NOT NULL,
  `mod_dep_order` int NOT NULL,
  `mod_dep_state` int NOT NULL,
  PRIMARY KEY (`mod_dep_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_departments
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_discount
-- ----------------------------
DROP TABLE IF EXISTS `mod_discount`;
CREATE TABLE `mod_discount` (
  `mod_dis_id` int NOT NULL AUTO_INCREMENT,
  `mod_dis_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_dis_detail` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_dis_rate` int DEFAULT NULL,
  `mod_dis_start_date` datetime DEFAULT NULL,
  `mod_dis_end_date` datetime DEFAULT NULL,
  `mod_dis_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_dis_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_discount
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_discount_products
-- ----------------------------
DROP TABLE IF EXISTS `mod_discount_products`;
CREATE TABLE `mod_discount_products` (
  `mod_dis_prod_dis_id` int NOT NULL,
  `mod_dis_prod_prod_id` int NOT NULL,
  `mod_dis_prod_orden` int NOT NULL,
  PRIMARY KEY (`mod_dis_prod_dis_id`,`mod_dis_prod_prod_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_discount_products
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_events
-- ----------------------------
DROP TABLE IF EXISTS `mod_events`;
CREATE TABLE `mod_events` (
  `mod_eve_id` int NOT NULL AUTO_INCREMENT,
  `mod_eve_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_eve_register_date` datetime DEFAULT NULL,
  `mod_eve_user_id` int DEFAULT NULL,
  `mod_eve_init_date` datetime DEFAULT NULL,
  `mod_eve_end_date` datetime DEFAULT NULL,
  `mod_eve_img_small` int DEFAULT NULL,
  `mod_eve_img_medium` int DEFAULT NULL,
  `mod_eve_img_large` int DEFAULT NULL,
  `mod_eve_seats` int DEFAULT NULL,
  `mod_eve_hall` int DEFAULT NULL,
  `mod_eve_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_eve_cost_seat` decimal(21,2) DEFAULT NULL,
  `mod_eve_coin` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Bs',
  `mod_eve_ent_id` int DEFAULT NULL,
  `mod_eve_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_eve_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_events
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_events_halls
-- ----------------------------
DROP TABLE IF EXISTS `mod_events_halls`;
CREATE TABLE `mod_events_halls` (
  `mod_eve_hall_id` int NOT NULL AUTO_INCREMENT,
  `mod_eve_hall_ent_id` int NOT NULL,
  `mod_eve_hall_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_hall_details` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_eve_hall_url_draw_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_hall_fn_js_draw` varchar(440) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_hall_num_tables` int DEFAULT NULL,
  `mod_eve_hall_num_seats` int DEFAULT NULL,
  `mod_eve_hall_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_hall_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_hall_country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_hall_coord` varchar(440) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_hall_user_id` int DEFAULT NULL,
  `mod_eve_hall_register_date` datetime DEFAULT NULL,
  `mod_eve_hall_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_eve_hall_id`,`mod_eve_hall_ent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_events_halls
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_events_options
-- ----------------------------
DROP TABLE IF EXISTS `mod_events_options`;
CREATE TABLE `mod_events_options` (
  `mod_eve_option_id` int NOT NULL AUTO_INCREMENT,
  `mod_eve_option_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_option_value` int DEFAULT NULL,
  `mod_eve_option_autoload` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`mod_eve_option_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_events_options
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_events_tickets
-- ----------------------------
DROP TABLE IF EXISTS `mod_events_tickets`;
CREATE TABLE `mod_events_tickets` (
  `mod_eve_tck_id` int NOT NULL AUTO_INCREMENT,
  `mod_eve_tck_eve_id` int DEFAULT NULL,
  `mod_eve_tck_hall_id` int DEFAULT NULL,
  `mod_eve_tck_table_id` int DEFAULT NULL COMMENT 'mesa id',
  `mod_eve_tck_seat_id` int DEFAULT NULL,
  `mod_eve_tck_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_tck_ref` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'reference',
  `mod_eve_tck_reserve_id` int NOT NULL,
  `mod_eve_tck_acp_id` int DEFAULT NULL COMMENT 'mod_accounting_plan id\r\n',
  `mod_eve_tck_cpe_id` int DEFAULT NULL,
  `mod_eve_tck_date_register` datetime DEFAULT NULL,
  `mod_eve_tck_cost` decimal(22,2) DEFAULT NULL,
  `mod_eve_tck_coin` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_tck_user_id` int NOT NULL,
  `mod_eve_tck_pay_tx_id` int DEFAULT NULL COMMENT 'mod_pay_tx_tck_id of mod_payments_tx_tickets',
  `mod_eve_tck_date_pay_register` datetime DEFAULT NULL,
  `mod_eve_tck_txr` int DEFAULT NULL COMMENT 'id_transaccion pasarela',
  `mod_eve_tck_state` int NOT NULL DEFAULT '0' COMMENT '// State 0:Libre, 1.temp 2:Recervado, 3: Vendido, 4:Anulado, 5:Cancelado  Null : sin estado ',
  PRIMARY KEY (`mod_eve_tck_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_events_tickets
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_external_docs
-- ----------------------------
DROP TABLE IF EXISTS `mod_external_docs`;
CREATE TABLE `mod_external_docs` (
  `mod_edc_id` int NOT NULL AUTO_INCREMENT,
  `mod_edc_site` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_user_id` int DEFAULT NULL,
  `mod_edc_approver_id` int DEFAULT NULL,
  `mod_edc_tpl_id` int DEFAULT NULL,
  `mod_edc_date_register` datetime DEFAULT NULL,
  `mod_edc_key` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_clsi_id` int DEFAULT NULL COMMENT 'mod_classification_info',
  `mod_edc_code` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_pw` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_edc_flags` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_edc_state` int DEFAULT NULL,
  PRIMARY KEY (`mod_edc_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_external_docs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_external_docs_options
-- ----------------------------
DROP TABLE IF EXISTS `mod_external_docs_options`;
CREATE TABLE `mod_external_docs_options` (
  `mod_edc_opt_id` int NOT NULL AUTO_INCREMENT,
  `mod_edc_opt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_edc_opt_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_edc_opt_ent_id` int NOT NULL,
  `mod_edc_opt_autoload` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`mod_edc_opt_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_external_docs_options
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_external_docs_page_design
-- ----------------------------
DROP TABLE IF EXISTS `mod_external_docs_page_design`;
CREATE TABLE `mod_external_docs_page_design` (
  `mod_edc_pd_id` int NOT NULL AUTO_INCREMENT,
  `mod_edc_pd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_pd_url` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_pd_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_edc_pd_ent_id` int DEFAULT NULL,
  `mod_edc_pd_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_edc_pd_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_external_docs_page_design
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_external_docs_templates
-- ----------------------------
DROP TABLE IF EXISTS `mod_external_docs_templates`;
CREATE TABLE `mod_external_docs_templates` (
  `mod_edc_tpl_id` int NOT NULL AUTO_INCREMENT,
  `mod_edc_tpl_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_edc_tpl_description` varchar(445) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_tpl_code` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_tpl_tags` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_tpl_type_id` int DEFAULT NULL COMMENT 'mod_external_docs_types',
  `mod_edc_tpl_pd_id` int DEFAULT NULL COMMENT 'mod_external_docs_page_design',
  `mod_edc_tpl_body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_edc_tpl_register_user_id` int DEFAULT NULL,
  `mod_edc_tpl_register_date` datetime DEFAULT NULL,
  `mod_edc_tpl_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_edc_tpl_ent_id` int DEFAULT NULL,
  `mod_edc_tpl_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_edc_tpl_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_external_docs_templates
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_external_docs_types
-- ----------------------------
DROP TABLE IF EXISTS `mod_external_docs_types`;
CREATE TABLE `mod_external_docs_types` (
  `mod_edc_type_id` int NOT NULL AUTO_INCREMENT,
  `mod_edc_type_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_type_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_type_parent_id` int NOT NULL,
  `mod_edc_type_ent_id` int NOT NULL,
  `mod_edc_type_color` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_edc_type_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_edc_type_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_external_docs_types
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_ganador_ganaderos
-- ----------------------------
DROP TABLE IF EXISTS `mod_ganador_ganaderos`;
CREATE TABLE `mod_ganador_ganaderos` (
  `mod_ggd_id` int NOT NULL AUTO_INCREMENT,
  `mod_ggd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_ggd_acu_id` int DEFAULT NULL,
  `mod_ggd_perfil` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_ggd_img_id` int DEFAULT NULL,
  `mod_ggd_dial` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '+591',
  `mod_ggd_celular` int DEFAULT NULL,
  `mod_ggd_user_id` int DEFAULT NULL,
  `mod_ggd_date_register` datetime DEFAULT NULL,
  `mod_ggd_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_ggd_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_ganador_ganaderos
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_ganador_ganaderos_propiedades
-- ----------------------------
DROP TABLE IF EXISTS `mod_ganador_ganaderos_propiedades`;
CREATE TABLE `mod_ganador_ganaderos_propiedades` (
  `mod_ggp_ggd_id` int NOT NULL COMMENT 'ganaderos id',
  `mod_ggp_gpr_id` int NOT NULL COMMENT 'propiedades id',
  `mod_ggp_order` int NOT NULL,
  PRIMARY KEY (`mod_ggp_ggd_id`,`mod_ggp_gpr_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_ganador_ganaderos_propiedades
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_ganador_propiedades
-- ----------------------------
DROP TABLE IF EXISTS `mod_ganador_propiedades`;
CREATE TABLE `mod_ganador_propiedades` (
  `mod_gpr_id` int NOT NULL AUTO_INCREMENT,
  `mod_gpr_name` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_gpr_description` varchar(550) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_gpr_ubicacion` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_gpr_coord` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_gpr_cover_id` int DEFAULT NULL COMMENT 'imagen portada',
  `mod_gpr_video` int DEFAULT NULL,
  `mod_gpr_date_register` int NOT NULL,
  `mod_gpr_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_gpr_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_ganador_propiedades
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_ganador_propiedades_files
-- ----------------------------
DROP TABLE IF EXISTS `mod_ganador_propiedades_files`;
CREATE TABLE `mod_ganador_propiedades_files` (
  `mod_gpf_file_id` int NOT NULL,
  `mod_gpf_gpr_id` int NOT NULL,
  `mod_gpf_order` int NOT NULL,
  PRIMARY KEY (`mod_gpf_file_id`,`mod_gpf_gpr_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_ganador_propiedades_files
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_ganador_propiedades_images
-- ----------------------------
DROP TABLE IF EXISTS `mod_ganador_propiedades_images`;
CREATE TABLE `mod_ganador_propiedades_images` (
  `mod_gpi_img_id` int NOT NULL,
  `mod_gpi_gpr_id` int NOT NULL,
  `mod_gpi_order` int NOT NULL,
  PRIMARY KEY (`mod_gpi_img_id`,`mod_gpi_gpr_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_ganador_propiedades_images
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_ganador_proyectos
-- ----------------------------
DROP TABLE IF EXISTS `mod_ganador_proyectos`;
CREATE TABLE `mod_ganador_proyectos` (
  `mod_gp_id` int NOT NULL AUTO_INCREMENT,
  `mod_gp_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_gp_description` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'explicación corta del proyecto',
  `mod_gp_details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT 'explicación larga del proyecto',
  `mod_gp_ggd_id` int NOT NULL DEFAULT '0' COMMENT 'ganadero id',
  `mod_gp_gpr_id` int NOT NULL COMMENT 'propiedad id',
  `mod_gp_rentabilidad` decimal(10,2) DEFAULT NULL COMMENT '10,5%',
  `mod_gp_periodo` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_gp_date_end_captacion` datetime DEFAULT NULL,
  `mod_gp_ticket_inversion` int DEFAULT '500' COMMENT 'ticket de inversión inicial de 500.00Bs',
  `mod_gp_size_lote` int NOT NULL DEFAULT '0' COMMENT 'numero de cabezas',
  `mod_gp_recaudacion_prevista` int NOT NULL DEFAULT '0' COMMENT 'multiplos de 500',
  `mod_gp_date_register` datetime DEFAULT NULL,
  `mod_gp_user_id` int NOT NULL DEFAULT '0',
  `mod_gp_state` int NOT NULL DEFAULT '0' COMMENT '0. No activo\r\n1. Disponible\r\n2. En ejecución\r\n3. Finalizado\r\n4. Cancelado\r\n5. De Baja',
  PRIMARY KEY (`mod_gp_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_ganador_proyectos
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_inventory_options
-- ----------------------------
DROP TABLE IF EXISTS `mod_inventory_options`;
CREATE TABLE `mod_inventory_options` (
  `mod_inv_opt_id` int NOT NULL AUTO_INCREMENT,
  `mod_inv_opt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_inv_opt_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_inv_opt_autoload` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_inv_opt_ent_id` int DEFAULT NULL,
  PRIMARY KEY (`mod_inv_opt_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_inventory_options
-- ----------------------------
BEGIN;
INSERT INTO `mod_inventory_options` VALUES (1, 'routeCategoryId', '3', '', 1);
COMMIT;

-- ----------------------------
-- Table structure for mod_inventory_units
-- ----------------------------
DROP TABLE IF EXISTS `mod_inventory_units`;
CREATE TABLE `mod_inventory_units` (
  `mod_inv_unt_id` int NOT NULL AUTO_INCREMENT,
  `mod_inv_unt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_inv_unt_summary` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_inv_unt_vars` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_inv_unt_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_inv_unt_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_inventory_units
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_invoices
-- ----------------------------
DROP TABLE IF EXISTS `mod_invoices`;
CREATE TABLE `mod_invoices` (
  `mod_inv_id` int NOT NULL AUTO_INCREMENT,
  `mod_inv_px_id` int NOT NULL COMMENT 'Id del pago relacionado',
  `mod_inv_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `mod_inv_total` decimal(10,2) NOT NULL,
  `mod_inv_status` varchar(255) NOT NULL DEFAULT 'pending',
  `mod_inv_register_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `mod_inv_user_id` int NOT NULL,
  `mod_inv_ent_id` int NOT NULL,
  `mod_inv_custom_data` json DEFAULT NULL,
  `mod_inv_class` varchar(255) DEFAULT NULL,
  `mod_inv_custom_id` varchar(255) DEFAULT NULL,
  `mod_inv_state` int DEFAULT '0',
  PRIMARY KEY (`mod_inv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of mod_invoices
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_jobtitle
-- ----------------------------
DROP TABLE IF EXISTS `mod_jobtitle`;
CREATE TABLE `mod_jobtitle` (
  `mod_jbt_id` int NOT NULL AUTO_INCREMENT,
  `mod_jbt_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_jbt_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_jbt_parent_id` int NOT NULL,
  `mod_jpt_duties_manual` int NOT NULL,
  `mod_jbt_ent_id` int NOT NULL,
  `mod_jbt_order` int NOT NULL,
  `mod_jbt_state` int NOT NULL,
  PRIMARY KEY (`mod_jbt_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_jobtitle
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_kardex
-- ----------------------------
DROP TABLE IF EXISTS `mod_kardex`;
CREATE TABLE `mod_kardex` (
  `mod_kdx_id` int NOT NULL AUTO_INCREMENT,
  `mod_kdx_user_id` int NOT NULL,
  `mod_kdx_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_fathers_lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_mothers_lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_ent_id` int DEFAULT NULL,
  `mod_kdx_entry_date` date DEFAULT NULL,
  `mod_kdx_ci` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_ci_extension` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_ci_expiration_date` date DEFAULT NULL,
  `mod_kdx_drivers_licence_expiration_date` date DEFAULT NULL,
  `mod_kdx_birth_date` date DEFAULT NULL,
  `mod_kdx_nationality` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_birth_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_sex` int DEFAULT NULL COMMENT '2 Masculino, 1 Femenino, 3. Otros',
  `mod_kdx_civil_status` int DEFAULT NULL COMMENT '0.single 1.married 2.divorced 3.widowed',
  `mod_kdx_dates_contact` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_kdx_phone_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_phone_corp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_phone_corp_ext` int DEFAULT NULL,
  `mod_kdx_personal_mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_corp_mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_personal_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_corp_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_coordinates` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_nro_cns` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_nro_afp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_afp` int DEFAULT NULL,
  `mod_kdx_shirt_size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_pant_size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_boot_size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_blood_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_removal_date` date DEFAULT NULL,
  `mod_kdx_cod_sap` int DEFAULT NULL,
  `mod_kdx_cv_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_workhours` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_trash` int DEFAULT NULL,
  `mod_kdx_state` int DEFAULT '0',
  PRIMARY KEY (`mod_kdx_id`,`mod_kdx_user_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=10012 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_kardex
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_kardex_jobtitle
-- ----------------------------
DROP TABLE IF EXISTS `mod_kardex_jobtitle`;
CREATE TABLE `mod_kardex_jobtitle` (
  `mod_kdx_jbt_jbt_id` int NOT NULL,
  `mod_kdx_jbt_kdx_id` int NOT NULL,
  `mod_kdx_jbt_ent_id` int NOT NULL,
  `mod_kdx_jbt_order` int NOT NULL,
  PRIMARY KEY (`mod_kdx_jbt_jbt_id`,`mod_kdx_jbt_kdx_id`,`mod_kdx_jbt_ent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_kardex_jobtitle
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_leagues
-- ----------------------------
DROP TABLE IF EXISTS `mod_leagues`;
CREATE TABLE `mod_leagues` (
  `mod_lg_id` int NOT NULL AUTO_INCREMENT,
  `mod_lg_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_lg_description` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_lg_sport` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_lg_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_lg_ent_id` int DEFAULT NULL,
  `mod_lg_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_lg_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_leagues
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_leagues_divisions
-- ----------------------------
DROP TABLE IF EXISTS `mod_leagues_divisions`;
CREATE TABLE `mod_leagues_divisions` (
  `mod_lg_div_id` int NOT NULL AUTO_INCREMENT,
  `mod_lg_div_lg_id` int DEFAULT NULL COMMENT 'Id league',
  `mod_lg_div_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_lg_div_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_lg_div_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_lg_div_state` int DEFAULT NULL,
  PRIMARY KEY (`mod_lg_div_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_leagues_divisions
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_leagues_divisions_teams
-- ----------------------------
DROP TABLE IF EXISTS `mod_leagues_divisions_teams`;
CREATE TABLE `mod_leagues_divisions_teams` (
  `mod_lg_div_tm_div_id` int NOT NULL,
  `mod_lg_div_tm_tm_id` int NOT NULL,
  `mod_lg_div_tm_order` int DEFAULT NULL,
  PRIMARY KEY (`mod_lg_div_tm_div_id`,`mod_lg_div_tm_tm_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_leagues_divisions_teams
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_logistics_distribution_move
-- ----------------------------
DROP TABLE IF EXISTS `mod_logistics_distribution_move`;
CREATE TABLE `mod_logistics_distribution_move` (
  `mod_log_dtz_id` int NOT NULL AUTO_INCREMENT,
  `mod_log_dtz_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_log_dtz_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_log_dtz_perimetrer` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_log_dtz_cost` decimal(20,2) NOT NULL,
  `mod_log_dtz_state` int DEFAULT '0',
  PRIMARY KEY (`mod_log_dtz_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_logistics_distribution_move
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_logistics_distribution_zone
-- ----------------------------
DROP TABLE IF EXISTS `mod_logistics_distribution_zone`;
CREATE TABLE `mod_logistics_distribution_zone` (
  `mod_log_dtz_id` int NOT NULL AUTO_INCREMENT,
  `mod_log_dtz_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_log_dtz_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_log_dtz_perimetrer` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_log_dtz_cost` decimal(20,2) NOT NULL,
  `mod_log_dtz_state` int DEFAULT '0',
  PRIMARY KEY (`mod_log_dtz_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_logistics_distribution_zone
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_make
-- ----------------------------
DROP TABLE IF EXISTS `mod_make`;
CREATE TABLE `mod_make` (
  `mod_mak_id` int NOT NULL AUTO_INCREMENT,
  `mod_mak_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_mak_description` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_mak_img_id` int NOT NULL,
  `mod_mak_state` int NOT NULL,
  PRIMARY KEY (`mod_mak_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_make
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_management
-- ----------------------------
DROP TABLE IF EXISTS `mod_management`;
CREATE TABLE `mod_management` (
  `mod_mag_id` int NOT NULL AUTO_INCREMENT,
  `mod_mag_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_mag_description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_mag_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_mag_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_management
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_organization_chart
-- ----------------------------
DROP TABLE IF EXISTS `mod_organization_chart`;
CREATE TABLE `mod_organization_chart` (
  `mod_orc_id` int NOT NULL AUTO_INCREMENT,
  `mod_orc_position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_orc_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_orc_functions` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_orc_doc_id` int DEFAULT NULL COMMENT 'rel functions',
  `mod_orc_location` int NOT NULL,
  `mod_orc_reports_to` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'id,id,id',
  `mod_orc_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_orc_user_id` int NOT NULL COMMENT 'id register',
  `mod_orc_date_register` datetime DEFAULT NULL,
  `mod_orc_ent_id` int DEFAULT NULL,
  `mod_orc_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_orc_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_organization_chart
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_organization_chart_rel
-- ----------------------------
DROP TABLE IF EXISTS `mod_organization_chart_rel`;
CREATE TABLE `mod_organization_chart_rel` (
  `mod_orc_rel_orc_id` int NOT NULL,
  `mod_orc_rel_user_id` int NOT NULL,
  `mod_orc_rel_kdx_id` int DEFAULT NULL,
  `mod_orc_rel_date_register` date DEFAULT NULL,
  `mod_orc_rel_order` int DEFAULT NULL,
  `mod_org_rel_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_orc_rel_orc_id`,`mod_orc_rel_user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_organization_chart_rel
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_pay_tx_suscriptions
-- ----------------------------
DROP TABLE IF EXISTS `mod_pay_tx_suscriptions`;
CREATE TABLE `mod_pay_tx_suscriptions` (
  `mod_pay_tx_sbs_id` bigint NOT NULL AUTO_INCREMENT,
  `mod_pay_tx_sbs_acu_id` bigint DEFAULT NULL COMMENT 'cuenta de usuario',
  `mod_pay_tx_sbs_cpe_id` bigint DEFAULT NULL,
  `mod_pay_tx_sbs_pay` decimal(22,2) DEFAULT NULL,
  `mod_pay_tx_sbs_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_pay_tx_sbs_coin` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_pay_tx_sbs_rs` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'razon social, ci',
  `mod_pay_tx_sbs_nit` bigint DEFAULT NULL,
  `mod_pay_tx_invoice` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'no-facturado' COMMENT 'facturado, no-facturado',
  `mod_pay_tx_num_invoice` bigint DEFAULT '0',
  `mod_pay_tx_sbs_mode` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_pay_tx_sbs_txr` bigint DEFAULT NULL COMMENT 'id transaccion',
  `mod_pay_tx_sbs_date_pay_register` datetime DEFAULT CURRENT_TIMESTAMP,
  `mod_pay_tx_sbs_callback` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_pay_tx_sbs_mail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_pay_tx_sbs_user_id` bigint DEFAULT NULL,
  `mod_pay_tx_sbs_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_pay_tx_sbs_return` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_pay_tx_sbs_register_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mod_pay_tx_sbs_qr_state` int DEFAULT '0' COMMENT '0. sin usar 1. usado',
  `mod_pay_tx_sbs_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_pay_tx_sbs_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_pay_tx_suscriptions
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_pay_tx_temp
-- ----------------------------
DROP TABLE IF EXISTS `mod_pay_tx_temp`;
CREATE TABLE `mod_pay_tx_temp` (
  `mod_pay_tx_tmp_id` int NOT NULL AUTO_INCREMENT,
  `mod_pay_tx_tmp_acu_id` int NOT NULL,
  `mod_pay_tx_tmp_cpe_id` int NOT NULL,
  `mod_pay_tx_tmp_code` int NOT NULL,
  `mod_pay_tx_tmp_entitie` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'banco, pasarela',
  `mod_pay_tx_tmp_ent_id` int NOT NULL,
  `mod_pay_tx_tmp_mode` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_pay_tx_tmp_register_date` datetime NOT NULL,
  `mod_pay_tx_tmp_nit` bigint NOT NULL,
  `mod_pay_tx_tmp_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_pay_tx_tmp_response` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_pay_tx_tmp_state` int NOT NULL DEFAULT '0' COMMENT '0. agregado 1.pagado 2. con error \r\n',
  PRIMARY KEY (`mod_pay_tx_tmp_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_pay_tx_temp
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_pay_tx_tickets
-- ----------------------------
DROP TABLE IF EXISTS `mod_pay_tx_tickets`;
CREATE TABLE `mod_pay_tx_tickets` (
  `mod_pay_tx_tck_id` int NOT NULL AUTO_INCREMENT,
  `mod_pay_tx_tck_acp_id` int DEFAULT NULL,
  `mod_pay_tx_tck_cpe_id` int NOT NULL,
  `mod_pay_tx_tck_eve_id` int NOT NULL,
  `mod_pay_tx_tck_reserve_id` int NOT NULL,
  `mod_pay_tx_tck_pay` decimal(22,2) NOT NULL DEFAULT '0.00',
  `mod_pay_tx_tck_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_pay_tx_tck_coin` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_pay_tx_tck_rs` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'razon social',
  `mod_pay_tx_tck_nit` int DEFAULT NULL,
  `mod_pay_tx_tck_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_pay_tx_tck_txr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'id transaccion',
  `mod_pay_tx_tck_date_pay_register` datetime NOT NULL,
  `mod_pay_tx_tck_callback` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_pay_tx_tck_mail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_pay_tx_tck_user_id` int NOT NULL DEFAULT '0',
  `mod_pay_tx_tck_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_pay_tx_tck_return` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_pay_tx_tck_register_date` datetime DEFAULT NULL,
  `mod_pay_tx_tck_state` int NOT NULL DEFAULT '0' COMMENT '0. Iniciado, 2.registrado, 3.Active/pagado, 4.Anulado 5.Delete',
  PRIMARY KEY (`mod_pay_tx_tck_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_pay_tx_tickets
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_persons
-- ----------------------------
DROP TABLE IF EXISTS `mod_persons`;
CREATE TABLE `mod_persons` (
  `mod_person_id` int NOT NULL AUTO_INCREMENT COMMENT 'Id único de la persona',
  `mod_person_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Nombre de la persona',
  `mod_person_lastname_father` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Apellido paterno de la persona',
  `mod_person_lastname_mother` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Apellido materno de la persona',
  `mod_person_email` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Correo electrónico de la persona',
  `mod_person_phone` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Teléfono de contacto',
  `mod_person_ent_id` int NOT NULL COMMENT 'Id de la entidad asociada',
  `mod_person_type_id` int NOT NULL COMMENT 'Id del tipo de persona',
  `mod_person_state` int DEFAULT '0' COMMENT 'Estado lógico de la persona (0: inactivo, 1: activo)',
  `mod_person_comments` text COLLATE utf8mb4_general_ci COMMENT 'Campo para comentarios adicionales sobre la persona',
  `mod_person_metadata` json DEFAULT NULL COMMENT 'Campo para datos adicionales en formato JSON',
  `mod_person_custom_class` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Clase CSS personalizada asociada a la persona',
  `mod_person_custom_id` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'ID personalizado asociado a la persona',
  `mod_person_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación',
  `mod_person_updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización',
  PRIMARY KEY (`mod_person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_persons
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_persons_types
-- ----------------------------
DROP TABLE IF EXISTS `mod_persons_types`;
CREATE TABLE `mod_persons_types` (
  `mod_person_type_id` int NOT NULL AUTO_INCREMENT COMMENT 'Id único del tipo de persona',
  `mod_person_type_code` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Código único para identificar el tipo',
  `mod_person_type_name` varchar(100) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Nombre descriptivo del tipo',
  `mod_person_type_description` text COLLATE utf8mb4_general_ci COMMENT 'Descripción detallada del tipo',
  `mod_person_type_metadata` json DEFAULT NULL COMMENT 'Datos adicionales en formato JSON',
  `mod_person_type_state` int DEFAULT '1' COMMENT 'Estado lógico del tipo (0: inactivo, 1: activo)',
  PRIMARY KEY (`mod_person_type_id`),
  UNIQUE KEY `mod_person_type_code_UNIQUE` (`mod_person_type_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_persons_types
-- ----------------------------
BEGIN;
INSERT INTO `mod_persons_types` VALUES (1, 'LEAD', 'Lead', 'Posible cliente potencial', NULL, 1);
INSERT INTO `mod_persons_types` VALUES (2, 'CUSTOMER', 'Cliente', 'Cliente activo', NULL, 1);
INSERT INTO `mod_persons_types` VALUES (3, 'PROVIDER', 'Proveedor', 'Proveedor de servicios o productos', NULL, 1);
COMMIT;

-- ----------------------------
-- Table structure for mod_places
-- ----------------------------
DROP TABLE IF EXISTS `mod_places`;
CREATE TABLE `mod_places` (
  `mod_plc_id` int NOT NULL AUTO_INCREMENT,
  `mod_plc_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_tags` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_json` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_plc_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ej: Av. 3 pasos al frente, Calle Isoso #34 Edificio Lourdes Dep. 54',
  `mod_plc_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_celular_whatsapp` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_info` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_img` int NOT NULL,
  `mod_plc_profile_icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_main_coord` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_perimeter` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_map_icon` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_content` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_acu_id` int NOT NULL DEFAULT '0',
  `mod_plc_cont_id` int NOT NULL DEFAULT '0' COMMENT 'contenido_id',
  `mod_plc_status` int NOT NULL COMMENT 'the situation at a particular time during a process.',
  `mod_plc_ranking` int NOT NULL DEFAULT '5',
  `mod_plc_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_map_ubication` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_24h` int NOT NULL,
  `mod_plc_ent_id` int DEFAULT NULL,
  `mod_plc_user_id` int DEFAULT NULL,
  `mod_plc_state` int NOT NULL,
  PRIMARY KEY (`mod_plc_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_places
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_places_accounts_ads
-- ----------------------------
DROP TABLE IF EXISTS `mod_places_accounts_ads`;
CREATE TABLE `mod_places_accounts_ads` (
  `mod_plc_aca_aca_id` int NOT NULL,
  `mod_plc_aca_plc_id` int NOT NULL,
  `mod_plc_aca_date_register` datetime NOT NULL,
  PRIMARY KEY (`mod_plc_aca_aca_id`,`mod_plc_aca_plc_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_places_accounts_ads
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_places_categorys
-- ----------------------------
DROP TABLE IF EXISTS `mod_places_categorys`;
CREATE TABLE `mod_places_categorys` (
  `mod_plc_cat_plc_id` int NOT NULL,
  `mod_plc_cat_cat_id` int NOT NULL,
  `mod_plc_cat_order` int NOT NULL,
  PRIMARY KEY (`mod_plc_cat_plc_id`,`mod_plc_cat_cat_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_places_categorys
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_prices_list
-- ----------------------------
DROP TABLE IF EXISTS `mod_prices_list`;
CREATE TABLE `mod_prices_list` (
  `mod_prl_id` int NOT NULL AUTO_INCREMENT,
  `mod_prl_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prl_description` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prl_user_id` int DEFAULT NULL,
  `mod_prl_base_list` int DEFAULT NULL COMMENT 'lista base id\r\n\r\n',
  `mod_prl_factor` float(11,3) DEFAULT '1.000',
  `mod_prl_rounding_method` int NOT NULL COMMENT '0. sin *  1: si el siguiente dígito hacia la derecha después del último que desea conservarse es menor a 5, entonces el último no debe ser modificado. Por ejemplo: 8,453 se convertiría en 8,45;\r\n\r\n*2: en el caso opuesto al anterior, cuando el dígito siguiente al límite es mayor a 5, el último se debe incrementar en una unidad. Por ejemplo: 8,459 se convertiría en 8,46;\r\n\r\n*3: si un 5 sigue al último dígito que desea conservarse y después del 5 hay al menos un número diferente de 0, el último se debe incrementar en una unidad. Por ejemplo: 6,345070 se convertiría en 6,35;\r\n\r\n*4 si el último dígito deseado es un número par y a su derecha hay un 5 como dígito final o seguido de ceros, entonces no se realizan más cambios que el mero truncamiento. Por ejemplo, 4,32500 y 4,325 pasarían a ser 4,32;\r\n\r\n*\r\n5: de manera opuesta a la regla anterior, si el último dígito requerido es un número impar, entonces debemos aumentarlo en una unidad. Por ejemplo: 4,31500 y 4,315 se convertirían en 4,32. \r\n',
  `mod_prl_register_date` datetime NOT NULL,
  `mod_prl_ent_id` int DEFAULT NULL,
  `mod_prl_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_prl_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_prices_list
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_producers
-- ----------------------------
DROP TABLE IF EXISTS `mod_producers`;
CREATE TABLE `mod_producers` (
  `mod_prd_id` int NOT NULL AUTO_INCREMENT,
  `mod_prd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prd_description` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prd_img_id` int NOT NULL,
  `mod_prd_state` int NOT NULL,
  PRIMARY KEY (`mod_prd_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_producers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_products
-- ----------------------------
DROP TABLE IF EXISTS `mod_products`;
CREATE TABLE `mod_products` (
  `mod_prod_id` int NOT NULL AUTO_INCREMENT,
  `mod_prod_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pathurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_description` varchar(445) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_tags` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_img` int DEFAULT NULL,
  `mod_prod_code` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_record_date` datetime DEFAULT NULL,
  `mod_prod_type` int NOT NULL DEFAULT '0',
  `mod_prod_ent_id` int DEFAULT '0',
  `mod_prod_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_prod_id`) USING BTREE,
  FULLTEXT KEY `mod_prod_dates` (`mod_prod_name`,`mod_prod_pathurl`,`mod_prod_tags`,`mod_prod_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_products
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_products_categorys
-- ----------------------------
DROP TABLE IF EXISTS `mod_products_categorys`;
CREATE TABLE `mod_products_categorys` (
  `mod_prod_cat_prod_id` int NOT NULL,
  `mod_prod_cat_cat_id` int NOT NULL,
  `mod_prod_cat_ent_id` int NOT NULL,
  `mod_prod_cat_order` int NOT NULL,
  PRIMARY KEY (`mod_prod_cat_prod_id`,`mod_prod_cat_cat_id`,`mod_prod_cat_ent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_products_categorys
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_products_clothing_store
-- ----------------------------
DROP TABLE IF EXISTS `mod_products_clothing_store`;
CREATE TABLE `mod_products_clothing_store` (
  `mod_prod_cst_id` int NOT NULL AUTO_INCREMENT,
  `mod_prod_cst_prod_id` int NOT NULL,
  `mod_prod_cst_size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'sx,l,m,l',
  `mod_prod_cst_codebar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_cst_contents` int NOT NULL,
  `mod_prod_cst_colors` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT 'json',
  `mod_prop_cst_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`mod_prod_cst_id`,`mod_prod_cst_prod_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_products_clothing_store
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_products_colors
-- ----------------------------
DROP TABLE IF EXISTS `mod_products_colors`;
CREATE TABLE `mod_products_colors` (
  `mod_prod_color_id` int NOT NULL AUTO_INCREMENT,
  `mod_prod_color_name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_color_hex` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_color_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_color_ent_id` int NOT NULL,
  `mod_prod_color_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_prod_color_id`,`mod_prod_color_hex`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_products_colors
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_products_files
-- ----------------------------
DROP TABLE IF EXISTS `mod_products_files`;
CREATE TABLE `mod_products_files` (
  `mod_prod_file_prod_id` int NOT NULL,
  `mod_prod_file_file_id` int NOT NULL,
  `mod_prod_file_order` int NOT NULL,
  PRIMARY KEY (`mod_prod_file_prod_id`,`mod_prod_file_file_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_products_files
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_products_food
-- ----------------------------
DROP TABLE IF EXISTS `mod_products_food`;
CREATE TABLE `mod_products_food` (
  `mod_prod_food_id` int NOT NULL AUTO_INCREMENT,
  `mod_prod_food_prod_id` int DEFAULT NULL,
  PRIMARY KEY (`mod_prod_food_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_products_food
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_products_options
-- ----------------------------
DROP TABLE IF EXISTS `mod_products_options`;
CREATE TABLE `mod_products_options` (
  `mod_prod_opt_id` int NOT NULL AUTO_INCREMENT,
  `mod_prod_opt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_opt_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_opt_autoload` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`mod_prod_opt_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_products_options
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_products_pharmacy
-- ----------------------------
DROP TABLE IF EXISTS `mod_products_pharmacy`;
CREATE TABLE `mod_products_pharmacy` (
  `mod_prod_pha_id` int NOT NULL AUTO_INCREMENT,
  `mod_prod_pha_code_provider` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_prov_id` int NOT NULL,
  `mod_prod_pha_lab_id` int NOT NULL,
  `mod_prod_pha_mak_id` int NOT NULL,
  `mod_prod_pha_presentation` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_codebar` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_phar_form` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_active_concentration_1` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_active_concentration_2` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_active_concentration_3` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_therapy_action` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_condition_sale` int NOT NULL,
  PRIMARY KEY (`mod_prod_pha_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_products_pharmacy
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_products_pharmacy_stock
-- ----------------------------
DROP TABLE IF EXISTS `mod_products_pharmacy_stock`;
CREATE TABLE `mod_products_pharmacy_stock` (
  `mod_prod_pha_stk_id` int NOT NULL AUTO_INCREMENT,
  `mod_prod_pha_stk_contry_id` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_stk_nro_health_registration` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_stk_expiration_date` datetime NOT NULL,
  `mod_prod_pha_stk_registration_date` datetime NOT NULL,
  `mod_prod_pha_stk_purchase_unit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_stk_ratio_units_box` int NOT NULL,
  `mod_prod_pha_stk_lot` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_stk_box_blister_ratio` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_stk_height_cm` int NOT NULL,
  `mod_prod_pha_stk_width_cm` int NOT NULL,
  `mod_prod_pha_stk_depth_cm` int NOT NULL,
  `mod_prod_pha_stk_diameter_cm` int NOT NULL,
  `mod_prod_pha_stk_storage_temperature_c` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`mod_prod_pha_stk_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_products_pharmacy_stock
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_products_prices_list
-- ----------------------------
DROP TABLE IF EXISTS `mod_products_prices_list`;
CREATE TABLE `mod_products_prices_list` (
  `mod_prod_prl_id` int NOT NULL AUTO_INCREMENT,
  `mod_prod_prl_prod_id` int NOT NULL,
  `mod_prod_prl_price` decimal(20,2) NOT NULL,
  `mod_prod_prl_previous_price` decimal(20,2) DEFAULT NULL,
  `mod_prod_prl_coin` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_prl_register_date` date NOT NULL,
  PRIMARY KEY (`mod_prod_prl_id`,`mod_prod_prl_prod_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_products_prices_list
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_products_relation
-- ----------------------------
DROP TABLE IF EXISTS `mod_products_relation`;
CREATE TABLE `mod_products_relation` (
  `mod_prod_rel_id` int NOT NULL AUTO_INCREMENT,
  `mod_prod_rel_prod_id` int NOT NULL,
  `mod_prod_rel_ent_id` int NOT NULL,
  `mod_prod_rel_type` int DEFAULT NULL,
  `mod_prod_rel_stock` int DEFAULT NULL,
  `mod_prod_rel_pricing` int DEFAULT NULL,
  PRIMARY KEY (`mod_prod_rel_id`,`mod_prod_rel_prod_id`,`mod_prod_rel_ent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_products_relation
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_products_rw
-- ----------------------------
DROP TABLE IF EXISTS `mod_products_rw`;
CREATE TABLE `mod_products_rw` (
  `mod_prod_rw_id` int NOT NULL AUTO_INCREMENT,
  `mod_prod_rw_prod_id` int NOT NULL,
  `mod_prod_rw_state` tinyint DEFAULT '0',
  `mod_prod_rw_rmc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_reference` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_familyname` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_modelname` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_ranking` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_spec_modelcase` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_spec_diameter` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_spec_material` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_spec_bezel` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_spec_windingcrown` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_spec_crystal` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_spec_waterresistance` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_spec_movement` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_spec_calibre` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_spec_bracelet` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_spec_braceletmaterial` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_spec_clasp` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_spec_dial` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_spec_gem_setting` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_feature1_title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_feature1_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_feature1_asset` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_feature2_title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_feature2_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_feature2_asset` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_feature3_title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_feature3_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_feature3_asset` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_brochure` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_rw_h1` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`mod_prod_rw_id`,`mod_prod_rw_prod_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_products_rw
-- ----------------------------
BEGIN;
INSERT INTO `mod_products_rw` VALUES (1, 0, 1, 'M124060-0001', '124060', 'Submariner', 'Submariner', '39928', 'Oyster, 41 mm, acero Oystersteel', '41 mm', 'Acero Oystersteel', 'Giratorio unidireccional graduado 60 minutos, disco Cerachrom de cerámica negra resistente a las rayaduras, números y graduaciones recubiertos en platino', 'Enroscada con sistema de triple hermeticidad Triplock', 'Zafiro resistente a las rayaduras', 'Hermético hasta 300 metros/1000 pies', 'Perpetual, mecánico, de cuerda automática', '3230, Manufactura Rolex', 'Oyster (3 elementos), eslabones macizos', 'Acero Oystersteel', 'Cierre de seguridad desplegable Oysterlock con sistema de extensión Rolex Glidelock', 'Negra', '', '	Bisel giratorio unidireccional', 'El bisel giratorio del Submariner es una característica fundamental de este reloj. Sus graduaciones de 60 minutos permiten controlar con seguridad y precisión el tiempo de inmersión y las paradas de descompresión. Manufacturado por Rolex a partir de una cerámica dura y resistente a la corrosión, el disco de bisel Cerachrom es prácticamente imposible de rayar. La sustancia luminiscente en el cero del bisel garantiza una legibilidad óptima, sin importar el grado de oscuridad del entorno. El canto estriado del bisel ha sido diseñado cuidadosamente con el fin de ofrecer un excelente agarre bajo el agua, incluso con los guantes puestos. ', 'm124060-0001_bezel', 'Esfera negra', 'La visualización luminiscente Chromalight de la esfera constituye una innovación que mejora la visibilidad en entornos oscuros, una función especial para los submarinistas. Los índices de formas simples —triángulos, círculos, rectángulos— y las anchas agujas de horas y minutos facilitan la lectura y previenen cualquier riesgo de confusión bajo el agua.', 'm124060-0001_dial', 'Acero Oystersteel', 'Rolex utiliza acero Oystersteel para las cajas de acero de sus relojes. Especialmente desarrollado por Rolex, el acero Oystersteel pertenece a la familia del acero 904L, las aleaciones más utilizadas en la alta tecnología y en las industrias química y aeroespacial, en las que la resistencia a la corrosión es esencial. El acero Oystersteel es extremadamente resistente, ofrece un acabado excepcional al ser pulido y conserva su belleza incluso en las condiciones más extremas.', 'm124060-0001_material', 'https://assets.rolex.com/api/brochure/es/submariner/m124060-0001.pdf', 'Rolex \r\nSUBMARINER\r\nOyster, 41 mm, acero Oystersteel\r\nM124060-0001');
COMMIT;

-- ----------------------------
-- Table structure for mod_products_types
-- ----------------------------
DROP TABLE IF EXISTS `mod_products_types`;
CREATE TABLE `mod_products_types` (
  `mod_prod_type_id` int NOT NULL AUTO_INCREMENT,
  `mod_prod_type_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_type_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_type_subfix` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_type_subfix_db` varchar(24) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_type_primary` int DEFAULT '0',
  `mod_prod_type_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_type_ent_id` int DEFAULT NULL,
  `mod_prod_type_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_prod_type_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_products_types
-- ----------------------------
BEGIN;
INSERT INTO `mod_products_types` VALUES (1, 'Store', 'store', '_str', '_store', 0, '', 1, 1);
INSERT INTO `mod_products_types` VALUES (2, 'Clothing store', 'clothingStore', '_cst', '_clothing_store', 1, NULL, 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for mod_products_warehouses
-- ----------------------------
DROP TABLE IF EXISTS `mod_products_warehouses`;
CREATE TABLE `mod_products_warehouses` (
  `mod_prod_wrh_prod_id` int NOT NULL,
  `mod_prod_wrh_wrh_id` int NOT NULL,
  `mod_prod_wrh_order` int NOT NULL,
  PRIMARY KEY (`mod_prod_wrh_prod_id`,`mod_prod_wrh_wrh_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_products_warehouses
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_providers
-- ----------------------------
DROP TABLE IF EXISTS `mod_providers`;
CREATE TABLE `mod_providers` (
  `mod_prov_id` int NOT NULL AUTO_INCREMENT,
  `mod_prov_code` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prov_user_id` int NOT NULL,
  `mod_prov_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prov_email` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prov_phone` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prov_address` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prov_rating` decimal(3,2) DEFAULT NULL,
  `mod_prov_city` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prov_zip` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prov_country` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prov_website` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prov_logo` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prov_description` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prov_type` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prov_created` datetime NOT NULL,
  `mod_prov_modified` datetime NOT NULL,
  `mod_prov_deleted` datetime DEFAULT NULL COMMENT 'Fecha de eliminación (si aplica)',
  `mod_prov_state` int DEFAULT '0' COMMENT 'Estado (0: Inactivo, 1: Activo)',
  PRIMARY KEY (`mod_prov_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Tabla de proveedores';

-- ----------------------------
-- Records of mod_providers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_rehabilitation_centers
-- ----------------------------
DROP TABLE IF EXISTS `mod_rehabilitation_centers`;
CREATE TABLE `mod_rehabilitation_centers` (
  `mod_rhb_center_id` int NOT NULL AUTO_INCREMENT,
  `mod_rhb_center_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_center_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_center_coord` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_center_user_id` int DEFAULT NULL,
  `mod_rhb_center_ent_id` int DEFAULT NULL,
  `mod_rhb_center_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_rhb_center_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_rehabilitation_centers
-- ----------------------------
BEGIN;
INSERT INTO `mod_rehabilitation_centers` VALUES (1, 'Centro 1', NULL, NULL, 4, 1, 1);
INSERT INTO `mod_rehabilitation_centers` VALUES (2, 'Centro 2', NULL, NULL, 2, 1, 1);
INSERT INTO `mod_rehabilitation_centers` VALUES (3, 'Centro 3', NULL, NULL, 0, 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for mod_rehabilitation_intern
-- ----------------------------
DROP TABLE IF EXISTS `mod_rehabilitation_intern`;
CREATE TABLE `mod_rehabilitation_intern` (
  `mod_rhb_int_id` int NOT NULL AUTO_INCREMENT,
  `mod_rhb_int_acu_id` int NOT NULL,
  `mod_rhb_int_date_intern` datetime DEFAULT NULL,
  `mod_rhb_int_date_graduation` datetime DEFAULT NULL,
  `mod_rhb_int_date_leaving` datetime DEFAULT NULL,
  `mod_rhb_int_reason_leaving` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_rhb_int_reason_addiction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_int_primary_consumption` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_int_state` int DEFAULT '0',
  PRIMARY KEY (`mod_rhb_int_id`,`mod_rhb_int_acu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_rehabilitation_intern
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_rehabilitation_primary_consumption
-- ----------------------------
DROP TABLE IF EXISTS `mod_rehabilitation_primary_consumption`;
CREATE TABLE `mod_rehabilitation_primary_consumption` (
  `mod_rhb_pcn_id` int NOT NULL AUTO_INCREMENT,
  `mod_rhb_pcn_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_pcn_summary` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_pcn_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_pcn_order` int DEFAULT NULL,
  `mod_rhb_pcn_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_rhb_pcn_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_rehabilitation_primary_consumption
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_rehabilitation_tutor
-- ----------------------------
DROP TABLE IF EXISTS `mod_rehabilitation_tutor`;
CREATE TABLE `mod_rehabilitation_tutor` (
  `mod_rhb_tutor_id` int NOT NULL AUTO_INCREMENT,
  `mod_rhb_tutor_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_fathers_lastname` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_mothers_lastname` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_rhb_id` int DEFAULT NULL COMMENT 'mod_rhb_user_id',
  `mod_rhb_tutor_gender` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_ci` int DEFAULT NULL,
  `mod_rhb_tutor_ext` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_dial` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_celular` int DEFAULT NULL,
  `mod_rhb_tutor_relationship` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_intern_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_coordinates` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_rhb_tutor_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_rehabilitation_tutor
-- ----------------------------
BEGIN;
INSERT INTO `mod_rehabilitation_tutor` VALUES (1, 'Hermany', 'Terrazas', 'Cossio', 1, '0', 4735799, 'SC', '591', 75313126, '', '', '', '', 1);
COMMIT;

-- ----------------------------
-- Table structure for mod_rehabilitation_user
-- ----------------------------
DROP TABLE IF EXISTS `mod_rehabilitation_user`;
CREATE TABLE `mod_rehabilitation_user` (
  `mod_rhb_id` int NOT NULL AUTO_INCREMENT,
  `mod_rhb_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_rhb_lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_rhb_acu_id` int DEFAULT NULL,
  `mod_rhb_tepcyr` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Time from First Consumption to Rehabilitation',
  `mod_rhb_birthday` date DEFAULT NULL,
  `mod_rhb_nationality` varchar(550) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_age_first_use` int DEFAULT NULL,
  `mod_rhb_register_date` date DEFAULT NULL,
  `mod_rhb_user_id` int DEFAULT NULL,
  `mod_rhb_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_rhb_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_rehabilitation_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_rewards
-- ----------------------------
DROP TABLE IF EXISTS `mod_rewards`;
CREATE TABLE `mod_rewards` (
  `mod_rwd_id` int NOT NULL,
  `mod_rwd_cpa_id` int NOT NULL,
  `mod_rwd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rwd_img` int DEFAULT NULL,
  `mod_rwd_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_rwd_code` varchar(21) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rwd_value` int NOT NULL DEFAULT '0',
  `mod_rwd_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_rwd_register_date` datetime NOT NULL,
  `mod_rwd_user_id` int DEFAULT NULL,
  `mod_rwd_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_rewards
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_rewards_register
-- ----------------------------
DROP TABLE IF EXISTS `mod_rewards_register`;
CREATE TABLE `mod_rewards_register` (
  `mod_rwd_reg_rwd_id` int NOT NULL,
  `mod_rwd_reg_acu_id` int NOT NULL,
  `mod_rwd_reg_cpa_id` int NOT NULL,
  `mod_rwd_reg_register_date` datetime NOT NULL,
  `mod_rwd_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_rwd_reg_rwd_id`,`mod_rwd_reg_acu_id`,`mod_rwd_reg_cpa_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_rewards_register
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_services
-- ----------------------------
DROP TABLE IF EXISTS `mod_services`;
CREATE TABLE `mod_services` (
  `mod_srv_id` int NOT NULL AUTO_INCREMENT,
  `mod_srv_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mod_srv_description` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mod_srv_active` int NOT NULL,
  `mod_srv_duration` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mod_srv_type` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mod_srv_price` decimal(10,2) NOT NULL,
  `mod_srv_created` datetime NOT NULL,
  `mod_srv_modified` datetime NOT NULL,
  `mod_srv_deleted` datetime DEFAULT NULL,
  `mod_srv_state` int DEFAULT '0',
  PRIMARY KEY (`mod_srv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Tabla de servicios';

-- ----------------------------
-- Records of mod_services
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_sliders
-- ----------------------------
DROP TABLE IF EXISTS `mod_sliders`;
CREATE TABLE `mod_sliders` (
  `mod_sli_id` int NOT NULL AUTO_INCREMENT,
  `mod_sli_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_sli_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_sli_date_register` datetime DEFAULT NULL,
  `mod_sli_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `mod_sli_cls` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_sli_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_sli_ent_id` int NOT NULL,
  `mod_sli_user_id` int DEFAULT NULL,
  `mod_sli_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_sli_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_sliders
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_sliders_categorys
-- ----------------------------
DROP TABLE IF EXISTS `mod_sliders_categorys`;
CREATE TABLE `mod_sliders_categorys` (
  `mod_sli_cat_sli_id` int NOT NULL,
  `mod_sli_cat_cat_id` int NOT NULL,
  `mod_sli_cat_order` int NOT NULL,
  PRIMARY KEY (`mod_sli_cat_sli_id`,`mod_sli_cat_cat_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_sliders_categorys
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_sliders_files
-- ----------------------------
DROP TABLE IF EXISTS `mod_sliders_files`;
CREATE TABLE `mod_sliders_files` (
  `mod_sli_file_file_id` int NOT NULL,
  `mod_sli_file_sli_id` int NOT NULL,
  `mod_sli_file_link` varchar(445) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_sli_file_target` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '_self',
  `mod_sli_file_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_sli_file_order` int DEFAULT NULL,
  PRIMARY KEY (`mod_sli_file_file_id`,`mod_sli_file_sli_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_sliders_files
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_sports
-- ----------------------------
DROP TABLE IF EXISTS `mod_sports`;
CREATE TABLE `mod_sports` (
  `mod_spt_id` int NOT NULL AUTO_INCREMENT,
  `mod_spt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_sp_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_spt_description` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_spt_img` int DEFAULT NULL,
  `mod_spt_ent_id` int DEFAULT NULL,
  `mod_spt_state` int DEFAULT NULL,
  PRIMARY KEY (`mod_spt_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_sports
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_sports_dates
-- ----------------------------
DROP TABLE IF EXISTS `mod_sports_dates`;
CREATE TABLE `mod_sports_dates` (
  `mod_sp_dt_id` int NOT NULL AUTO_INCREMENT,
  `mod_sp_dt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_sp_dt_date` date DEFAULT NULL,
  `mod_sp_dt_comp_id` int DEFAULT NULL COMMENT 'competition',
  `mod_sp_dt_div_id` int DEFAULT NULL,
  `mod_sp_dt_ent_id` int DEFAULT NULL,
  `mod_sp_dt_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_sp_dt_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_sports_dates
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_sports_games
-- ----------------------------
DROP TABLE IF EXISTS `mod_sports_games`;
CREATE TABLE `mod_sports_games` (
  `mod_sp_gm_id` int NOT NULL AUTO_INCREMENT,
  `mod_sp_gm_tm_1` int DEFAULT NULL,
  `mod_sp_gm_tm_2` int DEFAULT NULL,
  `mod_sp_gm_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_sp_gm_info` int DEFAULT NULL,
  `mod_sp_gm_tm_score_1` int DEFAULT NULL,
  `mod_sp_gm_tm_score_2` int DEFAULT NULL,
  `mod_sg_gm_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_sg_gm_date` datetime DEFAULT NULL,
  `mod_sg_gm_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_sg_gm_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_sp_gm_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_sports_games
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_sports_teams
-- ----------------------------
DROP TABLE IF EXISTS `mod_sports_teams`;
CREATE TABLE `mod_sports_teams` (
  `mod_sp_tm_id` int NOT NULL AUTO_INCREMENT,
  `mod_sp_tm_sport` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_sp_tm_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_sp_tm_pathurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_sp_tm_details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_sp_tm_logo` int DEFAULT NULL COMMENT 'file ID',
  `mod_sp_tm_banner` int DEFAULT NULL COMMENT 'file Id',
  `mod_sp_tm_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mos_sp_tm_ent_id` int DEFAULT NULL,
  `mod_sp_tm_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_sp_tm_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_sports_teams
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_stock
-- ----------------------------
DROP TABLE IF EXISTS `mod_stock`;
CREATE TABLE `mod_stock` (
  `mod_stk_id` int NOT NULL AUTO_INCREMENT,
  `mod_stk_sku` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_stk_prod_id` int NOT NULL,
  `mod_stk_quantity` int DEFAULT NULL,
  `mod_stk_register_date` datetime NOT NULL,
  `mod_stk_comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_stk_inv_id` int DEFAULT NULL COMMENT 'id factura',
  `mod_stk_max` int DEFAULT NULL,
  `mod_stk_min` int DEFAULT NULL,
  `mod_stk_alert` int DEFAULT NULL,
  `mod_stk_receipt_date` datetime NOT NULL COMMENT 'fecha de recepción',
  `mod_stk_lote` int NOT NULL,
  `mod_stk_prv_id` int NOT NULL COMMENT 'id proveedor',
  `mod_stk_net_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'precio neto',
  `mod_stk_ent_id` int DEFAULT NULL,
  `mod_stk_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_stk_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_stock
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_stock_receipt
-- ----------------------------
DROP TABLE IF EXISTS `mod_stock_receipt`;
CREATE TABLE `mod_stock_receipt` (
  `mod_str_id` int NOT NULL AUTO_INCREMENT,
  `mod_str_prod_id` int DEFAULT NULL,
  `mod_str_quantity` int DEFAULT NULL,
  `mod_str_register_date` datetime NOT NULL,
  `mod_str_comment` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_str_inv_id` int DEFAULT NULL COMMENT 'id factura',
  `mod_str_max` int DEFAULT NULL,
  `mod_str_min` int DEFAULT NULL,
  `mod_str_receipt_date` datetime NOT NULL COMMENT 'fecha de recepción',
  `mod_str_lote` int NOT NULL,
  `mod_str_prv_id` int NOT NULL COMMENT 'id proveedor',
  `mod_str_net_price` decimal(20,2) NOT NULL,
  `mod_str_cash_discount` decimal(20,2) NOT NULL,
  `mod_str_discount` int NOT NULL,
  `mod_str_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_str_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_stock_receipt
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_surveys
-- ----------------------------
DROP TABLE IF EXISTS `mod_surveys`;
CREATE TABLE `mod_surveys` (
  `mod_svy_id` int NOT NULL AUTO_INCREMENT,
  `mod_svy_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_svy_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_svy_created_by` int DEFAULT NULL,
  `mod_svy_creation_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `mod_svy_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`mod_svy_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_surveys
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_surveys_options
-- ----------------------------
DROP TABLE IF EXISTS `mod_surveys_options`;
CREATE TABLE `mod_surveys_options` (
  `mod_svy_so_id` int NOT NULL AUTO_INCREMENT,
  `mod_svy_so_sq_id` int DEFAULT NULL,
  `mod_svy_so_option` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_svy_so_order` int DEFAULT '1',
  `mod_svy_so_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_svy_so_id`) USING BTREE,
  KEY `mod_so_sq_id` (`mod_svy_so_sq_id`) USING BTREE,
  CONSTRAINT `mod_surveys_options_ibfk_1` FOREIGN KEY (`mod_svy_so_sq_id`) REFERENCES `mod_surveys_questions` (`mod_svy_sq_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_surveys_options
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_surveys_questions
-- ----------------------------
DROP TABLE IF EXISTS `mod_surveys_questions`;
CREATE TABLE `mod_surveys_questions` (
  `mod_svy_sq_id` int NOT NULL AUTO_INCREMENT,
  `mod_svy_sq_svy_id` int DEFAULT NULL,
  `mod_svy_sq_question` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_svy_sq_type` enum('text','multiple_choice','rating') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_svy_sq_is_required` tinyint(1) DEFAULT '1',
  `mod_svy_sq_order` int DEFAULT '1',
  `mod_svy_sq_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_svy_sq_id`) USING BTREE,
  KEY `mod_sq_svy_id` (`mod_svy_sq_svy_id`) USING BTREE,
  CONSTRAINT `mod_surveys_questions_ibfk_1` FOREIGN KEY (`mod_svy_sq_svy_id`) REFERENCES `mod_surveys` (`mod_svy_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_surveys_questions
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_surveys_responses
-- ----------------------------
DROP TABLE IF EXISTS `mod_surveys_responses`;
CREATE TABLE `mod_surveys_responses` (
  `mod_svy_svr_id` int NOT NULL AUTO_INCREMENT,
  `mod_svy_svr_acu_id` int DEFAULT NULL,
  `mod_svy_svr_survey_id` int DEFAULT NULL,
  `mod_svy_svr_sq_id` int DEFAULT NULL,
  `mod_svy_svr_response` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_svy_svr_atk_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_svy_svr_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `mod_svy_svr_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_svy_svr_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_surveys_responses
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_syllabus
-- ----------------------------
DROP TABLE IF EXISTS `mod_syllabus`;
CREATE TABLE `mod_syllabus` (
  `mod_syl_id` int NOT NULL AUTO_INCREMENT,
  `mod_syl_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_syl_summary` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_syl_ent_id` int DEFAULT NULL,
  `mod_syl_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_syl_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_syllabus
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_syllabus_categorys
-- ----------------------------
DROP TABLE IF EXISTS `mod_syllabus_categorys`;
CREATE TABLE `mod_syllabus_categorys` (
  `mod_syl_cat_syl_id` int NOT NULL,
  `mod_syl_cat_cat_id` int NOT NULL,
  `mod_syl_cat_order` int NOT NULL,
  PRIMARY KEY (`mod_syl_cat_syl_id`,`mod_syl_cat_cat_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_syllabus_categorys
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_syllabus_contents
-- ----------------------------
DROP TABLE IF EXISTS `mod_syllabus_contents`;
CREATE TABLE `mod_syllabus_contents` (
  `mod_syl_cont_id` int NOT NULL AUTO_INCREMENT,
  `mod_syl_cont_title` varchar(445) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_syl_cont_summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_syl_cont_per_id` int DEFAULT NULL,
  `mod_syl_cont_syl_id` int DEFAULT NULL,
  `mod_syl_cont_order` int DEFAULT NULL,
  `mod_syl_cont_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_syl_cont_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_syllabus_contents
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_syllabus_periods
-- ----------------------------
DROP TABLE IF EXISTS `mod_syllabus_periods`;
CREATE TABLE `mod_syllabus_periods` (
  `mod_syl_per_id` int NOT NULL AUTO_INCREMENT,
  `mod_syl_per_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_syl_per_summary` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_syl_per_ent_id` int DEFAULT NULL,
  `mod_syl_per_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_syl_per_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_syllabus_periods
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_syllabus_relations_periods
-- ----------------------------
DROP TABLE IF EXISTS `mod_syllabus_relations_periods`;
CREATE TABLE `mod_syllabus_relations_periods` (
  `mod_syl_rel_per_syl_id` int NOT NULL,
  `mod_syl_rel_per_per_id` int NOT NULL,
  `mod_syl_rel_per_order` int DEFAULT NULL,
  PRIMARY KEY (`mod_syl_rel_per_syl_id`,`mod_syl_rel_per_per_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_syllabus_relations_periods
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_tributes_contribuyente_form
-- ----------------------------
DROP TABLE IF EXISTS `mod_tributes_contribuyente_form`;
CREATE TABLE `mod_tributes_contribuyente_form` (
  `mod_tb_cf_typc_id` int NOT NULL,
  `mod_tb_cf_type_form_id` int NOT NULL,
  `mod_tb_cf_presentacion` varchar(445) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_tributes_contribuyente_form
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_tributes_facturas
-- ----------------------------
DROP TABLE IF EXISTS `mod_tributes_facturas`;
CREATE TABLE `mod_tributes_facturas` (
  `mod_tb_fac_id` int NOT NULL AUTO_INCREMENT,
  `mod_tb_fac_nit` int NOT NULL,
  `mod_tb_fac_img` int DEFAULT NULL,
  `mod_tb_fac_type` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'manual',
  `mod_tb_fac_periodo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fac_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_tb_fac_id`,`mod_tb_fac_nit`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_tributes_facturas
-- ----------------------------
BEGIN;
INSERT INTO `mod_tributes_facturas` VALUES (5, 2147483647, 57, 'trimestral', '1', 1);
INSERT INTO `mod_tributes_facturas` VALUES (6, 2147483647, 58, 'manual', '1', 1);
INSERT INTO `mod_tributes_facturas` VALUES (7, 0, 59, 'manual', '1', 1);
INSERT INTO `mod_tributes_facturas` VALUES (8, 0, 60, 'manual', '1', 1);
COMMIT;

-- ----------------------------
-- Table structure for mod_tributes_options
-- ----------------------------
DROP TABLE IF EXISTS `mod_tributes_options`;
CREATE TABLE `mod_tributes_options` (
  `mod_tb_op_id` int NOT NULL AUTO_INCREMENT,
  `mod_tb_op_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_tb_op_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_tb_op_autoload` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'yes',
  `mod_tb_op_date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `mod_tb_op_date_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`mod_tb_op_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_tributes_options
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_tributes_periods
-- ----------------------------
DROP TABLE IF EXISTS `mod_tributes_periods`;
CREATE TABLE `mod_tributes_periods` (
  `mod_tb_pr_id` int NOT NULL AUTO_INCREMENT COMMENT 'ej:202409 se refiere al periodo 2024-09 año/mes siempre seran 6 digitos',
  `mod_tb_pr_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_pr_year_fiscal` year DEFAULT NULL COMMENT 'ej:2024',
  `mod_tb_pr_init_monthly` date DEFAULT NULL COMMENT 'ej:2024-01-01',
  `mod_tb_pr_end_monthly` date DEFAULT NULL COMMENT 'ej:2024-12-31',
  `mod_tb_pr_init_quarterly` date DEFAULT NULL COMMENT 'ej:2024-01-01',
  `mod_tb_pr_end_quarterly` date DEFAULT NULL COMMENT 'ej:2024-12-31',
  `mod_tb_pr_type` varchar(25) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'ej: M M,T.. M: mensual, M,T: mensual y trimestral',
  `mod_tb_pr_active_user` int DEFAULT NULL COMMENT 'si este periodo esta activo para el usuario manejado por el cron de sistema para cerrar periodos',
  `mod_tb_pr_active_operator` int DEFAULT NULL COMMENT 'si este periodo esta activo para el operador manejado por el cron de sistema para cerrar periodos',
  `mod_tb_pr_message_register` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT 'es el campo texto para gestionar mensajes en el registro, es html hecho en markdown',
  `mod_tb_pr_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_tb_pr_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_tb_pr_status` int DEFAULT NULL,
  PRIMARY KEY (`mod_tb_pr_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=202513 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_tributes_periods
-- ----------------------------
BEGIN;
INSERT INTO `mod_tributes_periods` VALUES (202412, 'Periodo 12-2024', 2024, '2024-12-01', '2024-12-31', '2024-10-01', '2024-12-31', 'M,T', 1, 1, '- Facturas Mensuales desde el 2024/Dic/01 al 2024/Dic/31\r\n- Facturas Trimestrales desde el 2024/Oct/01 al 2024/Dic/31', NULL, NULL, 1);
INSERT INTO `mod_tributes_periods` VALUES (202501, 'Periodo 01-2025', 2025, '2025-01-01', '2024-01-31', '2025-01-01', '2025-03-31', 'M', 0, 0, '- Facturas Mensuales desde el 2025/Ene/01 al 2025/Ene/31', NULL, NULL, 0);
INSERT INTO `mod_tributes_periods` VALUES (202502, 'Periodo 02-2025', 2025, '2025-02-01', '2024-02-28', '2025-01-01', '2025-03-31', 'M', 0, 0, '- Facturas Mensuales desde el 2025/Feb/01 al 2025/Feb/28', NULL, NULL, 0);
INSERT INTO `mod_tributes_periods` VALUES (202503, 'Periodo 03-2025', 2025, '2025-03-01', '2024-03-31', '2025-01-01', '2025-03-31', 'M,T', 0, 0, '- Facturas Mensuales desde el 2025/Mar/01 al 2025/Mar/31 - Facturas Trimestrales desde el 2025/Ene/01 al 2025/Mar/31', NULL, NULL, 0);
INSERT INTO `mod_tributes_periods` VALUES (202504, 'Periodo 04-2025', 2025, '2025-04-01', '2024-04-30', '2025-04-01', '2025-06-30', 'M', 0, 0, '- Facturas Mensuales desde el 2025/Abr/01 al 2025/Abr/30', NULL, NULL, 0);
INSERT INTO `mod_tributes_periods` VALUES (202505, 'Periodo 05-2025', 2025, '2025-05-01', '2024-05-31', '2025-04-01', '2025-06-30', 'M', 0, 0, '- Facturas Mensuales desde el 2025/May/01 al 2025/May/31', NULL, NULL, 0);
INSERT INTO `mod_tributes_periods` VALUES (202506, 'Periodo 06-2025', 2025, '2025-06-01', '2024-06-30', '2025-04-01', '2025-06-30', 'M,T', 0, 0, '- Facturas Mensuales desde el 2025/Jun/01 al 2025/Jun/30 - Facturas Trimestrales desde el 2025/Abr/01 al 2025/Jun/30', NULL, NULL, 0);
INSERT INTO `mod_tributes_periods` VALUES (202507, 'Periodo 07-2025', 2025, '2025-07-01', '2024-07-31', '2025-07-01', '2025-09-30', 'M', 0, 0, '- Facturas Mensuales desde el 2025/Jul/01 al 2025/Jul/31', NULL, NULL, 0);
INSERT INTO `mod_tributes_periods` VALUES (202508, 'Periodo 08-2025', 2025, '2025-08-01', '2024-08-31', '2025-07-01', '2025-09-30', 'M', 0, 0, '- Facturas Mensuales desde el 2025/Ago/01 al 2025/Ago/31', NULL, NULL, 0);
INSERT INTO `mod_tributes_periods` VALUES (202509, 'Periodo 09-2025', 2025, '2025-09-01', '2024-09-30', '2025-07-01', '2025-09-30', 'M,T', 0, 0, '- Facturas Mensuales desde el 2025/Sep/01 al 2025/Sep/30 - Facturas Trimestrales desde el 2025/Jul/01 al 2025/Sep/30', NULL, NULL, 0);
INSERT INTO `mod_tributes_periods` VALUES (202510, 'Periodo 10-2025', 2025, '2025-10-01', '2024-10-31', '2025-10-01', '2025-12-31', 'M', 0, 0, '- Facturas Mensuales desde el 2025/Oct/01 al 2025/Oct/31', NULL, NULL, 0);
INSERT INTO `mod_tributes_periods` VALUES (202511, 'Periodo 11-2025', 2025, '2025-11-01', '2024-11-30', '2025-10-01', '2025-12-31', 'M', 0, 0, '- Facturas Mensuales desde el 2025/Nov/01 al 2025/Nov/30', NULL, NULL, 0);
INSERT INTO `mod_tributes_periods` VALUES (202512, 'Periodo 12-2025', 2025, '2025-12-01', '2024-12-31', '2025-10-01', '2025-12-31', 'M,T', 0, 0, '- Facturas Mensuales desde el 2025/Dic/01 al 2025/Dic/31 - Facturas Trimestrales desde el 2025/Oct/01 al 2025/Dic/31', NULL, NULL, 0);
COMMIT;

-- ----------------------------
-- Table structure for mod_tributes_plans_periods
-- ----------------------------
DROP TABLE IF EXISTS `mod_tributes_plans_periods`;
CREATE TABLE `mod_tributes_plans_periods` (
  `mod_tb_plpr_acu_id` int NOT NULL,
  `mod_tb_plpr_pl_id` int NOT NULL,
  `mod_tb_plpr_pr_id` int NOT NULL,
  `mod_tb_plpr_status` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_tributes_plans_periods
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_tributes_relations_periods
-- ----------------------------
DROP TABLE IF EXISTS `mod_tributes_relations_periods`;
CREATE TABLE `mod_tributes_relations_periods` (
  `mod_tb_rpd_id` int NOT NULL AUTO_INCREMENT,
  `mod_tb_rpd_pr_id` int DEFAULT NULL,
  `mod_tb_rpd_group_nit` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'g1,g2',
  `mod_tb_rpd_date_max_reception` date DEFAULT NULL,
  `mod_tb_rpd_date_max_preliquidation` date DEFAULT NULL,
  `mod_tb_rpd_date_ok_preliquidation` date DEFAULT NULL,
  `mod_tb_rpd_date_declaration` date DEFAULT NULL,
  PRIMARY KEY (`mod_tb_rpd_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_tributes_relations_periods
-- ----------------------------
BEGIN;
INSERT INTO `mod_tributes_relations_periods` VALUES (1, 202409, 'G1', '2024-10-05', '2024-10-09', '2024-10-11', '2024-10-12');
INSERT INTO `mod_tributes_relations_periods` VALUES (2, 202409, 'G2', '2024-10-05', '2024-10-10', '2024-10-14', '2024-10-17');
INSERT INTO `mod_tributes_relations_periods` VALUES (3, 202410, 'G1', '2024-11-05', '2024-11-09', '2024-11-11', '2024-11-12');
INSERT INTO `mod_tributes_relations_periods` VALUES (4, 202410, 'G2', '2024-11-05', '2024-11-10', '2024-11-14', '2024-11-17');
INSERT INTO `mod_tributes_relations_periods` VALUES (5, 202411, 'G1', '2024-12-05', '2024-12-09', '2024-12-11', '2024-12-12');
INSERT INTO `mod_tributes_relations_periods` VALUES (6, 202411, 'G2', '2024-12-05', '2024-12-10', '2024-12-14', '2024-12-17');
INSERT INTO `mod_tributes_relations_periods` VALUES (7, 202412, 'G1', '2025-01-05', '2025-01-09', '2025-01-11', '2025-01-12');
INSERT INTO `mod_tributes_relations_periods` VALUES (8, 202409, 'G2', '2025-01-05', '2025-01-10', '2025-01-14', '2025-01-17');
COMMIT;

-- ----------------------------
-- Table structure for mod_tributos_facturas_filter
-- ----------------------------
DROP TABLE IF EXISTS `mod_tributos_facturas_filter`;
CREATE TABLE `mod_tributos_facturas_filter` (
  `mod_tb_fcf_id` int NOT NULL AUTO_INCREMENT,
  `mod_tb_fcf_nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_fac_id` int DEFAULT NULL,
  `mod_tb_fcf_fac_nit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_fac_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_fac_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_fac_periodo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_fac_state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_nitProveedor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_razonSocialProveedor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_codigoAutorizacion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_numeroFactura` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_numeroDuiDim` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_fcf_fechaFacturaDuiDim` date DEFAULT NULL,
  `mod_tb_fcf_importeTotalCompra` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_importeIce` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_importeIehd` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_importeIpj` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_tasas` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_otroNoSujetoACreditoFiscal` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_importesExentos` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_importeComprasGravadasATasaCero` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_descuentos` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_importeGifCard` decimal(10,2) DEFAULT NULL,
  `mod_tb_fcf_user_id` int DEFAULT NULL,
  `mod_tb_fcf_date_register` date DEFAULT NULL,
  `mod_tb_fcf_state` int DEFAULT NULL,
  PRIMARY KEY (`mod_tb_fcf_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_tributos_facturas_filter
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mod_types_contribuyentes
-- ----------------------------
DROP TABLE IF EXISTS `mod_types_contribuyentes`;
CREATE TABLE `mod_types_contribuyentes` (
  `mod_typc_id` int NOT NULL AUTO_INCREMENT,
  `mod_typc_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_typc_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_typc_system` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_typc_state` int DEFAULT NULL,
  PRIMARY KEY (`mod_typc_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_types_contribuyentes
-- ----------------------------
BEGIN;
INSERT INTO `mod_types_contribuyentes` VALUES (1, 'Alquiler de Bienes', NULL, 'tributo_v1', 1);
INSERT INTO `mod_types_contribuyentes` VALUES (2, 'Consultor en Linea', NULL, 'tributo_v1', 1);
INSERT INTO `mod_types_contribuyentes` VALUES (3, 'Oficio Independiente', NULL, 'tributo_v1', 1);
INSERT INTO `mod_types_contribuyentes` VALUES (4, 'Profesional Independiente', NULL, 'tributo_v1', 1);
COMMIT;

-- ----------------------------
-- Table structure for mod_types_forms
-- ----------------------------
DROP TABLE IF EXISTS `mod_types_forms`;
CREATE TABLE `mod_types_forms` (
  `mod_type_form_id` int NOT NULL AUTO_INCREMENT,
  `mod_type_form_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_type_form_declaracion` varchar(24) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_type_form_mode` varchar(44) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_type_form_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_type_form_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_type_form_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=611 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_types_forms
-- ----------------------------
BEGIN;
INSERT INTO `mod_types_forms` VALUES (110, 'Formulario 110', 'trimestral', 'impuestos', '0', 0);
INSERT INTO `mod_types_forms` VALUES (200, 'Formulario 200', 'mensual', 'impuestos', '0', 1);
INSERT INTO `mod_types_forms` VALUES (400, 'Formulario 400', 'mensual', 'impuestos', '0', 0);
INSERT INTO `mod_types_forms` VALUES (610, 'Formulario 610', 'trimestral', 'impuestos', '0', 1);
COMMIT;

-- ----------------------------
-- Table structure for mod_types_oficios
-- ----------------------------
DROP TABLE IF EXISTS `mod_types_oficios`;
CREATE TABLE `mod_types_oficios` (
  `mod_tb_tof_id` int NOT NULL AUTO_INCREMENT,
  `mod_tb_tof_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_tof_descrition` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_tof_code` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_tb_tof_state` int NOT NULL,
  PRIMARY KEY (`mod_tb_tof_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_types_oficios
-- ----------------------------
BEGIN;
INSERT INTO `mod_types_oficios` VALUES (1, 'Alquiler de Bienes', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (2, 'Consultor en Linea', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (3, 'Instaladores', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (4, 'Chóferes', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (5, 'Carpiteros', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (6, 'Mecánicos', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (7, 'Consultores Independientes', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (8, 'Arquitectos', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (9, 'Abogados', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (10, 'Médicos', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (11, 'Catedráticos', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (12, 'Ingenieros', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (13, 'Veterinarios', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (14, 'Psicólogos', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (15, 'Bioquimicas', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (16, 'Economistas', '0', NULL, 1);
INSERT INTO `mod_types_oficios` VALUES (17, 'Otros Profesionales Independientes', '0', NULL, 1);
COMMIT;

-- ----------------------------
-- Table structure for mod_warehouses
-- ----------------------------
DROP TABLE IF EXISTS `mod_warehouses`;
CREATE TABLE `mod_warehouses` (
  `mod_wrh_id` int NOT NULL AUTO_INCREMENT,
  `mod_wrh_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_wrh_details` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_wrh_primary` bit(2) DEFAULT b'0',
  `mod_wrh_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_wrh_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of mod_warehouses
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for modules
-- ----------------------------
DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `mod_id` int NOT NULL AUTO_INCREMENT,
  `mod_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_pathurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_code` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_parent_id` int DEFAULT NULL,
  `mod_indexjs` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_css` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_db` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'database',
  `mod_prefix_db` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_relations_db` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`mod_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3802 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of modules
-- ----------------------------
BEGIN;
INSERT INTO `modules` VALUES (1, 'systems', NULL, 'systems', 'modules/systems/', 'SYS1', 'icon icon-systems', '#99C14C', 0, 'components/sistems.js', 'assets/css/dist/systems.css', 'systems', 'sys_', '', 0);
INSERT INTO `modules` VALUES (2, 'modules', NULL, 'modules', 'modules/modules/', 'MOD1', 'icon icon-box-close', '#99C14C', 0, 'components/modules.js', 'assets/css/dist/modules.css', 'modules', 'mod_', 'modules_categorys:mod_cat_mod_id,systems_modules:sys_mod_mod_id', 0);
INSERT INTO `modules` VALUES (3, 'sites', NULL, 'sites', 'modules/websites/', 'SIT1', 'icon icon-blocks', '#8900ff', 0, 'components/sites.js', 'assets/css/dist/sites.css', 'sites', 'site_', '', 0);
INSERT INTO `modules` VALUES (4, 'Hojas de Trabajo', NULL, 'worksheets', 'modules/websites/', 'WS1', 'icon icon-worksheets', '#26fff6', 0, 'components/worksheets.js', 'assets/css/dist/worksheets.css', 'worksheets', 'ws_', '', 1);
INSERT INTO `modules` VALUES (5, 'Bloques', NULL, 'blocks', 'modules/websites/', 'BL1', 'icon icon-blocks-web', '#FEBF10', 0, 'components/blocks.js', 'assets/css/dist/blocks.css', 'blocks', 'block_', '', 1);
INSERT INTO `modules` VALUES (6, 'Publicaciones', NULL, 'publications', 'modules/websites/', 'PUB1', 'icon icon-webpart', '#FEBF10', 0, 'components/publications.js', 'assets/css/dist/publications.css', 'publications', 'pub_', '', 1);
INSERT INTO `modules` VALUES (10, 'Dashboard WebSites', NULL, 'websites', 'modules/websites/', 'WEB1', 'icon icon-dashboard', '#99C14C', 0, 'components/websites.js', 'assets/css/dist/websites.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (11, 'Contenidos', NULL, 'contents', 'modules/websites/', 'CON1', 'icon icon-content', '#1ff4ed', 0, 'components/contents.js', 'assets/css/dist/contents.min.css', 'contents', 'cont_', 'contents_categorys:cont_cat_cont_id,contents_files:cont_file_cont_id', 1);
INSERT INTO `modules` VALUES (12, 'Categorias', NULL, 'categorys', 'modules/websites/', 'CAT1', 'icon icon-category', '#dd4b4d', 0, 'components/categorys.js', 'assets/css/dist/categorys.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (13, 'Media', NULL, 'media', 'modules/websites/', 'MED1', 'icon icon-media', '#f04f04', 0, 'components/media.js', 'assets/css/dist/media.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (14, 'Documentos', NULL, 'docs', 'modules/websites/', 'DOC1', 'icon icon-folder', '#FEBF10', 0, 'components/docs.js', 'assets/css/dist/docs.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (15, 'Posts', NULL, 'posts', 'modules/websites/', 'POST1', 'icon icon-news', '#37ff2c', 0, 'components/posts.js', 'assets/css/dist/posts.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (16, 'Enlaces', NULL, 'links', 'modules/websites/', 'LINK1', 'icon icon-link', '#26fff6', 0, 'components/links.js', 'assets/css/dist/links.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (17, 'Sliders', NULL, 'sliders', 'modules/websites/', 'SLD1', 'icon icon-slider', '#8900ff', 0, 'components/sliders.js', 'assets/css/dist/sliders.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (200, 'Dashboard RRHH', NULL, 'rrhh', 'modules/rrhh/', 'RRHH', 'icon icon-category', '#99C14C', 0, 'components/rrhh.js', 'assets/css/dist/rrhh.min.css', '', '', '', 0);
INSERT INTO `modules` VALUES (210, 'kardex', NULL, 'kardex', 'modules/rrhh/', 'KDX', 'icon icon-users', '#FEBF10', 0, 'components/kardex.js', '', '', '', '', 0);
INSERT INTO `modules` VALUES (220, 'Organigrama', NULL, 'organizationChart', 'modules/rrhh/', 'KDX', 'icon icon-users', '#FEBF10', 0, 'components/organizationChart.js', 'assets/css/dist/organizationChart', '', '', '', 0);
INSERT INTO `modules` VALUES (400, 'Dashboard Inventario', NULL, 'inventory', 'modules/inventory/', 'INV', 'icon icon-dashboard', '#FEBF10', 0, 'components/inventory.js', 'assets/css/dist/inventory.css', '', '', '', 1);
INSERT INTO `modules` VALUES (401, 'Productos', NULL, 'products', 'modules/inventory/', 'PRO', 'icon icon-box', '#CCC', 0, 'components/products.js', 'assets/css/dist/products.css', 'mod_products', 'mod_prod_', '', 1);
INSERT INTO `modules` VALUES (402, 'Stock', NULL, 'stock', 'modules/inventory/', 'STC', 'icon icon-unchecked', '#99C14C', 0, 'components/stock.js', 'assets/css/dist/stock.css', '', '', '', 1);
INSERT INTO `modules` VALUES (403, 'Lista de Precios', NULL, 'pricelist', 'modules/inventory/', 'STC', 'icon icon-coin', '#99C14C', 0, 'components/pricelist.js', 'assets/css/dist/pricelist.css', '', '', '', 1);
INSERT INTO `modules` VALUES (500, 'Dashboard Contabilidad', NULL, 'accounting', 'modules/accounting/', 'ACC', 'icon icon-dashboard', '#99C14C', 0, 'components/accounting.js', 'assets/css/dist/accounting.css', '', '', '', 1);
INSERT INTO `modules` VALUES (600, 'Dashboard Consejeria', NULL, 'counseling', 'modules/counseling/', NULL, 'icon icon-dashboard', '#99C14C', 0, 'components/counseling.js', '', '', '', '', 1);
INSERT INTO `modules` VALUES (601, 'Aconsejados', NULL, 'advised', 'modules/counseling/', NULL, 'icon icon-users', '#FEBF10', 0, 'components/advised.js', '', '', '', '', 1);
INSERT INTO `modules` VALUES (602, 'Agenda', NULL, 'calendarAdvised', 'modules/counseling/', NULL, 'icon icon-calendar', '#FEBF10', 0, 'components/calendarAdvised.js', 'assets/css/dist/calendarAdvised.css', '', '', '', 1);
INSERT INTO `modules` VALUES (603, 'Reportes', NULL, 'reportsCounseling', 'modules/counseling/', NULL, 'icon icon-table-check', '#FEBF10', 0, 'components/reportCounseling.js', 'assets/css/dist/reportCounseling.css', '', '', '', 1);
INSERT INTO `modules` VALUES (700, 'Dashboard Ventas', NULL, 'sales', 'modules/sales/', 'PVD', 'icon icon-dashboard', '#fcc54e', 0, 'components/sales.js', 'assets/css/dist/sales.css', '', '', '', 1);
INSERT INTO `modules` VALUES (701, 'Punto de Venta Tickets', NULL, 'salesPointTickets', 'modules/sales/', 'PVT', 'icon icon-cash-register', '#fcc54e', 0, 'components/salesPointTickets.js', 'assets/css/dist/salesPointTickets.css', '', '', '', 1);
INSERT INTO `modules` VALUES (800, 'Dashboard Brokers', NULL, 'brokers', 'modules/brokers/', 'PV', 'icon icon-dashboard', '#fcc54e', 0, 'components/brokers.js', 'assets/css/dist/brokers.css', '', '', '', 1);
INSERT INTO `modules` VALUES (801, 'Clientes', NULL, 'brokersCustomers', 'modules/brokers/', 'PV', 'icon icon-users', '#fcc54e', 0, 'components/brokersCustomers.js', 'assets/css/dist/brokersCustomers.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1000, 'Dashboard Agendas', NULL, 'appointments', 'modules/appointments/', 'AG1', 'icon icon-dashboard', '#99C14', 0, 'components/appointments.js', 'assets/css/dist/appointments.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1001, 'Agenda', NULL, 'agenda', 'modules/appointments/', 'AA1', 'icon icon-user', '#8900ff', 0, 'components/agenda.js', 'assets/css/dist/agenda.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1002, 'Calendario', NULL, 'calendars', 'modules/appointments/', 'AC1', 'icon icon-calendar', '#07c472', 0, 'components/calendars.js', 'assets/css/dist/calendars.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1003, 'Servicios', NULL, 'appointmentsServices', 'modules/appointments/', 'AS1', 'icon icon-folder-open', '#07c472', 0, 'components/appointmentsServices.js', 'assets/css/dist/appointmentsServices.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1200, 'Dashboard FLS', NULL, 'fls', 'modules/fls/', 'FLS', 'icon icon-dashboard', '#fcc54e', 0, 'components/fls.js', 'assets/css/dist/fls.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1201, 'Clientes', NULL, 'flsCustomers', 'modules/fls/', 'PV', 'icon icon-users', '#fcc54e', 0, 'components/flsCustomers.js', 'assets/css/dist/flsCustomers.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1300, 'Dashboard Boletería', NULL, 'tickets', 'modules/tickets/', 'TKS', 'icon icon-dashboard', '#fcc54e', 0, 'components/tickets.js', 'assets/css/dist/tickets.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1301, 'Eventos', NULL, 'ticketingEvents', 'modules/tickets/', 'EVTKS', 'icon icon-tag', '#fcc54e', 0, 'components/ticketingEvents.js', 'assets/css/dist/ticketingEvents.min.css', 'mod_events', 'mod_eve_', '', 1);
INSERT INTO `modules` VALUES (1400, 'Dashboard Rehabilitación', NULL, 'rehabilitation', 'modules/rehabilitation/', 'RHB', 'icon icon-dashboard', '#fcc54e', 0, 'components/rehabilitation.js', 'assets/css/dist/rehabilitation.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1401, 'Registro', NULL, 'rehabilitationRegistry', 'modules/rehabilitation/', 'RHBR', 'icon icon-user', '#fcc54e', 0, 'components/rehabilitationRegistry.js', 'assets/css/dist/rehabilitationRegistry.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1500, 'Dashboard Ads', NULL, 'ads', 'modules/ads/', 'ADS1', 'icon icon-dashboard', '#8900ff', 0, 'components/ads.js', 'assets/css/dist/ads.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1501, 'Lugares', NULL, 'places', 'modules/ads/', 'ADS1', 'icon icon-pointer', '#07c472', 0, 'components/places.js', 'assets/css/dist/places.min.css', '', '', '', 0);
INSERT INTO `modules` VALUES (1502, 'Campañas', NULL, 'campaignsAds', 'modules/ads/', 'ADS1', 'icon icon-circle-point', '#07c472', 0, 'components/campaignsAds.js', 'assets/css/dist/campaignsAds.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1503, 'Display', NULL, 'displayAds', 'modules/ads/', 'ADS2', 'icon icon-copy', '#07c472', 0, 'components/displayAds.js', 'assets/css/dist/displayAds.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1504, 'Avisos', NULL, 'advertisements', 'modules/ads/', 'ADS1', 'icon icon-blocks', '#073472', 0, 'components/advertisements.js', 'assets/css/dist/advertisements.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1600, 'Dashboard LMS', NULL, 'lms', 'modules/lms/', 'ADS1', 'icon icon-dashboard', '#8900ff', 0, 'components/lms.js', 'assets/css/dist/lms.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1601, 'Plan de Estudios', NULL, 'syllabus', 'modules/lms/', 'SB1', 'icon icon-category-r', '#8900ff', 0, 'components/syllabus.js', 'assets/css/dist/syllabus.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1700, 'Dashboard Activo Fijo', NULL, 'fixedassets', 'modules/fixedassets/', 'AF1', 'icon icon-dashboard', '#8900ff', 0, 'components/fixedassets.js', 'assets/css/dist/fixedassets.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1701, 'Activos', NULL, 'assets', 'modules/fixedassets/', 'ASS1', 'icon icon-tag-qr', '#8900ff', 0, 'components/assets.js', 'assets/css/dist/assets.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1800, 'Dashboard Operaciones', NULL, 'operations', 'modules/operations/', 'OP1', 'icon icon-dashboard', '#8900ff', 0, 'components/operations.js', 'assets/css/dist/operations.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1900, 'Dashboard CRM', NULL, 'crm', 'modules/crm/', 'CRM1', 'icon icon-dashboard', '#8900ff', 0, 'components/crm.js', 'assets/css/dist/crm.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1901, 'Clientes', NULL, 'customers', 'modules/accounts/', 'CS1', 'icon icon-folder-open', '#8900ff', 0, 'components/customers.js', 'assets/css/dist/customers.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (1902, 'Suscripciones', NULL, 'suscriptionsCrm', 'modules/crm/', 'SC1', 'icon icon-bell', '#99C144', 0, 'components/subscriptionsCrm.js', 'assets/css/dist/suscriptionsCrm.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (2000, 'Dashboard', NULL, 'restaurants', 'modules/restaurants/', 'RST1', 'icon icon-dashboard', '#8900ff', 0, 'components/restaurants.js', 'assets/css/dist/restaurants.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (2001, 'Ordenes', NULL, 'commands', 'modules/restaurants/', 'CS1', 'icon icon-command', '#8900ff', 0, 'components/commands.js', 'assets/css/dist/commands.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (2100, 'Dashboard', NULL, 'externaldocuments', 'modules/externaldocuments/', 'EXD1', 'icon icon-dashboard', '#8900ff', 0, 'components/externaldocuments.js', 'assets/css/dist/externaldocuments.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (2101, 'Bandeja de Entrada', NULL, 'inboxEd', 'modules/externaldocuments/', 'IBX1', 'icon icon-inbox', '#8900ff', 0, 'components/inboxEd.js', 'assets/css/dist/inboxEd.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (2102, 'Plantillas', NULL, 'templatesEd', 'modules/externaldocuments/', 'TMP-ED1', 'icon icon-news', '#ff9000', 0, 'components/templatesEd.js', 'assets/css/dist/templatesEd.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (2103, 'Traking', NULL, 'trakingEd', 'modules/externaldocuments/', 'TRK-ED1', 'icon icon-pointer', '#ff9040', 0, 'components/trakingEd.js', 'assets/css/dist/trakingEd.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (2200, 'Dashboard Geolocalización', NULL, 'geolocation', 'modules/geolocation/', 'GEO-1', 'icon icon-dashboard', '#ff9040', 0, 'components/geolocation.js', 'assets/css/dist/geolocation.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (2201, 'Lugares', NULL, 'places', 'modules/geolocation/', 'GEO-2', 'icon icon-pointer', '#E74745', 0, 'components/places.js', 'assets/css/dist/places.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (2300, 'Dashboard Deportes', NULL, 'sports', 'modules/sports/', 'GEO-1', 'icon icon-dashboard', '#ff9040', 0, 'components/sports.js', 'assets/css/dist/sports.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (2301, 'Futbol', NULL, 'futbol', 'modules/sports/', 'GEO-2', 'icon icon-futbol', '#E74745', 0, 'components/futbol.js', 'assets/css/dist/futbol.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (2400, 'Dashboard Concursos', NULL, 'competitions', 'modules/competitions/', 'GEO-1', 'icon icon-dashboard', '#ff9040', 0, 'components/competitions.js', 'assets/css/dist/competitions.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (3600, 'Dashboard', NULL, 'ganadorapp', 'modules/ganadorapp/', 'GAPP-1', 'icon icon-dashboard', '#E74745', 0, 'components/ganadorapp.js', 'assets/css/dist/ganadorapp.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (3690, 'Proyectos', NULL, 'proyectosGA', 'modules/ganadorapp/', 'PAPP-1', 'icon icon-tag', '#E74745', 0, 'components/proyectosga.js', 'assets/css/dist/proyectosga.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (3691, 'Neoganaderos', NULL, 'neoganaderos', 'modules/ganadorapp/', 'GNG-1', 'icon icon-users', '#E74745', 0, 'components/neoganaderos.js', 'assets/css/dist/neoganadores.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (3692, 'Ganaderos', NULL, 'ganaderos', 'modules/ganadorapp/', 'GAN-1', 'icon icon-dashboard', '#E74745', 0, 'components/ganaderos.js', 'assets/css/dist/ganaderos.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (3700, 'Dashboard', NULL, 'app_tributo', 'modules/app_tributo/', 'TAPP-1', 'icon icon-dashboard', '#E74745', 0, 'components/app_tributo.js', 'assets/css/dist/app_tributo.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (3701, 'Selección', NULL, 'app_tributo_selection', 'modules/app_tributo/', 'TAPP-2', 'icon icon-tag', '#E74745', 0, 'components/app_tributo_selection.js', 'assets/css/dist/app_tributo_selection.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (3800, 'Dashboard', NULL, 'app_candire', 'modules/app_candire/', 'CAPP-1', 'icon icon-dashboard', '#E74745', 0, 'components/app_candire.js', 'assets/css/dist/app_candire.min.css', '', '', '', 1);
INSERT INTO `modules` VALUES (3801, 'Campañas', NULL, 'app_candire_campaigns', 'modules/app_candire/', 'CAPP-2', 'icon icon-tag', '#E74745', 0, 'components/app_candire_campaigns.js', 'assets/css/dist/app_candire_campaigns.min.css', '', '', '', 1);
COMMIT;

-- ----------------------------
-- Table structure for modules_categorys
-- ----------------------------
DROP TABLE IF EXISTS `modules_categorys`;
CREATE TABLE `modules_categorys` (
  `mod_cat_mod_id` int NOT NULL,
  `mod_cat_cat_id` int NOT NULL,
  `mod_cat_ent_id` int NOT NULL,
  `mod_cat_order` int NOT NULL,
  PRIMARY KEY (`mod_cat_mod_id`,`mod_cat_cat_id`,`mod_cat_ent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of modules_categorys
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for notifications
-- ----------------------------
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `ntf_id` int NOT NULL AUTO_INCREMENT,
  `ntf_recipient_id` int NOT NULL COMMENT 'Id del destinatario',
  `ntf_recipient_type` varchar(255) NOT NULL COMMENT 'Tipo de destinatario (customer,provider)',
  `ntf_message` text NOT NULL,
  `ntf_sent_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ntf_status` varchar(255) DEFAULT 'pending,send,delivered',
  `ntf_ent_id` int NOT NULL,
  `ntf_custom_data` json DEFAULT NULL,
  `ntf_class` varchar(255) DEFAULT NULL,
  `ntf_custom_id` varchar(255) DEFAULT NULL,
  `ntf_state` int DEFAULT '0',
  PRIMARY KEY (`ntf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of notifications
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for options
-- ----------------------------
DROP TABLE IF EXISTS `options`;
CREATE TABLE `options` (
  `option_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `option_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `autoload` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of options
-- ----------------------------
BEGIN;
INSERT INTO `options` VALUES (1, 'site_title', 'Ion', 'yes');
INSERT INTO `options` VALUES (2, 'site_favicon', 'assets/img/favicon.png', 'yes');
INSERT INTO `options` VALUES (3, 'site_img', 'assets/img/logo.svg', 'yes');
INSERT INTO `options` VALUES (4, 'site_version', '22662', 'yes');
INSERT INTO `options` VALUES (5, 'user_id_default', '3', 'yes');
INSERT INTO `options` VALUES (6, 'bearer_token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3MjM0OTIzODczNDkiLCJuYW1lIjoibnVjbGVvIiwiaWF0Ijo5MzI0MjQzNDl9.jIsiHIdOGa-KHCJ2mUhLLKqlvTYVLolUyrp1HM6E7Cs-gedeon', 'yes');
INSERT INTO `options` VALUES (7, 'client_id', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9-gedeon', 'yes');
INSERT INTO `options` VALUES (8, 'client_secret', 'eyJzdWIiOiI3MjM0OTIzODczNDkiLCJuYW1lIjoibnVjbGVvIiwiaWF0Ijo5MzI0MjQzNDl9-gedeon', 'yes');
INSERT INTO `options` VALUES (9, 'locale', 'es_ES', 'yes');
INSERT INTO `options` VALUES (10, 'timezone', 'America/La_Paz', 'yes');
INSERT INTO `options` VALUES (12, 'site-meta', 'sites/default/controllers/pub/meta.pub.php', 'yes');
INSERT INTO `options` VALUES (13, 'entitie_id_default', '1', 'yes');
INSERT INTO `options` VALUES (14, 'dashboard_img', 'assets/img/logo.svg', 'yes');
INSERT INTO `options` VALUES (15, 'dashboard_favicon', 'assets/img/favicon.png', 'yes');
INSERT INTO `options` VALUES (16, 'dashboard_brand', 'assets/img/logo.svg', 'yes');
INSERT INTO `options` VALUES (17, 'path_icons_pub', 'assets/img/pubs', 'yes');
COMMIT;

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `post_id` int NOT NULL AUTO_INCREMENT,
  `post_title` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_pathurl` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_tags` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_review` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `post_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0',
  `post_img_ref` varchar(445) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_embed` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `post_preview_embed` int DEFAULT '0',
  `post_intern_embed` int DEFAULT '0',
  `post_video` int DEFAULT '0',
  `post_preview_video` int DEFAULT '0',
  `post_intern_video` int DEFAULT '0',
  `post_body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `post_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `post_author` int DEFAULT NULL,
  `post_cls` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_top_relations` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_middle_relations` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_last_relations` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `post_date` datetime DEFAULT NULL,
  `post_register_date` datetime NOT NULL,
  `post_ent_id` int DEFAULT NULL,
  `post_user_id` int DEFAULT NULL,
  `post_state` int DEFAULT '0',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of posts
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for posts_authors
-- ----------------------------
DROP TABLE IF EXISTS `posts_authors`;
CREATE TABLE `posts_authors` (
  `post_au_id` int NOT NULL AUTO_INCREMENT,
  `post_au_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_au_summary` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `post_au_img` int DEFAULT '0',
  `post_au_ent_id` int DEFAULT NULL,
  `post_au_contact` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `post_au_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_au_state` int NOT NULL,
  PRIMARY KEY (`post_au_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of posts_authors
-- ----------------------------
BEGIN;
INSERT INTO `posts_authors` VALUES (1, '', '', 0, 1, '', '', 1);
COMMIT;

-- ----------------------------
-- Table structure for posts_categorys
-- ----------------------------
DROP TABLE IF EXISTS `posts_categorys`;
CREATE TABLE `posts_categorys` (
  `post_cat_post_id` int NOT NULL,
  `post_cat_cat_id` int NOT NULL,
  `post_cat_ent_id` int DEFAULT NULL,
  `post_cat_order` int NOT NULL,
  PRIMARY KEY (`post_cat_post_id`,`post_cat_cat_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of posts_categorys
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for posts_files
-- ----------------------------
DROP TABLE IF EXISTS `posts_files`;
CREATE TABLE `posts_files` (
  `post_file_post_id` int NOT NULL,
  `post_file_file_id` int NOT NULL,
  `post_file_order` int DEFAULT NULL,
  PRIMARY KEY (`post_file_post_id`,`post_file_file_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of posts_files
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for publications
-- ----------------------------
DROP TABLE IF EXISTS `publications`;
CREATE TABLE `publications` (
  `pub_id` int NOT NULL AUTO_INCREMENT,
  `pub_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_description` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `pub_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_summary` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `pub_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_path_ui` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_path_icon` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_icon` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_class` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_attr_id` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_attr` varchar(445) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_count` int DEFAULT NULL,
  `pub_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `pub_html` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `pub_state` int DEFAULT '0',
  PRIMARY KEY (`pub_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of publications
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for publications_pattern
-- ----------------------------
DROP TABLE IF EXISTS `publications_pattern`;
CREATE TABLE `publications_pattern` (
  `pub_pat_id` int NOT NULL AUTO_INCREMENT,
  `pub_pat_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `pub_pat_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `pub_pat_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `pub_pat_ent_id` int DEFAULT '0',
  `pub_pat_order` int DEFAULT '0',
  `pub_pat_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`pub_pat_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of publications_pattern
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for publications_relations
-- ----------------------------
DROP TABLE IF EXISTS `publications_relations`;
CREATE TABLE `publications_relations` (
  `pub_rel_id` int NOT NULL AUTO_INCREMENT,
  `pub_rel_cat_id` int DEFAULT NULL,
  `pub_rel_ws_id` int DEFAULT NULL,
  `pub_rel_block_id` int DEFAULT NULL,
  `pub_rel_pub_id` int DEFAULT NULL,
  `pub_rel_state` int NOT NULL DEFAULT '1',
  `pub_rel_order` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`pub_rel_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of publications_relations
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `rol_id` int NOT NULL AUTO_INCREMENT,
  `rol_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `rol_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `rol_parent_id` int DEFAULT NULL,
  `rol_redirection_url` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `rol_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `rol_state` int DEFAULT '0',
  PRIMARY KEY (`rol_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
BEGIN;
INSERT INTO `roles` VALUES (1, 'Super Administrador', NULL, 0, '{_PATH_WEB}dashboard', NULL, 1);
INSERT INTO `roles` VALUES (2, 'Administrador', NULL, 1, '{_PATH_WEB}dashboard', NULL, 1);
INSERT INTO `roles` VALUES (3, 'Administrador de Entidad', NULL, 2, '{_PATH_WEB}dashboard', NULL, 1);
INSERT INTO `roles` VALUES (4, 'Administrador RRHH', NULL, 2, '{_PATH_WEB}dashboard', NULL, 1);
INSERT INTO `roles` VALUES (10, 'Encargado de Contabilidad', NULL, 2, '{_PATH_WEB}dashboard', NULL, 1);
COMMIT;

-- ----------------------------
-- Table structure for roles_categorys
-- ----------------------------
DROP TABLE IF EXISTS `roles_categorys`;
CREATE TABLE `roles_categorys` (
  `rol_cat_rol_id` int NOT NULL,
  `rol_cat_cat_id` int NOT NULL,
  `rol_cat_order` int NOT NULL,
  PRIMARY KEY (`rol_cat_rol_id`,`rol_cat_cat_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of roles_categorys
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for roles_entities
-- ----------------------------
DROP TABLE IF EXISTS `roles_entities`;
CREATE TABLE `roles_entities` (
  `rol_ent_rol_id` int NOT NULL,
  `rol_ent_ent_id` int NOT NULL,
  PRIMARY KEY (`rol_ent_rol_id`,`rol_ent_ent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of roles_entities
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for roles_modules
-- ----------------------------
DROP TABLE IF EXISTS `roles_modules`;
CREATE TABLE `roles_modules` (
  `rol_mod_rol_id` int NOT NULL,
  `rol_mod_mod_id` int NOT NULL,
  `rol_mod_ent_id` int NOT NULL,
  `rol_mod_order` int NOT NULL,
  `rol_mod_permits` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0,0,0,0,0' COMMENT 'See,Publish,Add,Edit,Delete',
  PRIMARY KEY (`rol_mod_mod_id`,`rol_mod_rol_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of roles_modules
-- ----------------------------
BEGIN;
INSERT INTO `roles_modules` VALUES (5, 500, 1, 1, '0,0,0,0,0');
INSERT INTO `roles_modules` VALUES (6, 500, 1, 0, '0,0,0,0,0');
INSERT INTO `roles_modules` VALUES (7, 500, 1, 0, '0,0,0,0,0');
INSERT INTO `roles_modules` VALUES (5, 501, 1, 1, '0,0,0,0,0');
INSERT INTO `roles_modules` VALUES (6, 501, 1, 0, '0,0,0,0,0');
INSERT INTO `roles_modules` VALUES (7, 501, 1, 1, '0,0,0,0,0');
INSERT INTO `roles_modules` VALUES (10, 1000, 1, 1, '0,0,0,0,0');
INSERT INTO `roles_modules` VALUES (10, 1001, 1, 1, '0,0,0,0,0');
COMMIT;

-- ----------------------------
-- Table structure for roles_sites
-- ----------------------------
DROP TABLE IF EXISTS `roles_sites`;
CREATE TABLE `roles_sites` (
  `rol_site_rol_id` int NOT NULL,
  `rol_site_site_id` int NOT NULL,
  `rol_site_order` int NOT NULL,
  PRIMARY KEY (`rol_site_rol_id`,`rol_site_site_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of roles_sites
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for roles_systems
-- ----------------------------
DROP TABLE IF EXISTS `roles_systems`;
CREATE TABLE `roles_systems` (
  `rol_sys_rol_id` int NOT NULL,
  `rol_sys_sys_id` int NOT NULL,
  `rol_sys_ent_id` int NOT NULL,
  `rol_sys_order` int DEFAULT '0',
  PRIMARY KEY (`rol_sys_rol_id`,`rol_sys_sys_id`,`rol_sys_ent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of roles_systems
-- ----------------------------
BEGIN;
INSERT INTO `roles_systems` VALUES (2, 6, 1, 0);
INSERT INTO `roles_systems` VALUES (5, 6, 1, 1);
INSERT INTO `roles_systems` VALUES (6, 6, 1, 2);
INSERT INTO `roles_systems` VALUES (7, 6, 1, 0);
INSERT INTO `roles_systems` VALUES (10, 5, 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for sites
-- ----------------------------
DROP TABLE IF EXISTS `sites`;
CREATE TABLE `sites` (
  `site_id` int NOT NULL AUTO_INCREMENT,
  `site_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `site_path_web` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `site_path_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `site_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `site_head` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `site_head_path_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `site_default` int NOT NULL DEFAULT '0',
  `site_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`site_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of sites
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sites_entities
-- ----------------------------
DROP TABLE IF EXISTS `sites_entities`;
CREATE TABLE `sites_entities` (
  `site_ent_site_id` int NOT NULL,
  `site_ent_ent_id` int NOT NULL,
  `site_ent_order` int NOT NULL,
  PRIMARY KEY (`site_ent_site_id`,`site_ent_ent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of sites_entities
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for systems
-- ----------------------------
DROP TABLE IF EXISTS `systems`;
CREATE TABLE `systems` (
  `sys_id` int NOT NULL AUTO_INCREMENT,
  `sys_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sys_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `sys_pathurl` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sys_path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sys_mod_default` int NOT NULL DEFAULT '0',
  `sys_code` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `sys_icon` varchar(240) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sys_color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sys_parent_id` int DEFAULT NULL,
  `sys_indexjs` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `sys_css` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `sys_order` int NOT NULL,
  `sys_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`sys_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of systems
-- ----------------------------
BEGIN;
INSERT INTO `systems` VALUES (1, 'Websites', NULL, 'websites', 'modules/websites/', 10, '', 'icon icon-code', '#6F91E9', 0, 'components/websites.js', 'assets/css/dist/websites.css', 0, 1);
INSERT INTO `systems` VALUES (2, 'Recursos Humanos', NULL, 'rrhh', 'modules/rrhh/', 200, '', 'icon icon-category', '#6F91E9', 0, 'components/rrhh.js', 'assets/css/dist/rrhh.css', 0, 1);
INSERT INTO `systems` VALUES (4, 'Inventario', NULL, 'inventory', 'modules/inventory/', 400, '', 'icon icon-box', '#FEBF10', 0, 'components/inventory.js', 'assets/css/dist/inventory.css', 0, 1);
INSERT INTO `systems` VALUES (5, 'Contabilidad', NULL, 'accounting', 'modules/accounting/', 500, '', 'icon icon-coin', '#6F91E9', 0, 'components/accounting.js', 'assets/css/dist/accounting.css', 0, 1);
INSERT INTO `systems` VALUES (6, 'Consejeria', NULL, 'counseling', 'modules/counseling/', 600, '', 'icon icon-users', '#27d27c', 0, 'components/counseling.js', 'assets/css/dist/counseling.css', 0, 1);
INSERT INTO `systems` VALUES (7, 'Ventas', NULL, 'sales', 'modules/sales/', 700, '', 'icon icon-sales', '#fc9835', 0, 'components/sales.js', 'assets/css/dist/sales.css', 0, 0);
INSERT INTO `systems` VALUES (8, 'Brokers', NULL, 'brokers', 'modules/brokers/', 800, '', 'icon icon-circle-doble', '#27d27c', 0, 'components/brokers.js', 'assets/css/dist/brokers.css', 0, 0);
INSERT INTO `systems` VALUES (10, 'Agendas', NULL, 'appointments', 'modules/appointments/', 1000, '', 'icon icon-calendar', '#1be5fc', 0, 'components/appointments.js', 'assets/css/dist/appointments.css', 0, 1);
INSERT INTO `systems` VALUES (12, 'Finalcial Services Leeds', NULL, 'fls', 'modules/fls/', 1200, '', 'icon icon-circle', '#27d27c', 0, 'components/fls.js', 'assets/css/dist/fls.css', 0, 0);
INSERT INTO `systems` VALUES (13, 'Boletería', NULL, 'tickets', 'modules/tickets/', 1300, '', 'icon icon-tag', '#27d27c', 0, 'components/tickets.js', 'assets/css/dist/tickets.min.css', 0, 1);
INSERT INTO `systems` VALUES (14, 'Rehabilitación', NULL, 'rehabilitation', 'modules/rehabilitation/', 1400, '', 'icon icon-user', '#6F91E', 0, 'components/rehabilitation.js', 'assets/css/dist/rehabilitation.min.css', 0, 1);
INSERT INTO `systems` VALUES (15, 'Ads', NULL, 'ads', 'modules/ads/', 1500, '', 'icon icon-loudspeaker', '#e71882', 0, 'components/ads.js', 'assets/css/dist/ads.min.css', 0, 1);
INSERT INTO `systems` VALUES (16, 'LMS', NULL, 'lms', 'modules/lms/', 1600, '', 'icon icon-graduation', '#FEBF10', 0, 'components/lms.js', 'assets/css/dist/lms.min.css', 0, 1);
INSERT INTO `systems` VALUES (17, 'Activo Fijo', NULL, 'fixedassets', 'modules/fixedassets/', 1700, '', 'icon icon-fixed-assets', '#9c27b0', 0, 'components/fixedassets.js', 'assets/css/dist/fixedassets.min.css', 0, 1);
INSERT INTO `systems` VALUES (18, 'Operaciones', NULL, 'operations', 'modules/operations/', 1800, '', 'icon icon-operations', '#258dfc', 0, 'components/operations.js', 'assets/css/dist/operations.min.css', 0, 1);
INSERT INTO `systems` VALUES (19, 'CRM', NULL, 'crm', 'modules/crm/', 1900, '', 'icon icon-crm', '#ef4848', 0, 'components/crm.js', 'assets/css/dist/crm.min.css', 0, 1);
INSERT INTO `systems` VALUES (20, 'Restaurant', NULL, 'restaurants', 'modules/restaurants/', 2000, '', 'icon icon-restaurant', '#6b6fa9', 0, 'components/restaurants.js', 'assets/css/dist/restaurants.min.css', 0, 1);
INSERT INTO `systems` VALUES (21, 'Doc. Externos', NULL, 'externaldocuments', 'modules/externaldocuments/', 2100, '', 'icon icon-doc', '#6b6fa9', 0, 'components/externaldocuments.js', 'assets/css/dist/externaldocuments.min.css', 0, 1);
INSERT INTO `systems` VALUES (22, 'Geoposición', NULL, 'geolocation', 'modules/geolocation/', 2200, '', 'icon icon-pointer-route', '#00d853', 0, 'components/geolocation.js', 'assets/css/dist/geolocation.min.css', 0, 1);
INSERT INTO `systems` VALUES (23, 'Deportes', NULL, 'sports', 'modules/sports/', 2200, '', 'icon icon-sport', '#ff7629', 0, 'components/sports.js', 'assets/css/dist/sports.min.css', 0, 1);
INSERT INTO `systems` VALUES (24, 'Concursos', NULL, 'competitions', 'modules/competitions/', 2400, '', 'icon icon-copy', '#ff3333', 0, 'components/competitions.js', 'assets/css/dist/competitions.min.css', 0, 1);
INSERT INTO `systems` VALUES (36, 'Ganador App', NULL, 'ganadorapp', 'modules/app_ganador/', 3600, '', 'icon icon-table-check', '#00d767', 0, 'components/app_ganador.js', 'assets/css/dist/app_ganador.min.css', 0, 1);
INSERT INTO `systems` VALUES (37, 'Tributo App', NULL, 'app_tributo', 'modules/app_tributo/', 3700, '', 'icon icon-coin', '#003767', 0, 'components/app_tributo.js', 'assets/css/dist/app_tributo.min.css', 0, 1);
INSERT INTO `systems` VALUES (38, 'Candire App', NULL, 'app_candire', 'modules/app_candire/', 3800, '', 'icon icon-circle-checkmark', '#003800', 0, 'components/app_candire.js', 'assets/css/dist/app_candire.min.css', 0, 1);
COMMIT;

-- ----------------------------
-- Table structure for systems_modules
-- ----------------------------
DROP TABLE IF EXISTS `systems_modules`;
CREATE TABLE `systems_modules` (
  `sys_mod_sys_id` int NOT NULL,
  `sys_mod_mod_id` int NOT NULL,
  `sys_mod_ent_id` int NOT NULL,
  `sys_mod_order` int NOT NULL,
  PRIMARY KEY (`sys_mod_sys_id`,`sys_mod_mod_id`,`sys_mod_ent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of systems_modules
-- ----------------------------
BEGIN;
INSERT INTO `systems_modules` VALUES (1, 10, 1, 1);
INSERT INTO `systems_modules` VALUES (1, 11, 1, 2);
INSERT INTO `systems_modules` VALUES (1, 12, 1, 3);
INSERT INTO `systems_modules` VALUES (1, 13, 1, 4);
INSERT INTO `systems_modules` VALUES (1, 14, 1, 5);
INSERT INTO `systems_modules` VALUES (1, 15, 1, 6);
INSERT INTO `systems_modules` VALUES (1, 16, 1, 7);
INSERT INTO `systems_modules` VALUES (1, 17, 1, 8);
INSERT INTO `systems_modules` VALUES (2, 200, 1, 1);
INSERT INTO `systems_modules` VALUES (2, 210, 1, 2);
INSERT INTO `systems_modules` VALUES (2, 220, 1, 3);
INSERT INTO `systems_modules` VALUES (4, 400, 1, 1);
INSERT INTO `systems_modules` VALUES (4, 401, 1, 2);
INSERT INTO `systems_modules` VALUES (4, 402, 1, 1);
INSERT INTO `systems_modules` VALUES (4, 403, 1, 2);
INSERT INTO `systems_modules` VALUES (5, 500, 1, 1);
INSERT INTO `systems_modules` VALUES (6, 600, 1, 1);
INSERT INTO `systems_modules` VALUES (6, 601, 1, 2);
INSERT INTO `systems_modules` VALUES (6, 602, 1, 3);
INSERT INTO `systems_modules` VALUES (6, 603, 1, 4);
INSERT INTO `systems_modules` VALUES (7, 700, 1, 1);
INSERT INTO `systems_modules` VALUES (7, 701, 1, 2);
INSERT INTO `systems_modules` VALUES (8, 800, 1, 1);
INSERT INTO `systems_modules` VALUES (8, 801, 1, 2);
INSERT INTO `systems_modules` VALUES (10, 1000, 1, 1);
INSERT INTO `systems_modules` VALUES (10, 1001, 1, 2);
INSERT INTO `systems_modules` VALUES (10, 1002, 1, 3);
INSERT INTO `systems_modules` VALUES (10, 1003, 1, 4);
INSERT INTO `systems_modules` VALUES (12, 1200, 1, 1);
INSERT INTO `systems_modules` VALUES (12, 1201, 1, 2);
INSERT INTO `systems_modules` VALUES (13, 1300, 1, 1);
INSERT INTO `systems_modules` VALUES (13, 1301, 1, 2);
INSERT INTO `systems_modules` VALUES (14, 1400, 1, 1);
INSERT INTO `systems_modules` VALUES (14, 1401, 1, 2);
INSERT INTO `systems_modules` VALUES (15, 1500, 1, 1);
INSERT INTO `systems_modules` VALUES (15, 1501, 1, 2);
INSERT INTO `systems_modules` VALUES (15, 1502, 1, 3);
INSERT INTO `systems_modules` VALUES (15, 1503, 1, 4);
INSERT INTO `systems_modules` VALUES (15, 1504, 1, 5);
INSERT INTO `systems_modules` VALUES (16, 1600, 1, 1);
INSERT INTO `systems_modules` VALUES (16, 1601, 1, 2);
INSERT INTO `systems_modules` VALUES (17, 1700, 1, 1);
INSERT INTO `systems_modules` VALUES (17, 1701, 1, 2);
INSERT INTO `systems_modules` VALUES (18, 1800, 1, 1);
INSERT INTO `systems_modules` VALUES (19, 1900, 1, 1);
INSERT INTO `systems_modules` VALUES (19, 1901, 1, 2);
INSERT INTO `systems_modules` VALUES (19, 1902, 1, 2);
INSERT INTO `systems_modules` VALUES (20, 2000, 1, 1);
INSERT INTO `systems_modules` VALUES (20, 2001, 1, 2);
INSERT INTO `systems_modules` VALUES (21, 2100, 1, 1);
INSERT INTO `systems_modules` VALUES (21, 2101, 1, 2);
INSERT INTO `systems_modules` VALUES (21, 2102, 1, 2);
INSERT INTO `systems_modules` VALUES (21, 2103, 1, 2);
INSERT INTO `systems_modules` VALUES (22, 2201, 1, 1);
INSERT INTO `systems_modules` VALUES (23, 2300, 1, 1);
INSERT INTO `systems_modules` VALUES (23, 2301, 1, 2);
INSERT INTO `systems_modules` VALUES (24, 2400, 1, 1);
INSERT INTO `systems_modules` VALUES (36, 3600, 1, 1);
INSERT INTO `systems_modules` VALUES (36, 3690, 1, 2);
INSERT INTO `systems_modules` VALUES (36, 3691, 1, 3);
INSERT INTO `systems_modules` VALUES (36, 3692, 1, 4);
INSERT INTO `systems_modules` VALUES (37, 3700, 1, 1);
INSERT INTO `systems_modules` VALUES (37, 3701, 1, 2);
INSERT INTO `systems_modules` VALUES (38, 3800, 1, 1);
INSERT INTO `systems_modules` VALUES (38, 3801, 1, 2);
INSERT INTO `systems_modules` VALUES (38, 3802, 1, 3);
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_lastname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_password` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_img` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_level` int NOT NULL DEFAULT '0',
  `user_state` int DEFAULT '0',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'admin', 'general', 'adm@wappcom.com', 'd2FwcGNvbSM=', 'https://lh3.googleusercontent.com/a-/AOh14Gg0Q2OTE5-h1lN_wfEqTXCcrB_gY0tSmuF8UIi6Kw=s96-c', 'admin', 0, 1);
COMMIT;

-- ----------------------------
-- Table structure for users_firms
-- ----------------------------
DROP TABLE IF EXISTS `users_firms`;
CREATE TABLE `users_firms` (
  `user_firm_id` int NOT NULL AUTO_INCREMENT,
  `user_firm_user_id` int NOT NULL,
  `user_firm_img` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_firm_reference` int DEFAULT NULL,
  `user_firm_md5` int DEFAULT NULL,
  `user_firm_state` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_firm_id`,`user_firm_user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of users_firms
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups` (
  `user_group_user_id` int NOT NULL,
  `user_group_group_id` int NOT NULL,
  `user_group_ent_id` int NOT NULL,
  `user_group_order` int NOT NULL,
  PRIMARY KEY (`user_group_user_id`,`user_group_group_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for users_path
-- ----------------------------
DROP TABLE IF EXISTS `users_path`;
CREATE TABLE `users_path` (
  `user_path_user_id` int NOT NULL,
  `user_path_mod_id` int NOT NULL,
  `user_path_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_path_order` int NOT NULL,
  PRIMARY KEY (`user_path_user_id`,`user_path_mod_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of users_path
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for users_roles
-- ----------------------------
DROP TABLE IF EXISTS `users_roles`;
CREATE TABLE `users_roles` (
  `user_rol_user_id` int NOT NULL,
  `user_rol_rol_id` int NOT NULL,
  `user_rol_ent_id` int NOT NULL,
  `user_rol_order` int NOT NULL,
  PRIMARY KEY (`user_rol_user_id`,`user_rol_rol_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of users_roles
-- ----------------------------
BEGIN;
INSERT INTO `users_roles` VALUES (1, 1, 1, 0);
INSERT INTO `users_roles` VALUES (3, 10, 1, 1);
INSERT INTO `users_roles` VALUES (4, 10, 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for users_tabs
-- ----------------------------
DROP TABLE IF EXISTS `users_tabs`;
CREATE TABLE `users_tabs` (
  `user_tab_user_id` int NOT NULL,
  `user_tab_sys_id` int NOT NULL,
  `user_tab_ent_id` int NOT NULL,
  `user_tab_sys_active` int NOT NULL,
  `user_tab_start_mod_id` int DEFAULT '0',
  `user_tab_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `user_tab_order` int NOT NULL,
  PRIMARY KEY (`user_tab_user_id`,`user_tab_sys_id`,`user_tab_ent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of users_tabs
-- ----------------------------
BEGIN;
INSERT INTO `users_tabs` VALUES (1, 1, 1, 0, 10, NULL, 1);
INSERT INTO `users_tabs` VALUES (1, 10, 1, 0, 1003, NULL, 2);
INSERT INTO `users_tabs` VALUES (1, 19, 1, 1, 1901, NULL, 3);
INSERT INTO `users_tabs` VALUES (1, 38, 1, 0, 3801, NULL, 4);
COMMIT;

-- ----------------------------
-- Table structure for users_tokens
-- ----------------------------
DROP TABLE IF EXISTS `users_tokens`;
CREATE TABLE `users_tokens` (
  `user_tk_user_id` bigint NOT NULL,
  `user_tk_type` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_tk_token` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_tk_expires_in` datetime DEFAULT NULL,
  `user_tk_date` datetime DEFAULT NULL,
  `user_tk_dates_browser` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`user_tk_type`,`user_tk_token`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of users_tokens
-- ----------------------------
BEGIN;
INSERT INTO `users_tokens` VALUES (1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC40MTg4MjQwMCAxNzMxNTk3MzU3NjczNjE0MmQ2NjQxMzkuNDc3NTQ1NzY=', NULL, '2024-11-14 11:15:57', 'Google Chrome,130.0.0.0,Windows:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/130.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC40NTcyOTQwMCAxNzMxMzc1NTEyNjczMmIxOTg2ZmE1NzIuNTM3NjE0ODI=', NULL, '2024-11-11 21:38:32', 'Google Chrome,130.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/130.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC40NzYwMzEwMCAxNzI3OTU2MzEyNjZmZTg1NTg3NDM4ODcuMDc4NjEzNjI=', NULL, '2024-10-03 07:51:52', 'Google Chrome,129.0.0.0,Windows:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC41MTc3NjMwMCAxNjUyNTU5MjUwNjI4MDBkOTI3ZTY4YjkuNTE2ODQ2MDk=', NULL, '2022-05-14 16:14:10', 'Apple Safari,604.1,Mac Os:Mozilla/5.0 (iPhone; CPU iPhone OS 15_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/101.0.4951.58 Mobile/15E148 Safari/604.1');
INSERT INTO `users_tokens` VALUES (1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC41NDk3MzAwMCAxNzI4NDEzNzAwNjcwNTgwMDQ4NjM2YTIuNjExODU1NjU=', NULL, '2024-10-08 14:55:00', 'Google Chrome,129.0.0.0,Windows:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC42NDYzNDUwMCAxNjUyNDA3MDkwNjI3ZGJiMzI5ZGNkMzMuOTIzMjgzNzA=', NULL, '2022-05-12 21:58:10', 'Google Chrome,101.0.4951.64,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC45MzkyMzQwMCAxNzMzOTQ4MTAxNjc1OWYyYzVlNTRlZjIuMjk3NzE0NDA=', NULL, '2024-12-11 16:15:01', 'Google Chrome,128.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC4xMTUxOTgwMCAxNzI3NjI1MDc4NjZmOTc3NzYxYzIwNzkuOTIwNTUyMzU=', NULL, '2024-09-29 11:51:18', 'Google Chrome,129.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC4yMDQ4OTcwMCAxNzI4NzQyMjg1NjcwYTgzOGQzMjA2YzAuMjk5Mzc1NDg=', NULL, '2024-10-12 10:11:25', 'Google Chrome,129.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC4yOTM0ODIwMCAxNzI3NzY4NTU2NjZmYmE3ZWM0N2E3MzMuOTM5NDg0MDk=', NULL, '2024-10-01 03:42:36', 'Google Chrome,129.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC4zMjMzMTkwMCAxNzI4MTgxNDkwNjcwMWY0ZjI0ZWYwMDkuNTE4MDU3OTk=', NULL, '2024-10-05 22:24:50', 'Google Chrome,129.0.0.0,Windows:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC4zNTUwNzQwMCAxNzMxNjMzNjI1NjczNmExZDk1NmIxODguNTI3NjUwMzk=', NULL, '2024-11-14 21:20:25', 'Google Chrome,131.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC4zOTAzOTkwMCAxNzMzOTQ4MTUyNjc1OWYyZjg1ZjUwOTEuMjM1NjgzMjM=', NULL, '2024-12-11 16:15:52', 'Google Chrome,128.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'refresh_token', '023b1b60bfbc82eae087c3a3288b481b', NULL, '2024-10-08 14:55:00', 'Google Chrome,129.0.0.0,Windows:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'refresh_token', '0501c9be8b395ac86993ba9a6491c492', NULL, '2024-10-05 22:24:50', 'Google Chrome,129.0.0.0,Windows:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'refresh_token', '126dd14bdd34a4da0f01cf05887c764c', NULL, '2024-11-11 21:38:32', 'Google Chrome,130.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/130.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'refresh_token', '2fd9ef6787daabe7812e599e9e90cced', NULL, '2024-09-29 11:51:18', 'Google Chrome,129.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'refresh_token', '316d0fa527f3928f33af5c87edc9e01c', NULL, '2024-10-12 10:11:25', 'Google Chrome,129.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'refresh_token', '4e624a7f6185c8ee65d3b56bc318c2f4', NULL, '2024-11-14 11:15:57', 'Google Chrome,130.0.0.0,Windows:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/130.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'refresh_token', '70613f8dc59784a39a82daf14d34dff7', NULL, '2024-10-01 03:42:36', 'Google Chrome,129.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'refresh_token', '7305830aa602db68acbe1de92a3f6248', NULL, '2024-11-14 21:20:25', 'Google Chrome,131.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'refresh_token', '95eedf105238b46bf165a74e79f960ec', NULL, '2024-12-11 16:15:01', 'Google Chrome,128.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'refresh_token', 'e530380a6f2c8ab20f3b58c62de7a0a4', NULL, '2024-12-11 16:15:52', 'Google Chrome,128.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36');
INSERT INTO `users_tokens` VALUES (1, 'refresh_token', 'f57ecab4d24d657251849c3bdac63856', NULL, '2024-10-03 07:51:52', 'Google Chrome,129.0.0.0,Windows:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36');
COMMIT;

-- ----------------------------
-- Table structure for worksheets
-- ----------------------------
DROP TABLE IF EXISTS `worksheets`;
CREATE TABLE `worksheets` (
  `ws_id` int NOT NULL AUTO_INCREMENT,
  `ws_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ws_level` int DEFAULT NULL,
  `ws_class` varchar(44) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ws_ent_id` int NOT NULL DEFAULT '0',
  `ws_state` int DEFAULT NULL,
  PRIMARY KEY (`ws_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of worksheets
-- ----------------------------
BEGIN;
INSERT INTO `worksheets` VALUES (1, 'planilla 1', 1, 'ws1', 1, 1);
INSERT INTO `worksheets` VALUES (2, 'planilla 2', 2, 'ws2', 1, 1);
INSERT INTO `worksheets` VALUES (3, 'planilla 3', 3, 'ws3', 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for worksheets_blocks
-- ----------------------------
DROP TABLE IF EXISTS `worksheets_blocks`;
CREATE TABLE `worksheets_blocks` (
  `ws_block_ws_id` int NOT NULL,
  `ws_block_block_id` int NOT NULL,
  `ws_block_order` int NOT NULL,
  PRIMARY KEY (`ws_block_ws_id`,`ws_block_block_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of worksheets_blocks
-- ----------------------------
BEGIN;
INSERT INTO `worksheets_blocks` VALUES (1, 1, 1);
INSERT INTO `worksheets_blocks` VALUES (1, 2, 2);
INSERT INTO `worksheets_blocks` VALUES (1, 3, 3);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
