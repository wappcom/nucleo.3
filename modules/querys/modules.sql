DROP TABLE IF EXISTS modules;


CREATE TABLE `modules` (
  `mod_id` int(11) NOT NULL,
  `mod_name` varchar(255) DEFAULT NULL,
  `mod_description` tinytext,
  `mod_pathurl` varchar(255) DEFAULT NULL,
  `mod_path` varchar(500) DEFAULT NULL,
  `mod_code` varchar(11) DEFAULT NULL,
  `mod_icon` varchar(255) DEFAULT NULL,
  `mod_color` varchar(10) DEFAULT NULL,
  `mod_parent_id` int(11) DEFAULT NULL,
  `mod_indexjs` varchar(400) NOT NULL,
  `mod_css` varchar(255) NOT NULL,
  `mod_db` varchar(255) NOT NULL COMMENT 'database',
  `mod_prefix_db` varchar(255) NOT NULL,
  `mod_relations_db` text NOT NULL,
  `mod_state` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `modules`
--

INSERT INTO `modules` (`mod_id`, `mod_name`, `mod_description`, `mod_pathurl`, `mod_path`, `mod_code`, `mod_icon`, `mod_color`, `mod_parent_id`, `mod_indexjs`, `mod_css`, `mod_db`, `mod_prefix_db`, `mod_relations_db`, `mod_state`) VALUES
(1, 'systems', NULL, 'systems', 'modules/systems/', 'SYS1', 'icon icon-systems', '#99C14C', 0, 'components/sistems.js', 'assets/css/dist/systems.css', 'systems', 'sys_', '', 0),
(2, 'modules', NULL, 'modules', 'modules/modules/', 'MOD1', 'icon icon-box-close', '#99C14C', 0, 'components/modules.js', 'assets/css/dist/modules.css', 'modules', 'mod_', 'modules_categorys:mod_cat_mod_id,systems_modules:sys_mod_mod_id', 0),
(3, 'sites', NULL, 'sites', 'modules/websites/', 'SIT1', 'icon icon-blocks', '#8900ff', 0, 'components/sites.js', 'assets/css/dist/sites.css', 'sites', 'site_', '', 0),
(4, 'Hojas de Trabajo', NULL, 'worksheets', 'modules/websites/', 'WS1', 'icon icon-worksheets', '#26fff6', 0, 'components/worksheets.js', 'assets/css/dist/worksheets.css', 'worksheets', 'ws_', '', 1),
(5, 'Bloques', NULL, 'blocks', 'modules/websites/', 'BL1', 'icon icon-blocks-web', '#FEBF10', 0, 'components/blocks.js', 'assets/css/dist/blocks.css', 'blocks', 'block_', '', 1),
(6, 'Publicaciones', NULL, 'publications', 'modules/websites/', 'PUB1', 'icon icon-webpart', '#FEBF10', 0, 'components/publications.js', 'assets/css/dist/publications.css', 'publications', 'pub_', '', 1),
(10, 'Dashboard WebSites', NULL, 'websites', 'modules/websites/', 'WEB1', 'icon icon-dashboard', '#99C14C', 0, 'components/websites.js', 'assets/css/dist/websites.min.css', '', '', '', 1),
(11, 'Contenidos', NULL, 'contents', 'modules/websites/', 'CON1', 'icon icon-content', '#1ff4ed', 0, 'components/contents.js', 'assets/css/dist/contents.min.css', 'contents', 'cont_', 'contents_categorys:cont_cat_cont_id,contents_files:cont_file_cont_id', 1),
(12, 'Categorias', NULL, 'categorys', 'modules/websites/', 'CAT1', 'icon icon-category', '#dd4b4d', 0, 'components/categorys.js', 'assets/css/dist/categorys.min.css', '', '', '', 1),
(13, 'Media', NULL, 'media', 'modules/websites/', 'MED1', 'icon icon-media', '#f04f04', 0, 'components/media.js', 'assets/css/dist/media.min.css', '', '', '', 1),
(14, 'Documentos', NULL, 'docs', 'modules/websites/', 'DOC1', 'icon icon-folder', '#FEBF10', 0, 'components/docs.js', 'assets/css/dist/docs.min.css', '', '', '', 1),
(15, 'Posts', NULL, 'posts', 'modules/websites/', 'POST1', 'icon icon-news', '#37ff2c', 0, 'components/posts.js', 'assets/css/dist/posts.min.css', '', '', '', 1),
(16, 'Enlaces', NULL, 'links', 'modules/websites/', 'LINK1', 'icon icon-link', '#26fff6', 0, 'components/links.js', 'assets/css/dist/links.min.css', '', '', '', 1),
(17, 'Sliders', NULL, 'sliders', 'modules/websites/', 'SLD1', 'icon icon-slider', '#8900ff', 0, 'components/sliders.js', 'assets/css/dist/sliders.min.css', '', '', '', 1),
(200, 'Dashboard RRHH', NULL, 'rrhh', 'modules/rrhh/', 'RRHH', 'icon icon-category', '#99C14C', 0, 'components/rrhh.js', 'assets/css/dist/rrhh.min.css', '', '', '', 0),
(210, 'kardex', NULL, 'kardex', 'modules/rrhh/', 'KDX', 'icon icon-users', '#FEBF10', 0, 'components/kardex.js', '', '', '', '', 0),
(220, 'Organigrama', NULL, 'organizationChart', 'modules/rrhh/', 'KDX', 'icon icon-users', '#FEBF10', 0, 'components/organizationChart.js', 'assets/css/dist/organizationChart', '', '', '', 0),
(400, 'Dashboard Inventario', NULL, 'inventory', 'modules/inventory/', 'INV', 'icon icon-dashboard', '#FEBF10', 0, 'components/inventory.js', 'assets/css/dist/inventory.css', '', '', '', 1),
(401, 'Productos', NULL, 'products', 'modules/inventory/', 'PRO', 'icon icon-box', '#CCC', 0, 'components/products.js', 'assets/css/dist/products.css', 'mod_products', 'mod_prod_', '', 1),
(402, 'Stock', NULL, 'stock', 'modules/inventory/', 'STC', 'icon icon-unchecked', '#99C14C', 0, 'components/stock.js', 'assets/css/dist/stock.css', '', '', '', 1),
(403, 'Lista de Precios', NULL, 'pricelist', 'modules/inventory/', 'STC', 'icon icon-coin', '#99C14C', 0, 'components/pricelist.js', 'assets/css/dist/pricelist.css', '', '', '', 1),
(500, 'Dashboard Contabilidad', NULL, 'accounting', 'modules/accounting/', 'ACC', 'icon icon-dashboard', '#99C14C', 0, 'components/accounting.js', 'assets/css/dist/accounting.css', '', '', '', 1),
(600, 'Dashboard Consejeria', NULL, 'counseling', 'modules/counseling/', NULL, 'icon icon-dashboard', '#99C14C', 0, 'components/counseling.js', '', '', '', '', 1),
(601, 'Aconsejados', NULL, 'advised', 'modules/counseling/', NULL, 'icon icon-users', '#FEBF10', 0, 'components/advised.js', '', '', '', '', 1),
(602, 'Agenda', NULL, 'calendarAdvised', 'modules/counseling/', NULL, 'icon icon-calendar', '#FEBF10', 0, 'components/calendarAdvised.js', 'assets/css/dist/calendarAdvised.css', '', '', '', 1),
(603, 'Reportes', NULL, 'reportsCounseling', 'modules/counseling/', NULL, 'icon icon-table-check', '#FEBF10', 0, 'components/reportCounseling.js', 'assets/css/dist/reportCounseling.css', '', '', '', 1),
(700, 'Dashboard Ventas', NULL, 'sales', 'modules/sales/', 'PVD', 'icon icon-dashboard', '#fcc54e', 0, 'components/sales.js', 'assets/css/dist/sales.css', '', '', '', 1),
(701, 'Punto de Venta Tickets', NULL, 'salesPointTickets', 'modules/sales/', 'PVT', 'icon icon-cash-register', '#fcc54e', 0, 'components/salesPointTickets.js', 'assets/css/dist/salesPointTickets.css', '', '', '', 1),
(800, 'Dashboard Brokers', NULL, 'brokers', 'modules/brokers/', 'PV', 'icon icon-dashboard', '#fcc54e', 0, 'components/brokers.js', 'assets/css/dist/brokers.css', '', '', '', 1),
(801, 'Clientes', NULL, 'brokersCustomers', 'modules/brokers/', 'PV', 'icon icon-users', '#fcc54e', 0, 'components/brokersCustomers.js', 'assets/css/dist/brokersCustomers.css', '', '', '', 1),
(1000, 'Dashboard Agendas', NULL, 'appointments', 'modules/appointments/', 'AG1', 'icon icon-dashboard', '#99C14', 0, 'components/appointments.js', 'assets/css/dist/appointments.css', '', '', '', 1),
(1001, 'Agenda', NULL, 'agenda', 'modules/appointments/', 'AA1', 'icon icon-user', '#8900ff', 0, 'components/agenda.js', 'assets/css/dist/agenda.css', '', '', '', 1), 
(1002, 'Calendario', NULL, 'calendars', 'modules/appointments/', 'AC1', 'icon icon-calendar', '#07c472', 0, 'components/calendars.js', 'assets/css/dist/calendars.css', '', '', '', 1),
(1003, 'Servicios', NULL, 'appointmentsServices', 'modules/appointments/', 'AS1', 'icon icon-folder-open', '#07c472', 0, 'components/appointmentsServices.js', 'assets/css/dist/appointmentsServices.css', '', '', '', 1),
(1200, 'Dashboard FLS', NULL, 'fls', 'modules/fls/', 'FLS', 'icon icon-dashboard', '#fcc54e', 0, 'components/fls.js', 'assets/css/dist/fls.css', '', '', '', 1),
(1201, 'Clientes', NULL, 'flsCustomers', 'modules/fls/', 'PV', 'icon icon-users', '#fcc54e', 0, 'components/flsCustomers.js', 'assets/css/dist/flsCustomers.css', '', '', '', 1),
(1300, 'Dashboard Boletería', NULL, 'tickets', 'modules/tickets/', 'TKS', 'icon icon-dashboard', '#fcc54e', 0, 'components/tickets.js', 'assets/css/dist/tickets.min.css', '', '', '', 1),
(1301, 'Eventos', NULL, 'ticketingEvents', 'modules/tickets/', 'EVTKS', 'icon icon-tag', '#fcc54e', 0, 'components/ticketingEvents.js', 'assets/css/dist/ticketingEvents.min.css', 'mod_events', 'mod_eve_', '', 1),
(1400, 'Dashboard Rehabilitación', NULL, 'rehabilitation', 'modules/rehabilitation/', 'RHB', 'icon icon-dashboard', '#fcc54e', 0, 'components/rehabilitation.js', 'assets/css/dist/rehabilitation.min.css', '', '', '', 1),
(1401, 'Registro', NULL, 'rehabilitationRegistry', 'modules/rehabilitation/', 'RHBR', 'icon icon-user', '#fcc54e', 0, 'components/rehabilitationRegistry.js', 'assets/css/dist/rehabilitationRegistry.min.css', '', '', '', 1),
(1500, 'Dashboard Ads', NULL, 'ads', 'modules/ads/', 'ADS1', 'icon icon-dashboard', '#8900ff', 0, 'components/ads.js', 'assets/css/dist/ads.css', '', '', '', 1),
(1501, 'Lugares', NULL, 'places', 'modules/ads/', 'ADS1', 'icon icon-pointer', '#07c472', 0, 'components/places.js', 'assets/css/dist/places.min.css', '', '', '', 0),
(1502, 'Campañas', NULL, 'campaignsAds', 'modules/ads/', 'ADS1', 'icon icon-circle-point', '#07c472', 0, 'components/campaignsAds.js', 'assets/css/dist/campaignsAds.min.css', '', '', '', 1),
(1503, 'Display', NULL, 'displayAds', 'modules/ads/', 'ADS2', 'icon icon-copy', '#07c472', 0, 'components/displayAds.js', 'assets/css/dist/displayAds.min.css', '', '', '', 1),
(1504, 'Avisos', NULL, 'advertisements', 'modules/ads/', 'ADS1', 'icon icon-blocks', '#073472', 0, 'components/advertisements.js', 'assets/css/dist/advertisements.min.css', '', '', '', 1),
(1600, 'Dashboard LMS', NULL, 'lms', 'modules/lms/', 'ADS1', 'icon icon-dashboard', '#8900ff', 0, 'components/lms.js', 'assets/css/dist/lms.min.css', '', '', '', 1),
(1601, 'Plan de Estudios', NULL, 'syllabus', 'modules/lms/', 'SB1', 'icon icon-category-r', '#8900ff', 0, 'components/syllabus.js', 'assets/css/dist/syllabus.min.css', '', '', '', 1),
(1700, 'Dashboard Activo Fijo', NULL, 'fixedassets', 'modules/fixedassets/', 'AF1', 'icon icon-dashboard', '#8900ff', 0, 'components/fixedassets.js', 'assets/css/dist/fixedassets.min.css', '', '', '', 1),
(1701, 'Activos', NULL, 'assets', 'modules/fixedassets/', 'ASS1', 'icon icon-tag-qr', '#8900ff', 0, 'components/assets.js', 'assets/css/dist/assets.min.css', '', '', '', 1),
(1800, 'Dashboard Operaciones', NULL, 'operations', 'modules/operations/', 'OP1', 'icon icon-dashboard', '#8900ff', 0, 'components/operations.js', 'assets/css/dist/operations.min.css', '', '', '', 1),
(1900, 'Dashboard CRM', NULL, 'crm', 'modules/crm/', 'CRM1', 'icon icon-dashboard', '#8900ff', 0, 'components/crm.js', 'assets/css/dist/crm.min.css', '', '', '', 1),
(1901, 'Clientes', NULL, 'customers', 'modules/accounts/', 'CS1', 'icon icon-folder-open', '#8900ff', 0, 'components/customers.js', 'assets/css/dist/customers.min.css', '', '', '', 1),
(1902, 'Suscripciones',NULL, 'suscriptionsCrm', 'modules/crm/', 'SC1', 'icon icon-bell', '#99C144', 0, 'components/subscriptionsCrm.js', 'assets/css/dist/suscriptionsCrm.min.css', '', '', '', 1),
(2000, 'Dashboard', NULL, 'restaurants', 'modules/restaurants/', 'RST1', 'icon icon-dashboard', '#8900ff', 0, 'components/restaurants.js', 'assets/css/dist/restaurants.min.css', '', '', '', 1),
(2001, 'Ordenes', NULL, 'commands', 'modules/restaurants/', 'CS1', 'icon icon-command', '#8900ff', 0, 'components/commands.js', 'assets/css/dist/commands.min.css', '', '', '', 1),
(2100, 'Dashboard', NULL, 'externaldocuments', 'modules/externaldocuments/', 'EXD1', 'icon icon-dashboard', '#8900ff', 0, 'components/externaldocuments.js', 'assets/css/dist/externaldocuments.min.css', '', '', '', 1),
(2101, 'Bandeja de Entrada', NULL, 'inboxEd', 'modules/externaldocuments/', 'IBX1', 'icon icon-inbox', '#8900ff', 0, 'components/inboxEd.js', 'assets/css/dist/inboxEd.min.css', '', '', '', 1),
(2102, 'Plantillas', NULL, 'templatesEd', 'modules/externaldocuments/', 'TMP-ED1', 'icon icon-news', '#ff9000', 0, 'components/templatesEd.js', 'assets/css/dist/templatesEd.min.css', '', '', '', 1),
(2103, 'Traking', NULL, 'trakingEd', 'modules/externaldocuments/', 'TRK-ED1', 'icon icon-pointer', '#ff9040', 0, 'components/trakingEd.js', 'assets/css/dist/trakingEd.min.css', '', '', '', 1),
(2200, 'Dashboard Geolocalización', NULL, 'geolocation', 'modules/geolocation/', 'GEO-1', 'icon icon-dashboard', '#ff9040', 0, 'components/geolocation.js', 'assets/css/dist/geolocation.min.css', '', '', '', 1),
(2201, 'Lugares', NULL, 'places', 'modules/geolocation/', 'GEO-2', 'icon icon-pointer', '#E74745', 0, 'components/places.js', 'assets/css/dist/places.min.css', '', '', '', 1),
(2300, 'Dashboard Deportes', NULL, 'sports', 'modules/sports/', 'GEO-1', 'icon icon-dashboard', '#ff9040', 0, 'components/sports.js', 'assets/css/dist/sports.min.css', '', '', '', 1),
(2301, 'Futbol', NULL, 'futbol', 'modules/sports/', 'GEO-2', 'icon icon-futbol', '#E74745', 0, 'components/futbol.js', 'assets/css/dist/futbol.min.css', '', '', '', 1),
(2400, 'Dashboard Concursos', NULL, 'competitions', 'modules/competitions/', 'GEO-1', 'icon icon-dashboard', '#ff9040', 0, 'components/competitions.js', 'assets/css/dist/competitions.min.css', '', '', '', 1),
(3600, 'Dashboard', NULL, 'ganadorapp', 'modules/ganadorapp/', 'GAPP-1', 'icon icon-dashboard', '#E74745', 0, 'components/ganadorapp.js', 'assets/css/dist/ganadorapp.min.css', '', '', '', 1),
(3690, 'Proyectos', NULL, 'proyectosGA', 'modules/ganadorapp/', 'PAPP-1', 'icon icon-tag', '#E74745', 0, 'components/proyectosga.js', 'assets/css/dist/proyectosga.min.css', '', '', '', 1),
(3691, 'Neoganaderos', NULL, 'neoganaderos', 'modules/ganadorapp/', 'GNG-1', 'icon icon-users', '#E74745', 0, 'components/neoganaderos.js', 'assets/css/dist/neoganadores.min.css', '', '', '', 1),
(3692, 'Ganaderos', NULL, 'ganaderos', 'modules/ganadorapp/', 'GAN-1', 'icon icon-dashboard', '#E74745', 0, 'components/ganaderos.js', 'assets/css/dist/ganaderos.min.css', '', '', '', 1),
(3700, 'Dashboard', NULL, 'app_tributo', 'modules/app_tributo/', 'TAPP-1', 'icon icon-dashboard', '#E74745', 0, 'components/app_tributo.js', 'assets/css/dist/app_tributo.min.css', '', '', '', 1),
(3701, 'Selección', NULL, 'app_tributo_selection', 'modules/app_tributo/', 'TAPP-2', 'icon icon-tag', '#E74745', 0, 'components/app_tributo_selection.js', 'assets/css/dist/app_tributo_selection.min.css', '', '', '', 1),
(3800, 'Dashboard', NULL, 'app_candire', 'modules/app_candire/', 'CAPP-1', 'icon icon-dashboard', '#E74745', 0, 'components/app_candire.js', 'assets/css/dist/app_candire.min.css', '', '', '', 1),
(3801, 'Campañas', NULL, 'app_candire_campaigns', 'modules/app_candire/', 'CAPP-2', 'icon icon-tag', '#E74745', 0, 'components/app_candire_campaigns.js', 'assets/css/dist/app_candire_campaigns.min.css', '', '', '', 1);

 
 
ALTER TABLE `modules`
  ADD PRIMARY KEY (`mod_id`) USING BTREE;


ALTER TABLE `modules`
  MODIFY `mod_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
