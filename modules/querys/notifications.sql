DROP TABLE IF EXISTS notifications;

CREATE TABLE `notifications` (
  `ntf_id` int NOT NULL AUTO_INCREMENT,
  `ntf_recipient_id` int NOT NULL COMMENT 'Id del destinatario',
  `ntf_recipient_type` varchar(255) NOT NULL COMMENT 'Tipo de destinatario (customer,provider)',
  `ntf_message` text NOT NULL,
  `ntf_sent_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `ntf_status` varchar(255) DEFAULT 'pending,send,delivered',
  `ntf_ent_id` int NOT NULL,
  `ntf_custom_data` json DEFAULT NULL,
  `ntf_class` varchar(255) DEFAULT NULL,
  `ntf_custom_id` varchar(255) DEFAULT NULL,
  `ntf_state` int DEFAULT '0',
  PRIMARY KEY (`ntf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;