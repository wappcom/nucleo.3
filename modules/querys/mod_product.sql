CREATE TABLE `mod_products_prices_list` (
  `mod_prod_prl_id` int NOT NULL,
  `mod_prod_prl_prod_id` int NOT NULL,
  `mod_prod_prl_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_prl_price` decimal(20,2) NOT NULL,
  `mod_prod_prl_previous_price` decimal(20,2) DEFAULT NULL,
  `mod_prod_prl_coin` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_prl_register_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-

--
ALTER TABLE `mod_products_prices_list`
  ADD PRIMARY KEY (`mod_prod_prl_id`,`mod_prod_prl_prod_id`) USING BTREE;

--
ALTER TABLE `mod_products_prices_list`
  MODIFY `mod_prod_prl_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;
COMMIT;


update mod_products_prices_list set mod_prod_prl_price = "" WHERE mod_prod_prl_code = "";