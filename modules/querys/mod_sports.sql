DROP TABLE `mod_sports_futbol`;

CREATE TABLE `mod_sports_futbol` (
`mod_spt_ftb_id` int(11) NOT NULL AUTO_INCREMENT,
`mod_spt_ftb_name` varchar(255) NOT NULL,
`mod_spt_ftb_description` mediumtext NULL,
`mod_spt_ftb_img` int(24) NULL,
`mod_spt_ftb_ent_id` int(11) NULL,
`mod_spt_ftb_state` int(11) NULL,
PRIMARY KEY (`mod_spt_ftb_id`) 
);
