DROP TABLE IF EXISTS mod_sliders;
DROP TABLE IF EXISTS mod_sliders_files;

CREATE TABLE `mod_sliders` (
  `mod_sli_id` int(11) NOT NULL,
  `mod_sli_name` varchar(255) NOT NULL,
  `mod_sli_description` tinytext NOT NULL,
  `mod_sli_cls` varchar(45) NOT NULL,
  `mod_sli_json` text,
  `mod_sli_ent_id` int(11) NOT NULL,
  `mod_sli_state` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


CREATE TABLE `mod_sliders_files` (
  `mod_sli_file_file_id` int(11) NOT NULL,
  `mod_sli_file_sli_id` int(11) NOT NULL,
  `mod_sli_file_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

ALTER TABLE `mod_sliders`
  ADD PRIMARY KEY (`mod_sli_id`) USING BTREE;

ALTER TABLE `mod_sliders`
  MODIFY `mod_sli_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;


ALTER TABLE `mod_sliders_files`
  ADD PRIMARY KEY (`mod_sli_file_file_id`,`mod_sli_file_sli_id`) USING BTREE;
