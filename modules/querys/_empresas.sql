-- Tabla de Empresas
CREATE TABLE `companies` (
  `company_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único de la empresa',
  `company_name` VARCHAR(255) NOT NULL COMMENT 'Nombre de la empresa',
  `company_email` VARCHAR(255) NOT NULL COMMENT 'Correo electrónico de la empresa',
  `company_phone` VARCHAR(20) DEFAULT NULL COMMENT 'Teléfono de contacto',
  `company_address` VARCHAR(255) DEFAULT NULL COMMENT 'Dirección de la empresa',
  `company_website` VARCHAR(255) DEFAULT NULL COMMENT 'Sitio web de la empresa',
  `company_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación de la empresa',
  `company_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización de la empresa',
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Tabla de Sitios
CREATE TABLE `sites` (
  `site_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único del sitio',
  `site_company_id` INT NOT NULL COMMENT 'Id de la empresa propietaria del sitio',
  `site_name` VARCHAR(255) NOT NULL COMMENT 'Nombre del sitio',
  `site_url` VARCHAR(255) NOT NULL COMMENT 'URL del sitio',
  `site_description` TEXT DEFAULT NULL COMMENT 'Descripción del sitio',
  `site_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación del sitio',
  `site_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización del sitio',
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Tabla de Campañas
CREATE TABLE `campaigns` (
  `campaign_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único de la campaña',
  `campaign_company_id` INT NOT NULL COMMENT 'Id de la empresa que crea la campaña',
  `campaign_name` VARCHAR(255) NOT NULL COMMENT 'Nombre de la campaña',
  `campaign_objective` VARCHAR(255) NOT NULL COMMENT 'Objetivo de la campaña (ejemplo: leads, ventas)',
  `campaign_status` VARCHAR(50) DEFAULT 'active' COMMENT 'Estado de la campaña (active, paused, completed)',
  `campaign_start_date` DATE NOT NULL COMMENT 'Fecha de inicio de la campaña',
  `campaign_end_date` DATE DEFAULT NULL COMMENT 'Fecha de finalización de la campaña',
  `campaign_budget_id` INT NOT NULL COMMENT 'Id del presupuesto asociado',
  `campaign_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación de la campaña',
  `campaign_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización de la campaña',
  PRIMARY KEY (`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Tabla de Anuncios
CREATE TABLE `ads` (
  `ad_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único del anuncio',
  `ad_campaign_id` INT NOT NULL COMMENT 'Id de la campaña asociada',
  `ad_site_id` INT NOT NULL COMMENT 'Id del sitio donde se mostrará el anuncio',
  `ad_type_id` INT NOT NULL COMMENT 'Id del tipo de anuncio (ejemplo: display, video)',
  `ad_title` VARCHAR(255) NOT NULL COMMENT 'Título del anuncio',
  `ad_description` TEXT DEFAULT NULL COMMENT 'Descripción del anuncio',
  `ad_media_id` INT NOT NULL COMMENT 'Id de la imagen o multimedia asociada',
  `ad_target_url` VARCHAR(255) NOT NULL COMMENT 'URL de destino del anuncio',
  `ad_status` VARCHAR(50) DEFAULT 'active' COMMENT 'Estado del anuncio (active, paused)',
  `ad_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación del anuncio',
  `ad_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización del anuncio',
  PRIMARY KEY (`ad_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Tabla de Audiencias
CREATE TABLE `audiences` (
  `audience_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único de la audiencia',
  `audience_name` VARCHAR(255) NOT NULL COMMENT 'Nombre de la audiencia',
  `audience_description` TEXT DEFAULT NULL COMMENT 'Descripción de la audiencia',
  `audience_type` VARCHAR(50) NOT NULL COMMENT 'Tipo de audiencia (ejemplo: remarketing, intereses)',
  `audience_size` INT DEFAULT NULL COMMENT 'Tamaño de la audiencia',
  `audience_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación de la audiencia',
  `audience_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización de la audiencia',
  PRIMARY KEY (`audience_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Tabla de Segmentaciones
CREATE TABLE `segmentations` (
  `segmentation_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único de la segmentación',
  `segmentation_campaign_id` INT NOT NULL COMMENT 'Id de la campaña asociada',
  `segmentation_audience_id` INT NOT NULL COMMENT 'Id de la audiencia asociada',
  `segmentation_location_id` INT DEFAULT NULL COMMENT 'Id de la ubicación asociada',
  `segmentation_device_id` INT DEFAULT NULL COMMENT 'Id del dispositivo asociado',
  `segmentation_optimized` TINYINT(1) DEFAULT 0 COMMENT 'Indica si la segmentación está optimizada (0: No, 1: Sí)',
  `segmentation_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación de la segmentación',
  `segmentation_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización de la segmentación',
  PRIMARY KEY (`segmentation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Tabla de Presupuestos
CREATE TABLE `budgets` (
  `budget_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único del presupuesto',
  `budget_campaign_id` INT NOT NULL COMMENT 'Id de la campaña asociada',
  `budget_daily_amount` DECIMAL(10,2) NOT NULL COMMENT 'Presupuesto diario asignado',
  `budget_total_amount` DECIMAL(10,2) NOT NULL COMMENT 'Presupuesto total asignado',
  `budget_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación del presupuesto',
  `budget_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización del presupuesto',
  PRIMARY KEY (`budget_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Tabla de Métricas de Rendimiento
CREATE TABLE `performance_metrics` (
  `metric_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único de la métrica',
  `metric_campaign_id` INT NOT NULL COMMENT 'Id de la campaña asociada',
  `metric_ad_id` INT NOT NULL COMMENT 'Id del anuncio asociado',
  `metric_site_id` INT NOT NULL COMMENT 'Id del sitio donde se mostró el anuncio',
  `metric_clicks` INT DEFAULT 0 COMMENT 'Número de clics',
  `metric_impressions` INT DEFAULT 0 COMMENT 'Número de impresiones',
  `metric_conversions` INT DEFAULT 0 COMMENT 'Número de conversiones',
  `metric_cost_per_click` DECIMAL(10,2) DEFAULT NULL COMMENT 'Costo por clic',
  `metric_cost_per_conversion` DECIMAL(10,2) DEFAULT NULL COMMENT 'Costo por conversión',
  `metric_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación de la métrica',
  `metric_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización de la métrica',
  PRIMARY KEY (`metric_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Tabla de Imágenes y Multimedia
CREATE TABLE `media` (
  `media_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único de la imagen o multimedia',
  `media_url` VARCHAR(255) NOT NULL COMMENT 'URL de la imagen o multimedia',
  `media_type` VARCHAR(50) NOT NULL COMMENT 'Tipo de media (ejemplo: imagen, video)',
  `media_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación de la media',
  `media_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización de la media',
  PRIMARY KEY (`media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Tabla de Ubicaciones
CREATE TABLE `locations` (
  `location_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único de la ubicación',
  `location_name` VARCHAR(255) NOT NULL COMMENT 'Nombre de la ubicación',
  `location_country` VARCHAR(255) NOT NULL COMMENT 'País de la ubicación',
  `location_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación de la ubicación',
  `location_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización de la ubicación',
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Tabla de Dispositivos
CREATE TABLE `devices` (
  `device_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único del dispositivo',
  `device_name` VARCHAR(255) NOT NULL COMMENT 'Nombre del dispositivo (ejemplo: móvil, desktop)',
  `device_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación del dispositivo',
  `device_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización del dispositivo',
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Tabla de Tipos de Anuncios
CREATE TABLE `ad_types` (
  `ad_type_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id único del tipo de anuncio',
  `ad_type_name` VARCHAR(255) NOT NULL COMMENT 'Nombre del tipo de anuncio (ejemplo: display, video)',
  `ad_type_description` TEXT DEFAULT NULL COMMENT 'Descripción del tipo de anuncio',
  `ad_type_created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación del tipo de anuncio',
  `ad_type_updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Última actualización del tipo de anuncio',
  PRIMARY KEY (`ad_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;