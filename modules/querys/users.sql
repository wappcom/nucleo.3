

CREATE TABLE `roles` (
  `rol_id` int NOT NULL AUTO_INCREMENT,
  `rol_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `rol_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `rol_parent_id` int DEFAULT NULL,
  `rol_redirection_url` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `rol_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `rol_state` int DEFAULT '0',
  PRIMARY KEY (`rol_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



CREATE TABLE `entities` (
  `ent_id` int NOT NULL AUTO_INCREMENT,
  `ent_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_brand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_code` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ent_record_date` datetime DEFAULT NULL,
  `ent_type` int DEFAULT NULL,
  `ent_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_state` int DEFAULT NULL,
  PRIMARY KEY (`ent_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


INSERT INTO `entities` (`ent_id`, `ent_name`, `ent_path`, `ent_brand`, `ent_code`, `ent_token`, `ent_record_date`, `ent_type`, `ent_notes`, `ent_state`) VALUES
(1, 'Wappcom', 'wappcom', 'modules/assets/img/logo.svg', 'CO1', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3IiwibmFtZSI6IkNhc2EgZGUgT3JhY2lvbiIsImlhdCI6MTIzNDU2Nzg5fQ.y1fa_y48leQNYbZGDlvJewtehVj78zMhkJ73B-8MGyE', '2020-10-20 14:55:39', 1, '', 1);

CREATE TABLE `roles_entities` (
  `rol_ent_role_id` int NOT NULL,
  `rol_ent_ent_id` int NOT NULL,
  `rol_ent_state` int DEFAULT NULL,
  PRIMARY KEY (`rol_ent_role_id`, `rol_ent_ent_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;