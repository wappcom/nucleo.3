DROP TABLE IF EXISTS `mod_ads`;
DROP TABLE IF EXISTS `mod_ads_position`;
DROP TABLE IF EXISTS `mod_ads_customers`;

CREATE TABLE `mod_ads` (
    `mod_ads_id` INT NOT NULL AUTO_INCREMENT , 
    `mod_ads_name` VARCHAR(255) NULL DEFAULT NULL , 
    `mod_ads_details` TEXT NULL DEFAULT NULL , 
    `mod_ads_code` VARCHAR(255) NULL DEFAULT NULL , 
    `mod_ads_file` INT NULL DEFAULT NULL , 
    `mod_ads_position` INT NULL DEFAULT NULL , 
    `mod_ads_user_id` INT NULL DEFAULT NULL , 
    `mod_ads_state` INT NOT NULL DEFAULT '0' , 
PRIMARY KEY (`mod_ads_id`)) ENGINE = MyISAM DEFAULT CHARSET=utf8mb4;


CREATE TABLE `mod_ads_position` (
    `mod_ads_pos_id` INT NOT NULL AUTO_INCREMENT , 
    `mod_ads_pos_name` VARCHAR(255) NULL DEFAULT NULL , 
    `mod_ads_pos_description` TEXT NULL DEFAULT NULL , 
    `mod_ads_pos_state` INT NOT NULL DEFAULT '0' ,
PRIMARY KEY (`mod_ads_pos_id`)) ENGINE = MyISAM DEFAULT CHARSET=utf8mb4;


CREATE TABLE `mod_ads_customers` (
  `mod_ads_cus_id` int NOT NULL,
  `mod_ads_cus_ads_id` int NOT NULL COMMENT 'mod_ads',
  `mod_ads_cus_cen_id` int NOT NULL COMMENT 'mod_customers_enterprise',
  `mod_ads_cus_date_init` datetime NOT NULL,
  `mod_ads_cus_date_end` timestamp NOT NULL,
  `mod_ads_cus_description` text COLLATE utf8mb4_general_ci NOT NULL,
  `mod_ads_cus_state` int NOT NULL DEFAULT '0' COMMENT '0: Inactivo, 1: Activo, 2: Desactivado por tiempo, 3: Desactivado por cantidad de clicks, 4: Desactivado por cantidad de impresiones'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

ALTER TABLE `mod_ads_customers`
  ADD PRIMARY KEY (`mod_ads_cus_id`,`mod_ads_cus_ads_id`,`mod_ads_cus_cen_id`);

ALTER TABLE `mod_ads_customers`
  MODIFY `mod_ads_cus_id` int NOT NULL AUTO_INCREMENT;


--
-- AUTO_INCREMENT de la tabla `menu_items`
--
ALTER TABLE `mod_ads`
  MODIFY `mod_ads_id` int(11) NOT NULL AUTO_INCREMENT;
 

ALTER TABLE `mod_ads_position`
  MODIFY `mod_ads_pos_id` int(11) NOT NULL AUTO_INCREMENT;
 

 
 