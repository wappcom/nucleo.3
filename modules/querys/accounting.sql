DROP TABLE IF EXISTS mod_accounting_plan;

CREATE TABLE `mod_accounting_plan` (
  `mod_acp_id` int(11) NOT NULL,
  `mod_acp_name` varchar(255) DEFAULT NULL,
  `mod_acp_descripcion` varchar(450) DEFAULT NULL,
  `mod_acp_parent_id` int(11) DEFAULT NULL,
  `mod_acp_code` varchar(255) DEFAULT NULL,
  `mod_acp_level` int(11) DEFAULT NULL,
  `mod_acp_coin` varchar(5) DEFAULT NULL,
  `mod_acp_state` int(11) NOT NULL DEFAULT '0'
) ENGINE = MyISAM DEFAULT CHARSET = utf8;
--
-- Índices para tablas volcadas
--
--
-- Indices de la tabla `mod_accounting_plan`
--
ALTER TABLE
  `mod_accounting_plan`
ADD
  PRIMARY KEY (`mod_acp_id`) USING BTREE;
 
ALTER TABLE
  `mod_accounting_plan`
MODIFY
  `mod_acp_id` int(11) NOT NULL AUTO_INCREMENT;