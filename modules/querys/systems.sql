DROP TABLE IF EXISTS systems;
DROP TABLE IF EXISTS systems_modules;

CREATE TABLE `systems` (
  `sys_id` int(11) NOT NULL,
  `sys_name` varchar(255) DEFAULT NULL,
  `sys_description` tinytext,
  `sys_pathurl` varchar(500) DEFAULT NULL,
  `sys_path` varchar(500) DEFAULT NULL,
  `sys_mod_default` int (11) NOT NULL DEFAULT '0',
  `sys_code` varchar(11) NOT NULL,
  `sys_icon` varchar(240) DEFAULT NULL,
  `sys_color` varchar(10) DEFAULT NULL,
  `sys_parent_id` int(11) DEFAULT NULL,
  `sys_indexjs` varchar(255) NOT NULL,
  `sys_css` varchar(255) NOT NULL,
  `sys_order` int(11) NOT NULL,
  `sys_state` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `systems`
--

INSERT INTO `systems` (`sys_id`, `sys_name`, `sys_description`, `sys_pathurl`, `sys_path`, `sys_mod_default`,`sys_code`, `sys_icon`, `sys_color`, `sys_parent_id`, `sys_indexjs`, `sys_css`, `sys_order`, `sys_state`) VALUES
(1, 'Websites', NULL, 'websites', 'modules/websites/', '10', '', 'icon icon-code', '#6F91E9', 0, 'components/websites.js', 'assets/css/dist/websites.css', 0, 1),
(2, 'Recursos Humanos', NULL, 'rrhh', 'modules/rrhh/', '200', '', 'icon icon-category', '#6F91E9', 0, 'components/rrhh.js', 'assets/css/dist/rrhh.css', 0, 1),
(4, 'Inventario', NULL, 'inventory', 'modules/inventory/', '400', '', 'icon icon-box', '#FEBF10', 0, 'components/inventory.js', 'assets/css/dist/inventory.css', 0, 1),
(5, 'Contabilidad', NULL, 'accounting', 'modules/accounting/', '500', '', 'icon icon-coin', '#6F91E9', 0, 'components/accounting.js', 'assets/css/dist/accounting.css', 0, 1),
(6, 'Consejeria', NULL, 'counseling', 'modules/counseling/', '600', '', 'icon icon-users', '#27d27c', 0, 'components/counseling.js', 'assets/css/dist/counseling.css', 0, 1),
(7, 'Ventas', NULL, 'sales', 'modules/sales/', '700', '', 'icon icon-sales', '#fc9835', 0, 'components/sales.js', 'assets/css/dist/sales.css', 0, 0),
(8, 'Brokers', NULL, 'brokers', 'modules/brokers/', '800', '', 'icon icon-circle-doble', '#27d27c', 0, 'components/brokers.js', 'assets/css/dist/brokers.css', 0, 0),
(10, 'Agendas', NULL, 'appointments', 'modules/appointments/', '1000', '', 'icon icon-calendar', '#1be5fc', 0, 'components/appointments.js', 'assets/css/dist/appointments.css', 0, 1),
(12, 'Finalcial Services Leeds', NULL, 'fls', 'modules/fls/', '1200', '', 'icon icon-circle', '#27d27c', 0, 'components/fls.js', 'assets/css/dist/fls.css', 0, 0),
(13, 'Boletería', NULL, 'tickets', 'modules/tickets/', '1300', '', 'icon icon-tag', '#27d27c', 0, 'components/tickets.js', 'assets/css/dist/tickets.min.css', 0, 1),
(14, 'Rehabilitación', NULL, 'rehabilitation', 'modules/rehabilitation/', '1400', '', 'icon icon-user', '#6F91E', 0, 'components/rehabilitation.js', 'assets/css/dist/rehabilitation.min.css', 0, 1),
(15, 'Ads', NULL, 'ads', 'modules/ads/', '1500', '', 'icon icon-loudspeaker', '#e71882', 0, 'components/ads.js', 'assets/css/dist/ads.min.css', 0, 1),
(16, 'LMS', NULL, 'lms', 'modules/lms/', '1600', '', 'icon icon-graduation', '#FEBF10', 0, 'components/lms.js', 'assets/css/dist/lms.min.css', 0, 1),
(17, 'Activo Fijo', NULL, 'fixedassets', 'modules/fixedassets/','1700', '', 'icon icon-fixed-assets', '#9c27b0', 0, 'components/fixedassets.js', 'assets/css/dist/fixedassets.min.css', 0, 1),
(18, 'Operaciones', NULL, 'operations', 'modules/operations/', '1800', '', 'icon icon-operations', '#258dfc', 0, 'components/operations.js', 'assets/css/dist/operations.min.css', 0, 1),
(19, 'CRM', NULL, 'crm', 'modules/crm/', '1900', '', 'icon icon-crm', '#ef4848', 0, 'components/crm.js', 'assets/css/dist/crm.min.css', 0, 1),
(20, 'Restaurant', NULL, 'restaurants', 'modules/restaurants/', '2000', '', 'icon icon-restaurant', '#6b6fa9', 0, 'components/restaurants.js', 'assets/css/dist/restaurants.min.css', 0, 1),
(21, 'Doc. Externos', NULL, 'externaldocuments', 'modules/externaldocuments/', '2100', '', 'icon icon-doc', '#6b6fa9', 0, 'components/externaldocuments.js', 'assets/css/dist/externaldocuments.min.css', 0, 1),
(22, 'Geoposición', NULL, 'geolocation', 'modules/geolocation/', '2200', '', 'icon icon-pointer-route', '#00d853', 0, 'components/geolocation.js', 'assets/css/dist/geolocation.min.css', 0, 1),
(23, 'Deportes', NULL, 'sports', 'modules/sports/', '2200', '', 'icon icon-sport', '#ff7629', 0, 'components/sports.js', 'assets/css/dist/sports.min.css', 0, 1),
(24, 'Concursos', NULL, 'competitions', 'modules/competitions/', '2400', '', 'icon icon-copy', '#ff3333', 0, 'components/competitions.js', 'assets/css/dist/competitions.min.css', 0, 1),
(36, 'Ganador App', NULL, 'ganadorapp', 'modules/app_ganador/', '3600', '', 'icon icon-table-check', '#00d767', 0, 'components/app_ganador.js', 'assets/css/dist/app_ganador.min.css', 0, 1),
(37, 'Tributo App', NULL, 'app_tributo', 'modules/app_tributo/', '3700', '', 'icon icon-coin', '#003767', 0, 'components/app_tributo.js', 'assets/css/dist/app_tributo.min.css', 0, 1),
(38, 'Candire App', NULL, 'app_candire', 'modules/app_candire/', '3800', '', 'icon icon-circle-checkmark', '#003800', 0, 'components/app_candire.js', 'assets/css/dist/app_candire.min.css', 0, 1);

--
-- Indices de la tabla `systems`
--
ALTER TABLE `systems`
  ADD PRIMARY KEY (`sys_id`) USING BTREE;



--
-- AUTO_INCREMENT de la tabla `systems`
--
ALTER TABLE `systems`
  MODIFY `sys_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;



CREATE TABLE `systems_modules` (
  `sys_mod_sys_id` int(11) NOT NULL,
  `sys_mod_mod_id` int(11) NOT NULL,
  `sys_mod_ent_id` int(11) NOT NULL,
  `sys_mod_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



INSERT INTO `systems_modules` (`sys_mod_sys_id`, `sys_mod_mod_id`, `sys_mod_ent_id`, `sys_mod_order`) VALUES
(1, 10, 1, 1),
(1, 11, 1, 2),
(1, 12, 1, 3),
(1, 13, 1, 4),
(1, 14, 1, 5),
(1, 15, 1, 6),
(1, 16, 1, 7),
(1, 17, 1, 8),
(2, 200, 1, 1),
(2, 210, 1, 2),
(2, 220, 1, 3),
(4, 400, 1, 1),
(4, 401, 1, 2),
(4, 402, 1, 1),
(4, 403, 1, 2),
(5, 500, 1, 1),
(6, 600, 1, 1),
(6, 601, 1, 2),
(6, 602, 1, 3),
(6, 603, 1, 4),
(7, 700, 1, 1),
(7, 701, 1, 2),
(8, 800, 1, 1),
(8, 801, 1, 2),
(10, 1000, 1, 1),
(10, 1001, 1, 2),
(10, 1002, 1, 3),
(10, 1003, 1, 4),
(12, 1200, 1, 1),
(12, 1201, 1, 2),
(13, 1300, 1, 1),
(13, 1301, 1, 2),
(14, 1400, 1, 1),
(14, 1401, 1, 2),
(15, 1500, 1, 1),
(15, 1501, 1, 2),
(15, 1502, 1, 3),
(15, 1503, 1, 4),
(15, 1504, 1, 5),
(16, 1600, 1, 1),
(16, 1601, 1, 2),
(17, 1700, 1, 1),
(17, 1701, 1, 2),
(18, 1800, 1, 1),
(19, 1900, 1, 1),
(19, 1901, 1, 2),
(19, 1902, 1, 2),
(20, 2000, 1, 1),
(20, 2001, 1, 2),
(21, 2100, 1, 1),
(21, 2101, 1, 2),
(21, 2102, 1, 2),
(21, 2103, 1, 2),
(22, 2201, 1, 1),
(23, 2300, 1, 1),
(23, 2301, 1, 2),
(24, 2400, 1, 1),
(36, 3600, 1, 1), 
(36, 3690, 1, 2), 
(36, 3691, 1, 3), 
(36, 3692, 1, 4),
(37, 3700, 1, 1), 
(37, 3701, 1, 2),
(38, 3800, 1, 1),
(38, 3801, 1, 2),
(38, 3802, 1, 3);


--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `systems_modules`
--
ALTER TABLE `systems_modules`
  ADD PRIMARY KEY (`sys_mod_sys_id`,`sys_mod_mod_id`,`sys_mod_ent_id`) USING BTREE;
 