DROP TABLE IF EXISTS contents;
DROP TABLE IF EXISTS contents_files;

CREATE TABLE `contents` (
  `cont_id` int(11) NOT NULL,
  `cont_title` varchar(255) NOT NULL,
  `cont_pathurl` varchar(255) NOT NULL,
  `cont_summary` tinytext,
  `cont_tags` varchar(300) DEFAULT NULL,
  `cont_img` int(11) DEFAULT NULL,
  `cont_body` text,
  `cont_author` int(11) DEFAULT NULL,
  `cont_register_date` datetime DEFAULT NULL,
  `cont_user_id` int(11) DEFAULT NULL COMMENT 'user register',
  `cont_notitle` int(11) DEFAULT '0',
  `cont_url` varchar(255) DEFAULT NULL,
  `cont_btn_title` varchar(45) DEFAULT NULL,
  `cont_target` varchar(12) NOT NULL DEFAULT '_blank',
  `cont_cls` varchar(255) NOT NULL COMMENT 'css class',
  `cont_state` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


 
ALTER TABLE `contents`
  ADD PRIMARY KEY (`cont_id`);
 
ALTER TABLE `contents`
  MODIFY `cont_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;


CREATE TABLE `contents_files` (
  `cont_file_cont_id` int(11) NOT NULL,
  `cont_file_file_id` int(11) NOT NULL,
  `cont_file_title` varchar(255) NOT NULL,
  `cont_file_summary` varchar(400) NOT NULL,
  `cont_file_url` varchar(255) NOT NULL,
  `cont_file_icon` varchar(25) NOT NULL,
  `cont_file_target` varchar(255) NOT NULL DEFAULT '_blank',
  `cont_file_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `contents_files`
  ADD PRIMARY KEY (`cont_file_cont_id`,`cont_file_file_id`);