DROP TABLE `mod_accounts_ads`;

CREATE TABLE `mod_accounts_ads` (
`mod_aca_id` int(11) NOT NULL AUTO_INCREMENT,
`mod_aca_cco_id` int(11) NOT NULL,
`mod_aca_name` varchar(255) NOT NULL,
`mod_aca_description` mediumtext NULL,
`mod_aca_code` int(24) NULL,
`mod_aca_ent_id` int(11) NULL,
`mod_aca_state` int(11) NULL,
PRIMARY KEY (`mod_aca_id`, `mod_aca_cco_id`) 
);
