
DROP TABLE IF EXISTS entitie;

CREATE TABLE `entities` (
  `ent_id` int NOT NULL,
  `ent_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_brand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_code` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ent_record_date` datetime DEFAULT NULL,
  `ent_type` int DEFAULT NULL,
  `ent_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_state` int DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `entities`
--

INSERT INTO `entities` (`ent_id`, `ent_name`, `ent_path`, `ent_brand`, `ent_code`, `ent_token`, `ent_record_date`, `ent_type`, `ent_notes`, `ent_state`) VALUES
(1, 'Wappcom', 'wappcom', 'modules/assets/img/logo.svg', 'CO1', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3IiwibmFtZSI6IkNhc2EgZGUgT3JhY2lvbiIsImlhdCI6MTIzNDU2Nzg5fQ.y1fa_y48leQNYbZGDlvJewtehVj78zMhkJ73B-8MGyE', '2020-10-20 14:55:39', 1, '', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `entities`
--
ALTER TABLE `entities`
  ADD PRIMARY KEY (`ent_id`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `entities`
--
ALTER TABLE `entities`
  MODIFY `ent_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;