CREATE TABLE `links` (
  `lnk_id` int(11) NOT NULL,
  `lnk_title` varchar(455) DEFAULT NULL,
  `lnk_description` varchar(445) DEFAULT NULL,
  `lnk_href` varchar(455) DEFAULT NULL,
  `lnk_target` varchar(12) DEFAULT '_blank',
  `lnk_cls` varchar(45) DEFAULT NULL,
  `lnk_tags` varchar(45) DEFAULT NULL,
  `lnk_img` int(11) DEFAULT NULL,
  `lnk_attr` varchar(120) DEFAULT NULL,
  `lnk_alt` varchar(120) DEFAULT NULL,
  `lnk_state` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


ALTER TABLE `links`
  ADD PRIMARY KEY (`lnk_id`);

 
ALTER TABLE `links`
  MODIFY `lnk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;
