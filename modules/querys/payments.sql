DROP TABLE IF EXISTS mod_payments_tx_tickets;


CREATE TABLE `mod_payments_tx_tickets` (
  `mod_pay_tx_tck_id` int(11) NOT NULL,
  `mod_pay_tx_tck_acp_id` int(11) DEFAULT NULL,
  `mod_pay_tx_tck_cpe_id` int(11) NOT NULL,
  `mod_pay_tx_tck_eve_id` int(11) DEFAULT NULL,
  `mod_pay_tx_tck_reserve_id` int(11) DEFAULT NULL,
  `mod_pay_tx_tck_pay` int(11) NOT NULL,
  `mod_pay_tx_tck_code` varchar(255) DEFAULT NULL,
  `mod_pay_tx_tck_coin` varchar(4) DEFAULT NULL,
  `mod_pay_tx_tck_rs` varchar(255) DEFAULT NULL COMMENT 'razon social',
  `mod_pay_tx_tck_nit` int(22) DEFAULT NULL,
  `mod_pay_tx_tck_register_date` datetime DEFAULT NULL,
  `mod_pay_tx_tck_state` int(11) NOT NULL DEFAULT '0' COMMENT '0. No-state, 1.Active/pagado, 2.Anulado 3.Delete'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `mod_accounting_tx_tickets`
--
ALTER TABLE `mod_payments_tx_tickets`
  ADD PRIMARY KEY (`mod_pay_tx_tck_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `mod_accounting_tx_tickets`
--
ALTER TABLE `mod_payments_tx_tickets`
  MODIFY `mod_pay_tx_tck_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;