<?php
header('Content-Type: text/html; charset=utf-8');
class PAYMENTS
{
    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function getPayEventQR(array $var =  null){
        //return $var;

        require_once(_PATH_NUCLEO . "modules/accounts/models/class/class.customers.php");
        $customers = new CUSTOMERS($this->fmt);


        $eventId = $var['eventId'];
        $statusState = $var['statusState'];

        $sql = "SELECT * FROM mod_payments_tx_tickets WHERE mod_pay_tx_tck_id = '".$eventId."' AND mod_pay_tx_tck_state = '".$statusState."' AND mod_pay_tx_tck_type = 'QR'";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $id = $row["mod_pay_tx_tck_id"];
                $return[$i]["id"] = $id;
                $cpeId = $row["mod_pay_tx_tck_cpe_id"];
                $return[$i]["cpeId"] = $cpeId;
                $return[$i]["dataCpeId"] = $customers->getCustomerPersonId($cpeId);
                $return[$i]["txr"] = $row["mod_pay_tx_tck_txr"];
                // encode json $callback
                $json  =$row["mod_pay_tx_tck_callback"];

                // Decodificar el JSON
                $data = json_decode($json);

                // Verificar si hay errores en el JSON
                if (json_last_error() !== JSON_ERROR_NONE) {
                    // Corregir el JSON
                    $json = preg_replace('/\s*([{}])\s*/','$1',$json);
                    $data = json_decode($json);
                }

                // Volver a codificar el JSON
                $json = json_encode($data);
                $return[$i]["callback"] = $json;
            }
            return $return;
        } else {
            return 0;
        }
        
    }

    public function registerTxTicketEvent(array $var = null)
    {
        require_once(_PATH_NUCLEO . "modules/accounts/models/class/class.customers.php");
        $customers = new CUSTOMERS($this->fmt);


        $userId = $var["user"]["userId"];
        $rolId = $var['user']['rolId'];
        $entitieId = $var['entitieId'];
        $input = $var['vars']['input'];
        $acp = $input['acp'];
        $cpeId = $input['cpeId']    ?? 0;
        $eveId = $input['eveId'] ?? 0;
        $reserveId = $input['reserveId'] ?? 0;
        $pay = $input['pay'] ?? "0.00";
        $coin = $input['coin'] ?? "Bs";
        $rs = $input['rs'] ?? "";
        $nit = $input['nit'] ?? "";
        $type = $input['type'] ?? "";
        $valueType = $input['valueType'] ?? "";
        $state = $input['state'] ?? 0;
        $now = $this->fmt->data->dateFormat();
        $code = $this->fmt->data->createCode(['num' => 6,'mode' => 'integer']);
        $code = $now . $code;
        $code = hash('sha256',$code);
        $cpe = $customers->getCustomerPersonId($cpeId);
        $var["cpe"] = $cpe;
        $json = json_encode($var);


        if ($cpeId == 0 || $eveId == 0 || $reserveId == 0) {
            $return["Error"]  = 1;
            $return["status"]  = "error";
            $return["message"]  = "No se ha seleccionado un evento ni una reserva.";
            return 0;
        }

        if ($type == "cash" || $type == "transfer" || $type == "card") {
            $state = 3;
        }

        $insert = "mod_pay_tx_tck_acp_id,mod_pay_tx_tck_cpe_id,mod_pay_tx_tck_eve_id,mod_pay_tx_tck_reserve_id,mod_pay_tx_tck_pay,mod_pay_tx_tck_code,mod_pay_tx_tck_coin,mod_pay_tx_tck_rs,mod_pay_tx_tck_nit,mod_pay_tx_tck_type,mod_pay_tx_tck_txr,mod_pay_tx_tck_register_date,mod_pay_tx_tck_user_id,mod_pay_tx_tck_json,mod_pay_tx_tck_state";
        $values = "'" . $acp . "','" . $cpeId . "','" . $eveId . "','" . $reserveId . "','" . $pay . "','" . $code . "','" . $coin . "','" . $rs . "','" . $nit . "','" . $type . "','" . $valueType . "','" . $now . "','" . $userId . "','" . $json . "','" . $state . "'";
        $sql = "insert into mod_payments_tx_tickets (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql,__METHOD__);

        $sql = "select max(mod_pay_tx_tck_id) as id from mod_payments_tx_tickets";
        $rs = $this->fmt->querys->consult($sql,__METHOD__);
        $row = $this->fmt->querys->row($rs);
        $id = $row["id"];

        $return["code"] = $code;
        $return["id"] = $id;
        return $return;
    }

    public function pay_libelula(array $var = null)
    {
        //return $var;

        require_once(_PATH_NUCLEO . "modules/accounts/models/class/class.customers.php");
        $customers = new CUSTOMERS($this->fmt);

        require_once(_PATH_NUCLEO . "modules/tickets/models/class/tickets.class.php");
        $tickets = new TICKETS($this->fmt);

        $cpeId = $var['cpeId'] ?? 0;
        $eventId = $var['eventId'] ?? 0;
        $reserveId = $var['reserveId'] ?? 0;
        $pay = $var['pay'] ?? "0.00";
        $coin = $var['coin'] ?? "Bs";
        $rs = $var['rs'] ?? "";
        $nit = $var['nit'] ?? "";
        $type = $var['type'] ?? "";
        $tk  = $var['tokenTx'] ?? "";
        // decode base 64
        
        $cpe = $customers->getCustomerPersonId($cpeId);
        $token = base64_encode($cpe["id"]."-".$cpe['email']."-".$eventId."-".$reserveId."-".$tk);
        $sussessPay = $token;
        $payToken = $token;
        $event = $tickets->getEventId($eventId);
        $identificador = $var['identificador'];
        $name = $cpe['name'];
        $lastname = $cpe['lastname'];
        $email = $cpe['email'];

        
        $now = $this->fmt->data->dateFormat();
        $now = date("Y-m-d H:i:s",strtotime($now . "+ 15 minutes"));

        //return _PATH_WEB.'apis/v1/pay-success.php?id='.$sussessPay;

        $curl = curl_init();

        curl_setopt_array($curl,array(
            CURLOPT_URL => 'https://api.todotix.com/rest/deuda/registrar',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
            "appkey": "11bb10ce-68ba-4af1-8eb7-4e6624fed729",
            "email_cliente": "'.$email.'",
            "identificador": "'.$tk.'",
            "callback_url": "'._PATH_WEB.'apis/v1/pay-success.php?id='.$sussessPay.'",
            "url_retorno": "'._PATH_WEB.'ion/pay?id='.$payToken.'",
            "descripcion": "Pago Reserva #'.$reserveId.' - '.$event['name'].' - '.$event['initDate'].'",
            "nombre_cliente": "'.$name.'",
            "apellido_cliente": "'.$lastname.'",
            "nit": "'.$nit.'",
            "razón_social": "'.$rs.'",
            "ci": "'.$ci.'",
            "emite_factura": 0,
            "tipo_factura": 3,
            "fecha_vencimiento": "'.$now.'",
            "lineas_detalle_deuda": [
                {
                    "concepto": "TEST PRODUCTO 1",
                    "cantidad": 1,
                    "costo_unitario": 1,
                    "descuento_unitario": 0
                }
            ],
            "lineas_metadatos": [
                {
                    "nombre": "Promo",
                    "dato": "Liquidación especial de invierno"
                },
                {
                    "nombre": "Vendedor",
                    "dato": "Juan Perez"
                },
                {
                    "nombre": "Tienda",
                    "dato": "Tienda Virtual 001"
                }
            ]
        }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $response = json_decode($response,true);

       if (($response["error"] == 0) && ($response["qr_simple_error"] == "")){
            $sqlx = "UPDATE mod_payments_tx_tickets SET 
                        mod_pay_tx_tck_state = '2',
                        mod_pay_tx_tck_txr = '".$response['id_transaccion']."',
                        mod_pay_tx_tck_return = '".json_encode($response)."'
                    WHERE mod_pay_tx_tck_id = '" . $identificador . "' AND mod_pay_tx_tck_code = '".$tk."'";
            $this->fmt->querys->consult($sqlx);
            //$rtn["sql"] =  $sqlx ;
            $rtn["Error"] = 0;
            $rtn["status"] = 'success';
            $rtn["message"] = 'se ha generado el codigo qr y registro de pago';
        } 

        if ($response["qr_simple_error"] != ""){
            $sqlx = "UPDATE mod_payments_tx_tickets SET 
                        mod_pay_tx_tck_return = '".json_encode($response)."'
                    WHERE mod_pay_tx_tck_id = '" . $identificador . "' AND mod_pay_tx_tck_code = '".$tk."'";
            $this->fmt->querys->consult($sqlx);

            $rtn["Error"] = 1;
            $rtn["status"] = 'error';
            $rtn["message"] = $response["qr_simple_error"];            
        }

        $rtn["response"] = $response;
        $rtn["url"] = _PATH_WEB.'apis/v1/pay-success.php?id='.$sussessPay;
        return $rtn;
    }

    public function insertQRPaymentTemp(array $var = null)
    {
        $code = $var["code"];
        $acuId = $var["acuId"];
        $nit = $var["nit"];
        $cpeId = $var["cpeId"];
        $entitie = $var["entitie"];
        $entId = $var["entId"];
        $mode = $var["mode"];
        $json = $var["json"];

        $now = $this->fmt->data->dateFormat();

        $insert = "mod_pay_tx_tmp_code,mod_pay_tx_tmp_acu_id,mod_pay_tx_tmp_cpe_id,mod_pay_tx_tmp_nit,mod_pay_tx_tmp_entitie,mod_pay_tx_tmp_ent_id,mod_pay_tx_tmp_mode,mod_pay_tx_tmp_json,mod_pay_tx_tmp_register_date";
        $values ="'".$code."','" . $acuId . "','" . $cpeId . "','" . $nit . "','" . $entitie . "','" . $entId . "','" . $mode . "','" . $json . "','" . $now . "'";
        $sql= "insert into mod_pay_tx_temp (".$insert.") values (".$values.")";
        $this->fmt->querys->consult($sql,__METHOD__);

        $sql = "select max(mod_pay_tx_tmp_id) as id from mod_pay_tx_temp";
        $rs = $this->fmt->querys->consult($sql,__METHOD__);
        $row = $this->fmt->querys->row($rs);

        return $row["id"];
        
    }

    public function getQRPaymentTemp(array $var = null)
    {

        //return json_encode($var);
        $_PATH_HOST = $var["pathHost"];
        $_PATH_NUCLEO = $var["pathNucleo"];

        require_once $_PATH_NUCLEO."modules/accounts/models/class/class.customers.php";
        require_once $_PATH_NUCLEO."modules/accounts/models/class/class.accounts.php";

        $sql= "SELECT * FROM mod_pay_tx_temp WHERE mod_pay_tx_tmp_state = '0' ORDER BY mod_pay_tx_tmp_register_date DESC";
        //return json_encode($sql);
        $rs = $this->fmt->querys->consult($sql,__METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $code = $row["mod_pay_tx_tmp_code"];
            $entitie = $row["mod_pay_tx_tmp_entitie"];
            $mode = $row["mod_pay_tx_tmp_mode"];
            $dateNow = $row["mod_pay_tx_tmp_register_date"];
            $nit = $row["mod_pay_tx_tmp_nit"];
            $acuId = $row["mod_pay_tx_tmp_acu_id"];
            $entId = $row["mod_pay_tx_tmp_ent_id"];
            $json = json_decode($row["mod_pay_tx_tmp_json"],true); 
            //$json = "";

            //return json_encode($row);

            if (($mode == "qr") && ($entitie == "bnb") ) {

                

                 $curl = curl_init();

                    curl_setopt_array(
                        $curl,
                        array(
                            CURLOPT_URL => 'https://marketapi.bnb.com.bo/ClientAuthentication.API/api/v1/auth/token',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_POSTFIELDS => '{
                        "accountId" : "CuXb7taz7tO5yy7nD6EELg==",
                        "authorizationId" : "7+bQLNq78kxCfGKhItnmtUgjE8JpPGSJA1qT+WxolOg="
                    }',
                            CURLOPT_HTTPHEADER => array(
                                'Content-Type:application/json'
                            )
                        )
                    );

                $response = curl_exec($curl);

                curl_close($curl);

                $response = json_decode($response,true);

                //return $response["success"];
                

                if ($response["success"] == true) {
                    //return json_encode($code);
                    $curl = curl_init();

                    curl_setopt_array(
                        $curl,
                        array(
                            CURLOPT_URL => 'https://marketapi.bnb.com.bo/QRSimple.API/api/v1/main/getQRStatusAsync',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_POSTFIELDS => '{
                            "qrId": "' . $code . '",
                        }',
                            CURLOPT_HTTPHEADER => array(
                                'Content-Type:application/json',
                                'Authorization: Bearer ' . $response["message"],
                                'cache-control: no-cache',
                            )
                        )
                    );


                    $responseCurl = curl_exec($curl);

                    curl_close($curl);

                    $response = json_decode($responseCurl,true);

                    //return json_encode($response);
 
                    $qrSuccess = $response["success"];

                    if ($qrSuccess == false) {
                        $rtn["Error"] = 1;
                        $rtn["status"] = "error";
                        $rtn["message"] = "Error." . $response["message"];
                        return $rtn;
                    }

                    $qrId = $response["id"];
                    $voucherId = $response["voucherId"]; //null = nopago ,0 = pago!!
                    $stateId = $response["statusId"];  //1=No Usado; 2= Usado 3=Expirado; 4=Con error. 
                    $expirationDate = $response["expirationDate"];

                    //pr8:"{"id":7203998,"statusId":2,"expirationDate":"2024-08-24T23:59:59","voucherId":"2N8M135766","success":true,"message":""}"

                    //$var[""]
                    //return json_encode(array("qrId" => $qrId,"voucherId" => $voucherId,"stateId" => $stateId,"expirationDate" => $expirationDate));

                    if ($stateId == 2) {
                        $customers = new CUSTOMERS($this->fmt);  
                        $accounts = new ACCOUNTS($this->fmt); 
                        
                        $planArray = $accounts->dataPlanId($json["planid"]);

                        /* $sql = "UPDATE mod_pay_tx_temp SET 
                            mod_pay_tx_tmp_state = '1',
                            mod_pay_tx_tmp_response = '" . json_encode($response) . "'
                        WHERE mod_pay_tx_tmp_code = '" . $code . "'";
                        $this->fmt->querys->consult($sql,__METHOD__); */

                        $sql = "UPDATE mod_accounts_users SET mod_acu_mothers_lastname = '" . $json["mothersLastname"] . "' WHERE mod_acu_id = '" . $acuId . "'";
                        $this->fmt->querys->consult($sql,__METHOD__);


                        $accountData = $accounts->dataAccount($acuId);

                        $vars["entitieId"] = $entId;
                        $vars["vars"]["inputs"]["nit"] = $nit;
                        $vars["vars"]["inputs"]["acuId"] = $acuId;
                        $vars["vars"]["inputs"]["code"] = $code;
                        $vars["vars"]["inputs"]["planId"] = $json["planid"];
                        $vars["vars"]["inputs"]["name"] = $accountData["name"];
                        $vars["vars"]["inputs"]["email"] = $accountData["email"];
                        //$vars["vars"]["inputs"]["mothersLastname"] = $json["mothersLastname"];
                        $vars["vars"]["inputs"]["lastname"] = $accountData["lastname"];
                        $vars["vars"]["inputs"]["ci"] = $json["ci"];
                        $vars["vars"]["inputs"]["complemento"] = $json["complemento"];

                        $arrayCPE = $customers->addCustomerPerson($vars);
                        $cpeId = $arrayCPE["data"]["id"];

                        $sql = "UPDATE  mod_pay_tx_temp SET mod_pay_tx_tmp_cpe_id = '" . $cpeId . "' WHERE mod_pay_tx_tmp_code = '" . $code . "'";
                        $this->fmt->querys->consult($sql,__METHOD__);

 
                        $pay = $planArray["cost"];
                        $coin = $planArray["coin"];
                        $codeIn =  $code."-".$dateNow;
                       
                        $registerDate = $this->fmt->data->dateFormat();
                        $rs = $accountData["name"] . " " . $accountData["lastname"];
                        $invoice = "no-facturado";
                        $numInvoice = 0;
                        $mode = "payQR";
                        $txr = $code;
                        $callback = json_encode($response);
                        $mail = $accountData["email"];
                        $userId = 0;
                        $vars["vars"]["inputs"]["mothersLastname"] = $json["mothersLastname"];                        
                        $json = json_encode($vars);
                        $return = "";
                        $state = "1";
                        $datePayRegister = $expirationDate;

                        $insertColumns = "mod_pay_tx_sbs_acu_id,mod_pay_tx_sbs_cpe_id,mod_pay_tx_sbs_pay,mod_pay_tx_sbs_code,mod_pay_tx_sbs_coin,mod_pay_tx_sbs_rs,mod_pay_tx_sbs_nit,mod_pay_tx_invoice,mod_pay_tx_num_invoice,mod_pay_tx_sbs_mode,mod_pay_tx_sbs_txr,mod_pay_tx_sbs_date_pay_register,mod_pay_tx_sbs_callback,mod_pay_tx_sbs_mail,mod_pay_tx_sbs_user_id,mod_pay_tx_sbs_json,mod_pay_tx_sbs_return,mod_pay_tx_sbs_register_date,mod_pay_tx_sbs_state";
        
                        $values = "'".$acuId."','".$cpeId."','".$pay."','".$codeIn."','".$coin."','".$rs."','".$nit."','".$invoice."','".$numInvoice."','".$mode."','".$txr."','".$datePayRegister."','".$callback."','".$mail."','".$userId."','".$json."','','".$registerDate."','1'";

                        $sql = "INSERT INTO mod_pay_tx_suscriptions (".$insertColumns.") VALUES (".$values.")";
                        $this->fmt->querys->consult($sql,__METHOD__);

                        $sql = "select max(mod_pay_tx_sbs_id) as id from mod_pay_tx_suscriptions";
                        $rs = $this->fmt->querys->consult($sql, __METHOD__);
                        $row = $this->fmt->querys->row($rs);

                        $suscriptionId = $row["id"];


                        

                        $rtn["Error"] = 0;
                        $rtn["status"] = "success";
                        $rtn["data"]["suscriptionId"] = $suscriptionId;
                        $rtn["data"]["tx"] = $tx;
                        $rtn["message"] = "Transacción Exitosa";
                         
                        $sql = "UPDATE mod_pay_tx_temp SET 
                            mod_pay_tx_tmp_state = '1',
                            mod_pay_tx_tmp_response = '" . json_encode($rtn) . "'
                        WHERE mod_pay_tx_tmp_code = '" . $code . "'";
                        $this->fmt->querys->consult($sql,__METHOD__);

                    }

                    

                    //$customers->addCustomerPerson($var);
                    //$this->registerTxSuscription();
                    
                    /* $rtn["Error"] = 0;
                    $rtn["status"] = "success";
                    $rtn["data"]["qrId"] = $response["qrId"];
                    $rtn["message"] = $response;
                    return json_encode($rtn); */
                     
                    
                }
                
            }

        } else {
            return 0;
        }
        

        

    }


    public function registerTxSuscription(array $transactionData = null)
    {
         
        $acuId = $transactionData["acuId"];
        $cpeId = $transactionData["cpeId"];
        $pay = $transactionData["pay"];
        $code = $transactionData["code"];
        $coin = $transactionData["coin"];
        $rs = $transactionData["rs"];
        $nit = $transactionData["nit"];
        $invoice = $transactionData["invoice"];
        $numInvoice = $transactionData["numInvoice"];
        $mode = $transactionData["mode"];
        $txr = $transactionData["txr"];
        $callback = $transactionData["callback"];
        $mail = $transactionData["mail"];
        $userId = $transactionData["userId"];
        $json = $transactionData["json"];
        $return = $transactionData["return"];
        $state = $transactionData["state"];
        $datePayRegister = $transactionData["datePayRegister"];
        $registerDate = $this->fmt->data->dateFormat();

        $insertColumns = "mod_pay_tx_sbs_acu_id,mod_pay_tx_sbs_cpe_id,mod_pay_tx_sbs_pay,mod_pay_tx_sbs_code,mod_pay_tx_sbs_coin,mod_pay_tx_sbs_rs,mod_pay_tx_sbs_nit,mod_pay_tx_invoice,mod_pay_tx_num_invoice,mod_pay_tx_sbs_mode,mod_pay_tx_sbs_txr,mod_pay_tx_sbs_date_pay_register,mod_pay_tx_sbs_callback,mod_pay_tx_sbs_mail,mod_pay_tx_sbs_user_id,mod_pay_tx_sbs_json,mod_pay_tx_sbs_return,mod_pay_tx_sbs_register_date,mod_pay_tx_sbs_state";
        

        $sql =  "INSERT INTO mod_pay_tx_suscriptions (mod_pay_tx_sbs_acu_id) VALUES ('.$acuId.')";

        $this->fmt->querys->consult($sql, __METHOD__, array_values($data));

        $sql = "SELECT MAX(mod_pay_tx_sbs_id) AS id FROM mod_pay_tx_suscriptions";
        $resultSet = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($resultSet);

        return $row["id"]; 
    }



    public function txTemp(array $var = null)
    {
        //return $var;

        $now = $this->fmt->data->dateFormat();
        

        $sql = "SELECT * FROM mod_pay_tx_temp WHERE mod_pay_tx_tmp_state = '0' AND mod_pay_tx_tmp_register_date = '" . $now . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);

        return $num;
        
    }

}