import {
    loadView,
    replacePath,
    disableId,
    unDisableId,
    alertPage,
    accessToken,
    disabledBtn,
    alertMessageError,
    activateBtn,
    deactivateBtn,
    addHtml,
    btnLoading,
    btnLoadingRemove,
    removeHtml
} from "../../components/functions.js";
import {
    validateEmail,
    validateNum,
    focusInput,
    optionsForm
} from "../../components/forms.js";
import {
    renderSelect
} from "../../components/renders/renderForms.js";
import {
    renderModal,
    renderModalClean,
    renderInnerWindow
} from "../../components/renders/renderModals.js";
import {
    renderTableForm,
    renderCol,
    renderColCheck,
    renderColId,
    renderColTitle,
    renderRowsTable,
    renderColState,
    renderColActions,
    renderColCategorys,
} from "../../components/renders/renderTables.js";
import {
    categorysListTable,
    createTable,
    dataTable,
    removeItemTable,
    inactiveBtnStateItem,
    activeBtnStateItem,
    mountTable,
    colCheck,
    colId,
    colName,
    colTitle,
    colState,
    colActionsBase,
    deleteItemModule,
    renderEmpty
} from "../../components/tables.js";
import {
    dayLiteral
} from "../../components/dates.js";

import {
    loadEventsActives,
    getData as getDataTicketingEvents
} from "../../tickets/components/ticketingEvents.js";

import * as hallsJs from "../../tickets/halls/components/hall-settings.js";
import * as ticketingEventsJs from "../../tickets/components/ticketingEvents.js";
import {
    loadAccountsUsers,
    add
} from "../../accounts/components/accountsUsers.js";
import {
    loadHalls
} from "../../tickets/components/tickets.js";
import {
    renderFormCreateCustomer,
    renderCustomerListItem,
    renderSuccessPay
} from "./renders/renderSalesPointTickets.js";
import {
    removeModalId
} from "../../components/modals.js";


const module = "salesPointTickets";
const system = "sales";
const pathurl = "modules/sales/";
let intervalId;
let timerRunning = false;
let tableId = "tableSalesPointTickets";

export const salesPointTicketsIndex = () => {
    loadEventsActives().then((events) => {
        //console.log('events', events, system, module);
        console.log("loadEventsActives", events);
        //console.log(arrayEvents);

        localStorage.setItem("eventsTickets", JSON.stringify(events.items));



        loadView(
            _PATH_WEB_NUCLEO +
            "modules/sales/views/salesPointTickets.html?" +
            _VS
        ).then((str) => {


            loadHalls().then((response) => {
                //console.log('loadHalls', response);
                if (response.Error == 0 && events.items != 0) {

                    let eventTicketsNowArray = JSON.parse(localStorage.getItem("eventsTicketNow"));
                    console.log("eventTicketsNowArray", eventTicketsNowArray);
                    if (eventTicketsNowArray == null) {
                        events['eventId'] = events.items[0].id;
                        events['hallIdEvent'] = events.items[0].hall
                        localStorage.setItem("eventsTicketNow", events.items[0].id);
                        localStorage.setItem("staffMaster", JSON.stringify(events.items[0].staffMaster));
                    }

                    events['strView'] = str;
                    loadUIEvent(events);

                } else if (events.items == 0) {
                    strView = '<div class="message messageCenter mesaggeAlert">No hay eventos registrados en el futuro. Crea un evento. </div>';
                    $(".bodyModule[system='" + system + "'][module='" + module + "']").html(strView);
                } else {
                    alertPage({
                        title: "Error",
                        text: "No se encontraron salas",
                        icon: "error",
                    });
                }
            }).catch(console.warn());



        }).catch(console.warn());
    }).catch(console.warn());
};

export const loadUIEvent = (events = []) => {
    console.log('loadUIEvent', events);
    let strView = events.strView;
    let halls = JSON.parse(localStorage.getItem("halls"));
    let hallIdEvent = events.hallIdEvent;
    let hallInit = halls.find(obj => obj.id === hallIdEvent);
    let hallId = hallInit.id;
    let staffMaster = JSON.parse(localStorage.getItem("staffMaster"));
    //console.log(halls.find( obj => obj.id === 1 ));
    //console.log("hallInit", hallInit);
    let rootFn = "hallsJs." + hallInit.drawFnInit;
    let eventId = events.eventId;


    let array = events.items;
    let arrayEvents = [];
    for (let i = 0; i < array.length; i++) {
        const ele = array[i];
        let item = {
            id: ele.id,
            name: ele.name +
                " :  " +
                dayLiteral({
                    dateInput: ele.initDate,
                    mode: "complete"
                }),
        };
        arrayEvents[i] = {
            ...item
        };
    }
    arrayEvents[0]["hallId"] = hallIdEvent;

    strView = strView.replace(/{{_MODULE}}/g, module);
    strView = strView.replace(/{{_SYSTEM}}/g, system);
    strView = strView.replace(/{{_SCRIPTS}}/g, "");
    strView = strView.replace(/{{_OPTIONS_COURTESY}}/g, optionsForm({
        options: staffMaster
    }));
    strView = strView.replace(/{{_PATHURL}}/g, pathurl);
    strView = strView.replace(
        /{{_LIST_EVENTS}}/g,
        renderSelect({
            array: arrayEvents,
            id: "inputEvent",
            attr: `data-module="${module}" data-system="${system}"`,
            selected: eventId,
        })
    );
    strView = replacePath(strView);

    $(".bodyModule[system='" + system + "'][module='" + module + "']").html(strView);

    eval(rootFn + ".index({module, hallId, eventId})");
    resizeBodysalesPointTickes();

    startTimer({
        eventId,
        hallId,
        rootFn
    });
}

export const resizeBodysalesPointTickes = (vars) => {
    const w = $(window).width();
    const h = $(window).height();
    const hBody = $(".bodyModule[module='" + module + "']").outerHeight();
    const wBody = $(".bodyModule[module='" + module + "']").outerWidth();

    const hBodyHead = $(
        ".bodyModule[module='" + module + "'] .head"
    ).outerHeight();
    const htBody = hBody;

    const hDrawUI = htBody - 44;
    const sidebar = $(".sidebarMenuModule[module='" + module + "']").outerWidth();

    const wBodySales = w - sidebar;


    $(".bodyModule[module='" + module + "'] .tbody").outerHeight(htBody);
    $(
        ".bodyModule[module='" + module + "'] .tbody .varSales label"
    ).outerHeight(hBodyHead);

    $(".bodyModule[module='" + module + "']").outerWidth(wBodySales);

    const wVarSales = $(
        ".bodyModule[module='" + module + "'] .varSales"
    ).outerWidth();
    const wtVarSales = wBodySales - wVarSales;

    $(".bodyModule[module='" + module + "'] .tbody .draw").outerWidth(
        wtVarSales
    );
    $(".bodyModule[module='" + module + "'] .tbody .draw .drawUI").outerWidth(
        wtVarSales
    );
    $(".bodyModule[module='" + module + "'] .tbody .draw .drawUI").outerHeight(
        hDrawUI
    );
    //console.log(hBody, hBodyHead);
};

export const loadCustomerForm = (vars) => {
    //console.log('loadCustomerForm');
    loadView(_PATH_WEB_NUCLEO + "modules/sales/views/customerForm.html?" + _VS).then((str) => {
            //console.log(str);
            loadAccountsUsers({
                orderBy: "recordDate ASC",
                limit: "100"
            }).then((response) => {
                console.log('loadAccountsUsers', response);
                const id = 'modalCustomerTickets';

                let body = renderTableForm({
                    id: "formTableCustomers",
                    data: response,
                    module,
                    system,
                    thead: [{
                        label: "id",
                        col: "id",
                        cls: "colId",
                        type: "none",
                    }, {
                        label: "Nombre",
                        col: "name",
                        cls: "colName",
                        type: "input",
                        id: "inputNameCol",
                    }, {
                        label: "Apellido p.",
                        col: "lastnameFather",
                        cls: "colLastName",
                        type: "input",
                        id: "inputLastnameFather",
                    }, {
                        label: "Apellido M.",
                        col: "lastnameMother",
                        cls: "colLastName",
                        type: "input",
                        id: "inputLastnameMother",
                    }, {
                        label: "Email",
                        col: "email",
                        cls: "colEmail",
                        type: "input",
                        id: "inputEmail",
                    }, {
                        label: "Celular",
                        col: "celular",
                        cls: "colCelular",
                        type: "input",
                        id: "inputCelular",
                    }, {
                        label: "Acción",
                        col: "",
                        cls: "colActions",
                        type: "action",
                        action: 'returnFn',
                        fnInit: 'nav.salesPointTickets.addAccountUser',
                        fn: 'nav.salesPointTickets.kiad'
                    }]
                });
                str = str.replace(/{{_ID}}/g, id);
                str = str.replace(/{{_MODULE}}/g, module);
                str = str.replace(/{{_BODY}}/g, body);
                $(".bodyModule[module='" + module + "']").append(
                    renderModal({
                        cls: "modalExtended",
                        body: str,
                        id,
                    })
                );
                $("#formTableCustomers #inputNameCol").focus();
            }).catch(console.warn());
        })
        .catch(console.warn());
}

export function addAccountUser(vars) {
    console.log('addAccountUser', vars);
    const id = vars.vars;
    let email = $("#" + id + " #inputEmail").val();
    let celular = $("#" + id + " #inputCelular").val();
    let name = $("#" + id + " #inputNameCol").val();
    let lastnameFather = $("#" + id + " #inputLastnameFather").val();
    let lastnameMother = $("#" + id + " #inputLastnameMother").val();

    //console.log(celular, typeof celular, validateNum(celular), email, validateEmail(email))

    if (validateEmail(email) && validateNum(celular) && name != "" && lastnameFather != "") {
        let arrayData = {
            name,
            lastnameFather,
            lastnameMother,
            email,
            celular,
        };
        if (vars.item == 'undefined') {
            console.log("item");
            add(arrayData);
        }
    } else {
        alertPage({
            text: "Error. Los datos no son correctos. Itentelo denuevo.",
            icon: "icn icon-alert-warning",
            animation_in: "bounceInRight",
            animation_out: "bounceOutRight",
            tipe: "danger",
            time: "3500",
            position: "top-left",
        })
    }




}

export const modalCloseCustomerForm = (vars) => {
    console.log('modalCloseCustomerForm');
    unDisableId("inputCustomer");
}

function startTimer(vars = []) {
    if (!timerRunning) {
        intervalId = setInterval(() => {
            //console.log('Timer running...',vars);
            getDataTicketingEvents({
                task: 'getTicketsEvents',
                return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
                input: {
                    eventId: vars.eventId,
                    hallId: vars.hallId
                }
            }).then((response) => {
                console.log('getData getTicketsEvents', response);

                if (response.status == 'success') {
                    let rootFn = vars.rootFn;
                    let payQR = response.payQR;
                    const numPayQR = response.numPayQR;

                    eval(rootFn + ".setData(response)");

                    if (numPayQR != 0) {
                        //console.log("chaplinGeneralSetDataQR");
                        let ticket = JSON.parse(localStorage.getItem("eventsTicketNow"));
                        let eventId = ticket.id;
                        let modalQRData = $("#payQR").data();
                        //$(".btnMesa").removeClass("disabled");
                        payQR.forEach((element, index) => {
                            console.log("data.forEach ~ element:", element, modalQRData.eventid, eventId)
                            let callback = JSON.parse(element.callback);
                            let txrId = element.id;
                            let txr = element.txr;

                            if ((txr == modalQRData.txr) && (eventId == modalQRData.eventid)) {

                                addHtml({
                                    selector: '#payQR .modalInner',
                                    type: 'insert',
                                    content: renderSuccessPay(element)
                                }) //type: html, append, prepend, before, after
                            }

                        });
                    }
                } else {
                    if (response != '0') {
                        alertMessageError({
                            message: response.message
                        })
                    }

                }
            }).catch(console.warn());
        }, 5000);

        timerRunning = true;
    }
}

function stopTimer() {
    if (timerRunning) {
        clearInterval(intervalId);
        timerRunning = false;
    }
}

export const addCustomerInput = (vars = []) => {
    //console.log('addCustomerInput',vars);
    let data = vars;
    $("#inputCustomer").addClass("disabled");
    $("#inputCustomer").val("");
    $(".varSales .formSearch").append(`<div class="boxCustomer">
            <div class="title">
                <label>${data.id}</label>
                <span>${data.name} ${data.lastname}</span>
            </div>
            <a class="btn btnIcon" id="btnCloseBoxCustomer"><i class="icon icon-circle-close"></i></a>
        </div>`);
    $("#ticketReservationForm").attr("data-cpeid", data.id);
    $(`div[module="salesPointTickets"] .outputs`).html("");
    $(`div[module="salesPointTickets"] .outputs`).removeClass("on");
    stateBtnAccionsVarSales();
}

export const stateBtnAccionsVarSales = (vars = []) => {

    let cpeId = $("#ticketReservationForm").attr("data-cpeid");
    let count = $("#ticketReservationForm").children().length;
    let reserveId = $("#ticketReservationForm").attr("data-reserveid");
    //console.log('stateBtnAccionsVarSales',count, cpeId);
    let id = vars.id || "btnTotaTicketsEvent";
    $("#btnTotaTicketsEvent").attr("data-cpeid", cpeId);
    $("#btnTotaTicketsEvent").attr("data-reserveid", reserveId);

    if ((count > 0) && (cpeId != "") && (cpeId != "undefined") && (cpeId != undefined) && (cpeId != "0")) {
        activateBtn("#" + id);
        activateBtn(".refReserveTicketEvent");
        $("#inputReferenceReserveTicketEvent").focus();
    } else {
        disabledBtn("#" + id);
        deactivateBtn(".refReserveTicketEvent");
        $("#inputReferenceReserveTicketEvent").val("");
    }
};

export const openWindowSales = (vars = []) => {
    console.log('openWindowSales', vars);
    let {
        module,
        type
    } = vars;
    let content = '';
    let eventNow = JSON.parse(localStorage.getItem("eventsTicketNow"));

    addHtml({
        selector: `.bodyModule`,
        type: 'append',
        content: renderModalClean({
            id: 'windowSales',
            title: `Ventas ${eventNow.name}`,
            attr: `data-module="${module}" data-type="${type}"`,
            selector: `.bodyModule[module="${module}"] .tbody`,
            body: `<div id="box${tableId}"></div>`
        })
    }) //type: html, append, prepend, before, after

    if (type == 'salesPointTickets') {
        ticketingEventsJs.getData({
            task: 'getTicketsEvents',
            return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
            input: {
                eventId: eventNow.id,
                hallId: eventNow.hallId,
                statusState: 'AND mod_eve_tck_state >0'
            }
        }).then((response) => {
            console.log('getTicketsEvent sales', response);
            if (response.status == 'success') {
                if (response.data != 0) {
                    let rows = "";
                    for (let i in response.data) {
                        let item = response.data[i];
                        //console.log("item", item);
                        let name = item.cpeData.name + " " + item.cpeData.lastname + `: <a hre="#">${item.cpeData.phone}</a> :${item.cpeData.ci}`;
                        let dataTicket = `Mesa${item.tableId} - s${item.num} ${item.coin}.${item.cost}`;
                        rows += renderRowsTable({
                            id: item.id,
                            content: renderColCheck({
                                    id: item.id,
                                }) +
                                renderColId({
                                    id: item.id,
                                }) +
                                renderCol({
                                    id: item.id,
                                    cls: '',
                                    attr: '',
                                    data: item.reserveId,
                                }) +
                                renderCol({
                                    id: item.id,
                                    cls: '',
                                    attr: '',
                                    data: item.reference,
                                }) +
                                renderCol({
                                    id: item.id,
                                    cls: '',
                                    attr: '',
                                    data: name,
                                }) +
                                renderCol({
                                    id: item.id,
                                    cls: '',
                                    attr: '',
                                    data: dataTicket,
                                }) +
                                renderCol({
                                    id: item.id,
                                    cls: '',
                                    attr: 'data-state="' + item.state + '"',
                                    data: item.stateText,
                                }) +

                                renderColActions({
                                    id: item.id,
                                    type: "btnDeleteFn",
                                    name: item.name,
                                    fnType: "btnDeleteRol",
                                    module,
                                    system,
                                }),
                        });
                    }

                    mountTable({
                        id: tableId,
                        columns: [
                            ...colCheck,
                            ...colId,
                            {
                                label: "#Reserva",
                                cls: "colName",
                            },
                            {
                                label: "Descripción",
                                cls: "",
                            },
                            {
                                label: "Cliente",
                                cls: "",
                            },
                            {
                                label: "Datos del ticket",
                                cls: "",
                            },

                            ...colState,
                            ...colActionsBase,
                        ],
                        rows,
                        module,
                        system,
                        container: "#box" + tableId,
                    });
                } else if (response === 0 || response.items == 0) {
                    $("#box" + tableId).html(renderEmpty());
                } else {
                    alertMessageError({
                        title: 'Error',
                        text: responsePath.message
                    });
                }



            }
        }).catch(console.warn());
    }
}

document.addEventListener("DOMContentLoaded", function () {

    //doble clik en el inputCustomer
    $("body").on("dblclick", `#inputCustomer`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        //console.log('dblclick');
        $("#inputCustomer").val("");
        $("#inputCustomer").focus();
        $("#ticketReservationForm").attr("data-cpeid", "");
        $(`div[module="salesPointTickets"] .outputs`).html("");
        $(`div[module="salesPointTickets"] .outputs`).removeClass("on");
        deactivateBtn("#btnAddCustomerExpress");
    });

    $("body").on("keyup", `#inputCustomer`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`#inputCustomer`,data);

        let keyValue = $(this).val();
        if (keyValue.length > 2) {
            getDataTicketingEvents({
                task: 'searchCustomers',
                return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
                input: keyValue
            }).then((response) => {
                //console.log('getData searchCustomers', response);
                if (response.status == 'success') {
                    let data = response.data.output;
                    if (data == 0) {
                        activateBtn("#btnAddCustomerExpress");
                    } else {
                        //recorrer data
                        let str = "";
                        data.forEach((item, index) => {
                            str += renderCustomerListItem(item);
                        });



                        addHtml({
                            selector: 'div[module="salesPointTickets"] .outputs',
                            type: 'insert',
                            content: str
                        }) //type: html, append, prepend, before, after
                        $(`div[module="salesPointTickets"] .outputs`).addClass("on");
                        $(`div[module="salesPointTickets"] .outputs`).scrollTop(0);

                    }
                } else {
                    alertMessageError({
                        message: response.message
                    })
                }
            }).catch(console.warn());
        } else {
            deactivateBtn("#btnAddCustomerExpress");
            $(`div[module="salesPointTickets"] .outputs`).html("");
            $(`div[module="salesPointTickets"] .outputs`).removeClass("on");
        }

    });

    $("body").on("click", `#btnAddCustomerExpress`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`#btnAddCustomerExpress`,data);
        addHtml({
            selector: 'body',
            type: 'prepend',
            content: renderModalClean({
                id: "formCreateCustomen",
                title: 'Agregar cliente',
                body: renderFormCreateCustomer({})
            })
        }).then(() => {
            focusInput("inputName");
        });
    });

    $("body").on("click", `#btnCreateCustomer`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`#btnCreateCustomer`, data);
        let event = JSON.parse(localStorage.getItem("eventsTicketNow"));

        let name = $("#inputName").val();
        let lastname = $("#inputLastname").val();
        let email = $("#inputEmail").val();
        let phone = $("#inputPhone").val();
        let ci = $("#inputCI").val();
        let adder = $("#inputAdder").val();
        let count = 0;

        if (name == "") {
            count++;
            $("#inputName").addClass("error");
            $("#messageFormCreateCustomer").append(`<div class="message messageDanger">Ingresa nombre/s validos.</div>`);
        }
        if (lastname == "") {
            count++;
            $("#inputLastname").addClass("error");
            $("#messageFormCreateCustomer").append(`<div class="message messageDanger">Ingresa apellido/s valido.</div>`);
        }
        if (ci == "" || ci.length < 5 || ci.length > 10 || validateNum(ci) == false) {
            count++;
            $("#inputCI").addClass("error");
            $("#messageFormCreateCustomer").append(`<div class="message messageDanger">Ingresa un CI valido</div>`);
        }
        //console.log("email", validateEmail(email));
        if (email == "" || validateEmail(email) == false) {
            count++;
            $("#inputEmail").addClass("error");
            $("#messageFormCreateCustomer").append(`<div class="message messageDanger">Ingresa un e-mail valido.</div>`);
        }
        if (phone == "" || validateNum(phone) == false) {
            count++;
            $("#inputPhone").addClass("error");
            $("#messageFormCreateCustomer").append(`<div class="message messageDanger">Ingresa un celular valido.</div>`);
        }

        if (count == 0) {
            btnLoading("btnCreateCustomer", "Cargando...");

            getDataTicketingEvents({
                task: 'createCustomer',
                return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
                input: {
                    name,
                    lastname,
                    ci,
                    email,
                    phone,
                    adder,
                    event
                }
            }).then((response) => {
                console.log('getData createCustomer', response);
                if (response.status == 'success') {
                    let data = response.data;
                    deactivateBtn("#btnAddCustomerExpress");
                    addCustomerInput(data);
                    removeHtml({
                        selector: "#formCreateCustomen"
                    });
                } else {
                    alertMessageError({
                        message: response.message
                    });
                    setTimeout(() => {
                        btnLoadingRemove("btnCreateCustomer", "Crear Cliente", "icon icon-plus");
                    }, 3500);
                }
            }).catch(console.warn());

        }


    });

    $("body").on("keyup", `#formCreateCustomen input`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`#formCreateCustomen input`, data);
        $("#messageFormCreateCustomer").html("");
        $("#formCreateCustomen input").removeClass("error");

    });

    $("body").on("click", `#btnCloseBoxCustomer`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`#btnCloseBoxCustomer`, data);
        $("#ticketReservationForm").attr("data-cpeid", "");
        $("#inputCustomer").removeClass("disabled");
        $(".boxCustomer").remove();
        focusInput("inputCustomer");
        stateBtnAccionsVarSales();
    });

    $("body").on("click", `.btnItemListCustomer`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`.btnItemListCustomer`,data);
        addCustomerInput(data);

    });

    $("body").on("click", `#btnSuccessPay`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`#btnSuccessPay`, data);

        removeModalId("payQR");

    });

    $("body").on("click", `#btnVEntasSalesPointTickets`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`#btnVEntasSalesPointTickets`,data);
        openWindowSales({
            module,
            type: "salesPointTickets"
        });

    });

    $("body").on("change", `.bodyModule[module="${module}"] #inputEvent`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let value = $(this).val();
        console.log(`.bodyModule[module="${module}"] #inputEvent`, data, value);

    });

});