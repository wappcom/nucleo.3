export const renderFormCreateCustomer = (vars =[]) => {
    //console.log('renderFormCreateCustomer',vars);
    return /*html*/`
        <div class="form formNoBottom" id="">
            <div class="formControl formFlex">
                <input class="formInput inputNameCreateCustomer" type="text" id="inputName" tabindex="3" name="inputName" placeholder="*Nombre(s)">
                <input class="formInput" type="text" id="inputLastname" tabindex="4" name="inputLastname" placeholder="*Apellidos">
            </div>
            <div class="formControl formFlex">
                <input class="formInput form60w" type="text" id="inputEmail" tabindex="5" name="inputEmail" placeholder="*Email">
                <input class="formInput form40w" type="text" id="inputPhone" tabindex="6" name="inputPhone" placeholder="*Celular">
            </div>
            <div class="formControl formFlex">
                <input class="formInput  form40w" type="text" id="inputCI" tabindex="7" name="inputCI" placeholder="*DNI/CI">
                <input class="formInput  form20w" type="text" id="inputAdder" tabindex="8" name="inputAdder" placeholder="Complemento">
            </div>
            <div class="formControl" id="messageFormCreateCustomer">
            </div>
            <div class="formControl formFlexRight ">
                <button class="btn btnLink btnCancel" tabindex="9" for="formCreateCustomen">
                    <span>Cancelar</span>
                </button>
                <button class="btn btnPrimary btnIconLeft" tabindex="10" id="btnCreateCustomer">
                    <i class="icon icon-plus"></i>
                    <span>Crear Cliente</span>
                </button>
            </div>
        </div>
    `;
}

export const renderCustomerListItem = (vars =[]) => {
    //console.log('renderCustomerList',vars);
    let id = vars.id ? vars.id : "";
    let name = vars.name ? vars.name : "";
    let lastname = vars.lastname ? vars.lastname : "";
    let email = vars.email ? vars.email : "";
    let phone = vars.phone ? vars.phone : "";
    let ci = vars.ci ? vars.ci : "";

    return /*html*/`
        <a  class="btn btnItemListCustomer" 
            data-id="${id}"
            data-name="${name}"
            data-lastname="${lastname}" 
            data-email="${email}"
            data-phone="${phone}"
            data-ci="${ci}"
        ><i class="icon icon-chevron-right"></i>${name} ${lastname}</a>
    `;
}

export const renderSuccessPay = (vars =[]) => {
    console.log('renderSuccessPay',vars);
    const dataCpeId = vars.dataCpeId ? vars.dataCpeId : "";
    const celular = dataCpeId["phone"] ? dataCpeId["phone"] : "";
    const nombre = dataCpeId["name"] ? dataCpeId["name"]+" "+dataCpeId["lastname"] : "";
    const txr = vars.txr ? vars.txr : "";
    return /*html*/`
        <div class="modalBody modalBodySuccessPay">
            <div class="message">
                <i class="icon icon-alert-success"></i>
                <h3>¡Pago realizado con éxito!</h3>
            </div>
        </div>
        <div class="modalFooter">
            <a class="btn btnSuccess btnIconLeft" id="" href="https://api.whatsapp.com/send?phone=591${celular}&text=Recibo%20tickets%20:%3A%20${txr}" >
                <i class="icon icon-whatsapp"></i>
                <span>Enviar Whatsapp</span>
            </a>
            <button class="btn btnPrimary btnIconLeft" id="btnSuccessPay">
                <i class="icon icon-check"></i>
                <span>Aceptar</span>
            </button>
        </div>
    `;
}