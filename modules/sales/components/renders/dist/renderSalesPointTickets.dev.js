"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.renderFormCreateCustomer = void 0;

var renderFormCreateCustomer = function renderFormCreateCustomer() {
  var vars = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  //console.log('renderFormCreateCustomer',vars);
  return (
    /*html*/
    "\n        <div class=\"form formNoBottom\" id=\"\">\n            <div class=\"formControl formFlex\">\n                <input class=\"formInput inputNameCreateCustomer\" type=\"text\" id=\"inputName\" tabindex=\"3\" name=\"inputName\" placeholder=\"*Nombre(s)\">\n                <input class=\"formInput\" type=\"text\" id=\"inputLastname\" tabindex=\"4\" name=\"inputLastname\" placeholder=\"*Apellidos\">\n            </div>\n            <div class=\"formControl formFlex\">\n                <input class=\"formInput form60w\" type=\"text\" id=\"inputEmail\" tabindex=\"5\" name=\"inputEmail\" placeholder=\"*Email\">\n                <input class=\"formInput form40w\" type=\"text\" id=\"inputPhone\" tabindex=\"6\" name=\"inputPhone\" placeholder=\"*Celular\">\n            </div>\n            <div class=\"formControl formFlex\">\n                <input class=\"formInput  form40w\" type=\"text\" id=\"inputCI\" tabindex=\"7\" name=\"inputCI\" placeholder=\"*DNI/CI\">\n                <input class=\"formInput  form20w\" type=\"text\" id=\"inputAdder\" tabindex=\"8\" name=\"inputAdder\" placeholder=\"Complemento\">\n            </div>\n            <div class=\"formControl\" id=\"messageFormCreateCustomer\">\n            </div>\n            <div class=\"formControl formFlexRight \">\n                <button class=\"btn btnLink btnCancel\" tabindex=\"9\" for=\"formCreateCustomen\">\n                    <span>Cancelar</span>\n                </button>\n                <button class=\"btn btnPrimary btnIconLeft\" tabindex=\"10\" id=\"btnCreateCustomer\">\n                    <i class=\"icon icon-plus\"></i>\n                    <span>Crear Cliente</span>\n                </button>\n            </div>\n        </div>\n    "
  );
};

exports.renderFormCreateCustomer = renderFormCreateCustomer;