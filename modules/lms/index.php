<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $_POST["page"] . "config.php");

require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

header('Content-Type: text/html; charset=utf8');
define("_VS", $fmt->options->version());

$html  = '';
$access_token = $fmt->auth->getInput('access_token');
$refresh_token = $fmt->auth->getInput('refresh_token');
$entitieId = $fmt->auth->getInput('idEntitie');
$rolId = $fmt->auth->getInput('idRol');
$pathSys = $fmt->auth->getInput('path');

$userId = $fmt->users->validateUserToken(array("access_token" => $access_token, "refresh_token" => $refresh_token, "idEntitie" => $entitieId));

if ($userId) {
    $arraySys = $fmt->systems->pathToData($pathSys);
    $sysId = $arraySys["sys_id"];
    $name = $arraySys["sys_name"];
    $icon = $arraySys["sys_icon"];
    $color = $arraySys["sys_color"];
    $pathUrlSystem = $arraySys["sys_pathurl"];
    $pathSystem = $arraySys["sys_path"];
    // $str = "var _CATEGORYS=" . json_encode($fmt->categorys->nodes(0, $entitieId)) . "\n";
    // $str .= "var _TYPES_PRODUCTS=" . json_encode($products->typesProducts()) . "\n";
    $modArray = $fmt->modules->modulesRols($rolId, $sysId, $entitieId);
    //var_dump($modArray);
    $modIdActive = $_POST["modIdActive"];
    if (!empty($modIdActive)) {
        $modActiveArray = $fmt->modules->dataId($modIdActive);
        $modIdActive =$modActiveArray["mod_pathurl"];
    } else {
        $modIdActive = $modArray[0]["mod_pathurl"];
    }
 
    $modArrayActive = $fmt->modules->dataId($_POST["modIdActive"]);
    $sibedarMod = $fmt->modules->sidebarMenuModulesHtml(array("sysId" => $sysId, "modules" => $modArray, "idActive" => $_POST["modIdActive"]));

    $index  = file_get_contents(_PATH_NUCLEO . $pathSystem . "views/index.html");

    $html =  str_replace("{{_VS}}", _VS, $index);
    $html =  str_replace("{{_NAME_TOP}}", $name, $html);
    $html =  str_replace("{{_MENU_TOP_MEDIUM}}", "", $html);
    $html =  str_replace("{{_MENU_TOP_RIGHT}}", "", $html);
    // $html =  str_replace("{{_PATH_MODULE}}",_PATH_WEB.$pathModule,$html);
    $html =  str_replace("{{_MODULE}}", $modIdActive, $html);
    $html =  str_replace("{{_NAME}}", "Menu " . $name, $html);
    $html =  str_replace("{{_ICON}}", $icon, $html);
    $html =  str_replace("{{_SCRIPTS}}", "", $html);
    $html =  str_replace("{{_COLOR}}", $color, $html);
    $html =  str_replace("_MODULE_SCRIPT", $str, $html);
    $html =  str_replace("{{_SIDEBAR_MODULE}}", $sibedarMod, $html);
    $html =  str_replace("{{_DASHBOARD}}", "dashboard", $html);
    //$html =  str_replace("{{_SIDEBAR_MODULE}}", json_encode( $modArray ), $html);

    $html =  str_replace("{{_SYSTEM}}", $pathUrlSystem, $html);
    $html =  $fmt->setUrlNucleo($html);
    echo $html;
} else {
    echo $fmt->errors->errorJson([
        "description" => "Access Auth. invalid user",
        "code" => "",
        "lang" => "es"
    ]);
}