import {
    capitalize
}
from "../../components/functions.js";

export const renderBtnMenuTop = (vars = []) => {
    //console.log('renderBtnMenuTop', vars);
    return /*html*/ `
        <a class = "btn btnLink btnIconLeft btnPeriodSyllabus connectedSortable" >
                <i class="icon icon-copy"></i>  
                <span>Periodos</span>
        </a>
   `;
}

export const renderBoxPeriordSyllabus = (vars = []) => {
    //console.log('renderBoxPeriordSyllabus', vars);
    let name = vars.name || '';
    let id = vars.id || '';
    let sylId = vars.sylId || '';
    return /*html*/ `
        <div class = "boxPeriodSyllabus formControl formGroupFrame"
        data-id = "${id}" >
            <div class="head nospace">
                <div class="title">
                    <span>${name}</span>
                </div>
                <div class="actions">
                    <a class="btn btnFull btnSmall btnIcon btnCollapse" data-status="0" data-id="${id}" data-syl-id="${sylId}"><i class="icon icon-arrow-o-du"></i></a>
                    <a class="btn btnFull btnSmall btnIconLeft btnAddContentSyllabus" data-id="${id}" data-syl-id="${sylId}">
                        <i class="icon icon-plus"></i>
                        <span>Agregar</span>
                    </a>
                    <a class="btn btnFull btnSmall btnIcon btnDeleteContentSyllabus" data-name="${name}" data-id="${id}" data-syl-id="${sylId}">
                        <i class="icon icon-trash"></i>
                    </a>
                </div>
            </div>
            <div class="boxContentSyllabus form" data-id="${id}" data-syl-id="${sylId}" data-state="0">
            </div>
        </div>
    `;
}

export const renderBtnNewSection = (vars = []) => {
    //console.log('renderNewSection', vars);
    return /*html*/ `
       <a class = "btn btnIconRight btnFull btnPeriodSyllabusSelect"
            data-name="${vars.name}" 
            data-id="${vars.id}" 
        >
            <span>${vars.name}</span> 
            <i class="icon icon-chevron-right"></i>
        </a>
   `;
}

export const renderBtnSelectPeriod = (vars = []) => {
    //console.log('renderBtnPeriod', vars);
    return /*html*/ `
        <div class="groupSelectPeriod" data-id="${vars.id}">
            <a class = "btn btnFull btnIconLeft btnSelectPeriod ui-state-default"
                data-name = "${vars.name}"
                data-id = "${vars.id}" >
                <i class="icon icon-gril-compac"></i>
                <input type="hidden" value="${vars.id}" name="inputIdPeriod[]">
                <span>${vars.name}</span>
            </a>
            <a class="btnCloseSelectPeriod" data-id="${vars.id}"><i class="icon icon-circle-close"></i></a>
        </div>
    `;
}

export const renderNewPeriodRow = (vars = []) => {
    //console.log('renderNewPeriodRow', vars);
    let state = vars.state ? vars.state : '0';
    let summary = vars.summary ? vars.summary : '';
    let id = vars.id ? vars.id : '';
    let sylId = vars.sylId ? vars.sylId : '';
    let name = vars.name ? vars.name : '';
    let index = parseFloat(vars.index) + 1;
    let inputItem = vars.inputItem ? vars.inputItem : '';
    return /*html*/ `
        <div class="item item-${index} form100w" data-index="${index}" data-id="${id}" data-syl-id="${sylId}">
            <div class = "formControlLandscape" >
                <div  class = "formControl form80w " >
                    <input class="formInput" type="hidden" name="inputItem"  value="${inputItem}">
                    <input class="formInput" type="hidden" name="inputOrder"  value="${index}">
                    <input class="formInput" type="text"   name="inputTitle" placeholder="Agrega un titulo" value="${name}">
                    <textarea class="formInput" placeholder="Agrega una descripción"  name="inputSummary" value="${summary}" rows="5"></textarea>
                </div>
                <div  class = "formControl form10w formActions" >
                    <a class="btn btnIcon btnSaveItemSyllabus" data-id="${id}" data-syl-id="${sylId}" data-order="${index}" >
                        <i class="icon icon-save"></i>
                    </a>
                    <a class="btn btnIcon btnDelete btnDeleteItemSyllabus" data-id="${id}" data-syl-id="${sylId}" data-index="${index}">
                        <i class="icon icon-trash"></i>
                    </a>
                </div>
            </div>
        </div>
   `;
}

export const renderListElements = (vars = []) => {
    //console.log('listElements', vars);
    return /*html*/ `
        <div class="modalHead">Nuevo Tema:</div>
        <form class="form formNoBottom formSlim" id="formElementSyllabus" >
            <div class="formControl" >
                <label>Titulo:</label>
                <input class="formInput" type="text" id="inputName" name="inputName">
            </div>
            <div class="formControl" >
                <label>Descripción:</label>
                <textarea class="formInput" id="inputSummary" name="inputSummary" row="10"></textarea>
            </div>
        </form>
    `;
}

export const renderElementItem = (vars = []) => {
    //console.log('elementItem', vars);
    return /*html*/ `
       <div class = "btn btnFull btnIconLeft btnSelectElement ui-state-default" data-id = "${vars.id}" >
            <div class="title">
                <i class="icon icon-gril-compac"></i>
                <input type="hidden" data-item="${vars.id}" data-name="${vars.name}" data-summary="${vars.summary}"  name="inputIdElement[]" />
                <span><b>${vars.name}</b> <br/> ${vars.summary} </span>
            </div>
            <div class="actions">
                <a class="btn btnIcon btnEditElement" data-id="${vars.id}" data-name="${vars.name}">
                    <i class="icon icon-pencil"></i>
                </a>
                <a class="btn btnIcon btnDeleteElement" data-id="${vars.id}" data-name="${vars.name}">
                    <i class="icon icon-trash"></i>
                </a>
            </div>
        </div>
   `;
}


export const renderElemBox = (vars = []) => {
    //console.log('renderElemBox', vars);
    return /*html*/ `<div class="boxElem  on" data-id="${vars.id}">
        <div class="boxElemHeader">
            <h3>${vars.name}</h3>
            <div class="accions">
                <a class="btn btnPrimary btnIconLeft btnSmall btnNewElement" data-fn="boxElem${capitalize(vars.module)}" data-id="${vars.id}">
                    <i class="icon icon-plus"></i>
                    <span>Nuevo Tema</span>
                </a>
            </div>
        </div>
        <div class= "boxElemBody" data-id = "${vars.id}" >
        </div>
    </div>`;
}