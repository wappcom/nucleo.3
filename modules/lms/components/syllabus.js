import {
    loadView,
    accessToken,
    alertMessageError,
    addHtml,
    replaceEssentials,
    dataModule,
    disableElem,
    unDisableElem,
    capitalize,
    changeBtn,
} from "../../components/functions.js";

import {
    setupTable,
    dataTable,
    removeItemTable
} from "../../components/tables.js";

import {
    removeModal,
    removeInnerForm,
    removeModalId
} from "../../components/modals.js";

import {
    renderNoDataTable
} from "../../components/renders/renderTables.js";

import {
    renderDeleteModal,
    innerForm,
    renderModal,
    renderModalConfirm
} from "../../components/renders/renderModals.js";


import {
    listCategorys,
    checkCategorys
} from "../../websites/components/categorys.js";

import {
    renderBtnMenuTop,
    renderNewPeriodRow,
    renderListElements,
    renderElementItem,
    renderElemBox,
    renderBtnNewSection,
    renderBtnSelectPeriod,
    renderBoxPeriordSyllabus
} from "./renderSyllabus.js";

import {
    errorInput,
    errorInputSelector
}
    from "../../components/forms.js";


let pathurl = "modules/lms/";
var module = "syllabus";
let system = "lms";
let tableId = "tableSyllabus";
let formId = "syllabusForm";

export const syllabusIndex = (vars = []) => {
    //console.log('syllabusIndex', vars, system);
    addHtml({
        selector: `.menuTopModule[system='${system}'] .menuTopRight`,
        type: 'insert',
        content: renderBtnMenuTop()
    }) //type: html, append, prepend, before, after

    loadView(_PATH_WEB_NUCLEO + "modules/lms/views/syllabus.html?" + _VS).then((responseView) => {
        //console.log('loadView', responseView);
        getData({
            task: 'getSyllabus',
            return: "returnArray"
        }).then((response) => {
            //console.log('getData', response, response.data);
            if (response.status == "success") {
                let data = response.data;
                let str = replaceEssentials({
                    str: responseView,
                    module,
                    icon: dataModule(module, 'icon'),
                    color: dataModule(module, 'color'),
                    name: dataModule(module, 'name'),
                    formId: "",
                    task: "",
                    fn: "add",
                    btnTask: "",
                    btnLabelAction: "Nuevo plan de Estudios",
                })
                if (data && data.length > 0) {
                    let table = setupTable({
                        tableId,
                        cols: [{
                            elem: "check",
                        }, {
                            elem: "id",
                            label: "Id",
                        }, {
                            elem: "name",
                            label: "Nombre",
                        }, {
                            elem: "state",
                            label: "Estado",
                        }, {
                            elem: "actions",
                            label: "Acciones",
                            btns: ["btnEdit", "btnDeleteFn"]
                        }],
                        data,
                    });

                    addHtml({
                        selector: ".bodyModule[module='" + module + "']",
                        type: 'insert',
                        content: str
                    }) //type: html, append, prepend, before, after

                    addHtml({
                        selector: ".bodyModule[module='" + module + "'] .tbody",
                        type: 'insert',
                        content: table
                    }) //type: html, append, prepend, before, after

                    dataTable({
                        elem: "#" + tableId,
                        orderCol: 0,
                    });

                } else if (data == 0) {
                    addHtml({
                        selector: ".bodyModule[module='" + module + "']",
                        type: 'insert',
                        content: str
                    });
                    addHtml({
                        selector: ".bodyModule[module='" + module + "'] .tbody",
                        type: 'insert',
                        content: renderNoDataTable()
                    }) //type: html, append, prepend, before, after
                } else {
                    alertMessageError({
                        message: response.message
                    });
                }
            } else {
                alertMessageError({
                    message: response.message
                });
            }

        }).catch(console.warn());
    }).catch(console.warn());
}

export const getFormAdd = (vars = []) => {
    console.log('getFormNew', vars);
    loadView(_PATH_WEB_NUCLEO + "modules/lms/views/formSyllabus.html?" + _VS).then((responseView) => {
        //console.log('loadView', responseView);
        let str = responseView;

        str = str.replace(/{{_CATEGORYS}}/g, listCategorys(_CATEGORYS));

        innerForm({
            module,
            body: replaceEssentials({
                str,
                module,
                title: "Nuevo plan de Estudios",
                formId: formId + 'Add',
                btnAction: "btnSaveSylllabus",
                btnNameAction: "Guardar"
            })
        });
    }).catch(console.warn());
}

export const getFormEdit = (vars = []) => {
    //console.log('getFormEdit', vars);
    let form = vars.form;
    let sylId = vars.id;
    getData({
        task: 'dataId',
        id: vars.id,
        return: "returnArray"
    }).then((response) => {
        console.log('getData getFormEdit', response);
        //console.log('getData getFormEdit', response.data, form);
        let data = response.data;
        let structure = data.structure;

        loadView(_PATH_WEB_NUCLEO + "modules/lms/views/formSyllabus.html?" + _VS).then((responseView) => {
            //console.log('loadView', responseView);
            let str = responseView;

            str = str.replace(/{{_CATEGORYS}}/g, listCategorys(_CATEGORYS));

            innerForm({
                module,
                body: replaceEssentials({
                    str,
                    module,
                    title: "Editar plan de Estudios",
                    formId: form,
                    btnAction: "btnUpdate",
                    btnNameAction: "Actualizar"
                })
            });

            $(`#${form} #inputId`).val(data.id);
            $(`#${form} #inputName`).val(data.name);
            $(`#${form} #inputSummary`).val(data.summary);

            checkCategorys(data.categorys);
            //console.log("structure", structure);
            if (structure != 0) {
                structure.forEach(element => {
                    addHtml({
                        selector: "#listElementsSyllabus",
                        type: 'prepend',
                        content: renderBoxPeriordSyllabus({
                            id: element.id,
                            name: element.name,
                            sylId
                        })
                    })
                    const items = element.content;
                    const numItems = items.length;

                    if (numItems > 0) {
                        addHtml({
                            selector: `.boxPeriodSyllabus[data-id='${element.id}'] .btnCollapse`,
                            type: 'append', // insert, append, prepend, replace
                            content: `<span class="numElem">${numItems}</span>`
                        })


                        items.forEach(function (elem, index) {
                            //console.log('items', numItems, index, elem);
                            addHtml({
                                selector: `.boxContentSyllabus[data-id='${element.id}'][data-syl-id='${sylId}'] `,
                                type: 'append',
                                content: renderNewPeriodRow({
                                    id: element.id,
                                    itemId: elem.id,
                                    name: elem.title,
                                    summary: elem.summary,
                                    index,
                                    sylId
                                })
                            })
                        });
                    }
                });
            }

            $("#listElementsSyllabus").sortable({
                connectWith: ".boxPeriodSyllabus .head"
            })

            $(".boxContentSyllabus").sortable({
                connectWith: ".box"
            })

            //console.log(_CATEGORYS)
        }).catch(console.warn());

    }).catch(console.warn());
}

export const periodSyllabusIndex = (vars = []) => {
    //console.log('periodSyllabusIndex', vars);
    getData({
        task: 'getPeriodSyllabus',
        return: 'returnArray'
    }).then((response) => {
        //console.log('getData', response, response.data);
        loadView(_PATH_WEB_NUCLEO + "modules/lms/views/period.html?" + _VS).then((responseView) => {
            let form = 'formPeriodSyllabus';
            let list = response.data.map((item, index) => { //console.log('item', item);    
                return renderNewPeriodRow({
                    id: item.id,
                    name: item.name,
                    state: item.state,
                    summary: item.summary ? item.summary : "",
                    index,
                    module,
                    form
                });
            });

            let str = responseView.replace("{{_LIST_SECTIONS}}", list.join(" "));

            let body = replaceEssentials({
                str,
                module,
                title: "Periodos",
                btnNameAction: "Nuevo Periodo",
                btnAction: "btnNewPeriodSyllabus",
                form
            });
            innerForm({
                id: form,
                module,
                body
            })
        }).catch(console.warn());
    }).catch(console.warn());
}

export const saveContentSyllabus = (vars = []) => {
    //console.log("saveContentSyllabus", vars, vars.data);
    let data = vars.data;
    const item = `.item[data-id='${data.id}'][data-syl-id="${data.sylId}"]`;
    const btnSaveItem = `.btnSaveItemSyllabus[data-id='${data.id}'][data-syl-id="${data.sylId}"][data-order="${data.order}"]`;
    let title = $(`${item} input[name='inputTitle']`).val();
    let inputId = $(`${item} input[name='inputItem']`).val();
    let itemId = inputId ? inputId : 0;
    let summary = $(`${item} textarea[name='inputSummary']`).val();
    let count = 0;

    //console.log("save title/summary", title, summary);

    if (title == "") {
        errorInputSelector(`${item} input[name='inputTitle']`, 'El campo no puede estar vacio');
        count++;
    }
    if (summary == "") {
        errorInputSelector(`${item} textarea[name='inputSummary']`, 'El campo no puede estar vacio');
        count++;
    }

    if (count == 0) {
        let inputs = {
            inputId: itemId,
            inputTitle: title,
            inputSummary: summary
        }

        getData({
            task: 'updateItem',
            return: 'returnArray', // returnId, returnState, returnArray
            data,
            inputs
        }).then((response) => {
            //console.log('addItem', response);
            if (response.status == 'success') {
                $(`${btnSaveItem} i`).attr('class', 'icon icon-check');
                $(`${btnSaveItem}`).addClass('btnSuccess animated flash');

                setTimeout(() => {
                    $(`${btnSaveItem} i`).attr('class', 'icon icon-save');
                    $(`${btnSaveItem}`).removeClass('btnSuccess animated flash');
                    $(`${item} input[name="inputItem"]`).val(response.data);
                }, 1000);
            } else {
                alertMessageError({
                    message: response.message
                })
            }
        }).catch(console.warn());
    }
}

//asycn function
export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/lms/controllers/apis/v1/syllabus.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        //console.log("Error: getDataPlaces")
        //console.log(error)
    }
}


document.addEventListener("DOMContentLoaded", function () {

    $("body").on("click", `.btnAddSylllabus`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data()
        console.log(`.btnAddSylllabus`, data);

        getFormAdd();
    })

    $("body").on("click", `.btnState[data-for='${tableId}']`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let id = $(this).attr("data-id");
        let state = $(this).attr("state") == "1" ? "0" : "1";
        //console.log(`.btnState[data-for='${tableId}']`, );
        $(this).attr("state", state);

        getData({
            task: 'changeState',
            return: 'returnState',
            id,
            state
        }).then((response) => {
            //console.log('getData', response);
            if (response.status != "success") {
                alertMessageError({
                    message: response.message
                });
            }
        }).catch(console.warn());

    });

    $("body").on("click", `.btnDeleteFn[data-fn='deleteTableSyllabus']`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let id = $(this).attr("data-id");
        let name = $(this).attr("data-name");

        //console.log(`.btnDelete`, );

        addHtml({
            selector: ".bodyModule[module='" + module + "']",
            type: 'prepend',
            content: renderDeleteModal({
                name,
                id: "modalDelete",
                cls: `btnDeleteSyllabusConfirm`,
                attr: `data-id="${id}"`,
            })
        }) //type: html, append, prepend, before, after

    });

    $("body").on("click", `.btnDeleteSyllabusConfirm`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let id = $(this).attr("data-id");
        //console.log(`.btnDeleteConfirmation[data-fn='delete${tableId}']`, );

        getData({
            task: 'delete',
            return: "returnState",
            id
        }).then((response) => {
            //console.log('delete', response);
            if (response.status == 'success') {
                removeItemTable(id, tableId);
                removeModalId("modalDelete");
            } else {
                alertMessageError({
                    message: response.message
                });
            }
        }).catch(console.warn());

    })

    $("body").on("click", `.btnEdit[data-for='${tableId}']`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let id = $(this).attr("data-id");
        let form = $(this).attr("data-for");
        //console.log(`.btnEdit`, );

        getFormEdit({
            id,
            form
        });

    })

    $("body").on("click", `.btnPeriodSyllabus`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        periodSyllabusIndex()

        //console.log(`.btnPeriodSyllabus`, );

    });

    $("body").on("click", `.btnSaveNewPeriodSyllabus`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let form = $(this).attr("data-form");
        let name = $(`#${form} #inputName`).val();
        let summary = $("#summaryPeriod").val();
        let count = 0;
        //console.log(`.btnSaveNewPeriodSyllabus`, name, summary);

        if (name == "") {
            count++;
            errorInput("inputName", "Ingrese el nombre del periodo");
        }

        if (count == 0) {
            //disableElem(".btnSaveNewPeriodSyllabus");
            getData({
                task: 'saveNewPeriod',
                return: "returnId",
                name,
                summary
            }).then((response) => {
                //console.log('getData', response);
                if (response.status == 'success') {
                    addHtml({
                        selector: `#formPeriodSyllabus div[data-form='${form}']`,
                        type: 'prepend',
                        content: renderNewPeriodRow({
                            id: response.data,
                            name,
                            summary
                        })
                    }) //type: html, append, prepend, before, after
                } else {
                    alertMessageError({
                        message: response.message
                    });
                }

            }).catch(console.warn());
        }
    })

    $("body").on("click", `.btnNewSection`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        //console.log(`.btnNewSection`, );

        getData({
            task: 'getPeriodSyllabus',
            return: "returnArray"
        }).then((response) => {
            //console.log('getData', response, response.data);
            if (response.status == "success") {
                let data = response.data;

                let body = data.map((item, index) => {
                    return renderBtnNewSection({
                        id: item.id,
                        name: item.name,
                    })
                })

                addHtml({
                    selector: ".bodyModule[module='" + module + "'] ",
                    type: 'prepend',
                    content: renderModal({
                        id: "modalNewSection",
                        attr: `data-fn="newSection"`,
                        body: `<label>Lista de Periodios</label>` + body.join(" ")
                    })
                }) //type: html, append, prepend, before, after
            }
        }).catch(console.warn());
    })

    $("body").on("click", `.btnPeriodSyllabusSelect`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`.btnPeriodSyllabusSelect`, );

        addHtml({
            selector: "#listElementsSyllabus",
            type: 'prepend',
            content: renderBoxPeriordSyllabus({
                id: data.id,
                name: data.name,
            })
        }) //type: html, append, prepend, before, after

        removeModalId("modalNewSection");
    });

    $("body").on("click", `.btnAddContentSyllabus`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let numElem = 0;
        //console.log(`.btnAddContentSyllabus`, data, data.sylId);


        $(`.boxContentSyllabus[data-id='${data.id}'][data-syl-id='${data.sylId}'] .item`).each(function (index, item) {
            //console.log(index + 1);
            numElem = index + 1;
        });

        addHtml({
            selector: `.boxContentSyllabus[data-id='${data.id}'][data-syl-id='${data.sylId}']`,
            type: 'prepend', // insert, append, prepend, replace
            content: renderNewPeriodRow({
                id: data.id,
                sylId: data.sylId,
                index: numElem
            })
        })

        $(`.btnCollapse[data-id='${data.id}'][data-syl-id='${data.sylId}']`).html(`<i class="icon icon-arrow-o-du"></i><span class="numElem">${numElem + 1}</span>`);
    })

    $("body").on("click", `.btnDeleteContentSyllabus`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data()
        //console.log(`.btnDeleteContentSyllabus`, data);
        let periodId = data.id;
        let sylId = data.sylId;
        let name = data.name;

        addHtml({
            selector: ".bodyModule[module='" + module + "']",
            type: 'prepend',
            content: renderDeleteModal({
                name,
                id: "modalDelete",
                cls: "deletePeriodContentConfirm",
                attr: `data-id="${periodId}" data-syl-id="${sylId}"`,
            })
        }) //type: html, append, prepend, before, after

    })

    $("body").on("click", `.deletePeriodContentConfirm`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data()
        console.log(`.deletePeriodContentConfirm`, data);
        getData({
            task: 'deletePeriodContent',
            return: 'returnState', // returnId, returnState, returnArray
            data
        }).then((response) => {
            console.log('getData deletePeriodContent', response);
            if (response.status == 'success') {
                removeModalId("modalDelete");
                $(`#boxPeriodSyllabus [data-id="${data.id}"]`).remove();
            } else {
                alertMessageError({
                    message: response.message
                })
            }
        }).catch(console.warn());
    })

    $("body").on("click", `.btnSelectPeriod`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let countElem = 0;

        $("#listElementsSyllabus .boxElem[data-id='" + data.id + "']").each(function (index, item) {
            countElem++;
        });

        //console.log(`.btnSelectPeriod`, countElem, data);

        $(".boxElem").removeClass("on");

        if (countElem == 0) {

            addHtml({
                selector: "#listElementsSyllabus",
                type: 'prepend',
                content: renderElemBox({
                    id: data.id,
                    name: data.name,
                    module
                })
            }) //type: html, append, prepend, before, after

        } else {
            $(`.boxElem[data-id='${data.id}']`).addClass("on");
        }


        /* addHtml({
            selector: ".bodyModule[module='" + module + "']",
            type: 'prepend',
            content: renderModalConfirm({
                id: "modalSelectPeriod",
                classBtnAction: "btnPrimary btnConfirmNewElement",
                labelBtnAction: "Agregar",
                attr: `data-fn="selectPeriod" data-id="${data.id}"`,
                body: renderListElements({
                    id: data.id
                })
            })
        }) //type: html, append, prepend, before, after */

    })

    $("body").on("click", `.btnConfirmNewElement[data-fn='selectPeriod']`, function (e) {
        e.preventDefault();

        let data = $(this).data();
        //console.log(`.btnConfirmNewElement`, data);
        let name = $(`#formElementSyllabus #inputName`).val();
        let summary = $(`#formElementSyllabus #inputSummary`).val();
        let count = 0;

        if (name == "") {
            count++;
            $("#formElementSyllabus #inputName").addClass("error");
            $("#formElementSyllabus #inputName").attr("placeholder", "Ingrese el nombre del tema");
        }

        if (count == 0) {
            removeModal();
            addHtml({
                selector: `#listElementsSyllabus .boxElem[data-id='${data.id}'] .boxElemBody`,
                type: 'prepend',
                content: renderElementItem({
                    id: data.id,
                    name,
                    summary
                })
            }) //type: html, append, prepend, before, after
            $(`#listElementsSyllabus .boxElem[data-id='${data.id}'] .boxElemBody`).sortable({
                connectWith: ".connectedSortable"
            })
        }
    });

    $("body").on("click", `.btnNewElement[data-fn='boxElemSyllabus']`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`.btnNewElement[data-fn='boxElemSyllabus']`, );

        addHtml({
            selector: ".bodyModule[module='" + module + "']",
            type: 'prepend',
            content: renderModalConfirm({
                id: "modalSelectPeriod",
                classBtnAction: "btnPrimary btnConfirmNewElement",
                labelBtnAction: "Agregar",
                attr: `data-fn="selectPeriod" data-id="${data.id}"`,
                body: renderListElements({
                    id: data.id
                })
            })
        }) //type: html, append, prepend, before, after */



    });

    $("body").on("click", `.btnCloseSelectPeriod`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        //console.log(`.btnCloseSelectPeriod`, );

    });

    $("body").on("click", `.btnCollapse`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        let data = $(this).data();
        let status = data.status == 0 ? 1 : 0;
        let numElem = 0;

        //console.log(`.btnCollapse 2`, data, status);

        $(this).data('status', status);

        $(`.boxContentSyllabus[data-id='${data.id}'][data-syl-id='${data.sylId}'] .item`).each(function (index, item) {
            //console.log(index + 1);
            numElem = index + 1;
        });
        if (numElem == 0) {
            $(".numElem", this).remove();
        } else {
            $(this).html(`<i class="icon icon-arrow-o-du"></i><span class="numElem">${numElem}</span>`);
        }
        $(`.boxContentSyllabus[data-id='${data.id}'][data-syl-id='${data.sylId}']`).attr("data-state", status);
    })

    $("body").on("click", `.btnDeleteItemSyllabus`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let inputId = $(`.item[data-id='${data.id}'][data-syl-id='${data.sylId}']`).val();
        let numElem = $(`.btnCollapse[data-id='${data.id}'][data-syl-id='${data.sylId}'] .numElem`).html();
        let num = parseInt(numElem) - 1;

        //console.log(`.btnDeleteItemSyllabus`, data, numElem, inputId);
        $(`.item[data-index='${data.index}']`).remove();

        if (inputId != "") {
            //console.log("call fn delete item")
        }

        if (num == 0) {
            $(`.btnCollapse[data-id='${data.id}'][data-syl-id='${data.sylId}'] .numElem`).remove();
            $(`.btnCollapse[data-id='${data.id}'][data-syl-id='${data.sylId}']`).html(`<i class="icon icon-arrow-o-du"></i>`);
        } else {
            $(`.btnCollapse[data-id='${data.id}'][data-syl-id='${data.sylId}'] .numElem`).html(num);
        }

    })

    $("body").on("click", `.btnSaveItemSyllabus`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`btnSaveItemSyllabus`, data);

        saveContentSyllabus({
            data
        });



    })

    $("body").on("click", `.btnSaveSylllabus`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let form = "syllabusFormAdd";
        let cont = 0;
        let inputName = $(`#${form} #inputName`).val();
        let inputSummary = $(`#${form} #inputSummary`).val();
        let inputState = $(`#${form} #inputState`).val();
        console.log(`.btnSaveSylllabus`, data);

        changeBtn({
            id: "btnUpdate",
            type: "save"
        });

        let categorys = [];
        let countCategorys = 0;
        $("#" + form + " input[name='inputCategorys[]']").each(function (e) {
            let valorCategorys = $(this).val();
            let checkCategorys = $(this).prop("checked");
            if (checkCategorys) {
                categorys[countCategorys] = valorCategorys;
                countCategorys++;
            }
        });

        if (inputName == "") {
            errorInput("inputName", "Ingrese el nombre del Plan de estudios");
            cont++;
        }
        if (cont == 0) {

            getData({
                task: 'create',
                return: 'returnId', // returnId, returnState, returnArray
                inputs: {
                    inputName,
                    inputSummary,
                    inputState,
                    categorys
                }
            }).then((response) => {
                console.log('getData save', response);
                if (response.status == 'success') {
                    removeInnerForm();
                    syllabusIndex();
                } else {
                    alertMessageError({
                        message: response.message
                    })
                }
            }).catch(console.warn());
        }
    })

    $("body").on("click", `.btnUpdate`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let cont = 0;
        let inputName = $("#" + tableId + " #inputName").val();
        //console.log(`.btnUpdate`, data, tableId, inputName);

        changeBtn({
            id: "btnUpdate",
            type: "loading"
        });

        let categorys = [];
        let countCategorys = 0;
        $("#" + tableId + " input[name='inputCategorys[]']").each(function (e) {
            let valorCategorys = $(this).val();
            let checkCategorys = $(this).prop("checked");
            if (checkCategorys) {
                categorys[countCategorys] = valorCategorys;
                countCategorys++;
            }
        });


        let periodSyllabus = [];
        let countSyllabus = 0;
        $("#" + tableId + " .boxPeriodSyllabus").each(function (e) {
            let id = $(this).data("id");
            periodSyllabus[countSyllabus] = id;
            countSyllabus++;
        });

        if (inputName == "") {
            errorInput("#inputName", "Ingrese el nombre del Plan de estudios");
            cont++;
        }
        if (cont == 0) {
            getData({
                task: 'update',
                return: 'returnState', // returnId, returnState, returnArray
                inputs: {
                    inputId: $("#" + tableId + " #inputId").val(),
                    inputName,
                    inputSummary: $("#" + tableId + " #inputSummary").val(),
                    inputState: $("#" + tableId + " #inputState").val(),
                    categorys,
                    periodSyllabus
                }
            }).then((response) => {
                console.log('update', response);
                if (response.status == 'success') {
                    changeBtn({
                        id: "btnUpdate",
                        type: "success"
                    });
                    setTimeout(() => {
                        removeInnerForm();
                    }, 1000);
                } else {
                    alertMessageError({
                        message: response.message
                    })
                }
            }).catch(console.warn());

        }
    })

}) // end DOMContentLoaded