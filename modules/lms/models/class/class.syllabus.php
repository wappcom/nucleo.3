<?php
class SYLLABUS
{
    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function dataId(array $var = null){
        //return $var;
        $vars = $var["vars"];
        $id = $vars["id"];
        $sql = "SELECT * FROM mod_syllabus WHERE mod_syl_id = '".$id." '";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){

            $row=$this->fmt->querys->row($rs);
            $id = $row["mod_syl_id"];
            $return["id"] = $id;
            $return["name"] = $row["mod_syl_name"];
            $return["summary"] = $row["mod_syl_summary"];
            $return["categorys"] = $this->getRelationsCategorys($id);
            $return["structure"] = $this->structureId($id);
            $return["state"] = $row["mod_syl_state"];
 
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function structureId(int $id = null){
        //return $var;
        $sql = "SELECT DISTINCT * FROM mod_syllabus_relations_periods,mod_syllabus_periods WHERE mod_syl_rel_per_syl_id = '${id}' AND mod_syl_rel_per_per_id=mod_syl_per_id ORDER BY mod_syl_rel_per_order DESC";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $perId = $row["mod_syl_rel_per_per_id"];
                $return[$i]["id"] = $perId;
                $return[$i]["name"] = $row["mod_syl_per_name"];
                $return[$i]["summary"] = $row["mod_syl_per_summary"];
                $return[$i]["state"] = $row["mod_syl_per_state"];
                $return[$i]["order"] = $i;
                $return[$i]["content"] = $this->getContent(array("perId"=>$perId, "id"=>$id));
            }
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function create(array $var = null){
        //return $var;
        $entitieId = $var["entitieId"];
        $inputs = $var["vars"]["inputs"];
        $inputName = $inputs["inputName"];
        $inputSummary = $inputs["inputSummary"];
        $inputState = $inputs["inputState"];
        $categorys = $inputs["categorys"];

        $insert = "mod_syl_name,mod_syl_summary,mod_syl_ent_id,mod_syl_state";
        $values ="'" .$inputName. "','".$inputSummary."','".$entitieId."','".$inputState."'";
        $sql= "insert into mod_syllabus (".$insert.") values (".$values.")";
        $this->fmt->querys->consult($sql,__METHOD__);

        $sql = "select max(mod_syl_id) as id from mod_syllabus";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);    
        $id = $row["id"];

        $insert = 'mod_syl_cat_syl_id,mod_syl_cat_cat_id,mod_syl_cat_order';
        $countCategorys = count($categorys);
        for ($i = 0; $i < $countCategorys; $i++) {
            $values = "'" . $id  . "','" . $categorys[$i] . "','" . $i . "'";
            $sql = "insert into mod_syllabus_categorys (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        return $id;

    }

    public function update(array $var = null){
        //return $var;
        $entitieId = $var["entitieId"];
        $inputs = $var["vars"]["inputs"];
        $categorys = $inputs["categorys"];
        $periodSyllabus =$inputs["periodSyllabus"];
        $id = $inputs["inputId"];

        $sql = "UPDATE mod_syllabus SET 
        mod_syl_name = '".$inputs["inputName"]."', 
        mod_syl_summary = '".$inputs["inputSummary"]."', 
        mod_syl_state = '".$inputs["inputState"]."' 
        WHERE mod_syl_id = '".$id."'";

        $this->fmt->querys->consult($sql);

        $this->fmt->modules->deleteRelation(array("from" => "mod_syllabus_categorys", "column" => "mod_syl_cat_syl_id", "item" => $id));

        $insert = 'mod_syl_cat_syl_id,mod_syl_cat_cat_id,mod_syl_cat_order';
        $countCategorys = count($categorys);
        for ($i = 0; $i < $countCategorys; $i++) {
            $values = "'" . $id  . "','" . $categorys[$i] . "','" . $i . "'";
            $sql = "insert into mod_syllabus_categorys (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        $this->fmt->modules->deleteRelation(array("from" => "mod_syllabus_relations_periods", "column" => "mod_syl_rel_per_syl_id", "item" => $id));

        $insert = 'mod_syl_rel_per_syl_id,mod_syl_rel_per_per_id,mod_syl_rel_per_order';
        $countPeriods = count($periodSyllabus);
        for ($i = 0; $i < $countPeriods; $i++) {
            $values = "'" . $id  . "','" . $periodSyllabus[$i] . "','" . $i . "'";
            $sql = "insert into mod_syllabus_relations_periods (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        } 


        return 1;

    }

    public function getContent(array $var = null){
        //return $var;
        $id = $var["id"];
        $perId = $var["perId"];
        $state  =  $var["state"] ? $var["state"] : "mod_syl_cont_state >= '0'";
        $sql = "SELECT * FROM mod_syllabus_contents WHERE mod_syl_cont_per_id = '${perId}' AND mod_syl_cont_syl_id = '${id}' AND ${state} ORDER BY mod_syl_cont_order DESC";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $id = $row["mod_syl_cont_id"];
                $return[$i]["id"] = $id;
                $return[$i]["title"] = $row["mod_syl_cont_title"];
                $return[$i]["summary"] = $row["mod_syl_cont_summary"];
                $return[$i]["state"] = $row["mod_syl_cont_state"];
            }
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);

        
    }

    public function updateItem(array $var = null){
        //return $var;
        $vars = $var["vars"];
        $data = $vars["data"];
        $inputs = $vars["inputs"];

        $inputId = $inputs["inputId"];
        $inputTitle = $inputs["inputTitle"];
        $inputSummary = $inputs["inputSummary"];
        $inputOrder = $data["order"];

        if ($inputId == 0) {
            //create new
            $array = array_merge($inputs,$data);
            return $this->addContent(array("vars"=>$array));
        }else{
            //update
          $sql = "UPDATE mod_syllabus_contents SET
                  mod_syl_cont_title='" . $inputTitle. "',
                  mod_syl_cont_summary='" . $inputSummary. "',
                  mod_syl_cont_order='" . $inputOrder. "'
                  WHERE mod_syl_cont_id= '" . $inputId . "' ";
          $this->fmt->querys->consult($sql);

          return $inputId;
        }
    }

    public function addContent(array $var = null){
        //return $var;
        $vars = $var["vars"];
        $inputId = $vars["inputId"];
        $perId = $vars["id"];
        $inputTitle = $vars["inputTitle"];
        $inputSummary = $vars["inputSummary"];
        $inputOrder = $vars["order"];
        $inputSylId = $vars["sylId"];


        $insert = "mod_syl_cont_title,mod_syl_cont_summary,mod_syl_cont_per_id,mod_syl_cont_syl_id,mod_syl_cont_order,mod_syl_cont_state";
        $values ="'" . $inputTitle . "','". $inputSummary . "','". $perId . "','". $inputSylId . "','". $inputOrder . "','1'";
        $sql= "insert into mod_syllabus_contents (".$insert.") values (".$values.")";
        $this->fmt->querys->consult($sql,__METHOD__);

        $sql = "select max(mod_syl_cont_id) as id from mod_syllabus_contents";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        return $row["id"];
    }
    
    public function getSyllabus(array $var = null){
        //return $var;
        $entitieId = $var['entitieId'];
        $sql = "SELECT * FROM mod_syllabus WHERE mod_syl_ent_id = '".$entitieId."' ORDER BY mod_syl_id DESC";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $id = $row["mod_syl_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_syl_name"];
                $return[$i]["summary"] = $row["mod_syl_summary"];
                $return[$i]["state"] = $row["mod_syl_state"];
            }
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function getPeriodSyllabus(array $var = null){
        //return $var;

        $entitieId = $var['entitieId'];
        $vars = $var["vars"];

        $sql = "SELECT * FROM mod_syllabus_periods WHERE mod_syl_per_ent_id = '".$var['entitieId']."' ORDER BY mod_syl_per_id DESC";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $id = $row["mod_syl_per_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_syl_per_name"];
                $return[$i]["summary"] = $row["mod_syl_per_summary"];
                $return[$i]["state"] = $row["mod_syl_per_state"];
                
            }
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
        
    }

    public function changeState(array $var = null){
        //return $var;
        $vars = $var["vars"];
        $id = $vars["id"];
        $state = $vars["state"];
        
        $sql = "UPDATE mod_syllabus SET
              mod_syl_state='" . $state. "'
              WHERE mod_syl_id = '" . $id . "' ";
        $this->fmt->querys->consult($sql);
        
        return 1;
    }

    public function delete(array $var = null){
        //return $var;
        
        $vars = $var["vars"];
        $id = $vars["id"];
        
        $sql="DELETE FROM mod_syllabus WHERE mod_syl_id='".$id."'";
        $this->fmt->querys->consult($sql);
        $up_sqr6 = "ALTER TABLE mod_syllabus AUTO_INCREMENT=1";
        $this->fmt->querys->consult($up_sqr6,__METHOD__);

        $this->fmt->modules->deleteRelation(array("from" => "mod_syllabus_categorys", "column" => "mod_syl_cat_syl_id", "item" => $id));

        $this->fmt->modules->deleteRelation(array("from" => "mod_syllabus_relations_periods", "column" => "mod_syl_rel_per_syl_id", "item" => $id));

        $sql="DELETE FROM mod_syllabus_contents WHERE  mod_syl_cont_syl_id='".$id."'";
        $this->fmt->querys->consult($sql);
        $up_sqr6 = "ALTER TABLE mod_syllabus_contents AUTO_INCREMENT=1";
        $this->fmt->querys->consult($up_sqr6,__METHOD__);

        return 1;
    }

    public function deletePeriodContent(array $var = null){
        //return $var;

        $perId = $var['vars']['data']['id'];
        $sylId = $var['vars']['data']['sylId'];
        
        $sql="DELETE FROM mod_syllabus_relations_periods WHERE mod_syl_rel_per_id='".$perId."' AND mod_syl_rel_per_syl_id='".$sylId."'";
        $this->fmt->querys->consult($sql);

        $sql="DELETE FROM mod_syllabus_contents WHERE mod_syl_cont_per_id='".$perId."' AND mod_syl_cont_syl_id='".$sylId."'";
        $this->fmt->querys->consult($sql);
        $up_sqr6 = "ALTER TABLE mod_syllabus_contents AUTO_INCREMENT=1";
        $this->fmt->querys->consult($up_sqr6,__METHOD__);

        return 1;
    }

    public function saveNewPeriod(array $var = null){
        //return $var;
        $entitieId = $var['entitieId'];
        $vars = $var["vars"];
         
        $name = $vars["name"];
        $summary = $vars["summary"];

        $insert = "mod_syl_per_name,mod_syl_per_summary,mod_syl_per_ent_id,mod_syl_per_state";
        $values ="'" . $name . "','". $summary . "','" . $entitieId . "','1'";
        $sql= "insert into mod_syllabus_periods (".$insert.") values (".$values.")";
        $this->fmt->querys->consult($sql,__METHOD__);

        $sql = "select max(mod_syl_per_id) as id from mod_syllabus_periods";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        return $row["id"];
        
    }

     public function getRelationsPeriods(int $id = null) {
        //return $var;
        $sql = "SELECT mod_syl_per_id,mod_syl_per_name FROM mod_syllabus_relations_periods,mod_syllabus, mod_syllabus_periods WHERE mod_syl_rel_per_syl_id = '".$id."' AND mod_syl_rel_per_per_id=mod_syl_per_id ORDER BY mod_syl_rel_per_order DESC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $catId = $row["mod_syl_per_id"];
                $return[$i]["id"] = $catId;
                $return[$i]["name"] = $row["mod_syl_per_name"];
            }
            return $return;
        } else {
            return 0;
        }
    }     
 

    public function getRelationsCategorys(int $id = null) {
        //return $var;
        $sql = "SELECT * FROM mod_syllabus_categorys,categorys WHERE mod_syl_cat_syl_id = '".$id."' AND mod_syl_cat_cat_id=cat_id ORDER BY mod_syl_cat_order DESC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $catId = $row["mod_syl_cat_cat_id"];
                $return[$i]["id"] = $catId;
                $return[$i]["name"] = $row["cat_name"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function getRelationsSyllabusCategorys(int $catId = null) {
        //return $var;
        $sql = "SELECT DISTINCT mod_syl_id, mod_syl_name , mod_syl_summary FROM mod_syllabus_categorys,categorys,mod_syllabus WHERE mod_syl_cat_cat_id = '".$catId."' AND mod_syl_cat_syl_id=mod_syl_id ORDER BY mod_syl_cat_order DESC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $catId = $row["mod_syl_id"];
                $return[$i]["id"] = $catId;
                $return[$i]["name"] = $row["mod_syl_name"];
                $return[$i]["summary"] = $row["mod_syl_sumary"];
            }
            return $return;
        } else {
            return 0;
        }
    }

}