<?php
header('Content-Type: text/html; charset=utf-8');
class CALENDARADVISED
{

    var $fmt;
    var $accounts;
    var $kardex;
    var $idModAdvised = 501;
    var $idService = 1;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
        require_once(_PATH_NUCLEO . "modules/accounts/models/class/class.accounts.php");
        $this->accounts = new ACCOUNTS($fmt);
        require_once(_PATH_NUCLEO . "modules/rrhh/models/class/class.kardex.php");
        $this->kardex = new KARDEX($fmt);
    }

    public function dataId($id = null)
    {
        $sql = "SELECT * FROM mod_advised_calendars WHERE mod_adv_cal_id = '" . $id . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_adv_cal_id"];
            $return["id"] = $id;
            $return["acuId"] = $row["mod_adv_cal_acu_id"];
            $return["couId"] = $row["mod_adv_cal_cou_id"];
            $return["type"] = $row["mod_adv_cal_type"];
            $return["dateStart"] = $row["mod_adv_cal_date_start"];
            $return["dateEnd"] = $row["mod_adv_cal_date_end"];
            $return["title"] = $row["mod_adv_cal_title"];
            $return["details"] = $row["mod_adv_cal_details"];
            $return["place"] = $row["mod_adv_cal_place"];
            $return["reasonCancel"] = $row["mod_adv_cal_reason_cancel"];
            $return["reschedule"] = $row["mod_adv_cal_reschedule"];
            $return["entId"] = $row["mod_adv_cal_ent_id"];
            $return["userId"] = $row["mod_adv_cal_user_id"];
            $return["state"] = $row["mod_adv_cal_state"];
            return $return;
        } else {
            return 0;
        }
        
    }

    public function addSchedule(array $var = null)
    {
        //return $var;
        $userId = $var["userId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];

        $couId = $vars["inputCouId"];
        $acuId = $vars["inputAccount"];
        $acuLabel = $vars["inputAccountLabel"];
        $hourStart = $vars["inputHourStart"];
        $hourEnd = $vars["inputHourEnd"];

        $dateStart = $vars["inputDateStart"];
        $dateEnd = $vars["inputDateEnd"];
        $place = $vars["inputPlace"];
        $others = $vars["inputOthers"];
        $comments = $vars["inputComments"];
        $typeAppointment = $vars["inputTypeAppointment"] ?? 'appointment';
        $flag = $vars["flag"] ?? '';

        if ($dateEnd == "") {
            $dateEnd = $dateStart;
        } 

        $start = $dateStart . " " . $hourStart;
        $end = $dateEnd . " " . $hourEnd;

        //return $typeAppointment." * ". $start." / ".$end;

        if (!empty($others)) {
            $place = $others;
        }

        if ($start == "" || $end == "" || $start=="0000-00-00 00:00:00" || $end=="0000-00-00 00:00:00") {
            $rtn["Error"] = 1;
            $rtn["status"] = 'error';
            $rtn["message"] = "La fecha no esta definida, revisar fecha por favor, intenta denuevo";
            return $rtn;
        }

        $responseDaysBlocked = $this->verifyDaysBlocked(array(
            "entitieId" => $entitieId,
            "dateStart" => $start,
            "dateEnd" => $end,
            "couId" => $couId,
            "acuId" => $acuId
        ));

        if ($responseDaysBlocked != 0) {
            $rtn["Error"] = 1;
            $rtn["message"] = "La fecha seleccionada no se encuentra disponible, intenta denuevo";
            return $rtn;
        }

        $responseVerify = $this->verifyDate(array(
            "entitieId" => $entitieId,
            "dateStart" => $start,
            "dateEnd" => $end,
            "couId" => $couId,
            "acuId" => $acuId
        ));

        //return ['responseVerify' => $responseVerify, 'Error' => '1', 'status' => 'error', 'start' => $start, 'end' => $end];

        if ($responseVerify != 0) {
            $rtn["Error"] = 1;
            $message = "";
            for ($i = 0; $i < count($responseVerify); $i++) {
                $message .= $responseVerify[$i]['title'] . " - " . $responseVerify[$i]['dateStart'] . " / " . $responseVerify[$i]['dateEnd'] . "\n";
            }

            $rtn["message"] = "Ya existe una cita en ese horario, intenta denuevo - ID ". "\n"."\n" .$message;
            $rtn["send"] = $responseVerify;
            return $rtn;
        }

        $responseAvailability = $this->verifyAvailability(array(
            "entitieId" => $entitieId,
            "dateStart" => $start,
            "dateEnd" => $end,
            "couId" => $couId,
            "acuId" => $acuId
        ));

        if ($responseAvailability != 0) {
            $rtn["Error"] = 1;
            $rtn["message"] = "La fecha no coincide con la disponibilidad del Consejero en horas: " . $responseAvailability . ", intenta denuevo";
            return $rtn;
        }

        $today = $this->fmt->modules->dateFormat();

        //Aqui validar horario 
        $insert = "mod_adv_cal_acu_id,
                mod_adv_cal_cou_id,
                mod_adv_cal_type,
                mod_adv_cal_date_start,
                mod_adv_cal_date_end,
                mod_adv_cal_title,
                mod_adv_cal_details,
                mod_adv_cal_place,
                mod_adv_cal_flags,
                mod_adv_cal_user_id,
                mod_adv_cal_date_register,
                mod_adv_cal_ent_id,
                mod_adv_cal_state";
        $values = "'" . $acuId . "','" .
            $couId . "','" .
            $typeAppointment . "','" .
            $start . "','" .
            $end . "','Cita " .
            $acuLabel . "','" .
            $comments . "','" .
            $place . "','" .
            $flag . "','" .
            $userId . "','" .
            $today. "','" .
            $entitieId . "','1'";

        $sql = "insert into mod_advised_calendars (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_adv_cal_id) as id from mod_advised_calendars";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $id  = $row["id"];

        $rtn["Error"] = 0;
        
        $rtn["status"] = 'success';
        $rtn["data"]["calId"] = $id;
        $rtn["data"]["dataCalId"] = $this->dataId($id);
        $rtn["data"]["couId"] = $couId;
        $rtn["data"]["counselorName"] = $this->fmt->users->fullName($couId);
        //$rtn["data"]["date"] = $dateStart;
        $rtn["data"]["dateStart"] = $dateStart;
        $rtn["data"]["dateEnd"] = $dateEnd;
        $rtn["data"]["hourStart"] = $hourStart;
        $rtn["data"]["typeAppointment"] = $typeAppointment;
        $rtn["data"]["hourEnd"] = $hourEnd;
        $rtn["data"]["flag"] = $flag;
        $rtn["data"]["accountDataId"] = $this->accounts->dataAccount($acuId);
        $rtn["data"]["linkCelular"] = $this->fmt->options->getValue("link_celular");

        return $rtn;
    }

    public function verifyDate(array $var = null)
    {
        //return $var;

        $dateStart = $var["dateStart"];
        $dateEnd = $var["dateEnd"];
        $couId = $var["couId"];
        $acuId = $var["acuId"];

       /*$sql = "SELECT mod_adv_cal_id 
                FROM mod_advised_calendars 
                WHERE mod_adv_cal_cou_id='" . $couId . "' AND  
                mod_adv_cal_state='1' AND 
                (mod_adv_cal_date_start between '" . $dateStart . "' and '" . $dateEnd . "' AND mod_adv_cal_date_end between '" . $dateStart . "' and '" . $dateEnd . "')";*/

        
        $sql = "SELECT mod_adv_cal_id,mod_adv_cal_title,mod_adv_cal_date_start,mod_adv_cal_date_end,mod_adv_cal_state
        FROM mod_advised_calendars
        WHERE mod_adv_cal_state = '1'
          AND mod_adv_cal_cou_id = '" . $couId . "'
          AND (
            ('" . $dateStart  . "' BETWEEN mod_adv_cal_date_start AND mod_adv_cal_date_end)
            OR ('" . $dateEnd . "' BETWEEN mod_adv_cal_date_start AND mod_adv_cal_date_end)
            OR (mod_adv_cal_date_start BETWEEN '" . $dateStart  . "' AND '" . $dateEnd . "')
            OR (mod_adv_cal_date_end BETWEEN '" . $dateStart  . "' AND '" . $dateEnd . "')
          );
        ";

        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i=0; $i < $num; $i++) { 
                $row = $this->fmt->querys->row($rs);
                $rtn [$i]["id"] = $row["mod_adv_cal_id"];
                $rtn [$i]["title"] = $row["mod_adv_cal_title"];
                $rtn [$i]["dateStart"] = $row["mod_adv_cal_date_start"];
                $rtn [$i]["dateEnd"] = $row["mod_adv_cal_date_end"];
                $rtn [$i]["state"] = $row["mod_adv_cal_state"];
            }
            
            return $rtn;
        } else {
            return 0;
        }
        
    }

    public function getTypesAppointments(array $var = null)
    {
        //return $var;
        $vars = $var["vars"];
        $userId = $var["user"]["userId"];
        $entitieId = $var["entitieId"];

        $sql = "SELECT * FROM mod_advised_types_appointments WHERE  mod_adv_tya_ent_id='" . $entitieId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_adv_tya_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_adv_tya_name"];
                $return[$i]["summary"] = $row["mod_adv_tya_summary"];
                $return[$i]["type"] = $row["mod_adv_tya_type"];
                $return[$i]["path"] = $row["mod_adv_tya_path"];
                $return[$i]["color"] = $row["mod_adv_tya_color"] ? $row["mod_adv_tya_color"] : "#a1c0fd";
                $return[$i]["json"] = $row["mod_adv_tya_json"];
                $return[$i]["state"] = $row["mod_adv_tya_state"];
            }
            return $return;
        } else {
            return 0;
        }
        
    }

    public function getTypesAppointmentsById(array $var = null)
    {
        //return $var;
        $vars = $var["vars"];
        $userId = $var["user"]["userId"];
        $entitieId = $var["entitieId"];
        $id = $var["id"];

        $sql = "SELECT * FROM mod_advised_types_appointments WHERE  mod_adv_tya_ent_id='" . $entitieId . "' and mod_adv_tya_id='" . $id . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $return["id"] = $id;
            $return["name"] = $row["mod_adv_tya_name"];
            $return["summary"] = $row["mod_adv_tya_summary"];
            $return["type"] = $row["mod_adv_tya_type"];
            $return["path"] = $row["mod_adv_tya_path"];
            $return["color"] = $row["mod_adv_tya_color"] ? $row["mod_adv_tya_color"] : "#a1c0fd";
            $return["json"] = $row["mod_adv_tya_json"];
            $return["state"] = $row["mod_adv_tya_state"];
            return $return;
        } else {
            return 0;
        }
        
    }

    public function getTypesAppointmentsByPath(array $var = null)
    {
        //return $var;
        $vars = $var["vars"];
        $userId = $var["user"]["userId"];
        $entitieId = $var["entitieId"];
        $path = $var["path"];

        $sql = "SELECT * FROM mod_advised_types_appointments WHERE  mod_adv_tya_ent_id='" . $entitieId . "' and mod_adv_tya_path='" . $path . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_adv_tya_id"];
            $return["id"] = $id;
            $return["name"] = $row["mod_adv_tya_name"];
            $return["summary"] = $row["mod_adv_tya_summary"];
            $return["type"] = $row["mod_adv_tya_type"];
            $return["path"] = $row["mod_adv_tya_path"];
            $return["color"] = $row["mod_adv_tya_color"] ? $row["mod_adv_tya_color"] : "#a1c0fd";
            $return["json"] = $row["mod_adv_tya_json"];
            $return["state"] = $row["mod_adv_tya_state"];
            return $return;
        } else {
            return 0;
        }
        
    }

    public function verifyAvailability(array $var = null)
    {
        //return $var;


        $dateStart = $var["dateStart"];
        $dateEnd = $var["dateEnd"];
        $couId = $var["couId"];
        $acuId = $var["acuId"];

        $timeStart = $this->fmt->modules->timeToDate($dateStart);
        $timeEnd = $this->fmt->modules->timeToDate($dateEnd);

        $day = $this->fmt->modules->dayLiteralNum($dateStart);

        $sql = "SELECT mod_adv_sch_hour_start, mod_adv_sch_hour_end
                FROM mod_advised_schedules
                WHERE mod_adv_sch_cou_id='" . $couId . "' AND  
                mod_adv_sch_state='1' AND mod_adv_sch_day = '" . $day . "' AND
                '" . $timeStart . "' >= mod_adv_sch_hour_start and  '" . $timeEnd . "' >= mod_adv_sch_hour_end";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return substr($row["mod_adv_sch_hour_start"], 0, -9) . " - " . substr($row["mod_adv_sch_hour_end"], 0, -9);
        } else {
            return 0;
        }
        
    }

    public function verifyDaysBlocked(array $var = null)
    {
        //return $var;

        $dateStart = $var["dateStart"];
        $dateEnd = $var["dateEnd"];
        $couId = $var["couId"];
        $acuId = $var["acuId"];

        $timeStart =  $dateStart;
        $timeEnd =  $dateEnd;

        $sql = "SELECT mod_adv_cb_id FROM mod_advised_calendars_blocked WHERE mod_adv_cb_state = '1' AND mod_adv_cb_ent_id = '1' AND ('" . $timeStart . "' BETWEEN mod_adv_cb_date_start AND mod_adv_cb_date_end) AND ('" . $timeEnd . "' BETWEEN mod_adv_cb_date_start AND mod_adv_cb_date_end)";

        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_adv_cb_id"];
        } else {
            return 0;
        }

    }

    public function schedulesCounselors(array $var = null)
    {
        $userId = $var["user_id"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        $couId = $vars["item"];

        $now = $this->fmt->modules->dateFormat();
        // date last month 
        $lastMonth = date("Y-m-d", strtotime($now . "-1 month"));

        $sql = "SELECT * FROM mod_advised_calendars WHERE mod_adv_cal_cou_id='" . $couId . "' AND mod_adv_cal_state = 1 AND mod_adv_cal_date_start > '" . $lastMonth . " 00:00'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $acuId = $row["mod_adv_cal_acu_id"];
                $couId = $row["mod_adv_cal_cou_id"];
                $return[$i]["id"] = $row["mod_adv_cal_id"];
                $return[$i]["acuId"] = $acuId;
                $return[$i]["couId"] = $couId;
                $return[$i]["dateStart"] = $row["mod_adv_cal_date_start"];
                $return[$i]["dateEnd"] = $row["mod_adv_cal_date_end"];
                $return[$i]["title"] = $row["mod_adv_cal_title"];
                $return[$i]["details"] = $row["mod_adv_cal_details"];
                $type = $row["mod_adv_cal_type"];
                $arrayType = $this->getTypesAppointmentsByPath(["path" => $type, "entitieId" => $entitieId]);
                $return[$i]["type"] = $row["mod_adv_cal_type"];
                $return[$i]["dataType"] = $arrayType;
                $return[$i]["place"] = $row["mod_adv_cal_place"];
                $return[$i]["state"] = $row["mod_adv_cal_state"];
                $dateStart = explode(" ", $row["mod_adv_cal_date_start"]);
                $dateEnd = explode(" ", $row["mod_adv_cal_date_end"]);
                $hourStart = substr($dateStart[1], 0, -3);
                $hourEnd = substr($dateEnd[1], 0, -3);
                $return[$i]["hourStart"] = $hourStart;
                $return[$i]["hourEnd"] = $hourEnd;
                $return[$i]["counselorName"] = $this->fmt->users->fullName($couId, 1);;
                $return[$i]["name"] = $this->accounts->fullName($acuId);
                $return[$i]["dataAcuId"] = $this->accounts->dataAccount($acuId);
                $return[$i]["linkCelular"] = $this->fmt->options->getValue("link_celular");
            }
            return $return;
        } else {
            return 0;
        }
        
    }

    public function changeStateItem(array $var = null)
    {
        //return $var;
        $vars = $var["vars"];
        $state = $vars["state"];
        $item = $vars["item"];


        $sql = "UPDATE mod_advised_calendars SET
                mod_adv_cal_state ='" . $state . "'
                WHERE mod_adv_cal_id = '" . $item . "' ";
        $this->fmt->querys->consult($sql);
        return 1;
    }

    public function deleteShedule(array $var = null)
    {
        //return $var;
        $vars = $var["vars"];
        $item = $vars["item"];
        $sql = "UPDATE mod_advised_calendars SET
                mod_adv_cal_state ='0'
                WHERE mod_adv_cal_id = '" . $item . "' ";
        $this->fmt->querys->consult($sql);
        return 1;
    }

    public function stateCancelShedule(array $var = null)
    {
        //return $var;
        $vars = $var["vars"];
        $item = $vars["item"];
        $value = $vars["value"];

        $sql = "UPDATE mod_advised_calendars SET
                mod_adv_cal_reason_cancel ='" . $value . "',
                mod_adv_cal_state ='2'
                WHERE mod_adv_cal_id = '" . $item . "' ";
        $this->fmt->querys->consult($sql);

        return 1;
    }

    public function reshedule(array $var = null)
    {
        //return $var; 

        $array = $this->addSchedule($var);

        if ($array["Error"] == 1) {
            $rtn["Error"] = 1;
            $rtn["status"] = "error";
            $rtn["message"] = "Error al reprogramar la cita. ".$array["message"];
            $rtn["data"] = $array;
            return $rtn;
        }

        $userId = $var["userId"];
        $vars = $var["vars"];
        $item = $vars["item"];
        /* $dateStart = $vars["dateStart"];
        $dateEnd = $vars["dateEnd"];
        $counselorId = $vars["counselorId"];
        $entitieId = $vars["entitieId"];
        $place = $vars["place"];
        $details = $vars["details"]; */

        $sql = "UPDATE mod_advised_calendars SET
                mod_adv_cal_user_id ='" . $userId . "',
                mod_adv_cal_reschedule ='" . $array["data"]["calId"] . "',
                mod_adv_cal_state ='3'
                WHERE mod_adv_cal_id = '" . $item . "' ";
        $this->fmt->querys->consult($sql);

        $rtn["Error"] = "0";
        $rtn["status"] = "success";
        //$rtn["arrayCalId"] = $array;
        $rtn["item"] = $item;
        $id = $array["data"]["calId"];
        $couId = $array["data"]["couId"];
        $dataId = $this->dataId($id);
        
        $rtn["data"]["calId"] = $id;
        $rtn["data"]["dataCalId"] = $dataId;
        $rtn["data"]["couId"] = $couId;
        $rtn["data"]["counselorName"] = $this->fmt->users->fullName($couId);
        //$rtn["data"]["date"] = $dateStart;
        $rtn["data"]["dateStart"] = $array["data"]["dateStart"];
        $rtn["data"]["dateEnd"] = $array["data"]["dateEnd"];
        $rtn["data"]["hourStart"] = $array["data"]["hourStart"];
        $rtn["data"]["typeAppointment"] = $array["data"]["typeAppointment"];
        $rtn["data"]["hourEnd"] = $array["data"]["hourEnd"];
        $rtn["data"]["accountDataId"] = $this->accounts->dataAccount($dataId["acuId"]);
        $rtn["data"]["linkCelular"] = $this->fmt->options->getValue("link_celular");

        return $rtn;
    }

    public function saveHoursBlocked(array $var = null)
    {
        $userId = $var["userId"];
        $vars = $var["vars"];
        $item = $vars["item"];
        $entitieId = $var["entitieId"];
        $date = $this->fmt->data->dbDate($vars["dateInput"]);
        $hourEnd = $vars["hourEndInput"];
        $hourStar = $vars["hourStartInput"];
        $inputAllDay = $vars["inputAllDay"];

        $dateStart = $date . " " . $hourStar;
        $dateEnd = $date . " " . $hourEnd;

        if ($inputAllDay == "true") {
            $dateStart = $date . " 00:00:00";
            $dateEnd = $date . " 23:59:59";
        }

        $insert = "mod_adv_cb_date_start,mod_adv_cb_date_end,mod_adv_cb_ent_id,mod_adv_cb_state";
        $values = "'" . $dateStart . ":00','" . $dateEnd . ":00','" . $entitieId . "','1'";
        $sql = "insert into mod_advised_calendars_blocked (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql1 = 'select max(mod_adv_cb_id) as id from mod_advised_calendars_blocked';
        $rs1 = $this->fmt->querys->consult($sql1, __METHOD__);
        $row = $this->fmt->querys->row($rs1);

        //return  $row["id"];
        return $this->dataDayBlocked($row["id"]);
    }


    public function dataDayBlocked(string $id = null)
    {
        //return $var;
        $sql = "SELECT * FROM mod_advised_calendars_blocked WHERE  mod_adv_cb_id = '" . $id . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {

            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_adv_cb_id"];
            $return["id"] = $id;
            $dateArray = explode(" ", $row["mod_adv_cb_date_start"]);
            $dateArrayEnd = explode(" ", $row["mod_adv_cb_date_end"]);
            $dateExplode = explode("-", $dateArray[0]);
            $return["date"] = $dateArray[0];
            $return["dateYear"] = $dateExplode[0];
            $return["dateMonth"] = $dateExplode[1];
            $return["dateDay"] = $dateExplode[2];
            $return["hourEnd"] = substr($dateArrayEnd[1], 0, -3);
            $return["hourStart"] = substr($dateArray[1], 0, -3);
            $return["dateStart"] = $row["mod_adv_cb_date_start"];
            $return["dateEnd"] = $row["mod_adv_cb_date_end"];
            $return["state"] = $row["mod_adv_cb_state"];

            return $return;
        } else {
            return 0;
        }
        
    }

    public function loadDaysBlocked(array $var = null)
    {
        $entitieId = $var["entitieId"];

        $sql = "SELECT * FROM mod_advised_calendars_blocked WHERE mod_adv_cb_state = '1' AND mod_adv_cb_ent_id = '" . $entitieId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_adv_cb_id"];
                $return[$i]["id"] = $id;
                $dateArray = explode(" ", $row["mod_adv_cb_date_start"]);
                $dateArrayEnd = explode(" ", $row["mod_adv_cb_date_end"]);
                $dateExplode = explode("-", $dateArray[0]);
                $hourEnd = substr($dateArrayEnd[1], 0, -3);
                $hourStart = substr($dateArray[1], 0, -3);
                $return[$i]["date"] = $dateArray[0];
                $return[$i]["dateYear"] = $dateExplode[0];
                $return[$i]["dateMonth"] = $dateExplode[1];
                $return[$i]["dateDay"] = $dateExplode[2];
                $return[$i]["hourEnd"] = $hourEnd;
                $return[$i]["hourStart"] = $hourStart;
                $return[$i]["dateStart"] = $row["mod_adv_cb_date_start"];
                $return[$i]["dateEnd"] = $row["mod_adv_cb_date_end"];
                if ($hourStart == '00:00' && $hourEnd == '23:45') {
                    $allday = 1;
                } else {
                    $allday = 0;
                }
                $return[$i]["allday"] =   $allday;
            }
            return $return;
        } else {
            return 0;
        }
        
    }

    public function deleteDateBloked(array $var = null)
    {
        $vars = $var["vars"];
        $item = $vars["item"];

        $sql = "DELETE FROM mod_advised_calendars_blocked WHERE mod_adv_cb_id='" . $item . "'";
        $this->fmt->querys->consult($sql);
        $up_sqr6 = "ALTER TABLE mod_advised_calendars_blocked AUTO_INCREMENT=1";
        $this->fmt->querys->consult($up_sqr6, __METHOD__);

        return 1;
    }

    public function updateHoursBlocked(array $var = null)
    {
        //return $var;
        $userId = $var["userId"];
        $vars = $var["vars"];
        $entitieId = $var["entitieId"];
        $item = $vars["item"];

        $date = $this->fmt->data->dbDate($vars["dateInput"], 0); // desactive hours 
        $hourEnd = $vars["hourEndInput"];
        $hourStar = $vars["hourStartInput"];

        $dateStart = $date . " " . $hourStar;
        $dateEnd = $date . " " . $hourEnd;

        $sql = "UPDATE mod_advised_calendars_blocked SET
                mod_adv_cb_date_start='" . $dateStart . "',
                mod_adv_cb_date_end='" . $dateEnd . "'
               WHERE mod_adv_cb_id= '" . $item  . "' AND mod_adv_cb_ent_id='" . $entitieId . "'";
        $this->fmt->querys->consult($sql);

        return $this->dataDayBlocked($item);
    }

    public function reportAppointment(array $var = null)
    {
        //return $var;
        $dateInit = $var["dateInit"];
        $dateEnd = $var["dateEnd"];
        $type = $var["type"] ? $var["type"] : "appointment";


        /* if ($dateInit == $dateEnd){
            $aux = "mod_adv_cal_date_start >= '" . $dateInit . "'";
        }else{ */
        $aux = "mod_adv_cal_date_start >= '" . $dateInit . "' AND mod_adv_cal_date_end <= '" . $dateEnd . "'";

        $sql = "SELECT * FROM mod_advised_calendars WHERE " . $aux . "  AND mod_adv_cal_type = '" . $type . "'  ORDER BY mod_adv_cal_date_start ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        $return['num'] = $num;
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_adv_cal_id"];
                $return["appointment"][$i]["id"] = $id;
            }

            return $return;
        } else {
            return 0;
        }
        
    }

    public function reportAppointmentsCounselor(array $var = null)
    {
        //return $var;

        $dateInit = $var["dateInit"];
        $dateEnd = $var["dateEnd"];
        $couId = $var["couId"];
        $type = $var["type"] ? $var["type"] : "appointment";
        //return $dateInit.":".$dateEnd;
        $state = $var["state"] ? $var["state"] : "";

        if ($state != "") {
            $stateSql = " AND mod_adv_cal_state = '" . $state . "'";
        }

        if ($dateInit == $dateEnd) {
            $dateInit = $dateInit . " 00:00:00";
            $dateEnd = $dateEnd . " 23:59:59";
        }

        $aux = "mod_adv_cal_date_start >= '" . $dateInit . "' AND mod_adv_cal_date_end <= '" . $dateEnd . "'";

        $sql = "SELECT * FROM mod_advised_calendars WHERE " . $aux . " AND mod_adv_cal_cou_id = '" . $couId . "' " . $stateSql  . " AND mod_adv_cal_type = '" . $type . "'  ORDER BY mod_adv_cal_date_start ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_adv_cal_id"];
                $return[$i]["id"] = $id;
                $return[$i]["title"] = $row["mod_adv_cal_title"];
                $return[$i]["details"] = $row["mod_adv_cal_details"];
                $return[$i]["place"] = $row["mod_adv_cal_place"];
                $return[$i]["reasonCancel"] = $row["mod_adv_cal_reason_cancel"];
                $return[$i]["state"] = $row["mod_adv_cal_state"];
            }
            return $return;
        } else {
            return 0;
        }
        
    }

    public function counselorsAssigned(int $id = null)
    {
        //return $var;
        $sql = "SELECT * FROM mod_advised_counselors WHERE mod_adv_cou_acu_id = '" . $id . "' ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_adv_cou_cou_id"];
                $return["counselor"][$i]["couId"] = $id;
                $return["counselor"][$i]["name"] = $this->fmt->users->fullName($id);
                $return["counselor"][$i]["order"] = $row["mod_adv_cou_order"];
            }
            $return["numCounselors"] = $num;
            return $return;
        } else {
            return 0;
        }
        
    }

    public function searchUserNotAssigned(array $var = null)
    {
        //return $var;
        require_once(_PATH_NUCLEO . "modules/counseling/models/class/class.counseling.php");
        $counseling = new COUNSELING($this->fmt);

        $q = $var["vars"]["search"];

        $search = "mod_acu_name LIKE '%" . $q . "%'  OR mod_acu_fathers_lastname LIKE '%" . $q . "%' OR mod_acu_mothers_lastname LIKE '%" . $q . "%' OR mod_acu_ci LIKE '%" . $q . "%' ";
        $sql = "SELECT * FROM mod_accounts_users, mod_accounts_services WHERE  mod_acu_sv_sv_id='1' AND ( " . $search . " ) AND mod_acu_sv_acu_id=mod_acu_id";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_acu_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_acu_name"] . " " . $row["mod_acu_fathers_lastname"] . " " . $row["mod_acu_mothers_lastname"];
                $return[$i]["email"] = $row["mod_acu_email"];
                $return[$i]["dial"] = $row["mod_acu_dial"];
                $return[$i]["phone"] = $row["mod_acu_celular"];
                $return[$i]["ci"] = $row["mod_acu_ci"];
                $return[$i]["ext"] = $row["mod_acu_ext"];
                $return[$i]["counselors"] =  $this->counselorsAssigned($id);
            }
            $rtn['result'] = $return;
            $rtn['sql'] =  $sql;
            $rtn['sqlnum'] =  $num;
            $rtn['counselors'] = $counseling->listCounselors($var);
            return $rtn;
        } else {
            return 0;
        }
        
    }

    public function searchUsersAccounts(array $var = null){
        //return $var;
        $entId = $var["entitieId"];
        $rolId = $var["user"]["rolId"];
        

        require_once(_PATH_NUCLEO . "modules/counseling/models/class/class.counseling.php");
        $counseling = new COUNSELING($this->fmt);

        $rolIdSecretaria = $counseling->getValue("rolIdSecretaria");
        $rolIdJefeConsejeros = $counseling->getValue("rolIdJefeConsejeros");
        $rolIdConsejeros = $counseling->getValue("rolIdConsejeros");

        $q = $var["vars"]["input"]["search"];
        $couId = $var["vars"]["input"]["couId"];

        //$search = "mod_acu_name LIKE '%" . $q . "%'  OR mod_acu_fathers_lastname LIKE '%" . $q . "%' OR mod_acu_mothers_lastname LIKE '%" . $q . "%' OR mod_acu_ci LIKE '%" . $q . "%' OR mod_acu_id LIKE '%" . $q . "%' ";

        $search = "mod_acu_id LIKE '%" . $q . "%' OR CONCAT(mod_acu_name, ' ',mod_acu_fathers_lastname, ' ',mod_acu_mothers_lastname) LIKE '%" . $q . "%' OR mod_acu_ci LIKE '%" . $q . "%'";

        $sql = "SELECT * FROM mod_accounts_users, mod_accounts_services WHERE  mod_acu_sv_sv_id='1' AND ( " . $search . " ) AND mod_acu_sv_acu_id=mod_acu_id ORDER BY mod_acu_id, mod_acu_name, mod_acu_fathers_lastname DESC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_acu_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_acu_name"] . " " . $row["mod_acu_fathers_lastname"] . " " . $row["mod_acu_mothers_lastname"];
                $return[$i]["email"] = $row["mod_acu_email"];
                $return[$i]["dial"] = $row["mod_acu_dial"];
                $return[$i]["phone"] = $row["mod_acu_celular"];
                $return[$i]["ci"] = $row["mod_acu_ci"];
                $return[$i]["ext"] = $row["mod_acu_ext"];
                $return[$i]["counselors"] =  $this->counselorsAssigned($id);
                $dataAssignet = $this->counselorsAssigned($id);
                $return[$i]["assigned"] = 0;

                if (is_array($dataAssignet)) {
                    foreach ($dataAssignet as $key => $value) {
                        if ($value["couId"] == $couId) {
                            $return[$i]["assigned"] = 1;
                        }
                    }
                }

            }
            $rtn['result'] = $return;
            //$rtn['sql'] =  $sql;
            $rtn['num'] =  $num;
            if ($rolIdSecretaria == $rolId){
                $rtn['rolEdit'] = 0;
            }
            if ($rolIdJefeConsejeros == $rolId || $rolIdConsejeros == $rolId){
                $rtn['rolEdit'] = 1;
            }
            
            $rtn['counselors'] = $counseling->listCounselors($var);
            return $rtn;
        } else {
            return 0;
        }
        //
        
    }

    public function asignedAdvised(array $var = null)
    {
        //return $var;
        $userId = $var["user"]["userId"];
        $acuId = $var["vars"]["acuId"];

        $insert = "mod_adv_cou_acu_id,mod_adv_cou_cou_id,mod_adv_cou_order";
        $values = "'" . $acuId . "', '" . $userId . "', '1'";
        $sql = "insert into  mod_advised_counselors (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        return 1;
    }

    public function saveEditDataAccountCalendar(array $var = null)
    {
        //return $var;
        $vars = $var["vars"];
        $inputs = $vars["inputs"];
        $id = $inputs["id"];
        $celular = $inputs["celular"];
        $email = $inputs["email"];

        if (!$this->fmt->validations->email($email, 1)) { // con 1, si esta vacio retorna 1, sirve para validar campos opcionales
            $return["error"] = 1;
            $return["status"] = "error";
            $return["message"] = "El email no es valido";
            return $return;
        }

        if (!$this->fmt->validations->numLiteral($celular)) {
            $return["error"] = 1;
            $return["status"] = "error";
            $return["message"] = "El celular no es valido";
            return $return;
        }

        $sql = "UPDATE mod_accounts_users SET
                mod_acu_celular ='" . $celular . "',
                mod_acu_email ='" . $email . "'
                WHERE mod_acu_id = '" . $id . "' ";
        $this->fmt->querys->consult($sql);

        $return["error"] = 0;
        $return["status"] = "success";
        return $return;
    }

    public function getDataCalendarList(array $var = null)
    {
        //return $var;
        $vars = $var["vars"];
        $inputs = $vars["inputs"];
        $couId = $inputs["couId"];

        $sql = "SELECT * FROM mod_advised_calendars WHERE mod_adv_cal_cou_id = '" . $couId . "' AND mod_adv_cal_ent_id = '" . $entitieId . "' AND mod_adv_cal_state IN (0,2,3,4)";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_adv_cal_id"];
                $return[$i]["id"] = $id;
                $return[$i][""] = $row["mod_"];
            }
            return $return;
        } else {
            return 0;
        }
        
    }
}
