<?php
header('Content-Type: text/html; charset=utf-8');
class COUNSELING
{

    var $fmt;
    var $accounts;
    var $kardex;
    var $idModAdvised = 501;
    var $idService = 1;
    var $dailyAttendance = 8;
    var $calendarAdvised;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
        require_once(_PATH_NUCLEO . "modules/accounts/models/class/class.accounts.php");
        $this->accounts = new ACCOUNTS($fmt);
        require_once(_PATH_NUCLEO . "modules/rrhh/models/class/class.kardex.php");
        $this->kardex = new KARDEX($fmt);
        require_once(_PATH_NUCLEO . "modules/counseling/models/class/class.calendarAdvised.php");
        $this->calendarAdvised = new CALENDARADVISED($fmt);
    }

    public function dataUserAdvisedId($id = null)
    {
        $sql = "SELECT * FROM  mod_accounts_users WHERE  mod_acu_id='" . $id . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $acuId = $row["mod_acu_id"];
            $fact["id"] = $acuId;
            $fact["id"] = $row["mod_acu_id"];
            $fact["name"] = $row["mod_acu_name"];
            $fact["fathers_lastname"] = $row["mod_acu_fathers_lastname"];
            $fact["mothers_lastname"] = $row["mod_acu_mothers_lastname"];
            $fact["age_range"] = $row["mod_acu_age_range"];
            $fact["email"] = $row["mod_acu_email"];
            $fact["birthday_date"] = $row["mod_acu_birthday_date"];
            //$fact[""]=$row["mod_acu_password"];
            $fact["imagen"] = $row["mod_acu_imagen"];
            $fact["level"] = $row["mod_acu_level"];
            $fact["gender"] = $row["mod_acu_gender"];
            $fact["ci"] = $row["mod_acu_ci"];
            $fact["ci_ext"] = $row["mod_acu_ci_ext"];
            $fact["celular"] = $row["mod_acu_celular"];
            $fact["record_date"] = $row["mod_acu_record_date"];
            $fact["type"] = $row["mod_acu_type"];
            $fact["city"] = $row["mod_acu_city"];
            $fact["timezone"] = $row["mod_acu_timezone"];
            $fact["locale"] = $row["mod_acu_locale"];
            $fact["token"] = $row["mod_acu_token"];
            $fact["state"] = $row["mod_acu_state"];
            $fact["entitieId"] = $row["mod_acu_ent"];


            $adviserId = $this->dataAccountIdToAdvisedId($acuId);
            $arrayAdviserId  = $this->dataAdvisedId($adviserId);

            //$fact[""]=$arrayAdviserId["adv_id"];
            //$fact[""]=$arrayAdviserId["adv_acu_id"];
            $fact["civil_state"] = $arrayAdviserId["mod_adv_civil_state"];
            $fact["comments"] = $arrayAdviserId["mod_adv_comments"];
            $fact["dependents"] = $arrayAdviserId["mod_adv_dependents"];
            $fact["telf_fax"] = $arrayAdviserId["mod_adv_telf_fax"];
            $fact["address"] = $arrayAdviserId["mod_adv_address"];
            $fact["coordinates"] = $arrayAdviserId["mod_adv_coordinates"];
            $fact["labor_status"] = $arrayAdviserId["mod_adv_labor_status"];
            $fact["company_work"] = $arrayAdviserId["mod_adv_company_work"];
            $fact["address_work"] = $arrayAdviserId["mod_adv_address_work"];
            $fact["assignedCounselor"] = $this->realationAdvisedCounselor($acuId);
            $fact["services"] = $this->accounts->realationAccountServices($acuId);


            return $fact;
        } else {
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function dataAdvisedId($id = null)
    {
        $sql = "SELECT * FROM  mod_advised_user WHERE  mod_adv_id='" . $id . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row;
        } else {
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function dataAccountIdToAdvisedId($acuId = null)
    {
        $sql = "SELECT mod_adv_id FROM  mod_advised_user WHERE  mod_adv_acu_id='" . $acuId . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_adv_id"];
        } else {
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function listCounselors($var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $rolId = $var["rolId"];
        $userIdSession = $var["userId"];
        $vars = $var["vars"];
        $filter = $vars["filter"] ? $vars["filter"] : "";
        $active = $vars["active"] ? $vars["active"] : 0;

        $rolIdJefeConsejeros = $this->getValue("rolIdJefeConsejeros");
        $rolIdConsejeros = $this->getValue("rolIdConsejeros");
        $rolIdSecretariaConsejeria = $this->getValue("rolIdSecretaria");

        $arrayCounselorsBoss = $this->fmt->users->usersRolEntitie($rolIdJefeConsejeros, $entitieId);
        $arrayCounselors = $this->fmt->users->usersRolEntitie($rolIdConsejeros, $entitieId);

        $numBoss = count($arrayCounselorsBoss);
        $num = count($arrayCounselors);

        if ($numBoss == 0 && $num == 0) {
            return 0;
        }

        for ($i = 0; $i < $numBoss; $i++) {
            $userId = $arrayCounselorsBoss[$i]["id"];
            $arrayFields = $this->counselorsFields($userId);
            $arrayKardex = $this->kardex->dataId($userId);
            if ($arrayFields != 0) {
                $numFields = count($arrayFields);
                $fields = [];
                for ($j = 0; $j < $numFields; $j++) {
                    $fields[$j] = $this->getName($arrayFields[$j]["id"]);
                }
                $fields = implode(", ", $fields);
            } else {
                $fields = 0;
            }

            $arrayCounselorsBoss[$i]["stateRRHH"] = $arrayKardex;
            $arrayCounselorsBoss[$i]["fields"] = $fields;
            $arrayCounselorsBoss[$i]["active"] = $active;
            $arrayCounselorsBoss[$i]["id"] = $userId;
        }

        //return $arrayCounselorsBoss;


        for ($i = 0; $i < $num; $i++) {
            $userId = $arrayCounselors[$i]["id"];
            $arrayFields = $this->counselorsFields($userId);
            if ($arrayFields != 0) {
                $numFieldsCounselor = count($arrayFields);
                $fields = [];
                for ($j = 0; $j < $numFieldsCounselor; $j++) {
                    $fields[$j] = $this->getName($arrayFields[$j]["id"]);
                }
                $fields = implode(", ", $fields);
            } else {
                $fields = 0;
            }
            $arrayKardex = $this->kardex->dataId($userId);

            $arrayCounselors[$i]["stateRRHH"] =  $arrayKardex;
            $arrayCounselors[$i]["fields"] = $fields;
            $arrayCounselors[$i]["arraysFields"] = $arrayFields;
            $arrayCounselors[$i]["active"] = $active;
            $arrayCounselors[$i]["id"] = $userId;
        }

        //return $arrayCounselors;

        if ($arrayCounselors == 0 && $arrayCounselorsBoss == 0) {
            return 0;
        }


        if ($filter == "rol") {
            //return "message:" . $rolId;
            if ($rolId == $rolIdConsejeros) { //Conselor
                //return "aqui" . $userIdSession;
                $arrayField = $this->counselorsFields($userIdSession);
                if ($arrayFields != 0) {
                    $numField = count($arrayField);
                    $field = [];
                    for ($j = 0; $j < $numField; $j++) {
                        $field[$j] = $this->getName($arrayField[$j]["id"]);
                    }
                } else {
                    $field = 0;
                }
                //return $field;
                //return $userIdSession;
                $arrayCounselor = [];
                $arrayUser = $this->fmt->users->getDataId($userIdSession);
                $arrayCounselor[0]["id"] = $userIdSession; // [todo] here get state kardex
                $arrayCounselor[0]["name"] = $arrayUser["name"]; // [todo] here get state kardex
                $arrayCounselor[0]["lastname"] = $arrayUser["lastname"]; // [todo] here get state kardex
                $arrayCounselor[0]["stateRRHH"] = 0; // [todo] here get state kardex
                if ($field == 0) {
                    $arrayCounselor[0]["fields"] = 0;
                } else {
                    $arrayCounselor[0]["fields"] = implode(", ", $field);
                }
                $arrayCounselor[0]["active"] = $active;

                return  $arrayCounselor;
            }

            if ($rolId == $rolIdJefeConsejeros  || $rolId == $rolIdSecretariaConsejeria) { //Jefe de Consejeros
                return array_merge($arrayCounselorsBoss, $arrayCounselors);
            }

            if ($rolId == 1) {
                return array_merge($arrayCounselorsBoss, $arrayCounselors);
            }
        } else {

            return array_merge($arrayCounselorsBoss, $arrayCounselors);
        }
    }

    public function counselorsFields($userId = null)
    {
        $sql = "SELECT mod_adv_fds_fds_id FROM mod_advised_counselors_fields WHERE mod_adv_fds_user_id='" . $userId . "' ORDER BY mod_adv_fds_order ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return[$i]["id"] = $row["mod_adv_fds_fds_id"];
            }
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function listAdvised($vars = null)
    {
        $userId = $vars["userId"];
        $rolId = $vars["rolId"];
        $entitieId = $vars["entitieId"];

        $rolIdJefeConsejeros = $this->getValue("rolIdJefeConsejeros");
        $rolIdConsejeros = $this->getValue("rolIdConsejeros");
        $rolIdSecretaria = $this->getValue("rolIdSecretaria");

        if (($rolId == $rolIdJefeConsejeros) || ($rolId == $rolIdSecretaria) || ($rolId == 1)  || ($rolId == 2)) {
            $sql = "SELECT * FROM mod_accounts_users,mod_accounts_services WHERE mod_acu_sv_acu_id=mod_acu_id AND mod_acu_sv_sv_id='1' AND mod_acu_state > 0 ORDER BY mod_acu_sv_order ASC";
            $canCreateUser = 1;
            $canEditHistory = 1;
        }

        if (($rolId == $rolIdSecretaria)) {
            $canEditHistory = 0;
        }

        if ($rolId == $rolIdConsejeros) {
            $canCreateUser = 1;
            $canEditHistory = 1;
            $sql = "SELECT * FROM mod_accounts_users,mod_accounts_services,mod_advised_counselors 
                    WHERE mod_acu_sv_acu_id=mod_acu_id AND mod_acu_sv_sv_id='1' AND mod_adv_cou_acu_id=mod_acu_id  AND	mod_adv_cou_cou_id='" . $userId . "' AND mod_acu_state > 0 
                    ORDER BY mod_acu_sv_order ASC";
        }

        $user["canCreateUser"] = $canCreateUser;
        $user["canEditHistory"] = $canEditHistory;

        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $userId = $row["mod_acu_id"];
                $user["users"][$i]["id"] = $userId;
                $user["users"][$i]["name"] =  $row["mod_acu_name"];
                $user["users"][$i]["fathersLastname"] =  $row["mod_acu_fathers_lastname"];
                $user["users"][$i]["mothersLastname"] = $row["mod_acu_mothers_lastname"];
                $user["users"][$i]["celular"] = $row["mod_acu_celular"];
                $user["users"][$i]["email"] = $row["mod_acu_email"];
                $user["users"][$i]["counselors"] =  $this->realationAdvisedCounselor($userId);
                $user["users"][$i]["ci"] =  $row["mod_acu_ci"];
                $user["users"][$i]["ext"] =  $row["mod_acu_ci_ext"];
                $dataAdvised = $this->dataAdvisedId($userId);
                $user["users"][$i]["fieldInitial"] =  $dataAdvised["mod_adv_fds_id"];
            }
            return $user;
        } else {
            $user["users"] = 0;
            return $user;
        }
    }

    public function saveAdvised($vars = null)
    {
        //return $vars;
        $userId = $vars["userId"];
        $rolId = $vars["rolId"];
        $entitieId = $vars["entitieId"];

        $var =  $vars["vars"];
        $email = $var["email"];
        $ci = $var["ci"];
        $ext = $var["ext"];
        $name = $var["name"] . " " . $var["fathersLastname"] . " " . $var["mothersLastname"];


        if (empty($ci)) {
            $return["Error"] = "error";
            $return["message"] = "Error, El CI esta vacio, intenta denuevo.";
            return $return;
        }

        if ($this->accounts->checkCI($ci, $ext)) {
            $return["Error"] = "error";
            $return["message"] = "Error, El CI existe por favor intenta con otro.";
            return $return;
        }

        if (!empty($email)) {
            if ($this->accounts->checkEmail($email)) {
                $return["Error"] = "error";
                $return["message"] = "Error, el Email existe";
                return $return;
            }
        }

        if ($vars != 0) {
            //return $var;
            $idAccount = $this->accounts->add(array("vars" => $var, "entitieId" => $entitieId));
            //var_dump($idAccount);
        }

        if ($idAccount) {
            $idAdvised = $this->addAdvisedData(array("vars" => $var, "idAccount" => $idAccount));
        }

        if ($idAdvised) {
            $return["Error"] = 0;
            $return["status"] = "success";
            $return["message"] = "Se ha guardado correctamente";
            $return["data"]["idAccount"] = $idAccount;
            $return["data"]["name"] = $name;
            return $return;
        } else {
            $return["Error"] = 1;
            $return["status"] = "error";
            $return["message"] = "Error, no se pudo guardar";

            return $return;
        }
    }

    public function updateAdvisedData(array $var = null)
    {
        //return $var;

        $vars = $var["vars"];

        $fullName = ucwords(strtolower($vars["name"] . " " . $vars["fathersLastname"] . " " . $vars["mothersLastname"]));
        $acuId = $var["acuId"];
        $fdsId = $vars["fieldInitial"] ? $vars["fieldInitial"] : 0;
        $civilState = $vars["civilState"];
        $dependents = $vars["dependents"];
        $address = $vars["address"];
        $coordinates = $vars["coordinates"] ? $vars["coordinates"] : "0,0";
        $laborStatus = $vars["laborStatus"];
        $companyWork = $vars["companyWork"];
        $addressWork = $vars["addressWork"];
        $comments = $vars["comments"];
        $telffax = $vars["telfFax"];
        $workArea = $vars["workArea"];
        $profesion = $vars["profesion"];
        $position = $vars["position"];
        $spouseFullName = $vars["spouseFullName"];
        $spouseGender = $vars["spouseGender"];
        $spouseCi = $vars["spouseCi"];
        $spouseExt = $vars["spouseExt"];
        $spouseBirthday = $vars["spouseBirthday"];
        $spouseNationality = $vars["spouseNationality"];
        $spouseTelffax = $vars["spouseTelffax"];
        $spouseCellphone = $vars["spouseCellphone"];
        $spouseEmail = $vars["spouseEmail"];
        $spouseLaborStatus = $vars["spouseLaborStatus"];
        $spouseCompanyWork = $vars["spouseCompanyWork"];
        $spouseAddressWork = $vars["spouseAddressWork"];
        $spouseWorkArea = $vars["spouseWorkArea"];
        $spouseProfesion = $vars["spouseProfesion"];
        $spousePosition = $vars["spousePosition"];

        $advisedId = $this->dataAccountIdToAdvisedId($acuId);


        //return 'ad:'.$acuId.' ai:'.$advisedId['mod_adv_id'];

        if ($advisedId == 0) {
            // Not exist account Advised
            $insert = "mod_adv_fullname
                ,mod_adv_acu_id
                ,mod_adv_fds_id
                ,mod_adv_civil_state
                ,mod_adv_dependents
                ,mod_adv_telf_fax
                ,mod_adv_address
                ,mod_adv_comments
                ,mod_adv_coordinates
                ,mod_adv_labor_status
                ,mod_adv_company_work
                ,mod_adv_address_work
                ,mod_adv_work_area
                ,mod_adv_profession
                ,mod_adv_position
                ,mod_adv_spouse_fullname
                ,mod_adv_spouse_gender
                ,mod_adv_spouse_ci
                ,mod_adv_spouse_ext
                ,mod_adv_spouse_birthday
                ,mod_adv_spouse_nationality
                ,mod_adv_spouse_telf_fax
                ,mod_adv_spouse_cellphone
                ,mod_adv_spouse_email
                ,mod_adv_spouse_labor_status
                ,mod_adv_spouse_company_work
                ,mod_adv_spouse_address_work
                ,mod_adv_spouse_work_area
                ,mod_adv_spouse_profession
                ,mod_adv_spouse_position";
            $values = "'" . $fullName . "','" .
                $acuId . "','" .
                $fdsId . "','" .
                $civilState . "','" .
                $dependents . "','" .
                $telffax . "','" .
                $address . "','" .
                $comments . "','" .
                $coordinates . "','" .
                $laborStatus . "','" .
                $companyWork . "','" .
                $addressWork . "','" .
                $workArea . "','" .
                $profesion . "','" .
                $position . "','" .
                $spouseFullName . "','" .
                $spouseGender . "','" .
                $spouseCi . "','" .
                $spouseExt . "','" .
                $spouseBirthday . "','" .
                $spouseNationality . "','" .
                $spouseTelffax . "','" .
                $spouseCellphone . "','" .
                $spouseEmail . "','" .
                $spouseLaborStatus . "','" .
                $spouseCompanyWork . "','" .
                $spouseAddressWork . "','" .
                $spouseWorkArea . "','" .
                $spouseProfesion . "','" .
                $spousePosition . "'";

            $sql = "insert into  mod_advised_user (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);

            return 1;
        } else {
            $sql = "UPDATE mod_advised_user SET
                mod_adv_fullname='" . $fullName . "',
                mod_adv_fds_id='" . $fdsId . "',
                mod_adv_civil_state='" . $civilState . "',
                mod_adv_dependents='" . $dependents . "',
                mod_adv_telf_fax='" . $telffax . "',
                mod_adv_address='" . $address . "',
                mod_adv_comments='" . $comments . "',
                mod_adv_coordinates='" . $coordinates . "',
                mod_adv_labor_status='" . $laborStatus . "',
                mod_adv_company_work='" . $companyWork . "',
                mod_adv_address_work='" . $addressWork . "',
                mod_adv_work_area='" . $workArea . "',
                mod_adv_profession='" . $profesion . "',
                mod_adv_position='" . $position . "',
                mod_adv_spouse_fullname='" . $spouseFullName . "',
                mod_adv_spouse_gender = '" . $spouseGender . "',
                mod_adv_spouse_ci = '" . $spouseCi . "',
                mod_adv_spouse_ext = '" . $spouseExt . "',
                mod_adv_spouse_birthday = '" . $spouseBirthday . "',
                mod_adv_spouse_nationality = '" . $spouseNationality . "',
                mod_adv_spouse_telf_fax = '" . $spouseTelffax . "',
                mod_adv_spouse_cellphone = '" . $spouseCellphone . "',
                mod_adv_spouse_email = '" . $spouseEmail . "',
                mod_adv_spouse_labor_status = '" . $spouseLaborStatus . "',
                mod_adv_spouse_company_work = '" . $spouseCompanyWork . "',
                mod_adv_spouse_address_work = '" . $spouseAddressWork . "',
                mod_adv_spouse_work_area = '" . $spouseWorkArea . "',
                mod_adv_spouse_profession = '" . $spouseProfesion . "',
                mod_adv_spouse_position = '" . $spousePosition . "'
                WHERE mod_adv_acu_id = '" . $acuId . "' ";
            $this->fmt->querys->consult($sql);
            return 1;
        }

        return 0;
    }

    public function modifiedAdvised($vars = null)
    {
        //return $vars;
        $userId = $vars["userId"];
        $var =  $vars["vars"];

        $response = $this->accounts->updateAccount(array("vars" => $var));

        if ($response != 0) {
            $acuId = $response;
            $this->updateAdvisedData(array("vars" => $var, "acuId" => $acuId));
            $this->deleteRelationCounselor($acuId);
            $this->accounts->deleteRelationServises($acuId);

            $rowSqlServices = 'mod_acu_sv_sv_id,mod_acu_sv_acu_id,mod_acu_sv_order';
            $services = $var["services"];
            $countServices = count($services);

            for ($i = 0; $i < $countServices; $i++) {
                $valoresServices = "'" . $services[$i]  . "','" . $acuId . "','" . $i . "'";
                $sqlServices = "insert into mod_accounts_services (" . $rowSqlServices . ") values (" . $valoresServices . ")";
                $this->fmt->querys->consult($sqlServices, __METHOD__);
            }

            $rowSqlCounselors = "mod_adv_cou_acu_id,mod_adv_cou_cou_id,mod_adv_cou_order";
            $counselors = $var["counselors"];
            $countCounselors = count($counselors);
            for ($i = 0; $i < $countCounselors; $i++) {
                $valuesSqlCounselors = "'" . $acuId . "','" . $counselors[$i] . "','" . $i . "'";
                $sqlCounselors = "insert into mod_advised_counselors (" . $rowSqlCounselors . ") values (" . $valuesSqlCounselors . ")";
                $this->fmt->querys->consult($sqlCounselors, __METHOD__);
            }
            return $response;
        } else {
            return 0;
        }
    }

    public function saveRelationAdvisedService($var = null)
    {
        return $var;
        /* $ingresar = 'mod_acu_sv_sv_id,mod_acu_sv_acu_id,mod_acu_sv_order';
        $valores  = "'" . $valor1 . "','" . $valor2 . "'";
        $sql = "insert into modulo (" . $ingresar . ") values (" . $valores . ")";
        $this->fmt->querys->consult($sql, __METHOD__); */
    }

    public function realationAdvisedCounselor($acuId = null)
    {
        $sql = "SELECT mod_adv_cou_cou_id FROM mod_advised_counselors,users WHERE mod_adv_cou_acu_id='" . $acuId . "' AND mod_adv_cou_cou_id=user_id AND user_state > 0 ORDER BY mod_adv_cou_order ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $userId = $row["mod_adv_cou_cou_id"];
                $return[$i]["id"] = $userId;
                $return[$i]["name"] =  $this->fmt->users->fullName($userId);
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function addAdvisedData($vars = null)
    {
        $var = $vars["vars"];
        $idAccount = $vars["idAccount"];
        $fullname = ucwords(strtolower($var["fathersLastname"] . " " . $var["mothersLastname"] . "," . $var["name"]));

        $services = $var["services"];
        $counselors = $var["counselors"];

        $adv_acuId                   = $idAccount;
        $adv_civilState              = $var["civilState"];
        $adv_dependents              = $var["dependents"];
        $adv_comments              = $var["comments"];
        $adv_telfFax                 = $var["telfFax"];
        $adv_address                 = $var["address"];
        $adv_coordinate              = "00.00.00";
        $adv_status                  = $var["WorkState"];
        $adv_companyWork             = $var["Work"];
        $adv_addressWork             = $var["AdressWork"];
        $adv_workArea                = $var["WorkSector"];
        $adv_profession              = $var["Profession"];
        $adv_positon                 = $var["Position"];
        $adv_spouse_name             = $var["NameSpouse"];
        $adv_spouse_gender           = $var["SexSpouse"];
        $adv_spouse_ci               = $var["CISpouse"];
        $adv_spouse_ext              = $var["ExtSpouse"];
        $adv_spouse_birthday         = $var["BirdaydateSpouse"];
        $adv_spouse_nationality      = $var["ExtSpouse"];
        $adv_spouse_telf             = $var["TelfFaxSpouse"];
        $adv_spouse_cellphone        = $var["CelularSpouse"];
        $adv_spouse_laborStatus      = $var["WorkStateSpouse"];
        $adv_spouse_companyWork      = $var["WorkSpouse"];
        $adv_spouse_addressWork      = $var["AdressWorkSpouse"];
        $adv_spouse_workArea         = $var["WorkSectorSpouse"];
        $adv_spouse_profession       = $var["ProfessionSpouse"];
        $adv_spouse_postion          = $var["PositionSpouse"];



        $ingresarAdvised = 'mod_adv_acu_id,mod_adv_fullname,mod_adv_civil_state,mod_adv_dependents,mod_adv_telf_fax,mod_adv_address,mod_adv_coordinates,mod_adv_labor_status,mod_adv_company_work,mod_adv_address_work, mod_adv_work_area, mod_adv_profession, mod_adv_position, mod_adv_spouse_fullname, mod_adv_spouse_gender , mod_adv_spouse_ci,mod_adv_spouse_ext,mod_adv_spouse_birthday,mod_adv_spouse_nationality,mod_adv_spouse_telf_fax,mod_adv_spouse_cellphone,mod_adv_spouse_labor_status,mod_adv_spouse_company_work,mod_adv_spouse_address_work,mod_adv_spouse_work_area,mod_adv_spouse_profession,mod_adv_spouse_position,mod_adv_comments';

        $valoresAdvised  = "'" . $adv_acuId . "','" .
            $fullname . "','" .
            $adv_civilState . "','" .
            $adv_dependents . "','" .
            $adv_telfFax . "','" .
            $adv_address . "','" .
            $adv_coordinate . "','" .
            $adv_status . "','" .
            $adv_companyWork . "','" .
            $adv_addressWork . "','" .
            $adv_workArea  . "','" .
            $adv_profession  . "','" .
            $adv_positon  . "','" .
            $adv_spouse_name  . "','" .
            $adv_spouse_gender  . "','" .
            $adv_spouse_ci . "','" .
            $adv_spouse_ext . "','" .
            $adv_spouse_birthday . "','" .
            $adv_spouse_nationality . "','" .
            $adv_spouse_telf . "','" .
            $adv_spouse_cellphone . "','" .
            $adv_spouse_laborStatus . "','" .
            $adv_spouse_companyWork . "','" .
            $adv_spouse_addressWork . "','" .
            $adv_spouse_workArea . "','" .
            $adv_spouse_profession . "','" .
            $adv_spouse_postion . "','" .
            $adv_comments . "'";

        $sqlAdvised = "insert into mod_advised_user (" . $ingresarAdvised . ") values (" . $valoresAdvised . ")";
        $this->fmt->querys->consult($sqlAdvised, __METHOD__);

        $rowSqlServices = 'mod_acu_sv_sv_id,mod_acu_sv_acu_id,mod_acu_sv_order';
        $countServices = count($services);
        for ($i = 0; $i < $countServices; $i++) {
            $valoresServices = "'" . $services[$i]  . "','" . $idAccount . "','" . $i . "'";
            $sqlServices = "insert into mod_accounts_services (" . $rowSqlServices . ") values (" . $valoresServices . ")";
            $this->fmt->querys->consult($sqlServices, __METHOD__);
        }


        $rowSqlCounselors = "mod_adv_cou_acu_id,mod_adv_cou_cou_id,mod_adv_cou_order";
        $countCounselors = count($counselors);
        for ($i = 0; $i < $countCounselors; $i++) {
            $valuesSqlCounselors = "'" . $idAccount . "','" . $counselors[$i] . "','" . $i . "'";
            $sqlCounselors = "insert into mod_advised_counselors (" . $rowSqlCounselors . ") values (" . $valuesSqlCounselors . ")";
            $this->fmt->querys->consult($sqlCounselors, __METHOD__);
        }

        return $idAccount;
    }

    public function dataHistoryId($id = null)
    {
        $sql = "SELECT * FROM  mod_advised_history WHERE  mod_adv_his_id='" . $id . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row;
        } else {
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function historyAdvised($vars = null)
    {
        $acuId = $vars["acuId"];
        $sql = "SELECT * FROM mod_advised_history WHERE mod_adv_his_acu_id='" . $acuId . "' ORDER BY mod_adv_his_date_init ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        $return["fields"] = $this->listFields();
        $return["num"] = $num;
        $return["acuId"] = $acuId;
        $return["dataAccount"] = $this->accounts->dataAccount($acuId);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id  = $row["mod_adv_his_id"];
                $fdsId  = $row["mod_adv_his_fds_id"];
                $comments  = $row["mod_adv_his_comments"];
                $dateInit  = $row["mod_adv_his_date_init"];
                $dateEnd = $row["mod_adv_his_date_end"];
                $place  = $row["mod_adv_his_place"];
                $data[$i]["id"] = $id;
                $data[$i]["field"] = $this->getName($fdsId);
                $data[$i]["comments"] = $comments;
                $data[$i]["type"] = $row["mod_adv_his_type"] ? 'Fuera de hora' : '0';
                $data[$i]["place"] = $place;
                $data[$i]["dateInit"] = $dateInit;
                $data[$i]["dateEnd"] = $dateEnd;
            }
            $return["data"] = $data;
        }
        return $return;
    }

    public function afterhoursRegistrationHistory(array $vars = null)
    {
        //return $vars;
        $userId = $vars["user"]["userId"];
        $var = $vars["vars"]["inputs"];
        $inputDataInit = $var["inputDateInit"];
        $inputDataEnd = $var["inputDateEnd"];
        $acuId = $var["acuId"];
        $inputComments = $var["inputComments"];
        $inputPlaceHistory = $var["inputPlaceHistory"];
        $inputOthers = $var["inputOthers"] != "" ? $var["inputOthers"] : $inputPlaceHistory;
        $inputFields = intval($var["inputFields"]);


        $now = $this->fmt->modules->dateFormat();

        $insert = "mod_adv_his_acu_id,mod_adv_his_user_id,mod_adv_his_fds_id,mod_adv_his_comments,mod_adv_his_date_registration,mod_adv_his_date_init,mod_adv_his_date_end,mod_adv_his_place,mod_adv_his_type,mod_adv_his_state";
        $values = "'" . $acuId . "','" . $userId . "','" . $inputFields . "','" . $inputComments . "','" . $now . "','" . $inputDataInit . "','" . $inputDataEnd . "','" . $inputOthers . "','1','1'";
        $sql = "insert into  mod_advised_history (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_adv_his_id) as id from  mod_advised_history";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        return $row["id"];
    }

    public function addHistory($vars = null)
    {
        //return $vars;
        $userId = $vars["userId"];
        $var = $vars["vars"];
        $calId = $var["inputCalId"];
        $order = $var["inputOrder"] ? $var["inputOrder"] : 0;

        $now = $this->fmt->modules->dateFormat();

        $insert = 'mod_adv_his_acu_id,mod_adv_his_user_id,mod_adv_his_fds_id,mod_adv_his_comments,mod_adv_his_date_init,mod_adv_his_place,mod_adv_his_state';
        $values  = "'" . $var["acuId"] . "','" . $userId . "','" . $var["inputFields"] . "','" . $var["inputComments"] . "','" . $now . "','" . $var["inputPlaceHistory"] . "','1'";
        $sql = "insert into mod_advised_history (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_adv_his_id) as id from mod_advised_history";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $historyId  = $row["id"];

        $insert = "mod_adv_his_cal_cal_id,mod_adv_his_cal_his_id,mod_adv_his_cal_order";
        $values = "'" . $calId . "','" . $historyId . "','" . $order . "'";
        $sql = "insert into  mod_advised_history_calendars (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        return $historyId;
    }

    public function updateHistoryEnd($vars = null)
    {
        $userId = $vars["userId"];

        $var = $vars["vars"];
        $hisId = $var["hisId"];
        $acuId = $var["acuId"];
        $inputField = $var["inputFields"];
        $inputComments = $var["inputComments"];
        $inputPlaceHistory = $var["inputPlaceHistory"];

        $now = $this->fmt->modules->dateFormat();

        $sql = "UPDATE mod_advised_history SET
           mod_adv_his_fds_id='" . $inputField . "',
           mod_adv_his_comments='" . $inputComments . "',
           mod_adv_his_place='" . $inputPlaceHistory . "',
           mod_adv_his_date_end='" . $now . "'
           WHERE mod_adv_his_acu_id= '" . $acuId . "' AND mod_adv_his_user_id='" . $userId . "' AND mod_adv_his_id='" . $hisId . "'";
        $this->fmt->querys->consult($sql);

        return 1;
    }

    public function updateHistoryRegister($vars = null)
    {
        $userId = $vars["userId"];

        $var = $vars["vars"];
        $hisId = $var["hisId"];
        $acuId = $var["acuId"];
        $inputFields = $var["inputFields"];
        $inputComments = $var["inputComments"];
        $inputPlaceHistory = $var["inputPlaceHistory"];

        $now = $this->fmt->modules->dateFormat();

        $sql = "UPDATE mod_advised_history SET
           mod_adv_his_fds_id='" . $inputFields . "',
           mod_adv_his_comments='" . $inputComments . "',
           mod_adv_his_place='" . $inputPlaceHistory . "',
           mod_adv_his_date_registration='" . $now . "'
           WHERE mod_adv_his_acu_id= '" . $acuId . "' AND mod_adv_his_user_id='" . $userId . "' AND mod_adv_his_id='" . $hisId . "'";
        $this->fmt->querys->consult($sql);

        $arrayHis = $this->dataHistoryId($hisId);

        $return["Error"] = 0;
        $return["field"] = $this->getName($arrayHis["mod_adv_his_fds_id"]);
        $return["dateInit"] = $arrayHis["mod_adv_his_date_init"];
        $return["dateEnd"] = $arrayHis["mod_adv_his_date_end"];
        $return["calendarRelation"] = $this->getCalendarRelationId($hisId);

        return $return;
    }

    public function deleteAdvised($vars = null)
    {
        $idMod = $this->idModAdvised;
        $id = $vars["vars"]['item'];

        $this->fmt->modules->deleteItem(array("from" => "mod_accounts_users", "column" => "mod_acu_id", "item" => $id));
        $this->fmt->modules->deleteItem(array("from" => "mod_advised_counselors", "column" => "mod_adv_cou_acu_id", "item" => $id));

        $advisedId = $this->dataAccountIdToAdvisedId($id);
        $this->fmt->modules->deleteItem(array("from" => "mod_advised_user", "column" => "mod_adv_id", "item" => $advisedId));
        $this->fmt->modules->deleteItem(array("from" => "mod_accounts_services", "column" => "mod_acu_sv_acu_id", "item" => $id, "query" => " AND mod_acu_sv_sv_id='" . $this->idService . "'"));

        return $id;
    }

    public function deleteRelationCounselor($acuId = null)
    {
        $this->fmt->modules->deleteRelation(array("from" => "mod_advised_counselors", "column" => "mod_adv_cou_acu_id", "item" => $acuId));
    }

    public function getCalendarRelationId($hisId = null)
    {
        $sql = "SELECT * FROM mod_advised_history_calendars WHERE mod_adv_his_cal_his_id='" . $hisId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $return = $row["mod_adv_his_cal_cal_id"];
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function getValue($name)
    {
        $sql = "SELECT mod_adv_opt_value FROM mod_advised_options WHERE mod_adv_opt_name='" . $name . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        return $row["mod_adv_opt_value"];
    }

    public function getName($id)
    {
        $sql = "SELECT mod_adv_opt_name FROM mod_advised_options WHERE mod_adv_opt_id='" . $id . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        return $row["mod_adv_opt_name"];
    }

    public function listFields()
    {
        $sql = "SELECT * FROM mod_advised_options  WHERE mod_adv_opt_value='fields' AND mod_adv_opt_autoload='yes'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return["$i"]["id"] = $row["mod_adv_opt_id"];
                $return["$i"]["name"] = $row["mod_adv_opt_name"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function modifyHours(array $var = null)
    {
        //return $var;
        $userId = $var["user_id"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];

        $couId = $vars["item"];
        $count = $vars["count"];
        $day = $vars["day"];
        $hourStart = $vars["hourStart"];
        $hourEnd = $vars["hourEnd"];
        //return $this->haveHourSchedules($var);
        if ($this->haveHourSchedules($var)) {
            $sql = "UPDATE mod_advised_schedules SET
                    mod_adv_sch_hour_start  ='" . $hourStart . "',
                    mod_adv_sch_hour_end ='" . $hourEnd . "'
                    WHERE mod_adv_sch_cou_id = '" . $couId . "' AND  mod_adv_sch_day='" . $day . "' AND mod_adv_sch_count='" . $count . "'";
            $this->fmt->querys->consult($sql);
            return 1;
        } else {
            $insert = "mod_adv_sch_cou_id,mod_adv_sch_day,mod_adv_sch_hour_start,mod_adv_sch_hour_end,mod_adv_sch_count,mod_adv_sch_state";
            $values = "'" . $couId . "','" . $day . "','" . $hourStart . "','" . $hourEnd . "','" . $count . "','1'";
            $sql = "insert into mod_advised_schedules (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);

            return 1;
        }
    }

    public function deteleHours(array $var = null)
    {
        //return $var;

        $userId = $var["user_id"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];

        $couId = $vars["id"];
        $count = $vars["count"];
        $day = $vars["day"];

        $sql = "DELETE FROM mod_advised_schedules WHERE mod_adv_sch_cou_id = '" . $couId . "' AND  mod_adv_sch_day='" . $day . "' AND mod_adv_sch_count='" . $count . "'";
        $this->fmt->querys->consult($sql);
        $up_sqr6 = "ALTER TABLE mod_advised_schedules AUTO_INCREMENT=1";
        $this->fmt->querys->consult($up_sqr6, __METHOD__);

        return 1;
    }

    public function haveHourSchedules(array $var = null)
    {

        $userId = $var["user_id"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];

        $couId = $vars["item"];
        $count = $vars["count"];
        $day = $vars["day"];

        $sql = "SELECT * FROM mod_advised_schedules WHERE mod_adv_sch_cou_id = '" . $couId . "' AND  mod_adv_sch_day='" . $day . "' AND mod_adv_sch_count='" . $count . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function loadHoursCounselors(array $var = null)
    {
        $sql = "SELECT * FROM mod_advised_schedules WHERE mod_adv_sch_state > 0";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_adv_sch_cou_id"];
                $return[$i]["id"] = $id;
                $return[$i]["hourStart"] = substr($row["mod_adv_sch_hour_start"], 0, -3);
                $return[$i]["hourEnd"] = substr($row["mod_adv_sch_hour_end"], 0, -3);
                $return[$i]["count"] = $row["mod_adv_sch_count"];
                $return[$i]["day"] = $row["mod_adv_sch_day"];
                $return[$i]["state"] = $row["mod_adv_sch_state"];
            }
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function dashboard(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $user = $var["user"];
        $vars = $var["vars"];
        $rolId = $user["rolId"];
        $userId = $user["userId"];

        $rolIdJefeConsejeros = $this->getValue("rolIdJefeConsejeros");
        $rolIdConsejeros = $this->getValue("rolIdConsejeros");
        $rolIdSecretariaConsejeria = $this->getValue("rolIdSecretaria");

        if ($rolId == "1" || $rolId == $rolIdJefeConsejeros || $rolId == $rolIdSecretariaConsejeria) {
            $list = $this->listCounselors($var);
            //$numList = count(list);

            $today =  $this->fmt->modules->dateFormate(["format" => "Y-m-d"]);
            $last = date("Y-m-d", strtotime($today . "- 10 days"));

            foreach ($list as $key => $value) {
                $rtn["general"]["counselor"][$key]["id"] = $value["id"];
                $rtn["general"]["counselor"][$key]["name"] = $value["name"];
                $rtn["general"]["counselor"][$key]["lastname"] = $value["lastname"];
                $rtn["general"]["counselor"][$key]["active"] = $value["active"];
                $rtn["general"]["counselor"][$key]["dailyAttendance"] = $this->dailyAttendance;
                $rtn["general"]["counselor"][$key]["todayAppointments"] = $this->calendarAdvised->reportAppointmentsCounselor(["couId" => $value["id"], "dateInit" => $today . ' 00:00:00', "dateEnd" => $today . " 23:59:59", "type" => "appointment"]);
                $rtn["general"]["counselor"][$key]["todayAppointmentsAttended"] = $this->calendarAdvised->reportAppointmentsCounselor(["couId" => $value["id"], "dateInit" => $today . ' 00:00:00', "dateEnd" => $today . " 23:59:59", "type" => "appointment", "state" => "4"]); //4 atendido
                $rtn["general"]["counselor"][$key]["lastAppointments"] = $this->calendarAdvised->reportAppointmentsCounselor(["couId" => $value["id"], "dateInit" => $last, "dateEnd" => $today . " 23:59:59", "type" => "appointment"]);
            }

            $rtn["general"]["report"]["todayReport"] = $this->calendarAdvised->reportAppointment(["dateInit" => $today . ' 00:00:00', "dateEnd" => $today . " 23:59:59", "type" => "appointment"]);
            $rtn["general"]["report"]["lastReport"] = $this->calendarAdvised->reportAppointment(["dateInit" => $last, "dateEnd" => $today . " 23:59:59", "type" => "appointment"]);

            return $rtn;
        }

        if ($rolId == $rolIdSecretariaConsejeria) {
            return "rol secretaria";
        }

        if ($rolId == $rolIdConsejeros) {
            //return "rol consejero";

            $today =  $this->fmt->modules->dateFormate(["format" => "Y-m-d"]);
            $last = date("Y-m-d", strtotime($today . "- 10 days"));

            $todayReport = $this->calendarAdvised->reportAppointmentsCounselor(["couId" => $userId, "dateInit" => $today, "dateEnd" => $today, "type" => "appointment"]);
            $lastReport = $this->calendarAdvised->reportAppointmentsCounselor(["couId" => $userId, "dateInit" => $last, "dateEnd" => $today . " 23:59:59", "type" => "appointment"]);

            $rtnDataToday['num'] = count($todayReport);
            $rtnDataLast['num'] = count($lastReport);

            $rtn["general"]["counselor"][0]["id"]  = $userId;
            $rtn["general"]["counselor"][0]["name"]  = $user["userName"];
            $rtn["general"]["counselor"][0]["lastname"]  = $user["userLastname"];
            $rtn["general"]["counselor"][0]["active"]  = 1;
            $rtn["general"]["counselor"][0]["todayAppointments"]  = $todayReport;
            $rtn["general"]["counselor"][0]["lastAppointments"]  = $lastReport;
            $rtn["general"]["counselor"][0]["dailyAttendance"]  = $this->dailyAttendance;
            $rtn["general"]["report"]["todayReport"] = $rtnDataToday;
            $rtn["general"]["report"]["lastReport"] =  $rtnDataLast;

            return $rtn;
        }

        return 0;
    }

    //Reports
    public function getReportsCounseling(array $var = null)
    {
        //return $var;
        $inputs = $var["vars"]["inputs"];
        $dateInit = $inputs["dateInit"];
        $dateEnd = $inputs["dateEnd"];
        $couId = $inputs["couId"];
        $rolId = $var["user"]["rolId"];
        $type = $inputs["type"];

        $arrayCounselor =  $this->listCounselors(["entitieId" => 1]);

        if ($rolId == 1 || $rolId == 7 || $rolId == 5 || $rolId == 6) {

            if ($type == "report-general") {
                if ($couId == 0) {
                    foreach ($arrayCounselor as $key => $value) {
                        $couId =  $value["id"];
                        $canArray = $this->cantidadConsejerias(["dateInit" => $dateInit, "dateEnd" => $dateEnd, "couId" => $couId]);
                        //$arrayCounselor[$key]["cantidadConsejerias"] = $canArray;
                        $arrayCounselor[$key]["cantidadConsejerias"] = $canArray['num'];
                        $arrayCounselor[$key]["datosConsejerias"] = $canArray['data'];
                        $arrayCounselor[$key]["cantidadPersonas"] = $this->cantidadPersonasAtendidas(["dateInit" => $dateInit, "dateEnd" => $dateEnd, "couId" => $couId]);
                        $arrayCounselor[$key]["cantidadCumplimiento"] = $this->cantidadCumplientoDeAtencion(["dateInit" => $dateInit, "dateEnd" => $dateEnd, "couId" => $couId]);
                    }
                    return $arrayCounselor;
                }

                if ($couId != 0) {
                    $return = [];
                    $arrayData = $this->fmt->users->getDataId($couId);
                    $return[0]["id"] = $couId; // with name
                    $return[0]["name"] = $arrayData['name']; // with name
                    $return[0]["lastname"] = $arrayData['lastname'];
                    $canArray = $this->cantidadConsejerias(["dateInit" => $dateInit, "dateEnd" => $dateEnd, "couId" => $couId]);
                    $return[0]["cantidadConsejerias"] = $canArray['num'];
                    //$return[0]["cantidadConsejerias"] = $canArray;
                    $return[0]["datosConsejerias"] = $canArray['data'];
                    $return[0]["cantidadPersonas"] = $this->cantidadPersonasAtendidas(["dateInit" => $dateInit, "dateEnd" => $dateEnd, "couId" => $couId]);
                    $return[0]["cantidadCumplimiento"] = $this->cantidadCumplientoDeAtencion(["dateInit" => $dateInit, "dateEnd" => $dateEnd, "couId" => $couId]);
                    return $return;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function cantidadConsejerias(array $var = null)
    {
        $dateInit = $var["dateInit"];
        $dateEnd = $var["dateEnd"];
        $couId = $var["couId"];

        $dateInit = $dateInit . " 00:00:00";
        $dateEnd = $dateEnd . " 23:59:59";

        $sql = "SELECT DISTINCT mod_adv_cal_id FROM mod_advised_calendars WHERE mod_adv_cal_date_start >= '" . $dateInit . "' AND  mod_adv_cal_date_end <= '" . $dateEnd . "' AND mod_adv_cal_cou_id = '" . $couId . "' AND mod_adv_cal_acu_id != 330  AND mod_adv_cal_acu_id != 465 AND mod_adv_cal_state in (1) ORDER BY  mod_adv_cal_date_start ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        $this->fmt->querys->leave($rs);

        $sql1 = "SELECT date(mod_adv_cal_date_start) AS fecha, COUNT(*) AS cantidad FROM mod_advised_calendars WHERE mod_adv_cal_date_start >= '" . $dateInit . "' AND mod_adv_cal_date_end <= '" . $dateEnd . "' AND mod_adv_cal_cou_id = '" . $couId . "' AND mod_adv_cal_acu_id != 330  AND mod_adv_cal_acu_id != 465 AND mod_adv_cal_state in (1) GROUP BY fecha ORDER BY mod_adv_cal_date_start ASC";
        $rs1 = $this->fmt->querys->consult($sql1, __METHOD__);
        $num1 = $this->fmt->querys->num($rs1);
        $rtn = [];
        if ($num1 > 0) {
            for ($i = 0; $i < $num1; $i++) {
                $row1 = $this->fmt->querys->row($rs1);
                $rtn[$i]["fecha"] = $row1["fecha"];
                $rtn[$i]["citas dia"] = $row1["cantidad"];
            }
        } else {
            $rtn = 0;
        }
        $this->fmt->querys->leave($rs1);

        $return["num"] = $num;
        $return["data"] = $rtn;
        return $return;
    }

    public function cantidadConsejeriasDia(array $var = null)
    {
        $date = $var["date"];
        $couId = $var["couId"];

        $sql = "SELECT DISTINCT mod_adv_cal_id FROM mod_advised_calendars WHERE mod_adv_cal_date_start >= '" . $date . " 00:00' AND  mod_adv_cal_date_end <= '" . $date . " 23:59' AND mod_adv_cal_cou_id = '" . $couId . "' AND mod_adv_cal_acu_id != 330 AND mod_adv_cal_acu_id != 465 AND mod_adv_cal_state in (1) ORDER BY  mod_adv_cal_date_start ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        return $num = $this->fmt->querys->num($rs);
        $this->fmt->querys->leave($rs);
    }

    public function cantidadPersonasAtendidas(array $var = null)
    {
        $dateInit = $var["dateInit"];
        $dateEnd = $var["dateEnd"];
        $couId = $var["couId"];

        $dateInit = $dateInit . " 00:00:00";
        $dateEnd = $dateEnd . " 23:59:59";

        $sql = "SELECT DISTINCT mod_adv_cal_acu_id FROM mod_advised_calendars WHERE mod_adv_cal_date_start >= '" . $dateInit . "' AND  mod_adv_cal_date_end <= '" . $dateEnd . "' AND mod_adv_cal_cou_id = '" . $couId . "'  AND mod_adv_cal_acu_id != 330 AND mod_adv_cal_acu_id != 465 AND mod_adv_cal_state in (1) ORDER BY  mod_adv_cal_date_start ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        return $num = $this->fmt->querys->num($rs);
        $this->fmt->querys->leave($rs);
    }

    public function cantidadCumplientoDeAtencion(array $var = null)
    {
        $dateInit = $var["dateInit"];
        $dateEnd = $var["dateEnd"];
        $couId = $var["couId"];

        $dateInit = $dateInit . " 00:00:00";
        $dateEnd = $dateEnd . " 23:59:59";

        $sql = "SELECT mod_adv_cal_id,mod_adv_cal_date_start FROM mod_advised_calendars WHERE mod_adv_cal_date_start >= '" . $dateInit . "' AND  mod_adv_cal_date_end <= '" . $dateEnd . "' AND mod_adv_cal_cou_id = '" . $couId . "' AND mod_adv_cal_acu_id != 330 AND mod_adv_cal_acu_id != 465 AND mod_adv_cal_state in (1) ORDER BY  mod_adv_cal_date_start ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $date = explode(" ", $row["mod_adv_cal_date_start"]);
                //$return[$i]["id"] = $row["mod_adv_cal_id"];
                $arr[$i]  = $date[0];
            }
            //return  array_unique($arr);
            $arrayDias = array_unique($arr);
            $countDias = count($arrayDias);
            $numDay = [];
            $i = 0;
            foreach ($arrayDias as $key => $value) {
                $dateDay = $value;
                $numDay[$i][$dateDay] = $this->cantidadConsejeriasDia(["date" => $dateDay, "couId" => $couId]);
                $i++;
            }

            //promedio array
            $sum = 0;
            foreach ($numDay as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    $sum = $sum + $value2;
                }
            }
            $promedio = $sum / $countDias;
            $return = round($promedio) * 100 / 8;
            return $return . "%";
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }
}