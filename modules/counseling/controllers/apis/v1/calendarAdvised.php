<?php

$access = json_decode($_POST["accessToken"], true);
require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $access["page"] . "config.php");
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

//echo json_encode("access:".$access); exit(0);


require_once(_PATH_NUCLEO . "modules/counseling/models/class/class.counseling.php");
$couseling = new COUNSELING($fmt);

require_once(_PATH_NUCLEO . "modules/counseling/models/class/class.calendarAdvised.php");
$calendar = new CALENDARADVISED($fmt);

header("Content-Type: application/json");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


switch ($_SERVER["REQUEST_METHOD"]) {
    case 'POST':
        $access_token = $access['access_token'];
        $refresh_token = $access['refresh_token'];
        $entitieId = $access['entitieId'];
        $action = $fmt->auth->getInput('action');

        
        $fmt->jointActions(['access'=>$access]);

        //echo json_encode("1:". $action); exit(0);
        //echo json_encode($vs); exit(0);

        $userId = $fmt->users->validateUserToken(array("access_token" => $access_token, "refresh_token" => $refresh_token, "idEntitie" => $entitieId));

        //echo json_encode($entitieId); exit(0);
        //echo json_encode($refresh_token); exit(0);
        //echo json_encode($userId); exit(0);

        if ($userId) {
            //echo "'usario validado";
            //echo json_encode("user: " . $userId . ": action: " . $action);
            //exit(0);

            $return["user"] = $fmt->users->userDataAuth(array("access_token" => $access_token, "idEntitie" => $entitieId));
            $userId = $return["user"]["userId"];
            $rolId = $return["user"]["rolId"];

            if ($action == "modifyHours") {
                $vars = json_decode($_POST['vars'], true);
                //echo json_encode($vars); exit(0);
                $state =  $couseling->modifyHours(array(
                    "userId" => $userId,
                    "vars" => $vars,
                    "entitieId" => $entitieId
                ));
                echo json_encode($state);
                exit(0);
            }
            if ($action == "deteleHours") {
                $vars = json_decode($_POST['vars'], true);
                //echo json_encode($vars); exit(0);
                $state =  $couseling->deteleHours(array(
                    "userId" => $userId,
                    "vars" => $vars,
                    "entitieId" => $entitieId
                ));
                if ($state == 1) {
                    $rtr["Error"] = 0;
                    echo json_encode($rtr);
                    exit(0);
                } else {
                    $rtr["Error"] = 1;
                    $rtr["message"] =  $state;
                    echo json_encode($rtr);
                    exit(0);
                }
            }

            if ($action == "loadHoursCounselors") {
                $vars = json_decode($_POST['vars'], true);
                $state =  $couseling->loadHoursCounselors(array(
                    "userId" => $userId,
                    "vars" => $vars,
                    "entitieId" => $entitieId
                ));
                echo json_encode($state);
                exit(0);
            }
            if ($action == "addSchedule") {
                $vars = $fmt->forms->serializeToArray(json_decode($_POST['vars'], true));
                $state =  $calendar->addSchedule(array(
                    "userId" => $userId,
                    "vars" => $vars,
                    "entitieId" => $entitieId,
                    "type" => $_POST['type']
                ));

                echo json_encode($state);


                exit(0);
            }

            if ($action == "schedulesCounselors") {
                $vars = json_decode($_POST['vars'], true);
                $state =  $calendar->schedulesCounselors(array(
                    "userId" => $userId,
                    "vars" => $vars,
                    "entitieId" => $entitieId
                ));
                //echo json_encode($state); 
                if ($state != 0) {
                    $rtn["Error"] = 0;
                    $rtn["data"] = $state;
                    echo json_encode($rtn);
                } else {
                    echo json_encode(0);
                }

                exit(0);
            }

            if ($action == "changeStateItem") {
                $vars = json_decode($_POST['vars'], true);
                //echo json_encode($vars); exit(0);
                $state =  $calendar->changeStateItem(array(
                    "userId" => $userId,
                    "vars" => $vars,
                    "entitieId" => $entitieId
                ));
                echo json_encode($state);
                exit(0);

                if ($state == 1) {
                    $rtn["Error"] = 0;
                    echo json_encode($rtn);
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Access Auth. invalid user",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
            }

            if ($action == "deleteSchedule") {
                $vars = json_decode($_POST['vars'], true);
                $state =  $calendar->deleteShedule(array(
                    "userId" => $userId,
                    "vars" => $vars,
                    "entitieId" => $entitieId
                ));
                //echo json_encode($state); exit(0);

                if ($state == 1) {
                    $rtn["Error"] = 0;
                    echo json_encode($rtn);
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Access Auth. invalid user",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
            }
            if ($action == "stateCancelShedule") {
                $vars = json_decode($_POST['vars'], true);
                $state =  $calendar->stateCancelShedule(array(
                    "userId" => $userId,
                    "vars" => $vars,
                    "entitieId" => $entitieId
                ));
                //echo json_encode($state); exit(0);

                if ($state == 1) {
                    $rtn["Error"] = 0;
                    echo json_encode($rtn);
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Access Auth. invalid user",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
            }



            if ($action == "reshedule") {
                $vars = json_decode($_POST['vars'], true);
                $state =  $calendar->reshedule(array(
                    "userId" => $userId,
                    "vars" => $vars,
                    "entitieId" => $entitieId
                ));

                echo json_encode($state); exit(0);

                /* if ($state == 1) {
                    $rtn["Error"] = 0;
                    $rtn["status"] = 'success';
                     
                    echo json_encode($rtn);
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Error return ".$state,
                        "code" => "",
                        "lang" => "es"
                    ]);
                } */
            }

            if ($action == "saveHoursBlocked") {
                $vars = json_decode($_POST['vars'], true);
                $state =  $calendar->saveHoursBlocked(array(
                    "userId" => $userId,
                    "vars" => $vars,
                    "entitieId" => $entitieId
                ));
                //echo json_encode($state); exit(0);
                if ($state) {
                    $rtn["Error"] = 0;
                    $rtn["state"] = $state;
                    echo json_encode($rtn);
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Access Auth. invalid user",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
            }

            if ($action == "loadDaysBlocked") {
                $vars = json_decode($_POST['vars'], true);
                $state =  $calendar->loadDaysBlocked(array(
                    "userId" => $userId,
                    "vars" => $vars,
                    "entitieId" => $entitieId
                ));
                //echo json_encode($state); exit(0);
                if ($state != 0) {
                    $rtn["Error"] = 0;
                    $rtn["state"] = $state;
                    $rtn["items"] = $state;
                    echo json_encode($rtn);
                } else if ($state == 0) {
                    echo json_encode(0);
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Access Auth. invalid user",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
            }
            if ($action == "deleteDateBloked") {
                $vars = json_decode($_POST['vars'], true);
                $state = $calendar->deleteDateBloked(array(
                    "userId" => $userId,
                    "vars" => $vars,
                    "entitieId" => $entitieId
                ));
                //echo json_encode($state); exit(0);
                if ($state == 1) {
                    $rtn["Error"] = 0;
                    $rtn["state"] = 1;
                    echo json_encode($rtn);
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Access Auth. invalid user",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
            }
            if ($action == "updateHoursBlocked") {
                $vars = json_decode($_POST['vars'], true);
                $state = $calendar->updateHoursBlocked(array(
                    "userId" => $userId,
                    "vars" => $vars,
                    "entitieId" => $entitieId
                ));
                //echo json_encode($state);
                //exit(0);
                if ($state != 0) {
                    $rtn["Error"] = 0;
                    $rtn["state"] = $state;
                    echo json_encode($rtn);
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Access Auth. invalid user",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
            }
        } else { //if $userId    
            echo $fmt->errors->errorJson([
                "description" => "Access Auth. invalid user",
                "code" => "",
                "lang" => "es"
            ]);
            exit(0);
        }
        break;

    default:
        echo $fmt->errors->errorJson([
            "description" => "Access Auth. Metod request.",
            "code" => "",
            "lang" => "es"
        ]);
        exit(0);
        break;
}