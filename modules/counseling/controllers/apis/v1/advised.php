<?php

header("Content-Type: application/json");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if (!empty($_POST["page"])) {
    $dataPost = 0;
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $_POST["page"] . "config.php");
}else{
    $dataPost = json_decode(file_get_contents("php://input"), true);//
    //echo json_encode($dataPost); exit(0); 
    //echo json_encode($dataPost["accessToken"]["access_token"]); exit(0);
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $dataPost["accessToken"]["page"] . "config.php");
}


require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();
$pathMod = "modules/counseling/";

require_once(_PATH_NUCLEO ."modules/counseling/models/class/class.counseling.php");
$counseling = new COUNSELING($fmt); 


switch ($_SERVER["REQUEST_METHOD"]) {
    case 'POST':
        if ($dataPost==0){
            $access_token = $fmt->auth->getInput('access_token');
            $refresh_token = $fmt->auth->getInput('refresh_token');
            $entitieId = $fmt->auth->getInput('entitieId');
            $actions = $fmt->auth->getInput('actions');
            $userId = $fmt->users->validateUserToken(array("access_token" => $access_token, "refresh_token" => $refresh_token, "idEntitie" => $entitieId));

            $return["user"] = $fmt->users->userDataAuth(array("access_token" => $access_token, "idEntitie" => $entitieId));
            $userId = $return["user"]["userId"];
            $rolId = $return["user"]["rolId"];
            //echo json_encode("user: ".$userId); exit(0);


            if ($userId) {
                if ($actions == "loadAdvised") {
                    //echo json_encode( $userId.":".$rolId ); exit(0);
                    //echo json_encode(["a", "b", "c"]);exit(0);

                    $users = $counseling->listAdvised(array("userId" => $userId, "rolId" => $rolId, "entitieId" => $entitieId));

                    if (!empty($users) && $users != 0) {
                        echo json_encode($users);
                        exit(0);
                    }
                    if ($users == 0) {
                        echo json_encode(0);
                    } else {
                        echo $fmt->errors->errorJson([
                            "description" => "Error create User",
                            "code" => "",
                            "lang" => "es"
                        ]);
                    }
                }

                if ($actions == "saveNewAdvised") {

                    //$vars['name'] =  $_POST['name'];
                    //echo  json_encode($_POST['vars']); exit(0);
                    $vars = json_decode($_POST['vars'], true);
                    //echo $vars["ProfessionSpouse"];

                    echo json_encode($counseling->saveAdvised(array("userId" => $userId, "rolId" => $rolId, "entitieId" => $entitieId, "vars" => $vars)));
                    exit(0);
                }

                if ($actions == "editAdvisedId") {

                    //echo  json_encode($_POST['item']); exit(0);
                    echo json_encode($counseling->dataUserAdvisedId($_POST['item']));
                    exit(0);
                }

                if ($actions == "modifiedAdvised") {
                    $vars = json_decode($_POST['vars'], true);

                    $advisedId =  $counseling->modifiedAdvised(array("userId" => $userId, "vars" => $vars));

                    if (is_numeric($advisedId)) {
                        $return["user"] = "";
                        $return["Error"] = 0;
                        $return["advisedId"] = $advisedId;
                        echo json_encode($return);
                    } else {
                        echo $fmt->errors->errorJson([
                            "description" => "Error update User" . $advisedId,
                            "code" => $advisedId,
                            "lang" => "es"
                        ]);
                    }
                }

                if ($actions == "deleteAdvised") {

                    //$vars['name'] =  $_POST['name'];
                    //echo  json_encode($_POST['vars']); exit(0);
                    $vars = json_decode($_POST['vars'], true);
                    echo json_encode($counseling->deleteAdvised(array("userId" => $userId, "rolId" => $rolId, "entitieId" => $entitieId, "vars" => $vars)));
                    exit(0);
                }

                if ($actions == "historyAdvised") {
                    //echo json_encode(1);
                    $vars = json_decode($_POST['vars'], true);
                    $item = $vars["item"];
                    echo json_encode($counseling->historyAdvised(array("acuId" => $item)));
                    exit(0);
                }

                if ($actions == "addHistory") {
                    $vars = json_decode($_POST['vars'], true);

                    $historyId = json_encode($counseling->addHistory(array("userId" => $userId, "vars" => $vars)));

                    if ($historyId) {
                        $return["user"] = "";
                        $return["Error"] = 0;
                        $return["historyId"] = $historyId;
                        echo json_encode($return);
                    } else {
                        echo $fmt->errors->errorJson([
                            "description" => "Error create User",
                            "code" => "",
                            "lang" => "es"
                        ]);
                    }
                }

                if ($actions == "updateHistoryEnd") {
                    $vars = json_decode($_POST['vars'], true);

                    $response = json_encode($counseling->updateHistoryEnd(array("userId" => $userId, "vars" => $vars)));

                    if ($response == 1) {
                        $return["user"] = "";
                        $return["Error"] = 0;
                        echo json_encode($return);
                    } else {
                        echo $fmt->errors->errorJson([
                            "description" => "Error create User",
                            "code" => "",
                            "lang" => "es"
                        ]);
                    }
                }

                if ($actions == "updateHistoryRegister") {
                    $vars = json_decode($_POST['vars'], true);

                    $response =  $counseling->updateHistoryRegister(array("userId" => $userId, "vars" => $vars));
                    //echo json_encode($response); exit(0);

                    if ($response["Error"] == "0") {
                        echo json_encode($response);
                        exit(0);
                    } else {
                        echo $fmt->errors->errorJson([
                            "description" => "Error create History",
                            "code" => "",
                            "lang" => "es"
                        ]);
                    }
                }

            }

        }else{
            $access_token = $dataPost["accessToken"]['access_token'];
            $refresh_token = $dataPost["accessToken"]['refresh_token'];
            $entitieId = $dataPost["accessToken"]['entitieId'];
            $action = $dataPost["action"];
            //echo json_encode("1:". $action); exit(0);
            //echo json_encode("2:". $entitieId); exit(0);

            $userId = $fmt->users->validateUserToken(array("access_token" => $access_token, "refresh_token" => $refresh_token, "idEntitie" => $entitieId));
            //echo json_encode("user: ".$userId); exit(0);

            if (!$userId) {
                errorDefault($fmt, "Error: Token invalid");
            }


            if ($userId){
                $return["user"] = $fmt->users->userDataAuth(array("access_token" => $access_token, "idEntitie" => $entitieId));
                $return["vars"] = json_decode($dataPost['vars'], true);
                $return["entitieId"] = $entitieId;
                $userId = $return["user"]["userId"];
                $rolId = $return["user"]["rolId"];
                $task = $return["vars"]["task"];
                $rtn = $return["vars"]["return"];

               //echo json_encode($return["user"]["rolId"]); exit(0);

                if ($action == "loadCounselors") {
        
                   $response = $counseling->listCounselors(array("userId" => $userId,"rolId" => $rolId, "entitieId" => $entitieId, "vars" => $return["vars"] ));
    
                    if ($response != 0){
                        $rx["Error"] = 0;
                        $rx["status"] = 'success';
                        $rx["data"] = $response;
                        echo json_encode($rx); exit(0);
                    }else{
                        $rtn["Error"] = 1;
                        $rtn["status"] = 'error';
                        $rtn["data"] = 0;
                        $rtn["message"] = $response;
                        echo json_encode($rtn); 
                    }
                    exit(0);
                }
    
            }
        }

        
    break;

    default:
        echo $fmt->errors->errorJson([
            "description" => "Access Auth. Metod request.",
            "code" => "",
            "lang" => "es"
        ]);
        break;
}