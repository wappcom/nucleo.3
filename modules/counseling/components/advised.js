import {
    listJsonToString,
    btnFn,
    loadView,
    replaceAll,
    replacePath,
    stopLoadingBar,
    loadBtnLoading,
    loadingBtnIcon,
    removeBtnLoading,
    unDisableId,
    removeInnerForm,
    alertMessageError,
    addHtml,
    capitalize,
    accessToken
} from "../../components/functions.js"
import {
    createTable,
    dataTable
} from "../../components/tables.js"
import {
    validateEmail
} from "../../components/forms.js"
import {
    deleteModalItem,
    deleteModal,
    removeModal
} from "../../components/modals.js"
import {
    innerForm,
    renderDeleteModal,
    renderModal
} from "../../components/renders/renderModals.js"

import {
    formHistoryAdvised
} from "./historyAdvised.js";
export {
    formHistoryAdvised
}



import { renderFormNewAdviserExpress } from './renders/renderCalendarAdvised.js';
import { addAccountList } from './calendarAdvised.js';

var rootApi = "modules/counseling/controllers/apis/v1/advised.php";
let numMod = 0;
let tableId = "tableAdvised";


export function advisedIndex() {
    const system = "counseling"
    const module = "advised"
    document.cookie = "promo_shown=1; Max-Age=2600000; Secure"
    //console.log("advisedIndex");

    addHtml({
        selector: `.menuTopMedium`,
        type: 'insert',
        content: ` `
    })
    addHtml({
        selector: `.menuTopRight .left`,
        type: 'insert',
        content: ` `
    })

    addHtml({
        selector: `.menuTopRight .right`,
        type: 'insert',
        content: ` `
    })

    loadDashboardAdvised().then((jsonData) => {
        //console.log("loadDashboardAdvised", jsonData);
        let str = ""
        let tbody = ""
        let usersLength = 0
        if (jsonData.users != 0) {
            usersLength = jsonData.users.length
        }

        for (let i = 0; i < usersLength; i++) {
            const element = jsonData.users[i]
            const name = element.name + " " + element.fathersLastname
            const field = element.fieldInitial

            tbody += "<tr>"
            tbody += "    <td class='colId'>" + element.id + "</td>"
            tbody += "    <td class='colAp'>" + element.fathersLastname + "</td>"
            tbody += "    <td class='colAm'>" + element.mothersLastname + "</td>"
            tbody += "    <td class='colName'>" + element.name + "</td>"
            tbody += "    <td class='colEmail'>" + element.email + "</td>"
            tbody += "    <td class='colCelular'>" + element.celular + "</td>"
            tbody += "    <td class='colCI'>" + element.ci + " " + element.ext + "</td>"
            tbody +=
                "    <td class='colConsejeros'>" +
                listJsonToString(element.counselors, "name") +
                "</td>"
            tbody += `<td class="actions">
                <div class="btns" data-id="${element.id}">`
            if (jsonData.canEditHistory != 0) {
                tbody += btnFn({
                    btnType: "btnHistory",
                    clss: "btnAddForm",
                    item: element.id,
                    module: module,
                    system: system,
                    fn: "formHistoryAdvised",
                    vars: name + "," + field,
                })
            }

            tbody += btnFn({
                btnType: "btnEdit",
                clss: "btnAddForm",
                item: element.id,
                module: module,
                system: system,
                fn: "formEditAdvised",
                vars: name + "",
            })
            tbody += btnFn({
                btnType: "",
                item: element.id,
                module: module,
                system: "",
                icon: "icon icon-trash",
                cls: "deleteFormAdvised",
                attr: `data-name="${name}" data-id="${element.id}"`,
            })

            tbody += `    </div>
                <a class="btnActionMobile" state="0" data-id="${element.id}" > 
                    <i class="icon icon-circle-plus"></i>
                </a> 
            </td>`;
            tbody += "</tr>"
        }

        str = createTable({
            id: "tableAdvised",
            thead: "id:col-id,Ap. Paterno:colAp,Ap. Materno:colAm,Name:colName,Email:colEmail,Telefono:colTelf,CI:colCI,Consejero:colConsejeros,Acciones:actions",
            body: tbody,
        })

        loadView(
            _PATH_WEB_NUCLEO + "modules/counseling/views/advised.html?" + _VS
        ).then((advisedHtml) => {
            clearInterval(displayLoadingBar)
            $(".bodyModule").html("")
            let strHtml = advisedHtml
            strHtml = replaceAll(strHtml, "{{_PATHURL}}", "modules/counseling/")
            strHtml = replaceAll(strHtml, "{{_MODULE}}", "advised")
            strHtml = replaceAll(strHtml, "{{_SYSTEM}}", "counseling")
            strHtml = replaceAll(strHtml, "{{_FN}}", "formNewAdvised")

            $(".bodyModule[module='advised']").html(strHtml)
            $(".bodyModule[module='advised'] .tbody").html(str)

            if (jsonData.canCreateUser == 0) {
                $("#addNewAdvised").remove()
            }

            dataTable({
                elem: "#tableAdvised"
            })
        })
    })

    //btns
    $("body").on("click", ".btnSaveAdvised", function (e) {
        //console.log("btnSaveAdvised")
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        //loadBtnLoading(e)

        saveAdvised(e).then((data) => {
            //console.log("saveAdvised", data);
            if (data != undefined) {
                if (data.Error == 0) {
                    removeInnerForm();
                    advisedIndex();
                } else {
                    alertMessageError({
                        title: 'Error',
                        message: data.message
                    });
                }
            }
        })
    })

    $("body").on("click", ".deleteFormAdvised", function (e) {
        //console.log("btnDeleteAdvised")
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        let data = $(this).data();
        let id = data.id;
        let name = data.name;
        /* let item = $(this).attr("item")
        let module = $(this).attr("module")
        let fn = $(this).attr("fn")
        let vari = $(this).attr("vars")
        let variArray = vari.split(",") */

        /*  deleteModalItem({
             name: variArray[0],
             item: item,
             attr: `item = ${item}`,
             classBtnAction: 'btnDeleteFormAdvisedConfirm',
             fn: fn
         }) */

        deleteModal({
            id: "deleteModal",
            name,
            attr: `data-name="${name}" data-fn="deleteAdvised" data-id="${id}"`,
        })
    })

    $("body").on(
        "click",
        '.btn[data-fn="deleteAdvised"]',
        function (e) {
            e.preventDefault()
            e.stopPropagation()
            e.stopImmediatePropagation()
            /* Act on the event */
            let item = $(this).attr("item")
            let fn = $(this).attr("fn")

            let data = $(this).data();
            let id = data.id;
            //console.log(".btnDeleteConfirm[fn='deleteFormAdvised' ]", item)

            deleteAdvised({
                item: id
            }).then((data) => {
                if (data) {
                    //console.log(data)
                    removeModal()
                    advisedIndex()
                }
            })
        }
    )

    $("body").on("keyup", "#inputSearchCounselor", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()

        var rex = new RegExp($(this).val(), "i")
        $("#listConunselors div").hide()
        $("#listConunselors div")
            .filter(function () {
                return rex.test($(this).text())
            })
            .show()
    })

    $("body").on("click", ".btnEditAdvised", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation();

        let item = $(this).attr("item")
        //loadingBtnIcon(".btnEditAdvised")

        const formName = "#editFormAdvised"
        let formValue = 0
        let timezoneVars = Intl.DateTimeFormat().resolvedOptions()
        let name = $(formName + " #inputName").val()
        let fathersLastname = $(formName + " #inputFathersLastname").val()
        let mothersLastname = $(formName + " #inputMothersLastname").val()
        let gender = $(formName + " #inputGender").val()
        let ci = $(formName + " #inputCI").val()
        let ext = $(formName + " #inputExt").val()
        let email = $(formName + " #inputEmail").val()
        let address = $(formName + " #inputAddress").val()
        let birthday = $(formName + " #inputBirthday").val()
        let celular = $(formName + " #inputCelular").val()
        let nationality = $(formName + " #inputNationality").val()
        let civilState = $(formName + " #inputCivilState").val()
        let dependent = $(formName + " #inputDependent").val()
        let telfFax = $(formName + " #inputTelfFax").val()
        let city = $(formName + " #inputCity").val();
        let comments = $(formName + " #inputComments").val();

        ///Datos services
        let services = []
        let counServices = 0
        $(" input[name='inputServices[]']").each(function (index) {
            let valorServices = $(this).val()
            let checkServices = $(this).prop("checked")
            if (checkServices) {
                services[counServices] = valorServices
                counServices++
            }
        })

        let counselors = []
        let coun = 0
        $(" input[name='inputCounselors[]']").each(function (index) {
            let valor = $(this).val()
            let check = $(this).prop("checked")
            if (check) {
                counselors[coun] = valor
                coun++
            }
        })

        //console.log(counselors)

        modifiedAdvised({
            acuId: item,
            name: name,
            fathersLastname: fathersLastname,
            mothersLastname: mothersLastname,
            gender: gender,
            ci: ci,
            ext: ext,
            city,
            email: email,
            birthday,
            celular: celular,
            nationality: nationality,
            civilState: civilState,
            dependents: dependent,
            telfFax: telfFax,
            services: services,
            counselors: counselors,
            address: address,
            comments
        }).then((response) => {
            //console.log("modifiedAdvised", response)

            if (response.Error == 0) {

                removeInnerForm();
                advisedIndex();
            } else {
                //console.log("Error", response.message)
            }

        })
    })

    /*     $("body").on("click", ".btnSearchCoun", function(e) {
            e.preventDefault()
            e.stopPropagation()
            e.stopImmediatePropagation()

            let counselors = [];
            let coun = 0;
            $(" input[name='inputCounselors[]']").each(function(index) {
                let valor = $(this).val()
                let check = $(this).prop('checked')
                if (check) {
                    counselors[coun] = valor
                    coun++
                }
            })

            //console.log(counselors)
        }) */
}

export const loadDashboardAdvised = async (vars) => { 
    var url = _PATH_WEB_NUCLEO + "modules/counseling/controllers/apis/v1/advised.php";
    let access_token = localStorage.getItem("access_token")
    let refresh_token = localStorage.getItem("refresh_token")
    let idEntitie = localStorage.getItem("idEntitie")

    const dataForm = new FormData()
    dataForm.append("access_token", access_token)
    dataForm.append("refresh_token", refresh_token)
    dataForm.append("entitieId", idEntitie)
    dataForm.append("actions", "loadAdvised")
    dataForm.append("page", _PATH_PAGE)

    let req = new Request(url, {
        async: true,
        method: "POST",
        mode: "cors",
        body: dataForm,
    })

    try {
        let contentAwait = await fetch(req)
        let jsonData = await contentAwait.json()
         //console.log("🚀 ~ file: advised.js:397 ~ loadDashboardAdvised ~ jsonData:", jsonData)
        //console.log("loadAdvised: " + jsonData)

        return jsonData
    } catch (err) {
        //console.log("loadAdvised Error:" + err)
    }
}

//Forms
export function formNewAdvised(vars) {
    var strHtml = ""
    numMod++
    const system = vars["system"]
    const module = vars["module"]
    const action = vars["action"]
    const pathurl = vars["pathurl"]
    loadView(
        _PATH_WEB_NUCLEO + pathurl + "views/formAdvised.html?" + _VS + numMod
    ).then((htmlView) => {
        //console.log(htmlView)
        strHtml = replacePath(htmlView)
        strHtml = replaceAll(strHtml, "{{_MODULE}}", module)
        strHtml = replaceAll(strHtml, "{{_SYSTEM}}", system)
        strHtml = replaceAll(strHtml, "{{_ID_FORM}}", "newFormAdvised")
        strHtml = replaceAll(strHtml, "{{_BTN_ACTION}}", "btnSaveAdvised")
        strHtml = replaceAll(strHtml, "{{_BTN_NAME_ACTION}}", "Guardar")
        strHtml = replaceAll(strHtml, "{{_TITLE}}", "<span>Nuevo</span> <span class='title2'>Aconsejado</a>")

        //stopLoadingBar()
        //$(".bodyModule[module='" + module + "'] .innerForm").html(strHtml);

        innerForm({
            module,
            body: strHtml,
        });

        $(".inputServices[value='1']").attr("checked", "checked")

        loadCounselors({ filter: "rol", active: 1 }).then((listData) => {
            console.log("listData",listData)
            let listHtml = "<br>";
            let list = listData.data
            for (const i in list) {
                const name = list[i].name + " " + list[i].lastname
                const id = list[i].id;
                let fields = list[i].fields;
                //const active = list[i].active ? "checked" : "";
                const active = "";
                //console.log(list[i].name)
                if (fields == 0){
                    fields = "---";
                }
                listHtml += `<div class="checkbox checkboxControl level level${id}" child="">
                                    <input name="inputCounselors[]" id="inputCounselors${id}" type="checkbox" class="check" ${active} value="${id}" >
                                    <div class="checkboxControlLabel"><span>${name}</span> <br><span class="reference">${fields}</span></div>
                            </div>`
            }
            $("#listConunselors").html(listHtml)
        })

        jQuery.datetimepicker.setLocale("es")

        $("#inputBirthday").datetimepicker({
            timepicker: false,
            datepicker: true,
            format: "Y-m-d",
            lang: "es",
        })

        $("#inputBirthdaySpouse").datetimepicker({
            timepicker: false,
            datepicker: true,
            format: "Y-m-d",
            lang: "es",
        })
    })
}

export function createUserExpress(vars) {
    //console.log("createUserExpress", vars);
    addHtml({
        selector: "body",
        type: 'prepend',
        content: renderModal({ id: "modalCreateAdviser", body: renderFormNewAdviserExpress({ id: 'formCreateAdviserExpress', counselorId: vars.counselor, inputReturn: vars.inputReturn }) })
    }) //type: html, append, prepend, before, after

    $("#inputBirthday").datetimepicker({
        timepicker: false,
        datepicker: true,
        format: "Y-m-d",
        lang: "es",
    })
}

export function formEditAdvised(vars) {
    //console.log(vars)

    var strHtml = ""
    numMod++
    const system = vars["system"]
    const module = vars["module"]
    const action = vars["action"]
    const pathurl = vars["pathurl"]
    const item = vars["item"]

    loadView(
        _PATH_WEB_NUCLEO +
        "modules/counseling/views/formAdvised.html?" +
        _VS +
        numMod
    ).then((htmlView) => {
        //console.log(htmlView)
        strHtml = replacePath(htmlView)
        strHtml = replaceAll(strHtml, "{{_MODULE}}", module)
        strHtml = replaceAll(strHtml, "{{_SYSTEM}}", system)
        strHtml = replaceAll(strHtml, "{{_ID_FORM}}", "editFormAdvised")
        strHtml = replaceAll(strHtml, "{{_BTN_ACTION}}", "btnEditAdvised")
        strHtml = replaceAll(strHtml, "{{_ITEM}}", item)
        strHtml = replaceAll(strHtml, "{{_BTN_NAME_ACTION}}", "Actualizar")
        strHtml = replaceAll(strHtml, "{{_TITLE}}", "Editar <span class='title2'>Aconsejado</span>")

        //console.log(vars);

        loadEditAdvisedId(vars).then((fact) => {
            //console.log(fact)

            //$(".bodyModule[module='" + module + "'] .innerForm").html(strHtml)
            innerForm({
                module,
                body: strHtml,
            });

            //Load Date
            $("#inputId").val(fact.id)
            $("#inputName").val(fact.name)
            $("#inputFathersLastname").val(fact.fathers_lastname)
            $("#inputMothersLastname").val(fact.mothers_lastname)
            if (fact.gender != "") {
                $("#inputGender option[value=" + fact.gender + "]").attr(
                    "selected",
                    "selected"
                )
            }
            $("#inputCI").val(fact.ci)
            if (fact.ci != "") {
                $("#inpuExt option[value='" + fact.ci_ext + "']").attr(
                    "selected",
                    "selected"
                )
            }
            $("#inputEmail").val(fact.email)
            $("#inputAddress").val(fact.address)
            $("#inputBirthday").val(fact.birthday_date)
            $("#inputCelular").val(fact.celular);
            $("#inputDependent").val(fact.dependents);
            $("#inputCity").val(fact.city);
            $("#inputTelfFax").val(fact.telf_fax);
            $("#inputComments").val(fact.comments);

            if (fact.services != 0) {
                let services = fact.services
                for (let j = 0; j < services.length; j++) {
                    const idSv = services[j].id
                    $(".inputServices[value='" + idSv + "']").attr(
                        "checked",
                        "checked"
                    )
                }
            }

            stopLoadingBar()

            loadCounselors({ filter: "rol" }).then((listData) => {
                //console.log("loadCounselors", listData)
                let listHtml = "<br>"
                let list = listData.data
                for (const i in list) {
                    const name = list[i].name + " " + list[i].lastname
                    const id = list[i].id
                    const fields = list[i].fields


                    //console.log(list[i].name)
                    listHtml += `<div class="checkbox checkboxControl level level${id}" child="">
                                    <input name="inputCounselors[]" id="inputCounselors${id}" type="checkbox" class="check inputCounselors" value="${id}" >
                                    <div class="checkboxControlLabel"><span>${name}</span> <br><span class="reference">${fields}</span></div>
                                </div>`
                }
                $("#listConunselors").html(listHtml)

                if (fact.assignedCounselor != 0) {
                    let assignedCounselor = fact.assignedCounselor
                    for (let j = 0; j < assignedCounselor.length; j++) {
                        const idConselor = assignedCounselor[j].id
                        $(".inputCounselors[value='" + idConselor + "']").attr(
                            "checked",
                            "checked"
                        )
                    }
                }
            })

            jQuery.datetimepicker.setLocale("es")

            $("#inputBirthday").datetimepicker({
                timepicker: false,
                datepicker: true,
                format: "Y-m-d",
                lang: "es",
            })
            $("#inputBirthdaySpouse").datetimepicker({
                timepicker: false,
                datepicker: true,
                format: "Y-m-d",
                lang: "es",
            })
        })
    })
}

//funciones
async function saveAdvised(e) {
    let idBtn = e.currentTarget.id
    var url = _PATH_WEB_NUCLEO + "modules/counseling/controllers/apis/v1/advised.php";
    let access_token = localStorage.getItem("access_token")
    let refresh_token = localStorage.getItem("refresh_token")
    let idEntitie = localStorage.getItem("idEntitie")

    const dataForm = new FormData()
    dataForm.append("access_token", access_token)
    dataForm.append("refresh_token", refresh_token)
    dataForm.append("entitieId", idEntitie)
    dataForm.append("actions", "saveNewAdvised")
    dataForm.append("page", _PATH_PAGE)

    const formName = "#newFormAdvised"
    let formValue = 0
    let timezoneVars = Intl.DateTimeFormat().resolvedOptions()
    let name = $(formName + " #inputName").val()
    let fathersLastname = $(formName + " #inputFathersLastname").val()
    let mothersLastname = $(formName + " #inputMothersLastname").val()
    let gender = $(formName + " #inputGender").val()
    let ci = $(formName + " #inputCI").val()
    let ext = $(formName + " #inputExt").val()
    let email = $(formName + " #inputEmail").val()
    let address = $(formName + " #inputAddress").val()
    let birthday = $(formName + " #inputBirthday").val()
    let celular = $(formName + " #inputCelular").val()
    let nationality = $(formName + " #inputNationality").val()
    let civilState = $(formName + " #inputCivilState").val()
    let dependents = $(formName + " #inputDependent").val()
    let telfFax = $(formName + " #inputTelfFax").val()
    let city = $(formName + " #inputCity").val()
    let comments = $(formName + " #inputComments").val()
    let referred = $(formName + " input[name='inputReferred']:checked").val()

    if (referred == "otros") {
        referred = $(formName + " #inputReferredDetail").val()
    }

    ///Datos services
    let services = []
    let counServices = 0
    $(" input[name='inputServices[]']").each(function (index) {
        let valorServices = $(this).val()
        let checkServices = $(this).prop("checked")
        if (checkServices) {
            services[counServices] = valorServices
            counServices++
        }
    })

    let counselors = []
    let coun = 0
    $(" input[name='inputCounselors[]']").each(function (index) {
        let valor = $(this).val()
        let check = $(this).prop("checked")
        if (check) {
            counselors[coun] = valor
            coun++
        }
    })

    //console.log(counselors)

    ///Datos laborales
    let WorkState = $(formName + " #inputWorkState").val()
    let Work = $(formName + " #inputWork").val()
    let WorkSector = $(formName + " #inputWorkSector").val()
    let AdressWork = $(formName + " #inputAdressWork").val()
    let Profession = $(formName + " #inputProfession").val()
    let Position = $(formName + " #inputPosition").val()

    ///Dconyuge
    let NameSpouse = $(formName + " #inputNameSpouse").val()
    let SexSpouse = $(formName + " #inputSexSpouse").val()
    let CISpouse = $(formName + " #inputCISpouse").val()
    let ExtSpouse = $(formName + " #inputExtSpouse").val()
    let birthdaySpouse = $(formName + " #inputBirthdaySpouse").val()
    let NroNationalitySpouse = $(formName + " #inputNroNationalitySpouse").val()
    let TelfFaxSpouse = $(formName + " #inputTelfFaxSpouse").val()
    let CelularSpouse = $(formName + " #inputCelularSpouse").val()
    let WorkStateSpouse = $(formName + " #inputWorkStateSpouse").val()
    let WorkSpouse = $(formName + " #inputWorkSpouse").val()
    let WorkSectorSpouse = $(formName + " #inputWorkSectorSpouse").val()
    let AdressWorkSpouse = $(formName + " #inputAdressWorkSpouse").val()
    let ProfessionSpouse = $(formName + " #inputProfessionSpouse").val()
    let PositionSpouse = $(formName + " #inputPositionSpouse").val()

    if (name == "") {
        $(formName + " #inputName").addClass("error")
        $(formName + " #inputName").attr(
            "placeholder",
            "Ingresar un Nombre valido."
        )
        $("body").on("keyup", "#inputName", function () {
            $(formName + " #inputName").removeClass("error")
            unDisableId(idBtn)
        })
        formValue++
    }
    if (fathersLastname == "") {
        $(formName + " #inputFathersLastname").addClass("error")
        $(formName + " #inputFathersLastname").attr(
            "placeholder",
            "Ingresar un Apellido valido."
        )
        $("body").on("keyup", "#inputFathersLastname", function () {
            $(formName + " #inputFathersLastname").removeClass("error")
            unDisableId(idBtn)
        })
        formValue++
    }
    /* if (mothersLastname == "") {
        $(formName + " #inputMothersLastname").addClass("error")
        $(formName + " #inputMothersLastname").attr(
            "placeholder",
            "Ingresar un Apellido valido."
        )
        $("body").on("keyup", "#inputMothersLastname", function () {
            $(formName + " #inputMothersLastname").removeClass("error")
            unDisableId(idBtn)
        })
        formValue++
    } */

    if (ci == "") {
        $(formName + " #inputCI").addClass("error")
        $(formName + " #inputCI").attr("placeholder", "Ingresar un CI valido.")
        $("body").on("keyup", "#inputCI", function () {
            $(formName + " #inputCI").removeClass("error")
            unDisableId(idBtn)
        })
        formValue++
    }

    /* if (email == "") {
        $(formName + " #inputEmail").addClass("error")
        $(formName + " #inputEmail").attr("placeholder", "Ingresar un Email valido.")
        $("body").on("keyup", "#inputEmail", function() {
            $(formName + " #inputEmail").removeClass("error")
            unDisableId(idBtn)
        })
        $("body").on("change", "#inputEmail", function() {
            $(formName + " #inputEmail").removeClass("error")
            unDisableId(idBtn)

        })
        formValue++
    } */
    //console.log("validateEmail(email)", validateEmail(email))
    if (email != "") {
        if (validateEmail(email) == null || validateEmail(email) == false) {
            $(formName + " #inputEmail").addClass("error")
            $(formName + " #inputEmail").attr(
                "placeholder",
                "Ingresar un Email valido."
            )
            $("body").on("keyup", "#inputEmail", function () {
                $(formName + " #inputEmail").removeClass("error")
                unDisableId(idBtn)
            })
            $("body").on("change", "#inputEmail", function () {
                $(formName + " #inputEmail").removeClass("error")
                unDisableId(idBtn)
            })

            alertMessageError({
                title: 'Error',
                message: "Erro en email, ingresar un Email valido.",
            });
            formValue++
        }
    }

    if (birthday == "") {
        $(formName + " #inputBirthday").addClass("error")
        $(formName + " #inputBirthday").attr(
            "placeholder",
            "Fecha no valida."
        )
        $("body").on("keyup", "#inputBirthday", function () {
            $(formName + " #inputBirthday").removeClass("error")
            unDisableId(idBtn)
        })
        $("body").on("change", "#inputBirthday", function () {
            $(formName + " #inputBirthday").removeClass("error")
            unDisableId(idBtn)
        })
        formValue++
    }
    if (celular == "") {
        $(formName + " #inputCelular").addClass("error")
        $(formName + " #inputCelular").attr(
            "placeholder",
            "Ingresar un Celular valido."
        )
        $("body").on("keyup", "#inputCelular", function () {
            $(formName + " #inputCelular").removeClass("error")
            unDisableId(idBtn)
        })
        formValue++
    }

    if (formValue == 0) {
        dataForm.append("vars", JSON.stringify({
            name,
            fathersLastname,
            mothersLastname,
            gender,
            ci,
            ext,
            email,
            address,
            birthday,
            celular,
            comments,
            nationality,
            city,
            civilState,
            dependents,
            telfFax,
            WorkState,
            WorkSector,
            AdressWork,
            Profession,
            Position,
            NameSpouse,
            SexSpouse,
            CISpouse,
            ExtSpouse,
            birthdaySpouse,
            NroNationalitySpouse,
            TelfFaxSpouse,
            CelularSpouse,
            WorkStateSpouse,
            WorkSpouse,
            WorkSectorSpouse,
            AdressWorkSpouse,
            ProfessionSpouse,
            PositionSpouse,
            referred,
            services,
            counselors,
            locale: timezoneVars.locale,
            timezone: timezoneVars.timeZone
        }));

        let req = new Request(url, {
            async: true,
            method: "POST",
            mode: "cors",
            body: dataForm,
        })

        try {
            let contentAwait = await fetch(req)
            let data = await contentAwait.json()
            //console.log('saveAdvised', data)
            //console.log("saveAdvised: " + data)
            return data
        } catch (err) {
            //console.log("saveAdvised Error:" + err)
        }
    } else {
        removeBtnLoading(e)
    }
}

async function saveAdvisedExpress(vars = []) {
    let idBtn = vars.btnId
    var url = _PATH_WEB_NUCLEO + "modules/counseling/controllers/apis/v1/advised.php";
    let access_token = localStorage.getItem("access_token")
    let refresh_token = localStorage.getItem("refresh_token")
    let idEntitie = localStorage.getItem("idEntitie")

    const dataForm = new FormData()
    dataForm.append("access_token", access_token)
    dataForm.append("refresh_token", refresh_token)
    dataForm.append("entitieId", idEntitie)
    dataForm.append("actions", "saveNewAdvised")
    dataForm.append("page", _PATH_PAGE)

    const formName = "#" + vars.id
    let formValue = 0
    let timezoneVars = Intl.DateTimeFormat().resolvedOptions()
    let name = $(formName + " #inputName").val()
    let fathersLastname = $(formName + " #inputLastNameFather").val()
    let mothersLastname = $(formName + " #inputLastNameMother").val()

    let ci = $(formName + " #inputCI").val()
    let ext = $(formName + " #inputExt").val()

    let birthday = $(formName + " #inputBirthday").val()
    let celular = $(formName + " #inputCelular").val()


    ///Datos services
    let services = []
    let counServices = 0
    $("input[name='inputServices[]']").each(function (index) {
        let valorServices = $(this).val()
        let checkServices = $(this).prop("checked")
        if (checkServices) {
            services[counServices] = valorServices
            counServices++
        }
    })

    let counselors = []
    let coun = 0
    $(" input[name='inputCounselors[]']").each(function (index) {
        let valor = $(this).val()
        let check = $(this).prop("checked")
        if (check) {
            counselors[coun] = valor
            coun++
        }
    })

    //console.log(counselors)

    if (name == "") {
        $(formName + " #inputName").addClass("error")
        $(formName + " #inputName").attr(
            "placeholder",
            "Ingresar un Nombre valido."
        )
        $("body").on("keyup", "#inputName", function () {
            $(formName + " #inputName").removeClass("error")
            unDisableId(idBtn)
        })
        formValue++
    }
    if (fathersLastname == "") {
        $(formName + " #inputFathersLastname").addClass("error")
        $(formName + " #inputFathersLastname").attr(
            "placeholder",
            "Ingresar un Apellido valido."
        )
        $("body").on("keyup", "#inputFathersLastname", function () {
            $(formName + " #inputFathersLastname").removeClass("error")
            unDisableId(idBtn)
        })
        formValue++
    }
    /* if (mothersLastname == "") {
        $(formName + " #inputMothersLastname").addClass("error")
        $(formName + " #inputMothersLastname").attr(
            "placeholder",
            "Ingresar un Apellido valido."
        )
        $("body").on("keyup", "#inputMothersLastname", function () {
            $(formName + " #inputMothersLastname").removeClass("error")
            unDisableId(idBtn)
        })
        formValue++
    } */

    if (ci == "") {
        $(formName + " #inputCI").addClass("error")
        $(formName + " #inputCI").attr("placeholder", "Ingresar un CI valido.")
        $("body").on("keyup", "#inputCI", function () {
            $(formName + " #inputCI").removeClass("error")
            unDisableId(idBtn)
        })
        formValue++
    }

    /* if (email == "") {
        $(formName + " #inputEmail").addClass("error")
        $(formName + " #inputEmail").attr("placeholder", "Ingresar un Email valido.")
        $("body").on("keyup", "#inputEmail", function() {
            $(formName + " #inputEmail").removeClass("error")
            unDisableId(idBtn)
        })
        $("body").on("change", "#inputEmail", function() {
            $(formName + " #inputEmail").removeClass("error")
            unDisableId(idBtn)

        })
        formValue++
    } */
    //console.log("validateEmail(email)", validateEmail(email))


    if (birthday == "") {
        $(formName + " #inputBirthday").addClass("error")
        $(formName + " #inputBirthday").attr(
            "placeholder",
            "Fecha no valida."
        )
        $("body").on("keyup", "#inputBirthday", function () {
            $(formName + " #inputBirthday").removeClass("error")
            unDisableId(idBtn)
        })
        $("body").on("change", "#inputBirthday", function () {
            $(formName + " #inputBirthday").removeClass("error")
            unDisableId(idBtn)
        })
        formValue++
    }
    if (celular == "") {
        $(formName + " #inputCelular").addClass("error")
        $(formName + " #inputCelular").attr(
            "placeholder",
            "Ingresar un Celular valido."
        )
        $("body").on("keyup", "#inputCelular", function () {
            $(formName + " #inputCelular").removeClass("error")
            unDisableId(idBtn)
        })
        formValue++
    }

    if (formValue == 0) {
        dataForm.append("vars", JSON.stringify({
            name,
            fathersLastname,
            mothersLastname,
            ci,
            ext,
            birthday,
            celular,
            services,
            counselors,
            locale: timezoneVars.locale,
            timezone: timezoneVars.timeZone
        }));

        let req = new Request(url, {
            async: true,
            method: "POST",
            mode: "cors",
            body: dataForm,
        })

        try {
            let contentAwait = await fetch(req)
            let data = await contentAwait.json()
            //console.log('saveAdvised', data)
            //console.log("saveAdvised: " + data)
            return data
        } catch (err) {
            //console.log("saveAdvised Error:" + err)
        }
    }
}

async function modifiedAdvised(vars) {
    //console.log(vars)
    var vari = JSON.stringify(vars)
    var url = _PATH_WEB_NUCLEO + "modules/counseling/controllers/apis/v1/advised.php";
    const dataForm = new FormData()
    dataForm.append("access_token", localStorage.getItem("access_token"))
    dataForm.append("refresh_token", localStorage.getItem("refresh_token"))
    dataForm.append("entitieId", localStorage.getItem("idEntitie"))
    dataForm.append("page", _PATH_PAGE)

    dataForm.append("actions", "modifiedAdvised")
    dataForm.append("vars", vari)

    let req = new Request(url, {
        async: true,
        method: "POST",
        mode: "cors",
        body: dataForm,
    })
    try {
        let contentAwait = await fetch(req)
        let str = await contentAwait.json()
        //console.log(str)
        //console.log('modifiedAdvised: ' + str)
        return str
    } catch (err) {
        //console.log("modifiedAdvised Error: " + err)
    }
}

async function deleteAdvised(vars) {
    var url = _PATH_WEB_NUCLEO + "modules/counseling/controllers/apis/v1/advised.php";
    let access_token = localStorage.getItem("access_token")
    let refresh_token = localStorage.getItem("refresh_token")
    let idEntitie = localStorage.getItem("idEntitie")

    const dataForm = new FormData()
    dataForm.append("access_token", access_token)
    dataForm.append("refresh_token", refresh_token)
    dataForm.append("entitieId", idEntitie)
    dataForm.append("actions", "deleteAdvised")
    dataForm.append("page", _PATH_PAGE)

    dataForm.append("vars", `{"item":"${vars.item}"}`)

    let req = new Request(url, {
        async: true,
        method: "POST",
        mode: "cors",
        body: dataForm,
    })

    try {
        let contentAwait = await fetch(req)
        let data = await contentAwait.json()
        //console.log("deleteAdvised: " + data)
        return data
    } catch (err) {
        //console.log("deleteAdvised Error:" + err)
    }
}

export const loadCounselors = async (vars = []) => {
    //console.log('loadCounselors', vars);
    const url = _PATH_WEB_NUCLEO + "modules/counseling/controllers/apis/v1/advised.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "loadCounselors",
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        let str = res.data;
        //console.log('loadCounselors', str);
        localStorage.setItem("counselors", JSON.stringify(str.data))
        return str
    } catch (error) {
        console.log("Error: loadCounselors")
        console.log(error)
    }
}

async function loadEditAdvisedId(vars) {
    // //console.log(vars);
    // //console.log(vars.item);

    const system = "counseling"
    const module = "advised"
    var url = _PATH_WEB_NUCLEO + "modules/counseling/controllers/apis/v1/advised.php";
    let access_token = localStorage.getItem("access_token")
    let refresh_token = localStorage.getItem("refresh_token")
    let idEntitie = localStorage.getItem("idEntitie")

    const dataForm = new FormData()
    dataForm.append("access_token", access_token)
    dataForm.append("refresh_token", refresh_token)
    dataForm.append("entitieId", idEntitie)
    dataForm.append("actions", "editAdvisedId")
    dataForm.append("item", vars.item)
    dataForm.append("page", _PATH_PAGE)

    let req = new Request(url, {
        async: true,
        method: "POST",
        mode: "cors",
        body: dataForm,
    })

    try {
        let contentAwait = await fetch(req)
        let str = await contentAwait.json()
        //console.log(str)
        //console.log("loadAdvisedId: " + str)
        return str
    } catch (err) {
        //console.log("loadAdvised Error:" + err)
    }
}

/* 
document.addEventListener("click", function (e) {
    //console.log("click", e);
    let target = e.target;
    let id = target.id;

    if (target.classList.contains("btnDeleteFn")) {
        let data = target.dataset;
        //console.log(data);

        addHtml({
            selector: `#root`,
            type: 'prepend', // insert, append, prepend, replace
            content: renderDeleteModal({
                name: data.name,
                id: "modalDelete",
                cls: "btnDeleteAdvised",
                attr: `data-id="${data.id}"`,
            })
        })
    }

    if (target.classList.contains("btnDeleteAdvised")) {
        let data = target.dataset;
        deleteAdvised({
            item: data.id
        }).then((data) => {
            if (data) {
                //console.log(data)
                removeModal()
                advisedIndex()
            }
        })
    }
}); */


document.addEventListener("DOMContentLoaded", function () {
    $("body").on("click", `.btnSaveAdviserExpress`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`.btnSaveAdviserExpress`, data);
        data["btnId"] = e;
        saveAdvisedExpress(data).then((dataOut) => {
            //console.log("saveAdvisedExpress", dataOut);

            if (dataOut.status == "success") {
                removeModal()

                addAccountList({ acuId: dataOut.data.idAccount, name: dataOut.data.name, inputreturn: data.inputreturn })
            }
            if (dataOut.Error == "error") {
                $(`.message[for="${data.id}"]`).html(`<div class="message messageDanger">${dataOut.message}</div>`);
            }
        }).catch((err) => {
            //console.log(err)
        });
    });
});