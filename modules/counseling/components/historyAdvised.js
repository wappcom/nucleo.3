import {
	listJsonToString,
	btnFn,
	loadView,
	replaceAll,
	todaysDate,
	replacePath,
	stopLoadingBar,
	loadBtnLoading,
	loadingBtnIcon,
	addHtml,
	accessToken,
	alertMessageError,
	removeLoadingBtnIcon,
	changeBtn,
	timer
} from "../../components/functions.js";
import {
	createTable,
	dataTable
} from "../../components/tables.js";
import {
	validateEmail,
	validateDate,
	editorText,
	addMessageToForm
} from "../../components/forms.js";
import {
	deleteModalItem,
	removeModal,
	removeModalId
} from "../../components/modals.js";
import {
	innerForm,
	renderModalClean
} from "../../components/renders/renderModals.js";

import {
	renderSelectOptions
} from "../../components/renders/renderForms.js";
import {
	renderListHistory,
	renderHistory,
	renderAfterhoursRegistrationHistory
} from "./renders/renderCounseling.js";

let numMod = 0;

export function formHistoryAdvised(vars) {
	//console.log("🚀 ~ file: historyAdvised.js:50 ~ formHistoryAdvised ~ vars:", vars)
	let strHtml = "";
	let values = vars;
	numMod++;
	const item = vars["item"];
	const system = vars["system"];
	const module = vars["module"];
	const action = vars["action"];
	const pathurl = vars["pathurl"];

	$(".modalFormCalendar").remove();
	loadView(
		_PATH_WEB_NUCLEO +
		"modules/counseling/views/history.html?" +
		_VS +
		numMod
	).then((htmlView) => {
		//console.log(htmlView)
		strHtml = replacePath(htmlView);
		strHtml = replaceAll(strHtml, "{{_MODULE}}", module);
		strHtml = replaceAll(strHtml, "{{_SYSTEM}}", system);
		strHtml = replaceAll(strHtml, "{{_ITEM}}", item);
		strHtml = replaceAll(strHtml, "{{_ID_FORM}}", "historyAdvised");
		strHtml = replaceAll(strHtml, "{{_BTN_ACTION}}", "btnHistoryAdvised");
		strHtml = replaceAll(strHtml, "{{_BTN_NAME_ACTION}}", "Añadir");
		strHtml = replaceAll(strHtml, "{{_TITLE}}", "Historial Aconsejado");

		//console.log(vars);

		loadHistoryAdvised(vars).then((reject) => {
			//console.log(values)
			//console.log("loadHistoryAdvised", reject);
			let arrayVars = values.vars.split(",");
			let name = reject.dataAccount.name + " " + reject.dataAccount.lastname;
			let fieldId = arrayVars[1];
			let dataHistory = reject.data;
			strHtml = replaceAll(strHtml, "{{_NAME}}", name);
			localStorage.setItem("fieldsHistory", JSON.stringify(reject.fields));

			strHtml = replaceAll(strHtml, "{{_DATE}}", todaysDate("full"));
			strHtml = replaceAll(strHtml, "{{_HISTORY_ID}}", vars.data.calId);
			strHtml = replaceAll(
				strHtml,
				"{{_OPTION_FIELDS}}",
				renderSelectOptions({
					options: reject.fields,
					selectedDefaultValue: fieldId,
				})
			);

			//console.log(strHtml);
			//console.log(module, strHtml);

			//$(".bodyModule[module='" + module + "'] .innerForm").html(strHtml);

			innerForm({
				module,
				body: strHtml,
			});

			//new EasyEditor('#inputComments')
			editorText("inputComments");
			$("#inputComments").focus();

			if (reject.num > 0) {
				$("#historyAdvisedHistory").html(renderListHistory());

				//console.log(dataHistory);
				//console.log(dataHistory.length);
				$("#historyAdvisedHistory .listHistory").html("");
				for (let i = 0; i < dataHistory.length; i++) {
					const comments = dataHistory[i].comments;
					const id = dataHistory[i].id;
					const dateInit = dataHistory[i].dateInit;
					const dateEnd = dataHistory[i].dateEnd;
					const field = dataHistory[i].field;
					let place = dataHistory[i].place;
					let type = dataHistory[i].type;

					if (place == "office") {
						place = "En Oficina";
					}

					$("#historyAdvisedHistory .listHistory").prepend(
						renderHistory({
							comments,
							id,
							dateInit,
							dateEnd,
							place,
							field,
							type
						})
					);
				}
			}
		});
	});
}

export function changeBtnStop(item) {
	$("#btnRegisterHistoryAdvised").removeClass("btnPrimary btnRegisterHistory disabled");
	$("#btnRegisterHistoryAdvised").addClass("btnDanger btnStopRegister");
	$("#btnRegisterHistoryAdvised .icon").removeClass("icon-refresh icon-loading");
	$("#btnRegisterHistoryAdvised .icon").addClass("icon-checked-o");
	$("#btnRegisterHistoryAdvised span").html("Detener");
	$("#btnRegisterHistoryAdvised").attr("numhistory", item);
}

export function changeBtnRegisterDate() {
	$("#btnRegisterHistoryAdvised").removeClass("btnDanger btnStopRegister disabled");
	$("#btnRegisterHistoryAdvised .icon").removeClass("icon-refresh icon-loading");
	$("#btnRegisterHistoryAdvised .icon").addClass("icon-clock");
	$("#btnRegisterHistoryAdvised").addClass("btnSuccess btnRegisterDate");
	$("#btnRegisterHistoryAdvised span").html("Registrar");
}

export function clearFormHistory() {
	$("#btnRegisterHistoryAdvised .icon").removeClass("icon-refresh icon-loading");
	$("#btnRegisterHistoryAdvised").removeClass("btnSuccess btnRegisterDate disabled");
	$("#btnRegisterHistoryAdvised").removeClass("btnIconLeft");
	$("#btnRegisterHistoryAdvised").addClass("btnPrimary btnRegisterHistory");
	$("#inputComments").val("");
	$(".trumbowyg-editor").html("");
	$("#inputFields").val(0);
	$("#inputPlaceHistory").val("office");
	$("#inputOthers").val("");
	$("#inputOthers").removeClass("on");
	$("#btnRegisterHistoryAdvised span").html("Iniciar Registro");
}

export const afterhoursRegistrationHistory = (vars = []) => {
	console.log('afterhoursRegistrationHistory', vars);
	loadingBtnIcon("#btnAfterhoursRegistrationHistorySave");
	getData({
		task: 'afterhoursRegistrationHistory',
		return: 'returnId', // returnId, returnState, returnArray
		inputs: vars
	}).then((response) => {
		console.log('getData afterhoursRegistrationHistory', response);
		if (response.status == 'success') {
			removeLoadingBtnIcon("#btnAfterhoursRegistrationHistorySave");
			changeBtn({ selector: "#btnAfterhoursRegistrationHistorySave", type: "success", text: "Se guardo con exito!" });
			timer(1000);
			removeModalId("modalAfterhoursRegistrationHistory");
			$("#historyAdvisedHistory .listHistory").prepend(
				renderHistory({
					comments: vars.inputComments,
					id: response.data,
					field: vars.inputFields,
					dateInit: vars.inputDateInit,
					dateEnd: vars.inputDateEnd,
					place: vars.inputOthers == "" ? vars.inputPlaceHistory : vars.inputOthers,
					type: 'Fuera de Hora'
				})
			);
		} else {
			alertMessageError({ message: response.message })
		}
	}).catch(console.warn());
}

async function addHistory(vars) {
	var vari = JSON.stringify(vars);
	var url =
		_PATH_WEB_NUCLEO + "modules/counseling/controllers/apis/v1/advised.php";
	const dataForm = new FormData();
	dataForm.append("access_token", localStorage.getItem("access_token"));
	dataForm.append("refresh_token", localStorage.getItem("refresh_token"));
	dataForm.append("entitieId", localStorage.getItem("idEntitie"));
	dataForm.append("page", _PATH_PAGE);

	dataForm.append("actions", "addHistory");
	dataForm.append("vars", vari);

	let req = new Request(url, {
		async: true,
		method: "POST",
		mode: "cors",
		body: dataForm,
	});
	try {
		let contentAwait = await fetch(req);
		let str = await contentAwait.json();
		//console.log(str);
		//console.log('addHistory: ' + str)
		return str;
	} catch (err) {
		//console.log("addHistory Error: " + err);
	}
}

async function updateHistoryEnd(vars) {
	var vari = JSON.stringify(vars);
	var url =
		_PATH_WEB_NUCLEO + "modules/counseling/controllers/apis/v1/advised.php";
	const dataForm = new FormData();
	dataForm.append("access_token", localStorage.getItem("access_token"));
	dataForm.append("refresh_token", localStorage.getItem("refresh_token"));
	dataForm.append("entitieId", localStorage.getItem("idEntitie"));
	dataForm.append("page", _PATH_PAGE);

	dataForm.append("actions", "updateHistoryEnd");
	dataForm.append("vars", vari);

	let req = new Request(url, {
		async: true,
		method: "POST",
		mode: "cors",
		body: dataForm,
	});
	try {
		let contentAwait = await fetch(req);
		let str = await contentAwait.json();
		//console.log(str)
		//console.log('updateHistoryEnd: ' + str)
		return str;
	} catch (err) {
		//console.log("updateHistoryEnd Error: " + err);
	}
}

async function updateHistoryRegister(vars) {
	var vari = JSON.stringify(vars);
	var url =
		_PATH_WEB_NUCLEO + "modules/counseling/controllers/apis/v1/advised.php";
	const dataForm = new FormData();
	dataForm.append("access_token", localStorage.getItem("access_token"));
	dataForm.append("refresh_token", localStorage.getItem("refresh_token"));
	dataForm.append("entitieId", localStorage.getItem("idEntitie"));
	dataForm.append("page", _PATH_PAGE);

	dataForm.append("actions", "updateHistoryRegister");
	dataForm.append("vars", vari);

	let req = new Request(url, {
		async: true,
		method: "POST",
		mode: "cors",
		body: dataForm,
	});
	try {
		let contentAwait = await fetch(req);
		let str = await contentAwait.json();
		//console.log(str)
		//console.log('updateHistoryRegister: ' + str)
		return str;
	} catch (err) {
		//console.log("updateHistoryRegister Error: " + err);
	}
}

async function loadHistoryAdvised(vars) {
	var vari = JSON.stringify(vars);
	var url =
		_PATH_WEB_NUCLEO + "modules/counseling/controllers/apis/v1/advised.php";
	const dataForm = new FormData();
	dataForm.append("access_token", localStorage.getItem("access_token"));
	dataForm.append("refresh_token", localStorage.getItem("refresh_token"));
	dataForm.append("entitieId", localStorage.getItem("idEntitie"));
	dataForm.append("page", _PATH_PAGE);

	dataForm.append("actions", "historyAdvised");
	dataForm.append("vars", vari);

	let req = new Request(url, {
		async: true,
		method: "POST",
		mode: "cors",
		body: dataForm,
	});
	try {
		let contentAwait = await fetch(req);
		let str = await contentAwait.json();
		//console.log(str)
		//console.log('loadHistoryAdvised: ' , str)
		return str;
	} catch (err) {
		//console.log("loadHistoryAdvised Error: " + err);
	}
}

export const getData = async (vars = []) => {
	//console.log('getData', vars);
	const url = _PATH_WEB_NUCLEO + "modules/counseling/controllers/apis/v1/counseling.php";
	let data = JSON.stringify({
		accessToken: accessToken(),
		vars: JSON.stringify(vars)
	});
	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: data
		})
		//console.log(res.data);
		return res.data
	} catch (error) {
		console.log("Error: getDataPlaces", error);
	}
}

document.addEventListener("DOMContentLoaded", function () {
	$("body").on("change", "#inputPlaceHistory", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let value = $(this).val();
		//console.log(value);

		if (value == "others") {
			$("#inputOthers").addClass("on");
			$("#inputOthers").focus();
		} else {
			$("#inputOthers").removeClass("on");
		}
	});

	$("body").on("click", ".btnRegisterHistory", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		loadBtnLoading(e);
		const item = $(this).attr("item");
		const inputFields = $("#inputFields").val();
		let inputPlaceHistory = $("#inputPlaceHistory").val();
		const inputComments = $("#inputComments").val();
		const inputOthers = $("#inputOthers").val();
		const inputCalId = $("#inputCalId").val();

		if (inputPlaceHistory != "office") {
			inputPlaceHistory = inputOthers;
		}

		addHistory({
			acuId: item,
			inputFields: inputFields,
			inputPlaceHistory: inputPlaceHistory,
			inputComments: inputComments,
			inputCalId,
		}).then((response) => {
			console.log("🚀 ~ file: historyAdvised.js:394 ~ response:", response)
			if (response.Error == 0) {
				changeBtnStop(response.historyId);
			} else {
				//console.log("Error." + response.message);
			}
		});
	});

	$("body").on("click", ".btnStopRegister", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		//console.log(".btnStopRegister");
		const item = $(this).attr("item");
		const numHistory = $(this).attr("numhistory");
		const inputFields = $("#inputFields").val();
		let inputPlaceHistory = $("#inputPlaceHistory").val();
		const inputComments = $("#inputComments").val();
		const inputOthers = $("#inputOthers").val();
		$(".icon", this).remove();
		loadBtnLoading(e);

		if (inputPlaceHistory != "office") {
			inputPlaceHistory = inputOthers;
		}

		updateHistoryEnd({
			acuId: item,
			hisId: numHistory,
			inputFields: inputFields,
			inputPlaceHistory: inputPlaceHistory,
			inputComments: inputComments,
		}).then((response) => {
			//console.log("updateHistoryEnd", response);
			if (response.Error == 0) {
				changeBtnRegisterDate();
			} else {
				//console.log(response.message);
			}
		});
	});



	$("body").on("change", "#inputSearchCounselor", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();

		//console.log(`#inputSearchCounselor`, );
	})

	$("body").on("click", ".btnRegisterDate", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		const item = $(this).attr("item");
		const numHistory = $(this).attr("numhistory");
		const inputFields = $("#inputFields").val();
		let inputPlaceHistory = $("#inputPlaceHistory").val();
		const inputComments = $("#inputComments").val();
		const inputOthers = $("#inputOthers").val();
		$(".icon", this).remove();
		loadBtnLoading(e);

		const system = "counseling";
		const module = "advised";

		if (inputPlaceHistory != "office") {
			inputPlaceHistory = inputOthers;
		}

		updateHistoryRegister({
			acuId: item,
			hisId: numHistory,
			inputFields: inputFields,
			inputPlaceHistory: inputPlaceHistory,
			inputComments: inputComments,
		}).then((response) => {
			//console.log("updateHistoryRegister", response);
			if (response.Error == 0) {

				let formH = $("#historyAdvisedHistory .listHistory");
				//console.log("formH", formH.length);
				if (formH.length == 0) {
					$("#historyAdvisedHistory .history").html(renderListHistory());
				}

				//console.log("borrar datos");
				clearFormHistory();

				$("#historyAdvisedHistory .listHistory").prepend(
					renderHistory({
						comments: inputComments,
						id: numHistory,
						field: response.flield,
						dateInit: response.dateInit,
						dateEnd: response.dateEnd,
						place: inputPlaceHistory,
					})
				);
				$("#history" + numHistory).addClass("animated bounceIn");
				$("#inputComments").focus();
			} else {
				//console.log("Error, updateHistoryRegister", response.message);
			}
		});
	});

	$("body").on("click", `#btnAfterhoursRegistrationHistory`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		$(".dropdown").removeClass("on");
		$(".btnDropdown").removeClass("on");
		let data = $(this).data();
		let inputComments = $(`#inputComments`).val();

		if (inputComments == "") {
			$(`#inputComments`).focus();
			alertMessageError({ message: "Debe ingresar un comentario" });
		} else {
			addHtml({
				selector: "body",
				type: 'prepend',
				content: renderModalClean({
					id: "modalAfterhoursRegistrationHistory", title: 'Registro fuera de hora', body: renderAfterhoursRegistrationHistory({
						dateInit: '',
						dateEnd: '',
						item: data.id
					})
				}),
			}) //type: html, append, prepend, before, after

			$(`#inputDateInitAfterhoursRegistration`).datetimepicker({
				timepicker: false,
				datepicker: true,
				format: "Y-m-d H:i",
				lang: "es",
			})

			$(`#inputDateEndAfterhoursRegistration`).datetimepicker({
				timepicker: false,
				datepicker: true,
				format: "Y-m-d H:i",
				lang: "es",
			})
		}


	});
});

$("body").on("click", `#btnAfterhoursRegistrationHistorySave`, function (e) {
	e.preventDefault();
	e.stopPropagation();
	e.stopImmediatePropagation();
	let inputDateInit = $(`#inputDateInitAfterhoursRegistration`).val();
	let inputDateEnd = $(`#inputDateEndAfterhoursRegistration`).val();
	let data = $(this).data();
	let inputComments = $(`#inputComments`).val();
	let inputFields = $(`#inputFields`).val();
	let inputPlaceHistory = $(`#inputPlaceHistory`).val();
	let inputOthers = $(`#inputOthers`).val();


	let validDataInit = validateDate(inputDateInit);
	let validDataEnd = validateDate(inputDateEnd);

	console.log(`.btnAfterhoursRegistrationHistorySave`, data, inputDateInit, inputDateEnd, validDataEnd, validDataInit);



	if (inputDateEnd == "" || inputDateInit == "" || validDataInit == 0 || validDataEnd == 0) {
		console.log('message error')
		addMessageToForm({ selector: '#messagesAfterhoursRegistrationHistory', cls: "messageDanger", message: "Debe ingresar las fechas de inicio y fin", timer: 5000 });
	} else {
		afterhoursRegistrationHistory({ inputDateInit, inputDateEnd, acuId: data.id, inputComments, inputFields, inputPlaceHistory, inputOthers });
	}
});