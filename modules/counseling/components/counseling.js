
import { loadView, accessToken, capitalize } from '../../components/functions.js';
import { renderDashboardSection, renderGeneralBlock, blockSimpleData } from '../../components/renders/renderDashboard.js';

let system = "counseling";
let module = "counseling";

export function counselingIndex() {
    getData({
        task: 'dashboard',
        return: 'returnArray', // returnId, returnState, returnArray
    }).then((response) => {
        //console.log('getData dashboard', response);
        if (response.status == 'success') {
            loadView(_PATH_WEB_NUCLEO + "modules/counseling/views/dashboard.html?" + _VS).then((responseView) => {
                //console.log('loadView', responseView);
                let str = responseView;
                str = str.replace(/\{{_NAME_USER}}/g, userData.userName);
                str = str.replace(/\{{_MODULE}}/g, module);
                $(`.bodyModule[system='${system}'][module='${module}']`).html(str);
                dashboard(response.data);
            }).catch(console.warn());
        } else {
            alertMessageError({ message: response.message })
        }
    }).catch(console.warn());
}

export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/counseling/controllers/apis/v1/counseling.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("Error: getData", error);
    }
}

export const dashboard = (vars = []) => {
    //console.log('dashboard', vars);
    let counselor = vars.general.counselor;
    //let str = renderDashboardSection({ title: "Consejerias", content: renderGeneralBlock({ id: "dataGeneral", data: vars }), clsBody: "flex" });
    let str = blockSimpleData({
        id: "dataConsejeriasAgendadas",
        cls: "col col15w boxYellow",
        label: 'Consejerias Agendadas',
        value: vars.general.report.todayReport.num
    });
    str += blockSimpleData({
        id: "dataPersonasAtendidas",
        cls: "col col15w boxBlue",
        label: 'Personas atendidas',
        value: '0'
    });

    let dataValuesToday = '';
    let dataValuesLast = '';

    counselor.forEach(element => {
        let name = element.name + " " + element.lastname;
        let num = element.todayAppointments == 0 ? 0 : element.todayAppointments.length;
        let dailyAttendance = element.dailyAttendance == 0 ? 0 : element.dailyAttendance;
        let numAtendidas = (element.todayAppointmentsAttended == 0 || element.todayAppointmentsAttended == undefined) ? 0 : element.todayAppointmentsAttended.length;
        //let numAtendidas = 8;
        let value = 0;
        let color = '';
        let aux = '';

        if (numAtendidas == 1) {
            color = 'alert';
            value = 12;
        }
        if (numAtendidas == 2) {
            color = 'alert';
            value = 25;
        }
        if (numAtendidas == 3) {
            color = 'alert';
            value = 34;
        }

        if (numAtendidas == 4) {
            color = 'warning';
            value = 50;
        }
        if (numAtendidas == 5) {
            color = 'warning';
            value = 60;
        }

        if (numAtendidas == 6) {
            color = 'success';
            value = 70;
        }
        if (numAtendidas == 7) {
            color = 'success';
            value = 85;
        }
        if (numAtendidas >= 8) {
            color = 'success';
            value = 100;
            aux = `<span class="crown" > <svg width="25" height="16" viewBox="0 0 25 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12.4067 11.6484C9.42577 11.6484 6.44423 11.6484 3.46332 11.6484C2.85295 11.6484 2.65703 11.5076 2.51322 10.9514C2.01714 9.03369 1.52042 7.11656 1.03752 5.19583C0.996079 5.02969 0.878651 5.00321 0.767503 4.94482C0.186643 4.63543 -0.105357 4.03532 0.0346772 3.42557C0.180363 2.78994 0.693404 2.36678 1.37725 2.31863C2.01525 2.27349 2.61495 2.6521 2.83285 3.24619C3.0181 3.75181 2.92391 4.2171 2.5622 4.61978C2.43724 4.75883 2.41024 4.82925 2.56409 4.97311C3.16944 5.53892 3.75155 6.1276 4.35314 6.69763C5.38487 7.67575 7.19025 7.60232 8.11523 6.49056C9.19657 5.19161 10.2113 3.8427 11.2669 2.52389C11.3906 2.36919 11.2757 2.31261 11.2129 2.22413C10.8619 1.73055 10.8368 1.11779 11.1709 0.644078C11.5112 0.161335 11.9979 -0.0583675 12.5988 0.0132615C13.2155 0.0872981 13.6808 0.517072 13.824 1.10756C13.9194 1.49941 13.8397 1.86839 13.5954 2.19102C13.4943 2.32465 13.473 2.4029 13.5866 2.54736C14.5819 3.80899 15.5603 5.08327 16.5562 6.3443C17.5628 7.61857 19.3425 7.73655 20.5249 6.61637C21.1051 6.06681 21.676 5.50762 22.2593 4.96108C22.3818 4.84671 22.3912 4.78772 22.2681 4.65771C21.6741 4.02749 21.767 3.09391 22.4578 2.59371C23.124 2.11157 24.0892 2.27048 24.5514 2.93861C25.0318 3.63263 24.795 4.5295 24.0107 4.94543C23.8889 5.00983 23.8349 5.08026 23.8022 5.20847C23.3124 7.11717 22.8151 9.02466 22.319 10.9322C22.184 11.451 21.9767 11.6039 21.4122 11.6057C19.9057 11.6099 18.3999 11.6153 16.8934 11.6178C15.3976 11.6196 13.9018 11.6178 12.4061 11.6178C12.4061 11.628 12.4061 11.6382 12.4061 11.6484H12.4067Z" fill="#FEBF10" />
                <path d="M12.4393 15.9997C9.50106 15.9997 6.56348 15.9997 3.62527 15.9997C2.98538 15.9997 2.74676 15.7662 2.74613 15.1438C2.74613 14.7231 2.7455 14.3017 2.74613 13.881C2.74738 13.3085 3.00547 13.0587 3.60203 13.0587C6.90571 13.0581 10.21 13.0587 13.5137 13.0587C16.0858 13.0587 18.6579 13.0587 21.2294 13.0587C21.885 13.0587 22.1299 13.2977 22.1312 13.9339C22.1312 14.3649 22.1343 14.7959 22.1299 15.2269C22.1249 15.7379 21.8486 15.9997 21.3154 15.9997C18.3565 16.0003 15.3976 15.9997 12.4386 15.9997H12.4393Z" fill="#FEBF10" />
            </svg></span>`;
        }

        let html = '';
        if (value <= 50) {
            html = '<div class="circle-right"  color="' + color + '"><div class="mask-right" style="transform:rotate(' + (value * 3.6) + 'deg)"></div></div>';
        } else {
            value -= 50;
            html = '<div class="circle-right" color="' + color + '"><div class="mask-right" style="transform:rotate(180deg)"></div></div>';
            html += '<div class="circle-left" color="' + color + '"><div class="mask-left"  style="transform:rotate(' + (value * 3.6) + 'deg)"></div></div>';
        }


        dataValuesToday += `<div class="boxData" couId="">
                                ${aux}
                                <div class="progress">

                                    ${html}
                                    <label>${numAtendidas}:${num}/${dailyAttendance}</label>
                                </div>
                                <div class="name">${name}</div>
                            </div>`;
    });

    str += renderDashboardSection({
        id: "dataDiaria",
        cls: "col col65w boxGray",
        clsBody: "dataDiaria",
        title: "Datos del día",
        content: dataValuesToday
    });

    $(`.bodyModule[system='${system}'][module='${module}'] .tbody`).html(str);

    /* let labels = [];
    let dataValuesToday = [];
    let dataValuesLast = [];

    counselor.forEach(element => {
        labels.push(element.name + " " + capitalize(element.lastname.charAt(0) + "."));
        dataValuesToday.push(element.todayAppointments.length);
        dataValuesLast.push(element.lastAppointments.length);
    });

    const data = {
        labels: labels,
        datasets: [{
            label: 'Hoy',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: dataValuesToday,
        }, {
            label: 'Hace 10d',
            backgroundColor: '#00bcd4',
            borderColor: '#00bcd4',
            data: dataValuesLast,
        }]
    };

    const config = {
        type: 'bar',
        data: data,
        options: {}
    };


    const myChart = new Chart(
        document.getElementById('dataGeneral'),
        config
    ); */



}

document.addEventListener("DOMContentLoaded", function () {
    //counselingIndex()
})