import {
    dateFormat
} from "../../../components/functions.js";

import { renderInputDate } from '../../../components/renders/renderForms.js';

import { renderModalClean } from '../../../components/renders/renderModals.js';

import {
    todayTime
} from '../../../components/dates.js';


export function renderListHistory() {
    return `<div class="formControl">
        <div class="group formControlSearch">
            <i class="icon icon-search iconSearch btnSearchCoun "></i>
            <input class="formInput" id="inputSearchCounselor" name="inputSearchCounselor" type="text" value="">
            <i class="icon icon-circle-close iconClear btnSearchClear" for="inputSearchCounselor" restore="listConunselors"></i>
        </div>
    </div>
    <div class="group listHistory">
    </div>`
}

export function renderHistory(vars) {
    //console.log("renderHistory", vars)
    let id = vars.id
    let place = vars.place
    let field = vars.field
    let type = vars.type
    let dateInit = dateFormat({
        date: vars.dateInit
    })
    let dateEnd = dateFormat({
        date: vars.dateEnd
    })
    let extEnd = ''
    let extField = ''
    let extType = ''
    let fieldsHistory = JSON.parse(localStorage.getItem('fieldsHistory'))
    //console.log(vars.dateEnd)
    //console.log(fieldsHistory)
    if (type != 0 && type != undefined) {
        extType = `<span class="typeHistory" lang="es"> ${type} </span>`;
        // serach id in fieldsHistory
        console.log("field", parseFloat(field));
        if (field != 0 && field != undefined && !isNaN(parseFloat(field))) {
            let fieldArray = fieldsHistory.find(element => element.id == field);
            field = fieldArray.name;
            console.log("field", field);
        }
    } else {
        extType = "";
    }


    if (vars.dateEnd == "undefined" && vars.dateEnd == "") {
        extEnd = `<a class="btn btnMini btnDanger" item="${id}" >Cerrar Historial</a>`
    } else {
        extEnd = `<div class="date"><span>fin: ${dateEnd}</span> ${extType}</div>`
    }

    if (field != "undefined" && field != "" && field != null) {
        extField = `<span class="date">Tipo: ${field}</span>`
    }
    return `
        <div class="history history-${vars.id} ${vars.clss}" id="history${vars.id}">
            <div class="formControlHead">
                <label for="">
                    <span class="num">Nro. ${vars.id}</span>  
                    <span class="date">Inicio: ${dateInit}</span>  
                    <span class="date">Lugar: ${place}</span>
                    ${extField}
                </label>
                ${extEnd} 
            </div>
            <div class="formControl comments">${vars.comments}</div>
        </div>
    `
}

export function renderAfterhoursRegistrationHistory(vars) {
    let nowInit = todayTime();
    let nowEnd = todayTime({ addHours: 1 });
    return /*html*/ `
         <form class="form">
            <div class="formControl formFlex">
            ${renderInputDate({ cls: "form50w", label: 'Fecha y hora inicio', id: 'inputDateInitAfterhoursRegistration', value: nowInit }) +
        renderInputDate({ cls: "form50w", label: 'Fecha y hora fin', id: 'inputDateEndAfterhoursRegistration', value: nowEnd })}
                
            </div>
            <div class="formControl" id="messagesAfterhoursRegistrationHistory">
            </div>
            <div class="formControl formBtns">
                    <a class="btn btnLink btnCancelModalRender" for="modalAfterhoursRegistrationHistory">Cancelar</a>
                    <a class="btn btnPrimary" id="btnAfterhoursRegistrationHistorySave" data-id="${vars.item}" ><i></i><span>Registrar Sesión</span></a>
                </div>
         </form>
    `;
}
