import {
    listHours
} from "../../../components/forms.js";
import {
    renderBtnStateSwitch,
    renderSelectHoursStartEnd,
    renderSelect,
} from "../../../components/renders/renderForms.js";

import {
    calendarYear,
    y2k
} from "../../../components/calendar.js";
import {
    literalDay
} from "../../../components/dates.js";
import {
    formConfigCalendar,
    loadFormHoursBlocked,
    selectTypesAppointments
} from "../calendarAdvised.js";


export const renderListCounselors = (array) => {
    let str = "";
    let aux = "";
    //console.log("renderListCounselors", array);
    if (array != undefined) {
        for (let index = 0; index < array.length; index++) {
            const element = array[index];
            if (index == 0) {
                aux = "active";
            } else {
                aux = "";
            }
            str += `<li>
                    <a class="btnCalendarCounselor counselor counselor-${element.id
                }  ${aux}" item="${element.id}">
                        <span>${element.name + " " + element.lastname}<span>
                        <span class="state"><span>
                    </a>
                </li>`;
        }
        return str;
    } else {
        return 0;
    }
};

export const renderCalendarAdvised = (array) => {
    let str = "";
    let aux = "";
    for (let index = 0; index < array.length; index++) {
        const element = array[index];
        if (index == 0) {
            aux = "active";
        } else {
            aux = "";
        }
        str += ` <div class="calendar calendar-${element.id} ${aux}" id="calendarAdvised${element.id}"></div>`;
    }
    return str;
};

export const renderFormCalendar = (vars) => {
    console.log("renderFormCalendar", vars);
    let time;
    let timeDateHours;
    let timeDatesHoursInit;
    let timeDatesHoursEnd;
    let hr = vars.hour;
    let hStart;
    let hEnd;
    let nameForm = "formCalendar";
    let type = vars.type ? vars.type : "appointment";
    let typesAppointmentsArray = JSON.parse(localStorage.getItem("typesAppointments"));
    let typeAppointment;

    if (typesAppointmentsArray && typesAppointmentsArray.length > 0) {
        typeAppointment = typesAppointmentsArray.find((element) => element.path == type);
    } else {
        // Handle the case where typesAppointmentsArray is 0 or empty
        typeAppointment = { /* default values or handling */ };
    }

    if (vars.view == "month") {
        hStart = "00:00";
        hEnd = "01:00";
    } else {
        hStart = hr;
        hEnd = moment(hr, "HH:mm").add(1, "hours").format("HH:mm");
    }


    let dataBase = {
        date: vars.date,
        day: vars.day,
        month: vars.month,
        year : vars.year,
        hour : vars.hour,
        hourSelected: vars.hourSelected,
        dayLiteral: vars.dayLiteral,
        monthLiteral: vars.monthLiteral,
        hStart : hStart,
        hEnd : hEnd,
    };
    
    

    let id = vars.id;

    time = `<div class="hours">
                <div class="hourStart">${listHours("inputHourStart", hStart)}</div> - 
                <div class="hourEnd">${listHours("inputHourEnd", hEnd)}</div>   
            </div>`;

    return /*html*/ `
        <div class="modal modalFormCalendar modalDraggable" id="${nameForm}">
            
            <div class="head">
                <label>Agendar Cita: ${vars.counselorName}</label>
                <div class="actions">
                    <div class="boxColorFormCalendar"></div>
                    <select id="selectTypeFormCalendar">
                        ${vars.typesFormCalendar}
                    </select>
                    <button class="btnModalClose" for="${nameForm}"><i class="icon icon-close"></i></button>
                </div>
            </div>
            <form class="form" id="form${id}" autocomplete="off" >
                <input type="hidden" id="inputTypeAppointment" name="inputTypeAppointment" value="${type}">
                <input type="hidden" id="inputTypeDate" name="inputTypeDate" value="hours">
                <input type="hidden" id="inputCounselorId" name="inputCounselorId" value="${vars.id}">
                <textarea style="display:none" id="inputDateBase" name="inputDateBase">${ JSON.stringify(dataBase) }</textarea>

                <div class="headTypeAppointment">
                    <div class="formControl formSelectList">
                        <label for="">Registro de Usuario:</label>
                        <div class="boxInput">
                            <i class="icon icon-doc"></i>
                            <input class="formInput inputSelectList" id="inputAccountLabel" name="inputAccountLabel" for="inputAccount" autocomplete="nope" type="text" />
                            <input id="inputCouId" name="inputCouId" value="${vars.counselorId}" autocomplete="nope" type="hidden" />
                            <input id="inputAccount" name="inputAccount" autocomplete="nope" type="hidden" />
                        </div>
                        <div class="boxSelectList" for="inputAccount">
                            <div class="search searchAccount">
                                <i class="icon icon-search"></i>
                                <a class="btn btnIcon btnCreateUser" data-counselor="${vars.counselorId}"><i class="icon icon-user-plus"></i></a>
                                <input type="text" class="formInput" data-counselor="${vars.counselorId}" data-counselor="${vars.counselorId}" id="inputSearchAccountUsers" for="inputAccount">
                            </div>
                            <div class="otherResults" for="inputAccount"> </div>
                            <!--<div class="search">
                                <i class="icon icon-search"></i>
                                <input type="text" class="formInput inputSearch" id="inputSearchAccount" for="inputAccount">
                            </div>
                            <div class="list" for="inputAccount">
                                
                            </div>
                            <div class="otherResults" for="inputAccount"> </div>
                            <div class="btns" for="inputAccount"> </div>-->
                        </div>
                    </div>
                    <!--<div class="formControl formSelectList">
                        <label for="">Registro de Usuario:</label>
                        <div class="boxInput">
                            <i class="icon icon-search"></i>
                            <a class="btn btnIcon btnCreateUser" data-counselor="${vars.counselorId}"><i class="icon icon-user-plus"></i></a>
                            <input type="text" class="formInput" data-counselor="${vars.counselorId}" id="inputSearchAccountCalendarAdvised" autocomplete="off" for="inputAccount">
                            <input id="inputCouId" name="inputCouId" value="${vars.counselorId}" autocomplete="nope" type="hidden" />
                            <input id="inputAccount" name="inputAccount" autocomplete="nope" type="hidden" />
                        </div>
                        <div class="boxSelectList" for="inputAccount">
                        </div>
                    </div>-->
                </div>
                <div class="formControl" style="z-index:10">
                    <div id="spaceDateFormCalendar">
                        <div class="formControl formControlDate on" for="hours">
                            <i class="icon icon-clock"></i><span class="label" lang="es">${vars.dayLiteral},${vars.day} de ${vars.monthLiteral} de ${vars.year}</span> ${time}
                            <input id="inputDateStart" name="inputDateStart" value="${vars.date}" autocomplete="nope" type="hidden" />
                            <input id="inputDateEnd" name="inputDateEnd" value="" autocomplete="nope" type="hidden" />
                        </div>
                    </div>
                </div>
                <div class="formControl formControlSelectAndOthers" style="z-index:8">
                    <label for="">Ubicación: </label>
                    <div class="boxGroup">
                        <select class="formInput" name="inputPlace" id="inputPlace" for="inputOthers">
                            <option value="office" selected>En oficinas</option>
                            <option value="others">En otro lugar</option>
                        </select>
                        <input type="text" placeholder="Indique el lugar..." class="formInput" id="inputOthers" name="inputOthers">
                    </div>
                </div>
                <div class="formControl" style="z-index:7">
                    <label for="">Detalles</label>
                    <textarea class="formInput" name="inputComments" id="inputComments" rows="2"></textarea>
                </div>
                <div class="message" id="messageRegisterShedule" style="z-index:6"></div>
                <div class="formGroup formActions" style="z-index:5">
                    <a class="btn btnLink btnModalClose" for="${nameForm}"><span>Cancelar</span></a>
                    <a class="btn btnPrimary btnRegisterShedule" id="btnRegisterShedule"  form="form${id}">
                        <i class=""></i>
                        <span>Guardar</span>
                    </a>
                </div>
            </form>
        </div>
    `;
};

export const renderOpenFormCalendar = (vars) => {
   // console.log("🚀 ~ file: renderCalendarAdvised.js:189 ~ renderOpenFormCalendar ~ vars:", vars)
    let time;
    let valuePlace;
    let auxOffice = "selected";
    let auxOther;
    let auxClsOther;
    let place = vars.place ? vars.place : "";
    let hStart = vars.hourStart;
    let hEnd = vars.hourEnd;
    let acuId = vars.acuId;
    let rolId = vars.rolId;
    let btnHistorial = "";
    let title = vars.title;
    let email = vars.email;
    let dial = vars.dataAcuId.dial;
    let celular = vars.dataAcuId.celular;
    let linkCelular = vars.linkCelular;
    let canEditHistory = vars.canEditHistory;
    let canCreateUser = vars.canCreateUser;
    let localStorageTypesAppointments = JSON.parse(localStorage.getItem("typesAppointments"));
    let typesAppointments =  localStorageTypesAppointments.find( type => type.path == vars.type );
    let titleForm = "Cita agendada ";
    let typesAppointmentsColor = "#3a87ad";
    let typeAppointmentPath = "";

    console.log("typesAppointments", typesAppointments);
    if (typesAppointments != "" && typesAppointments != undefined){
        titleForm =  typesAppointments.name ?? "Cita agendada ";
        typesAppointmentsColor =  typesAppointments.color ?? "#3a87ad";
        typeAppointmentPath =  typesAppointments.path ?? "";
    }

    if (celular != "") {
        title = `<span class="name"><b><a class="btnEditDataAccount" data-id="${acuId}" data-name="${title}" data-celular="${celular}" data-email="${email}">${title}</a></b> </span>  <div class="phone"><i class="icon icon-whatsapp"></i><a target="_blank" title="${dial}${celular}" href="${linkCelular}${dial}${celular}">${celular}</a></div>`;
    }

    

    let id = vars.item;

    let counselors = JSON.parse(localStorage.getItem("counselors"));
    let arrayCounteselors = [];
    for (let j = 0; j < counselors.length; j++) {
        const counselor = counselors[j];

        const co = {
            id: counselor.id,
            name: counselor.name + " " + counselor.lastname,
        };
        arrayCounteselors[j] = {
            ...co
        };
    }
    let selectCounselors = renderSelect({
        array: arrayCounteselors,
        id: "inputCounselors",
        selected: vars.counselorId,
    });
    //console.log(counselors);

    time = `<div class="hours">
                <div class="hourStart">${listHours(
        "inputHourStart",
        hStart
    )}</div> - 
                <div class="hourEnd">${listHours("inputHourEnd", hEnd)}</div>   
            </div>`;

    if (place != "") {
        if (place == "office") {
            valuePlace = "";
            auxOffice = "selected";
            auxOther = "";
            auxClsOther = "";
        } else {
            valuePlace = place;
            auxOffice = "";
            auxOther = "selected";
            auxClsOther = "on";
        }
    }

    if (rolId == "1" || rolId || "2" && rolId || "3" && rolId || "5" && rolId || "6") {
        if (canEditHistory == "1") {
            btnHistorial = /*html*/ `
                    <a class="btn btnSuccess btnStartHistorial btnAddForm"
                        item="${acuId}" 
                        system="${vars.system}" 
                        module="${vars.module}" 
                        data-cal-id = "${id}"
                        id="btnStartHistorial" fn="formHistoryAdvised" form="form${id}">
                        <i class=""></i>
                        <span>Ir a Historial</span>
                    </a>`;

        }
    }



    return /*html*/ `
        <div class="modal modalFormCalendar modalDraggable" item="${vars.item}" id="formCalendar">
            <div class="head">
                <label lang="es" >
                    <div class="type" style="background:${typesAppointmentsColor}"></div> 
                    ID ${id} - ${titleForm} , ${vars.counselorName}</label>
                <button class="btnModalClose" for="formCalendar"><i class="icon icon-close"></i></button>
            </div>
            <form class="form" id="formCalendar${id}">
                <label class="headUser" title="acuId - ${acuId}">${title}</label>
                <input type="hidden" id="inputSheduleId" name="input" value="${vars.item}">
                <div class="formControl formSelectList">
                    <div class="boxInput">
                        <input id="inputCouId" name="inputCouId" value="${vars.counselorId}" autocomplete="nope" type="hidden" />
                        <input id="inputName" name="inputName" value="${vars.title}" autocomplete="nope" type="hidden" />
                        <input id="inputAccount" name="inputAccount" value="${acuId}" autocomplete="nope" type="hidden" />
                        <input id="inputTypeAppointment" name="inputTypeAppointment" value="${typeAppointmentPath}" autocomplete="nope" type="hidden" />
                    </div>
                </div>
                
                <div id="spaceDate">
                    <div class="formControl formControlDate">
                        <i class="icon icon-clock"></i>
                        <!--<span class="label" lang="es">${vars.data.dayLiteral}</span> ${time}-->
                        <input id="inputDate" name="inputDate" value="${vars.data.dayLiteral}" class="formInput form50w" autocomplete="nope" type="text" />
                        ${time}
                    </div>
                </div>
                <div class="formControl ListCounselor">
                    <label for="">Consejero:</label>
                    ${selectCounselors} 
                </div>
                <div class="formControl formControlSelectAndOthers">
                    <label for="">Ubicación:</label>
                    <div class="boxGroup">
                        <select class="formInput" name="inputPlace" id="inputPlace" for="inputOthers">
                            <option value="office" ${auxOffice}>En oficinas</option>
                            <option value="others" ${auxOther}>En otro lugar</option>
                        </select>
                        <input type="text" placeholder="Indique el lugar..." class="formInput ${auxClsOther}" id="inputOthers" name="inputOthers" value="${valuePlace}">
                    </div>
                </div>
                <div class="formControl">
                    <label for="">Detalles:</label>
                    <textarea class="formInput" name="inputComments" id="inputComments" rows="2">${vars.details}</textarea>
                </div>
                
                <div class="formGroup formActions">
                    <a class="btn btnFull btnCancelShedule" counselor="${vars.counselorId}" item="${vars.item}" name="${vars.title}"><span>Cancelar Cita</span></a>
                    <a class="btn btnWarning btnStateCancel" counselor="${vars.counselorId}" item="${vars.item}" name="${vars.title}"><i class="icon icon-trash"></i></a>
                    <a class="btn btnInfo btnReshedule" id="btnResheduleCalAdvised"
                        item="${vars.item}"
                        module="${vars.module}"
                        counselor="${vars.counselorId}"
                        data-start="${vars.data.dateStart}"
                        data-end = "${vars.data.dateEnd}"
                        data-hourStart= "${hStart}"
                        data-hourEnd= "${hEnd}"
                    ><span>Reagendar</span></a>
                    
                    ${btnHistorial}
                </div>
            </form>
        </div>
    `;
}

export function renderModalConfigBodyCalendar(vars) {
    return /*html*/ `
        <div class="head">
            <div class="title">
                <i class="icon icon-conf"></i><h2 class="">${vars.title}</h2>
            </div>
            <div class="actions">
                <button type="button" class="btnModalClose btnIcon btn" for="${vars.id}" system="${vars.system}"><i class="icon icon-close"></i></button>
            </div>
        </div>
        <div class="tbody">
            <div class="sidebarMenu" for="${vars.id}">
                 <button type="button" class="btnActionModal btnIconLeft btn active" content="configCalendar" module="${vars.module}" fn="configCalendarAdvised"  ><i class="icon icon-list"></i><span>Ajustes</span></button>
                 <button type="button" class="btnActionModal btnIconLeft btn" content="blockedDatesCalendar" module="${vars.module}" fn="blockedDatesCalendar" ><i class="icon icon-calendar"></i><span>Fechas bloqueadas</span></button>
                 <button type="button" class="btnActionModal btnIconLeft btn" content="typesAppointments" module="${vars.module}" fn="typesAppointments" ><i class="icon icon-blocks"></i><span>Tipos de eventos</span></button>
            </div>
            <div class="contents" id="content${vars.id}">
                ${vars.body}
            </div>
        </div>
    `;
}

export function renderModalConfigContentBodyCalendar(vars) {
    var today = new Date();
    var thisYear = y2k(today.getYear());
    return /*html*/ `
        <div class="content active" id="configCalendar">
            <div class="head">
                <div class="title">
                    <i class="icon icon-list"></i><h2 class="">Ajustes</h2>
                </div>
                <div class="actions">
                    
                </div>
            </div>
            <div class="tbody">
                <form class="form">
                    <label for="">Horarios de Consejeros:</label>
                    <div class="formControl formStateSwitch listConunselorsHours">
                    </div>
                </form>
            </div>
        </div>
        <div class="content" id="blockedDatesCalendar">
            <div class="contentCalendar">
                <div class="tabs" for="boxCalendarBA">
                    <a class="tab active"  item="cal1">${thisYear}</a>
                    <a class="tab"  item="cal2">${thisYear + 1}</a>
                </div>
                <div class="tabsContents" id="boxCalendarBA">
                    <div class="tabContent boxCalendar active"  item="cal1">
                        ${calendarYear({
        year: thisYear,
        fnDay: "blockedDayCalendarAdvised",
    })}
                    </div>
                    <div class="tabContent boxCalendar " item="cal2">
                        ${calendarYear({
        year: thisYear + 1,
        fnDay: "blockedDayCalendarAdvised",
    })}
                    </div>
                </div>
            </div>
        </div>
        <div class="content" id="typesAppointments">
            <div class="head">
                <div class="title">
                    <i class="icon icon-blocks"></i><h2 class="">Tipos de Eventos</h2>
                </div>
                <div class="actions">
                    
                </div>
            </div>
            <div class="tbody">
            </div>
        </div>
    `;
}

export const renderFormBlockContent = (vars) => {
    //console.log("renderFormBlockContent", vars);
    let item = vars.item;
    let check = 'checked'
    let labelBtn = 'Guardar'
    let idBtn = 'btnRegister'
    let btnDelete = '';
    let valCheck = '1';
    let allday = vars.allday ? 'checked' : '';
    //let aux = vars.data.blockedDates.map((item) => { });

    if (item != 0) {
        check = "";
        valCheck = '0';
        labelBtn = "Actualizar";
        idBtn = "btnUpdateBlocked";
        btnDelete = `<a class="btn btnDanger" id="btnDeleteBlocked" for="formBlockDateAdvised" item="${item}">
                        <i class="icon icon-trash"></i>
                    </a>`;

    }

    return /*html*/ `
        <div class="modal modalFormCalendar modalDraggable" id="formBlockDateAdvised"">
            <div class="head">
                <label>Bloquear: ${literalDay({
        dateInput: vars.date,
        type: "inverse",
        mode: "complete",
    })}</label>
                <button class="btnModalClose" for="formBlockDateAdvised"><i class="icon icon-close"></i></button>
            </div>
            <form class="form" >
                <div class="formControl">
                    <div class="checkbox checkboxControl" child="">
                        <input name = "inputAllDay"
                        id = "inputAllDay"
                        type = "checkbox"
                        class = "check"
                        ${allday}
                        value = "${valCheck}" >
                        <div class="checkboxControlLabel"><span>Bloquear todo el día.</span></div>
                    </div>
                </div>
                <div class="formControl others" for="inputAllDay">
                     
                </div>
                <div class="formControl formActions fit">
                    <a class="btn btnLink btnModalClose" for="formBlockDateAdvised"><span>Cancelar</span></a>
                    ${btnDelete}
                    <a class="btn btnPrimary ${idBtn}"
                        id="${idBtn}"
                        item="${item}"
                        date="${vars.date}" 
                        for="formBlockDateAdvised">
                        <span>${labelBtn}</span>
                    </a>
                </div>
            </form>
        </div>
        `;
};

export const renderItemBlock = (vars) => {
    return /*html*/ `
        <div class="list"></div>
        <div class="hours" item="${vars.id}">

        </div>
    `;
};

export const renderBtnAddHours = (vars) => {
    var id = vars.id ? vars.id : "";

    return /*html*/ `
        <i class="icon icon-clock"></i>
        <div class="listHours" item="${id}" id="listHours">
            
            <div class="day" item="${id}" day="1">
                <label>Lunes</label>
                <div class="boxHours" item="${id}" day="1">
                     
                </div>
                <a class="btnAddHours" id="btnAddHours${id}" count="1"  item="${id}" day="1" ><i class="icon icon-circle-plus"></i></a>
            </div>
            <div class="day" item="${id}" day="2">
                <label>Martes</label>
                <div class="boxHours" item="${id}" day="2">
                </div>
                <a class="btnAddHours" id="btnAddHours${id}" count="1" item="${id}" day="2" ><i class="icon icon-circle-plus"></i></a>
            </div>
            <div class="day" item="${id}" day="3">
                <label>Miercoles</label>
                <div class="boxHours" item="${id}" day="3">
                </div>
                <a class="btnAddHours" id="btnAddHours${id}" count="1" item="${id}" day="3" ><i class="icon icon-circle-plus"></i></a>
            </div>
            <div class="day" item="${id}" day="4">
                <label>Jueves</label>
                <div class="boxHours" item="${id}" day="4">
                </div>
                <a class="btnAddHours" id="btnAddHours${id}" count="1" item="${id}" day="4" ><i class="icon icon-circle-plus"></i></a>
            </div>
            <div class="day" item="${id}" day="5">
                <label>Viernes</label>
                <div class="boxHours" item="${id}" day="5">
                </div>
                <a class="btnAddHours" id="btnAddHours${id}" count="1" item="${id}" day="5" ><i class="icon icon-circle-plus"></i></a>
            </div>
            <div class="day" item="${id}" day="6">
                <label>Sabado</label>
                <div class="boxHours" item="${id}" day="6">
                </div>
                <a class="btnAddHours" id="btnAddHours${id}" count="1" item="${id}" day="6" ><i class="icon icon-circle-plus"></i></a>
            </div>
            <div class="day" item="${id}" day="7">
                <label>Domingo</label>
                <div class="boxHours" item="${id}" day="7">
                </div>
                <a class="btnAddHours" id="btnAddHours${id}" count="1" item="${id}" day="7" ><i class="icon icon-circle-plus"></i></a>
            </div>
        </div>
    `;
};

export const renderModalConfigSidebar = (vars) => {
    return /*html*/ `
       <button type="button" class="btnActionModal btnIconLeft btn active" content="configCalendar" module="${vars.module}" fn="configCalendarAdvised"  ><i class="icon icon-list"></i><span>Ajustes</span></button>
       <button type="button" class="btnActionModal btnIconLeft btn" content="blockedDatesCalendar" module="${vars.module}" fn="blockedDatesCalendar" ><i class="icon icon-calendar"></i><span>Fechas bloqueadas</span></button>
       <button type="button" class="btnActionModal btnIconLeft btn" content="typesAppointments" module="${vars.module}" fn="typesAppointments" ><i class="icon icon-blocks"></i><span>Tipos de eventos</span></button>
    `;
};

export const renderFormNewAdviserExpress = (vars = []) => {
    //console.log('formNewAdviserExpress', vars);
    let id = vars.id ? vars.id : "";
    let counselorId = vars.counselorId ? vars.counselorId : "";
    let inputReturn = vars.inputReturn ? vars.inputReturn : "";

    return `<form class="form" id="${id}">
        <label class="head"><h3>Nuevo Aconsejado [express]</h3></label>
        <div class="formControl">
            <label><span class="required">*</span>Nombre(s):</label>
            <input class="formInput" type="text" name="inputName" id="inputName" value="">
            <input  type="hidden" checked name="inputCounselors[]" value="${counselorId}">
            <input  type="hidden" checked name="inputServices[]" value="1">
        </div>
        <div class="formControl">
            <label><span class="required">*</span>Apellido Paterno:</label>
            <input class="formInput" type="text" name="inputLastNameFather" id="inputLastNameFather" value="">
        </div>
        <div class="formControl">
            <label>Apellido Materno:</label>
            <input class="formInput" type="text" name="inputLastNameMother" id="inputLastNameMother" value="">
        </div>
        <div class="formControl formFlex">
            <div class="formControl form50w formInputCalendar">
                <label for="inputBirthday" lang="es"><span class="required">*</span>Fecha de Nacimiento</label>
                <i class="icon icon-calendar"></i>
                <input class="formInput" id="inputBirthday" autocomplete="off" value="" type="text">
            </div>
            <div class="formControl form50w">
                <label for="inputName" lang="es"><span class="required">*</span>Celular</label>
                <div class="formInput formDialCelular" for="inputCelular" state="0" mode="dialcelular">
                    <a class="btnDialCelular" for="inputCelular" form="newFormAdvised">
                        <img src="${_PATH_WEB_NUCLEO}/vendor/svg/flags/bo.svg">
                        <span>+591</span>
                        <i class="icon-chevron-down"></i>
                    </a>
                    <input id="inputCelularDial" name="inputCelularDial" type="hidden" value="+591">
                    <input class="" id="inputCelular" name="inputCelular" type="text" value="">
                </div>
            </div>
        </div>
        <div class="formControl form50w formInputCI formSelect">
            <label for="inputCI" lang="es" class=""><span class="required">*</span>Carnet de Identidad</label>
            <input class="formInput" id="inputCI" type="text" value="">
            <select name="" id="inputExt">
                <option value="SC" selected="">
                    SC
                </option>
                <option value="LP">LP</option>
                <option value="CB">CB</option>
                <option value="CH">CH</option>
                <option value="OR">OR</option>
                <option value="PT">PT</option>
                <option value="TJ">TJ</option>
                <option value="BE">BE</option>
                <option value="PD">PD</option>
                <option value="EXT">EXT</option>
            </select>
        </div>
        <div class="formControl">
            <div class="message" for="${id}"></div>
        </div>
        <div class="formControl formActions normal">
            <a class="btn btnLink btnCancelModal">Cancel</a>
            <a class="btn btnPrimary btnSaveAdviserExpress" data-id="${id}" data-inputreturn="${inputReturn}">Guardar</a>
        </div>
    </form>`;
}

export const renderHeadCalendars = (vars = []) => {
    //console.log('renderHeadCalendars', vars);
    return /*html*/ `
        <div class="head">
        <i class="icon icon-calendar"></i>
                    <span>Agenda Consejería</span>
        </div>

    `;
}

export const renderHeadActionsCalendars = (vars = []) => {
    //console.log('renderHeadCalendars', vars);
    return /*html*/ `    
        <button class="btnConfig" module="${vars.module}" system="${vars.system}">
            <i class="icon icon-conf"></i>
        </button>
    `;
}

export const renderSearchUsersAdvised = (vars = []) => {
    console.log('renderSearchUsersAdvised', vars);
    return /*html*/ `
        <div class="innerSearchUsersAdvised">
            <div class="inner">
                <div class="search">
                    <i class="icon icon-search"></i>
                    <input type="text" name="inputSearchUsersAdvised" id="inputSearchUsersAdvised" placeholder="Buscar Aconsejado">
                    <a class="btnCloseSearchUsersAdvised"><i class="icon icon-circle-close"></i></a>
                </div>
            </div>
        </div>
    `;
}

export const renderEditDataAccount = (vars = []) => {
    return /*html*/ `
        <div class="innerEditDataAccount" id="${vars.id}">
          
            <div class="modalHeader">
                <h3>Editar Datos de Cuenta</h3>
            </div>
            <form class="modalBody form">
                    <div class="formControl">
                        <label for="inputName">Nombre</label>
                        <input type="hidden" class="" name="inputId" id="inputId" value="${vars.item}">
                        <input type="text" class="formInput disabled" name="inputName" id="inputName" value="${vars.name}">
                    </div>
                    <div class="formControl">
                        <label for="inputLastNameFather">Email:</label>
                        <input class="formInput" type="text" name="inputEmail" id="inputEmail" value="${vars.email}">
                    </div>
                    <div class="formControl">
                        <label for="inputLastNameFather"><div class="required">*</div>Celular:</label>
                        <input class="formInput" type="text" name="inputCelular" id="inputCelular" value="${vars.celular}">
                    </div>
                    <div class="formActions normal">
                        <a class="btn btnLink btnCancelModal" for="${vars.id}">Cancel</a>
                        <a class="btn btnPrimary btnSaveEditDataAccount" for="${vars.id}"><i class=""></i><span>Actualizar</span></a>
                    </div>
            </form>
      
        </div>
    `;
}

export const renderWindowCalendarList = (vars = []) => {
    console.log('renderWindowCalendarList', vars);
    let couId = vars.couId;
    let couName = vars.name;
    let arrayData = vars.data;
    let data = '';

    return /*html*/`
        <div class="innerWindow on innerWindowCalendarList" id="innerCalendarList${couId}">
            <div class="header">
                <h3>Agenda de ${couName}</h3>
                <div class="flexLineEnd">
                    <div class="searchBox col30"> 
                        <div class="innerSearch">
                            <i class="icon icon-search"></i>
                            <input type="text" name="inputSearchCalendarList" id="inputSearchCalendarList" placeholder="Buscar Historico Calendario">
                        </div>
                    </div>
                    <button class="btn btnIcon btnCloseInnerWindow" for="innerCalendarList${couId}"><i class="icon icon-close"></i></button>
                </div>
            </div>
            <div class="modalBody">
                <div class="inner">
                    ${data}
                </div>
            </div>
        </div> 
    `;
}