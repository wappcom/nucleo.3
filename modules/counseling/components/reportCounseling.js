import { accessToken, loadView, loadingSelector, alertMessageError } from '../../components/functions.js';
import { convertLiteralToDbDate, unDbDate } from '../../components/dates.js';
import {
    categorysListTable,
    createTable,
    dataTable,
    removeItemTable,
    inactiveBtnStateItem,
    activeBtnStateItem,
    mountTable,
    colCheck,
    colId,
    colName,
    colTitle,
    colState,
    colActionsBase,
    deleteItemModule,
    renderEmpty,
    downloadXLSTable
} from "../../components/tables.js";
import {
    renderCol,
    renderColCheck,
    renderColId,
    renderColTitle,
    renderRowsTable,
    renderColState,
    renderColActions,
    renderColCategorys,
} from "../../components/renders/renderTables.js";

import { loadCounselors  } from "./advised.js";


let module = 'reportsCounseling';
let system = 'counseling';
let tableId = 'tableReportsCounseling';

export const reportsCounselingIndex = (vars = []) => {
    loadView(_PATH_WEB_NUCLEO + "modules/counseling/views/report.html?" + _VS).then((responseView) => {
        //console.log('loadView', responseView);
        loadCounselors().then((listData) => {
            //console.log('loadCounselors', listData);
            let str = responseView;
            let counselorArray = JSON.parse(localStorage.getItem('counselors'));
            //console.log('counselorArray', counselorArray);
            if (counselorArray == null) {
                counselorArray = [];
            }
            let _LIST_COUNSELORS = counselorArray.map((item) => {
                return `<option value="${item.id}">${item.name} ${item.lastname}</option>`;
            });

            str = str.replace(/\{{_COUNSELORS}}/g, _LIST_COUNSELORS);
            $(`.bodyModule[module='${module}']`).html(str);
            // date today
            let date = new Date();
            // agregar cero a la izquierda 
            let day = ("0" + date.getDate()).slice(-2);
            let mouth = ("0" + (date.getMonth() + 1)).slice(-2);
            let dateInit = day + "-" + mouth + "-" + date.getFullYear();

            $(`#inputInit`).val(dateInit);
            $(`#inputEnd`).val(dateInit);

            $("#inputInit").datetimepicker({
                timepicker: false,
                format: "d-m-Y",
                lang: "es",
            });
            $("#inputEnd").datetimepicker({
                timepicker: false,
                format: "d-m-Y",
                lang: "es",
            });
        }).catch(console.warn());
    }).catch(console.warn());
}

export const reportsCounselingIndexGenerate = (vars = []) => {
    //console.log('reportsCounselingIndex', vars);

    let dateInit = vars.init
    let dateEnd = vars.end
    let couId = vars.couId
    let type = vars.type

    getData({
        task: 'getReportsCounseling',
        return: 'returnArray', // returnId, returnState, returnArray
        inputs: {
            dateInit,
            dateEnd,
            couId,
            type
        }
    }).then((response) => {
        console.log('getData reportsCounselingIndexGenerate', response);
        if (response.status == 'success') {
            let rows = "";
            for (let i in response.data) {
                let item = response.data[i];
                //console.log("item", item);
                rows += renderRowsTable({
                    id: item.id,
                    content:
                        renderColId({
                            id: item.id,
                        }) +
                        renderColTitle({
                            id: item.id,
                            title: item.name + " " + item.lastname,
                        }) +

                        renderCol({
                            id: item.id,
                            cls: '',
                            attr: '',
                            data: item.cantidadConsejerias,
                        }) +
                        renderCol({
                            id: item.id,
                            cls: '',
                            attr: '',
                            data: item.cantidadPersonas,
                        }) +
                        renderCol({
                            id: item.id,
                            cls: '',
                            attr: '',
                            data: item.cantidadCumplimiento,
                        }) +
                        renderCol({
                            id: item.id,
                            cls: '',
                            attr: '',
                            type: 'array',
                            data: item.datosConsejerias,
                        })
                });
            }

            mountTable({
                id: tableId,
                columns: [
                    ...colId,
                    {
                        label: "Nombre Completo",
                        cls: "colNameFull",
                    },

                    {
                        label: "cantidad Consejerias",
                        cls: "",
                    },
                    {
                        label: "Cantidad de Personas Atendidas",
                        cls: "",
                    },
                    {
                        label: "Cumplimiento",
                        cls: "",
                    },
                    {
                        label: "Datos",
                        cls: "",
                    }
                ],
                rows,
                module,
                system,
                container: ".bodyModule[module='" + module + "'] .tableContainer",
            });

            const name = vars.type + "-" + dateInit + "-to-" + dateEnd;

            $(".bodyModule[module='" + module + "'] #tableContainerHead").html(`<div class="section">
                <div class="left">
                    <h3>Resultado de Reporte</h3>
                </div>
                <div class="right">
                    <button class="btn btnFull btnSmall" data-table="tableReportsCounseling" data-name="${name}" id="descargarXLSReport">Descargar XLS</button>
                </div>
            </div>`);

            //descargarXLS
            /*const table = 'tableContainer';
            const name = vars.type;
            var uri = 'data:application/vnd.ms-excel;base64,',
                template = `<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>`,
                base64 = function (s) {
                    return window.btoa(unescape(encodeURIComponent(s)))
                },
                format = function (s, c) {
                    return s.replace(/{(\w+)}/g, function (m, p) {
                        return c[p];
                    })
                }
            var toExcel = document.getElementById(table).innerHTML;
            var ctx = {
                worksheet: name || '',
                table: toExcel
            };
            var link = document.createElement("a");
            link.download = name + "-" + dateInit + "-to-" + dateEnd + ".xls";
            link.href = uri + base64(format(template, ctx))
            link.click(); */

        } else {
            alertMessageError({ message: response.message })
        }
    }).catch(console.warn());
}


export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/counseling/controllers/apis/v1/counseling.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("Error: getDataPlaces", error);
    }
}

document.addEventListener("DOMContentLoaded", function () {
    $("body").on("click", `#btnGenerateReportCounseling`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`#btnGenerateReportCounseling`, data);

        let inputType = $(`#inputType`).val();
        let inputCounselor = $(`#inputCounselor`).val();
        let inputInit = unDbDate($(`#inputInit`).val());
        let inputEnd = unDbDate($(`#inputEnd`).val());

        loadingSelector({ selector: '.tableContainer' });

        reportsCounselingIndexGenerate({ type: inputType, couId: inputCounselor, init: inputInit, end: inputEnd })

    });

    $("body").on("click", `#descargarXLSReport`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let tableId = data.table;
        let name = data.name;

        downloadXLSTable({ tableId, name });

    });
});