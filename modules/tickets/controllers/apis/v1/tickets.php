<?php
$access = json_decode($_POST["accessToken"], true);
//echo json_encode($access); exit(0);
require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $access["page"] . "config.php");
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

require_once(_PATH_NUCLEO . "modules/tickets/models/class/tickets.class.php");
$tickets = new TICKETS($fmt); 

header("Content-Type: application/json");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

switch ($_SERVER["REQUEST_METHOD"]) {
    case 'POST':
        $access_token = $access['access_token'];
        $refresh_token = $access['refresh_token'];
        $entitieId = $access['entitieId'];
        $action = $fmt->auth->getInput('action');
        //echo json_encode("1:". $action); exit(0);
        //echo json_encode("2:". $entitieId); exit(0);
        
        $userId = $fmt->users->validateUserToken(array("access_token" => $access_token, "refresh_token" => $refresh_token, "idEntitie" => $entitieId));

        //echo json_encode($userId); exit(0);
        if ($userId) {

            $return["user"] = $fmt->users->userDataAuth(array("access_token" => $access_token, "idEntitie" => $entitieId));
            $userId = $return["user"]["userId"];
            $rolId = $return["user"]["rolId"];

            //echo json_encode($userId.":".$rolId); exit(0);
            //halls
            if ($action == "loadHalls") {
                
                $halls =$tickets->loadHalls(array("rolId" => $rolId, 
                                                            "userId" => $userId ,
                                                            "entitieId" => $entitieId));
                //echo json_encode($halls);exit(0);
                
                if (count($halls) > 0) {
                    $resume["Error"] = 0;
                    $resume["items"] = $halls;
                    echo json_encode($resume);
                }elseif($hall==0){
                    echo 0;
                }else{
                    echo $fmt->errors->errorJson([
                        "description" => "Error, ",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
                exit(0);
            }

            if ($action == "saveNewHall") {
                //echo json_encode(1); exit(0);
                $vars = $fmt->forms->serializeToArray(json_decode( $_POST['vars'], true));
                 
                $hallId = json_encode($tickets->saveNewHall(array("rolId" => $rolId, 
                                                            "userId" => $userId ,
                                                            "vars" => $vars,
                                                            "entitieId" => $entitieId))); 
                if ($hallId) {
                    $resume["Error"] = 0;
                    $resume["hallId"] = $hallId;
                    echo json_encode($resume);
                }else{
                    echo $fmt->errors->errorJson([
                        "description" => "Error, ",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
                exit(0);
            }

            if ($action == "updateFormHall") {
                //echo json_encode(1); exit(0);
                $vars = $fmt->forms->serializeToArray(json_decode( $_POST['vars'], true));
                 
                $hallId = json_encode($tickets->updateFormHall(array("rolId" => $rolId, 
                                                            "userId" => $userId ,
                                                            "vars" => $vars,
                                                            "entitieId" => $entitieId))); 
                if ($hallId) {
                    $resume["Error"] = 0;
                    $resume["hallId"] = $hallId;
                    echo json_encode($resume);
                }else{
                    echo $fmt->errors->errorJson([
                        "description" => "Error, ",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
                exit(0);
            }

            if($action == "deleteHall"){
                $vars =  json_decode( $_POST['vars'], true);
                //echo json_encode($vars); exit(0);
                $state = $tickets->deleteHall($vars["item"]);

                if ($state==1){
                    //echo json_encode($state);
                    $resume["Error"] = 0;
                    echo json_encode($resume);
                }else{
                    echo $fmt->errors->errorJson([
                        "description" => "Error, delete Item.",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
                
            }

            if($action == "editItemHall"){
                $item =  json_decode( $_POST['item'], true);
                //echo json_encode($item); exit(0);
                $state = $tickets->dataIdHall(array("rolId" => $rolId, 
                                                    "userId" => $userId ,
                                                    "item" => $item,
                                                    "entitieId" => $entitieId));
                //echo json_encode($state); exit(0);
                if ($state){
                    //echo json_encode($state);
                    $resume["Error"] = 0;
                    $resume["data"] = $state;
                    echo json_encode($resume);
                }else{
                    echo $fmt->errors->errorJson([
                        "description" => "Error, delete Item.",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
                
            }

            //Events
            if($action == "loadEvents") {
                
                $items = $tickets->loadEvents(array("rolId" => $rolId, 
                                                    "userId" => $userId ,
                                                    "entitieId" => $entitieId));
                //echo json_encode($items); exit(0);
                 
                if($items == 0){
                    $resume["Error"] = 0;
                    $resume["items"] = 0;
                    echo json_encode($resume);
                    exit(0);
                }
                
                if (count($items) > 0) {
                    $resume["Error"] = 0;
                    $resume["items"] = $items;
                    echo json_encode($resume);
                    exit(0);
                }else{
                    echo $fmt->errors->errorJson([
                        "description" => "Error, ",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
                exit(0);
            }    

            //Events
            if($action == "loadEventsActives") {
                
                $items = $tickets->loadEventsActives(array("rolId" => $rolId, 
                                                    "userId" => $userId ,
                                                    "entitieId" => $entitieId));
                //echo json_encode($items); exit(0);
                 if($items == 0){
                    $resume["Error"] = 0;
                    $resume["items"] = 0;
                    echo json_encode($resume);
                    exit(0);
                }
                
                if (count($items) > 0) {
                    $resume["Error"] = 0;
                    $resume["items"] = $items;
                    echo json_encode($resume);
                }else{
                    echo $fmt->errors->errorJson([
                        "description" => "Error, ",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
                exit(0);
            }  

            if ($action == "saveEvent"){
                $vars = $fmt->forms->serializeToArray(json_decode($_POST['vars'], true));
                $eventId = $tickets->saveEvent(array("rolId" => $rolId, 
                                                    "userId" => $userId ,
                                                    "vars" => $vars,
                                                    "entitieId" => $entitieId)); 
                
                //echo  json_encode($eventId); exit(0);
                if (is_int($eventId)) {
                    $resume["Error"] = 0;
                    $resume["eventId"] = $eventId;
                    echo json_encode($resume);
                }else{
                    echo $fmt->errors->errorJson([
                        "description" => "Error, ",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
                exit(0);
            }

            if ($action == "loadEvent"){
                $vars = json_decode($_POST['vars'], true);
                $event = $tickets->loadEvent(array("rolId" => $rolId, 
                                                    "userId" => $userId ,
                                                    "vars" => $vars,
                                                    "entitieId" => $entitieId)); 
                
                //echo  json_encode($event); exit(0);
                if ($event) {
                    $resume["Error"] = 0;
                    $resume["data"] = $event;
                    echo json_encode($resume);
                }else{
                    echo $fmt->errors->errorJson([
                        "description" => "Error, ",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
                exit(0);
            }

            if ($action == "updateEvent"){
                $vars = $fmt->forms->serializeToArray(json_decode($_POST['vars'], true));
                //echo  json_encode($vars); exit(0);
                $event = $tickets->updateEvent(array("rolId" => $rolId, 
                                                    "userId" => $userId ,
                                                    "vars" => $vars,
                                                    "entitieId" => $entitieId)); 
                
                //echo  json_encode($event); exit(0);
                if (is_int($event)) {
                    $resume["Error"] = 0;
                    echo json_encode($resume);
                }else{
                    echo $fmt->errors->errorJson([
                        "description" => "Error, ",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
                exit(0);
            }
            
        }else {
            echo $fmt->errors->errorJson([
                "description" => "Access Auth. invalid user",
                "code" => "",
                "lang" => "es"
            ]);
            exit(0);
        }

        
    break;

    default:
        echo $fmt->errors->errorJson([
            "description" => "Access Auth. Metod request.",
            "code" => "",
            "lang" => "es"
        ]);
        exit(0);
    break;
}