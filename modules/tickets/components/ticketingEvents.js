import {
	listJsonToString,
	btnFn,
	loadView,
	replaceAll,
	replacePath,
	stopLoadingBar,
	loadBtnLoading,
	btnLoading,
	btnLoadingRemove,
	accessToken,
	alertPage,
	alertResponseMessageError,
	loadingBtnIcon,
	alertMessageError,
	addHtml,
	activateBtn, 
	deactivateBtn,
	disabledBtn,
	unDisabledBtn,
	changeBtn
} from "../../components/functions.js";
import { financial } from "../../components/dates.js";
import { draggable } from "../../components/modals.js";
import { dataTable, createTable } from "../../components/tables.js";
import {
	sortJSON,
	resetForm,
	errorInput,
	validateNum,
	validateDate,
} from "../../components/forms.js";
import { unDbDate } from "../../components/dates.js";
import { renderSelect } from "../../components/renders/renderForms.js";
import { renderNoDataTable } from "../../components/renders/renderTables.js";
import { renderTrEvents, 
	renderTicketsEventUnit,
	renderTicketsEventTitle,
	renderTicketsEventTotal, 
	renderPayQR
} from "./renders/renderTickets.js";
import { stateBtnAccionsVarSales } from "../../sales/components/salesPointTickets.js";

const system = "tickets";
const module = "ticketingEvents";
const pathurl = "modules/tickets/";
var numMod = 0;

export const ticketingEventsIndex = () => {
	loadView(
		_PATH_WEB_NUCLEO + "modules/tickets/views/ticketingEvents.html"
	).then((str) => {
		loadEvents()
			.then((response) => {
				//console.log("loadEvents", response);
				str = str.replace(/{{_MODULE}}/g, module);
				str = str.replace(/{{_SYSTEM}}/g, system);
				str = str.replace(/{{_PATHURL}}/g, pathurl);
				str = str.replace(/{{_FN}}/g, "formNewEvent");
				str = replacePath(str);
				$(".bodyModule[module='" + module + "']").html(str);
				if (response != null && response.Error == 0) {
					const tableId = "tableEvents";
					const data = response.items;
					let strTable = "";
					let tbody = "";
					//console.log(data);

					for (let i = 0; i < data.length; i++) {
						const elem = data[i];
						elem["table"] = tableId;
						elem["module"] = module;
						elem["system"] = system;
						tbody += renderTrEvents(elem);
					}

					strTable = createTable({
						id: tableId,
						thead: ":check,id:colId,Nombre,Fecha Inicio, Fecha Fin,Estado:colState,Acciones:colActions",
						body: tbody,
					});

					$(".bodyModule[module='" + module + "'] >.tbody").html(
						strTable
					);

					dataTable({
						elem: "#" + tableId,
						orderCol: 1,
					});
				} else if (response === 0) {
					$(".bodyModule[module='" + module + "'] >.tbody").html(
						renderNoDataTable()
					);
				} else {
					alertResponseMessageError({ message: response.message });
				}
			})
			.catch(console.warn());
	});
};

//New Event
export const formNewEvent = () => {
	let halls = JSON.parse(localStorage.getItem("halls"));
	let strHalls = "";
	numMod++;
	//console.log(halls);
	//console.log(_PATH_WEB_NUCLEO + pathurl + "views/formEvent.html?" + _VS + numMod)
	loadView(
		_PATH_WEB_NUCLEO + pathurl + "views/formEvent.html?" + _VS + numMod
	).then((str) => {
		//console.log(htmlView)
		str = str.replace(/{{_MODULE}}/g, module);
		str = str.replace(/{{_SYSTEM}}/g, system);
		str = str.replace(/{{_FORM_ID}}/g, "newFormEvent");
		str = str.replace(/{{_CLS_BTN_ACTION}}/g, "btnSuccess btnSaveEvent");
		str = str.replace(/{{_BTN_ACTION}}/g, "btnSaveEvent");
		str = str.replace(/{{_BTN_NAME_ACTION}}/g, "Publicar");
		str = str.replace(/{{_TITLE}}/g, "Nuevo Evento");
		stopLoadingBar();
		$(".bodyModule[module='" + module + "'] .innerForm").html(str);

		strHalls = renderSelect({ array: halls, id: "inputHalls" });
		//console.log(strHalls);
		$(".bodyModule[module='" + module + "'] .innerForm .listHalls").html(
			strHalls
		);
		$("#inputStatus").val("1");
		$("#inputInitDate").datetimepicker({
			timepicker: true,
			datepicker: true,
			formatTime: "H:i",
			format: "d-m-Y H:i",
			lang: "es",
		});
		$("#inputEndDate").datetimepicker({
			timepicker: true,
			datepicker: true,
			formatTime: "H:i",
			format: "d-m-Y H:i",
			lang: "es",
		});
	});
};

export const saveEvent = async (form) => {
	console.log("saveEvent", form);
	const url =
		_PATH_WEB_NUCLEO + "modules/tickets/controllers/apis/v1/tickets.php";
	const dataForm = new FormData();
	dataForm.append("accessToken", JSON.stringify(accessToken()));
	dataForm.append("action", "saveEvent");

	if (validateForm(form) == 0) {
		//console.log(JSON.stringify($("#" + form).serializeArray()));
		dataForm.append("vars", JSON.stringify($("#" + form).serializeArray()));
		try {
			let res = await axios({
				async: true,
				method: "post",
				responseType: "json",
				url: url,
				headers: {},
				data: dataForm,
			});
			//console.log(res.data);
			return res.data;
		} catch (error) {
			console.log("Error saveEvent");
			console.log(error);
		}
	} else {
		return 0;
	}
};

export const loadEvents = async (vars) => {
	//console.log("loadEvents");
	const url =
		_PATH_WEB_NUCLEO + "modules/tickets/controllers/apis/v1/tickets.php";
	const dataForm = new FormData();
	dataForm.append("accessToken", JSON.stringify(accessToken()));
	dataForm.append("action", "loadEvents");

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: dataForm,
		});
		//console.log(res.data);
		return res.data;
	} catch (error) {
		console.log("Error loadEvents");
		console.log(error);
	}
};

export const loadEventsActives = async (vars) => {
	//console.log("loadEventsActives");
	const url =
		_PATH_WEB_NUCLEO + "modules/tickets/controllers/apis/v1/tickets.php";
	const dataForm = new FormData();
	dataForm.append("accessToken", JSON.stringify(accessToken()));
	dataForm.append("action", "loadEventsActives");

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: dataForm,
		});
		//console.log(res.data);
		return res.data;
	} catch (error) {
		console.log("Error loadEvents");
		console.log(error);
	}
};

export const createTicketEvent = (vars = []) => {
	//console.log("createTicketEvent fn", vars);
	let eventsTickets = JSON.parse(localStorage.getItem("eventsTickets"));
	let eventsTicketNow = JSON.parse(localStorage.getItem("eventsTicketNow"));
	let dataTable = JSON.parse(localStorage.getItem("hallDataNow"));

	if (eventsTickets != null) {
		let dataEvent = eventsTickets.filter((item) => item.id == eventsTicketNow.id);
		//console.log('dataEvent', dataEvent, dataTable);
		//dataSend['vars']['inputs']['eventId'] = dataEvent.id;
		//dataSend['vars']['inputs']['hallId'] = dataEvent.hall;
		let eventId = dataEvent[0].id;
		let hallId = dataEvent[0].hall;
		let tableId = vars.id;
		let level = dataEvent[0].level;
		let coin = dataEvent[0].coin;
		let cpeId = $("#ticketReservationForm").attr("data-cpeid") || "";
		let reserveId = $("#ticketReservationForm").attr("data-reserveid") || "";
		let ref = $("#inputReferenceReserveTicketEvent").val() || "";

		let input = {
			eventId,
			hallId,
			tableId,
			dataTable,
			cpeId,
			level,
			coin,
			reserveId,
			ref
		}

		getData({
			task: 'createTicketEvent',
			return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
			input
		}).then((response) => {
			//console.log('getData createTicketEvent', response);
			if (response.status == 'success') {
				let data = response.data;
				let reserveId = response.reserveId;
				let num = response.num;

				$("#ticketReservationForm").attr("data-reserveid", reserveId);
				$("#ticketReservationTitle").html(renderTicketsEventTitle({ num, reserveId }));

				data.forEach((element,index) => {
					let reserveId = element.reserveId;
					let id = element.id;
					let elemId = $(`.contentTicketsEventUnit[data-id="${id}"]`);
					if (elemId.length == 0) {
						createTicketUnit(element);
					}
				});

				stateBtnAccionsVarSales();
			
			/*else if (response.status == 'noReservation') {
				let data = response.data;
				let reserveId = data.reserveId;
				$("#ticketReservationForm").attr("reserveid", reserveId);
				createTicketUnit({
					id: data.id, reserveId: data.reserveId, eventId, hallId, tableId, level: data.level, coin : data.coin, cost: data.cost,num:1
				}); */

			} else {
				alertMessageError({ message: response.message })
			}
		}).catch(console.warn());
	}
};

export const createTicketUnit = (vars = []) => {
	//console.log("createTicketUnit fn", vars);
	addHtml({
		selector: '#ticketReservationForm',
		type: 'prepend',
		content: renderTicketsEventUnit(vars)
	}) //type: html, append, prepend, before, after
}

export const validateForm = (form) => {
	let count = 0;
	const name = $("#" + form + " #inputName").val();
	const seats = $("#" + form + " #inputNroSeats").val();
	const initDate = $("#" + form + " #inputInitDate").val();
	const endDate = $("#" + form + " #inputEndDate").val();
	const halls = $("#" + form + " #inputHalls").val();
	const costSeat = $("#" + form + " #inputCostSeat").val();

	if (name == "" || name.length < 5) {
		errorInput("inputName", "Agrega un nombre de Sala valido.");
		count++;
	}

	if (seats == "" || !validateNum(seats)) {
		errorInput("inputNroSeats", "Agrega un número  de Butacas valido.");
		count++;
	}

	if (initDate == "" || !validateDate(initDate)) {
		errorInput("inputInitDate", "Agrega una fecha valida.");
		count++;
	}
	if (endDate == "" || !validateDate(endDate)) {
		errorInput("inputEndDate", "Agrega una fecha valida.");
		count++;
	}

	if (halls == "" || halls == 0) {
		errorInput("inputHalls", "Selecciona una sala valida.");
		count++;
	}

	if (costSeat == "" || costSeat == "0,00") {
		errorInput("inputCostSeat", "Ingresa un costo valido.");
		count++;
	}

	return count;
};

//Edit Event
export const formEditEvent = (vars) => {
	let halls = JSON.parse(localStorage.getItem("halls"));
	let strHalls = "";
	numMod++;
	//console.log(vars);
	//console.log(halls);
	//console.log(_PATH_WEB_NUCLEO + pathurl + "views/formEvent.html?" + _VS + numMod)
	loadView(
		_PATH_WEB_NUCLEO + pathurl + "views/formEvent.html?" + _VS + numMod
	).then((str) => {
		//console.log(str);
		loadEvent(vars)
			.then((response) => {
				console.log(response);

				str = str.replace(/{{_MODULE}}/g, module);
				str = str.replace(/{{_SYSTEM}}/g, system);
				str = str.replace(/{{_FORM_ID}}/g, "editFormEvent");
				str = str.replace(/{{_ITEM}}/g, response.data.id);
				str = str.replace(/{{_BTN_ACTION}}/g, "btnUpdateEvent");
				str = str.replace(
					/{{_CLS_BTN_ACTION}}/g,
					"btnUpdateEvent btnInfo"
				);
				str = str.replace(/{{_BTN_NAME_ACTION}}/g, "Actualizar");
				str = str.replace(/{{_TITLE}}/g, "Editar Evento");
				stopLoadingBar();
				//console.log(module);
				//console.log(str);
				$(".bodyModule[module='" + module + "'] .innerForm").html(str);

				strHalls = renderSelect({
					array: halls,
					id: "inputHalls",
					selected: response.data.hall,
				});
				//console.log(strHalls);
				$(
					".bodyModule[module='" + module + "'] .innerForm .listHalls"
				).html(strHalls);

				$("#inputId").val(response.data.id);
				$("#inputName").val(response.data.name);
				$("#inputDescription").val(response.data.description);
				$("#inputInitDate").val(unDbDate(response.data.initDate));
				$("#inputEndDate").val(unDbDate(response.data.endDate));
				$("#inputStatus").val(response.data.state);
				$("#inputCoin").val(response.data.coin);
				$("#inputCostSeat").val(response.data.costSeat);
				$("#inputNroSeats").val(response.data.seats);
				$("#inputNroSeats").removeClass("disabled");

				$("#inputInitDate").datetimepicker({
					timepicker: true,
					datepicker: true,
					formatTime: "H:i",
					format: "d-m-Y H:i",
					lang: "es",
				});
				$("#inputEndDate").datetimepicker({
					timepicker: true,
					datepicker: true,
					formatTime: "H:i",
					format: "d-m-Y H:i",
					lang: "es",
				});
			})
			.catch(console.warn());
	});
};

export const loadEvent = async (vars) => {
	console.log("loadEvent");
	const url =
		_PATH_WEB_NUCLEO + "modules/tickets/controllers/apis/v1/tickets.php";
	const dataForm = new FormData();
	dataForm.append("accessToken", JSON.stringify(accessToken()));
	dataForm.append("action", "loadEvent");
	dataForm.append("vars", JSON.stringify(vars));

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: dataForm,
		});
		//console.log(res.data);
		return res.data;
	} catch (error) {
		console.log("Error: loadEvent");
		console.log(error);
	}
};

export const updateEvent = async (form) => {
	console.log("updateEvent", form);
	const btn = "btnUpdateEvent";
	const url =
		_PATH_WEB_NUCLEO + "modules/tickets/controllers/apis/v1/tickets.php";
	const dataForm = new FormData();
	dataForm.append("accessToken", JSON.stringify(accessToken()));
	dataForm.append("action", "updateEvent");

	console.log("val", validateForm(form));

	if (validateForm(form) == 0) {
		//console.log(JSON.stringify($("#" + form).serializeArray()));
		btnLoading(btn);
		dataForm.append("vars", JSON.stringify($("#" + form).serializeArray()));
		try {
			let res = await axios({
				async: true,
				method: "post",
				responseType: "json",
				url: url,
				headers: {},
				data: dataForm,
			});
			//console.log(res.data);
			btnLoadingRemove(btn);
			return res.data;
		} catch (error) {
			console.log("Error saveEvent");
			console.log(error);
		}
	} else {
		return 0;
	}
};

export const getData = async (vars = []) => {
	//console.log('getData', vars);
	const url = _PATH_WEB_NUCLEO + "modules/tickets/controllers/apis/v1/ticketspoint.php";
	let data = JSON.stringify({
		accessToken: accessToken(),
		vars: JSON.stringify(vars)
	});
	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: data
		})
		//console.log(res.data);
		return res.data
	} catch (error) {
		console.log("Error: getDataTickets", error);
	}
}

export const clearSidebarSales = (vars =[]) => {
	console.log('clearSi',vars)
	$(".varSales form").removeClass("on");
	$(".formSelectTicket").addClass("on");
	$("#ticketReservationForm").html("");
}

document.addEventListener("DOMContentLoaded", function () {
	$("body").on("change", "#inputHalls", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		const id = $(this).val();
		let halls = JSON.parse(localStorage.getItem("halls"));
		$("#inputHalls").removeClass("error");
		//console.log(halls);
		//console.log("change-inputHalss", id);
		const response = halls.find((hall) => hall.id == id);
		//console.log(response);
		//console.log(response.numMaxSeats);
		if (halls != 0) {
			$("#inputNroSeats").val(response.numMaxSeats);
			$("#inputNroSeats").removeClass("disabled");
			$("#inputNroSeats").removeClass("error");
			$("#inputNroSeats").focus();
		} else {
			$("#inputNroSeats").val("");
		}
	});

	$("body").on("click", ".btnSaveEvent", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		console.log(".btnSaveEvent");
		let form = $(this).attr("form");
		btnLoading("btnSaveEvent");
		saveEvent(form)
			.then((response) => {
				console.log(response);
				if (response.Error == 0) {
					resetForm(form);
					btnLoadingRemove("btnSaveEvent");
					ticketingEventsIndex();
				} else {
					alertResponseMessageError({ message: response.message });
				}
			})
			.catch(console.warn());
	});

	$("body").on("change", "#inputInitDate", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		//console.log('#inputInitDate');
		$(this).removeClass("error");
	});

	$("body").on("change", "#inputEndDate", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		//console.log('#inputEndDate');
		$(this).removeClass("error");
	});

	$("body").on("change", "#inputCostSeat", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		//console.log('#inputCostSeat');
		let value = $(this).val();
		//console.log(financial(value));
		$(this).val(financial(value));
	});

	$("body").on("click", ".btnUpdateEvent", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		console.log(".btnUpdateEvent");
		const form = $(this).attr("form");
		updateEvent(form)
			.then((response) => {
				console.log(response);
				if (response.Error == 0) {
					ticketingEventsIndex();
				} else {
					alertResponseMessageError({ message: response.message });
				}
			})
			.catch(console.warn());
	});

	$("body").on("click",`.btnCloseContentTickets`, function(e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let event = JSON.parse(localStorage.getItem("eventsTicketNow"));
		console.log(`.btnCloseContentTickets`,data, event);

		getData({
			task: 'closeContentTickets',
			return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
			input: {
				eventId: event.id,
				ticketId : data.id,
				tableId : data.tableid,
			}
		}).then((response) => {
			console.log('getData closeContentTickets', response);
			if(response.status=='success'){
				let data = response.data;
				if (data.state == 5){
					$(`.contentTicketsEventUnit[data-id="${data.ticketId}"][data-eventid="${data.eventId}"][data-tableid="${data.tableId}"]`).remove();
					let count = $("#ticketReservationForm").children().length;

					if(count==0){
						$("#ticketReservationTitle").html(``);
					}
				}
			}else{
				 alertMessageError({message: response.message})
			}
		}).catch(console.warn());

	});

	$("body").on("click",`#btnTotaTicketsEvent`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let event = JSON.parse(localStorage.getItem("eventsTicketNow"));
		let ref = $("#inputReferenceReserveTicketEvent").val();
        //console.log(`#btnTotaTicketsEvent`,data,ref);

        getData({
            task: 'totaTicketEvent',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
            input: {
                eventId: event.id,
                cpeId : data.cpeid,
                reserveId : data.reserveid,
				ref
            }
        }).then((response) => {
            //console.log('getData totaTicketEvent', response);
			if (response.status == 'success') {
				$("#btnPayTicketsEvent").attr("data-cpeid", data.cpeid);
				$("#btnPayTicketsEvent").attr("data-reserveid", data.reserveid);

                deactivateBtn('.form');
                disabledBtn('.containerDraw');
				activateBtn(`.formTotal`);
				$("#btnPayTicketsEvent #totalPayTickets").html(`${response.data.coin}.${response.data.costTotal}`);
				unDisabledBtn('#btnPayTicketsEvent');
				addHtml({
					selector: '.boxTotal', 
					type: 'insert', 
					content: renderTicketsEventTotal(response.data) 
				}) //type: html, append, prepend, before, after
            }else{
                 alertMessageError({message: response.message})
            }
        }).catch(console.warn());
        
    });

	$("body").on("click",`#btnBackSelectTicket`, function(e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		console.log(`#btnBackSelectTicket`,data);
		deactivateBtn('.form');
		activateBtn(`.formSelectTicket`);
		activateBtn(`.containerDraw`);
	});

	$("body").on("click",`#btnPayTicketsEvent`, function(e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let event = JSON.parse(localStorage.getItem("eventsTicketNow"));
		let ref = $("#inputRefTicketEvent").val();
		let valueTypePay = $('input[name="tipPay"]:checked').val();

		let inputCardNumber = $("#inputCardNumber").val();
		let inputTransferNumber = $("#inputTransferNumber").val();

		//console.log(`#btnPayTicketsEvent`, data, valueTypePay);
		changeBtn({
		    selector: "#btnPayTicketsEvent",
			type: "loading",
			text: "Procesando..."
		})
		getData({
			task: 'payTicketsEvent',
			return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
			input: {
				eventId: event.id,
				cpeId: data.cpeid,
				reserveId: data.reserveid,
				ref,
				valueTypePay,
				inputCardNumber,
				inputTransferNumber
			}
		}).then((response) => {
			console.log('getData PayTicketsEvent', response);
			if(response.status=='success' && response.pay.status == 'success'){
				let pay = response.pay.response;
				let type = response.type;
				let qrPayUrl = pay.qr_simple_url;
				let txr = pay.id_transaccion;
				let eventId = event.id;

				if (type=="QR"){
					addHtml({
						selector: "body", 
						type: 'prepend', 
						content: renderPayQR({qrPayUrl,txr,eventId}) 
					}) //type: html, append, prepend, before, after
				}else{
					clearSidebarSales();
				}

				

			}else{
				 let msg = response.message;
				 if (response.pay.status == 'error'){ 
					msg = response.pay.message;
				 }

				 alertMessageError({message: msg})
			}
		}).catch(console.warn());
	});

	$("body").on("click",`#boxTipsPays .tip .tippay`, function(e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let id = $(this).attr('for');
		console.log(`#boxTipsPays .tip .tippay`,data, id);
		$(`#boxTipsPays .tip #${id}`).prop('checked', true);

		deactivateBtn(`#inputCourtesyTipPay`);

		if (id=="tipPay5"){
			activateBtn(`#inputCourtesyTipPay`);
		}

		$("#boxAccionTotal input[type='text']").val('');
	});

	
});
