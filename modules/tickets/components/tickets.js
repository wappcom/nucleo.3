import {
	loadView,
	btnLoading,
	btnLoadingRemove,
	disableId,
	unDisableId,
	accessToken,
	alertPage,
} from "../../components/functions.js";
import {
	createTable,
	dataTable,
	removeItemTable,
} from "../../components/tables.js";
import {
	resizeModalConfig,
	deleteModalItem,
	removeModalId,
} from "../../components/modals.js";
import { errorInput, validateNum, resetForm } from "../../components/forms.js";
import {
	renderModalConfig,
	renderModalConfigBody,
} from "../../components/renders/renderModals.js";
import { renderNoDataTable } from "../../components/renders/renderTables.js";
import {
	renderConfigTickets,
	renderConfigHalls,
	renderTr,
} from "./renders/renderTickets.js";
import { modifyHours } from "../../counseling/components/calendarAdvised.js";

const system = "tickets";
const module = "tickets";
const pathurl = "modules/tickets/";
let numMod = 0;
const btnSaveHallId = "saveFormNewHall";


export const ticketsIndex = () => {
	localStorage.setItem("halls", "0");
	loadHalls();
	//console.log(accessToken());
};

export const saveFormNewHall = async (form) => {
	//console.log(form);
	let count = 0;
	const name = $("#" + form + " #inputName").val();
	const seats = $("#" + form + " #inputSeats").val();
	const address = $("#" + form + " #inputAddress").val();
	const coord = $("#" + form + " #inputCoord").val();

	if (name == "" || name.length < 5) {
		errorInput("inputName", "Agrega un nombre de Sala valido.");
		count++;
	}

	if (seats == "" || !validateNum(seats)) {
		errorInput("inputSeats", "Agrega un número max de Butacas valido.");
		count++;
	}
	if (address == "" || address.length < 5) {
		errorInput("inputAddress", "Agrega una dirección valida.");
		count++;
	}
	if (coord == "" || coord.length < 5) {
		errorInput("inputCoord", "Agrega una dirección valida.");
		count++;
	}

	if (count == 0) {
		btnLoading(btnSaveHallId);

		const url =
			_PATH_WEB_NUCLEO +
			"modules/tickets/controllers/apis/v1/tickets.php";
		const dataForm = new FormData();
		dataForm.append("accessToken", JSON.stringify(accessToken()));
		dataForm.append("action", "saveNewHall");
		dataForm.append("vars", JSON.stringify($("#" + form).serializeArray()));

		try {
			let res = await axios({
				async: true,
				method: "post",
				responseType: "json",
				url: url,
				headers: {},
				data: dataForm,
			});
			//console.log(res.data)
			const str = res.data;

			if (str.Error == 0) {
				resetForm(form);
				btnLoadingRemove(btnSaveHallId);
				return str.hallId;
			} else {
				alertPage({
					text:
						"Error. por favor contactarse con soporte. " +
						response.message,
					icon: "icn icon-alert-warning",
					animation_in: "bounceInRight",
					animation_out: "bounceOutRight",
					tipe: "danger",
					time: "3500",
					position: "top-left",
				});
				return 0;
			}
		} catch (error) {
			console.log(error);
		}
	} else {
		return 0;
	}
};

export async function loadHalls() {
	//console.log("loadHalls")
	const url =
		_PATH_WEB_NUCLEO + "modules/tickets/controllers/apis/v1/tickets.php";
	const dataForm = new FormData();
	dataForm.append("accessToken", JSON.stringify(accessToken()));
	dataForm.append("action", "loadHalls");

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: dataForm,
		});
		//console.log(res.data);
		if (res.data != 0) {
			localStorage.setItem("halls", JSON.stringify(res.data.items));
		}
		return res.data;
	} catch (error) {
		console.log(error);
	}
}

export const hallsIndex = () => {
	//console.log("hallsIndex");

	loadHalls().then((response) => {
		console.log(response);
		if (response != null && response.Error == 0) {
			const tableId = "tableHalls";
			const data = response.items;
			let strTable = "";
			let tbody = "";

			console.log(data);

			for (let i = 0; i < data.length; i++) {
				const elem = data[i];
				elem["table"] = tableId;
				elem["module"] = module;
				elem["system"] = system;
				tbody += renderTr(elem);
			}
			strTable = createTable({
				id: tableId,
				thead: ":check,id:colId,Nombre,Estado:colState,Acciones:colActions",
				body: tbody,
			});

			$("#listConfigHalls").html(strTable);

			dataTable({
				elem: "#" + tableId,
				orderCol: 1,
			});
		} else if (response === 0) {
			$("#listConfigHalls").html(renderNoDataTable());
		} else {
			alertPage({
				text: "Error. por favor contactarse con soporte. ",
				icon: "icn icon-alert-warning",
				animation_in: "bounceInRight",
				animation_out: "bounceOutRight",
				tipe: "danger",
				time: "3500",
				position: "top-left",
			});
		}
	});
};

export const focusInput = (id) => {
	$("#" + id).focus();
	$("#configFormHalls .head label").html("Nueva Sala");
	$("#configFormHalls .actions .btnPrimary").attr("id", "saveFormNewHall");
	$("#configFormHalls .actions .btnPrimary span").html("Guardar");
	$("#configFormHalls .actions .btnPrimary").addClass("saveFormNewHall");
};

export const backConfigTickets = (id, fn, vars) => {
	$(".content").removeClass("active");
	$("#" + id).addClass("active");
	resetForm("formNewHall");
	if (fn != "" && fn != null) {
		eval(fn + "(" + vars + ")");
	}
};

export const deleteHall = async (vars) => {
	const url =
		_PATH_WEB_NUCLEO + "modules/tickets/controllers/apis/v1/tickets.php";
	const dataForm = new FormData();
	dataForm.append("accessToken", JSON.stringify(accessToken()));
	dataForm.append("action", "deleteHall");
	dataForm.append("vars", JSON.stringify(vars));

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: dataForm,
		});
		//console.log(res.data)
		return res.data;
	} catch (error) {
		console.log("deleteHall");
		console.log(error);
	}
};

export const editItemHall = async (item) => {
	const url =
		_PATH_WEB_NUCLEO + "modules/tickets/controllers/apis/v1/tickets.php";
	const dataForm = new FormData();
	dataForm.append("accessToken", JSON.stringify(accessToken()));
	dataForm.append("action", "editItemHall");
	dataForm.append("item", item);

	$("#configFormHalls .head label").html("Editar Sala");
	$("#configFormHalls .actions .btnPrimary").attr("id", "updateFormHall");
	$("#configFormHalls .actions .btnPrimary span").html("Actualizar");
	$("#configFormHalls .actions .btnPrimary").removeClass("saveFormNewHall");

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: dataForm,
		});
		console.log(res.data);
		let dataItem = res.data.data;
		console.log(dataItem.name);
		//Cargar datos
		$("#configFormHalls #inputName").val(dataItem.name);
		$("#configFormHalls #inputId").val(dataItem.id);
		$("#configFormHalls #inputDetails").val(dataItem.details);
		$("#configFormHalls #inputTables").val(dataItem.tables);
		$("#configFormHalls #inputSeats").val(dataItem.seats);
		$("#configFormHalls #inputAddress").val(dataItem.address);
		$("#configFormHalls #inputCoord").val(dataItem.coord);
		$("#configFormHalls #inputCountry").val(dataItem.country);
		$("#configFormHalls #inputCity").val(dataItem.city);
		$("#configFormHalls #inputUrlFnDraw").val(dataItem.drawId);

		return res.data;
	} catch (error) {
		console.log("editItemHall");
		console.log(error);
	}
};

export const updateFormHall = async (form) => {
	//console.log("aqio1");
	let count = 0;
	const name = $("#" + form + " #inputName").val();
	const seats = $("#" + form + " #inputSeats").val();
	const address = $("#" + form + " #inputAddress").val();
	const coord = $("#" + form + " #inputCoord").val();

	if (name == "" || name.length < 5) {
		errorInput("inputName", "Agrega un nombre de Sala valido.");
		count++;
	}

	if (seats == "" || !validateNum(seats)) {
		errorInput("inputSeats", "Agrega un número max de Butacas valido.");
		count++;
	}
	if (address == "" || address.length < 5) {
		errorInput("inputAddress", "Agrega una dirección valida.");
		count++;
	}
	if (coord == "" || coord.length < 5) {
		errorInput("inputCoord", "Agrega una dirección valida.");
		count++;
	}

	if (count == 0) {
		btnLoading(btnSaveHallId);

		const url =
			_PATH_WEB_NUCLEO +
			"modules/tickets/controllers/apis/v1/tickets.php";
		const dataForm = new FormData();
		dataForm.append("accessToken", JSON.stringify(accessToken()));
		dataForm.append("action", "updateFormHall");
		dataForm.append("vars", JSON.stringify($("#" + form).serializeArray()));

		try {
			let res = await axios({
				async: true,
				method: "post",
				responseType: "json",
				url: url,
				headers: {},
				data: dataForm,
			});
			console.log(res.data);
			const str = res.data;

			if (str.Error == 0) {
				resetForm(form);
				btnLoadingRemove(btnSaveHallId);
				backConfigTickets("configHalls", "hallsIndex");
			} else {
				alertPage({
					text:
						"Error. por favor contactarse con soporte. " +
						str.message,
					icon: "icn icon-alert-warning",
					animation_in: "bounceInRight",
					animation_out: "bounceOutRight",
					tipe: "danger",
					time: "3500",
					position: "top-left",
				});
				return 0;
			}
		} catch (error) {
			console.log(error);
		}
	} else {
		return 0;
	}
};

document.addEventListener("DOMContentLoaded", function () {
	$("body").on("click", ".btnConfigTickets", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		numMod++;

		const idModal = "modalConfigTickets";

		loadView(
			_PATH_WEB_NUCLEO +
			"modules/tickets/views/sidebarConfig.html?" +
			_VS +
			numMod
		).then((sidebarMenu) => {
			loadView(
				_PATH_WEB_NUCLEO +
				"modules/tickets/views/formHall.html?" +
				_VS +
				numMod
			).then((formHalls) => {
				//console.log(sidebarMenu);
				//console.log(formHalls);
				sidebarMenu = sidebarMenu.replace(/{{_MODULE}}/g, module);
				let strBody = "";
				strBody += renderConfigTickets();
				strBody += renderConfigHalls({ module });
				strBody += formHalls.replace(/{{_FORM_ID}}/g, "formNewHall");
				strBody = strBody.replace(
					/{{_BTN_ACTION}}/g,
					"saveFormNewHall"
				);
				strBody = strBody.replace(/{{_BTN_NAME_ACTION}}/g, "Guardar");

				let body = renderModalConfigBody({
					title: "Configuración",
					system,
					id: idModal,
					sidebar: sidebarMenu,
					body: strBody,
				});
				$(".ws[system='" + system + "']").append(
					renderModalConfig({ body, system, id: idModal })
				);
				resizeModalConfig(system);
			});
		});
	});

	$("body").on("click", "#saveFormNewHall", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let form = $(this).attr("form");

		saveFormNewHall(form).then(function (response) {
			if (response != 0) {
				backConfigTickets("configHalls", "hallsIndex");
			}
		});
		//disableId("saveFormNewHall");
	});

	$("body").on("keyup", "input", function () {
		unDisableId(btnSaveHallId);
	});

	$("body").on("click", ".btnBackConfigTickets", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		console.log("back");
		backConfigTickets("configHalls", "hallsIndex");
	});

	$("body").on("click", ".btnDelete[module='" + module + "']", function (e) {
		//console.log("btnDeleteAdvised")
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();

		let item = $(this).attr("item");
		let module = $(this).attr("module");
		let fn = $(this).attr("fn");
		let vari = $(this).attr("vars");
		let variArray = vari.split(",");

		deleteModalItem({ name: variArray[0], item: item, fn: fn });

		$("body").on(
			"click",
			'.btnDeleteConfirm[fn="deleteItemHall"]',
			function (e) {
				e.preventDefault();
				e.stopPropagation();
				e.stopImmediatePropagation();
				/* Act on the event */
				let item = $(this).attr("vars");
				let fn = $(this).attr("fn");
				//console.log(item)

				deleteHall({ item: item }).then((response) => {
					if (response.Error == 0) {
						console.log(response);
						removeModalId(item);
						removeItemTable(item);
						//advisedIndex()
					} else {
						alertPage({
							text:
								"Error. por favor contactarse con soporte. " +
								response.message,
							icon: "icn icon-alert-warning",
							animation_in: "bounceInRight",
							animation_out: "bounceOutRight",
							tipe: "danger",
							time: "3500",
							position: "top-left",
						});
					}
				});
			}
		);
	});

	$("body").on("click", "#updateFormHall", function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let form = $(this).attr("form");

		updateFormHall(form);
	});
});
