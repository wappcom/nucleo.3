import { empty, btnFn } from "../../../components/functions.js";
// import {renderBtnState} from "../../components/renders/render.js";
import { renderBtnState } from "../../../components/renders/renderTables.js";
import { unDbDate } from "../../../components/dates.js";
import {renderModalClean } from "../../../components/renders/renderModals.js";

export function renderTr(vars) {
	return (
		`
    <tr class="rowItem" item="${vars.id}">
        <td class="colCheck colId"><input name="inputCheck[]"  type="checkbox" value="${vars.id}"></td>
        <td class="colId">${vars.id}</td>
        <td class="colImg"><a>${vars.name}<span></a></td>
        <td class="colState">` +
		renderBtnState({
			item: vars.id,
			fn: "changeStateItem",
			state: vars.state,
			module: vars.module,
			system: vars.system,
		}) +
		`</td>
        <td class="colActions">
            <div class="btns">
                 ` +
		btnFn({
			btnType: "btnEdit",
			clss: "btnActionModal",
			attr: "content='configFormHalls' module='" + vars.module + "'",
			item: vars.id,
			module: vars.module,
			system: vars.system,
			fn: "editItemHall",
			vars: vars.id,
		}) +
		`
                        ` +
		btnFn({
			btnType: "btnDelete",
			item: vars.id,
			module: vars.module,
			system: vars.system,
			fn: "deleteItemHall",
			vars: vars.name + "",
		}) +
		`
            </div>
        </td>
     </tr>
   `
	);
}

export function renderTrEvents(vars) {
	return (
		`
    <tr class="rowItem" item="${vars.id}">
        <td class="colCheck colId"><input name="inputCheck[]"  type="checkbox" value="${vars.id
		}"></td>
        <td class="colId">${vars.id}</td>
        <td class="colImg"><a>${vars.name}<span></a></td>
        <td class=""><a>${unDbDate(vars.initDate)}<span></a></td>
        <td class=""><a>${unDbDate(vars.endDate)}<span></a></td>
        <td class="colState">` +
		renderBtnState({
			item: vars.id,
			state: vars.state,
			module: vars.module,
			system: vars.system,
		}) +
		`</td>
        <td class="colActions">
            <div class="btns">
                 ` +
		btnFn({
			btnType: "btnEdit",
			clss: "btnAddForm",
			attr: "",
			item: vars.id,
			module: vars.module,
			system: vars.system,
			fn: "formEditEvent",
			vars: vars.id,
		}) +
		`
                        ` +
		btnFn({
			btnType: "btnDelete",
			item: vars.id,
			module: vars.module,
			system: vars.system,
			fn: "deleteModuleItem",
			vars: vars.name + ",",
		}) +
		`
            </div>
        </td>
     </tr>
   `
	);
}

export const renderConfigTickets = (vars) => {
	return /*html*/ `
        <div class="content active" id="configTickes">
            <div class="head">
                <div class="title">
                    <i class="icon icon-list"></i><h2 class="">Ajustes</h2>
                </div>
                <div class="actions">

                </div>
            </div>
            <div class="tbody">
                 
            </div>
        </div>
    `;
};

export const renderConfigHalls = (vars) => {
	return /*html*/ `
        <div class="content" id="configHalls">
            <div class="head">
                <div class="title">
                    <i class="icon icon-pointer"></i><h2 class="">Salas</h2>
                </div>
                <div class="actions">
                    <a  class="btn btnPrimary btnIconLeft btnActionModal"
                        content="configFormHalls"
                        module="${vars.module}"
                        fn="focusInput"
                        vars = "'inputName'"
                        >
                        <i class="icon icon-plus"></i>
                        <span>Agregar Sala</span>
                    </a>
                </div>
            </div>
            <div class="tbody" id="listConfigHalls">
            </div>
        </div>
    `;
};


export const renderTicketsEventUnit = (vars) => {
	//console.log("renderTicketsEventUnit:", vars)
	let reserveId = vars.reserveId ? vars.reserveId : "";
	let id = vars.id ? vars.id : "";
	let level = vars.level ? vars.level : "";
	let tableId = vars.tableId ? vars.tableId : "";
	let eventId = vars.eventId ? vars.eventId : "";
	let num = vars.num ? vars.num : "";
	let dataHall = JSON.parse(localStorage.getItem("hallDataNow"));

	return	/*html*/`
		<div class="contentTicketsEventUnit" 
			data-id = "${id}"
			data-level = "${level}"
			data-reserveid="${reserveId}" 
			data-eventid="${eventId}" 
			data-tableid="${tableId}">
			<button  class="btn btnIcon btnCloseContentTickets" data-id="${id}" data-tableid="${tableId}">
			<input type="hidden" name="inputTicketEventId" value="${id}">
			<i class="icon icon-circle-close"></i></button>
			<div class="inner">
				<div class="head">
					<div class="title">
						<label>ID: </label>
						<span>Nro.${id}</span>
					</div>
				</div>
				<div class="tbody">
				 	<div class="target">M${tableId}-${num}</div>
 					<div class="cost">${vars.cost}.- ${vars.coin}</div>
 					<div class="level">NIVEL ${vars.level}</div>
				</div>
			</div>
		</div>
	`;
}

export const renderTicketsEventTitle = (vars) => {
	let reserveId = vars.reserveId ? vars.reserveId : "";
	let num = vars.num ? vars.num : "";

	return /*html*/ `
 
			<span class="reservaIdTitle">Id Reserva:${reserveId}</span>
			<div class="numTickets">
				<span>Nro.Entradas</span> 
				<div class="num">${num}</div>
			</div>
 
	`;
}

export const renderTicketsEventTotal = (vars) => {
	//console.log("renderTicketsEventTotal:", vars)
	let reserveId = vars.reserveId ? vars.reserveId : "";
	let num = vars.num ? vars.num : "";
	let name = vars.cpeData.name ?? "";
	let lastname = vars.cpeData.lastname ?? "";
	let email = vars.cpeData.email ?? "";
	let phone = vars.cpeData.phone ?? "";
	let ci = vars.cpeData.ci ?? "";
	let adder = vars.cpeData.adder != 0 ? vars.cpeData.adder : "";
	let tablesId = vars.tablesId ? vars.tablesId : "";
	let costTotal = vars.costTotal ? vars.costTotal : "";
	let coin = vars.coin ? vars.coin : "";
	let ref = vars.ref ? vars.ref : "";

	return /*html*/ `
		<div class="ticketEventTotal">
			<span class="reservaIdTitle">ID Reserva ${reserveId}</span>
			<div class="numTickets">
				<span>Nro.Entradas</span> 
				<div class="num">${num}</div>
			</div>
			<div class="data">
				<span><b>Mesa: </b>${tablesId}</b></span>
				<span><b>Asientos: </b>${num} </span>
			</div>
			<div class="total">
				<div><b>Nombre Cliente:</b> ${name} ${lastname} </div>
				<div><b>Referencia:</b> ${ref} </div>
				<input type="hidden" name="inputRefTicketEvent" value="${ref}">
				<div><b>Correo:</b> ${email} </div>
				<div><b>Celular:</b> ${phone} </div>
				<div><b>CI:</b> ${ci} ${adder} </div>
			</div>
			<div class="finance">
				<div><b>Total:</b> ${coin}. ${costTotal}</div>
			</div>
		</div>
 
	`;
}

export const renderPayQR = (vars =[]) => {
	console.log('renderPayQR',vars);
	return renderModalClean({
		id: 'payQR',
		attr: `data-txr="${vars.txr}" data-eventid="${vars.eventId}"`,
		cls: 'modalPayQR',
		title : 'Pago QR',
		body: /*html*/`
			<img src="${vars.qrPayUrl}"/>
		`
	})
}

