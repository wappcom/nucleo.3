<?php
header('Content-Type: text/html; charset=utf-8');
class TICKETS
{
    var $fmt;
    var $brokers;
    var $events;
    var $customers;
    var $payments;


    function __construct($fmt)
    {
        $this->fmt = $fmt;
        require_once(_PATH_NUCLEO . "modules/accounts/models/class/class.customers.php");
        $this->customers = new CUSTOMERS($this->fmt);

        require_once(_PATH_NUCLEO . "modules/sales/models/class/payments.class.php");
        $this->payments = new PAYMENTS($this->fmt);
    }

    public function dataIdHall(array $var = null)
    {
        $id = $var["item"];
        $sql = "SELECT * FROM mod_events_halls WHERE mod_eve_hall_id='" . $id . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_eve_hall_id"];
            $return["id"] = $id;
            $return["name"] = $row["mod_eve_hall_name"];
            $return["details"] = $row["mod_eve_hall_details"];
            $return["drawId"] = $row["mod_eve_hall_draw_id"];
            $return["tables"] = $row["mod_eve_hall_num_tables"];
            $return["seats"] = $row["mod_eve_hall_num_seats"];
            $return["address"] = $row["mod_eve_hall_address"];
            $return["city"] = $row["mod_eve_hall_city"];
            $return["country"] = $row["mod_eve_hall_country"];
            $return["coord"] = $row["mod_eve_hall_coord"];
            $return["userId"] = $row["mod_eve_hall_user_id"];
            $return["registerDate"] = $row["mod_eve_hall_register_date"];
            $return["state"] = $row["mod_eve_hall_state"];
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function loadHalls(array $var = null)
    {
        //return $var;
        $userId = $var['userId'];
        $rolId = $var['rolId'];
        $entitieId = $var['entitieId'];

        $sql = "SELECT * FROM mod_events_halls WHERE mod_eve_hall_ent_id='" . $entitieId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                //mod_cco_id	mod_cco_name	mod_cco_description	mod_cco_img	mod_cco_acu_id	mod_cco_register_date	mod_cco_user_id	mod_cco_state
                $return[$i]['id'] = $row['mod_eve_hall_id'];
                $return[$i]['name'] = $row['mod_eve_hall_name'];
                $return[$i]['details'] = $row['mod_eve_hall_details'];
                $return[$i]['drawUrl'] = $row['mod_eve_hall_url_draw'];
                $return[$i]['drawFnInit'] = $row['mod_eve_hall_fn_js_draw'];
                $return[$i]['numMaxTables'] = $row['mod_eve_hall_num_tables'];
                $return[$i]['numMaxSeats'] = $row['mod_eve_hall_num_seats'];
                $return[$i]['address'] = $row['mod_eve_hall_address'];
                $return[$i]['country'] = $row['mod_eve_hall_country'];
                $return[$i]['coord'] = $row['mod_eve_hall_coord'];
                $return[$i]['city'] = $row['mod_eve_hall_city'];
                $return[$i]['userId'] = $row['mod_eve_hall_user_id'];
                $return[$i]['registerDate'] = $row['mod_eve_hall_register_date'];
                $return[$i]['state'] = $row['mod_eve_hall_state'];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function saveNewHall(array $var = null)
    {
        $userId = $var['userId'];
        $rolId = $var['rolId'];
        $entitieId = $var['entitieId'];
        $vars = $var['vars'];

        $name = $vars['inputName'];
        $details = $vars['inputDetails'];
        $urlFnDraw = $vars['inputUrlFnDraw'];
        $tables = $vars['inputTables'];
        $seats = $vars['inputSeats'];
        $address = $vars['inputAddress'];
        $city = $vars['inputCity'];
        $coord = $vars['inputCoord'];
        $country = $vars['inputCountry'];

        $dateNow = $this->fmt->modules->dateFormat();

        $insert =
            'mod_eve_hall_name,mod_eve_hall_details,mod_eve_hall_url_draw_id,mod_eve_hall_num_tables,	mod_eve_hall_num_seats,mod_eve_hall_address,mod_eve_hall_city,mod_eve_hall_country,mod_eve_hall_coord,mod_eve_hall_user_id,mod_eve_hall_register_date,mod_eve_hall_ent_id,mod_eve_hall_state';

        $values =
            "'" .
            $name .
            "','" .
            $details .
            "','" .
            $urlFnDraw .
            "','" .
            $tables .
            "','" .
            $seats .
            "','" .
            $address .
            "','" .
            $city .
            "','" .
            $country .
            "','" .
            $coord .
            "','" .
            $userId .
            "','" .
            $dateNow .
            "','" .
            $entitieId .
            "','1'";

        $sql =
            'insert into mod_events_halls (' .
            $insert .
            ') values (' .
            $values .
            ')';
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = 'select max(mod_eve_hall_id) as id from mod_events_halls';
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        return $row['id'];
    }

    public function updateFormHall(array $var = null)
    {
        $userId = $var['userId'];
        $rolId = $var['rolId'];
        $entitieId = $var['entitieId'];
        $vars = $var['vars'];

        $id = $vars['inputId'];
        $name = $vars['inputName'];
        $details = $vars['inputDetails'];
        $urlFnDraw = $vars['inputUrlFnDraw'];
        $tables = $vars['inputTables'];
        $seats = $vars['inputSeats'];
        $address = $vars['inputAddress'];
        $city = $vars['inputCity'];
        $coord = $vars['inputCoord'];
        $country = $vars['inputCountry'];


        $dateNow = $this->fmt->modules->dateFormat();

        $sql = "UPDATE mod_events_halls SET
                mod_eve_hall_name='" . $name . "',
                mod_eve_hall_details='" . $details . "',
                mod_eve_hall_url_draw_id='" . $urlFnDraw . "',
                mod_eve_hall_num_tables='" . $tables . "',
                mod_eve_hall_num_seats='" . $seats . "',
                mod_eve_hall_address='" . $address . "',
                mod_eve_hall_city='" . $city . "',
                mod_eve_hall_country='" . $country . "',
                mod_eve_hall_coord='" . $coord . "',
                mod_eve_hall_user_id='" . $userId . "',
                mod_eve_hall_register_date='" . $dateNow . "',
                mod_eve_hall_ent_id='" . $entitieId . "'
                WHERE mod_eve_hall_id= '" . $id . "' ";
        $this->fmt->querys->consult($sql);


        return  1;
    }

    public function deleteHall(Int $id = null)
    {
        $this->fmt->modules->deleteItem(array("from" => "mod_events_halls", "column" => "mod_eve_hall_id", "item" => $id));
        return 1;
    }

    //Events
    public function loadEvents(array $var = null)
    {
        //return $var;
        $userId = $var['userId'];
        $rolId = $var['rolId'];
        $entitieId = $var['entitieId'];

        $sql = "SELECT * FROM mod_events WHERE mod_eve_ent_id='" . $entitieId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);

                $return[$i]['id'] = $row['mod_eve_id'];
                $return[$i]['name'] = $row['mod_eve_name'];
                $return[$i]['description'] = $row['mod_eve_description'];
                $return[$i]['registerDate'] = $row['mod_eve_register_date'];
                $return[$i]['userId'] = $row['mod_eve_user_id'];
                $return[$i]['initDate'] = $row['mod_eve_init_date'];
                $return[$i]['endDate'] = $row['mod_eve_end_date'];
                $return[$i]['seats'] = $row['mod_eve_seats'];
                $return[$i]['hall'] = $row['mod_eve_hall'];
                $return[$i]['costSeat'] = $row['mod_eve_cost_seat'];
                $return[$i]['coin'] = $row['mod_eve_coin'];
                $return[$i]['entitieId'] = $row['mod_eve_ent_id'];
                $return[$i]['state'] = $row['mod_eve_state'];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function loadEventsActives(array $var = null)
    {
        //return $var;
        $userId = $var['userId'];
        $rolId = $var['rolId'];
        $entitieId = $var['entitieId'];
        $staffMasterRolId = $this->getItem('staffMasterRolId');
        $arrayStaffRol = $this->fmt->users->usersRolEntitie($staffMasterRolId, $entitieId);
        $now = $this->fmt->data->dateFormat();
        
        

        $index = 0;
        foreach ($arrayStaffRol as $key => $value) {
            $staff[$index]["id"] = $value['id'];
            $staff[$index]["name"] = $value['name']." ".$value['lastname'];
            $index++;
        }

        $sql = "SELECT * FROM mod_events WHERE mod_eve_ent_id='" . $entitieId . "' AND mod_eve_end_date > '" . $now . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);

        /* $rt["now"]=$now;
        $rt["sql"]=$sql;
        $rt["num"]=$num;
        return $rt;
         */
        
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return[$i]['id'] = $row['mod_eve_id'];
                $return[$i]['name'] = $row['mod_eve_name'];
                $return[$i]['description'] = $row['mod_eve_description'];
                $return[$i]['registerDate'] = $row['mod_eve_register_date'];
                $return[$i]['userId'] = $row['mod_eve_user_id'];
                $return[$i]['initDate'] = $row['mod_eve_init_date'];
                $return[$i]['endDate'] = $row['mod_eve_end_date'];
                $return[$i]['seats'] = $row['mod_eve_seats'];
                $return[$i]['hall'] = $row['mod_eve_hall'];
                $return[$i]['costSeat'] = $row['mod_eve_cost_seat'];
                $return[$i]['coin'] = $row['mod_eve_coin'];
                $return[$i]['entitieId'] = $row['mod_eve_ent_id'];
                $return[$i]['state'] = $row['mod_eve_state'];
                //$return[$i]['staffMaster'] = $staffMasterRolId;
                $return[$i]['staffMaster'] = $staff;
            }

            return $return;
        } else {
            return 0;
        }
    }

    public function loadEvent(array $var = null)
    {
        //return $var;
        $userId = $var['userId'];
        $rolId = $var['rolId'];
        $entitieId = $var['entitieId'];
        $vars = $var['vars'];
        $item = $vars['item'];

        $sql = "SELECT * FROM mod_events WHERE mod_eve_id='" . $item . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {

            $row = $this->fmt->querys->row($rs);
            $return['id'] = $row['mod_eve_id'];
            $return['name'] = $row['mod_eve_name'];
            $return['description'] = $row['mod_eve_description'];
            $return['registerDate'] = $row['mod_eve_register_date'];
            $return['userId'] = $row['mod_eve_user_id'];
            $return['initDate'] = $row['mod_eve_init_date'];
            $return['endDate'] = $row['mod_eve_end_date'];
            $return['seats'] = $row['mod_eve_seats'];
            $return['hall'] = $row['mod_eve_hall'];
            $return['costSeat'] = $row['mod_eve_cost_seat'];
            $return['coin'] = $row['mod_eve_coin'];
            $return['entitieId'] = $row['mod_eve_ent_id'];
            $return['state'] = $row['mod_eve_state'];

            return $return;
        } else {
            return 0;
        }
    }

    public function getEventId(int $id = null)
    {
        //return $var;

        $sql = "SELECT * FROM mod_events WHERE mod_eve_id='" . $id . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {

            $row = $this->fmt->querys->row($rs);
            $return['id'] = $row['mod_eve_id'];
            $return['name'] = $row['mod_eve_name'];
            $return['description'] = $row['mod_eve_description'];
            $return['registerDate'] = $row['mod_eve_register_date'];
            $return['userId'] = $row['mod_eve_user_id'];
            $return['initDate'] = $row['mod_eve_init_date'];
            $return['endDate'] = $row['mod_eve_end_date'];
            $return['seats'] = $row['mod_eve_seats'];
            $return['hall'] = $row['mod_eve_hall'];
            $return['costSeat'] = $row['mod_eve_cost_seat'];
            $return['coin'] = $row['mod_eve_coin'];
            $return['entitieId'] = $row['mod_eve_ent_id'];
            $return['state'] = $row['mod_eve_state'];
            $img = [];
            
            if ($row["mod_eve_img_small"]) {
                $imgBd = $row["mod_eve_img_small"];
                $img["imgSmall"]["id"] = $imgBd;
                $img["imgSmall"]["pathurl"] = $this->fmt->files->imgId($imgBd, "");
                $img["imgSmall"]["name"] = $this->fmt->files->name($imgBd, "");
            } 
            if ($row["mod_eve_img_medium"]) {
                $imgBd = $row["mod_eve_img_medium"];
                $img["imgMedium"]["id"] = $imgBd;
                $img["imgMedium"]["pathurl"] = $this->fmt->files->imgId($imgBd, "");
                $img["imgMedium"]["name"] = $this->fmt->files->name($imgBd, "");
            }
            if ($row["mod_eve_img_large"]) {
                $imgBd = $row["mod_eve_img_large"];
                $img["imgLarge"]["id"] = $imgBd;
                $img["imgLarge"]["pathurl"] = $this->fmt->files->imgId($imgBd, "");
                $img["imgLarge"]["name"] = $this->fmt->files->name($imgBd, "");
            }

            $return['img'] = $img;

            return $return;
        } else {
            return 0;
        }
    }

    public function saveEvent(array $var = null)
    {
        $userId = $var['userId'];
        $rolId = $var['rolId'];
        $entitieId = $var['entitieId'];
        $vars = $var['vars'];

        $row["mod_eve_name"] = $vars['inputName'];
        $row["mod_eve_description"] = $vars['inputDescription'];
        $row["mod_eve_register_date"] = $dateNow = $this->fmt->modules->dateFormat();
        $row["mod_eve_user_id"] = $userId;
        $row["mod_eve_init_date"] = $this->fmt->data->dbDate($vars['inputInitDate']);
        $row["mod_eve_end_date"] = $this->fmt->data->dbDate($vars['inputEndDate']);
        $row["mod_eve_seats"] = $vars['inputNroSeats'];
        $row["mod_eve_hall"] = $vars['inputHalls'];
        $row["mod_eve_cost_seat"] = $vars['inputCostSeat'];
        $row["mod_eve_coin"] = $vars['inputCoin'];
        $row["mod_eve_ent_id"] = $entitieId;
        $row["mod_eve_state"] = $vars['inputStatus'];

        $values = implode("','", $row);
        $insert = implode(",", array_keys($row));
        $sql = "insert into mod_events (" . $insert . ") values ('" . $values . "')";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = 'select max(mod_eve_id) as id from mod_events';
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        return $row['id'];
    }

    public function updateEvent(array $var = null)
    {
        $userId = $var['userId'];
        $rolId = $var['rolId'];
        $entitieId = $var['entitieId'];
        $vars = $var['vars'];

        $dateNow = $this->fmt->modules->dateFormat();

        $sql = "UPDATE mod_events SET 
                mod_eve_name = '" . $vars['inputName'] . "',
                mod_eve_description = '" . $vars['inputDescription'] . "',
                mod_eve_register_date = '" . $dateNow . "',
                mod_eve_user_id = '" . $userId . "',
                mod_eve_init_date = '" . $this->fmt->data->dbDate($vars['inputInitDate']) . "',
                mod_eve_end_date = '" . $this->fmt->data->dbDate($vars['inputEndDate']) . "',
                mod_eve_seats = '" . $vars['inputNroSeats'] . "',
                mod_eve_hall = '" . $vars['inputHalls'] . "',
                mod_eve_cost_seat = '" . $vars['inputCostSeat'] . "',
                mod_eve_coin = '" . $vars['inputCoin'] . "',
                mod_eve_ent_id = '" . $entitieId . "',
                mod_eve_state = '" . $vars['inputStatus'] . "' 
                WHERE mod_eve_id = '" . $vars['inputId'] . "'";
        $this->fmt->querys->consult($sql);
        return 1;
    }

    public function getItem($name = "", $autoload = "yes")
	{
		$sql = "SELECT mod_eve_option_value FROM mod_events_options WHERE mod_eve_option_name='" . $name . "' AND mod_eve_option_autoload='" . $autoload . "'";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$row = $this->fmt->querys->row($rs);
        $value = $row["mod_eve_option_value"];

        if(!empty($value)){
            return $value;
        }else{
            return 0;
        }
	}

    //Tickets
    public function getTicketsEvents(array $var = null)
    {
        //return $var;
        $userId = $var['userId'];
        $rolId = $var['rolId'];
        $entitieId = $var['entitieId'];
        $eventId = $var['vars']['input']['eventId'];
        $hallId =  $var['vars']['input']['hallId'];
        $statusState = $var['vars']['input']['statusState'] ?? ' AND mod_eve_tck_state != 5';

        $sql = "SELECT * FROM mod_events_tickets WHERE mod_eve_tck_eve_id='" . $eventId . "' AND mod_eve_tck_hall_id='" . $hallId . "' " . $statusState . " ORDER BY mod_eve_tck_id DESC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_eve_tck_id"];
                $return[$i]["id"] = $id;
                $return[$i]["eveId"] = $row["mod_eve_tck_eve_id"];
                $return[$i]["hallId"] = $row["mod_eve_tck_hall_id"];
                $return[$i]["acpId"] = $row["mod_eve_tck_acp_id"];
                $return[$i]["cpeId"] = $row["mod_eve_tck_cpe_id"];
                $return[$i]["cpeData"] = $this->customers->getCustomerPersonId($row["mod_eve_tck_cpe_id"]);
                $return[$i]["code"] = $row["mod_eve_tck_code"];
                $return[$i]["num"] = $i + 1;
                $return[$i]["tableId"] = $row["mod_eve_tck_table_id"];
                $return[$i]["dateRegister"] = $row["mod_eve_tck_date_register"];
                $return[$i]["userId"] = $row["mod_eve_tck_user_id"];
                $state = $row["mod_eve_tck_state"];
                $return[$i]["state"] = $state;
                switch ($state) {
                    case '0':
                        $valueTextState = "Libre";
                        break;
                    case '1':
                        $valueTextState = "Temporal";
                        break;

                    case '2':
                        $valueTextState = "Reservado";
                        break;

                    case '3':
                        $valueTextState = "Vendido";
                        break;

                    case '4':
                        $valueTextState = "Anulado";
                        break;

                    case '5':
                        $valueTextState = "Cancelado";
                        break;

                    default:
                        $valueTextState = "Sin estado";
                        break;
                }
                $return[$i]["stateText"] = $valueTextState;
                $return[$i]["reserveId"] = $row["mod_eve_tck_reserve_id"];
                $return[$i]['reference'] = $row['mod_eve_tck_ref'];
                $return[$i]["cost"] = $row["mod_eve_tck_cost"];
                $return[$i]["coin"] = $row["mod_eve_tck_coin"];
            }

            $rtn["Error"] = 0;
            $rtn["status"] = "success"; 
            $rtn["data"]= $return;
            $rtn["eventId"] = $eventId;
            $arrayPayEventQR = $this->payments->getPayEventQR(['eventId' =>$eventId, 'hallId' => $hallId, 'statusState' => 3 ]);
            $rtn["payQR"] = $arrayPayEventQR;
            if ( $arrayPayEventQR == 0){
                $rtn["numPayQR"] = 0;
            }else{
                $rtn["numPayQR"] = count($arrayPayEventQR);
            }
            $rtn["message"] = "Tickets encontrados";

            return $rtn;

        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function createTicketEvent(array $var = null)
    {
        //return $var;

        $userId = $var['user']['userId'];
        $rolId = $var['user']['rolId'];
        $entitieId = $var['entitieId'];
        $input = $var['vars']['input'];
        $eventId = $var['vars']['input']['eventId'];
        $hallId =  $var['vars']['input']['hallId'];
        $tableId = $var['vars']['input']['tableId'];
        $dataTable = $var['vars']['input']['dataTable'];
        $level = $var['vars']['input']['level'];
        $dataTableId = $dataTable['mesas'];
        $accountingPlanId = $input['accountingPlanId'] ? $input['accountingPlanId'] : 0;
        $cpeId = $input['cpeId'] ? $input['cpeId'] : 0;
        $ref = $input['ref'] ? $input['ref'] : 0;
        $dataEvent = $this->getEventId($eventId);
        $cost = $dataEvent['costSeat'];
        $coin = $dataEvent['coin'] ? $dataEvent['coin'] : 'Bs';
        $reserveIdCode =  $this->fmt->data->createCode(['num' => 2, 'mode' => 'integer']) . '00';
        $reserveIdCode = $this->compareReservation($reserveIdCode);
        $reserveId =  $input["reserveId"] != 0 ? $input["reserveId"] : $reserveIdCode;
        $code =  $input["code"] ? $input["code"] : "";
        $now = $this->fmt->data->dateFormat();
        $return = [];
        $index = 0;

        foreach ($dataTableId as $key => $value) {
            if ($value['id'] == $tableId) {
                $max = $value['max'] ? $value['max'] : 1;
                $min = $value['min'] ? $value['min'] : 1;
                $level = $value['level'] ? $value['level'] : 0;
                //return $value;
            }
        }
        //return $dataBase = $dataTableId[$key]['dataBase'];

        $sql = "SELECT * FROM mod_events_tickets WHERE mod_eve_tck_eve_id='" . $eventId . "' AND mod_eve_tck_hall_id='" . $hallId . "' AND mod_eve_tck_table_id = '" . $tableId . "' AND mod_eve_tck_state IN (1,2,3) ORDER BY mod_eve_tck_id DESC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);



        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_eve_tck_id"];
                $index = $i + 1;
                $return[$i]["id"] = $id;
                $return[$i]["eventId"] = $eventId;
                $return[$i]["hallId"] = $hallId;
                $return[$i]["tableId"] = $tableId;
                $return[$i]["num"] = $index;
                $return[$i]["code"] = $row["mod_eve_tck_code"];
                $return[$i]["dateRegister"] = $row["mod_eve_tck_date_register"];
                $return[$i]["userId"] = $row["mod_eve_tck_user_id"];
                $return[$i]["state"] = $row["mod_eve_tck_state"];
                $return[$i]["reserveId"] = $row["mod_eve_tck_reserve_id"];
                $return[$i]["cost"] = $row["mod_eve_tck_cost"];
                $return[$i]["coin"] = $row["mod_eve_tck_coin"];
                $return[$i]["acpId"] = $row["mod_eve_tck_acp_id"];
                $return[$i]["cpeId"] = $row["mod_eve_tck_cpe_id"];
                $return[$i]["ref"] = $row["mod_eve_tck_ref"];
                $return[$i]["level"] = $level;
            }

            if ($index < $max) {

                /* $reserveId =  $this->fmt->data->createCode(['num' => 2, 'mode' => 'integer']) . '00';
                $reserveId = $this->compareReservation($reserveId);
                $code = ''; */

                $insert = "mod_eve_tck_eve_id,mod_eve_tck_hall_id,mod_eve_tck_table_id,mod_eve_tck_acp_id,mod_eve_tck_cpe_id,mod_eve_tck_code,mod_eve_tck_date_register,mod_eve_tck_user_id,mod_eve_tck_state,mod_eve_tck_reserve_id,mod_eve_tck_cost,mod_eve_tck_coin,mod_eve_tck_ref";
                $values = "'" . $eventId . "','" . $hallId . "','" . $tableId . "','" . $accountingPlanId . "','" . $cpeId . "','" . $code . "','" . $now . "','" . $userId . "','1','" . $reserveId . "','" . $cost . "','" . $coin . "','" . $ref . "'";
                $sqlInsert = "insert into mod_events_tickets (" . $insert . ") values (" . $values . ")";
                $this->fmt->querys->consult($sqlInsert, __METHOD__);

                $sqlSelect = "select max(mod_eve_tck_id) as id from mod_events_tickets";
                $rsSelect = $this->fmt->querys->consult($sqlSelect, __METHOD__);
                $rowSelect = $this->fmt->querys->row($rsSelect);

                $id = $rowSelect['id'];

                $code = hash('sha256', $now . "-" . $eventId . "-" . $id);

                $sqlUpdate = "UPDATE mod_events_tickets SET mod_eve_tck_code = '" . $code . "' WHERE mod_eve_tck_id = '" . $id . "'";
                $this->fmt->querys->consult($sqlUpdate, __METHOD__);


                $return[$index]['id'] = $id;
                $return[$index]["eventId"] = $eventId;
                $return[$index]["hallId"] = $hallId;
                $return[$index]["tableId"] = $tableId;
                $return[$index]['num'] = $index + 1;
                $return[$index]['code'] = $code;
                $return[$index]['dateRegister'] = $now;
                $return[$index]['userId'] = $userId;
                $return[$index]['state'] = $index;
                $return[$index]['reserveId'] = $reserveId;
                $return[$index]['cost'] = $cost;
                $return[$index]['coin'] = $coin;
                $return[$index]['acpId'] = $accountingPlanId;
                $return[$index]['cpeId'] = $cpeId;
                $return[$index]['ref'] = $ref;
                $return[$index]["level"] = $level;
                $index = $index + 1;
            }

            $rtn['Error'] = '0';
            $rtn['status'] = 'success';
            $rtn['reserveId'] = $reserveId;
            $rtn['num'] = $index;
            $rtn['max'] = $max;
            $rtn['data'] = $return;
            $rtn['dataEvent'] = $dataEvent;
            $rtn["message"] = "filas encontradas";
            $rtn["level"] = $level;


            return $rtn;
        } else {


            /* $reserveId =  $this->fmt->data->createCode(['num' => 2, 'mode' => 'integer']) . '00';
            $reserveId = $this->compareReservation($reserveId);
            $code = ''; */


            $insert = "mod_eve_tck_eve_id,mod_eve_tck_hall_id,mod_eve_tck_table_id,mod_eve_tck_acp_id,mod_eve_tck_cpe_id,mod_eve_tck_code,mod_eve_tck_date_register,mod_eve_tck_user_id,mod_eve_tck_state,mod_eve_tck_reserve_id,mod_eve_tck_cost,mod_eve_tck_coin,mod_eve_tck_ref";
            $values = "'" . $eventId . "','" . $hallId . "','" . $tableId . "','" . $accountingPlanId . "','" . $cpeId . "','" . $code . "','" . $now . "','" . $userId . "','1','" . $reserveId . "','" . $cost . "','" . $coin . "','" . $ref . "'";
            $sql = "insert into mod_events_tickets (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);

            $sql = "select max(mod_eve_tck_id) as id from mod_events_tickets";
            $rs = $this->fmt->querys->consult($sql, __METHOD__);
            $row = $this->fmt->querys->row($rs);

            $id = $row['id'];
            $code = hash('sha256', $now . "-" . $eventId . "-" . $id);

            $sqlUpdate = "UPDATE mod_events_tickets SET mod_eve_tck_code = '" . $code . "' WHERE mod_eve_tck_id = '" . $id . "'";
            $this->fmt->querys->consult($sqlUpdate, __METHOD__);

            $rtn['Error'] = '0';
            $rtn['status'] = 'success';
            $rtn['num'] = 1;
            $rtn['reserveId'] = $reserveId;

            $ticket[0]['id'] = $id;
            $ticket[0]['eventId'] = $eventId;
            $ticket[0]['hallId'] = $hallId;
            $ticket[0]['tableId'] = $tableId;
            $ticket[0]['num'] = 1;
            $ticket[0]['code'] = $code;
            $ticket[0]['max'] = $max;
            $ticket[0]['cost'] = $cost;
            $ticket[0]['coin'] = $coin;
            $ticket[0]['reserveId'] = $reserveId;
            $ticket[0]['level'] = $level;
            $ticket[0]['ref'] = $ref;

            $rtn['data'] = $ticket;
            $rtn['dataEvent'] = $dataEvent;

            return $rtn;
        }
        $this->fmt->querys->leave($rs);
    }

    public function compareReservation($code = null)
    {

        //limpiar $code;

        $sql = "SELECT * FROM mod_events_tickets WHERE mod_eve_tck_reserve_id='" . $code . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $code = $this->fmt->data->createCode(['num' => 2, 'mode' => 'integer']) . '000';
            $code = $this->compareReservation($code);
            return $code;
        } else {
            return $code;
        }
    }

    public function closeContentTickets(array $var = null)
    {
        //return $var;
        $userId = $var['userId'];
        $rolId = $var['rolId'];
        $entitieId = $var['entitieId'];
        $eventId = $var['vars']['input']['eventId'];
        $ticketId = $var['vars']['input']['ticketId'];
        $tableId = $var['vars']['input']['tableId'];


        $sql = "UPDATE mod_events_tickets SET
                mod_eve_tck_state ='5'
                WHERE mod_eve_tck_table_id = '" . $tableId . "' AND mod_eve_tck_eve_id = '" . $eventId . "' AND mod_eve_tck_id = '" . $ticketId . "' ";
        $this->fmt->querys->consult($sql);

        $rtn['Error'] = '0';
        $rtn['status'] = 'success';
        $rtn['data']['ticketId'] = $ticketId;
        $rtn['data']['tableId'] = $tableId;
        $rtn['data']['eventId'] = $eventId;
        $rtn['data']['state'] = 5;
        return $rtn;
    }

    public function searchCustomers(array $var = null)
    {
        //return $var;
        $userId = $var["user"]["userId"];
        $rolId = $var['user']['rolId'];
        $entitieId = $var['entitieId'];
        $input = $var['vars']['input'];

        $rtn["output"] = $this->customers->searchCustomersPersons($var);
        return $rtn;
    }

    public function createCustomer(array $var = null)
    {
        //return $var;
        return $this->customers->addCustomerPerson($var);
    }

    public function totaTicketEvent(array $var = null)
    {
        //return $var;

        $userId = $var["user"]["userId"];
        $rolId = $var['user']['rolId'];
        $entitieId = $var['entitieId'];
        $input = $var['vars']['input'];
        $cpeId = $input['cpeId'];
        $eventId = $input['eventId'];
        $reserveId = $input['reserveId'];
        $ref = $input['ref'];
        $costTotal = 0;
        $coin = "";
        $tablesId = [];

        $sql = "UPDATE mod_events_tickets SET
                mod_eve_tck_cpe_id='" . $cpeId . "',
                mod_eve_tck_ref='" . $ref . "'
                WHERE mod_eve_tck_eve_id ='" . $eventId . "'  AND mod_eve_tck_reserve_id= '" . $reserveId . "' ";
        $this->fmt->querys->consult($sql);

        $sql = "SELECT * FROM mod_events_tickets WHERE mod_eve_tck_eve_id ='" . $eventId . "'  AND mod_eve_tck_reserve_id= '" . $reserveId . "' ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_eve_tck_id"];
                $index = $i + 1;
                $return[$i]["id"] = $id;
                $return[$i]["eventId"] = $eventId;
                $return[$i]["hallId"] = $row["mod_eve_tck_hall_id"];
                $tablesId[$i] = $row["mod_eve_tck_table_id"];
                $return[$i]["tableId"] = $row["mod_eve_tck_table_id"];
                $return[$i]["num"] = $index;
                $return[$i]["code"] = $row["mod_eve_tck_code"];
                $return[$i]["dateRegister"] = $row["mod_eve_tck_date_register"];
                $return[$i]["userId"] = $row["mod_eve_tck_user_id"];
                $return[$i]["state"] = $row["mod_eve_tck_state"];
                $return[$i]["reserveId"] = $row["mod_eve_tck_reserve_id"];
                $cost = floatval($row["mod_eve_tck_cost"]);
                $coin = $row["mod_eve_tck_coin"];
                //convertir $cost a float con 2 decimales

                $costTotal = $costTotal + $cost;
                $costTotal = number_format($costTotal, 2, '.', '');
                $return[$i]["cost"] = $row["mod_eve_tck_cost"];
                $return[$i]["coin"] = $row["mod_eve_tck_coin"];
                $return[$i]["acpId"] = $row["mod_eve_tck_acp_id"];
                $return[$i]["cpeId"] = $row["mod_eve_tck_cpe_id"];
                $return[$i]["ref"] = $row["mod_eve_tck_ref"];
                $return[$i]["level"] = $row["mod_eve_tck_level"];
            }
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);

        $rtn["cpeData"] = $this->customers->getCustomerPersonId($cpeId);
        $rtn["num"] = $num;
        $rtn["tickets"] = $return;
        $rtn["eventId"] = $eventId;
        $rtn["reserveId"] = $reserveId;
        $rtn["costTotal"] = $costTotal;
        $rtn["tablesId"] = implode(",", array_unique($tablesId));
        $rtn["ref"] = $ref;
        $rtn["coin"] = $coin;

        return $rtn;
    }

    public function payTicketsEvent(array $var = null)
    {
        //return $var;
        $userId = $var["user"]["userId"];
        $rolId = $var['user']['rolId'];
        $entitieId = $var['entitieId'];
        $input = $var['vars']['input'];
        $cpeId = $input['cpeId'] ? $input['cpeId'] : "";
        $eventId = $input['eventId'] ? $input['eventId'] : "";
        $reserveId = $input['reserveId'] ? $input['reserveId'] : "";
        $typePay = $input['valueTypePay'] ? $input['valueTypePay'] : "";
        $inputCardNumber = $input['inputCardNumber'] ? $input['inputCardNumber'] : "";
        $inputTransferNumber = $input['inputTransferNumber'] ? $input['inputTransferNumber'] : "";
        $valueTypePay = "";

        if ($typePay == "card") {
            $valueTypePay = $inputCardNumber;

            //inputCardNumber menor a 4 digitos
            $numDigCard = strlen($inputCardNumber);
            if ($inputCardNumber == "" || $numDigCard < 4) {
                $return["Error"] = 1;
                $return['status'] = 'error';
                $return["data"] = "into-valid-card-number";
                $return["message"] = "Ultimos 4 digitos de la tarjeta incorrectos";
                return $return;
            }

        } 
        
        if ($typePay == "transfer") {
            $valueTypePay = $inputTransferNumber;
            // $inputTransferNumber sea un numero
            $typeTransferNumber = is_numeric($inputTransferNumber);

            if ($inputTransferNumber == "" ||  $typeTransferNumber == false ) {
                $return["Error"] = 1;
                $return['status'] = 'error';
                $return["data"] = "into-valid-tranfer-number";
                $return["message"] = "Numero de transferencia incorrecto";
                return $return;
            }

        } 

        $ref = $input['ref'] ? $input['ref'] : "";
        $costTotal = 0;
        $coin = "";
        $acpId = 0;
        $tablesId = [];
        $cpe = $this->customers->getCustomerPersonId($cpeId);

        $now = $this->fmt->data->dateFormat();

        $sql = "SELECT * FROM mod_events_tickets WHERE mod_eve_tck_eve_id ='" . $eventId . "'  AND mod_eve_tck_reserve_id= '" . $reserveId . "' ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_eve_tck_id"];
                $index = $i + 1;
                $rtn[$i]["id"] = $id;
                $rtn[$i]["eventId"] = $eventId;
                $rtn[$i]["hallId"] = $row["mod_eve_tck_hall_id"];
                $tablesId[$i] = $row["mod_eve_tck_table_id"];
                $rtn[$i]["tableId"] = $row["mod_eve_tck_table_id"];
                $rtn[$i]["num"] = $index;
                $rtn[$i]["code"] = $row["mod_eve_tck_code"];
                $rtn[$i]["dateRegister"] = $row["mod_eve_tck_date_register"];
                $rtn[$i]["userId"] = $row["mod_eve_tck_user_id"];
                $rtn[$i]["state"] = $row["mod_eve_tck_state"];
                $rtn[$i]["reserveId"] = $row["mod_eve_tck_reserve_id"];
                $cost = floatval($row["mod_eve_tck_cost"]);
                $coin = $row["mod_eve_tck_coin"];
                //convertir $cost a float con 2 decimales

                $costTotal = $costTotal + $cost;
                $costTotal = number_format($costTotal, 2, '.', '');
                $rtn[$i]["cost"] = $row["mod_eve_tck_cost"];
                $rtn[$i]["coin"] = $row["mod_eve_tck_coin"];
                $acpId = $row["mod_eve_tck_acp_id"];
                $rtn[$i]["acpId"] = $row["mod_eve_tck_acp_id"];
                $rtn[$i]["cpeId"] = $row["mod_eve_tck_cpe_id"];
                $rtn[$i]["ref"] = $row["mod_eve_tck_ref"];
            }

            $dataReserve = [
                "acpId" => $acpId,
                "cpeId" => $cpe["id"],
                "eveId" => $eventId,
                "reserveId" => $reserveId,
                "pay" => $costTotal,
                "coin" => $coin,
                "rs" => $cpe["name"] . " " . $cpe["lastname"],
                "nit" => $cpe["ci"],
                "type" => $typePay,
                "valueType" => $valueTypePay,
                "state" => 1
            ];


            $var["vars"]["input"] = $dataReserve;

            $array = $this->payments->registerTxTicketEvent($var);

            $return['regiter'] = $array;
            $return['cpeData'] = $cpe;
            $return['reserveData'] = $dataReserve;
            $return['type'] = $typePay;
            $return['num'] = $num;

            if ($typePay == "QR"){
                $return['pay'] = $this->payments->pay_libelula([
                    "email_cliente" => $cpe["email"],
                    "identificador" => $array["id"],
                    "cpeId" => $cpeId,
                    "eventId" => $eventId,
                    "reserveId" => $reserveId,
                    "code" => $array["code"],
                    "type" => "QR",
                    "pay" => $costTotal,
                    "nit" => $cpe["ci"],
                    "ci" => $cpe["ci"],
                    "tokenTx" => $array["code"],
                    "coin" => $coin,
                    "tablesId" => implode(",", array_unique($tablesId)),
                    "ref" => $ref,
                ]);
            } 

            if ($typePay =="cash" && $typePay =="card" && $typePay =="transfer") {
                $return['pay'] = $this->payments->pay_libelula([
                    "email_cliente" => $cpe["email"],
                    "identificador" => $array["id"],
                    "cpeId" => $cpeId,
                    "eventId" => $eventId,
                    "reserveId" => $reserveId,
                    "code" => $array["code"],
                    "type" => $typePay,
                    "pay" => $costTotal,
                    "nit" => $cpe["ci"],
                    "ci" => $cpe["ci"],
                    "tokenTx" => $array["code"],
                    "coin" => $coin,
                    "tablesId" => implode(",", array_unique($tablesId)),
                    "ref" => $ref,
                    "status" => "success",
                ]);
            }
            $return["Error"] = 0;
            $return['status'] = 'success';

            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }
}