"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.resetTable = exports.setData = exports.index = void 0;

var _index = require("../../../components/functions.js");

var _renderChaplin = require("./renderChaplin.js");

var _ticketingEvents = require("../../components/ticketingEvents.js");

// State 0: libre 1:Semillena, 2:Llena, 3:Excedida
var arrayData = {
  imgUrl: "ion/assets/img/plano-1-chaplin.svg?vs=" + _VS,
  mesas: [{
    id: 0
  }, {
    id: 1,
    dataBase: "0/2",
    loan: 2,
    min: 1,
    max: 2,
    type: 2,
    // 1: normal, 2: combine
    rel: [2, 3],
    x: 430,
    y: 110,
    level: 1,
    state: 0
  }, {
    id: 2,
    dataBase: "0/4",
    loan: 2,
    min: 1,
    max: 4,
    type: 2,
    rel: [2, 3],
    x: 400,
    y: 140,
    level: 1,
    state: 0
  }, {
    id: 3,
    dataBase: "0/2",
    loan: 1,
    min: 1,
    max: 2,
    type: 2,
    rel: [2, 3],
    x: 445,
    y: 155,
    level: 1,
    state: 0
  }, {
    id: 4,
    dataBase: "0/4",
    loan: 1,
    min: 1,
    type: 2,
    max: 6,
    rel: [2, 3],
    x: 350,
    y: 140,
    level: 1,
    state: 0
  }]
};
localStorage.setItem("hallDataNow", JSON.stringify(arrayData));
var mesas = []; //let arrayDataNow = JSON.parse(arrayData);
//convertir arrayData en un array de objetos

var arrayDataNow = arrayData.mesas;
arrayDataNow.forEach(function (element, index) {
  var id = element.id;
  var min = element.min;
  var max = element.max;
  var state = element.state;

  if (id != 0) {
    mesas[id] = [];
    mesas[id]["min"] = min;
    mesas[id]["max"] = max;
    mesas[id]["data"] = max;
    mesas[id]["state"] = state;
  }
}); //console.log ("mesas", mesas);

var index = function index() {
  var vars = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  //console.log("chaplinGeneralIndex", vars);
  //console.log(_PATH_WEB + arrayData.imgUrl);
  var ticket = JSON.parse(localStorage.getItem("eventsTicketNow")); //console.log(arrayData);

  (0, _index.loadView)(_PATH_WEB + arrayData.imgUrl).then(function (response) {
    //console.log('loadView', response);
    var str = response;
    var strSVG = '';

    for (var i = 1; i < arrayData.mesas.length; i++) {
      var elem = arrayData.mesas[i]; //console.log("elem", elem)
      //elem['eventId'] = 1;

      strSVG += (0, _renderChaplin.mesa)(elem);
    }

    str = str.replace(/{{_INSERT}}/g, strSVG);
    $(".drawUI[module='" + vars.module + "']").html("<div class=\"containerDraw\">".concat(str, "</div>"));
    svgPanZoom("#mapChaplinShow", {
      zoomEnabled: true,
      controlIconsEnabled: true,
      fit: true,
      center: true,
      customEventsHandler: {
        init: function init(options) {
          function updateSvgClassName() {
            console.log("updateSvgClassName");
          }

          this.listeners = {
            click: function click(e) {
              //console.log("click", e, e.srcElement.localName)
              //console.log("click" , e.target.href.baseVal)
              if (e.srcElement.localName === "use") {
                var id = $(e.target.href.baseVal).attr("mesa");
                console.log("click", id);
                (0, _ticketingEvents.createTicketEvent)({
                  id: id
                });
              }
            }
          };

          for (var eventName in this.listeners) {
            options.svgElement.addEventListener(eventName, this.listeners[eventName]);
          }
        },
        destroy: function destroy(options) {
          for (var eventName in this.listeners) {
            options.svgElement.removeEventListener(eventName, this.listeners[eventName]);
          }
        }
      }
    }); //$(".btnMesa[mesa='2'][event-id='1']").attr("state", 3)
    //$(".btnMesa[mesa='2'][event-id='1'] .data").html("hh")
  })["catch"](console.warn());
};

exports.index = index;

var setData = function setData() {
  var vars = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  //console.log("setData ~ vars:", vars)
  var data = vars.data;

  if (data != 0) {
    //console.log("chaplinGeneralSetData", vars, mesas);
    var ticket = JSON.parse(localStorage.getItem("eventsTicketNow")); //$(".btnMesa").removeClass("disabled");

    data.forEach(function (element, index) {
      //console.log("data.forEach ~ element:", element)
      var tableId = element.tableId;
      var state = element.state;
      var num = parseFloat(element.num);
      var max = parseFloat(mesas[tableId]["max"]); // aqui poner el estado de la mesa

      if (state != 5) {
        $("#mesaId" + tableId + " .data").html(num + "/" + mesas[tableId]["max"]);
        $("#mesaId" + tableId + " ").attr("state", element.state);
      }

      if (num == max) {
        $("#mesaId" + tableId + " ").addClass("disabled");
      }
    });
  } else {
    $(".btnMesa").removeClass("disabled");
    $(".btnMesa").attr("state", 0);
    $("#ticketReservationForm").html("");
    arrayDataNow.forEach(function (element, index) {
      var id = element.id;
      var max = element.max;
      var base = element.dataBase;

      if (id != 0) {
        $("#mesaId" + id + " .data").html(base);
      }
    });
  }
};

exports.setData = setData;

var resetTable = function resetTable() {
  var vars = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  console.log('resetTable', vars);
};

exports.resetTable = resetTable;