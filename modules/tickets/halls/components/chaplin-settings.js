import { loadView, accessToken } from "../../../components/functions.js";
import { mesa } from "./renderChaplin.js";
import { createTicketEvent } from "../../components/ticketingEvents.js";


// State 0: libre 1:Semillena, 2:Llena, 3:Excedida
const arrayData = {
    imgUrl: "ion/assets/img/plano-1-chaplin.svg?vs=" + _VS,
    mesas: [{ id: 0 },
    {
        id: 1,
        dataBase: "0/2",
        loan: 2,
        min: 1,
        max: 2,
        type: 2, // 1: normal, 2: combine
        rel: [2, 3],
        x: 430,
        y: 110,
        level: 1,
        state: 0,
    }, {
        id: 2,
        dataBase: "0/4",
        loan: 2,
        min: 1,
        max: 4,
        type: 2,
        rel: [2, 3],
        x: 400,
        y: 140,
        level: 1,
        state: 0,
    }, {
        id: 3,
        dataBase: "0/2",
        loan: 1,
        min: 1,
        max: 2,
        type: 2,
        rel: [2, 3],
        x: 445,
        y: 155,
        level: 1,
        state: 0,
    }, {
        id: 4,
        dataBase: "0/4",
        loan: 1,
        min: 1,
        type: 2,
        max: 6,
        rel: [2, 3],
        x: 350,
        y: 140,
        level: 1,
        state: 0,
    },
    ]
};


localStorage.setItem("hallDataNow", JSON.stringify(arrayData));
let mesas = [];
//let arrayDataNow = JSON.parse(arrayData);
//convertir arrayData en un array de objetos
let arrayDataNow = arrayData.mesas;

arrayDataNow.forEach((element,index) => {
    
    let id= element.id;
    let min = element.min;
    let max = element.max;
    let state = element.state;

    if (id != 0) {
        mesas[id] = [];
        mesas[id]["min"] = min;
        mesas[id]["max"] = max;
        mesas[id]["data"] = max;
        mesas[id]["state"] = state;
    }
});

//console.log ("mesas", mesas);



export const index = (vars = []) => {
    //console.log("chaplinGeneralIndex", vars);
    //console.log(_PATH_WEB + arrayData.imgUrl);
    let ticket = JSON.parse(localStorage.getItem("eventsTicketNow"));

    //console.log(arrayData);

    loadView(_PATH_WEB + arrayData.imgUrl)
        .then((response) => {
            //console.log('loadView', response);
            let str = response;
            let strSVG = '';
            for (let i = 1; i < arrayData.mesas.length; i++) {
                const elem = arrayData.mesas[i];
                //console.log("elem", elem)
                //elem['eventId'] = 1;
                strSVG += mesa(elem);

            }

            str = str.replace(/{{_INSERT}}/g, strSVG);

            $(".drawUI[module='" + vars.module + "']").html(
                `<div class="containerDraw">${str}</div>`
            );

            svgPanZoom("#mapChaplinShow", {
                zoomEnabled: true,
                controlIconsEnabled: true,
                fit: 1,
                center: true,
                minZoom: 0.5,
                maxZoom: 15,
                customEventsHandler: {
                    init: function (options) {
                        function updateSvgClassName() {
                            console.log("updateSvgClassName")
                        }

                        this.listeners = {
                            click: function (e) {
                                //console.log("click", e, e.srcElement.localName)
                                //console.log("click" , e.target.href.baseVal)
                                if (e.srcElement.localName === "use") {
                                    const id = $(e.target.href.baseVal).attr("mesa");
                                    //console.log("click", id);
                                    createTicketEvent({ id });
                                }
                            },
                        }
                        for (var eventName in this.listeners) {
                            options.svgElement.addEventListener(eventName, this.listeners[eventName])
                        }
                    }, destroy: function (options) {
                        for (var eventName in this.listeners) {
                            options.svgElement.removeEventListener(eventName, this.listeners[eventName])
                        }
                    }
                }
            });

            var panZoomTiger = svgPanZoom('#mapChaplinShow');
            panZoomTiger.zoom(1.25)
             

            //$(".btnMesa[mesa='2'][event-id='1']").attr("state", 3)
            //$(".btnMesa[mesa='2'][event-id='1'] .data").html("hh")
        })
        .catch(console.warn());
};

export const setData = (vars = []) => {
    //console.log("setData ~ vars:", vars)
    
    let data = vars.data;
    

    if (data != 0) {
        //console.log("chaplinGeneralSetData", vars, mesas);
        let ticket = JSON.parse(localStorage.getItem("eventsTicketNow"));
        //$(".btnMesa").removeClass("disabled");
        data.forEach((element,index) => {
            //console.log("data.forEach ~ element:", element)
            let tableId = element.tableId;
            let state = element.state;
            let num = parseFloat(element.num);
            let max = parseFloat(mesas[tableId]["max"]);
            // aqui poner el estado de la mesa
            if (state != 5) {
                $("#mesaId" + tableId + " .data").html(num+"/"+mesas[tableId]["max"]);
                $("#mesaId" + tableId + " ").attr("state", element.state);
            } 

            if (num == max) {
                $("#mesaId" + tableId + " ").addClass("disabled");
            }
            
        });
    }else{
        $(".btnMesa").removeClass("disabled");
        $(".btnMesa").attr("state", 0);
        $("#ticketReservationForm").html("");
        arrayDataNow.forEach((element,index) => {
            let id= element.id;
            let max = element.max;
            let base = element.dataBase;
            if (id != 0) {
                $("#mesaId" + id + " .data").html(base);
            }
        });
    }

    
}

export const resetTable = (vars =[]) => {
    console.log('resetTable',vars);
    
}


