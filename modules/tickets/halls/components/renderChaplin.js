const variables = {
    level: {
        1: {
            stroke: "#ce06fb"
        },
        2: {
            stroke: "#4592fb"
        },
        3: {
            stroke: "#74f0fb"
        },
        4: {
            stroke: "#64ff68"
        },
        5: {
            stroke: "#faa23a"
        },
        6: {
            stroke: "#919b9b"
        },
        7: {
            stroke: "#222"
        }
    },

}

export const mesa = (vars) => {
    //console.log('mesaGrande');
    let id = vars.id || 1;
    let data = vars.dataBase || "";
    let stroke = "#02FB48";
    let fill = "#fff";
    let pX = vars.x || 1;
    let pY = vars.y || 1;
    let pxText1 = pX + 4;
    let pxText2 = pX + 4;
    let pyText1 = pY + 11;
    let pyText2 = pY + 21;
    let dimension = ''
    let fillText = '#333';

    /*     if (vars.state != 0) {
            fill = variables.state[vars.state].fill;
            fillText = variables.state[vars.state].fillText;
        }
        if (vars.nivel != 0) {
            stroke = variables.nivel[vars.nivel].stroke;
        } */

    if (vars.type === 2) {
        dimension = ` width="25" height="25" `;
    }
    if (vars.type === 6) {
        dimension = ` width="35" height="25" `;
    }

    return `
        <g id="mesaId${id}" class="btnMesa" mesa="${id}" event-id="${vars.eventId}" state="${vars.state}" level="${vars.level}" >
            <rect x="${pX}" y="${pY}" class="mesa" mesa="${id}"   ${dimension} rx="3"  stroke-width="2"/>
            <text x="${pxText1}" y="${pyText1}" class="text"  rx="3" font-size="9px" style="font-family: arial; font-weight: bold">M${id}</text>
            <text x="${pxText2}" y="${pyText2}" class="data"  rx="3" font-size="11px" style="font-family: arial;">${data}</text>
        </g>
        <use xlink:href="#mesaId${id}"/>
    `
}