import * as fun from "../../components/functions.js";

const module = "app_tributo_selection";
const system = "app_tributo";

export const app_tributo_selectionIndex = (vars = []) => {
    console.log('app_tributo_selectionIndex', vars);   

    fun.loadView(_PATH_WEB_NUCLEO + "modules/app_tributo/views/selection.html?" + _VS).then((responseView) => {
        //console.log('loadView', responseView);
        let content = responseView;

        content = content.replace(/{{_MODULE}}/g, module);
        content = content.replace(/{{_SYSTEM}}/g, system);  
        content = content.replace(/{{_NAME}}/g,fun.dataModule(module, "name"));  
        content = content.replace(/{{_ICON}}/g,fun.dataModule(module, "icon"));  
        content = content.replace(/{{_COLOR}}/g,fun.dataModule(module, "color"));  

        getData({
            task: 'getDataSelection',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
            
        }).then((response) => {
            console.log('getData getDataSelection', response);
            if (response.status == 'success') {
                let data = response.data;
                let facturaSubidas = data.facturasSubidas;
                let typesContribuyentes = data.typeContribuyentes;
                let typesOficios = data.typeOficios;
              
                let btns = '';

                localStorage.setItem('facturasSubidas', JSON.stringify(facturaSubidas));
                localStorage.setItem('typesContribuyentes', JSON.stringify(typesContribuyentes));
                localStorage.setItem('typesOficios', JSON.stringify(typesOficios));

                facturaSubidas.forEach(element => {
                    btns += `<li class="item btnItemFactura" data-id="${element.id}" >
                        <button class="" data-id="${element.id}" data-type="${element.type}">
                            <span class="text">${element.id} - NIT ${element.nit} </span>
                            <span class="date">${element.file.datatime}</span>
                        </button>
                    </li>`;    
                });

                fun.addHtml({
                    selector: `.bodyModule[module="${module}"]`,
                    type: 'append', //insert, append, prepend, replace
                    content
                });

                fun.addHtml({
                    selector: `.bodyModule[module="${module}"] #listFacturasAppTributo`,
                    type: 'insert', //insert, append, prepend, replace
                    content: btns
                });
                
            }else{
                 fun.alertMessageError({message: response.message})
            }
        }).catch(console.warn());
        

    }).catch(console.warn());
}

export const chargeFormFactura = (factura =[]) => {
    console.log('chargeFormFactura',factura);
  //cammbiar el tipo de factura
  //cammbiar el nitfata
  
    fun.addHtml({
       selector:`.topImage`,
       type: 'insert', //insert, append, prepend, replace
       content : `<span>[${factura.data.id}] ${factura.data.nit} </span>`
    })

    const imageContainer = document.getElementById('image-container');
    const zoomImage = document.getElementById('zoom-image');
    const zoomInButton = document.getElementById('zoom-in');
    const zoomOutButton = document.getElementById('zoom-out');
    const resetButton = document.getElementById('reset');

    let scale = 1;
    const scaleStep = 0.1;
    let offsetX = 0;
    let offsetY = 0;
    let isDragging = false;
    let startX;
    let startY;

    function setScale(newScale) {
      const oldScale = scale;
      scale = Math.min(4, Math.max(0.1, newScale)); // Limitando la escala entre 0.1 y 4
      const factor = scale / oldScale;
      const deltaX = (imageContainer.clientWidth / 2 - offsetX) * (factor - 1);
      const deltaY = (imageContainer.clientHeight / 2 - offsetY) * (factor - 1);
      offsetX += deltaX;
      offsetY += deltaY;
      zoomImage.style.transform = `scale(${scale}) translate(${offsetX}px, ${offsetY}px)`;
    }

    function handleMouseDown(event) {
      event.preventDefault();
      isDragging = true;
      startX = event.clientX - offsetX;
      startY = event.clientY - offsetY;
      zoomImage.style.cursor = 'grabbing';
    }

    function handleMouseUp(event) {
      isDragging = false;
      zoomImage.style.cursor = 'grab';
    }

    function handleMouseMove(event) {
      event.preventDefault();
      if (isDragging) {
        let x = event.clientX - startX;
        let y = event.clientY - startY;
        offsetX = x;
        offsetY = y;
        setScale(scale);
      }
    }

    zoomInButton.addEventListener('click', () => {
      setScale(scale + scaleStep);
    });

    zoomOutButton.addEventListener('click', () => {
      setScale(scale - scaleStep);
    });

    resetButton.addEventListener('click', () => {
      scale = 1;
      offsetX = 0;
      offsetY = 0;
      zoomImage.style.transform = `scale(${scale}) translate(${offsetX}px, ${offsetY}px)`;
    });

    imageContainer.addEventListener('wheel', (event) => {
      event.preventDefault();
      const delta = Math.sign(event.deltaY) * scaleStep;
      setScale(scale + delta);
    });

    zoomImage.addEventListener('mousedown', handleMouseDown);
    zoomImage.addEventListener('mouseup', handleMouseUp);
    zoomImage.addEventListener('mousemove', handleMouseMove);
    zoomImage.addEventListener('mouseleave', handleMouseUp);
}

export const getData = async (vars = []) => {
   //console.log('getData', vars);
   const url = _PATH_WEB_NUCLEO + "modules/app_tributo/controllers/apis/v1/app_tributo.php";
   let data = JSON.stringify({
       accessToken: fun.accessToken(),
       vars: JSON.stringify(vars)
   });
   try {
       let res = await axios({
           async: true,
           method: "post",
           responseType: "json",
           url: url,
           headers: {},
           data: data
        })
       //console.log(res.data);
       fun.jointActions(res.data)
       return res.data
   } catch (error) {
      console.log("Error: getData ", error);
   }
}

document.addEventListener("DOMContentLoaded", function () {
    $("body").on("click",`.btnItemFactura`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let facturaId = data.id;
        
        let facturasSubidas = JSON.parse(localStorage.getItem('facturasSubidas'));

      let factura = facturasSubidas.find(element => element.id == facturaId);
      
      $(".btnItemFactura").removeClass('active');

        $("#image-container").html(`<img id="zoom-image" src="${_PATH_FILES+ factura.file.pathurl}" alt="Imagen a hacer zoom">`);
        $("#inputNitProveedor").focus();
        console.log('facturasSubidas', facturasSubidas, factura);
        $(this).addClass('active');
        $("#formDefAppTributo").removeClass('disabled');
        $("#formDefAppTributo").attr('data-id',facturaId);
        chargeFormFactura({ form: 'formDefAppTributo', data: factura });
    }); 


    $("body").on("change",`input[name="opcionTypeFactura"]`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        const form = this.closest('form');
        const id = form.getAttribute('data-id');
        console.log(`input[name="opcionTypeFactura"]`,data , this.value,form.getAttribute('data-id'));

        getData({
            task: 'updateFacturaQR',
            return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
            inputs: { id, type: this.value }    
        }).then((response) => {
            console.log('getData updateFacturaQR', response);
            if(response.status=='success'){
              
            }else{
              alertMessageError({message: response.message})
            }
        }).catch(console.warn());
    });
});