<?php
header('Content-Type: text/html; charset=utf-8');
class APP_TRIBUTO
{
    var $fmt;
    //var $tributes;

    function __construct($fmt)
    {
        //
        $this->fmt = $fmt;
        //
    }

    public function getDataSelection(array $var = null)
    {
        //return $var;
        $rtn["facturasSubidas"] = $this->getFacturasSubidas($var);
        $rtn["typesContribuyentes"] = $this->getTypesContribuyentes();
        $rtn["typeOficios"] = $this->getTypesOficios();
        return $rtn;
    }

    public function getFacturasSubidas(array $var = null)
    {

        //require_once (_PATH_HOST . "models/class/tributes.class.php");
        //$tributes = new TRIBUTES($this->fmt); */

        //return $var;
        $sql = "SELECT * FROM mod_tributes_facturas WHERE mod_tb_fac_state = '1'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_tb_fac_id"];
                $imgId = $row["mod_tb_fac_img"];
                $return[$i]["id"] = $id;
                $return[$i]["nit"] = $row["mod_tb_fac_nit"];
                $return[$i]["dataNit"] = $this->dataNit($row["mod_tb_fac_nit"]);
                $return[$i]["imgId"] = $imgId;
                $return[$i]["file"] = $this->fmt->files->dataItem($imgId);
                $return[$i]["type"] = $row["mod_tb_fac_type"];
                $return[$i]["periodo"] = $row["mod_tb_fac_periodo"];
                $return[$i]["state"] = $row["mod_tb_fac_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function dataNit($nit = null)
    {
        //return $nit;

        $sql = "SELECT * FROM mod_customers_persons_data WHERE mod_cpdt_cep_nit = '" . $nit . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $rtn["cpdtId"] = $row["mod_cpdt_id"];
                $rtn["businessName"] = $row["mod_cpdt_cep_business_name"];
                $rtn["typeContribuyente"] = $row["mod_cpdt_cep_type_contribuyente"];

                $rtn["typeOficios"] = $row["mod_cpdt_cep_type_oficio"];
                $rtn["priority"] = $row["mod_cpdt_cep_priority"];
                $rtn["type_contract"] = $row["mod_cpdt_cep_type_contract"];
                $rtn["cepId"] = $row["mod_cpdt_cep_id"];
                $rtn["json"] = $row["mod_cpdt_cep_json"];
                $rtn["user"] = $row["mod_cpdt_cep_user"];
                $rtn["pw"] = $row["mod_cpdt_cep_pw"];
            }
            return $rtn;
        } else {
            return 0;
        }
    }

    public function getTypesContribuyentes(array $var = null)
    {

        //return $var;

        $entitieId = $var["entitieId"];
        $sql = "SELECT * FROM mod_types_contribuyentes WHERE mod_typc_state = '1'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_typc_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_typc_name"];
                $return[$i]["description"] = $row["mod_typc_description"];
                $return[$i]["system"] = $row["mod_typc_system"]; //nacional-bolivia
                $return[$i]["state"] = $row["mod_typc_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function getTypesOficios(array $var = null)
    {
        //return $var;

        $entitieId = $var["entitieId"];

        $sql = "SELECT * FROM mod_types_oficios WHERE mod_tb_tof_state = '1'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_tb_tof_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_tb_tof_name"];
                $return[$i]["description"] = $row["mod_tb_tof_description"];
                $return[$i]["code"] = $row["mod_tb_tof_code"];
                $return[$i]["state"] = $row["mod_tb_tof_state"];
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function updateFacturaQR(array $var = null)
    {
        //return $var;
        $facturaId = $var["vars"]["inputs"]["id"];
        $type = $var["vars"]["inputs"]["type"];

        $sql = "UPDATE mod_tributes_facturas SET
                mod_tb_fac_type ='" . $type . "'
                WHERE mod_tb_fac_id = '" . $facturaId . "'";
        $this->fmt->querys->consult($sql);
        return 1;
    }
}