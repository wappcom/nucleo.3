import {
    accessToken,
    loadView,
    addHtml,
    jointActions,
    alertMessageSuccess,
    alertMessageError,
} from "./../../components/functions.js";

import {
    editorText,
    errorInput,
    loadInitChargeMedia,
    loadItemMedia,
    loadFilesFormLoader,
    loadSingleFileFormLoader,
    loadChargeMediaInit,
    removalMediaItems,
    addValueField,
    clearForm,
    removeFileItem,
} from "../../components/forms.js";

import { 
    renderLeagueItem,
    renderDivisionItem,
    renderTeamItem,
    renderGameItems,
    renderGameItem
} from "./renders/renderFutbol.js";


let module = "futbol";
let system = "sports";

export const futbolIndex = (vars = []) => {
    console.log("futbolIndex", vars);
    
    getData({
        task: "getDataFutbol",
        return: "returnArray", // returnId, returnState, returnArray, returnMessage, returnObject
        inputs:{
            inputSport: "futbol"
        }
    }).then((response) => {
            console.log("getData getDataFutbol", response);
            if (response.status == "success") {
                loadView(_PATH_WEB_NUCLEO + "modules/sports/views/futbol.html" + "?" + _VS)
                    .then((responseView) => {
                        //console.log('loadView', responseView);
                        let content = responseView;
                        let data = response.data;
                        let leagues = "";

                        if (data.length > 0) {
                            data.forEach((item) => {
                                leagues += renderLeagueItem({
                                    id: item.id,
                                    name: item.name,
                                });
                            });
                        }

                        content = content.replace(/{{_LEAGUES}}/gi, leagues);

                        addHtml({
                            selector: '.bodyModule[module="futbol"]',
                            type: "append",
                            content,
                        });
                    })
                    .catch(console.warn());
            } else {
                alertMessageError({ message: response.message });
            }
        })
        .catch(console.warn());
};

export const addLeague = ({ inputName, inputDescription, inputJson }) => {
    console.log("addLeague", inputName, inputDescription, inputJson);
    getData({
        task: "addLeague",
        return: "returnId", // returnId, returnState, returnArray, returnMessage, returnObject
        inputs: {
            inputName,
            inputDescription,
            inputJson,
        },
    })
        .then((response) => {
            console.log("getData addLeague", response);
            if (response.status == "success") {
                let formNewLeague = document.querySelector(".formNewLeague");
                let btnAddLeague = document.querySelector("#btnAddLeague");
                formNewLeague.classList.remove("active");
                btnAddLeague.classList.remove("disabled");
                alertMessageSuccess({ message: response.message });
                addHtml({
                    selector: `.listLeagues`,
                    type: "prepend", //insert, append, prepend, replace
                    content: renderLeagueItem({
                        id: response.data,
                        name: inputName,
                    }),
                });
            } else {
                alertMessageError({ message: response.message });
            }
        })
        .catch(console.warn());
};

export const addDivision = ({ inputName, inputDescription, inputJson, inputLeagueId }) => {
    console.log("addDivision", inputName, inputDescription, inputJson, inputLeagueId);
    getData({
        task: "addDivision",
        return: "returnId", // returnId, returnState, returnArray, returnMessage, returnObject
        inputs: {
            inputName,
            inputDescription,
            inputJson,
            inputLeagueId
        },
    })
        .then((response) => {
            console.log("getData addDivision", response);
            if (response.status == "success") {
                let formNewDivision = document.querySelector(".formNewDivision");
                let btnAddSportDivision = document.querySelector("#btnAddSportDivision");
                formNewDivision.classList.remove("active");
                btnAddSportDivision.classList.remove("disabled");
                alertMessageSuccess({ message: response.message });
                addHtml({
                    selector: `.listDivisions`,
                    type: "prepend", //insert, append, prepend, replace
                    content: renderDivisionItem({
                        id: response.data,
                        name: inputName,
                    }),
                });
            } else {
                alertMessageError({ message: response.message });
            }
        })
        .catch(console.warn());
};

export const addTeam = ({ inputName, inputPathUrl,inputDetails, inputJson, inputImagen, inputDivisionId }) => {
    console.log("addTeam", inputName, inputDetails, inputJson, inputImagen);
    getData({
        task: "addTeam",
        return: "returnObject", // returnId, returnState, returnArray, returnMessage, returnObject
        inputs: {
            inputName,
            inputPathUrl,
            inputDetails,
            inputJson,
            inputImagen,
            inputDivisionId
        },
    }).then((response) => {
            console.log("getData addTeam", response);
            if (response.status == "success") {
                let data = response.data;
                let formNewTeam = document.querySelector(".formNewTeam");
                let idFormTeamSports = document.getElementById("formTeamSports");
                let btnAddTeam = document.querySelector("#btnAddTeam");
                formNewTeam.classList.remove("active");
                btnAddTeam.classList.remove("disabled");
                alertMessageSuccess({ message: response.message });
                
                addHtml({
                    selector: `.listTeams`,
                    type: "prepend", //insert, append, prepend, replace
                    content: renderTeamItem({
                        id: data.teamId,
                        name: data.teamName,
                        img: data.img.thumb,
                    }),
                });     

                clearForm(idFormTeamSports);
                removeFileItem();

            } else {
                alertMessageError({ message: response.message });
            }
        }
    ).catch(console.warn());

};

export const addGameDate = ({ inputGameDateLiteral, inputGameDate, inputDivisionId }) => {
    console.log("addGameDate", inputGameDateLiteral, inputGameDate, inputDivisionId);
    getData({
        task: "addGameDate",
        return: "returnId", // returnId, returnState, returnArray, returnMessage, returnObject
        inputs: {
            inputName:inputGameDateLiteral,
            inputDate:inputGameDate,
            inputDivisionId
        },
    }).then((response) => {
            console.log("getData addGameDate", response);
            if (response.status == "success") {
                let formNewGameDate = document.querySelector(".formNewGameDate");
                let btnAddDateGame = document.querySelector("#btnAddDateGame");
                formNewGameDate.classList.remove("active");
                btnAddDateGame.classList.remove("disabled");
                alertMessageSuccess({ message: response.message });
                let content = "";
                let data = response.data;
                
                /* data.forEach((item) => {
                    content += renderGameItem({
                        id: item.id,
                        name: item.name,
                    });
                }); */

                content = renderGameItems({
                    id: data.id,
                    name: data.name,
                });

                

                addHtml({
                   selector:`.listGameDates`,
                   type: 'append', //insert, append, prepend, replace
                   content
                })

                clearForm(document.getElementById("formGameDate"));

            } else {
                alertMessageError({ message: response.message });
            }
        }
    ).catch(console.warn());

};

export const getDivisions = ({ inputLeagueId }) => {
    console.log("getDivisions", inputLeagueId);
    
    $(".bodyModule[module='futbol'] .boxGameDates").remove();

    getData({
        task: "getDivisions",
        return: "returnArray", // returnId, returnState, returnArray, returnMessage, returnObject
        inputs: {
            inputLeagueId
        },
    })
        .then((response) => {
            console.log("getData getDivisions", response);
            if (response.status == "success") {
                loadView(_PATH_WEB_NUCLEO + "modules/sports/views/divisions.html" + "?" + _VS)
                    .then((responseView) => {
                        //console.log('loadView', responseView);
                        let content = responseView;
                        let data = response.data;
                        let divisions = "";

                        if (data.length > 0) {
                            data.forEach((item) => {
                                divisions += renderDivisionItem({
                                    id: item.id,
                                    name: item.name,
                                });
                            });
                        }

                        content = content.replace(/{{_LEAGUE_ID}}/gi, inputLeagueId);
                        content = content.replace(/{{_DIVISIONS}}/gi, divisions);

                        addHtml({
                            selector: '.bodyModule[module="futbol"] #boxLeague',
                            type: "append",
                            content,
                        });
                    })
                    .catch(console.warn());
            } else {
                alertMessageError({ message: response.message });
            }
        })
        .catch(console.warn());
};

export const getGamesDates = ({ inputDivisionId }) => {
    console.log("getGamesDates", inputDivisionId);
    $(".bodyModule[module='futbol'] .boxGameDates").remove();

    getData({
        task: 'getGamesDates',
        return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
        inputs : {
            inputDivisionId
        }
    }).then((response) => {
        console.log('getData getGamesDates', response);
        if(response.status=='success'){
            loadView(_PATH_WEB_NUCLEO + "modules/sports/views/game-dates.html?" + _VS).then((responseView) => {
                //console.log('loadView', responseView);
                let content = responseView;
                let data = response.data;
                let teams = "";
                let gamesDates = "";

                if (data.teams.length > 0) {
                    data.teams.forEach((item) => {
                        teams += renderTeamItem({
                            id: item.id,
                            name: item.name,
                            img: item.img.thumb,
                        });
                    });
                }

                if (data.dates.length > 0) {
                    data.dates.forEach((item) => {
                        gamesDates += renderGameItems({
                            id: item.id,
                            name: item.name,
                        });
                    });
                }
                
                content = content.replace(/{{_LEAGUE_ID}}/gi, "");
                content = content.replace(/{{_DIVISIONS_ID}}/gi,  inputDivisionId);
                content = content.replace(/{{_GAMES_DATES}}/gi, gamesDates);
                content = content.replace(/{{_TEAMS}}/gi, teams);
        
                addHtml({
                    selector: '.bodyModule[module="futbol"] #boxLeague',
                    type: "append",
                    content
                });

                jQuery.datetimepicker.setLocale("es");

                $(".formNewGameDate #inputGameDate").datetimepicker({
                    timepicker: false,
                    datepicker: true,
                    format: "Y-m-d",
                    lang: "es",
                });
                
            }).catch(console.warn());
        }else{
             alertMessageError({message: response.message})
        }
    }).catch(console.warn());

    
};

export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url =
        _PATH_WEB_NUCLEO + "modules/sports/controllers/apis/v1/futbol.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars),
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data,
        });
        //console.log(res.data);
        jointActions(res.data);
        return res.data;
    } catch (error) {
        console.log("Error: getData ", error);
    }
};

document.addEventListener("click", function (e) {
    //console.log ("click fultbol", e.target);
    let id = e.target.id;
    let cls = e.target.classList;

    let formNewLeague = document.querySelector(".formNewLeague");
    let btnAddLeague = document.querySelector("#btnAddLeague");

    let formNewDivision = document.querySelector(".formNewDivision");
    let btnAddSportDivision = document.querySelector("#btnAddSportDivision");

    let formNewTeam = document.querySelector(".formNewTeam");
    let btnAddTeam = document.querySelector("#btnAddTeam");

    let formNewGameDate = document.querySelector(".formNewGameDate");
    let btnAddDateGame = document.querySelector("#btnAddDateGame");

    if (id == "btnAddLeague") {
        //add class to formNewLeague
        formNewLeague.classList.add("active");
        // add class disabled to btnAddLeague

        btnAddLeague.classList.add("disabled");
    }

    if (id == "btnAddSportDivision") {
        formNewDivision.classList.add("active");
        btnAddSportDivision.classList.add("disabled");
    }

    if (id == "btnCancelLeague") {
        //add class to formNewLeague
        formNewLeague.classList.remove("active");
        // add class disabled to

        btnAddLeague.classList.remove("disabled");
    }

    if (id == "btnSaveLeague") {
        console.log("btnSaveLeague");
        let inputName = document.querySelector("#inputLeagueName").value;
        if (inputName == "") {
            errorInput("inputLeagueName", "Debe ingresar un nombre.");
            return;
        }
        addLeague({
            inputName,
            inputDescription: document.querySelector("#inputLeagueDescription")
                .value,
            inputJson: document.querySelector("#inputLeagueJson").value,
        });
    }

    if (cls.contains("btnLeagueGo")) {
        console.log("btnLeagueGo", e.target.dataset);
        getDivisions({ inputLeagueId: e.target.dataset.leagueId});
    }

    if (id == "btnSaveDivision"){
        console.log("btnSaveDivision");
        let inputName = document.querySelector("#inputDivisionName").value;
        let inputLeagueId = e.target.dataset.leagueId;

        if (inputName == "") {
            errorInput("inputDivisionName", "Debe ingresar un nombre.");
            return;
        }
        addDivision({
            inputName,
            inputDescription: document.querySelector("#inputDivisionDescription")
                .value,
            inputJson: document.querySelector("#inputDivisionJson").value,
            inputLeagueId
        });
    }

    if (id == "btnCancelDivision") {
        formNewDivision.classList.remove("active");
        btnAddSportDivision.classList.remove("disabled");
    }

    if (cls.contains("btnDivisionGo")) {
        console.log("btnDivisionGo", e.target.dataset);
        getGamesDates({ inputDivisionId: e.target.dataset.divisionId});
    }

    if (id == "btnAddTeam"){
        formNewTeam.classList.add("active");
        btnAddTeam.classList.add("disabled");
    }

    if (id == "btnCancelTeam") {
        formNewTeam.classList.remove("active");
        btnAddTeam.classList.remove("disabled");
    }

    if (id == "btnSaveSportTeam"){
        let inputName = document.querySelector("#inputTeamSportName").value;
        let inputDetails = document.querySelector("#inputTeamSportDetails").value;
        let inputJson = document.querySelector("#inputTeamSportJson").value;
        let inputImagen = document.querySelector("#inputTeamSportImagen").value;
        let inputPathUrl = document.querySelector("#inputTeamSportPathUrl").value;
        let inputDivisionId = e.target.dataset.divisionId;

        if (inputName == "") {
            errorInput("inputTeamSportName", "Debe ingresar un nombre.");
            return;
        }

        addTeam ({
            inputName,
            inputPathUrl,
            inputDetails,
            inputJson,
            inputImagen,
            inputDivisionId
        });
    }

    if (id == "btnAddDateGame"){
        console.log("btnAddDateGame");
        formNewGameDate.classList.add("active");
        btnAddDateGame.classList.add("disabled");
    }

    if (id == "btnSaveGameDate"){
        console.log("btnSaveGameDate");
        let inputGameDateLiteral = document.querySelector("#inputGameDateLiteral").value;
        let inputGameDate = document.querySelector("#inputGameDate").value;
        let inputDivisionId = e.target.dataset.divisionId;

        if (inputGameDateLiteral == "") {
            errorInput("inputGameDateLiteral", "Debe ingresar un fecha literal.");
            return;
        }

        addGameDate({
            inputGameDateLiteral,
            inputGameDate,
            inputDivisionId
        });
    }

    if (id == "btnCancelGameDate") {
        formNewGameDate.classList.remove("active");
        btnAddDateGame.classList.remove("disabled");
    }
    
});
