export const renderLeagueItem = (vars = []) => {
    console.log("renderLeagueItem", vars);
    return /*html*/ `
        <button class="btnSelectSportItem btnLeagueGo" title="ID ${vars.id}" data-league-id="${vars.id}">
            <span>${vars.name}</span>
            <i class="icon icon-chevron-right"></i>
        </button>
    `;
};


export const renderDivisionItem = (vars = []) => {
    console.log("renderDivisionItem", vars);
    return /*html*/ `
        <button class="btnSelectSportItem btnDivisionGo" title="ID ${vars.id}" data-division-id="${vars.id}">
            <span>${vars.name}</span>
            <i class="icon icon-chevron-right"></i>
        </button>
    `;
};

export const renderTeamItem = (vars = []) => {
    console.log("renderTeamItem", vars);
    return /*html*/ `
        <button class="btnSelectSportItem btnTeamGo" title="ID ${vars.id}" data-team-id="${vars.id}">
            <i class="icon icon-points-v"></i>
            <div class="info">
                <div class="img"><img src="${_PATH_FILES+vars.img}" alt="${vars.name}"></div>
                <span>${vars.name}</span>
            </div>
        </button>
    `;
}

export const renderGameItems = (vars = []) => {
    console.log("renderGameItems", vars);
    return /*html*/ `
        <div class="btnSelectSportItem gameItem" data-id="${vars.id}">
            <div class="header">
                <div class="name">${vars.name}</div>
                <a class="btn btnPrimary btnMini" data-id="${vars.id}" >
                    <i class="icon icon-plus"></i>
                </a>
            </div>
            <div class="listGamesItems" data-id="${vars.id}">
            </div>
        </div>
    `;
}

export const renderGameItem = (vars = []) => {
    console.log("renderGameItem", vars);
    return /*html*/ `
        <button class="btnSelectSportItem btnGameItemGo" title="ID ${vars.id}" data-team-id="${vars.id}">
            <i class="icon icon-points-v"></i>
            <div class="info">
                <span>${vars.team1}</span>
                <span>vs</span>
                <span>${vars.team2}</span>
            </div>
        </button>
    `;
}


