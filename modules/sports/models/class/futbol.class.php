<?php
header('Content-Type: text/html; charset=utf-8');
class FUTBOL
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function dataId(array $var = null)
    {
        return $var;
    }

    public function getDataFutbol(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $sport = $var["vars"]["inputs"]["inputSport"];

        $sql = "SELECT * FROM mod_leagues WHERE mod_lg_ent_id = '" . $entitieId."' AND mod_lg_sport = '".$sport."'";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_lg_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_lg_name"];
                $return[$i]["description"] = $row["mod_lg_description"];
                $return[$i]["json"] = $row["mod_lg_json"];
                $return[$i]["sport"] = $row["mod_lg_sport"];
                $return[$i]["state"] = $row["mod_lg_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function getDivisions(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $inputLeagueId = $var["vars"]["inputs"]["inputLeagueId"];

        $sql = "SELECT * FROM mod_leagues_divisions WHERE mod_lg_div_lg_id = '".$inputLeagueId."'";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_lg_div_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_lg_div_name"];
                $return[$i]["description"] = $row["mod_lg_div_description"];
                $return[$i]["json"] = $row["mod_lg_div_json"];
                $return[$i]["state"] = $row["mod_lg_div_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function getGamesDates(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $inputDivisionId = $var["vars"]["inputs"]["inputDivisionId"];

        $sql = "SELECT mod_sp_tm_id,mod_sp_tm_name,mod_sp_tm_logo FROM mod_sports_teams, mod_leagues_divisions_teams WHERE mos_sp_tm_ent_id = '".$entitieId."' AND mod_lg_div_tm_div_id='".$inputDivisionId."' AND mod_lg_div_tm_tm_id= mod_sp_tm_id ORDER BY mod_sp_tm_name ASC";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_sp_tm_id"];
                $return["data"]["teams"][$i]["id"] = $id;
                $return["data"]["teams"][$i]["name"] = $row["mod_sp_tm_name"];
                $return["data"]["teams"][$i]["img"] =  $this->fmt->files->dataBasicImg($row["mod_sp_tm_logo"]);
            }
        }else{
            $return["data"]["teams"] = 0;
        }

        $sql = "SELECT * FROM mod_sports_dates WHERE mod_sp_dt_ent_id = '".$entitieId."'  ORDER BY mod_sp_dt_date ASC";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_sp_dt_id"];
                $return["data"]["dates"][$i]["id"] = $id;
                $return["data"]["dates"][$i]["name"] = $row["mod_sp_dt_name"];
                $return["data"]["dates"][$i]["date"] = $row["mod_sp_dt_date"];
            }
        }else{
            $return["data"]["dates"] = 0;
        }
        
        $return["Error"] = 0;
        $return["status"] = 'success';
        //$return["data"]["teams"] = 0;
        //$return["data"]["dates"] = 0;

        return $return; 

    }

    public function addLeague(array $var = null)
    {
        //return $var;
        $entitieId = $var['entitieId'];
        $inputs = $var['vars']['inputs'];
        $inputName = $inputs['inputName'];
        $inputDescription = $inputs['inputDescription'];
        $inputJson = $inputs['inputJson'];
        $inputSport = $inputs['inputSport'];

        $insert = "mod_lg_name, mod_lg_description, mod_lg_sport, mod_lg_json, mod_lg_ent_id, mod_lg_state";
        $values = "'" . $inputName . "','" . $inputDescription . "','".$inputSport."','" . $inputJson . "','" . $entitieId . "','1'";
        $sql = "insert into mod_leagues  (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_lg_id) as id from mod_leagues";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $id = $row["id"];

        return $id;

    }

    public function addDivision(array $var = null)
    {
        //return $var;
        $inputs = $var['vars']['inputs'];
        $inputName = $inputs['inputName'];
        $inputDescription = $inputs['inputDescription'];
        $inputJson = $inputs['inputJson'];
        $inputLeagueId = $inputs['inputLeagueId'];

        $insert = "mod_lg_div_name, mod_lg_div_description, mod_lg_div_json, mod_lg_div_lg_id, mod_lg_div_state";
        $values = "'" . $inputName . "','" . $inputDescription . "','" . $inputJson . "','" . $inputLeagueId . "','1'";
        $sql = "insert into mod_leagues_divisions  (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_lg_div_id) as id from mod_leagues_divisions";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $id = $row["id"];

        return $id;
    }
    
    public function addTeam(array $var = null)
    {
        //return $var;
        $entitieId = $var['entitieId'];
        $inputs = $var['vars']['inputs'];
        $inputName = $inputs['inputName'];
        $inputPathUrl = $inputs['inputPathUrl'];
        $inputDetails = $inputs['inputDetails'];
        $inputJson = $inputs['inputJson'];
        $inputImagen = $inputs['inputImagen'];
        $inputDivisionId = $inputs['inputDivisionId'];
        $inputSport = $inputs['inputSport'] ? $inputs['inputSport'] : 'futbol';

        $insert = "mod_sp_tm_sport,mod_sp_tm_name,mod_sp_tm_pathurl,mod_sp_tm_details,mod_sp_tm_logo,mod_sp_tm_banner,mod_sp_tm_json,mos_sp_tm_ent_id,mod_sp_tm_state";
        $values ="'" . $inputSport . "','" . $inputName . "','".$inputPathUrl."','" . $inputDetails . "','" . $inputImagen . "','','" . $inputJson . "','" . $entitieId . "','1'";
        $sql= "insert into mod_sports_teams (".$insert.") values (".$values.")";
        $this->fmt->querys->consult($sql,__METHOD__);

        $sql = "select max(mod_sp_tm_id) as id from mod_sports_teams";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $id = $row["id"];

        $insert = "mod_lg_div_tm_div_id,mod_lg_div_tm_tm_id,mod_lg_div_tm_order";
        $values = "'" . $inputDivisionId . "','" . $id . "','1'";
        $sql = "insert into mod_leagues_divisions_teams (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $rtn["Error"] = 0;
        $rtn["status"] = "success";
        $rtn["data"]["teamId"] = $id;
        $rtn["data"]["teamName"] = $inputName;
        $rtn["data"]["img"] = $this->fmt->files->dataBasicImg($inputImagen);

        return $rtn;
    }

    public function addGameDate(array $var = null)
    {
        //return $var;
        $entitieId = $var['entitieId'];
        $inputs = $var['vars']['inputs'];
        $inputName = $inputs['inputName'];
        $inputDate = $inputs['inputDate'];
        $inputDivisionId = $inputs['inputDivisionId'];

        $insert = "mod_sp_dt_name,mod_sp_dt_date,mod_sp_dt_div_id,mod_sp_dt_ent_id,mod_sp_dt_state";
        $values = "'" . $inputName . "','" . $inputDate . "','".$inputDivisionId."','" . $entitieId . "','1'";
        $sql = "insert into mod_sports_dates (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_sp_dt_id) as id from mod_sports_dates";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $id = $row["id"];

        return $id;
        
    }
}