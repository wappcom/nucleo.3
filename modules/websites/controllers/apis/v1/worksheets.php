<?php
/* $foo = file_get_contents("php://input"); echo json_encode($foo, true); exit(0);*/

$dataPost = json_decode(file_get_contents("php://input"), true);//
//echo json_encode($dataPost); exit(0);
//echo json_encode($dataPost["accessToken"]["access_token"]); exit(0);

require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $dataPost["accessToken"]["page"] . "config.php");
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

require_once(_PATH_NUCLEO . "models/class/class.contents.php");
$contents = new CONTENTS($fmt);

header("Content-Type: application/json");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

switch ($_SERVER["REQUEST_METHOD"]) {
    case 'POST':
        $access_token = $dataPost["accessToken"]['access_token'];
        $refresh_token = $dataPost["accessToken"]['refresh_token'];
        $entitieId = $dataPost["accessToken"]['entitieId'];
        $action = $dataPost["action"];
        //echo json_encode("1:". $action); exit(0);
        //echo json_encode("2:". $entitieId); exit(0);

        $userId = $fmt->users->validateUserToken(array("access_token" => $access_token, "refresh_token" => $refresh_token, "idEntitie" => $entitieId));

        //echo json_encode($userId); exit(0);
        if (!$userId) {
            errorDefault($fmt, "Error: Token invalid");
        }

        if ($userId) {
            $return["user"] = $fmt->users->userDataAuth(array("access_token" => $access_token, "idEntitie" => $entitieId));
            $return["vars"] = json_decode($dataPost['vars'], true);
            $return["entitieId"] = $entitieId;
            $userId = $return["user"]["userId"];
            $rolId = $return["user"]["rolId"];
            $task = $return["vars"]["task"];
            $module = $return["vars"]["module"];
            $rtn = $return["vars"]["return"];

            //$fmt->auth->getActionState
            //$fmt->auth->getActionReturnId
            //$fmt->auth->getActionReturnArray

            if ($action == "getPatterns") {
                $fmt->auth->getActionReturnArray($fmt->worksheets->getPatterns($return));
            } 

            if ($action == "savePattern") {
                //echo json_encode($return); exit(0);
                $fmt->auth->getActionReturnId($fmt->worksheets->savePattern($return));
            }

             if ($action == "deletePattern") {
                //echo json_encode($return); exit(0);
                $fmt->auth->getActionState($fmt->worksheets->deletePattern($return));
            }  


            if ($action == "getWorksheets") {
                $fmt->auth->getActionReturnArray($fmt->worksheets->getWorksheets($return));
            }

            if ($action == "getWorksheetsId") {
                $fmt->auth->getActionReturnArray($fmt->worksheets->getWorksheetsId($return));
            }

            if ($action == "updateWorksheet") {
                //echo json_encode($return); exit(0);
                $fmt->auth->getActionState($fmt->worksheets->updateWorksheet($return));
            }  
            
            switch ($rtn) {
                case "returnArray":
                    $auxReturn =  "getActionReturnArray";
                    break;
                case "returnId":
                    $auxReturn =  "getActionReturnId";
                    break;                
                case "returnState":
                    $auxReturn =  "getActionState";
                    break;
                case "returnObject":
                    $auxReturn =  "getActionObject";
                    break;
                    
                default:
                    errorDefault($fmt, "Error: Return invalid");
                    break;
            }

           // echo json_encode ($auxReturn.":".$action); exit(0);

            //$fmt->auth->getActionState
            //$fmt->auth->getActionReturnId
            //$fmt->auth->getActionReturnArray

            //$fmt->auth->getActionReturnId($fmt->worksheets->savePattern($return));
            if (!empty($task) && $module=="worksheets") {
                $fmt->auth->$auxReturn($fmt->worksheets->$task($return));
            }

            if (!empty($task) && $module=="publications") {
                $fmt->auth->$auxReturn($fmt->pubs->$task($return));
            }
            

 
        }
        break;

    default:
    errorDefault($fmt);
    break;

}

function errorDefault($fmt, $error = "Access Auth. Metod request.")
{
    echo $fmt->errors->errorJson([
        "description" => $error,
        "code" => "",
        "lang" => "es"
    ]);
    exit(0);
}