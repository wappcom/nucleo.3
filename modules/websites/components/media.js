import {
    loadView,
    accessToken,
    alertMessageError,
    replacePath,
    addHtml,
    capitalize
} from '../../components/functions.js';

import {
    createTable,
    dataTable,
    removeItemTable
} from "../../components/tables.js";

import {
	innerForm,
    renderDeleteModal,
    renderDeleteModalFn
} from "../../components/renders/renderModals.js";

import {
    renderTr
} from './renders/renderMedia.js';

import {
    removeModalId 
} from "../../components/modals.js";


const system = "websites";
const module = "media";
const pathUrl = "modules/websites/";
const tableId = "mediaTable";
const formId = "mediaForm";

export const mediaIndex = (vars = []) => {
    //console.log('mediaIndex', vars);
    loadView(_PATH_WEB_NUCLEO + "modules/websites/views/media.html?v" + _VS).then((responseView) => {
        //console.log('loadView', responseView);
        getData({
            task: 'getMediaList',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
        }).then((response) => {
            console.log('getData getMediaList', response);
            let str = responseView;
            str = str.replace(/{{_MODULE}}/g, module);
            str = str.replace(/{{_SYSTEM}}/g, system);
            str = str.replace(/{{_PATHURL}}/g, pathUrl);
            str = str.replace(/{{_FN}}/g, "formNewMedia");
            str = replacePath(str);
            $(".bodyModule[module='" + module + "']").html(str);
            if (response != null && response.Error == 0) {
                //const tableId = tableId;
                const data = response.data;
                let strTable = "";
                let tbody = "";
                //console.log(data);

                for (let i = 0; i < data.length; i++) {
                    const elem = data[i];
                    elem["table"] = tableId;
                    elem["module"] = module;
                    elem["system"] = system;
                    tbody += renderTr(elem);
                }

                strTable = createTable({
                    id: tableId,
                    thead: ":check,id:colId,Nombre,Estado:colState,Acciones:colActions",
                    body: tbody,
                });

                $(".bodyModule[module='" + module + "'] >.tbody").html(
                    strTable
                );

                dataTable({
                    elem: "#" + tableId,
                    orderCol: 1,
                });

            } else if (response.data == 0) {
                $(".bodyModule[module='" + module + "'] >.tbody").html(
                    renderNoDataTable()
                );
            } else {
                alertMessageError({
                    message: response.message
                })
            }
        }).catch(console.warn());

    }).catch(console.warn());
}

export const formNewMedia = (vars =[]) => {
    console.log('click')
    loadView(_PATH_WEB_NUCLEO + "modules/websites/views/formMedia.html?" + _VS).then((responseView) => {
        let str = responseView;
        str = str.replace(/{{_TITLE}}/g, "Nuevo Documento");
        str = str.replace(/{{_FORM_ID}}/g,formId);
        str = str.replace(/{{_MODULE}}/g,module);
        str = str.replace(/{{_SYSTEM}}/g,system);
        str = str.replace(/{{_BTN_LABEL}}/g,"Guardar");

        // getData({
        //     task: 'getDataDocs',
        //     return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
            
        // }).then((response) => {
        //     if(response.status=='success'){
        //         let categorys = response.data.categorys;
        //         let date = response.data.date;

        //         str = str.replace(/{{_CATEGORYS}}/g, listCategorys(categorys));
        //         str = str.replace(/{{_BTN_ACTION}}/g, 'btnAddDoc');

        //         innerForm({
        //             cls: 'mode-compact',
        //             module,
        //             body: str,
        //         });

        //         jQuery.datetimepicker.setLocale(_LOCALE);

        //         $(`#${formId} #inputDate`).val(date);
        //         $(`#${formId} #inputDate`).datetimepicker({
        //             timepicker: false,
        //             datepicker: true,
        //             format: "Y-m-d H:i",
        //             lang: "es",
        //             timeZone: _TIMEZONE
        //         });
        //         console.log('innerForm',formId);
        //     }else{
        //          alertMessageError({message: response.message})
        //     }
        // }).catch(console.warn());
    });
}

export const deleteMediaItem = (vars = []) => {
    console.log('deleteMediaItem', vars);

    getData({
        task: 'deleteMediaItem',
        return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
        inputs : vars
    }).then((response) => {
        //console.log('getData deleteMediaItem ', response);
        if(response.status=='success'){
            removeModalId('modalDelete');
            removeItemTable(vars.inputId,tableId);
                
        }else{
             alertMessageError({message: response.message})
        }
    }).catch(console.warn());
}

export const deleteMediaItems = (vars = []) => {
    console.log('deleteMediaItems', vars);
    getData({
        task: 'deleteMediaItems',
        return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
        inputs : vars
    }).then((response) => {
        console.log('getData deleteMediaItems ', response);
        if(response.status=='success'){
            removeModalId('modalDelete');
            for (let i = 0; i < vars.inputIds.length; i++) {
                const element = vars.inputIds[i];
                removeItemTable(element,tableId);
            }  
            //restaurar select a option value 0 
            $(`.selectActionsMediaTable[for="#${tableId}"]`).val(0);
        }else{
             alertMessageError({message: response.message})
        }
    }).catch(console.warn());
}

export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/media.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data;
    } catch (error) {
        console.log("Error: getDataPlaces", error);
    }
}

document.addEventListener("DOMContentLoaded", function () {
    $("body").on("change",`.selectActionsMediaTable[for="#${tableId}"]`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.selectActionsMediaTable`, data);
        let ids = [];
        $(`#${tableId} input[name="inputCheck[]"]:checked`).each(function() {
            ids.push($(this).val());
        })
        //console.log("ids", ids);
        deleteMediaItems({inputIds : ids});
    }); 

    $("body").on("click",`.btnDeleteItemMedia`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnDeleteItemMedia`, data);


        addHtml({
            selector: `body`,
            type: "append", // insert, append, prepend, replace
            content: renderDeleteModalFn({
                name: data.name,
                id: "modalDelete",
                module,
                attr: `data-module="${module}" data-id="${data.id}"`,
            }),
        });
        
    });

    $("body").on("click",`.btnDeleteConfirmationMedia`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`.btnDeleteConfirmationMedia`,data);

        deleteMediaItem({ inputId: data.id });
    });
});