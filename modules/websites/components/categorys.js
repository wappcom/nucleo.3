import {
    empty,
    replacePath,
    replaceEssentials,
    replaceAll,
    loadView,
    stopLoadingBar,
    emptyReturn,
    loadingBtnIcon,
    btnLoadingRemove,
    unDisableId,
    accessToken,
    removeBtnLoading,
    dataModule,
    alertPage,
    alertMessageError,
    addHtml,
    capitalize
} from "../../components/functions.js";

import {
    sortJSON,
    editorText,
    errorInput,
    errorFormInput,
    loadInitChargeMedia,
    loadItemMedia,
    loadFilesFormLoader,
    loadSingleFileFormLoader,
    loadChargeMediaInit,
    removalMediaItems,
    optionsForm,
    validateUrl,
    validatePathUrlNode,
    listElemSelectables,
    loadChargeImageInit
} from "../../components/forms.js";

import {
    removeModalId
}
from "../../components/modals.js";

import {
    renderItemInputContent,
    renderItemImage
} from "../../components/renders/renderForms.js";

import {
    renderDeleteModal,
    renderDeleteOrRemove,
    innerForm,
    renderModalClean
} from "../../components/renders/renderModals.js";

import {
    renderNode,
    renderTaskNodes,
} from "./renders/renderCategorys.js";

import {
    loadWorksheetsNode
} from "./worksheets.js";

let pathurl = "modules/websites/";
var module = "categorys";
let system = "websites";
let tableId = "categorys";

export const categorysIndex = () => {
    getCategorys().then((e) => {
        //console.log('getCategorys', _CATEGORYS);
        loadView(_PATH_WEB_NUCLEO + pathurl + "views/categorys.html?" + _VS).then((htmlView) => {
            //console.log('loadView', htmlView);
            let html = htmlView;
            html = html.replaceAll("{{_CHILD}}", _CATEGORYS.length);

            $(".bodyModule[module='" + module + "']").html(
                replaceEssentials({
                    str: html,
                    module,
                    system,
                    pathurl,
                    color: dataModule(module, "color"),
                    icon: dataModule(module, "icon"),
                    name: dataModule(module, "name"),
                })
            );

            createCategorys();

        }).catch(console.warn());
    }).catch(console.warn());
}


export const createCategorys = () => {

    //listCategorys(_CATEGORYS)
    //let rootCategorys = document.getElementById("rootCategorys");
    let list = /*html*/ `
    <!--<div class="newNode" item="0" parent="0" child="${_CATEGORYS.length}">
        <a class="btn btnNewNode btnAddNode">
            <i class="icon icon-plus"></i><span>Nuevo nodo</span>
        </a>
    </div> -->
    <div class="nodes">
        ${listCategorys(_CATEGORYS)}       
    </div>
    `;

    $("#rootCategorys").html(list);
}

export const getCategorys = async () => {
    //console.log('getCategorys');
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/categorys.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "getCategorys"
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        _CATEGORYS = res.data.data;
        //return res.data
    } catch (error) {
        console.log("Error: getCategorys")
        console.log(error)
    }
}

export function listCategorys(vars) {
    //console.log(vars)
    let str = "";
    let lastItem = "";
    for (let i = 0; i < vars.length; i++) {
        const id = vars[i].id;
        const name = vars[i].name;
        const level = vars[i].level;
        const children = vars[i].children;
        const state = vars[i].state;
        const pathurl = vars[i].pathurl;
        let activeChild = "";
        let childs = "";
        let numChildrens = "";

        var numLast = vars.length - 1;
        if (i == numLast) {
            lastItem = "level-last";
        } else {
            lastItem = "";
        }

        if (children == 0) {
            activeChild = "";
            numChildrens = 0;
        } else {
            activeChild = "haveChild";
            numChildrens = children.length;
        }


        //console.log("children : " + children)
        //console.log("children.length: " + children.length)
        if (children != 0) {
            if (children.length != 0) {
                //console.log("tiene hijos" + children)
                children.parentId = id;
                childs = listCategorysChildren(children);
            }
            //console.log(str)
        }

        str += renderNode({
            level,
            name,
            id,
            lastItem,
            parent: 0,
            activeChild,
            childs,
            numChildrens,
            state,
            pathurl,
        });
    }
    return str;
}

export function listCategorysChildren(vars) {
    //console.log(vars)
    //console.log(vars.length)
    var str = "";
    let lastItem = "";
    for (let i = 0; i < vars.length; i++) {
        var levelMargin = "";
        const typeLine = "";
        const id = vars[i].id;
        const name = vars[i].name;
        const level = vars[i].level;
        const children = vars[i].children;
        const state = vars[i].state;
        const parent = vars.parentId;
        const pathurl = vars[i].pathurl;
        let childs = "";
        let numChildrens = "";
        let activeChild = "";

        var numLast = vars.length - 1;

        if (i == numLast) {
            lastItem = "level-last";
        } else {
            lastItem = "";
        }
        //console.log("listCategorysChildren children.length: " + children.length)

        var levelStatus = "";
        let mleft = 24 * level;
        if (children.length >= 1) {
            levelStatus = "childrenActive";
            numChildrens = children.length;
        } else {
            levelStatus = "";
            numChildrens = 0;
        }
        if (children.length == 1) {
            levelStatus = "childActive";
        }

        if (children == 0) {
            activeChild = "";
        } else {
            activeChild = "haveChild";
        }


        /* str += `<div class="checkbox level level${level} ${lastItem}" level="${level}" child="${levelStatus}" id="item${id}" item="${id}" parent="${parent}" >
                    <span class="box ${levelStatus}"><input name="inputCategorys[]" id="inputCatTree${id}" type="checkbox" value="${id}"></span>
                    <span class="name">${name}</span>`;
        str += `</div>`; */

        if (children != 0) {
            if (children.length != 0) {
                //str += `<div class="children  ${levelStatus}" for="${id}" >`;
                children.parentId = id;
                childs = listCategorysChildren(children);
                //str += "</div>";
            }
        }

        str += renderNode({
            level,
            name,
            id,
            lastItem,
            parent,
            activeChild,
            childs,
            numChildrens,
            state,
            pathurl
        })
    }
    return str;
}

export function checkCategorys(categorys, name = "inputCategorys[]") {
    //console.log(categorys, name);
    if (categorys != 0) {
        for (let j = 0; j < categorys.length; j++) {
            const id = categorys[j].id;
            $("input[name='" + name + "'][value='" + id + "']").attr(
                "checked",
                "checked"
            );
        }
    }
}

export const optionsCategorys = (vars = []) => {
    //console.log('optionsCategorys', vars);
    let options = vars.options;
    let initialLabel = vars.initialLabel ? vars.initialLabel : "Raiz";
    let idSelected = vars.idSelected ? vars.idSelected : 0;

    let html = `<option value="0">${initialLabel}</option>`;
    for (let i = 0; i < options.length; i++) {
        let item = options[i].id;
        let name = options[i].name;
        let level = options[i].level;
        let numChildrens = options[i].children.length;
        let children = options[i].children;
        let selected = "";
        let aux = "";
        if (item == idSelected) {
            selected = "selected";
        }
        for (let j = 0; j < level; j++) {
            aux += "--"
        }
        name = aux + name;
        html += `<option value="${item}" ${selected}>${name}</option>`;
        if (numChildrens > 0) {
            html += optionsChildrenCategorys({
                options: children,
                idSelected
            });
        }
    }
    return html;
}

export const optionsChildrenCategorys = (vars = []) => {
    //console.log('optionsChildrenCategorys', vars);
    let options = vars.options;
    let idSelected = vars.idSelected ? vars.idSelected : 0;
    let html = "";

    for (let i = 0; i < options.length; i++) {
        let item = options[i].id;
        let name = options[i].name;
        let level = options[i].level;
        let numChildrens = options[i].children.length;
        let children = options[i].children;
        let selected = "";
        let aux = "";
        if (item == idSelected) {
            selected = "selected";
        }
        for (let j = 0; j < level; j++) {
            aux += "--"
        }
        name = aux + name;
        html += `<option value="${item}" ${selected}>${name}</option>`;
        if (numChildrens > 0) {
            html += optionsChildrenCategorys({
                options: children,
                idSelected
            });
        }
    }
    return html;
}

export const loadFormNewNode = (vars) => {
    //console.log('loadFormNewNode', vars);
    let item = vars.item;
    let parent = vars.parent;
    let child = parseFloat(vars.child);
    let order = child + 1;
    getCategorys().then((e) => {
        //console.log('loadFormNewNode', item, parent, child, order);

        loadView(_PATH_WEB_NUCLEO + "modules/websites/views/formCategory.html?" + _VS).then((response) => {
            //console.log('formCategorys', response);
            let html = response;
            html = html.replace(/\{{_LABEL}}/g, "Nueva categoria");
            html = html.replace(/\{{_FORM_ID}}/g, "formNewNode");
            html = html.replace(/\{{_BTN_ACTION}}/g, "btnSaveNewNode");
            html = html.replace(/\{{_BTN_NAME_ACTION}}/g, "Guardar");
            html = html.replace(/\{{_RELATED}}/g, "");
            html = html.replace(/\{{_LIST_RELATED}}/g, listElemSelectables({
                data: _CATEGORYS,
                btn: "btnItemSelect"
            }));
            html = html.replace(/\{{_LIST_RELATED_PATHURL}}/g, listElemSelectables({
                data: _CATEGORYS,
                btn: "btnItemSelectPathUrl"
            }));
            html = html.replace(/\{{_PARENTS}}/g, optionsCategorys({
                options: _CATEGORYS,
                initialLabel: "Raiz",
                idSelected: item
            }));

            $(".bodyModule[module='" + module + "']").append(replaceEssentials({
                str: html,
                module,
                system,
                pathurl,
                color: dataModule(module, "color"),
            }));

            $("#inputOrder").val(order);
            $("#inputNameNode").focus();
            $(".formModal").draggable();
            $(".formModal").resizable();

            resizeCategorys();
            //editorText("inputDescription");
        }).catch(console.warn());
    }).catch(console.warn());

}

export const loadFormEditNode = (vars) => {
    //console.log('loadFormNewNode', vars);
    let item = vars.item;
    let parent = vars.parent;
    let child = parseFloat(vars.child);

    getCategorys().then((e) => {
        //console.log('loadFormNewNode', item, parent, child, order);
        let cat = getNode(item);
        console.log('loadFormEditNode', _CATEGORYS, cat, cat.parentId);
        loadView(_PATH_WEB_NUCLEO + "modules/websites/views/formCategory.html?" + _VS).then((response) => {
            console.log('formCategorys', response);
            let html = response;
            let imgId = cat.img;
            let bannerId = cat.banner;
            let form = "formEditNode";

            html = html.replace(/\{{_LABEL}}/g, "Editar categoria");
            html = html.replace(/\{{_FORM_ID}}/g, "formEditNode");
            html = html.replace(/\{{_BTN_ACTION}}/g, "btnUpdateNode");
            html = html.replace(/\{{_BTN_NAME_ACTION}}/g, "Actualizar");
            html = html.replace(/\{{_RELATED}}/g, "");
            html = html.replace(/\{{_LIST_RELATED}}/g, listElemSelectables({
                data: _CATEGORYS,
                btn: "btnItemSelect"
            }));
            html = html.replace(/\{{_LIST_RELATED_PATHURL}}/g, listElemSelectables({
                data: _CATEGORYS,
                btn: "btnItemSelectPathUrl"
            }));
            html = html.replace(/\{{_PARENTS}}/g, optionsCategorys({
                options: _CATEGORYS,
                initialLabel: "Raiz",
                idSelected: cat.parentId
            }));

            $(".bodyModule[module='" + module + "']").append(replaceEssentials({
                str: html,
                module,
                system,
                pathurl,
                color: dataModule(module, "color"),
            }));

            $("#" + form + " #inputId").val(cat.id);
            $("#" + form + " #inputNameNode").val(cat.name);
            $("#" + form + " input[data-for='inputNameNode']").attr("title", cat.name);

            let inputPath = cat.path;
            let related = cat.related;
            if (related != "" && related != null) {
                let arrayRelated = cat.related.split(",");
                for (let i = 0; i < arrayRelated.length; i++) {
                    const ele = arrayRelated[i];
                    const name = getNode(ele).name;
                    const id = 'inputRelated'
                    $(".boxInputContent[for='" + id + "']").append(
                        renderItemInputContent({
                            id,
                            value: ele,
                            name
                        })
                    );
                }
            }
            let inputPathurl = cat.pathurl;
            let valPathUrl = inputPathurl.replaceAll(inputPath, "");
            $("#" + form + " #inputPath").val(inputPath);
            $("#" + form + " #inputPathurl").val(valPathUrl);
            $("#" + form + " .formPathurl[for='inputPath']").val(inputPath);
            $("#" + form + " #inputDescription").val(cat.description);
            $("#" + form + " #inputOrder").val(cat.order);
            //$("#"+form+" #inputParent option:eq("+cat.parent+")").prop('selected', true);
            $("#" + form + " #inputRelated").val(cat.related);
            $("#" + form + " #inputIcon").val(cat.icon);
            $("#" + form + " #inputColor").val(cat.color);
            $("#" + form + " #inputCls").val(cat.cls);
            $("#" + form + " #inputJson").val(cat.json);
            $("#" + form + " #inputConfigPath").val(cat.configpath);
            $("#" + form + " #inputState option:eq(" + cat.state + ")").prop('selected', true);

            $("#inputNameNode").focus();
            $(".formModal").draggable();
            $(".formModal").resizable();

            if (imgId != 0) {
                //let imgId = img.id;
                $("#" + form + " #inputImage").val(imgId.id);
                $("#" + form + " #previewImg").html(renderItemImage({
                    item: imgId.id,
                    id: "inputTypeImgFile",
                    pathFile: _PATH_FILES + imgId.pathurl,
                }));

                $(".formUpFile .inner[for='inputTypeImgFile']").addClass("off");
            }

            /* loadSingleFileFormLoader({
                id: "inputImageCharge",
                file: imgId,
            }); */
            loadSingleFileFormLoader({
                id: "inputBannerCharge",
                file: bannerId,
            });

            resizeCategorys();
            //editorText("inputDescription");
        }).catch(console.warn());
    }).catch(console.warn());

}

export const saveNewNode = (form) => {
    //console.log('saveNewNode', form);
    const name = $("#inputNameNode").val();
    const path = $("#inputPath").val();
    const pathurl = $("#inputPathurl").val();
    let lastCaracterPathUrl = pathurl.substr(pathurl.length - 1);
    let cont = 0;
    //console.log("pathurl",pathurl);
    if (name == "") {
        errorInput("inputNameNode", "Debe ingresar un nombre");
        cont++;
    }
    if (path == "") {
        errorInput("inputPath", "Debe ingresar un path");
        cont++;
    }
    /* if (pathurl == "") {
        errorInput("inputPathurl", "Debe ingresar un pathurl");
        cont++;
    } else {
        if (lastCaracterPathUrl != "/") {
            $("#inputPathurl").addClass("error");
            $(".messagePathurl").html("<div class='messageAlert'>El pathurl debe terminar con una barra /</div>");
            cont++;
        }
    } */

    //console.log("root", validatePathUrlNode(pathurl));
    if (validatePathUrlNode(pathurl) == false) {
        errorInput("inputPathurl", "Debe ingresar una ruta valida");
        cont++;
    }

    let pathUrlFinal = pathurl + path;

    if (cont == 0) {
        insertNode({
            inputName: $("#" + form + " #inputNameNode").val(),
            inputPath: $("#" + form + " #inputPath").val(),
            inputPathurl: pathUrlFinal,
            inputDescription: $("#" + form + " #inputDescription").val(),
            inputOrder: $("#" + form + " #inputOrder").val(),
            inputParentId: $("#" + form + " #inputParent").val(),
            inputRelated: $("#" + form + " #inputRelated").val(),
            inputIcon: $("#" + form + " #inputIcon").val(),
            inputColor: $("#" + form + " #inputColor").val(),
            inputImage: $("#" + form + " #inputImage").val(),
            inputBanner: $("#" + form + " #inputBanner").val(),
            inputJson: $("#" + form + " #inputJson").val(),
            inputConfigPath: $("#" + form + " #inputConfigPath").val(),
            inputCls: $("#" + form + " #inputCls").val(),
            inputState: $("#" + form + " #inputState").val(),
        }).then((response) => {
            console.log('insertNode', response);
            if (response.status == "success") {
                $(".formModal").remove();
                categorysIndex();
            } else {
                if (response.message == "path_exist") {
                    errorFormInput({
                        id: "inputPath",
                        messageId: "messagePath",
                        message: "<div class='messageAlert'>El path ingresado ya existe</div>"
                    });
                } else {
                    alertMessageError({
                        message: response.message,
                    });
                }
            }
        }).catch(console.warn());
    }
}

export const saveEditNode = (form) => {
    console.log('saveEditNode', form);
    const name = $("#inputNameNode").val();
    const path = $("#inputPath").val();
    const pathurl = $("#inputPathurl").val();
    let lastCaracterPathUrl = "";

    if (pathurl != "" && pathurl != null) {
        lastCaracterPathUrl = pathurl.charAt(pathurl.length - 1);
    }
    let cont = 0;
    //console.log("pathurl",pathurl);
    if (name == "") {
        errorInput("inputNameNode", "Debe ingresar un nombre");
        cont++;
    }
    if (path == "") {
        errorInput("inputPath", "Debe ingresar un path");
        cont++;
    }
   /*  if (pathurl == "") {
        errorInput("inputPathurl", "Debe ingresar un pathurl");
        cont++;
    } else {
        if (lastCaracterPathUrl != "/") {
            $("#inputPathurl").addClass("error");
            $(".messagePathurl").html("<div class='messageAlert'>El pathurl debe terminar con una barra /</div>");
            cont++;
        }
    } */

    //console.log("root", validatePathUrlNode(pathurl));
    if (validatePathUrlNode(pathurl) == false) {
        errorInput("inputPathurl", "Debe ingresar una ruta valida");
        cont++;
    }

    let pathUrlFinal = pathurl + path;

    if (cont == 0) {
        updateNode({
            inputId: $("#" + form + " #inputId").val(),
            inputName: $("#" + form + " #inputNameNode").val(),
            inputPath: $("#" + form + " #inputPath").val(),
            inputPathurl: pathUrlFinal,
            inputDescription: $("#" + form + " #inputDescription").val(),
            inputOrder: $("#" + form + " #inputOrder").val(),
            inputParentId: $("#" + form + " #inputParent").val(),
            inputRelated: $("#" + form + " #inputRelated").val(),
            inputIcon: $("#" + form + " #inputIcon").val(),
            inputColor: $("#" + form + " #inputColor").val(),
            inputImage: $("#" + form + " #inputImage").val(),
            inputBanner: $("#" + form + " #inputBanner").val(),
            inputJson: $("#" + form + " #inputJson").val(),
            inputConfigPath: $("#" + form + " #inputConfigPath").val(),
            inputCls: $("#" + form + " #inputCls").val(),
            inputState: $("#" + form + " #inputState").val(),
        }).then((response) => {
            console.log('updateNode', response);
            if (response.status == "success") {
                $(".formModal").remove();
                categorysIndex();
            } else {
                alertMessageError({
                    message: response.message,
                });
            }
        }).catch(console.warn());
    }
}

export const resizeCategorys = (vars) => {

    let w = $(window).width();
    let h = $(window).height();
    //console.log('resizeCategorys', w, h);
    let hBody = $(".bodyModule[module='" + module + "']").outerHeight();
    let hFormModal = hBody - 150;
    let hFormModalForm = hFormModal - 94;

    if (w > 474) {
        $(".formModal").css("height", hFormModal);
        $(".formModal .form").css("height", hFormModalForm);
    }
}

export function getNode(item) {
    let categorys = _CATEGORYS;
    for (let i = 0; i < categorys.length; i++) {
        const id = categorys[i].id;
        const children = categorys[i].children;
        let returnNode = 0;
        if (item == id) {
            return categorys[i];
        }
        if (children != 0) {
            returnNode = getNodeChildren(categorys[i].children, item);
            if (returnNode != 0) {
                return returnNode;
            }
        }
    }
    return 0;
}

export function getNodeChildren(categorys, item) {
    for (let i = 0; i < categorys.length; i++) {
        const id = categorys[i].id;
        const children = categorys[i].children;
        let returnNode = 0;

        if (item == id) {
            return categorys[i];
        }
        if (children != 0) {
            returnNode = getNodeChildren(categorys[i].children, item);
            if (returnNode != 0) {
                return returnNode;
            }
        }
    }
    return 0;
}

export const removeAllAddNode = () => {
    $(".add").html("");
    $(".btnNode").attr("state-add", "0");

}



// fn async axios 

export const insertNode = async (vars) => {
    console.log('insertNode', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/categorys.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "insertNode",
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("Error: insertNode")
        console.log(error)
    }
}

export const updateNode = async (vars) => {
    console.log('updateNode', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/categorys.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "updateNode",
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("Error: insertNode")
        console.log(error)
    }
}

export const changeStateNode = async (vars) => {
    console.log('changeStateNode', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/categorys.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "changeStateNode",
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("Error: changeStateNode")
        console.log(error)
    }
}

export const reorderCategorys = (node = 0) => {
    console.log('reorderCategorys', node, _CATEGORYS);
    let nameNode = node != 0 ? _CATEGORYS[node].name : "Raiz";
    let body = "";

    if (node != 0) {
        let index = node - 1;
        _CATEGORYS[index].children.forEach(element => {
            body += `<div class="itemSortable ui-state-default" data-id="${element.id}">
                <i class="icon icon-gril-compac"></i>
                <span>${element.name}</span>
            </div>`;
        });
    } else {
        _CATEGORYS.forEach(element => {
            body += `<div class="itemSortable ui-state-default" data-id="${element.id}">
            <i class="icon icon-gril-compac"></i>
            <span>${element.name}</span>
        </div>`;
        });
    }

    addHtml({
        selector: `body`,
        type: 'append', //insert, append, prepend, replace
        content: renderModalClean({
            id: "modalOrderCategorys",
            title: "Reordenar " + nameNode,
            btns: `<a class="btn btnPrimary btnMini" id="btnOrderCategorys" data-node="${node}"><i class="icon icon-refresh"></i><span>Actualizar</span></a>`,
            clsHeader: 'blue-sky',
            body
        })
    })

    $("#modalOrderCategorys .modalBody").sortable();
}

export const deleteNode = async (vars) => {
    console.log('deleteNode', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/categorys.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "deleteNode",
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("Error: deleteNode")
        console.log(error)
    }
}

export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/categorys.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        //console.log("Error: getDataPlaces")
        //console.log(error)
    }
}

document.addEventListener("DOMContentLoaded", function () {

    var TimeFn = null;

    $("body").on("dblclick", ".bodyModule[module='" + module + "'] .btnNode", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        clearTimeout(TimeFn)
        removeAllAddNode();
        console.log('.btnEditNode');
        const item = $(this).attr("item");
        const parent = $(this).attr("parent");
        const child = $(this).attr("child");

        loadFormEditNode({
            item,
            parent,
            child
        });
    })

    $("body").on("click", ".bodyModule[module='" + module + "']  .btnNode", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        clearTimeout(TimeFn)

        const item = e.target.getAttribute("item");
        const state_add = e.target.getAttribute("state-add");
        const state = e.target.getAttribute("state");
        const child = e.target.getAttribute("child");


        TimeFn = setTimeout(function () {
            //console.log("btnNode", item);
            removeAllAddNode();



            if (state_add == "0") {
                renderTaskNodes({
                    item,
                    child
                });
                $(".add[item='" + item + "'] .btnStateNode").attr("state", state);
                e.target.setAttribute("state-add", "1");

            } else {
                $(".add[item='" + item + "']").html("");


                e.target.setAttribute("state-add", "0");

            }
        }, 300);

    });

    $("body").on("click", ".bodyModule[module='" + module + "']  .btnStateNode", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        const item = $(this).attr("item");
        let state = $(this).attr("state");

        //console.log("btnStateNode", item, state);

        if (state == "0") {
            state = 1;

        } else {
            state = 0;
        }

        $(this).attr("state", state);
        $(".btnNode[item='" + item + "']").attr("state", state);


        changeStateNode({
            item,
            state
        }).then((response) => {
            //console.log('changeStateNode', response);

            if (response.status == "success") {
                alertPage({
                    text: "Cambio de estado exitoso. ",
                    icon: "icn icon-alert-success",
                    animation_in: "bounceInRight",
                    animation_out: "bounceOutRight",
                    type: "success",
                    time: "2500",
                    position: "bottom-left",
                })

            } else {
                alertMessageError(response.message);
            }

        }).catch(console.warn());

    });

    $("body").on("click", ".bodyModule[module='" + module + "']  .btnAddNode", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        const item = $(this).parent().attr("item");
        const parent = $(this).parent().attr("parent");
        const child = $(this).parent().attr("child");
        removeAllAddNode();


        loadFormNewNode({
            item,
            parent,
            child
        });

    })

    $("body").on("click", ".bodyModule[module='" + module + "'] .btnEditNode", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        console.log('.btnEditNode');
        const item = $(this).parent().attr("item");
        const parent = $(this).parent().attr("parent");
        const child = $(this).parent().attr("child");
        removeAllAddNode();

        loadFormEditNode({
            item,
            parent,
            child
        });
    })

    $("body").on("click", ".btnUpdateNode", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        console.log('.btnUpdateNode');
        let form = $(this).attr("form");
        removeAllAddNode();
        saveEditNode(form);
    })

    $("body").on("click", ".bodyModule[module='" + module + "'] .btnSaveNewNode", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        //console.log('.btnSaveNewNode');
        let form = $(this).attr("form");
        removeAllAddNode();
        saveNewNode(form);
    })

    $("body").on("click", ".bodyModule[module='" + module + "'] .btnDeleteNode", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        removeAllAddNode();

        let item = $(this).attr("item");
        let cat = getNode(item);
        console.log('.btnDeleteNode', item, cat);
        if (cat !== undefined && cat !== null && cat !== 0) {
            let name = cat.name;
            let parentId = cat.parentId;
            let children = cat.children;
            let attr = `item="${item}" parent="${parentId}" children="${children == 0 ? 0 : children.length}"`;

            let title = /*html*/ `
                    <label lang="es">Estas seguro de eliminar <b>${cat.name}</b>? <br>Esta acción será permanete. </label> 
                    <form class="form" id="formDeleteNode">
                        <div class="formControl formCheckbox">
                            <div class="formCheckboxItem">
                                <input class="formInput" type="checkbox" checked  name="inputNodes" id="inputNodes" value="1">
                                <label class="formCheckLabel" for="inputNodes" lang="es">Borrar también Hijos (${children.length})</label>
                            </div>
                        </div>
                    </form>
                    `;
            if (children == 0) {
                title = "";
            }
            $(".bodyModule[module='" + module + "']").prepend(
                renderDeleteModal({
                    title,
                    attr,
                    name,
                    module,
                    classBtnAction: "btnDeleteNodeConfirm"
                })
            );
        }
    });

    $("body").on("click", ".bodyModule[module='" + module + "'] .btnDeleteNodeConfirm", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        let item = $(this).attr("item");
        let parentId = $(this).attr("parent");
        let children = $(this).attr("children");
        let inputNodes = $("#formDeleteNode #inputNodes").val();
        let parentArray = getNode(parentId);
        let numParentChildren = 0;
        //console.log(_CATEGORYS);
        console.log('btnDeleteNodeConfirm', item, parentId, inputNodes);

        if (children == 0) {
            inputNodes = 0;
        } else {
            if (parentId != 0) {
                numParentChildren = parentArray.children.length;
            } else {
                numParentChildren = _CATEGORYS.length;
            }
        }


        deleteNode({
            item,
            parentId,
            numParentChildren,
            inputNodes
        }).then((response) => {
            console.log('deleteNode', response);
            if (response.status == "success") {
                $(".modalDeleteNode").remove();
                categorysIndex();
            } else {
                alertMessageError(response.message);
            }
        }).catch(console.warn());
    })


    $("body").on("click", ".bodyModule[module='" + module + "'] .btnWorksheetsNode", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        //console.log('.btnWorksheetsNode');
        let item = $(this).attr("item");
        let name = $(".btnNode[item='" + item + "']").text();
        loadWorksheetsNode({
            item,
            name,
            module
        });
    });

    $("body").on("click", `.btnTrashImg`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let input = $(this).parents(".formChargeImage").attr("for");
        console.log(`.btnTrashImg`, data, input);
        addHtml({
            selector: `body`,
            type: 'append', // insert, append, prepend, replace
            content: renderDeleteOrRemove({
                name: data.name,
                id: "modalDelete",
                attr: `data-id="${data.id}" data-input="${input}"`,
            })
        });
    });

    $("body").on("click", `.btnDeleteAction`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnDeleteAction`, data);

        getData({
            task: 'deleteMedia',
            return: 'returnState',
            ids: [data.id],
        }).then((response) => {
            console.log('getData', response);
            if (response.Error == 0) {
                $(".modalConfirm").remove();
                loadChargeImageInit(data.input);
            } else {
                alertMessageError({
                    message: response.message,
                })
            }
        }).catch(console.warn());

    });

    $("body").on("click", `#nodesOrderCategorys`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`#nodesOrderCategorys`, data);
        reorderCategorys(data.node);
    });

    $("body").on("click", `#btnOrderCategorys`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`#btnOrderCategorys`, data);

        // Crea un array para almacenar los valores de data-id
        var arrayDataId = [];

        var arrayDataId = $('#modalOrderCategorys .modalBody .itemSortable').map(function () {
            // Utiliza la función data() de jQuery para obtener el valor de 'data-id'
            return $(this).data('id');
        }).get();

        // Muestra el array en la consola
        console.log(arrayDataId);

        getData({
            task: 'reorderCategorys',
            return: 'returnState', // returnId, returnState, returnArray, returnMessage, returnObject
            input: {
                node: data.node,
                categorys: arrayDataId
            }
        }).then((response) => {
            console.log('getData reorderCategorys', response);
            if (response.status == 'success') {
                removeModalId("modalOrderCategorys");
                categorysIndex();
            } else {
                alertMessageError({
                    message: response.message
                })
            }
        }).catch(console.warn());

    });

    $("body").on("click",`.btnShortNode`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnShortNode`, data);
        
        reorderCategorys(data.node);
        
    });

})