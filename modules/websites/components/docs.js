import { 
    loadView,
    accessToken,
    alertMessageError,
    dataModule,
    addHtml,
    capitalize,
    jointActions
} from '../../components/functions.js';

import {
    editorText,
    errorInput,
    errorInputSelector,
    loadInitChargeMedia,
    loadItemMedia,
    loadFilesFormLoader,
    loadSingleFileFormLoader,
    loadChargeMediaInit,
    removalMediaItems,
    ckeckboxChecked,
    stateBtnSwitch
} from "../../components/forms.js";

import {
    innerForm,
    renderModalClean,
    removeInnerForm,
    renderDeleteModalFn
} from "../../components/renders/renderModals.js";

import {
    getCategorys,
    listCategorys,
    checkCategorys
} from "./categorys.js";

import {
    createTable,
    dataTable,
    removeItemTable,
} from "../../components/tables.js";

import {
    renderNoDataTable
} from "../../components/renders/renderTables.js";

import {
    renderCheckBox,
    renderPill,
    renderItemVideo,
    renderItemImage
} from "../../components/renders/renderForms.js";

import {
    renderTr
} from './renders/renderDocs.js';


let module = 'docs';
let system = 'websites';
let formId = 'docsForm';
let tableId = 'docsTable';

export const docsIndex = (vars =[]) => {
    console.log('docsIndex',vars);
    loadView(_PATH_WEB_NUCLEO + "modules/websites/views/docs.html?" + _VS).then((responseView) => {
        //console.log('loadView', responseView);
        let content = responseView;
 
        addHtml({
            selector:`.bodyModule[module="${module}"]`,
            type: 'insert', //insert, append, prepend, replace
            content 
        });

        getData({
            task: 'getDataDocs',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject     
        }).then((response) => {
            console.log('getData getDataDocs', response);
            if(response.status=='success'){

                if (response.status == "success") {
                    if (response != null && response.Error == 0) {
                        const data = response.data.getDocs;
                        let strTable = "";
                        let tbody = "";
                        //console.log(data);

                        for (let i = 0; i < data.length; i++) {
                            const elem = data[i];
                            elem["table"] = tableId;
                            elem["module"] = module;
                            elem["system"] = system;
                            elem["path"] = capitalize(module);
                            tbody += renderTr(elem);
                        }

                        strTable = createTable({
                            id: tableId,
                            thead: ":check,id:colId,Titulo,Fecha,Usuario,Estado:colState,Acciones:colActions",
                            body: tbody,
                        });

                        $(
                            ".bodyModule[module='" + module + "'] >.tbody"
                        ).html(strTable);

                        dataTable({
                            elem: "#" + tableId,
                            orderCol: 1,
                        });
                    } else if (response.data == 0) {
                        $(
                            ".bodyModule[module='" + module + "'] >.tbody"
                        ).html(renderNoDataTable());
                    }
                } else {
                    alertMessageError({
                        message: response.message,
                    });
                }
                
            }else{
                 alertMessageError({message: response.message})
            }
        }).catch(console.warn());

    }).catch(console.warn());
}

export const newDocForm = (vars =[]) => {
    loadView(_PATH_WEB_NUCLEO + "modules/websites/views/formDocs.html?" + _VS).then((responseView) => {
        let str = responseView;
        str = str.replace(/{{_TITLE}}/g, "Nuevo Documento");
        str = str.replace(/{{_FORM_ID}}/g,formId);
        str = str.replace(/{{_MODULE}}/g,module);
        str = str.replace(/{{_SYSTEM}}/g,system);
        str = str.replace(/{{_BTN_LABEL}}/g,"Guardar");

        getData({
            task: 'getDataDocs',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
            
        }).then((response) => {
            console.log('getData getDataDocs', response);
            if(response.status=='success'){
                let categorys = response.data.categorys;
                let date = response.data.date;

                str = str.replace(/{{_CATEGORYS}}/g, listCategorys(categorys));
                str = str.replace(/{{_BTN_ACTION}}/g, 'btnAddDoc');

                innerForm({
                    cls: 'mode-compact',
                    module,
                    body: str,
                });

                jQuery.datetimepicker.setLocale(_LOCALE);

                $(`#${formId} #inputDate`).val(date);
                $(`#${formId} #inputDate`).datetimepicker({
                    timepicker: false,
                    datepicker: true,
                    format: "Y-m-d H:i",
                    lang: "es",
                    timeZone: _TIMEZONE
                });
                console.log('innerForm',formId);
            }else{
                 alertMessageError({message: response.message})
            }
        }).catch(console.warn());
    });
}

export const editDocForm = (vars =[]) => {
    let formId = vars.formId;

    loadView(_PATH_WEB_NUCLEO + "modules/websites/views/formDocs.html?" + _VS).then((responseView) => {
        let str = responseView;
        str = str.replace(/{{_TITLE}}/g, "Editar Documento");
        str = str.replace(/{{_FORM_ID}}/g,formId);
        str = str.replace(/{{_MODULE}}/g,module);
        str = str.replace(/{{_SYSTEM}}/g,system);
        str = str.replace(/{{_BTN_LABEL}}/g,"Actualizar");
        

        getData({
            task: 'getDataDoc',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
            inputs: {
                inputId : vars.id
            }
        }).then((response) => {
            console.log('getData getDataDoc', response, response.data.getDoc, formId);
            if(response.status=='success'){
                let categorys = response.data.categorys;
                
                let dataDoc = response.data.getDoc;
                let img = dataDoc.img;   
                let categorysSelect = dataDoc.categorys;  
                let date = dataDoc.date;
                  
                str = str.replace(/{{_CATEGORYS}}/g, listCategorys(categorys));
                str = str.replace(/{{_BTN_ACTION}}/g, 'btnUpdateDoc');

                innerForm({
                    cls: 'mode-compact',
                    module,
                    body: str,
                });

                jQuery.datetimepicker.setLocale(_LOCALE);

                $(`#${formId} #inputDate`).val(date);
                $(`#${formId} #inputDate`).datetimepicker({
                    timepicker: false,
                    datepicker: true,
                    format: "Y-m-d H:i",
                    lang: "es",
                    timeZone: _TIMEZONE,
                    value : date
                });

                $(`#${formId} #inputId`).val(dataDoc.id);
                $(`#${formId} #inputTitle`).val(dataDoc.title);
                $(`#${formId} #inputDescription`).val(dataDoc.inputDescription);
                $(`#${formId} #inputTags`).val(dataDoc.tags);
                $(`#${formId} #boxInputDoc`).html(`<p><b>fileId:</b> [${dataDoc.file.id}] </p> 
                                                    <input type="hidden" name="inputDoc" id="inputDoc" value="${dataDoc.file.id}">
                                                    <p><b>filePath:</b> ${dataDoc.file.pathurl}</p>`);
                $(`#${formId} #inputImage`).val(dataDoc.image);

                checkCategorys(categorysSelect);

                if (img != 0) {
                    let imgId = img.id;
                    $("#" + formId + " #inputImage").val(imgId);
                    $("#" + formId + " #previewImg").html(renderItemImage({
                        item: imgId,
                        id: "inputTypeImgFile",
                        pathFile: _PATH_FILES + img.pathurl,
                    }));

                    $(`#${formId} .formUpFile .inner[for='inputTypeImgFile']`).addClass("off");
                }

                console.log('innerForm',formId);
            }else{
                 alertMessageError({message: response.message})
            }
        }).catch(console.warn());
    });
}

export const addDoc = (vars =[]) => {
    console.log('addNewDoc',vars);
    let form = vars.form;
    /* let data = form.serializeArray();
    let dataObj = {};

    data.forEach((item) => {
        dataObj[item.name] = item.value;
    }); */
    let count = 0;
    let inputTitle = $(`#${form} #inputTitle`).val();
    let inputDoc = $(`#${form} #inputDoc`).val();
    let inputDescription = $(`#${form} #inputDescription`).val();
    let inputTags = $(`#${form} #inputTags`).val();
    let inputDate = $(`#${form} #inputDate`).val();
    let inputImage = $(`#${form} #inputImage`).val();

    if (inputTitle == "" || inputTitle == null) {
        errorInput("inputTitle", "Debe ingresar un titulo");
        count++;
    }

    if (inputDoc == "" || inputDoc == null) {
        errorInput("inputDoc", "Debe ingresar un documento");
        count++;
    }

    if (count == 0) {

        let categorys = [];
        let countCategorys = 0;
        $("#" + form + " input[name='inputCategorys[]']").each(function (e) {
            let valorCategorys = $(this).val();
            let checkCategorys = $(this).prop("checked");
            if (checkCategorys) {
                categorys[countCategorys] = valorCategorys;
                countCategorys++;
            }
        });

        getData({
            task: 'addDoc',
            return: 'returnId', // returnId, returnState, returnArray, returnMessage, returnObject
            inputs:{
                inputTitle,
                inputDoc,
                inputDescription,
                inputTags,
                inputDate,
                inputImage,
                categorys: categorys
            }
        }).then((response) => {
            console.log('getData addNewDoc', response);
            if(response.status=='success'){
                removeInnerForm();
                docsIndex();
            }else{
                 alertMessageError({message: response.message})
            }
        }).catch(console.warn());
    }
}

export const updateDocForm = (vars =[]) => {
    console.log('updateDocForm',vars);
    let form = formId;
    let count = 0;
    let inputId = $(`#${form} #inputId`).val();
    let inputTitle = $(`#${form} #inputTitle`).val();
    let inputDoc = $(`#${form} #inputDoc`).val();
    let inputDescription = $(`#${form} #inputDescription`).val();
    let inputTags = $(`#${form} #inputTags`).val();
    let inputDate = $(`#${form} #inputDate`).val();
    let inputImage = $(`#${form} #inputImage`).val();

    if (inputTitle == "" || inputTitle == null) {
        errorInput("inputTitle", "Debe ingresar un titulo");
        count++;
    }

    if (inputDoc == "" || inputDoc == null) {
        errorInput("inputDoc", "Debe ingresar un documento");
        count++;
    }

    if (count == 0) {
        let categorys = [];
        let countCategorys = 0;
        $("#" + form + " input[name='inputCategorys[]']").each(function (e) {
            let valorCategorys = $(this).val();
            let checkCategorys = $(this).prop("checked");
            if (checkCategorys) {
                categorys[countCategorys] = valorCategorys;
                countCategorys++;
            }
        });

        //console.log('updateDocForm',inputTitle, inputDoc, inputDescription, inputDate, inputImage, categorys);

        getData({
            task: 'updateDoc',
            return: 'returnState', // returnId, returnState, returnArray, returnMessage, returnObject
            inputs:{
                inputId,
                inputTitle,
                inputDoc,
                inputDescription,
                inputTags,
                inputDate,
                inputImage,
                categorys
            }
        }).then((response) => {
            console.log('getData updateDoc', response);
            if(response.status=='success'){
                removeInnerForm();
                docsIndex();
            }else{
                 alertMessageError({message: response.message})
            }
        }).catch(console.warn());
    }

}

export const deleteItemDocModal = (vars = []) => {
    console.log('deleteItemDocModal', vars);
    addHtml({
        selector: `body`,
        type: "append", // insert, append, prepend, replace
        content: renderDeleteModalFn({
            name: vars.name,
            id: "modalDelete",
            module,
            attr: `data-module="${module}" data-id="${vars.id}"`,
        }),
    });
}

export const deleteItemDoc = (vars = []) => {
    console.log('deleteItemDoc', vars);

    getData({
        task: 'deleteItemDoc',
        return: 'returnState', // returnId, returnState, returnArray, returnMessage, returnObject
        item: vars.id
    }).then((response) => {
        console.log('getData deleteItemDoc', response);
      
        if (response.status == "success") {
            if (response != null && response.Error == 0) {
                removeItemTable(vars.id,tableId);
                $("#modalDelete").remove();
            } else if (response.data == 0) {
                alertMessageError({
                    message: "No se pudo eliminar el registro",
                });
            }
        } else {
            alertMessageError({
                message: response.message,
            });
        }
         
    }).catch(console.warn());

}

export const getData = async (vars = []) => {
   //console.log('getData', vars);
   const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/docs.php";
   let data = JSON.stringify({
       accessToken: accessToken(),
       vars: JSON.stringify(vars)
   });
   try {
       let res = await axios({
           async: true,
           method: "post",
           responseType: "json",
           url: url,
           headers: {},
           data: data
        })
       //console.log(res.data);
       jointActions(res.data)
       return res.data
   } catch (error) {
      console.log("Error: getData ", error);
   }
}

document.addEventListener('DOMContentLoaded', function(e) {
    //console.log('docs.js DOMContentLoaded');
    $("body").on("click",`#btnNewDoc`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`#btnNewDoc`,data);
        newDocForm();
    });

    $("body").on("click",`#btnAddDoc`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`addNewDoc`,data);
        addDoc(data);
    });


    $("body").on("click",`.btnDeleteFormDocs`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnDeleteFormDocs`,data);
        deleteItemDocModal(data);
    });

    $("body").on("click",`.btnDeleteConfirmationDocs`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnDeleteConfirmationDocs`,data);
        deleteItemDoc(data);
    });

    $("body").on("click",`.btnEditFormDocs`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnEditFormDocs`,data);
        data.formId = formId;
        editDocForm(data);
    });

    $("body").on("click",`#btnUpdateDoc`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`#btnUpdateDoc`,data);
        updateDocForm(data);
    });


});