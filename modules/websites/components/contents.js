import {
	empty,
	replacePath,
	replaceEssentials,
	replaceAll,
	loadView,
	stopLoadingBar,
	emptyReturn,
	loadingBtnIcon,
	btnLoadingRemove,
	unDisableId,
	accessToken,
	removeBtnLoading,
	alertMessageError,
	dataModule,
	addHtml,
	capitalize,
	alertPage,
	jointActions
} from "../../components/functions.js";

import {
	listCategorys,
	checkCategorys
} from "./categorys.js";

import {
	editorText,
	errorInput,
	loadInitChargeMedia,
	loadItemMedia,
	loadFilesFormLoader,
	loadSingleFileFormLoader,
	loadChargeMediaInit,
	removalMediaItems,
	ckeckboxChecked
} from "../../components/forms.js";

import {
	deleteModalItem,
	removeModal,
	closeModal,
} from "../../components/modals.js";
import {
	categorysListTable,
	createTable,
	dataTable,
	removeItemTable,
	inactiveBtnStateItem,
	activeBtnStateItem,
	mountTable,
	colCheck,
	colId,
	colTitle,
	colState,
	colActionsBase,
	deleteItemModule,
	renderEmpty
} from "../../components/tables.js";
import {
	renderCol,
	renderColCheck,
	renderColId,
	renderColTitle,
	renderRowsTable,
	renderColState,
	renderColActions,
	renderColCategorys,
} from "../../components/renders/renderTables.js";
import {
	innerForm,
	renderDeleteModal
} from "../../components/renders/renderModals.js";

import {
	renderCheckBox
} from "../../components/renders/renderForms.js";

import {
	getCategorys
} from "./categorys.js";

import {
	getData as getPublications
} from "./publications.js";

/* renderColTitle,
	renderColState,
	renderColActionsBase, */
let pathurl = "modules/websites/";
var module = "contents";
let system = "websites";
let tableId = "tableContents";
export const contentsIndex = () => {
	//console.log("contentsIndex");
	loadView(_PATH_WEB_NUCLEO + pathurl + "views/contents.html?" + _VS).then(
		(htmlView) => {
			// //console.log(htmlView, module, system);
			loadContents()
				.then((response) => {
					//console.log("loadContents", response);
					stopLoadingBar();

					$(".innerForm").remove();

					$(".bodyModule[module='" + module + "']").html(
						replaceEssentials({
							str: htmlView,
							module,
							system,
							pathurl,
							fn: "formNewContent",
							color: dataModule(module, "color"),
						})
					);

					if (response.Error == 0 && response.items != 0) {

						let rows = "";
						for (let i in response.items) {
							let item = response.items[i];
							//console.log("item", item);
							rows += renderRowsTable({
								id: item.id,
								content: renderColCheck({
									id: item.id,
								}) +
									renderColId({
										id: item.id,
									}) +
									renderColTitle({
										id: item.id,
										title: item.title,
									}) +
									renderColCategorys({
										id: item.id,
										fn: "categorysListTable",
										array: item.categorys,
									}) +
									renderColCategorys({
										id: item.id,
										cls: "pubs",
										fn: "pubsListTable",
										array: item.pubs,
									}) +
									renderColState({
										id: item.id,
										state: item.state,
										module,
										system,
									}) +
									renderColActions({
										id: item.id,
										name: item.title,
										type: "btnEdit,btnDelete",
										fnType: "formEditContent,btnDelete",
										module,
										system,
									}),
							});
						}

						mountTable({
							id: tableId,
							columns: [
								...colCheck,
								...colId,
								...colTitle,
								{
									label: "Categorias",
									cls: "colCategorys",
								},
								{
									label: "Publicaciones",
									cls: "colCategorys",
								},
								...colState,
								...colActionsBase,
							],
							rows,
							module,
							system,
							container: ".bodyModule[module='" + module + "'] .tbody",
						});

					} else if (response === 0 || response.items == 0) {
						$(".bodyModule[module='" + module + "']  .tbody").html(renderEmpty())
					} else {
						alertMessageError({message: response.message})
					}
				})
				.catch(console.warn());
		}
	);
};

export const returnIndex = (vars = []) => {
	//console.log('returnIndex',vars);
	$("#innerForm .inner").html("");
	$("#innerForm").removeClass("on");
	contentsIndex();
};

export const formNewContent = () => {
	getCategorys().then((e) => {
		loadView(
			_PATH_WEB_NUCLEO + "modules/websites/views/formContent.html?" + _VS
		)
			.then((response) => {
				//console.log('loadView', response);

				let str = response;
				str = str.replace(/{{_MODULE}}/g, module);
				str = str.replace(/{{_SYSTEM}}/g, system);
				str = str.replace(/{{_FORM_ID}}/g, "newFormContent");
				str = str.replace(/{{_ITEM}}/g, "");
				str = str.replace(/{{_BTNS_TOP}}/g, "");
				str = str.replace(/{{_BTN_ACTION}}/g, "btnNewContent");
				str = str.replace(/{{_CATEGORYS}}/g, listCategorys(_CATEGORYS));
				str = str.replace(
					/{{_CLS_BTN_ACTION}}/g,
					"btnNewContent btnInfo"
				);
				str = str.replace(/{{_BTN_NAME_ACTION}}/g, "Guardar");
				str = str.replace(/{{_TITLE}}/g, "Nuevo Contenido");
				innerForm({
					module,
					body: str,
				});

				editorText("inputSummary");
				editorText("inputBody");
			})
			.catch(console.warn());
	});
};

export const formEditContent = (id) => {
	//console.log("formEditContent", id);
	getPublications({
		task: 'dataPublications',
		return: 'returnArray'
	}).then((responsePubs) => {
		//console.log('getPublications', responsePubs);
		getCategorys().then((e) => {
			loadView(
				_PATH_WEB_NUCLEO + "modules/websites/views/formContent.html?" + _VS
			).then((htmlView) => {
				loadDataContent({
					id,
				}).then((response) => {
					console.log("loadDataContent", response);
					let str = htmlView;
					let formId = 'editFormContent';
					let files = response.data.media;
					let img = response.data.img;
					let btnsTop = `<a class="btn btnFull btnUpdateItem" 
									id="updateItem" 
									item = "${response.data.id}"
									form = "${formId}" >
									<i class=""></i>
									<span>Actualizar</span></a>`
					//console.log(files);

					str = str.replace(/{{_MODULE}}/g, module);
					str = str.replace(/{{_SYSTEM}}/g, system);
					str = str.replace(/{{_FORM_ID}}/g, formId);
					str = str.replace(/{{_ITEM}}/g, response.data.id);
					str = str.replace(/{{_BTN_ACTION}}/g, "btnUpdateContent");
					str = str.replace(/{{_BTNS_TOP}}/g, btnsTop);
					str = str.replace(/{{_CATEGORYS}}/g, listCategorys(_CATEGORYS));
					str = str.replace(/{{_PUBLICATIONS}}/g, renderCheckBox({
						array: responsePubs.data,
						id: 'inputPublications',
					}));
					str = str.replace(
						/{{_CLS_BTN_ACTION}}/g,
						"btnUpdateContent btnInfo"
					);
					str = str.replace(/{{_BTN_NAME_ACTION}}/g, "Guardar");
					str = str.replace(/{{_TITLE}}/g, "Editar Contenido");

					innerForm({
						module,
						body: str,
					});

					$("#inputId").val(response.data.id);
					$("#inputTitle").val(response.data.title);
					$("#inputSummary").val(response.data.summary);
					$("#inputState").val(response.data.state);
					$("#inputBody").val(response.data.body);
					$("#inputTags").val(response.data.tags);
					$("#inputBtnTitle").val(response.data.btnTitle);
					$("#inputClass").val(response.data.cls);
					$("#inputJson").val(response.data.json);
					$("#inputIcon").val(response.data.icon);
					$("#inputUrl").val(response.data.url);
					$("#inputAuthor").val(response.data.author);
					$("#inputNotitle").val(response.data.notitle);
					$("#inputTarget").val(response.data.target);
					$("#inputPathurl").val(response.data.pathurl);

					editorText("inputBody");
					editorText("inputSummary");

					/* if (files != 0) {
						loadInitChargeMedia('inputFilesMedia')
						for (let i = 0; i < files.length; i++) {
							const element = files[i];
							let index = i + 1;
							//console.log(element,index);
							loadItemMedia({ id: 'inputFilesMedia', index: index, img: _PATH_FILES + element.pathurl, name: element.title, item: element.id })
						}
					} */

					loadFilesFormLoader({
						id: "inputFilesMedia",
						files,
						module
					});

					loadSingleFileFormLoader({
						id: "inputImageCharge",
						file: img,
						module
					});

					//console.log("response.data.categorys", response.data.categorys);
					ckeckboxChecked({
						array: response.data.pubs,
						name: "inputPublications[]",
					});
					checkCategorys(response.data.categorys);
				});
			});
		}).catch(console.warn());
	}).catch(console.warn());
};

export const addForm = (form = "") => {
	//console.log("addForm", form);

	let title = $("#" + form + " #inputTitle").val();
	let count = 0;

	if (title == "") {
		errorInput("inputTitle", "Ingrese un titulo");
		count++;
	}

	if (count == 0) {
		addNewContent(form)
			.then((response) => {
				//console.log("addNewContent", response);
				if (response.Error == 0) {
					returnIndex();
				} else {
					alertMessageError({
						message: response.message,
					});
				}
			})
			.catch(console.warn());
	}
};

export const updateForm = (form, type = "save") => {
	//console.log("updateForm", form);
	let title = $("#" + form + " #inputTitle").val();
	let count = 0;

	if (title == "") {
		errorInput("inputTitle", "Ingrese un titulo");
		count++;
	}

	if (type == "update") {
		$(`.btnUpdateItem[form='editFormContent'] span`).html("Actualizando...");
		$(`.btnUpdateItem[form='editFormContent']`).removeClass("btnFull");
		$(`.btnUpdateItem[form='editFormContent']`).addClass("animated infinite flash btnSuccess");
	}

	if (count == 0) {
		updateContent(form)
			.then((response) => {
				//console.log("updateContent", response);
				if (response.Error == 0) {
					if (type == "save") {
						returnIndex();
					}
					if (type == "update") {
						setTimeout(() => {
							$(`.btnUpdateItem[form='editFormContent'] span`).html("Actualizar");
							$(`.btnUpdateItem[form='editFormContent']`).addClass("btnFull");
							$(`.btnUpdateItem[form='editFormContent']`).removeClass("animated infinite flash btnSuccess");
						}, 1000);

					}
				} else {
					alertMessageError({
						message: response.message,
					});
				}
			})
			.catch(console.warn());
	}
};

export const formEditMedia = (vars = []) => {
	//console.log("formEditMedia", vars);
	loadView(
		_PATH_WEB_NUCLEO + "modules/websites/views/formFileRel.html?" + _VS
	)
		.then((response) => {
			//console.log('loadView', response);
			let str = response;
			str = str.replace(/{{_ITEM_CONTENT}}/g, vars.inputId);
			str = str.replace(/{{_ITEM_MEDIA}}/g, vars.item);
			str = str.replace(/{{_FORM}}/g, "formMediaEdit");
			str = str.replace(/{{_BTN_UPDATE}}/g, "btnUpdateMediaEdit");

			loadDataItemMedia(vars)
				.then((responseLoad) => {
					console.log("loadDataItemMedia", responseLoad);
					let data = responseLoad.data;
					//console.log(data);
					$("#modalFormEditMedia .body .col-form").html(str);

					$("#inputTitle").val(data.title);
					$("#inputSummary").val(data.summary);
					$("#inputUrl").val(data.url);
					$("#inputBtnTitle").val(data.btnTitle);
					$("#inputTarget option[value=" + data.target + "]").attr(
						"selected",
						true
					);
					$("#inputIcon").val(data.icon);
					$(".iconSelect").addClass(data.icon);

					editorText("inputSummary");
				})
				.catch(console.warn());
		})
		.catch(console.warn());
};

export const updateFormEditMedia = (form = "") => {
	//console.log('updateFormEditMedi',vars);
	updateItemMedia(form)
		.then((response) => {
			//console.log("updateItemMedia", response);
			if (response.Error == 0) {
				closeModal();
			} else {
				alertMessageError({
					message: response.message,
				});
			}
		})
		.catch(console.warn());
};

export const deleteSelectedItems = (id = null) => {
	//console.log('deleteSelectedItems', id);
	$(".selectActionsTable[for='#" + tableId + "'] option[value='0']").attr("selected", "selected");
	deleteModalItem({
		id,
		name: 'Eliminar items seleccionados',
		attr: '',
		module,
		fn: 'deleteSelectedConfirms'
	});
}

export const deleteSelectedConfirms = () => {
	//console.log('deleteSelectedConfirms');
	let check = new Array();
	$("#" + tableId + " input[name='inputCheck[]']").each(function (i) {
		let auxCheck = $(this).prop("checked");
		let auxValue = $(this).val();
		if (auxCheck) {
			check[i] = auxValue;
		}
	});

	// //console.log(check);
	deleteContents({
		ids: check
	}).then((response) => {
		//console.log('deleteContents', response);
		if (response.Error == 0) {
			$(".modalConfirm").remove();
			contentsIndex();
		} else {
			alertMessageError({
				message: response.message,
			})
		}
	}).catch(console.warn());
}


//asynv fn axios
export const loadContents = async (vars) => {
	//console.log("loadContents");
	const url =
		_PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/contents.php";
	let data = JSON.stringify({
		accessToken: accessToken(),
		action: "loadContents",
	});

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "text",
			url: url,
			headers: {},
			data: data,
		});
		//console.log(res.data);
		return res.data;
	} catch (error) {
		//console.log("Error: loadContents");
		//console.log(error);
	}
};

export const deleteContents = async (vars) => {
	//console.log('deleteContents ', vars);
	const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/contents.php";
	let data = JSON.stringify({
		accessToken: accessToken(),
		action: "deleteContents",
		vars: JSON.stringify(vars)
	});

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: data
		})
		//console.log(res.data);
		return res.data
	} catch (error) {
		//console.log("Error: deleteContents ")
		//console.log(error)
	}
}

export const loadDataContent = async (vars) => {
	//console.log("loadDataContent", vars);
	const url =
		_PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/contents.php";
	let data = JSON.stringify({
		accessToken: accessToken(),
		action: "loadDataContent",
		vars: JSON.stringify(vars),
	});

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: data,
		});
		//console.log(res.data);
		return res.data;
	} catch (error) {
		//console.log("Error: loadDataContent");
		//console.log(error);
	}
};

export const addNewContent = async (form) => {
	//console.log("addNewContent", form);
	const url =
		_PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/contents.php";

	//charge categorys
	let categorys = [];
	let countCategorys = 0;
	$("#" + form + " input[name='inputCategorys[]']").each(function (e) {
		let valorCategorys = $(this).val();
		let checkCategorys = $(this).prop("checked");
		if (checkCategorys) {
			categorys[countCategorys] = valorCategorys;
			countCategorys++;
		}
	});

	let mediaFiles = [];
	let countMediaFiles = 0;
	$("#" + form + " input[name = 'inputFilesMedia[]']").each(function (e) {
		let values = $(this).val();
		mediaFiles[countMediaFiles] = values;
		countMediaFiles++;
	});

	let data = JSON.stringify({
		accessToken: accessToken(),
		action: "addNewContent",
		vars: JSON.stringify({
			inputId: $("#" + form + " #inputId").val(),
			inputTitle: $("#" + form + " #inputTitle").val(),
			inputSummary: $("#" + form + " #inputSummary").val(),
			inputTags: $("#" + form + " #inputTags").val(),
			inputBody: $("#" + form + " #inputBody").val(),
			inputAuthor: $("#" + form + " #inputAuthor").val(),
			inputPathurl: $("#" + form + " #inputPathurl").val(),
			inputUrl: $("#" + form + " #inputUrl").val(),
			inputIcon: $("#" + form + " #inputIcon").val(),
			inputTarget: $("#" + form + " #inputTarget").val(),
			inputBtnTitle: $("#" + form + " #inputBtnTitle").val(),
			inputClass: $("#" + form + " #inputClass").val(),
			inputJson: $("#" + form + " #inputJson").val(),
			inputNotitle: $("#" + form + " #inputNotitle").val(),
			inputImg: $("#" + form + " #inputImage").val(),
			inputMediaFiles: mediaFiles,
			inputState: $("#" + form + " #inputState").val(),
			inputCategorys: categorys,
			inputMedia: mediaFiles,
		}),
	});

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "text",
			url: url,
			headers: {},
			data: data,
		});
		//console.log(res.data);
		return res.data;
	} catch (error) {
		//console.log("Error: addNewContent");
		//console.log(error);
	}
};

export const updateContent = async (form) => {
	//console.log("updateConten", form);
	const url =
		_PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/contents.php";

	//charge categorys
	let categorys = [];
	let countCategorys = 0;
	$("#" + form + " input[name='inputCategorys[]']").each(function (e) {
		let valorCategorys = $(this).val();
		let checkCategorys = $(this).prop("checked");
		if (checkCategorys) {
			categorys[countCategorys] = valorCategorys;
			countCategorys++;
		}
	});


	//charge pubs
	let pubs = [];
	let countPubs = 0;
	$("#" + form + " input[name='inputPublications[]']").each(function (e) {
		let valorPubs = $(this).val();
		let checkPubs = $(this).prop("checked");
		if (checkPubs) {
			pubs[countPubs] = valorPubs;
			countPubs++;
		}
	});

	let mediaFiles = [];
	let countMediaFiles = 0;
	$("#" + form + " .formChargeMedia .item").each(function (e) {
		let values = $(this).attr("item");
		//console.log("values", values);
		mediaFiles[countMediaFiles] = values;
		countMediaFiles++;
	});

	let data = JSON.stringify({
		accessToken: accessToken(),
		action: "updateContent",
		vars: JSON.stringify({
			inputId: $("#" + form + " #inputId").val(),
			inputTitle: $("#" + form + " #inputTitle").val(),
			inputSummary: $("#" + form + " #inputSummary").val(),
			inputTags: $("#" + form + " #inputTags").val(),
			inputBody: $("#" + form + " #inputBody").val(),
			inputAuthor: $("#" + form + " #inputAuthor").val(),
			inputPathurl: $("#" + form + " #inputPathurl").val(),
			inputUrl: $("#" + form + " #inputUrl").val(),
			inputIcon: $("#" + form + " #inputIcon").val(),
			inputTarget: $("#" + form + " #inputTarget").val(),
			inputBtnTitle: $("#" + form + " #inputBtnTitle").val(),
			inputClass: $("#" + form + " #inputClass").val(),
			inputJson: $("#" + form + " #inputJson").val(),
			inputNotitle: $("#" + form + " #inputNotitle").val(),
			inputImg: $("#" + form + " #inputImage").val(),
			inputMediaFiles: mediaFiles,
			inputState: $("#" + form + " #inputState").val(),
			inputCategorys: categorys,
			inputMedia: mediaFiles,
			inputPubs: pubs,
		}),
	});

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "text",
			url: url,
			headers: {},
			data: data,
		});
		//console.log(res.data);
		return res.data;
	} catch (error) {
		//console.log("Error: updateContent");
		//console.log(error);
	}
};

export const deleteItemsMedia = async (vars = []) => {
	console.log('deleteItemsMedia fn async', vars);
	getData({
		task: 'deleteItemsMedia',
		return: 'returnState', // returnId, returnState, returnArray, returnMessage, returnObject
		items: vars.array,
		contentId: vars.item
	}).then((response) => {
		console.log('getData deleteItemsMedia', response);
		if(response.status=='success'){
			
		}else{
			 alertMessageError({message: response.message})
		}
	}).catch(console.warn());
};

export const loadDataItemMedia = async (vars = []) => {
	//console.log("loadDataItemMedia", vars);
	const url =
		_PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/contents.php";
	let data = JSON.stringify({
		accessToken: accessToken(),
		action: "loadDataItemMedia",
		vars: JSON.stringify(vars),
	});

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: data,
		});
		//console.log(res.data);
		return res.data;
	} catch (error) {
		//console.log("Error: loadDataItemMedia");
		//console.log(error);
	}
};

export const updateItemMedia = async (form = "") => {
	//console.log("updateItemMedia", form);
	const url =
		_PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/contents.php";
	let data = JSON.stringify({
		accessToken: accessToken(),
		action: "updateItemMedia",
		vars: JSON.stringify({
			inputIdContent: $("#" + form + " #inputIdContent").val(),
			inputIdMedia: $("#" + form + " #inputIdMedia").val(),
			inputTitle: $("#" + form + " #inputTitle").val(),
			inputSummary: $("#" + form + " #inputSummary").val(),
			inputUrl: $("#" + form + " #inputUrl").val(),
			inputTarget: $("#" + form + " #inputTarget").val(),
			inputBtnTitle: $("#" + form + " #inputBtnTitle").val(),
			inputIcon: $("#" + form + " #inputIcon").val(),
		}),
	});

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: data,
		});
		//console.log(res.data);
		return res.data;
	} catch (error) {
		//console.log("Error: updateItemMedia");
		//console.log(error);
	}
};


export const getData = async (vars = []) => {
   //console.log('getData', vars);
   const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/contents.php";
   let data = JSON.stringify({
	   accessToken: accessToken(),
	   vars: JSON.stringify(vars)
   });
   try {
	   let res = await axios({
		   async: true,
		   method: "post",
		   responseType: "json",
		   url: url,
		   headers: {},
		   data: data
		})
	   //console.log(res.data);
	   jointActions(res.data)
	   return res.data
   } catch (error) {
	  console.log("Error: getData ", error);
   }
}

document.addEventListener("DOMContentLoaded", function () {
	$("body").on("click", `.btnDelete`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		console.log(`.btnDelete Content`, data);

		addHtml({
			selector: `body`,
			type: 'append', // insert, append, prepend, replace
			content: renderDeleteModal({
				name: data.name,
				id: "modalDelete",
				attr: `data-fn="delete${capitalize(tableId)}" data-id="${data.id}"`,
			})
		})

	})

	$("body").on("click", `.btnDeleteConfirmation`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		console.log(`.btnDeleteConfirmation`, data);

		deleteContents({
			ids: [data.id],
		}).then((response) => {
			//console.log('deleteContents', response);
			if (response.Error == 0) {
				$(".modalConfirm").remove();
				contentsIndex();
			} else {
				alertMessageError({
					message: response.message,
				})
			}
		}).catch(console.warn());

	})

	$("body").on("click",`.btnRemove`, function(e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		console.log(`.btnRemove`, data);
		
		$(`.formChargeImage[for="${data.input}"] .itemMedia[item="${data.id}"]`).remove();
		$(".boxInputFile").removeClass("hide");
		$("#modalDelete").remove();
		$(`#inputImage[for="${data.input}"]`).val("");
	});


	$("body").on("click",`.btnConfirmDeleteMediaContents[data-module="contents"]`, function(e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		console.log(`.btnConfirmDeleteMediaContents[module="contents"]`,data);
		deleteItemsMedia(data);
	});
});

document.addEventListener("click", (e) => {
	//console.log("click", e);

	if (e.target.matches(".btnUpdateContent")) {
		//console.log("btnUpdateContent");
		let form = e.target.getAttribute("form");
		updateForm(form);
	}

	if (e.target.matches(".btnUpdateItem")) {
		//console.log("btnUpdateContent");
		let form = e.target.getAttribute("form");
		updateForm(form, "update");
	}

	if (e.target.matches(".btnNewContent")) {
		//console.log(".btnNewContent", e.target.getAttribute("form"));
		let form = e.target.getAttribute("form");
		//deleteForm(form);
		addForm(form);
	}

	if (e.target.matches(".btnUpdateMediaEdit")) {
		//console.log(".btnUpdateMediaEdit", e.target.getAttribute("form"));
		let form = e.target.getAttribute("form");
		updateFormEditMedia(form);
	}

});