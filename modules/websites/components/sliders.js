import {
    empty,
    replacePath,
    replaceEssentials,
    replaceAll,
    loadView,
    stopLoadingBar,
    emptyReturn,
    loadingBtnIcon,
    btnLoadingRemove,
    unDisableId,
    accessToken,
    removeBtnLoading,
    alertMessageError,
    dataModule,
    jointActions,
    addHtml,
    capitalize,
    activeSelector,
    unactiveSelector,
    alertPage,
} from "../../components/functions.js";

import { today, todayTime } from "../../components/dates.js";

import {
    editorText,
    errorInput,
    errorInputSelector,
    loadInitChargeMedia,
    loadItemMedia,
    loadFilesFormLoader,
    loadSingleFileFormLoader,
    loadChargeMediaInit,
    removalMediaItems,
    ckeckboxChecked,
} from "../../components/forms.js";

import {
    categorysListTable,
    createTable,
    dataTable,
    removeItemTable,
    inactiveBtnStateItem,
    activeBtnStateItem,
    mountTable,
    colCheck,
    colId,
    colName,
    colTitle,
    colState,
    colActionsBase,
    deleteItemModule,
    renderEmpty,
    mountMacroTable,
    clearMacroTable,
} from "../../components/tables.js";

import {
    renderCol,
    renderColCheck,
    renderColId,
    renderColTitle,
    renderRowsTable,
    renderColState,
    renderColActions,
    renderColCategorys,
    renderColCheckboxState,
    renderColTitlePath,
} from "../../components/renders/renderTables.js";

import {
    innerForm,
    renderDeleteModal,
    renderDeleteModalFn,
    renderModalConfig,
    renderModalConfigBody,
    renderModalClean,
    removeInnerForm,
} from "../../components/renders/renderModals.js";

import { draggable, removeModalId } from "../../components/modals.js";

import { getCategorys, listCategorys, checkCategorys } from "./categorys.js";

import { renderListElements } from "../../components/renders/renderLists.js";

const module = "sliders";
const system = "websites";
let tableId = "tableSliders";

export const slidersIndex = () => {
    //console.log('slidersIndex', module);
    getData({
        task: "getDataSliders",
        return: "returnArray", // returnId, returnState, returnArray, returnMessage, returnObject
    })
        .then((responseData) => {
            console.log('getDataSliders', responseData);

            if (responseData.status == "success") {
                loadView(
                    _PATH_WEB_NUCLEO +
                        "modules/websites/views/sliders.html?" +
                        _VS
                ).then((response) => {
                    //console.log(response);
                    let content = "";

                    

                    let str = replaceEssentials({

                        

                        str: response,
                        module,
                        system,
                        icon: dataModule(module, "icon"),
                        name: dataModule(module, "name"),
                        color: dataModule(module, "color"),
                        btnId: "btnNewFormSlider",
                        btnName: "Nuevo Slider",
                        content,
                    });

                    str = str.replace(/{{_MODULE}}/g, module);
                    str = str.replace(/{{_SYSTEM}}/g, system);

                    addHtml({
                        selector: `.bodyModule[module="${module}"]`,
                        type: "insert",
                        content: str, // insert, append, prepend, replace
                    });

                    if (responseData.Error == 0 && responseData.data != 0) {
                        let rows = "";
                        for (let i in responseData.data) {
                            let item = responseData.data[i];

                            //console.log("item", item);
                            rows += renderRowsTable({
                                id: item.id,
                                content:
                                    renderColCheck({
                                        id: item.id,
                                    }) +
                                    renderColId({
                                        id: item.id,
                                    }) +
                                    renderColTitlePath({
                                        id: item.id,
                                        title: item.name,
                                        link: _PATH_WEB + item.link,
                                    }) +
                                    renderCol({
                                        id: item.id,
                                        data: item.date,
                                    }) +
                                    renderColCategorys({
                                        id: item.id,
                                        fn: "categorysListTable",
                                        array: item.categorys,
                                    }) +
                                    renderCol({
                                        id: item.id,
                                        data: item.user.userName,
                                    }) +
                                    renderColState({
                                        id: item.id,
                                        state: item.state,
                                        fn: "changeStatePost",
                                        module,
                                        system,
                                    }) +
                                    renderColActions({
                                        id: item.id,
                                        name: item.title,
                                        type: "btn,btn",
                                        fnType: "btnEditItemSlider,btnDeleteItemSlider",
                                        attr: `data-id="${item.id}" data-module="${module}" data-system="${system}" data-name="${item.name}"`,
                                    }),
                            });
                        }

                        mountTable({
                            id: tableId,
                            columns: [
                                ...colCheck,
                                ...colId,
                                ...colName,
                                {
                                    label: "Fecha",
                                    cls: "colDate",
                                },
                                {
                                    label: "Categorias",
                                    cls: "colCategorys",
                                },
                                {
                                    label: "User",
                                    cls: "colAuthors",
                                },
                                ...colState,
                                ...colActionsBase,
                            ],
                            rows,
                            module,
                            system,
                            container:
                                ".bodyModule[module='" + module + "'] .tbody",
                        });
                    } else if (responseData === 0 || responseData.data == 0) {
                        console.log(`.bodyModule[module="${module}"] `);
                        $(`.bodyModule[module="${module}"]  .tbody`).html(
                            renderEmpty()
                        );
                    } else {
                        alertPage({
                            text:
                                "Error. por favor contactarse con soporte. " +
                                responseData.message,
                            icon: "icn icon-alert-warning",
                            animation_in: "bounceInRight",
                            animation_out: "bounceOutRight",
                            tipe: "danger",
                            time: "3500",
                            position: "top-left",
                        });
                    }
                });
            } else {
                alertMessageError({
                    message: responseData.message,
                });
            }
        })
        .catch(console.warn());
};

export const getData = async (vars = []) => {
    const url =
        _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/sliders.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars),
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data,
        });
        jointActions(res.data);
        return res.data;
    } catch (error) {
        console.log("Error: getData ", error);
    }
};

export const sliderUpdate = ({ formId, id }) => {
    let inputTitle = $("#" + formId + " #inputTitle").val();
    let inputDescription = $("#" + formId + " #inputDescription").val();
    let inputCls = $("#" + formId + " #inputClass").val();
    let inputJson = $("#" + formId + " #inputJson").val();
    let inputDate = $("#" + formId + " #inputDate").val();
    let inputState = $("#" + formId + " #inputState").val();

    let count = 0;

    if (inputTitle == "") {
        errorInputSelector(
            `#${formId} #inputTitle`,
            "El campo Título es obligatorio"
        );
        count++;
    }

    let categorys = [];
    let countCategorys = 0;
    $("#" + formId + " input[name='inputCategorys[]']").each(function (e) {
        let valorCategorys = $(this).val();
        let checkCategorys = $(this).prop("checked");
        if (checkCategorys) {
            categorys[countCategorys] = valorCategorys;
            countCategorys++;
        }
    });

    if (count == 0) {
        let mediaFiles = [];
        let countMediaFiles = 0;
        $("#" + formId + " input[name = 'inputFilesMedia[]']").each(function (e) {
            let values = $(this).val();
            mediaFiles[countMediaFiles] = values;
            countMediaFiles++;
        });
        getData({
            task: "updateSlider",
            return: "returnId", // returnId, returnState, returnArray, returnMessage, returnObject
            inputs: {
                inputId: id,
                inputTitle,
                inputDescription,
                inputDate,
                inputCls,
                inputJson,
                inputState,
                categorys,
                mediaFiles,
            },
        }).then((response) => {
                console.log("getData update slider", response);
                if (response.status == "success") {
                    removeInnerForm();
                    slidersIndex();
                } else {
                    alertMessageError({
                        message: response.message,
                    });
                }
            })
            .catch(console.warn());
    }
};

document.addEventListener("DOMContentLoaded", function () {
    $("body").on("click", `#btnNewFormSlider`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`#btnNewFormSlider`, data);
        loadView(
            _PATH_WEB_NUCLEO + "modules/websites/views/formSliders.html?" + _VS
        ).then((response) => {
            let str = replacePath(response);
            str = str.replace(/{{_TITLE}}/g, "Nuevo Slider");
            str = str.replace(/{{_BTN_NAME_ACTION}}/g, "Guardar");
            str = str.replace(/{{_BTN_ACTION}}/g, "btnSaveFormSlider");
            str = str.replace(/{{_FORM_ID}}/g, "newFormSlider");
            str = str.replace(/{{_CATEGORYS}}/g, listCategorys(_CATEGORYS));
            str = str.replace(/{{_MODULE}}/g, module);
            str = str.replace(/{{_SYSTEM}}/g, system);
            innerForm({
                module,
                body: str,
            });

            jQuery.datetimepicker.setLocale("es");
            let date = todayTime();
            $("#newFormSlider #inputDate").val(date);
            $("#newFormSlider #inputDate").datetimepicker({
                timepicker: false,
                datepicker: true,
                format: "Y-m-d H:i",
                lang: "es",
            });
        });
    });

    $("body").on("click", `.btnEditItemSlider`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let formId = "updateFormSlider";
        console.log(`.btnEditItemSlider`, data, formId);

        loadView(
            _PATH_WEB_NUCLEO + "modules/websites/views/formSliders.html?" + _VS
        ).then((response) => {
            let str = replacePath(response);
            str = str.replace(/{{_TITLE}}/g, "Editar Slider");
            str = str.replace(/{{_BTN_NAME_ACTION}}/g, "Actualizar");
            str = str.replace(/{{_BTN_ACTION}}/g, "btnUpdateFormSlider");
            str = str.replace(/{{_FORM_ID}}/g, formId);
            str = str.replace(/{{_CATEGORYS}}/g, listCategorys(_CATEGORYS));
            str = str.replace(/{{_MODULE}}/g, module);
            str = str.replace(/{{_SYSTEM}}/g, system);

            getData({
                task: "getItemSlider",
                return: "returnArray", // returnId, returnState, returnArray, returnMessage, returnObject
                inputId: data.id,
            })
                .then((response) => {
                    console.log("getData getItemSlider", response);
                    if (response.status == "success") {
                        let data = response.data;
                        let files = response.data.media;
                        let filesMedia = [];

                        console.log("files", files);
                       

                        $("#btnUpdateFormSlider").attr("data-item", data.id);
                        $(`#${formId} #inputId`).val(data.id);
                        $(`#${formId} #inputTitle`).val(data.name);
                        $(`#${formId} #inputDescription`).val(data.description);
                        $(`#${formId} #inputCls`).val(data.cls);
                        $(`#${formId} #inputJson`).val(data.json);
                        $(`#${formId} #inputDate`).val(data.date);
                        $(`#${formId} #inputState`).val(data.state);

                        if (data.categorys != null && data.categorys != 0) {
                            data.categorys.forEach((cat) => {
                                document
                                    .querySelectorAll(
                                        'input[name="inputCategorys[]"]'
                                    )
                                    .forEach((input) => {
                                        if (input.value === cat.id.toString()) {
                                            input.checked = true;
                                        }
                                    });
                            });
                        }

                        if (files != 0) {
                            files.forEach((file) => {
                                filesMedia.push(file.data);
                            });

                            console.log("filesMedia", filesMedia);
                            
                            loadFilesFormLoader({
                                id: "inputFilesMedia",
                                files: filesMedia,
                                module
                            });
                            
                        }
                        
                    } else {
                        alertMessageError({ message: response.message });
                    }
                })
                .catch(console.warn());

            innerForm({
                module,
                body: str,
            });
        });
    });

    $("body").on("click", `#btnSaveFormSlider`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`#btnSaveFormSlider`, data);
        let formId = data.form;
        let count = 0;

        let inputTitle = $("#" + formId + " #inputTitle").val();
        let inputDescription = $("#" + formId + " #inputDescription").val();
        let inputCls = $("#" + formId + " #inputCls").val();
        let inputJson = $("#" + formId + " #inputJson").val();
        let inputState = $("#" + formId + " #inputState").val();
        let inputDate = $("#" + formId + " #inputDate").val();

        if (inputTitle == "") {
            errorInput("inputTitle", "Debe ingresar un titulo");
            count++;
        }

        let categorys = [];
        let countCategorys = 0;
        $("#" + formId + " input[name='inputCategorys[]']").each(function (e) {
            let valorCategorys = $(this).val();
            let checkCategorys = $(this).prop("checked");
            if (checkCategorys) {
                categorys[countCategorys] = valorCategorys;
                countCategorys++;
            }
        });

        if (count == 0) {
            let mediaFiles = [];
            let countMediaFiles = 0;
            $("#" + formId + " input[name = 'inputFilesMedia[]']").each(
                function (e) {
                    let values = $(this).val();
                    mediaFiles[countMediaFiles] = values;
                    countMediaFiles++;
                }
            );

            getData({
                task: "addSlider",
                return: "returnId", // returnId, returnState, returnArray
                inputs: {
                    inputTitle,
                    inputDescription,
                    mediaFiles,
                    categorys,
                    inputCls,
                    inputJson,
                    inputDate,
                    inputState,
                },
            })
                .then((response) => {
                    console.log("addSlider", response);
                    if (response.status == "success") {
                        removeInnerForm();
                        slidersIndex();
                    } else {
                        alertMessageError({
                            message: response.message,
                        });
                    }
                })
                .catch(console.warn());
        }
    });

    $("body").on("click", `#btnUpdateFormSlider`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(data.form);
        sliderUpdate({
            formId: data.form,
            id: data.item,
        });
    });

    $("body").on(
        "click",
        `.btnDeleteItemSlider[data-module="${module}"]`,
        function (e) {
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            let data = $(this).data();
            console.log(`.btnDeleteFormListElement`, data);
            let id = $(this).attr("data-id");
            let name = $(this).attr("data-name");

            console.log(`.btnDelete`);

            addHtml({
                selector: ".bodyModule[module='" + module + "']",
                type: "prepend",
                content: renderDeleteModal({
                    name,
                    module,
                    cls: "btnDeleteConfirmationSliders",
                    id: "modalDeleteFormListElement",
                    attr: `data-fn="deleteFormListElement" data-module="${module}" data-id="${id}"`,
                }),
            }); //type: html, append, prepend, before, after
        }
    );

    $("body").on("click", `.btnDeleteConfirmationSliders`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let id = $(this).attr("data-id");
        console.log(`.btnDeleteConfirmationSliders`, id);

        getData({
            task: "delete",
            return: "returnState",
            inputId: id,
        })
            .then((response) => {
                console.log("delete", response);
                if (response.status == "success") {
                    slidersIndex();
                    removeModalId("modalDeleteFormListElement");
                } else {
                    alertMessageError({
                        message: response.message,
                    });
                }
            })
            .catch(console.warn());
    });

    $("body").on(
        "click",
        `.btnEditListElement[data-module="${module}"]`,
        function (e) {
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            let data = $(this).data();
        }
    );
});
