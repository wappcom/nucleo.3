import {
    empty,
    replacePath,
    replaceEssentials,
    replaceAll,
    loadView,
    stopLoadingBar,
    emptyReturn,
    loadingBtnIcon,
    btnLoadingRemove,
    unDisableId,
    accessToken,
    removeBtnLoading,
    alertMessageError,
    dataModule,
    addHtml,
    capitalize,
    jointActions,
} from "../../components/functions.js";



import {
    editorText,
    errorInput,
    loadInitChargeMedia,
    loadItemMedia,
    loadFilesFormLoader,
    loadSingleFileFormLoader,
    loadChargeMediaInit,
    removalMediaItems,
    ckeckboxChecked,
    errorInputSelector,
} from "../../components/forms.js";



import {
    createTable,
    dataTable,
    removeItemTable,
} from "../../components/tables.js";

import {
    renderTr
} from "./renders/renderLinks.js";

import {
    getData as getPublications
} from "./publications.js";

import {
    getCategorys,
    listCategorys,
    checkCategorys
} from "./categorys.js";
import {
    renderNoDataTable
} from "../../components/renders/renderTables.js";

import {
    innerForm,
    removeInnerForm,
    renderDeleteModal,
    renderDeleteModalFn,
} from "../../components/renders/renderModals.js";

import {
    renderCheckBox
} from "../../components/renders/renderForms.js";

let module = "links";
let system = "websites";
let formId = "formLinks";
let tableId = "tableLinks";

export const linksIndex = (vars = []) => {
    //console.log("linksIndex", vars);
    loadView(_PATH_WEB_NUCLEO + "modules/websites/views/links.html?" + _VS)
        .then((responseView) => {
            //console.log('loadView', responseView);
            let content = responseView;
            content = replaceAll(content, "{{_MODULE}}", module);
            content = replaceAll(content, "{{_SYSTEM}}", system);
            content = replaceAll(
                content,
                "{{_COLOR}}",
                dataModule(module, "color")
            );
            content = replaceAll(content, "{{_FN}}", "newFormLink");

            addHtml({
                selector: `.bodyModule[module='${module}']`,
                type: "insert",
                content,
            }); //type: html, append, prepend, before, after

            getData({
                    task: "getLinksList",
                    return: "returnArray", // returnId, returnState, returnArray, returnMessage, returnObject
                })
                .then((response) => {
                    //console.log("getLinksList", response);
                    if (response.status == "success") {
                        if (response != null && response.Error == 0) {
                            const data = response.data;
                            let strTable = "";
                            let tbody = "";
                            //console.log(data);

                            for (let i = 0; i < data.length; i++) {
                                const elem = data[i];
                                elem["table"] = tableId;
                                elem["module"] = module;
                                elem["system"] = system;
                                elem["path"] = capitalize(module);
                                tbody += renderTr(elem);
                            }

                            strTable = createTable({
                                id: tableId,
                                thead: ":check,id:colId,Titulo,Url,Estado:colState,Acciones:colActions",
                                body: tbody,
                            });

                            $(
                                ".bodyModule[module='" + module + "'] >.tbody"
                            ).html(strTable);

                            dataTable({
                                elem: "#" + tableId,
                                orderCol: 1,
                            });
                        } else if (response.data == 0) {
                            $(
                                ".bodyModule[module='" + module + "'] >.tbody"
                            ).html(renderNoDataTable());
                        }
                    } else {
                        alertMessageError({
                            message: response.message,
                        });
                    }
                })
                .catch(console.warn());
        })
        .catch(console.warn());
};

export const newFormLink = (vars = []) => {
    //console.log("newFormLink", vars);
    getPublications({
            task: "dataPublications",
            return: "returnArray",
        })
        .then((responsePubs) => {
            loadView(_PATH_WEB_NUCLEO + "modules/websites/views/linksForm.html?" + _VS)
                .then((responseView) => {
                    //console.log('loadView', responseView);
                    let content = responseView;
                    content = replaceAll(content, "{{_MODULE}}", module);
                    content = replaceAll(content, "{{_SYSTEM}}", system);
                    content = replaceAll(content, "{{_BTNS_TOP}}", "");
                    content = replaceAll(content, "{{_BTN_ACTION}}", "btnFormNewLink");
                    content = replaceAll(
                        content,
                        "{{_BTN_NAME_ACTION}}",
                        "Guardar Nuevo Link"
                    );
                    content = replaceAll(content, "{{_FORM_ID}}", formId);
                    content = replaceAll(content, "{{_TITLE}}", "Nuevo Link");
                    content = replaceAll(
                        content,
                        "{{_CATEGORYS}}",
                        listCategorys(_CATEGORYS)
                    );

                    content = content.replace(
                        /{{_PUBLICATIONS}}/g,
                        renderCheckBox({
                            array: responsePubs.data,
                            id: "inputPublications",
                        })
                    );

                    innerForm({
                        module,
                        body: content,
                    });
                })
                .catch(console.warn());
        })
        .catch(console.warn());
};

export const editFormLink = ({
    module,
    id = 0
}) => {
    //console.log("editFormLink", module, id);
    getPublications({
            task: "dataPublications",
            return: "returnArray"
        })
        .then((responsePubs) => {
            loadView(
                    _PATH_WEB_NUCLEO +
                    "modules/websites/views/linksForm.html?" +
                    _VS
                )
                .then((responseView) => {
                    //console.log('loadView', responseView);
                    let content = responseView;
                    content = replaceAll(content, "{{_MODULE}}", module);
                    content = replaceAll(content, "{{_SYSTEM}}", system);
                    content = replaceAll(content, "{{_BTNS_TOP}}", "");
                    content = replaceAll(
                        content,
                        "{{_BTN_ACTION}}",
                        "btnFormUpdateLink"
                    );
                    content = replaceAll(
                        content,
                        "{{_BTN_NAME_ACTION}}",
                        "Actualizar Link"
                    );
                    content = replaceAll(content, "{{_FORM_ID}}", formId);
                    content = replaceAll(content, "{{_TITLE}}", "Editar Link");
                    content = replaceAll(
                        content,
                        "{{_CATEGORYS}}",
                        listCategorys(_CATEGORYS)
                    );

                    content = content.replace(/{{_PUBLICATIONS}}/g, renderCheckBox({
                        array: responsePubs.data,
                        id: "inputPublications",
                    }));

                    content = content.replace(/{{_ITEM}}/g, id);

                    innerForm({
                        module,
                        body: content,
                    });

                    getData({
                            task: "getDataId",
                            return: "returnArray", // returnId, returnState, returnArray, returnMessage, returnObject
                            id,
                        })
                        .then((response) => {
                            //console.log("getData getDataId", response);
                            if (response.status == "success") {
                                let data = response.data;
                                let img = data.img;
                                let target = data.target;

                                $("#" + formId + " #inputId").val(data.id);
                                $("#" + formId + " #inputTitle").val(
                                    data.title
                                );
                                $("#" + formId + " #inputDescription").val(
                                    data.description
                                );
                                // activar el option del input select
                                $("#" + formId + " #inputTarget option[value='" + target + "']").attr("selected", true);
                                $("#" + formId + " #inputUrl").val(data.url);
                                $("#" + formId + " #inputTags").val(data.tags);
                                $("#" + formId + " #inputState").val(
                                    data.state
                                );
                                $("#" + formId + " #inputClass").val(data.cls);
                                $("#" + formId + " #inputJson").val(data.json);
                                $("#" + formId + " #inputIcon").val(data.icon);
                                $("#" + formId + " div[for='inputIcon'] .iconSelect").attr("class", data.icon);
                                $("#" + formId + " #inputAttr").val(data.attr);
                                $("#" + formId + " #inputAlt").val(data.alt);

                                loadSingleFileFormLoader({
                                    id: "inputImageCharge",
                                    file: img,
                                });

                                checkCategorys(data.categorys);

                                ckeckboxChecked({
                                    array: response.data.pubs,
                                    name: "inputPublications[]",
                                });

                            } else {
                                alertMessageError({
                                    message: response.message,
                                });
                            }
                        })
                        .catch(console.warn());
                })
                .catch(console.warn());
        })
        .catch(console.warn());
};

export const linkAdd = ({
    form
}) => {
    //console.log('linkAdd',form);
    let title = $(`#${form} #inputTitle`).val();
    let url = $(`#${form} #inputUrl`).val();
    let count = 0;
    console.log("linkAdd", title, url);

    if (title == "") {
        errorInputSelector(
            `#${form} #inputTitle`,
            "El campo Título es obligatorio"
        );
        count++;
    }

    if (url == "") {
        errorInputSelector(`#${form} #inputUrl`, "El campo URL es obligatorio");
        count++;
    }

    //charge categorys
    let categorys = [];
    let countCategorys = 0;
    $("#" + form + " input[name='inputCategorys[]']").each(function (e) {
        let valorCategorys = $(this).val();
        let checkCategorys = $(this).prop("checked");
        if (checkCategorys) {
            categorys[countCategorys] = valorCategorys;
            countCategorys++;
        }
    });

    //charge pubs
    let pubs = [];
    let countPubs = 0;
    $("#" + form + " input[name='inputPublications[]']").each(function (e) {
        let valorPubs = $(this).val();
        let checkPubs = $(this).prop("checked");
        if (checkPubs) {
            pubs[countPubs] = valorPubs;
            countPubs++;
        }
    });

    if (count == 0) {
        getData({
                task: "add",
                return: "returnArray", // returnId, returnState, returnArray, returnMessage, returnObject
                inputs: {
                    inputTitle: title,
                    inputDescription: $(`#${form} #inputDescription`).val(),
                    inputUrl: url,
                    inputTags: $(`#${form} #inputTags`).val(),
                    inputTarget: $(`#${form} #inputTarget`).val(),
                    inputIcon: $(`#${form} #inputIcon`).val(),
                    inputAlt: $(`#${form} #inputAlt`).val(),
                    inputClass: $(`#${form} #inputClass`).val(),
                    inputAttr: $(`#${form} #inputAttr`).val(),
                    inputImage: $(`#${form} #inputImage`).val(),
                    inputJson: $(`#${form} #inputJson`).val(),
                    inputCategorys: categorys,
                    inputPublications: pubs,
                },
            })
            .then((response) => {
                console.log("getData add", response);
                if (response.status == "success") {
                    removeInnerForm();
                    linksIndex();
                } else {
                    alertMessageError({
                        message: response.message,
                    });
                }
            })
            .catch(console.warn());
    }
};

export const linkUpdate = ({
    form,
    id
}) => {
    //console.log('linkUpdate',form,id);
    let title = $(`#${form} #inputTitle`).val();
    let url = $(`#${form} #inputUrl`).val();
    let count = 0;

    if (title == "") {
        errorInputSelector(
            `#${form} #inputTitle`,
            "El campo Título es obligatorio"
        );
        count++;
    }

    if (url == "") {
        errorInputSelector(`#${form} #inputUrl`, "El campo URL es obligatorio");
        count++;
    }

    //charge categorys
    let categorys = [];
    let countCategorys = 0;
    $("#" + form + " input[name='inputCategorys[]']").each(function (e) {
        let valorCategorys = $(this).val();
        let checkCategorys = $(this).prop("checked");
        if (checkCategorys) {
            categorys[countCategorys] = valorCategorys;
            countCategorys++;
        }
    })

    //charge pubs
    let pubs = [];
    let countPubs = 0;
    $("#" + form + " input[name='inputPublications[]']").each(function (e) {
        let valorPubs = $(this).val();
        let checkPubs = $(this).prop("checked");
        if (checkPubs) {
            pubs[countPubs] = valorPubs;
            countPubs++;
        }
    });

    if (count == 0) {
        getData({
                task: "update",
                return: "returnState", // returnId, returnState, returnArray, returnMessage, returnObject
                inputs: {
                    inputId: id,
                    inputTitle: title,
                    inputDescription: $(`#${form} #inputDescription`).val(),
                    inputUrl: url,
                    inputTags: $(`#${form} #inputTags`).val(),
                    inputTarget: $(`#${form} #inputTarget`).val(),
                    inputIcon: $(`#${form} #inputIcon`).val(),
                    inputAlt: $(`#${form} #inputAlt`).val(),
                    inputClass: $(`#${form} #inputClass`).val(),
                    inputAttr: $(`#${form} #inputAttr`).val(),
                    inputImage: $(`#${form} #inputImage`).val(),
                    inputJson: $(`#${form} #inputJson`).val(),
                    inputCategorys: categorys,
                    inputPublications: pubs,
                    inputState: $(`#${form} #inputState`).val()
                },
            }).then((response) => {
                //console.log("getData update link", response);
                if (response.status == "success") {
                    removeInnerForm();
                    linksIndex();
                } else {
                    alertMessageError({
                        message: response.message,
                    });
                }
            })
            .catch(console.warn());
    }
}

export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url =
        _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/links.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars),
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data,
        });
        //console.log(res.data);
        jointActions(res.data);
        return res.data;
    } catch (error) {
        console.log("Error: getData ", error);
    }
};

document.addEventListener("DOMContentLoaded", function () {
    $("body").on("click", `#btnFormNewLink`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`#btnFormNewLink`, data);
        linkAdd({
            form: data.form,
        });
    });

    $("body").on("click", `.btnEditFormLinks`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`.btnEditFormLinks`, data);
        editFormLink(data);
    });

    $("body").on("click", `#btnFormUpdateLink`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`#btnFormUpdateLink`, data);
        linkUpdate({
            form: data.form,
            id: data.item
        });
    });

    $("body").on(`click`, `.btnDeleteForm${capitalize(module)}`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnDeleteForm${capitalize(module)}`, data);
        addHtml({
            selector: `body`,
            type: "append", // insert, append, prepend, replace
            content: renderDeleteModalFn({
                name: data.name,
                id: "modalDelete",
                module,
                attr: `data-module="${module}" data-id="${data.id}"`,
            }),
        });
    });

    $("body").on("click", `.btnDeleteConfirmation${capitalize(module)}`,
        function (e) {
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            let data = $(this).data();
            console.log(`.btnDeleteConfirmation${capitalize(module)}`, data);

            getData({
                    task: "deleteItems",
                    return: "returnState", // returnId, returnState, returnArray, returnMessage, returnObject
                    ids: [data.id],
                })
                .then((response) => {
                    console.log("getData DeleteItems", response);
                    if (response.status == "success") {
                        $(".modalConfirm").remove();
                        eval(`${module}Index()`);
                    } else {
                        alertMessageError({
                            message: response.message,
                        });
                    }
                })
                .catch(console.warn());
        }
    );
});