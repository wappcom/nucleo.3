import {
    loadingBarIdReturn,
    loadView
} from "../../components/functions.js";
import {
    renderModal
} from "../../components/renders/renderModals.js";
import {
    btnStateToogle
} from "../../components/nav.js";

import {
    renderLoading
} from "../../components/renders/render.js";

import {
    publicationsIndex
} from "./publications.js";


const module = "websites";
const system = "websites";

export function websitesIndex() {
    //clearInterval(displayLoadingBar);

    //console.log("websitesIndex");
    $(".bodyModule[system='" + system + "'][module='" + module + "']").html("Dashboard webSite");

}

export function websitesConfigCreate(btnId) {
    console.log("websitesConfigCreate", btnId, module);
    const modalId = "modalConfigWebsites";
    $(".boxModule[system='" + system + "']").prepend(
        renderModal({
            cls: "modalMain animated fadeUp",
            body: loadingBarIdReturn(),
            id: modalId,
        })
    );

    loadView(_PATH_WEB_NUCLEO + "modules/websites/views/menuConfig.html?" + _VS).then((response) => {
        //console.log('loadView', response);
        $("#modalConfigWebsites .modalInner").html(response);

    }).catch(console.warn());

    resizeConfig();

}



export const resizeConfig = (vars) => {
    //console.log('resizeConfig',vars);
    $("#modalConfigWebsites").outerHeight($(window).height() - 84);
}

document.addEventListener("DOMContentLoaded", function () {
    $("body").on("click", `#modalConfigWebsites .btnSidebarModal`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let content = $(this).attr("content");
        console.log(`#modalConfigWebsites .btnSidebarModal`, content);
        $("#modalConfigWebsites .btnSidebarModal").removeClass("active");
        $(this).addClass("active");
        eval(content + "Index()");
    });

    $("body").on("click", `.btnConfigWebsites`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data()
        console.log(`.btnConfigWebsites`, data);
        const state = $(this).attr("state");
        const btnId = $(this).id;
        if (state == 0) {
            //console.log('btnConfigWebsites');
            websitesConfigCreate(btnId);
            $(this).attr("state", "1");
        } else {
            $("#modalConfigWebsites").remove();
            $(this).attr("state", "0");
        }
    })
})

/* 
document.addEventListener('click', function (event) {
    // console.log('works fine', event)
    let btnId = event.target.parentElement.id;
    let btnClass = event.target.classList;

    if (btnId == 'btnConfigWebsites') {
        const id = $("#" + btnId);
        if (id.attr("state") == 0) {
            console.log('btnConfigWebsites');
            websitesConfigCreate(btnId);
            btnStateToogle(btnId);
        } else {
            $("#modalConfigWebsites").remove();
            btnStateToogle(btnId);
        }
    }
*/

/* if ( btnClass.matches('.btnConfigWebsites')) {
    event.preventDefault();
}*/
/*}, false); */