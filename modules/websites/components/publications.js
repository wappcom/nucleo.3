import {
    empty,
    replacePath,
    replaceEssentials,
    replaceAll,
    loadView,
    stopLoadingBar,
    emptyReturn,
    loadingBtnIcon,
    btnLoadingRemove,
    unDisableId,
    accessToken,
    removeBtnLoading,
    dataModule,
    alertMessageError,
    addHtml,
    getScript,
    loadViewJson,
    capitalize,
    removeInnerForm,
    jointActions
} from "../../components/functions.js";

import {
    errorInput
} from "../../components/forms.js";
import {
    renderHelperText,
} from "../../components/renders/renderForms.js";

import {
    pubElem
} from "./renders/renderPublications.js";

import {
    renderWorksheetElem
} from "./renders/renderWorksheets.js";

import {
    dataTable,
    setupTable,
    removeItemTable
} from "../../components/tables.js";

import {
    removeModal,
    removeModalId,
    deleteModal
} from "../../components/modals.js";

import {
    renderModalConfig,
    renderDeleteModal,
    innerForm,
    renderModalClean,
    renderContextMenu
} from "../../components/renders/renderModals.js";

var module = 'publications';
var system = 'websites';
let pathurl = "modules/websites/";
let tableId = "tablePublications";


export const publicationsIndex = (vars = []) => {
    //console.log('publicationsIndex', vars);
    let selector = vars.selector ? vars.selector : "#modalConfigWebsites .modalbody";
    let siteId = vars.siteId ? vars.siteId : 1;

    loadView(_PATH_WEB_NUCLEO + "modules/websites/views/publications.html?" + _VS).then((responseView) => {
        //console.log('loadView', responseView);
        dataPublications({
            siteId
        }).then((responsePubs) => {
            //console.log('dataPublications', responsePubs);


            if (responsePubs.status == 'success') {
                let data = responsePubs.data;
                let table = setupTable({
                    tableId,
                    cols: [{
                        elem: "check",
                    }, {
                        elem: "id",
                        label: "Id",
                    }, {
                        elem: "name",
                        label: "Nombre",
                    }, {
                        elem: "state",
                        label: "Estado",
                    }, {
                        elem: "actions",
                        label: "Acciones",
                        btns: ["btnEdit", "btnDelete"]
                    }],
                    data,
                });
                addHtml({
                    selector,
                    type: 'insert',
                    content: replaceEssentials({
                        str: responseView,
                        icon: 'icon icon-webpart',
                        name: 'Publicaciones',
                        btnLabelAction: 'Nueva Publicación',
                        btnAction: 'publicationsCreate',
                        cls: 'btnAdd',
                        formId: 'formNewPublication',
                        body: table
                    })
                }) //type: html, append, prepend, before, after

                dataTable({
                    elem: "#" + tableId,
                    orderCol: 0,
                });
            } else {
                alertMessageError({
                    message: response.message
                });
            }

        }).catch(console.warn());
    }).catch(console.warn());


}

export const getFormNewPub = (vars = []) => {
    //console.log('getFormNewPub', vars);
    const siteId = 1;
    const formId = "formNewPub";
    const attr = `block="${vars.blockId}" form="${formId}" ws="${vars.wsId}" cat="${vars.catId}"`;
    getTypesPubsSiteId({
        siteId
    }).then((responsePubsList) => {
        //console.log('dataPublications', responsePubsList);

        loadView(_PATH_WEB_NUCLEO + "modules/websites/views/formPubModal.html?" + _VS).then((response) => {
            //console.log('loadView', response);
            let str = replaceEssentials({
                str: response,
                module: vars.module,
                form: formId,
            });

            str = str.replace(/\{{_LIST_PUBS}}/g, responsePubsList);
            str = str.replace(/\{{_FORM_MODAL}}/g, "modalPubNewModal");
            str = str.replace(/\{{_BTN_ACTION_LABEL}}/g, "Insertar");
            str = str.replace(/\{{_BTN_ACTION_CLS}}/g, "btnInsertPub");
            str = str.replace(/\{{_BTN_ACTION_ATTR}}/g, attr);


            addHtml({
                elem: ".boxModule[module=" + vars.module + "]",
                type: "prepend",
                html: renderModalConfig({
                    module: vars.module,
                    system,
                    id: "modalPubForm",
                    item: vars.item,
                    attr,
                    innerCls: "animated enlarge",
                    body: str
                })
            })

            resizePublications();
        }).catch(console.warn()); //End Load View
    }).catch(console.warn()); //End getPubsSiteId
}

export const formNewPub = (vars = []) => {
    console.log('formNewPub', vars);
    const siteId = 1;
    const formId = "formNewPub";
    const attr = ``;

    getTypesPubsSiteId({
        siteId
    }).then((responsePubsList) => {
        //console.log('dataPublications', responsePubsList);
        loadView(_PATH_WEB_NUCLEO + "modules/websites/views/formPublication.html?" + _VS).then((responseView) => {
            //console.log('loadView', responseView);
            let str = replaceEssentials({
                str: responseView,
                module: vars.module,
                form: formId,
            });

            str = str.replace(/\{{_LIST_PUBS}}/g, responsePubsList);
            str = str.replace(/\{{_BTN_ACTION_LABEL}}/g, "Agregar");
            str = str.replace(/\{{_FN}}/g, "savePub");
            str = str.replace(/\{{_BTN_ACTION_ATTR}}/g, attr);

            addHtml({
                selector: `#modalConfigWebsites .bodyModule`,
                type: 'prepend', // insert, append, prepend, replace
                content: `<div class="innerForm on responsive">${str}</div>`
            })
            resizePublications();

        }).catch(console.warn());
    }).catch(console.warn());
}

export const getFormSelectedPub = (vars = []) => {
    //console.log('getFormSelectedPub', vars);
    const siteId = 1;
    const formId = "formSelectedPub";
    const attr = `level="${vars.level}" block="${vars.blockId}" form="${formId}" ws="${vars.wsId}" cat="${vars.catId}"`;
    dataPublications({
        siteId
    }).then((responsePubs) => {
        //console.log('dataPublications', responsePubs);
        let responsePubsList = getPubListSelect(responsePubs);

        loadView(_PATH_WEB_NUCLEO + "modules/websites/views/formPubSelect.html?" + _VS).then((response) => {
            //console.log('loadView', response);
            let str = replaceEssentials({
                str: response,
                module: vars.module,
                form: formId,
            });

            str = str.replace(/\{{_LIST_PUBS}}/g, responsePubsList);
            str = str.replace(/\{{_BTN_ACTION_ATTR}}/g, attr);

            addHtml({
                elem: ".boxModule[module=" + vars.module + "]",
                type: "prepend",
                html: renderModalConfig({
                    module: vars.module,
                    system,
                    id: "modalPubForm",
                    item: vars.item,
                    attr,
                    innerCls: "animated enlarge", //enlarge",
                    body: str
                })
            })

            resizePublications();
        }).catch(console.warn()); //End Load View
    }).catch(console.warn()); //End getPubsSiteId

}

export const getPubTypeListSelect = (vars = []) => {
    //console.log('getPubListSelect', vars);
    const pathHost = vars.items.pathHost;
    const arrayPubs = vars.items.data;


    let str = '';
    for (let i = 0; i < arrayPubs.length; i++) {
        let elm = arrayPubs[i];
        //console.log(elm);
        let strElm = pubElem(elm, i);
        strElm = replaceEssentials({
            str: strElm,
        })
        str += strElm;
    }

    return str;

}

export const getPubListSelect = (vars = []) => {
    //console.log('getPubListSelect', vars);
    let arrayPubs = vars.data.data;
    if (arrayPubs == undefined) {
        arrayPubs = vars.data;
    }

    //console.log('arrayPubs', arrayPubs);

    let str = '';
    for (let i = 0; i < arrayPubs.length; i++) {
        let elm = arrayPubs[i];

        //console.log(elm, icon);
        let strElm = pubElem({
            name: elm.name,
            item: elm.id,
            path: elm.path,
            pathUI: elm.pathUI,
            pathTpl: elm.pathTpl,
            cls: elm.cls,
            description: elm.description,
            type: elm.type,
            pathIcon: elm.pathIcon,
        }, i);

        str += strElm;
    }
    return str;

}

export const resizePublications = (vars = []) => {
    //console.log('resizeWorkspace',vars);
    let w = $(window).width();
    let h = $(window).height();
    let hModalConfigPub = $(".modalConfig .modalInner").outerHeight();

    $("#modalPubForm .form").outerHeight(hModalConfigPub);
}

export const insertPub = (vars = []) => {
    //console.log('insertPub', vars);
    const form = vars.form;
    let name = $("#" + form + " #inputName").val();
    let path = $("#" + form + " #inputPath").val();
    let cont = 0;

    if (name == "") {
        errorInput("inputName", "Debe ingresar un nombre");
        cont++;
    }
    if (path == "") {
        renderHelperText({
            elem: "#messagePub",
            message: "Debe elegir un tipo de publicación"
        });
        cont++;
    }

    if (cont == 0) {
        savePublication({
            inputName: name,
            inputTitle: $("#" + form + " #inputTitle").val(),
            inputDescription: $("#" + form + " #inputDescription").val(),
            inputPath: path,
            inputPathTpl: $("#" + form + " #inputPathTpl").val(),
            inputPathUI: $("#" + form + " #inputPathUI").val(),
            inputType: $("#" + form + " #inputType").val(),
            inputCls: $("#" + form + " #inputCls").val(),
            inputAttrId: $("#" + form + " #inputAttrId").val(),
            inputJson: $("#" + form + " #inputJson").val(),
            inputHtml : $("#" + form + " #inputHtml").val(),
            inputState: $("#" + form + " #inputState").val()
        }).then((response) => {
            //console.log('savePublication', response);

        }).catch(console.warn());
    }
}

export const editPublication = (vars = []) => {
    console.log('editPublication', vars);
    let str = '';
    let attr = vars.attr ? vars.attr : '';
    let type = vars.pub.type ? vars.pub.type : 0;
    let urlPubOrigin = '';
    let urlPathType = '';
    if (type == 'host') {
        // urlPubOrigin = _PATH_WEB + vars.pub.path.replace('.php', '.json');
        urlPathType = _PATH_WEB;
    }
    if (type == 'nucleo') {
        // urlPubOrigin = _PATH_WEB_NUCLEO + vars.pub.path.replace('.php', '.json');
        urlPathType = _PATH_WEB_NUCLEO;
    }

    //console.log("type", type, urlPathType);
    // console.log("urlPubOrigin", urlPubOrigin);
    let dataPubOrigin = '';

    getData({
        task: 'getPubsOrigin',
        return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
    }).then((responsePubsOrigin) => {
        console.log('getData getPubsOrigin', responsePubsOrigin);
        if (responsePubsOrigin.status == 'success') {
            loadView(_PATH_WEB_NUCLEO + "modules/websites/views/formPubEditModal.html?" + _VS).then((response) => {
                loadView(_PATH_WEB_NUCLEO + "modules/websites/views/formPubEdit.html?" + _VS).then((responseFormPub) => {
                    //console.log('loadView', response);
                    let pathPub = vars.pub.path;
                    dataPubOrigin = responsePubsOrigin.data.find(pub => pub.path == pathPub).headers;
                    let pathIconOrigin = '';

                    //console.log('dataPubOrigin', dataPubOrigin);

                    str = replaceEssentials({
                        str: response,
                        item: vars.item,
                        form: 'formPubEdit',
                    });

                    let strFromPub = responseFormPub.replace(/\{{_FORM}}/g, 'formPubEdit');
                    strFromPub = strFromPub.replace(/\{{_BTN_ACTION_ATTR}}/g, '');

                    str = str.replace(/\{{_EDIT_PUBLICATION}}/g, strFromPub);
                    str = str.replace(/\{{_BTN_ACTION_CLS}}/g, 'btnUpdatePublication');
                    str = str.replace(/\{{_BTN_ACTION_LABEL}}/g, 'Actualizar Publicación');


                    //console.log('vars.pub', vars.pub);
                    //console.log('data', vars.pub, dataPubOrigin);
                    //console.log('pathUI', vars.pub.pathUI);

                    getData({
                        task: 'getUI',
                        return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
                        id: vars.pub.id
                    }).then((responseUI) => {
                        console.log('getData getUI', responseUI);

                        if(responseUI.status=='success'){
                             
                            if (vars.pub.pathUI == '' || vars.pub.pathUI == null) {
                                str = str.replace(/\{{_PANEL_UI}}/g, '');
                            } else {
                                //console.log('pathUI pub', vars.pub);
                                
                                //getScript(urlPathType + vars.pub.pathUI, 'head');

                               getDataUI({
                                    url: _PATH_WEB + vars.pub.pathUI,
                                    id: vars.pub.id,
                                }).then((responseViewUI) => {
                                    console.log('getDataUI ukt', responseViewUI);
                                    if(responseViewUI!=''){
                                        str = str.replace(/\{{_PANEL_UI}}/g, `<div id="formPathUIPub" 
                                        data-id = "${vars.pub.id}"
                                        data-name = "${vars.pub.name}" ${vars.pub.attr} 
                                        data-attrId = "${vars.pub.attrId}"
                                        data-count = "${vars.pub.count}"
                                        data-cls = "${vars.pub.cls}"
                                        data-json = '${vars.pub.json}'>${responseViewUI}</div>`);
                                    } 


                                    addHtml({
                                        elem: ".boxModule[module=" + vars.module + "]",
                                        type: "prepend",
                                        html: renderModalConfig({
                                            module: vars.module,
                                            system,
                                            form: 'formPubEdit',
                                            id: "modalEditPub",
                                            item: vars.item,
                                            attr,
                                            innerCls: "animated fadeIn",
                                            body: str
                                        })
                                    })
                
                                    if (vars.pub.pathUI == '' || vars.pub.pathUI == null) {
                                        $(".tab[item='editPublication']").addClass('active');
                                        $(".tabContent[item='editPublication']").addClass('active');
                                        $(".tab[item='panelUI']").hide();
                                        $(".tabContent[item='panelUI']").hide();
                                    }
                
                                    if (dataPubOrigin.pathIcon != '') {
                                        pathIconOrigin = _PATH_WEB + dataPubOrigin.pathIcon;
                                    } else {
                                        pathIconOrigin = _PATH_WEB_NUCLEO + 'modules/assets/img/default.pub.svg';
                                    }
                
                
                                    $("#formPubEdit #fieldPubSelected").html(pubElem({
                                        name: dataPubOrigin.name,
                                        description: dataPubOrigin.description + '[path:' + vars.pub.path + ']',
                                        item: '',
                                        cls: 'on',
                                        pathFile: '',
                                        pathFileUI: '',
                                        pathTpl: '',
                                        pathIcon: pathIconOrigin
                                    }))
                
                                    $("#formPubEdit #inputId").val(vars.pub.id);
                                    $("#formPubEdit #inputName").val(vars.pub.name);
                                    $("#formPubEdit #inputTitle").val(vars.pub.title);
                                    $("#formPubEdit #inputDescription").val(vars.pub.description);
                                    $("#formPubEdit #inputPath").val(vars.pub.path);
                                    $("#formPubEdit #inputPathTpl").val(vars.pub.pathTpl);
                                    $("#formPubEdit #inputPathUI").val(vars.pub.pathUI);
                                    $("#formPubEdit #inputType").val(vars.pub.type);
                                    $("#formPubEdit #inputCls").val(vars.pub.cls);
                                    $("#formPubEdit #inputAttrId").val(vars.pub.attrId);
                                    $("#formPubEdit #inputAttr").val(vars.pub.attr);
                                    $("#formPubEdit #inputCount").val(vars.pub.count);
                                    $("#formPubEdit #inputJson").val(vars.pub.json);
                                    $("#formPubEdit #inputHtml").val(vars.pub.html);
                                    $("#formPubEdit #inputState").val(vars.pub.state);
                
                                    
                
                                    resizePublications();



                                }).catch(console.warn());
                                
                                
                            }
        
                            

                        }else{
                             alertMessageError({message: response.message})
                        }
                    }).catch(console.warn());

                    


                }).catch(console.warn());
            }).catch(console.warn());
        } else {
            alertMessageError({
                message: response.message
            })
        }
    }).catch(console.warn());



}

export const getDataUI = async (vars = []) => {
   console.log('getDataUI',vars.url);
   const url = vars.url;
   let data = JSON.stringify({
       accessToken: accessToken(),
       vars: JSON.stringify(vars)
   });
   try {
       let res = await axios({
           async: true,
           method: "post",
           responseType: "text",
           url: url,
           headers: {},
           data: data
        })
       //console.log(res.data);
       jointActions(res.data)
       return res.data
   } catch (error) {
      console.log("Error:getDataUI  ", error);
   }
}

export const formSavePublication = (vars = []) => {
    console.log('formSavePublication', vars);
    let form = vars.form ? vars.form : 'formNewPub';
    let name = $(`#${form} #inputName`).val();
    let count = 0;

    if (name == "") {
        errorInput("inputName", "Debe ingresar un nombre");
        count++;
        $("body").on("change", `#${form} #inputName`, function (e) {
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            $(this).remove('error');
        })
    }

    if (count == 0) {
        getData({
            task: 'savePublication',
            return: 'returnId', // returnId, returnState, returnArray
            inputs: JSON.stringify($('#' + form).serializeArray())
        }).then((response) => {
            console.log('savePublication', response);
            if (response.status == "success") {
                removeInnerForm();
                publicationsIndex();
            } else {
                alertMessageError({
                    message: response.message
                });
            }
        }).catch(console.warn());
    }
}

// fn async 
export const dataPublications = async (vars = []) => {
    //console.log('getPubsSiteId', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/publications.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "dataPublications",
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);

        return res.data;

    } catch (error) {
        //console.log("Error: dataPublications")
        //console.log(error)
    }
}

export const getTypesPubsSiteId = async (vars = []) => {
    //console.log('getPubsSiteId', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/publications.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "getPubsTypesSiteId",
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        console.log(res.data);
        //console.log(getPubListSelect(res.data));

        return getPubListSelect(res.data);

    } catch (error) {
        //console.log("Error: getPubsSiteId")
        //console.log(error)
    }
}

export const duplicatePublication = async (vars = []) => {
    //console.log('duplicatePublication', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/publications.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "duplicatePublication",
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        //console.log("Error: duplicatePublication")
        //console.log(error)
    }
}

export const getCreateNewPub = (vars = []) => {
    //console.log('getCreateNewPub', vars);
    let form = vars.form ? vars.form : 'formNewPub';

    getData({
        task: 'getPubsOrigin',
        return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
    }).then((response) => {
        console.log('getData getPubsOrigin', response);
        let data = response.data;
        let pubsOrigin = '';

        if (data != 0) {
            for (let i = 0; i < data.length; i++) {
                let elm = data[i].headers;
                let path = data[i].path;
                console.log("elm", elm);
                pubsOrigin += pubElem({
                    name: elm.name,
                    item: elm.id,
                    path,
                    pathUI: elm.pathUI,
                    pathTpl: elm.pathTpl,
                    cls: elm.cls,
                    description: elm.description,
                    type: elm.type,
                    pathIcon: elm.pathIcon,

                }, i);
            }
        }

        console.log("pubsOrigin",pubsOrigin);

        if (response.status == 'success') {
            loadView(_PATH_WEB_NUCLEO + "modules/websites/views/formPubModal.html?" + _VS).then((responseView) => {
                //console.log('loadView', responseView);
                let content = responseView;

                content = content.replace(/\{{_LIST_PUBS}}/g, pubsOrigin);
                content = content.replace(/\{{_FORM_MODAL}}/g, "modalPubNewModal");
                content = content.replace(/\{{_FORM}}/g, form);
                content = content.replace(/\{{_FORM_SELECT}}/g, 'formSelectedPub');
                content = content.replace(/\{{_BTN_ACTION_LABEL}}/g, 'Guardar e Insertar Pub.');
                content = content.replace(/\{{_BTN_ACTION_CLS}}/g, 'btnSaveNewPub');

                addHtml({
                    selector: `body`,
                    type: 'prepend',
                    content: renderModalClean({
                        title: 'Nueva Publicación',
                        id: 'modalPubNewModal',
                        cls: 'modalExtendedMedia',
                        body: content
                    }) // insert, append, prepend, replace
                })
            }).catch(console.warn());
        } else {
            alertMessageError({
                message: response.message
            })
        }
    }).catch(console.warn());


}

export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/publications.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("Error: getData", error)
    }
}

document.addEventListener("DOMContentLoaded", function () {

    $("body").on("keyup", "#inputSearchInPubList", function () {
        var id = $(this).attr("for");
        //console.log(id);
        var rex = new RegExp($(this).val(), 'i');
        $('#' + id + ' .listPublications .pubElem').hide();
        $('#' + id + ' .listPublications .pubElem').filter(function () {
            return rex.test($(this).text());
        }).show();
    });

    $("body").on("click", ".btnPubElem", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let form = $(this).parent().attr("for");
        let item = $(this).attr("item");

        let wsId = $("#modalPubForm[form='" + form + "']").attr("ws");
        let blockId = $("#modalPubForm[form='" + form + "']").attr("block");
        let catId = $("#modalPubForm[form='" + form + "']").attr("cat");
        let module = $("#modalPubForm[form='" + form + "']").attr("module");
        let level = $("#modalPubForm[form='" + form + "']").attr("level");
        let name = data.name;
        //console.log('.btnPubElem',  data, form, item, name, wsId, blockId, catId, module);

        if (form == "formNewPub") {

            $(".pubElem").removeClass("active");
            $(this).addClass("active");

            $("#" + form + " #inputName").val(data.name);
            $("#" + form + " #inputTitle").val(data.title);
            $("#" + form + " #inputDescription").val(data.description);
            $("#" + form + " #inputPath").val(data.path);
            $("#" + form + " #inputPathUI").val(data.pathui);
            $("#" + form + " #inputPathTpl").val(data.pathtpl);
            $("#" + form + " #inputCls").val(data.cls);
            $("#" + form + " #inputType").val(data.type);
            $("#" + form + " #inputPathIcon").val(data.pathicon);

        }

        if (form == "formEditPub") {}

        if (form == "formSelectedPub") {
            //console.log('.btnPubElem', form, item, name, title, wsId, blockId, catId, module);
            removeModal({
                timer: 0,
                animation: ''
            });
            let content = renderWorksheetElem({
                wsId,
                blockId,
                catId,
                module,
                item,
                level,
                state: 1,
                label: name,
                animation: 'animated bounceIn',
                type: 'publication'
            });

            // //console.log("root", `.wsElem[type='block'][ws='${wsId}'][cat='${catId}'][item='${blockId}'] .body`);
            addHtml({
                selector: `.wsElem[type='block'][ws='${wsId}'][cat='${catId}'][item='${blockId}'] >.body`,
                type: 'append',
                content
            }) //type: html, append, prepend, before, after


        }

    });

    $("body").on("contextmenu",`.btnPubElem`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnPubElem contextmenu`,data);

        addHtml({
           selector:`body`,
           type: 'prepend', //insert, append, prepend, replace
           content: renderContextMenu({
                module, 
                id:'modalContextMenuPub',
                array: [
                    {
                        label: "Editar",
                        icon: "icon icon-pencil",
                        cls : "btn btnIconLeft btnEditPubSelectMenu",
                        type: "btn",
                    },
                    { type: "separator"},
                    {
                        label: "Eliminar",
                        type: "btn",
                        cls: "btn btnDanger btnDeletePubSelectMenu",
                        attr: `data-id="${data.item}" data-name="${data.name}"`,
                        icon: "icon icon-trash",
                    }
                ]
            })
        });

        var mouseX = event.pageX;
        var mouseY = event.pageY;

        $("#modalContextMenuPub").css({
            top: mouseY + "px",
            left: mouseX + "px"
        }).show();
        
    });

    $("body").on("mouseleave",`#modalContextMenuPub`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        $(this).remove();
    });

    $("body").on("click",`.btnDeletePubSelectMenu`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnDeletePubSelectMenu`,data);

        deleteModal({
            id: "deleteModalSelectMenu",
            name: data.name,
            cls: "btnDeleteConfirmationSelectPub",
            attr: `data-id="${data.id}"`,
        })
    });

    $("body").on("click", `.btnDeleteConfirmationSelectPub`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let id = $(this).attr("data-id");
 
        getData({
            task: 'delete',
            return: "returnState",
            id
        }).then((response) => {
            console.log('delete', response);
            if (response.status == 'success') {
                 $(".listPublications[for='formSelectedPub'] .pubElem[item='"+id+"']").remove();
                 removeModalId("deleteModalSelectMenu");
            } else {
                alertMessageError({
                    message: response.message
                });
            }
        }).catch(console.warn());
    })

    $("body").on("click", ".btnInsertPub", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let form = $(this).attr("form");
        let blockId = $(this).attr("block");
        let catId = $(this).attr("cat");
        let wsId = $(this).attr("ws");
        //console.log('.btnInsertPub', blockId, form);

        insertPub({
            form,
            blockId,
            catId,
            wsId
        });

    });

    $("body").on("click", `.btnNewPub`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`.btnNewPub`, data.form);
        getCreateNewPub({
            'form': data.form
        });
    })

    $("body").on("click", `.btnDelete[data-fn='delete${capitalize(tableId)}']`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        //console.log(`.btnDelete[data-fn='delete${capitalize(tableId)}']`, );

        let id = $(this).attr("data-id");
        let name = $(this).attr("data-name");

        //console.log(`.btnDelete`, );

        addHtml({
            selector: ".workspace",
            type: 'prepend',
            content: renderDeleteModal({
                name,
                id: `modalDelete${capitalize(tableId)}`,
                attr: `data-fn="delete${capitalize(tableId)}" data-id="${id}"`,
            })
        }) //type: html, append, prepend, before, after

    })

    $("body").on("click", `.btnDeleteConfirmation[data-fn='delete${capitalize(tableId)}']`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let id = $(this).attr("data-id");
        //console.log(`.btnDeleteConfirmation[data-fn='${capitalize(tableId)}']`, );
        getData({
            task: 'delete',
            return: "returnState",
            id
        }).then((response) => {
            //console.log('delete', response);
            if (response.status == 'success') {
                removeItemTable(id, tableId);
                removeModalId(`modalDelete${capitalize(tableId)}`);
            } else {
                alertMessageError({
                    message: response.message
                });
            }
        }).catch(console.warn());
    })

    $("body").on("click", `.btnAdd`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        console.log(`.btnAdd`, );
        formNewPub();
    })

    //save pub
    $("body").on("click", `.btnActionForm[data-form='formNewPub']`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let form = $(this).attr("data-form");
        console.log(`.btnSavePub`, );

        formSavePublication({
            form
        });

    })

    $("body").on("click", `.btnSaveNewPub`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnSaveNewPub`, data);
        let cont = 0;
        let selector = '.formNewPubOrigin';
        const formSelect = data.formSelect;

        let wsId = $("#modalPubForm[form='" + formSelect + "']").attr("ws");
        let blockId = $("#modalPubForm[form='" + formSelect + "']").attr("block");
        let catId = $("#modalPubForm[form='" + formSelect + "']").attr("cat");
        let module = $("#modalPubForm[form='" + formSelect + "']").attr("module");
        let level = $("#modalPubForm[form='" + formSelect + "']").attr("level");

        const inputName = $(`${selector} #inputName`).val();
        const inputDescription = $(`${selector} #inputDescription`).val();
        const inputTitle = $(`${selector} #inputTitle`).val();
        const inputType = $(`${selector} #inputType`).val();
        const inputSummary = $(`${selector} #inputSummary`).val();
        const inputPath = $(`${selector} #inputPath`).val();
        const inputPathUI = $(`${selector} #inputPathUI`).val();
        const inputPathTpl = $(`${selector} #inputPathTpl`).val();
        const inputPathIcon = $(`${selector} #inputPathIcon`).val();
        const inputCls = $(`${selector} #inputCls`).val();
        const inputAttrId = $(`${selector} #inputAttrId`).val();
        const inputAttr = $(`${selector} #inputAttr`).val();
        const inputCount = $(`${selector} #inputCount`).val();
        const inputJson = $(`${selector} #inputJson`).val();
        const inputHtml = $(`${selector} #inputHtml`).val();
        const inputState = $(`${selector} #inputState`).val();


        if (inputName == "") {
            errorInput("inputName", "Debe ingresar un nombre");
            cont++;
        }

        if (cont == 0) {
            getData({
                task: 'savePublication',
                return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
                inputs: {
                    inputName,
                    inputDescription,
                    inputTitle,
                    inputType,
                    inputSummary,
                    inputPath,
                    inputPathUI,
                    inputPathTpl,
                    inputPathIcon,
                    inputCls,
                    inputAttrId,
                    inputAttr,
                    inputCount,
                    inputJson,
                    inputHtml,
                    inputState
                }
            }).then((response) => {
                console.log('getData saveNewPub', response);
                if (response.status == 'success') {
                    let item = response.data.pubId;
                    removeModal({
                        timer: 0,
                        animation: ''
                    });
                    let content = renderWorksheetElem({
                        wsId,
                        blockId,
                        catId,
                        module,
                        item,
                        level,
                        state: 1,
                        label: inputName,
                        animation: 'animated bounceIn',
                        type: 'publication'
                    });

                    //console.log("root", `.wsElem[type='block'][ws='${wsId}'][cat='${catId}'][item='${blockId}'] .body`, content);
                    addHtml({
                        selector: `.wsElem[type='block'][ws='${wsId}'][cat='${catId}'][item='${blockId}'] >.body`,
                        type: 'append',
                        content
                    }) //type: html, append, prepend, before, after

                } else {
                    alertMessageError({
                        message: response.message
                    })
                }
            }).catch(console.warn());
        }
    });

    $(window).resize(function () {
        resizePublications();
    })

    $("body").on("click",`.btnUpdatePublication`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let cont = 0;
        const form = data.form;
        //console.log(`.btnUpdatePublication`,data, form);
        

        const inputId = $(`#${form} #inputId`).val();
        const inputName = $(`#${form} #inputName`).val();
        const inputDescription = $(`#${form} #inputDescription`).val();
        const inputTitle = $(`#${form} #inputTitle`).val();
        const inputType = $(`#${form} #inputType`).val();
        const inputSummary = $(`#${form} #inputSummary`).val();
        const inputPath = $(`#${form} #inputPath`).val();
        const inputPathUI = $(`#${form} #inputPathUI`).val();
        const inputPathTpl = $(`#${form} #inputPathTpl`).val();
        const inputPathIcon = $(`#${form} #inputPathIcon`).val();
        const inputCls = $(`#${form} #inputCls`).val();
        const inputAttrId = $(`#${form} #inputAttrId`).val();
        const inputAttr = $(`#${form} #inputAttr`).val();
        const inputCount = $(`#${form} #inputCount`).val();
        const inputJson = $(`#${form} #inputJson`).val();
        const inputHtml = $(`#${form} #inputHtml`).val();
        const inputState = $(`#${form} #inputState`).val();


        if (inputName == "") {
            errorInput("inputName", "Debe ingresar un nombre");
            cont++;
        }

        if (cont == 0) {
            
            getData({
                task: 'update',
                return: 'returnState', 
                inputs: {
                    inputId,
                    inputName,
                    inputDescription,
                    inputTitle,
                    inputType,
                    inputSummary,
                    inputPath,
                    inputPathUI,
                    inputPathTpl,
                    inputPathIcon,
                    inputCls,
                    inputAttrId,
                    inputAttr,
                    inputCount,
                    inputJson,
                    inputHtml,
                    inputState
                }
            }).then((response) => {
                console.log('getData updatePub', response);
                if (response.status == 'success') {
                    removeModal({
                        timer: 0,
                        animation: ''
                    });

                    dataPublications().then((responsePubs) => {
                        //console.log('responsePubs', responsePubs);
                        localStorage.setItem("_PUBLICATIONS", JSON.stringify(responsePubs.data));
                    })

                } else {
                    alertMessageError({
                        message: response.message
                    })
                }
            }).catch(console.warn());
        }

        
    });
});