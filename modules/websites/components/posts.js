import {
    empty,
    replacePath,
    replaceEssentials,
    replaceAll,
    loadView,
    stopLoadingBar,
    emptyReturn,
    loadingBtnIcon,
    btnLoadingRemove,
    unDisableId,
    accessToken,
    removeBtnLoading,
    alertMessageError,
    dataModule,
    jointActions,
    addHtml,
    capitalize,
    activeSelector,
    unactiveSelector,
    loadingBar
} from "../../components/functions.js";

import {
    editorText,
    errorInput,
    errorInputSelector,
    loadInitChargeMedia,
    loadItemMedia,
    loadFilesFormLoader,
    loadSingleFileFormLoader,
    loadChargeMediaInit,
    removalMediaItems,
    ckeckboxChecked,
    stateBtnSwitch
} from "../../components/forms.js";

import {
    deleteModalItem,
    removeModal,
    removeModalId,
    closeModal,
    resizeModalConfig,
    deleteModal,
} from "../../components/modals.js";

import {
    categorysListTable,
    createTable,
    dataTable,
    removeItemTable,
    inactiveBtnStateItem,
    activeBtnStateItem,
    mountTable,
    colCheck,
    colId,
    colTitle,
    colState,
    colActionsBase,
    deleteItemModule,
    renderEmpty,
    mountMacroTable,
    clearMacroTable
} from "../../components/tables.js";

import {
    renderCol,
    renderColCheck,
    renderColId,
    renderColTitle,
    renderRowsTable,
    renderColState,
    renderColActions,
    renderColCategorys,
    renderColCheckboxState,
    renderColTitlePath
} from "../../components/renders/renderTables.js";

import {
    innerForm,
    renderDeleteModal,
    renderModalConfig,
    renderModalConfigBody,
    renderModalClean,
    removeInnerForm
} from "../../components/renders/renderModals.js";


import {
    renderCheckBox,
    renderPill,
    renderItemVideo,
    renderItemImage
} from "../../components/renders/renderForms.js";

import {
    getCategorys,
    listCategorys,
    checkCategorys
} from "./categorys.js";

import {
    getData as getPublications
} from "./publications.js";

import * as renderPosts from './renders/renderPosts.js';

let pathurl = "modules/websites/";
var module = "posts";
let system = "websites";
let tableId = "tablePosts";

export const postsIndex = () => {
    
    loadView(_PATH_WEB_NUCLEO + pathurl + "views/posts.html?" + _VS).then(
        (htmlView) => {
            $(".bodyModule[module='" + module + "']").html(
                replaceEssentials({
                    str: htmlView,
                    module,
                    system,
                    pathurl,
                    fn: "formNewPosts",
                    color: dataModule(module, "color"),
                })
            );
            $(".bodyModule[module='" + module + "']  .tbody").html('<div class="loading loadingFluidTop"></div>');
        
            getData({
                task: 'getPosts',
                return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
            }).then((response) => {
                console.log('getData getPosts', response);
                if (response.status == 'success') {

                    if (response.Error == 0 && response.data != 0) {

                        let rows = "";
                        for (let i in response.data) {
                            let item = response.data[i];
                            let author = "";

                            if (item.author != 0 && item.author != undefined) {
                                author = item.author[0].name;
                            } else {
                                author = "";
                            }
                            // console.log("item", item);
                            rows += renderRowsTable({
                                id: item.id,
                                content: renderColCheck({
                                        id: item.id,
                                    }) +
                                    renderColId({
                                        id: item.id,
                                    }) +
                                    renderColTitlePath({
                                        id: item.id,
                                        title: item.title + ` <a class="shortlink" path-item="${item.id}"><i class="icon icon-link"></i></a>`,
                                        link: _PATH_WEB + item.link,
                                    }) +
                                     renderCol({
                                        id: item.id,
                                        data: item.tags,
                                    }) +
                                    renderCol({
                                        id: item.id,
                                        data: author,
                                    }) +
                                    renderCol({
                                        id: item.id,
                                        data: item.date,
                                    }) +
                                    renderColCategorys({
                                        id: item.id,
                                        fn: "categorysListTable",
                                        array: item.categorys,
                                    }) +
                                    renderColState({
                                        id: item.id,
                                        state: item.state,
                                        fn: "changeStatePost",
                                        module,
                                        system,
                                    }) +
                                    renderColActions({
                                        id: item.id,
                                        name: item.title,
                                        type: "btn,btn",
                                        fnType: "btnEditItemPost,btnDeleteItemPost",
                                        attr: `data-id="${item.id}" data-module="${module}" data-system="${system}" data-title="${item.title}"`
                                    }),
                            });
                        }

                        mountTable({
                            id: tableId,
                            columns: [
                                ...colCheck,
                                ...colId,
                                ...colTitle,
                                {
                                    label: "Tags",
                                    cls: "colTags",
                                },
                                {
                                    label: "Autor",
                                    cls: "colAuthors",
                                }, {
                                    label: "Fecha",
                                    cls: "colDate",
                                },
                                {
                                    label: "Categorias",
                                    cls: "colCategorys",
                                },
                                ...colState,
                                ...colActionsBase,
                            ],
                            rows,
                            module,
                            system,
                            container: ".bodyModule[module='" + module + "'] .tbody",
                        });


                    } else if (response === 0 || response.data == 0) {
                        $(".bodyModule[module='" + module + "']  .tbody").html(renderEmpty())
                    } else {
                        alertPage({
                            text: "Error. por favor contactarse con soporte. " +
                                response.message,
                            icon: "icn icon-alert-warning",
                            animation_in: "bounceInRight",
                            animation_out: "bounceOutRight",
                            tipe: "danger",
                            time: "3500",
                            position: "top-left",
                        })
                    }
                } else {
                    alertMessageError({
                        message: response.message
                    })
                }
            }).catch(console.warn());
            stopLoadingBar();
            $(".innerForm").remove();


        }).catch(console.warn());
}

export const searchTablePosts = (vars =[]) => {
    console.log('searchTablePosts',vars);
    getData({
        task: 'searchPost',
        return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
        inputs: {
            inputSearch: vars.search
        }
    }).then((response) => {
        console.log('getData searchPost ', response);
        if (response.status == 'success') {
            let data = response.data;
            console.log("table", tableId);
            // Paso 2: Define la función externa 
            if (data.length > 0) {

                let rows = "";
                        for (let i in response.data) {
                            let item = response.data[i];
                            let author = "";

                            if (item.author != 0 && item.author != undefined) {
                                author = item.author[0].name;
                            } else {
                                author = "";
                            }
                            // console.log("item", item);
                            rows += renderRowsTable({
                                id: item.id,
                                content: renderColCheck({
                                        id: item.id,
                                    }) +
                                    renderColId({
                                        id: item.id,
                                    }) +
                                    renderColTitlePath({
                                        id: item.id,
                                        title: item.title,
                                        link: _PATH_WEB + item.link,
                                    }) +
                                    renderCol({
                                        id: item.id,
                                        data: author,
                                    }) +
                                    renderCol({
                                        id: item.id,
                                        data: item.date,
                                    }) +
                                    renderColCategorys({
                                        id: item.id,
                                        fn: "categorysListTable",
                                        array: item.categorys,
                                    }) +
                                    renderColState({
                                        id: item.id,
                                        state: item.state,
                                        fn: "changeStatePost",
                                        module,
                                        system,
                                    }) +
                                    renderColActions({
                                        id: item.id,
                                        name: item.title,
                                        type: "btn,btn",
                                        fnType: "btnEditItemPost,btnDeleteItemPost",
                                        attr: `data-id="${item.id}" data-module="${module}" data-system="${system}" data-title="${item.title}"`
                                    }),
                            });
                        }
                
                let tableTemp = `<table class="table">${rows}</table>`;

                $(".table-responsive[data-id='" + tableId + "'] .table-wrapper-inner").html(`${tableTemp}`);
                $(".table-responsive[data-id='" + tableId + "'] .table-wrapper-inner").addClass("active");
                 
            } else {
                $(".table-responsive[data-id='" + tableId + "'] .table-wrapper-inner").html("");
                $(".table-responsive[data-id='" + tableId + "'] .table-wrapper-inner").removeClass("active");
            }
 
        }else if (response === 0 || response.data == 0) {
            console.log("No data");
        }else{
             alertMessageError({message: response.message})
        }
    }).catch(console.warn());
}

export const formNewPosts = (vars = []) => {
    //console.log('formNewPosts', vars);
    getData({
        task: 'getDataPosts',
        return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
    }).then((response) => {
        console.log('getData getDataPosts', response);
        if (response.status == 'success') {
            let categorys = response.data.categorys;
            let date = response.data.date;
            loadView(_PATH_WEB_NUCLEO + "modules/websites/views/formPost.html?" + _VS).then((responseView) => {
                //console.log('loadView', responseView);
                let str = responseView;
                str = str.replace(/{{_MODULE}}/g, module);
                str = str.replace(/{{_SYSTEM}}/g, system);
                str = str.replace(/{{_FORM_ID}}/g, "newFormPost");
                str = str.replace(/{{_ITEM}}/g, "");
                str = str.replace(/{{_BTN_ACTION_1}}/g, "btnNewPost");
                str = str.replace(/{{_CATEGORYS}}/g, listCategorys(categorys));
                str = str.replace(
                    /{{_CLS_BTN_ACTION}}/g,
                    "btnNewPost btnInfo"
                );
                str = str.replace(/{{_BTN_NAME_ACTION_1}}/g, "Guardar");
                str = str.replace(/{{_BTN_NAME_ACTION_2}}/g, "Publicar");
                str = str.replace(/{{_BTNS_TOP}}/g, "<a class='btn btnPrimary btnSaveNewPost' id='btnSaveNewPost' data-form='newFormPost' ><span>Guardar</span></a>");
                str = str.replace(/{{_BTN_ACTION_2}}/g, "btnPublishNewPost");
                str = str.replace(/{{_TITLE}}/g, "Nuevo Post");
                innerForm({
                    module,
                    body: str,
                });

                // editorText("inputSummary");
                editorText("inputBody");

                jQuery.datetimepicker.setLocale("es");

                $("#newFormPost #inputDate").val(date);
                $("#newFormPost #inputDate").datetimepicker({
                    timepicker: false,
                    datepicker: true,
                    format: "Y-m-d H:i",
                    lang: "es",
                });

            }).catch(console.warn());
        } else {
            alertMessageError({
                message: response.message
            })
        }
    }).catch(console.warn());
}

export const formEditPosts = (vars = []) => {
    //console.log('formNewPosts', vars);
    getData({
        task: 'getDataPostId',
        return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
        item: vars.id
    }).then((response) => {
        console.log('getData getDataPostId', response);
        if (response.status == 'success') {
            let form = 'editFormPost';
            let categorys = response.data.categorys;
            let listCategorysArray = response.data.listCategorys;

            let files = response.data.media;
            let img = response.data.img;
            let video = response.data.video;
            let date = response.data.date;

            //console.log("categorys", categorys);
            loadView(_PATH_WEB_NUCLEO + "modules/websites/views/formPost.html?" + _VS).then((responseView) => {
                //console.log('loadView', responseView);
                let str = responseView;
                str = str.replace(/{{_MODULE}}/g, module);
                str = str.replace(/{{_SYSTEM}}/g, system);
                str = str.replace(/{{_FORM_ID}}/g, form);
                str = str.replace(/{{_ITEM}}/g, "");
                str = str.replace(/{{_BTNS_TOP}}/g, "");

                str = str.replace(/{{_CATEGORYS}}/g, listCategorys(listCategorysArray));
                str = str.replace(
                    /{{_CLS_BTN_ACTION}}/g,
                    "btnNewPost btnInfo"
                );
                str = str.replace(/{{_BTN_NAME_ACTION_1}}/g, "Guardar");
                str = str.replace(/{{_BTN_NAME_ACTION_2}}/g, "Actualizar");
                str = str.replace(/{{_BTN_ACTION_1}}/g, "btnSavePost");
                str = str.replace(/{{_BTN_ACTION_2}}/g, "btnUpdatePost");
                str = str.replace(/{{_TITLE}}/g, "Edigar Post");
                innerForm({
                    module,
                    body: str,
                });

                $("#" + form + " #inputId").val(response.data.id);
                $("#" + form + " #inputTitle").val(response.data.title);
                $("#" + form + " #inputSummary").val(response.data.review);
                $("#" + form + " #inputState").val(response.data.state);
                $("#" + form + " #inputDate").val(date);
                $("#" + form + " #inputImgRef").val(response.data.imgRef);

                $("#" + form + " #inputEmbed").html(response.data.embed);
                $("#" + form + " #previewEmbed").html(response.data.previewEmbed);
                $("#" + form + " #inputInternEmbed").val(response.data.internEmbed);

                stateBtnSwitch({
                    form: form,
                    id: "inputPreviewEmbed",
                    value: response.data.previewEmbed
                });


                if (video != 0) {
                    let videoId = video.id;
                    $("#" + form + " #inputVideo").val(video.id);


                    $("#" + form + " #previewVideo").html(renderItemVideo({
                        item: videoId,
                        id: "inputTypeFile",
                        pathFile: _PATH_FILES + video.pathurl,
                    }));

                    $(".formUpFile .inner[for='inputTypeFile']").addClass("off");

                    stateBtnSwitch({
                        form: form,
                        id: "inputPreviewVideo",
                        value: response.data.previewVideo
                    });

                    stateBtnSwitch({
                        form: form,
                        id: "inputInternVideo",
                        value: response.data.internVideo
                    });
                }

                if (img != 0) {
                    let imgId = img.id;
                    $("#" + form + " #inputImage").val(imgId);
                    $("#" + form + " #previewImg").html(renderItemImage({
                        item: imgId,
                        id: "inputTypeImgFile",
                        pathFile: _PATH_FILES + img.pathurl,
                    }));

                    $(".formUpFile .inner[for='inputTypeImgFile']").addClass("off");
                }

                $("#" + form + " #inputBody").val(response.data.body);
                $("#" + form + " #inputTags").val(response.data.tags);
                $("#" + form + " #inputClass").val(response.data.cls);
                $("#" + form + " #inputJson").val(response.data.json);

                $("#" + form + " #inputPathurl").val(response.data.pathurl);

                loadFilesFormLoader({
                    id: "inputFilesMedia",
                    files,
                });

                /* loadSingleFileFormLoader({
                    id: "inputImageCharge",
                    file: img,
                }); */

                checkCategorys(categorys);

                // editorText("inputSummary");
                editorText("inputBody");

                jQuery.datetimepicker.setLocale("es")

                $("#" + form + " #inputDate").datetimepicker({
                    timepicker: false,
                    datepicker: true,
                    format: "Y-m-d H:i",
                    lang: "es",
                    timeZone: 'America/La_Paz',
                    value : date
                });

                if (response.data.author != 0) {
                    $("#" + form + " #inputAuthor").val(response.data.author[0].id);
                    insertPillAuthor({
                        name: response.data.author[0].name,
                        id: response.data.author[0].id,
                        module
                    })
                }

            }).catch(console.warn());
        } else {
            alertMessageError({
                message: response.message
            })
        }
    }).catch(console.warn());
}


export const newPost = (vars = []) => {
    console.log('newPost', vars);
    let form = vars.form;
    let count = 0;
    let state = vars.state || 1;

    let inputTitle = $(`#${form} #inputTitle`).val();
    const inputPathurl = $(`#${form} #inputPathurl`).val();
    const inputSummary = $(`#${form} #inputSummary`).val();
    const inputTags = $(`#${form} #inputTags`).val();
    let inputBody = $(`#${form} #inputBody`).val();
    const inputAuthor = $(`#${form} #inputAuthor`).val();
    const inputImg = $(`#${form} #inputImage`).val();
    const inputImgRef = $(`#${form} #inputImgRef`).val();
    const inputCls = $(`#${form} #inputClass`).val();
    const inputJson = $(`#${form} #inputJson`).val();
    const inputEmbed = $(`#${form} #inputEmbed`).val();
    const inputPreviewEmbed = $(`#${form} #inputPreviewEmbed`).val();
    const inputInternEmbed = $(`#${form} #inputInternEmbed`).val();
    const inputVideo = $(`#${form} #inputVideo`).val();
    const inputPreviewVideo = $(`#${form} #inputPreviewVideo`).val();
    const inputInternVideo = $(`#${form} #inputInternVideo`).val();
    let inputState = $(`#${form} #inputState`).val();
    const inputDate = $(`#${form} #inputDate`).val();


    if (inputTitle == "" || inputTitle == null) {
        errorInput("inputTitle", "Debe ingresar un titulo");
        count++;
    }

    //revisar que embed tenga un contendido donde empiece con <iframe y termine con </iframe>
    if (inputEmbed != "" && inputEmbed != null && inputEmbed != undefined) {
        if (inputEmbed.indexOf("<iframe") == -1) {
            errorInput("inputEmbed", "Debe tener un contenido de <iframe>");
            count++;
        }
        if (inputEmbed.indexOf("</iframe>") == -1) {
            errorInput("inputEmbed", "Debe tener un contenido de </iframe>");
            count++;
        }
    }

    if (count == 0) {

        let categorys = [];
        let countCategorys = 0;
        $("#" + form + " input[name='inputCategorys[]']").each(function (e) {
            let valorCategorys = $(this).val();
            let checkCategorys = $(this).prop("checked");
            if (checkCategorys) {
                categorys[countCategorys] = valorCategorys;
                countCategorys++;
            }
        });

        let mediaFiles = [];
        let countMediaFiles = 0;
        $("#" + form + " input[name = 'inputFilesMedia[]']").each(function (e) {
            let values = $(this).val();
            mediaFiles[countMediaFiles] = values;
            countMediaFiles++;
        });

        inputBody = inputBody.replace(/&nbsp;/g, '');
        inputBody = inputBody.replace(/'/g, '"'); 


        getData({
            task: 'saveNewPost',
            return: 'returnId', // returnId, returnState, returnArray
            inputs: {
                inputTitle,
                inputPathurl,
                inputSummary,
                inputTags,
                inputBody,
                inputAuthor,
                inputImg,
                inputImgRef,
                inputEmbed,
                inputPreviewEmbed,
                inputInternEmbed,
                inputVideo,
                inputPreviewVideo,
                inputInternVideo,
                categorys,
                mediaFiles,
                inputCls,
                inputJson,
                inputDate,
                inputState
            }
        }).then((response) => {
            console.log('saveNewPost', response);
            if (response.status == "success") {
                removeInnerForm();
                postsIndex();
            } else {
                alertMessageError({
                    message: response.message
                })
            }
        }).catch(console.warn());
    }
}

export const changeStatePost = (vars = []) => {
    console.log('changeStatePost', vars);
    const item = vars.item;
    const state = vars.state == 0 ? 1 : 0;
    getData({
        task: 'changeState',
        return: 'returnState', // returnId, returnState, returnArray, returnMessage, returnObject
        inputs: {
            inputId: item,
            inputState: state
        }
    }).then((response) => {
        console.log('getData changeState', response, "state", state);
        if (response.status == 'success') {

            if (state == 0) {
                $(".btnStateModuleItem[item='" + item + "']").attr("state", 0);
            }
            if (state == 1) {
                $(".btnStateModuleItem[item='" + item + "']").attr("state", 1);
            }
        } else {
            alertMessageError({
                message: response.message
            })
        }
    }).catch(console.warn());
}

export const cleanInputAuthorPosts = (vars = []) => {
    console.log('cleanInputAuthorPosts', vars);
    $("#inputAuthor").val("");
    $("#inputAuthorSearch").removeClass("disabled");
    $("#inputAuthorSearch").focus();
}

export const insertPillAuthor = (vars = []) => {
    console.log('insertPillAuthor', vars);
    addHtml({
        selector: "#boxLauncherPillAuthor",
        type: 'append',
        content: renderPill({
            name: vars.name,
            id: vars.id,
            clsName: 'btnEditItemAuthor',
            module: vars.module,
            fn: 'cleanInputAuthorPosts'
        }) // insert, append, prepend, replace
    });
}


export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/posts.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        jointActions(res.data)
        return res.data
    } catch (error) {
        console.log("Error: getData ", error);
    }
}

document.addEventListener("keyup", function (e) {
    //console.log("dom", e)     
    if (e.target.id == "inputAuthorSearch") {
        let value = e.target.value;
        const attrFor = 'inputAuthorSearch';
        //console.log("inputAuthorSearch", value, attrFor);

        getData({
            task: 'getAuthorSearch',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
            inputs: {
                inputName: value
            }
        }).then((response) => {
            console.log('getData getAuthorSearch', response);

            //$(".boxResults[for='" + attrFor + "'] .boxResultsInner").html("");
            //unactiveSelector(`.boxResults[for="${attrFor}"]`);

            if (response.status == 'success') {
                let num = response.data.num;
                let items = response.data.items;

                unactiveSelector(`.boxResults[for="${attrFor}"]`);
                if (num >= 1 && items != 0) {
                    activeSelector(`.boxResults[for="${attrFor}"]`);
                    let str = "";
                    items.forEach((item) => {
                        str += `<a class="item" data-id="${item.id}" data-name="${item.name}" href="javascript:void(0)">${item.name}</a>`;
                    })
                    $(".boxResults[for='" + attrFor + "'] .boxResultsInner").html(str);
                }

            } else {
                alertMessageError({
                    message: response.message
                })
            }
        }).catch(console.warn());

    }
    if (e.key == "Enter") {
        if (e.target.id == "inputName") {
            //Save Data Authors macrotable 
            const tableAutorsId = "tableAutors";
            let inputName = e.target.value;
            let inputSummary = $(`#${tableAutorsId} #inputSummary`).val();
            let inputContact = $(`#${tableAutorsId} #inputContact`).val();
            let inputState = $(`#${tableAutorsId} #inputState`).val();

            if (inputSummary != "") {
                console.log(e.target.value);

                getData({
                    task: 'addAuthorPosts',
                    return: 'returnId',
                    inputs: {
                        inputName,
                        inputSummary,
                        inputContact,
                        inputState
                    }
                }).then((responseData) => {
                    console.log('getData', responseData);

                    if (responseData.status == 'success') {
                        let id = responseData.data;
                        clearMacroTable("#tableColors");

                        $(`#${tableAutorsId} table tbody`).append(`<tr rowId="${id}">
                            <td class="colCheck"><input type="checkbox" class="inputCheckBox" data-id="${id}"></td>
                            <td class="colId">${id}</td>
                            <td class="colName">${inputName}</td>
                            <td class="colTextarea">${inputSummary}</td>
                            <td class="colTextarea">${inputContact}</td>
                            <td class="colState">${renderColCheckboxState({inputState,id,action:'changeState',fn:'fnAuthors'})}</td>
                        </tr>`);
                    } else {
                        alertMessageError({
                            message: responseData.message
                        })
                    }

                }).catch(console.warn());

            } else {
                alertMessageError({
                    message: "El campo Descripción es obligatorio"
                })
            }
        }
    }
});

document.addEventListener("DOMContentLoaded", function () {

    $("body").on("click", `.btnSaveNewPost`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let form = data.form;
        console.log(`.btnSaveNewPost`, data, form);
        newPost({
            form,
            state: '0'
        });
    });

    $("body").on("click", `.btnPublishNewPost`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let form = data.form;
        console.log(`.btnPublishNewPost`, data, form);
        newPost({
            form,
            state: '1'
        });
    });

    $("body").on("click", `.btnUpdatePost`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let form = data.form;
        let count = 0;
        console.log(`.btnUpdatePost`, data);

        const inputId = $(`#${form} #inputId`).val();
        let inputTitle = $(`#${form} #inputTitle`).val();
        const inputPathurl = $(`#${form} #inputPathurl`).val();
        const inputSummary = $(`#${form} #inputSummary`).val();
        const inputTags = $(`#${form} #inputTags`).val();
        let inputBody = $(`#${form} #inputBody`).val();
        const inputAuthor = $(`#${form} #inputAuthor`).val();
        const inputImg = $(`#${form} #inputImage`).val();
        const inputEmbed = $(`#${form} #inputEmbed`).val();
        const inputPreviewEmbed = $(`#${form} #inputPreviewEmbed`).val();
        const inputInternEmbed = $(`#${form} #inputInternEmbed`).val();
        const inputVideo = $(`#${form} #inputVideo`).val();
        const inputPreviewVideo = $(`#${form} #inputPreviewVideo`).val();
        const inputInternVideo = $(`#${form} #inputInternVideo`).val();
        const inputDate = $(`#${form} #inputDate`).val();

        const inputImgRef = $(`#${form} #inputImgRef`).val();
        const inputCls = $(`#${form} #inputClass`).val();
        const inputJson = $(`#${form} #inputJson`).val();

        const inputState = $(`#${form} #inputState`).val();

        //console.log("const inputEmbed", inputEmbed, inputVideo);
        console.log("const inputVideo", inputVideo, inputPreviewVideo, inputInternVideo);


        if (inputTitle == "" && inputTitle != undefined) {
            errorInput("inputTitle", "Debe ingresar un titulo");
            count++;
        }

        if (inputEmbed != "" && inputEmbed != undefined && inputEmbed != null) {
            if (inputEmbed.indexOf("<iframe") == -1) {
                errorInput("inputEmbed", "Debe tener un contenido de <iframe>");
                count++;
            }
            if (inputEmbed.indexOf("</iframe>") == -1) {
                errorInput("inputEmbed", "Debe tener un contenido de </iframe>");
                count++;
            }
        }

        if (count == 0) {

            let categorys = [];
            let countCategorys = 0;
            $("#" + form + " input[name='inputCategorys[]']").each(function (e) {
                let valorCategorys = $(this).val();
                let checkCategorys = $(this).prop("checked");
                if (checkCategorys) {
                    categorys[countCategorys] = valorCategorys;
                    countCategorys++;
                }
            });

            let mediaFiles = [];
            let countMediaFiles = 0;
            $("#" + form + " input[name = 'inputFilesMedia[]']").each(function (e) {
                let values = $(this).val();
                mediaFiles[countMediaFiles] = values;
                countMediaFiles++;
            });

            inputBody = inputBody.replace(/&nbsp;/g, '');
            inputBody = inputBody.replace(/'/g, '"'); 

            getData({
                task: 'updatePost',
                return: 'returnState', // returnId, returnState, returnArray
                inputs: {
                    inputId,
                    inputTitle,
                    inputPathurl,
                    inputSummary,
                    inputDate,
                    inputTags,
                    inputBody,
                    inputAuthor,
                    inputImg,
                    inputImgRef,
                    inputEmbed,
                    inputPreviewEmbed,
                    inputInternEmbed,
                    inputVideo,
                    inputPreviewVideo,
                    inputInternVideo,
                    categorys,
                    mediaFiles,
                    inputCls,
                    inputJson,
                    inputState
                }
            }).then((response) => {
                console.log('updatePost', response);
                if (response.status == "success") {
                    removeInnerForm();
                    postsIndex();
                } else {
                    alertMessageError({
                        message: response.message
                    })
                }
            }).catch(console.warn());
        }

    });

    $("body").on("click", `.btnEditItemPost`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log(`.btnEditItemPost`, data);
        formEditPosts(data);
    });

    $("body").on("click", `#btnConfigPosts`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`#btnConfigPosts`, data);
        const idModal = "modalConfigPosts";

        getData({
            task: 'getDataConfigPosts',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
        }).then((responseData) => {
            console.log('getData getDataConfigPosts', responseData);
            if (responseData.status == 'success') {
                const tableId = "tableAutors";
                let tableAuthors = mountMacroTable({
                    tableId,
                    data: responseData.data.authors,
                    fn: "fnAuthors",
                    module,
                    cols: [{
                        label: "id",
                        cls: 'colId',
                        type: "id",
                        render: 'id',
                    }, {
                        label: "*Nombre Completo",
                        cls: 'colName',
                        type: "text",
                        render: 'name',
                        attrName: 'inputName',
                        id: 'inputName',
                    }, {
                        label: "*Descripción",
                        cls: 'colTextarea',
                        type: "textarea",
                        render: 'summary',
                        attrName: 'inputSummary',
                        id: 'inputSummary',
                    }, {
                        label: "Datos de Contacto",
                        cls: 'colTextarea',
                        type: "textarea",
                        render: 'contact',
                        attrName: 'inputContact',
                        id: 'inputContact',
                    }, {
                        label: "Estado",
                        cls: 'colState',
                        type: "state",
                        render: 'state',
                        action: 'changeState',
                        attrName: 'inputState',
                        id: 'inputState',
                    }]
                })


                let body = renderModalConfigBody({
                    title: 'Configuración',
                    system,
                    id: 'modalConfigPosts',
                    sidebar: renderPosts.renderSidebar({
                        module
                    }),
                    body: renderPosts.renderBodyConfig({
                        tableAuthors
                    })
                })
                $("#root").append(renderModalConfig({
                    body,
                    system,
                    id: idModal
                }));

                resizeModalConfig(system);
            } else {
                alertMessageError({
                    message: response.message
                })
            }
        }).catch(console.warn());

    });

    $("body").on("click", `#btnNewAuthorPosts`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnNewAuthor`, data);
        loadView(_PATH_WEB_NUCLEO + "modules/websites/views/formAuthor.html?" + _VS).then((responseView) => {
            //console.log('loadView', responseView);

            let content = responseView;
            content = content.replaceAll("{{_FORM_ID}}", "formNewAuthorPosts");
            content = content.replaceAll("{{_MODULE}}", module);
            content = content.replaceAll("{{_SYSTEM}}", system);
            content = content.replaceAll("{{_BTN_LABEL}}", 'Guardar');
            content = content.replaceAll("{{_BTN_ACTION}}", 'btnSaveNewAuthorPosts');

            addHtml({
                selector: `body`,
                type: 'append',
                content: renderModalClean({
                    id: "modalFormPostAuthor",
                    title: 'Nuevo Autor',
                    body: content,
                    clsHeader: 'blue-sky'
                }) // insert, append, prepend, replace
            })
        }).catch(console.warn());
    });

    $("body").on("click", `#btnSaveNewAuthorPosts`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`#btnSaveNewAuthorPosts`, data);
        let count = 0;

        let inputName = $(`#${data.form} #inputAuthorName`).val();
        let inputSummary = $(`#${data.form} #inputAuthorSummary`).val();
        let inputContact = $(`#${data.form} #inputAuthorContact`).val();
        let inputState = $(`#${data.form} #inputAuthorState`).val();
        let inputImage = $(`#${data.form} #inputImageAuthor`).val();
        let inputPath = $(`#${data.form} #inputAuthorPathurl`).val();
        let type = data.type || "";

        console.log("type", type);

        if (inputName == "") {
            errorInputSelector(`#${data.form} #inputName`, "Ingrese el Nombre del Autor");
            count++;
        }
        if (inputSummary == "") {
            errorInputSelector(`#${data.form} #inputSummary`, "Ingrese la descripción del Autor");
            count++;
        }

        if (count == 0) {
            getData({
                task: 'addAuthorPosts',
                return: 'returnId',
                inputs: {
                    inputName,
                    inputSummary,
                    inputContact,
                    inputImage,
                    inputPath,
                    inputState
                }
            }).then((responseData) => {
                console.log('getData addAuthorPosts', responseData);
                if (responseData.status == 'success') {
                    let dataId = responseData.data;
                    if (type == "express") {
                        removeModalId("modalFormPostAuthor");
                        let launch = data.launch;
                        addHtml({
                            selector: "#" + launch,
                            type: 'append',
                            content: renderPill({
                                name: inputName,
                                clsName: 'btnEditItemAuthor',
                                id: dataId,
                                fn: 'cleanInputAuthorPosts'
                            }) // insert, append, prepend, replace
                        });
                        $(".btnClearInputForm").remove();
                        $("#inputAuthorSearch").val("");
                        $(`#${data.form} #inputAuthor`).val(dataId.id);
                        $("#inputAuthorSearch").addClass("disabled");
                    }
                }

                if (responseData.status == 'error') {
                    alertMessageError({
                        message: responseData.message
                    })
                }
            }).catch(console.warn());
        }

    });

    $("body").on("click",`.btnEditItemAuthor`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnEditItemAuthor`,data);
        
        loadView(_PATH_WEB_NUCLEO + "modules/websites/views/formAuthor.html?" + _VS).then((responseView) => {
            //console.log('loadView', responseView);
            let form = "formUpdateAuthorPosts";
            let content = responseView;
            content = content.replaceAll("{{_FORM_ID}}", form);
            content = content.replaceAll("{{_MODULE}}", module);
            content = content.replaceAll("{{_SYSTEM}}", system);
            content = content.replaceAll("{{_BTN_ACTION}}", "btnUpdateAuthorPosts");
            content = content.replaceAll("{{_BTN_LABEL}}", "Actualizar");

            getData({
                task: 'getDataAuthorId',
                return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
                item : data.id
            }).then((response) => {
                console.log('getData getDataAuthorId',response);
                if(response.status=='success'){
                    let author = response.data;
                    console.log("author",author);

                    addHtml({
                        selector: `body`,
                        type: 'append',
                        content: renderModalClean({
                            id: "modalFormPostAuthor",
                            title: 'Editar Autor',
                            body: content,
                            clsHeader: 'blue-sky'
                        }) // insert, append, prepend, replace
                    });

                    $(`#${form} #inputAuthorId`).val(author.id);
                    $(`#${form} #inputAuthorName`).val(author.name);
                    $(`#${form} #inputAuthorSummary`).val(author.summary);
                    $(`#${form} #inputAuthorContact`).val(author.contact);
                    $(`#${form} #inputAuthorPathurl`).val(author.path);
                    $(`#${form} #inputAuthorState`).val(author.state);
                    $(`#${form} #inputAuthorImageAuthor`).val(author.img);
                    $(`#${form} #previewImgAuthor`).html(renderItemImage({
                        item: author.img.id,
                        id: "inputImageAuthor",
                        pathFile: _PATH_FILES + author.img.pathurl,
                    }));

                }else{
                     alertMessageError({message: response.message})
                }
            }).catch(console.warn());

            
        }).catch(console.warn());
    });

    $("body").on("click",`#btnUpdateAuthorPosts`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnUpdateAuthorPosts`,data);

        let count = 0;

        let inputId = $(`#${data.form} #inputAuthorId`).val();
        let inputName = $(`#${data.form} #inputAuthorName`).val();
        let inputSummary = $(`#${data.form} #inputAuthorSummary`).val();
        let inputContact = $(`#${data.form} #inputAuthorContact`).val();
        let inputState = $(`#${data.form} #inputAuthorState`).val();
        let inputImage = $(`#${data.form} #inputImageAuthor`).val();
        let inputPath = $(`#${data.form} #inputAuthorPathurl`).val();
        let type = data.type || "";

        console.log("type", type);

        if (inputName == "") {
            errorInputSelector(`#${data.form} #inputName`, "Ingrese el Nombre del Autor");
            count++;
        }
        if (inputSummary == "") {
            errorInputSelector(`#${data.form} #inputSummary`, "Ingrese la descripción del Autor");
            count++;
        }

        if (count == 0) {
            getData({
                task: 'updateAuthorPosts',
                return: 'returnState',
                inputs: {
                    inputId,
                    inputName,
                    inputSummary,
                    inputContact,
                    inputImage,
                    inputPath,
                    inputState
                }
            }).then((responseData) => {
                console.log('getData addAuthorPosts', responseData);
                if (responseData.status == 'success') {
    
                        removeModalId("modalFormPostAuthor");
                        $(`#${data.form} .btnEditItemAuthor`).html(inputName);
                        $(`#${data.form} #inputAuthor`).val(inputId);
                        $("#inputAuthorSearch").addClass("disabled");
            
                }

                if (responseData.status == 'error') {
                    alertMessageError({
                        message: responseData.message
                    })
                }
            });

        }
        
    });

    $("body").on("click", `.boxResults[for="inputAuthorSearch"] .item`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.boxResults[for="inputAuthorSearch"] .item`, data);
        /*  addHtml({
             selector: "#boxLauncherPillAuthor",
             type: 'append',
             content: renderPill({
                 name: data.name,
                 id: data.id,
                 module,
                 fn: 'cleanInputAuthorPosts'
             }) // insert, append, prepend, replace
         }); */

        insertPillAuthor({
            name: data.name,
            id: data.id,
            module
        });

        $(".btnClearInputForm").remove();
        $("#inputAuthorSearch").val("");
        $(`#inputAuthor`).val(data.id);
        $("#inputAuthorSearch").addClass("disabled");
    });

    $("body").on("click", `.btnDeleteItemPost`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnDeleteItemPost`, data);

        deleteModal({
            id: "deleteModal",
            name: data.title,
            cls: "deleteModalItemPost",
            attr: `data-id="${data.id}"`,
        })

    });

    $("body").on("click", `.deleteModalItemPost`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let item = data.id;
        console.log(`.deleteModalItemPost`, data);

        getData({
            task: 'deletePost',
            return: 'returnState', // returnId, returnState, returnArray
            item
        }).then((data) => {
            console.log("deletePost", data)
            if (data.Error == 0) {
                removeModal();
                removeItemTable(item);
                //productsIndex()
            }
        })
    });

    $("body").on("click", `.btnSelectionPost`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let dataBtn = $(this).data();
        console.log(`.btnSelectionPost`, dataBtn);

        getData({
            task: 'getPosts',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
        }).then((response) => {
            console.log('getData getPosts', response);
            if (response.status == 'success') {

                let data = response.data;
                let btnItem = '<div class="item"><input type="search" class="inputSearch" name="search" id="search" placeholder="Buscar Post..." ></div>';



                for (let i = 0; i < data.length; i++) {
                    let item = data[i];
                    let imgId = 0;
                    let pathImg = '';
                    let pathThumb = '';

                    if (item.img != 0) {
                        imgId = item.img.id;
                        pathImg = item.img.pathurl;
                        pathThumb = item.img.thumb;
                    } else {
                        imgId = 0;
                        pathImg = 'sites/default/assets/img/no-image.jpg';
                        pathThumb = 'sites/default/assets/img/no-image-thumb.jpg';
                    }

                    btnItem += `<a class="item btnItemSelectionPost" 
                        data-return = "${dataBtn.return}"
                        data-idreturn = "${dataBtn.item}"
                        data-item="${item.id}" 
                        data-img= "${imgId}"
                        data-title = "${item.title}"
                        data-thumb = "${pathThumb}"
                        data-path="${item.pathurl}">
                        <div class="img">
                            <img src = "${_PATH_FILES + pathThumb}" >
                        </div>
                        <span class="info">${item.title}</span>
                    </a>`
                }

                addHtml({
                    selector: `body`,
                    type: 'append',
                    content: renderModalClean({
                        id: 'modalSelectionPost',
                        attr: `data-item="${data.item}"`,
                        title: 'Selecciona una Noticia',
                        body: btnItem
                    })
                })

            } else {
                alertMessageError({
                    message: response.message
                })
            }
        }).catch(console.warn());


    });

    $("body").on("click", `.btnItemSelectionPost`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let returnData = data.return;
        console.log(`.btnItemSelectionPost`, data);

        $(`#${returnData}`).val(data.item);

        $(`.postBox .box[for='inputPost${data.idreturn}'] .info .title`).html(data.title);
        $(`.postBox .box[for='inputPost${data.idreturn}'] .info .review`).html(data.review);
        $(`.postBox .box[for='inputPost${data.idreturn}'] .img img`).attr('src', _PATH_FILES + data.thumb);

        removeModalId('modalSelectionPost');

    });

    $("body").on("keyup", `form[module="${module}"] textarea[id^="inputEmbed"]`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let id = data.active;
        //console.log(`inputEmbed`, data);
        $(`.btnSwitch[data-for="${id}"]`).attr("value", "1");
        $(`#${id}`).val(1);

        let inputEmbed = $(this).val();
        let count = 0;

        if (inputEmbed != "") {
            if (inputEmbed.indexOf("<iframe") == -1) {
                alertMessageError({
                    message: 'Añade un Embed de Youtube'
                })
                count++;
            }
            if (inputEmbed.indexOf("</iframe>") == -1) {
                alertMessageError({
                    message: 'Añade un Embed de Youtube'
                })
                count++;
            }
        }

        if (count == 0) {
            $("#previewEmbed").html(inputEmbed);
        }
    });

    

});