import {
    empty,
    btnFn
} from "../../../components/functions.js";
// import {renderBtnState} from "../../components/renders/render.js";
import {
    renderBtnState
} from "../../../components/renders/renderTables.js";
import {
    unDbDate
} from "../../../components/dates.js";

export function renderTr(vars) {


    return (
        `
    <tr class="rowItem" item="${vars.id}">
        <td class="colCheck colId"><input name="inputCheck[]"  type="checkbox" value="${vars.id}"></td>
        <td class="colId">${vars.id}</td>
        <td class="colImg">
            <a>
                <span><img src="${_PATH_FILES + vars.thumb}" /></span>
                <span>${vars.name}</span>
            </a>
        </td>
        <td class="colState">` +
        renderBtnState({
            item: vars.id,
            fn: "changeStateItem",
            state: vars.state,
            module: vars.module,
            system: vars.system,
        }) +
        `</td>
        <td class="colActions">
            <div class="btns">
                 ` +
        btnFn({
            btnType: "btnEdit",
            clss: "btnActionModal",
            attr: "content='' module='" + vars.module + "'",
            item: vars.id,
            module: vars.module,
            system: vars.system,
            fn: "editItemMedia",
            vars: vars.id,
        }) +
        `
                        ` +
        btnFn({
            cls: "btnDeleteItemMedia",
            item: vars.id,
            module: vars.module,
            system: vars.system,
            icon : "icon icon-trash",
            attr: `data-id="${vars.id}" data-module="${vars.module}" data-system="${vars.system}" data-name="${vars.name}"`,
        }) +
        `
            </div>
        </td>
     </tr>
   `
    );
}