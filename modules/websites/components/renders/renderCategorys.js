export const renderNode = (vars) => {
    //console.log("renderNode", vars);
    let level = vars.level != "" ? vars.level : 0;
    let id = vars.id || "";
    let add = vars.add || "";
    let name = vars.name || "";
    let lastItem = vars.lastItem || "";
    let activeChild = vars.activeChild || "";
    let childs = vars.childs || "";
    let parent = vars.parent ? vars.parent : 0;
    let numChildrens = vars.numChildrens ? vars.numChildrens : 0;
    let state = vars.state ? vars.state : 0;
    let pathurl = vars.pathurl ? vars.pathurl : "";
    let collapse = "";

    if (numChildrens > 0) {
        collapse = `<a class="btnAddNode btnCollapseNode"  title="Collapse" item="${id}">
                <i class="icon icon-chevron-down"></i>
            </a>`;
    }

    return /*html*/ `
        <div class="checkbox level level${level} ${lastItem}" child="${numChildrens}" parent="${parent}" level="${level}" item="${id}" >
            <div class="head" state="${state}">
                <span class="box ${activeChild}">
                    <input name="inputCategorys[]" id="inputCat${id}" type="checkbox" value="${id}">
                </span>
                <span class="name btnNode" title="${id}" child="${numChildrens}" state="${state}"   parent="${parent}" state-add="0" item="${id}">${name}</span>
                <div class="add" parent="${parent}" child="${numChildrens}" pathurl="${pathurl}" item="${id}">${add} ${collapse}</div>
            </div>
            <div class="childs ${activeChild}">
                ${childs}
            </div>
        </div>
    `;
}

export const renderTaskNodes = (vars) => {
    //console.log('renderTaskNodes', vars);
    let id = vars.item;
    let child = vars.child ? vars.child : 0;
    let rootCategorys = document.getElementById("rootCategorys");
    let order = "";
    let collapse = "";

    if (child > 0) {
        order = `<a class="btnShortNode"  title="Ordenar" data-node="${id}">
                        <i class="icon icon-sort"></i>
                    </a>`;
    }

    $("#rootCategorys .add[item='" + id + "']").html( /*html*/ `
        <a class="btnActionNode btnAddNode" title="Nueva Categoria Hija" item="${id}">\
            <i class="icon icon-plus"></i>
        </a>
        ${order}
        <!--<a class="btnActionNode btnShipNode"  title="Saltar a url" item="${id}">
            <i class="icon icon-skip"></i>
        </a>-->
        <a class="btnActionNode btnStateNode" state="0" title="Activar/Desactivar" item="${id}">
            <i class="icon icon-eye-open"></i>
            <i class="icon icon-eye-close"></i>
        </a>
        <a class="btnActionNode btnEditNode" title="Editar" item="${id}">
            <i class="icon icon-pencil"></i>
        </a>
        <a class="btnActionNode btnWorksheetsNode" title="Contenidos" item="${id}">
            <i class="icon icon-worksheets"></i>
        </a>
        <a class="btnActionNode btnDeleteNode" title="Eliminar Categoria" item="${id}">
            <i class="icon icon-trash"></i>
        </a>
    `);
}