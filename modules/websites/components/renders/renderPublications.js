import {
    replaceEssentials
} from '../../../components/functions.js'

export const pubElem = (vars = [], index = "") => {
    console.log('pubElem', vars);

    let item = vars.item ? vars.item : "";
    let name = vars.name ? vars.name : "";
    let title = vars.title ? vars.title : "";
    let description = vars.description ? vars.description : "";
    let type = vars.type ? vars.type : "";
    let pathAux = vars.path ? vars.path : "";
    let pathui = vars.pathUI ? vars.pathUI : "";
    let pathtpl = vars.pathTpl ? vars.pathTpl : "";
    let cls = vars.cls ? vars.cls : "";
    let pathIcon = vars.pathIcon ? vars.pathIcon : "";

    let icon = "";


    if (pathIcon == '' || pathIcon == null) {
        icon = _PATH_WEB_NUCLEO + 'modules/assets/img/default.pub.svg?' + _VS;
    } else {
        if (type == "host") {
            icon = _PATH_WEB + pathIcon + '?v=' + _VS;
        } else if (type == "nucleo") {
            icon = _PATH_WEB_NUCLEO + pathIcon + '?v=' + _VS;
        } else {
            icon = replaceEssentials({
                str: pathIcon,
            }) + '?v=' + _VS;
        }
    }

    let attr = `data-path="${pathAux}" data-type="${type}" data-name="${name}" data-pathui="${pathui}"  data-pathtpl="${pathtpl}" data-cls="${cls}" data-title="${title}" data-pathicon="${pathIcon}" data-description="${description}"`;

    let pubCls = vars.pubCls ? vars.pubCls : "btnPubElem";

    return /*html*/ `
        <div class = "pubElem ${pubCls} pubElem-${index}" ${attr} data-item="${item}" data-name="${name}" item="${item}" >
            <div class="inner">
                <div class="img">
                    <img src="${icon}" alt="${name}" />
                </div>
                <div class="text">
                    <h3>${name}</h3>
                    <p>${description}</p>
                </div>
            </div>
        </div>
    `;
}