 export const renderWorksheetElem = (vars) => {
     //console.log('renderWorksheetElem', vars)
     let elmState = '';
     let elmPubState = '';
     let auxElmCls = '';

     if (vars.blockChildrenNum > 0) {
         elmState = vars.blockChildren.map(function (elm) {

             return renderWorksheetElem({
                 wsId: vars.wsId,
                 catId: vars.catId,
                 module: vars.module,
                 label: elm.name,
                 item: elm.id,
                 type: elm.type,
                 state: elm.state,
                 cls: elm.cls ? elm.cls : '',
                 level: elm.level ? elm.level : '',
                 blockChildrenNum: elm.blockChildrenNum,
                 blockChildren: elm.blockChildren,
                 pubChildrenNum: elm.pubChildrenNum,
                 pubChildren: elm.pubChildren
             })
         });
     }

     if (vars.pubChildrenNum > 0) {
         elmPubState = vars.pubChildren.map(function (elm) {

             return renderWorksheetElem({
                 wsId: vars.wsId,
                 catId: vars.catId,
                 module: vars.module,
                 label: elm.name,
                 item: elm.id,
                 type: elm.type,
                 state: elm.state,
                 cls: elm.cls ? elm.cls : '',
                 level: elm.level ? elm.level : '',
                 blockChildrenNum: elm.blockChildrenNum,
                 blockChildren: elm.blockChildren,
                 pubChildrenNum: elm.pubChildrenNum,
                 pubChildren: elm.pubChildren
             })
         });
     }



     let auxIcon = "";
     let actions = "";
     let auxAnimation = "";
     let auxActive = vars.state > 0 ? 'open' : 'close';
     let btnEdit = `<a class="btn btnIcon btnEdit" module="${vars.module}" cat="${vars.catId}" type="${vars.type}" item="${vars.item}"><i class="icon icon-pencil"></i></a>`;
     let btnNew = `<a class="btn btnIcon btnNew"  module="${vars.module}" cat="${vars.catId}" level="${vars.level}"  ws="${vars.wsId}" type="${vars.type}" item="${vars.item}"><i class="icon icon-square-plus"></i></a>`;
     let btnDelete = `<a class="btn btnIcon btnDelete" module="${vars.module}" item="${vars.item}" type="${vars.type}" item="${vars.item}"><i class="icon icon-circle-close"></i></a>`;

     let btnDuplicate = `<a class="btn btnIcon btnDuplicate"   module="${vars.module}" type="${vars.type}" item="${vars.item}"><i class="icon icon-copy"></i></a>`;

     let btnActive = `<a class="btn btnIcon btnActive" module="${vars.module}" type="${vars.type}" item="${vars.item}"><i class="icon icon-eye-${auxActive}"></i></a>`;

     if (vars.type == "block") {
         auxElmCls = "ui-state-disabled";
         actions = btnNew;
     }

     if (vars.type == "publication") {
         auxIcon = "<i class='icon icon-gril-compac'></i>";
         auxElmCls = "ui-state-default";
         actions = btnEdit + btnDuplicate + btnActive + btnDelete;
         auxAnimation = vars.animation ? vars.animation : '';
     }

     return /*html*/ `
        <div
            class="wsElem ${auxElmCls} ${vars.cls} ${auxAnimation}" 
            level="${vars.level}" 
            type="${vars.type}" 
            state="${vars.state}" 
            ws="${vars.wsId}" 
            cat="${vars.catId}" 
            item="${vars.item}">
            <div class="head ui-state-disabled">
                <div class="title">
                    ${auxIcon}
                    <label>${vars.label}</label>
                </div>
                <div class="actions">
                    ${actions}
                </div>
            </div>
            <div class="body connectedSortable droptrue" level="${vars.level}"  ws="${vars.wsId}" type="${vars.type}" item="${vars.item}">
                ${ elmPubState.toString().replace(/,/g,"")}
            </div>
            ${elmState.toString().replace(/,/g,"")}
        </div>`;
 }