export function renderSidebar(vars) {
    return /*html*/ `
        <button
            type="button"
            class="btnActionModal btnIconLeft btn active"
            module="${vars.module}"
            fn=""
            content="contentGeneral"
        >
            <i class="icon icon-list"></i>
            <span>General</span>
        </button>
        <button
            type="button"
            class="btnActionModal btnIconLeft btn"
            module="${vars.module}"
            fn=""
            content="contentAuthors"
        >
            <i class="icon icon-users"></i>
            <span>Autores</span>
        </button>
    `;
}

export const renderBodyConfig = (vars = []) => {
    console.log('renderBodyConfig', vars);
    return /*html*/ `
        <div class="content active" id="contentGeneral">
            <div class="head">
                <div class="left">
                    <div class="title">
                        <i class="icon icon-list"></i>
                        <span>General</span>
                    </div>
                </div>
                <div class="right"></div>
            </div>
        </div>
        <div class="content" id="contentAuthors">
            <div class="head">
                <div class="left">
                    <div class="title">
                        <i class="icon icon-users"></i>
                        <span>Autores</span>
                    </div>
                </div>
                <div class="right">
                    <!--<button type="button" class="btn btnPrimary btnSmall btnIconLeft" id="btnAddAuthor">
                        <i class="icon icon-plus"></i>
                        <span>Nuevo Autor</span>
                    </button>-->
                </div>
            </div>
            <div class="tbody">
                ${vars.tableAuthors}
            </div>
        </div> 
    `;
}