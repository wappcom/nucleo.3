import { renderBtnState } from "../../../components/renders/renderTables.js";
import { unDbDate,dayLiteral } from "../../../components/dates.js";

export function renderTr(vars) {
    //console.log ("renderTr", vars);
    let path = vars.path;
    let url = _PATH_FILES + vars.file.pathurl;
    let img = _PATH_FILES + vars.file.thumb;
    let imgUrl = "";
    let date =  vars.date || "---";
    let user = vars.user || "---";
    date = dayLiteral({dateInput : date, mode: "compact"})

    if (img != 0){
        imgUrl = `<img src="${img}" alt="${vars.title}" title="${vars.title}">`;
    }

    return (
        `
    <tr class="rowItem" item="${vars.id}">
        <td class="colCheck colId"><input name="inputCheck[]"  type="checkbox" value="${vars.id}"></td>
        <td class="colId">${vars.id}</td>
        <td class="colImg colTitle"><div class="img">${imgUrl}</div><a target="_blank" href="${url}">${vars.title}</a></td>
        <td class="colDate">${date}</td>
        <td class="col">${user}</td>
        <td class="colState">` +
        renderBtnState({
            item: vars.id,
            fn: "changeStateItem",
            state: vars.state,
            module: vars.module,
            system: vars.system,
        }) +
        `</td>
        <td class="colActions">
            <div class="btns">
                <button class="btn btnIcon btnEditForm${path}" type="button"   data-id="${vars.id}" data-module="${vars.module}"><i class="icon icon-pencil"></i></button>
                <button class="btn btnIcon hoverDelete btnDeleteForm${path}" type="button" data-name="${vars.title}" data-id="${vars.id}" data-module="${vars.module}" ><i class="icon icon-trash"></i></button>
            </div>
        </td>
     </tr>
   `
    );
}