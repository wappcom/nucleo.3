import { empty, btnFn } from "../../../components/functions.js";
// import {renderBtnState} from "../../components/renders/render.js";
import { renderBtnState } from "../../../components/renders/renderTables.js";
import { unDbDate } from "../../../components/dates.js";

export function renderTr(vars) {
    //console.log ("renderTr", vars);
    let path = vars.path;
    return (
        `
    <tr class="rowItem" item="${vars.id}">
        <td class="colCheck colId"><input name="inputCheck[]"  type="checkbox" value="${vars.id}"></td>
        <td class="colId">${vars.id}</td>
        <td class="colTitle"><span>${vars.title}</span></td>
        <td class="colImg"><a href="${vars.url}" target="_blank"><span>${vars.url}</span></a></td>
        <td class="colState">` +
        renderBtnState({
            item: vars.id,
            fn: "changeStateItem",
            state: vars.state,
            module: vars.module,
            system: vars.system,
        }) +
        `</td>
        <td class="colActions">
            <div class="btns">
                <button class="btn btnIcon btnEditForm${path}" type="button"   data-id="${vars.id}" data-module="${vars.module}"><i class="icon icon-pencil"></i></button>
                <button class="btn btnIcon hoverDelete btnDeleteForm${path}" type="button" data-name="${vars.title}" data-id="${vars.id}" data-module="${vars.module}" ><i class="icon icon-trash"></i></button>
            </div>
        </td>
     </tr>
   `
    );
}