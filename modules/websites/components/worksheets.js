import {
    replacePath,
    replaceEssentials,
    replaceAll,
    loadView,
    stopLoadingBar,
    emptyReturn,
    loadingBtnIcon,
    btnLoadingRemove,
    unDisableId,
    accessToken,
    removeBtnLoading,
    dataModule,
    addHtml,
    removeHtml,
    alertMessageError,
    btnLoading,
    changeBtn,
    empty
} from "../../components/functions.js";
import {
    sortJSON,
    editorText,
    errorInput,
    errorFormInput,
    loadInitChargeMedia,
    loadItemMedia,
    loadFilesFormLoader,
    loadSingleFileFormLoader,
    loadChargeMediaInit,
    removalMediaItems,
    optionsForm,
    validateUrl,
    validatePathUrlNode,
    moveCursorToEnd,
    selectTextInput,
    listElemSelectables,

} from "../../components/forms.js";

import {
    renderItemInputContent,
} from "../../components/renders/renderForms.js";

import {
    renderDeleteModal,
    renderModalConfig,
    innerForm,
    renderModalRenameFile,
    renderModalRename,
    renderModalDelete,
    renderModalConfirm
} from "../../components/renders/renderModals.js";

import {
    getCategorys
} from "./categorys.js";

import {
    getFormNewPub,
    getPubListSelect,
    resizePublications,
    getFormSelectedPub,
    duplicatePublication,
    dataPublications,
    editPublication,
} from "./publications.js";

import {
    renderNode,
    renderTaskNodes,
} from "./renders/renderCategorys.js";

import {
    renderWorksheetElem
} from "./renders/renderWorksheets.js";

import {
    removeModal,
    tooltip
} from "../../components/modals.js";




var module = 'worksheets';
var system = 'websites';
let pathurl = "modules/websites/";
let idFormWs = "worksheetNode";
 

export const loadWorksheetsNode = (vars) => {
    ////console.log('loadWorksheetsNode', vars);
    let moduleVar = vars.module ? vars.module : 'worksheets';
    let catId = vars.item ? vars.item : 0;
    let wsId = vars.wsId ? vars.wsId : 1;
    dataPublications().then((responsePubs) => {
        //console.log('responsePubs', responsePubs);
        //_PUBLICATIONS = responsePubs.data;

        localStorage.setItem("_PUBLICATIONS", JSON.stringify(responsePubs.data));

        loadWorksheetId({
            module: moduleVar,
            catId,
            wsId,
            catName: vars.name
        });

    }).catch((error) => {
        console.log('error', error);
    });
}

export const resizeWorkspaceNode = () => {
    //console.log('resizeWorkspaceNode', vars);
    let w = $(window).width();
    let wSidebarMenu = document.querySelector('.sidebarMenuModule').offsetWidth;
    let bodyCategorys = document.querySelector('.bodyModule[module="categorys"]');
    let boxModuleCategorys = document.querySelector('.boxModule[module="categorys"]');
    let worksheetNode = document.getElementById('worksheetNode');
    let wWs = w - (200 + 390);
    bodyCategorys.style.width = "390px";
    boxModuleCategorys.style.justifyContent = "flex-start";
    worksheetNode.style.width = wWs + "px";

    console.log("wSidebarMenucategorys", wWs, wSidebarMenu );
}

export const loadWorksheetId = (vars = []) => {
    //console.log('loadWorksheetId', vars);

    getWorksheets().then((getWorksheetsResponse) => {
        getPatterns().then((getPatternsResponse) => {
            //console.log('getPatterns', getPatternsResponse);
            // console.log("getWorksheets", getWorksheetsResponse);

            let worksheetsArray = getWorksheetsResponse.data;
            let patternsArray = getPatternsResponse.data;
            let wsId = vars.wsId;
            let catId = vars.catId;
            let catName = vars.catName;
            let moduleVar = vars.module ? vars.module : 'worksheets';

            getWorksheetsId({
                wsId: wsId,
                module: moduleVar,
                catId
            }).then((getWsResponse) => {
                //console.log('getWorksheetsId', getWsResponse);

                let wsState = '';
                if (getWsResponse.Error == 0 && getWsResponse.data != 0) {
                    wsState = getWsResponse.data.map(function (elm) {
                        return renderWorksheetElem({
                            wsId,
                            catId,
                            module: moduleVar,
                            label: elm.name,
                            item: elm.id,
                            type: elm.type,
                            cls: elm.cls,
                            level: elm.level,
                            state: elm.state,
                            blockChildrenNum: elm.blockChildrenNum,
                            blockChildren: elm.blockChildren,
                            pubChildrenNum: elm.pubChildrenNum,
                            pubChildren: elm.pubChildren
                        })
                    });
                } else {
                    wsState = "";
                }
                ////console.log('wsState', wsState);
                loadView(_PATH_WEB_NUCLEO + "modules/websites/views/worksheetNode.html?" + _VS).then((response) => {
                    ////console.log('loadView', response);
                    let str = replaceEssentials({
                        str: response,
                        module: moduleVar,
                        system,
                        pathurl,
                        item: catId,
                        title: catName,
                        formId: 'spaceWorksheet',
                        btnAction: 'btnUpdateWorksheet',
                        btnNameAction: "Actualizar",
                    })

                    str = str.replace(/\{{_CAT_ID}}/g, catId);
                    str = str.replace(/\{{_WS_ID}}/g, wsId);

                    str = str.replace(/\{{_OPTIONS_WORKSHEET}}/g, optionsForm({
                        options: worksheetsArray,
                        initialLabel: 0,
                        idSelected: wsId
                    }));

                    str = str.replace(/\{{_OPTIONS_PATTERNS}}/g, optionsForm({
                        options: patternsArray,
                        initialLabel: 0,
                        idSelected: 1,
                        des: 1 // check data description
                    }));

                    innerForm({
                        module: moduleVar,
                        id: idFormWs,
                        body: str,
                    });

                    $("#spaceWorksheet").html(
                        wsState
                    )
                    $("#inputWorksheet").append(`<option value="0" lang="es">Agregar Hoja</option>`);
                    if (patternsArray.length == 0) {
                        $("#inputPattener").append(`<option value="-" title="---" lang="es">---</option>`);
                    }


                    $("#" + idFormWs + " .tbody .inner .droptrue").sortable({
                        connectWith: ".connectedSortable"
                    })

                    $("#" + idFormWs + " .tbody .inner .dropfalse").sortable({
                        connectWith: ".connectedSortable",
                        dropOnEmpty: false,
                    })

                    resizeWorkspaceNode();

                }).catch(console.warn());
            }).catch(console.warn());
        }).catch(console.warn());

    }).catch(console.warn());
}

// Function async axios

export const getPatterns = async (vars) => {
    ////console.log('getPatterns', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/worksheets.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "getPatterns",
        vars
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        ////console.log(res.data);
        return res.data
    } catch (error) {
        //console.log("Error: getPatterns")
        //console.log(error)
    }
}


export const getWorksheets = async () => {
    ////console.log('getWorksheets');
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/worksheets.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "getWorksheets",
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        ////console.log(res.data);
        return res.data
    } catch (error) {
        //console.log("Error: getWorksheets")
        //console.log(error)
    }
}

export const getWorksheetsId = async (vars) => {
    ////console.log('getWorksheetsId', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/worksheets.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "getWorksheetsId",
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        ////console.log(res.data);
        return res.data
    } catch (error) {
        //console.log("Error: getWorksheetsId")
        //console.log(error)
    }
}

export const updateWorksheet = async (vars = []) => {
    //console.log('updateWorksheet', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/worksheets.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "updateWorksheet",
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        ////console.log(res.data);
        return res.data
    } catch (error) {
        //console.log("Error: updateWorksheet")
        //console.log(error)
    }
}

export const savePattern = async (vars = []) => {
    //console.log('savePattern', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/worksheets.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "savePattern",
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        ////console.log(res.data);
        return res.data
    } catch (error) {
        //console.log("Error: savePattern")
        //console.log(error)
    }
}

export const deletePattern = async (vars = []) => {
    //console.log('deletePattern', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/worksheets.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        action: "deletePattern",
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        ////console.log(res.data);
        return res.data
    } catch (error) {
        //console.log("Error: deletePattern")
        //console.log(error)
    }
}

export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/websites/controllers/apis/v1/worksheets.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("Error: getData", error)
    }
}

document.addEventListener("DOMContentLoaded", function () {
    $("body").on("click", "#" + idFormWs + " .btnNew[type='block']", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let wsId = $(this).attr("ws");
        let catId = $(this).attr("cat");
        let blockId = $(this).attr("item");
        let module = $(this).attr("module");
        let level = $(this).attr("level");
        ////console.log(`btnNew[type='block']`, blockId);
        /* getFormNewPub({
            blockId,
            wsId,
            catId,
            module
        }); */

        getFormSelectedPub({
            blockId,
            wsId,
            catId,
            module,
            level
        });
    })

    $("body").on("click", ".btnDelete[type='publication']", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        //console.log('.btnDelete');
        let module = $(this).attr("module");
        let item = $(this).attr("item");
        $(this).parents(".wsElem[type='publication']").remove();
    })

    $("body").on("click", "#" + idFormWs + " .btnDuplicate[type='publication']", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let item = $(this).attr("item");
        let selector = `.wsElem[type='publication'][item='${item}']`;
        let wsId = $(selector).attr("ws");
        let catId = $(selector).attr("cat");
        let module = 'categorys';
        let level = $(this).parents('[type="block"]').attr("level");
        let blockId = $(this).parents('[type="block"]').attr("item");

        //console.log(`btnDuplicate[type='publication']`, item, module, blockId);

        let name = $(selector).find(".title label").text();
        let attr = `data-level="${level}" data-block="${blockId}" data-ws="${wsId}" data-item="${item}" data-cat="${catId}" data-fn="duplicatePub" data-id="inputformRename" data-module="${module}"`;
        const text = name + " (copia)";

        addHtml({
            selector: `.boxModule[module='${module}']`,
            type: 'prepend',
            content: renderModalRename({
                'id': 'formRename',
                input: text,
                fn: 'duplicatePub',
                item,
                icon: 'icon icon-copy',
                text: 'Cambia el nombre de la publicación',
                module,
                attr
            })
        }) //type: html, append, prepend, before, after


        $("#inputformRename").focus();
        //moveCursorToEnd("inputformRename");
        selectTextInput({
            id: "inputformRename"
        });



    });

    $("body").on("click", ".btnModalRename[data-fn='duplicatePub']", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log('.btnModalRename', data);
        let inputName = $("#" + data.id).val();
        let itemOrigin = data.item;

        duplicatePublication({
            inputName,
            itemOrigin
        }).then((response) => {
            //console.log("duplicatePublication", response);

            if (response.status == "success") {
                let content = renderWorksheetElem({
                    wsId: data.ws,
                    blockId: data.block,
                    catId: data.cat,
                    module: data.module,
                    item: response.state,
                    level: data.level,
                    state: 1,
                    cls: '',
                    label: inputName,
                    animation: 'animated enlarge',
                    type: 'publication'
                });


                // //console.log("root", `.wsElem[type='block'][ws='${wsId}'][cat='${catId}'][item='${blockId}'] .body`);
                addHtml({
                    selector: `.wsElem[type='block'][ws='${data.ws}'][cat='${data.cat}'][item='${data.block}'] .body`,
                    type: 'append',
                    content
                }) //type: html, append, prepend, before, after */

                removeModal();

            } else {
                alertMessageError({
                    title: 'Error',
                    text: response.message
                })
            }




        }).catch(console.warn());

    })

    $("body").on("click", "#" + idFormWs + " .btnActive[type='publication']", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let item = $(this).attr("item");
        let elm = $(this).parents(".wsElem[type='publication']");
        let block = $(this).parents(".wsElem[type='block']").attr("item");
        let selector = `.wsElem[type='publication'][item='${item}']`;
        let state = $(selector).attr("state");

        if (state == "1") {
            state = 0;
            $(selector).attr("state", "0");
            $('.icon', this).removeClass("icon-eye-open").addClass("icon-eye-close");
        } else {
            state = 1;
            $(selector).attr("state", "1");
            $('.icon', this).removeClass("icon-eye-close").addClass("icon-eye-open");
        }

        getData({
            task: 'updateStateRelations',
            return: 'returnState', // returnId, returnState, returnArray
            state,
            item: elm.attr("item"),
            ws: elm.attr("ws"),
            cat: elm.attr("cat"),
            block: block,
            module: 'publications',
        }).then((response) => {
            console.log('updateStateRelations', response);
            if (response.status == 'success') {

            } else {
                alertMessageError({
                    message: response.message
                })
            }
        }).catch(console.warn());

    });

    $("body").on("click", ".btnEdit[type='publication']", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let pubId = $(this).attr("item");
        let _PUBLICATIONS = JSON.parse(localStorage.getItem("_PUBLICATIONS"));
        console.log('_PUBLICATIONS', _PUBLICATIONS);
        let pub = _PUBLICATIONS.find(item => item.id == pubId);
        //console.log('.btnEdit[type=publication]', _PUBLICATIONS, pub);
        editPublication({
            pub,
            item: pubId,
            attr: '',
            system,
            module: 'categorys'
        });
    })

    $("body").on("change", "#inputWorksheet", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        const data = $(this).data();
        const wsId = $(this).val();
        //console.log('inputWorksheet', wsId, data);
        if (wsId != "0") {
            loadWorksheetId({
                wsId,
                module: data.module,
                catId: data.cat,
                catName: data.name
            });
        } else {
            // action new worksheet
        }
    })

    $("body").on("click", "#" + idFormWs + " .btnUpdateWorksheet", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        //console.log('.btnUpdateWorksheet', data);

        let pub = [];
        $("#" + data.form + " .wsElem[type='publication']").each(function (index, elem) {
            let item = $(this).attr("item");
            let state = $(this).attr("state");
            let blockId = $(this).parents('[type="block"]').attr("item");
            let elm = "div.wsElem[type='publication'][item='" + item + "']";
            let order = $(this).parents('[type="block"]').find(elm).index();
            let wsId = $(this).attr("ws");
            let catId = $(this).attr("cat");
            ////console.log('wsElem', item, blockId, wsId, catId, order + 1);
            pub[index] = {
                catId,
                wsId,
                blockId,
                pub: item,
                state,
                order: order + 1
            }
        });

        //console.log('pub', pub);
        data["pubs"] = pub;

        //btnLoading("btnUpdateWorksheet");

        changeBtn({
            id: "btnUpdateWorksheet",
            type: "loading",
            //text: "Actualizando...",
            //icon: "icon icon-loading icon-refresh",
            //action : "disabled"
        })


        updateWorksheet(data).then((response) => {
            //console.log('updateWorksheet', response);
            if (response.status == "success") {
                changeBtn({
                    id: "btnUpdateWorksheet",
                    type: "success",
                    //text: "Actualizando...",
                    //icon: "icon icon-loading icon-refresh",
                    //action : "disabled"
                })

                setTimeout(() => {
                    changeBtn({
                        id: "btnUpdateWorksheet",
                        type: "primary",
                        text: "Actualizar",
                        icon: "",
                    })
                }, 1000);

            } else {
                alertMessageError({
                    title: 'Error',
                    text: response.message
                })
            }
        }).catch(console.warn());
    })

    $("body").on("click", ".btnNewPattern", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        const module = 'categorys';
        const id = 'formPattern';
        let attr = '';
        //console.log('btnNewPattern');

        // load new pattern
        addHtml({
            selector: `.boxModule[module='${module}']`,
            type: 'prepend',
            content: renderModalRename({
                id,
                input: "",
                fn: 'savePattern',
                title: 'Nuevo patrón',
                labelBtnAction: 'Guardar',
                icon: 'icon icon-square-plus',
                text: 'Pon un nombre al patrón',
                module,
                body: `<div class="formControl">
                            <label>Descripcion</label>
                            <textarea type="text" class="formInput" id="inputDescription"></textarea>
                        </div>`,
                attr
            })
        }) //type: html, append, prepend, before, after

        $("#input" + id).focus();


    });

    $("body").on("click", ".btnModalRename[fn='savePattern']", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let inputName = $("#inputformPattern").val();
        let inputDescription = $("#inputDescription").val();
        let count = 0;
        //console.log('btnModalRename savePattern', inputName);

        let pubs = [];
        $("#" + idFormWs + " .wsElem[type='publication']").each(function (index, elem) {
            let item = $(this).attr("item");
            let state = $(this).attr("state");
            let blockId = $(this).parents('[type="block"]').attr("item");
            let elm = "div.wsElem[type='publication'][item='" + item + "']";
            let order = $(this).parents('[type="block"]').find(elm).index();
            let wsId = $(this).attr("ws");
            let catId = $(this).attr("cat");
            ////console.log('wsElem', item, blockId, wsId, catId, order + 1);
            pubs[index] = {
                blockId,
                pub: item,
                state,
                order: order + 1
            }
        });

        //console.log('pubs', pubs);

        if (inputName == "") {
            $("#inputformPattern").focus();
            errorFormInput({
                id: "inputformPattern",
                message: "Añade un nombre al patrón",
                messageId: ""
            });
            count++;
        }

        if (count == 0) {
            changeBtn({
                selector: ".btnModalRename[fn='savePattern']",
                type: "loading",
                text: "Guardando...",
                //icon: "icon icon-loading icon-refresh",
                //action : "disabled"
            })
            savePattern({
                inputName,
                inputDescription,
                pubs
            }).then((response) => {
                //console.log('savePattern', response);
                if (response.status == "success") {
                    changeBtn({
                        selector: ".btnModalRename[fn='savePattern']",
                        type: "success",
                        //text: "Actualizando...",
                        //icon: "icon icon-loading icon-refresh",
                        //action : "disabled"
                    })

                    setTimeout(() => {
                        removeModal();
                        $("#inputPattener").prepend(`<option value="${response.state}" data-description="${inputDescription}">${inputName}</option>`);
                        $("#inputPattener:first").val(response.state).trigger("change");
                    }, 1000);

                } else {
                    alertMessageError({
                        title: 'Error',
                        text: response.message
                    })
                }
            }).catch(console.warn());
        }

    })

    $("body").on("mouseover", "#inputPattener", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let description = $(this).find(":selected").data("description");
        //console.log('#inputPattener', description);
        tooltip({
            selector: "#inputPattener",
            text: description,
            position: "top",
        });
    })

    $("body").on("mouseout", "#inputPattener", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        $(".tooltip").remove();
    })

    $("body").on("click", ".btnDeletePattern", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let patternId = $("#inputPattener").val();
        let pattern = $("#inputPattener").find("option:selected").text();
        const module = 'categorys';
        //console.log('.btnDeletePattern', pattern, patternId);

        addHtml({
            selector: `.boxModule[module='${module}']`,
            type: 'prepend',
            content: renderModalDelete({
                id: "formDeletePattern",
                text: "¿Estás seguro de eliminar el patrón <strong>" + pattern + "</strong>?",
                classBtnAction: 'btnDeletePatternConfirm',
                module,
                attr: `data-pattern-id="${patternId}"`
            })
        }) //type: html, append, prepend, before, after

    })

    $("body").on("click", ".btnDeletePatternConfirm", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let patternId = $(this).attr("data-pattern-id");
        //console.log('.btnDeletePatternConfirm', patternId);

        changeBtn({
            selector: ".btnDeletePatternConfirm",
            type: "loading",
            text: 'Eliminando Patrón',
            action: 'btnWarning'
        })
        deletePattern({
            patternId
        }).then((response) => {
            //console.log('deletePattern', response);
            if (response.status == "success") {
                $("#inputPattener").find("option[value='" + patternId + "']").remove();
                removeModal();
            } else {
                alertMessageError({
                    title: 'Error',
                    text: response.message
                })
                removeModal();
            }
        }).catch(console.warn());
    })

    $("body").on("click", ".btnLoadPattern", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let module = 'categorys';

        let patternId = $("#inputPattener").val();
        let data = $("#inputPattener").data();
        //console.log('.btnLoadPattern', );

        addHtml({
            selector: `.boxModule[module='${module}']`,
            type: 'prepend',
            content: renderModalConfirm({
                id: "formLoadPattern",
                icon: "icon icon-alert-warning iconDanger",
                text: "<strong>¿Estás seguro de cargar el patrón? </strong> <br> Si hay elementos se eliminaran del workspace",
                classBtnAction: 'btnPrimary btnLoadPatternConfirm',
                module,
                attr: `data-pattern-id="${patternId}" data-ws="${data.ws}" data-cat="${data.cat}"`
            })
        }) //type: html, append, prepend, before, after
    })

    $("body").on("click", ".btnLoadPatternConfirm", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let module = 'categorys';
        let data = $(this).data();
        let patternId = data.patternId;
        let wsId = data.ws;
        let catId = data.cat;
        let idForm = 'spaceWorksheet';

        ////console.log('.btnLoadPatternConfirm', module, patternId);

        dataPublications().then((responsePubs) => {
            ////console.log('dataPublications', responsePubs);
            let pubs = responsePubs.data;
            getPatterns().then((getPatternsResponse) => {
                console.log('getPatterns', getPatternsResponse);
                if (getPatternsResponse.status == "success") {
                    let pattern = getPatternsResponse.data.find(item => item.id == patternId);

                    $("#" + idForm + " .wsElem[type='publication']").remove();

                    //console.log('paterns/pubs', JSON.parse(pattern.json), pubs);
                    if (pattern) {
                        let pubsJson = JSON.parse(pattern.json);

                        for (let index = 0; index < pubsJson.length; index++) {
                            const item = pubsJson[index];
                            let pubId = item.pub;
                            let pub = pubs.find(itemPub => itemPub.id == pubId);
                            let blockId = item.blockId;
                            //console.log('data', item, pubId, pub);
                            const content = renderWorksheetElem({
                                wsId,
                                blockId,
                                catId,
                                module,
                                item: pubId,
                                level: index,
                                state: item.state,
                                cls: '',
                                label: pub.name,
                                animation: 'animated enlarge',
                                type: 'publication'
                            });
                            $("#" + idForm + " .wsElem .body[type='block'][item='" + blockId + "']").append(content);
                        }
                    } else {
                        alertMessageError({
                            title: 'Error',
                            text: "No se encontró el patrón" + getPatternsResponse.message
                        })
                    }
                    removeModal();
                } else {
                    alertMessageError({
                        title: 'Error',
                        text: getPatternsResponse.message
                    })
                }

            }).catch(console.warn());
        }).catch(console.warn());

    })

    $(window).resize(function () {
        resizePublications();
    })
});