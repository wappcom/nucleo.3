<?php
header('Content-Type: text/html; charset=utf-8');
class SLIDERS
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function dataId($sliderId = 0)
    {
        //return $var;
        $sql = "SELECT * FROM mod_sliders WHERE mod_sli_id='" . $sliderId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_sli_id"];
            $return["id"] = $id;
            $return["name"] = $row["mod_sli_name"];
            $return["description"] = $row["mod_sli_description"];
            $return["date"] = $row["mod_sli_date"];
            $return["media"] = $this->relationsFiles($id);
            $return["categorys"] = $this->relationCat($id);
            $return["cls"] = $row["mod_sli_cls"];
            $return["json"] = $row["mod_sli_json"];
            $userId = $row["mod_sli_user_id"];
            $return["user"]["userId"] = $userId;
            $return["user"]["userName"] = $this->fmt->users->fullName($userId);
            $return["state"] = $row["mod_sli_state"];

            return $return;
        } else {
            return 0;
        }
    }

    public function getItemSlider(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $rtn = $this->dataId($var["vars"]["inputId"]);

        //$rtn["media"] = $this->relationsFiles($var["vars"]["inputId"]);
        return $rtn;
    }

    public function getDataSliders(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];

        $sql = "SELECT * FROM mod_sliders WHERE mod_sli_ent_id='" . $entitieId . "' ORDER BY mod_sli_id DESC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_sli_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_sli_name"];
                $return[$i]["description"] = $row["mod_sli_description"];
                $return[$i]["cls"] = $row["mod_sli_cls"];
                $return[$i]["json"] = $row["mod_sli_json"];
                $return[$i]["date"] = $row["mod_sli_date"];
                $return[$i]["state"] = $row["mod_sli_state"];
                $userId = $row["mod_sli_user_id"];
                $return[$i]["user"]["userId"] = $userId;
                $return[$i]["user"]["userName"] = $this->fmt->users->fullName($userId);
                $return[$i]["media"] = $this->relationsFiles($id);
                $return[$i]["categorys"] = $this->relationCat($id);
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function getLastSliderCat(int $catId = 0)
    {
        //return $catId;

        $sql = "SELECT * FROM mod_sliders_categorys WHERE mod_sli_cat_cat_id='" . $catId . "' ORDER BY mod_sli_cat_sli_id DESC LIMIT 1";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_sli_cat_sli_id"];
            return $this->dataId($id);
        } else {
            return 0;
        }

    }
    public function addSlider(array $var = null)
    {
        //return $var;

        $entitieId = $var["entitieId"];
        $userId = $var["user"]["userId"];
        $inputs = $var["vars"]["inputs"];

        $title = $inputs["inputTitle"];
        $description = $inputs["inputDescription"];
        $cls = $inputs["inputCls"];
        $json = $inputs["inputJson"];
        $mediaFiles = $inputs["mediaFiles"];
        $categorys = $inputs["categorys"];
        $date = $inputs["inputDate"];
        $state = $inputs["inputState"] ? $inputs["inputState"] : 0;

        $now = $this->fmt->modules->dateFormat();

        $insert = "mod_sli_name,mod_sli_description,mod_sli_cls,mod_sli_json,mod_sli_date_register,mod_sli_date,mod_sli_ent_id,mod_sli_user_id,mod_sli_state";
        $values = "'" . $title . "','" . $description . "','" . $cls . "','" . $json . "','" . $now . "','" . $date . "','" . $entitieId . "','" . $userId . "','" . $state . "'";
        $sql = "insert into mod_sliders (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_sli_id) as id from mod_sliders";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        $id = $row["id"];

        $insert = 'mod_sli_cat_sli_id,mod_sli_cat_cat_id,mod_sli_cat_order';
        $countCategorys = count($categorys);
        for ($i = 0; $i < $countCategorys; $i++) {
            $values = "'" . $id . "','" . $categorys[$i] . "','" . $i . "'";
            $sql = "insert into mod_sliders_categorys (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        $insert = 'mod_sli_file_sli_id,mod_sli_file_file_id,mod_sli_file_order';
        $countFiles = count($mediaFiles);

        for ($i = 0; $i < $countFiles; $i++) {
            $values = "'" . $id . "','" . $mediaFiles[$i] . "','" . $i . "'";
            $sql = "insert into mod_sliders_files (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        return $id;
    }

    public function updateSlider(array $var = null)
    {
        //return $var;

        //$sliderId = $var["sliderId"];
        $inputs = $var["vars"]["inputs"];

        $inputId = $inputs["inputId"];
        $entitieId = $var["entitieId"];
        $title = $inputs["inputTitle"];
        $description = $inputs["inputDescription"];
        $cls = $inputs["inputCls"];
        $date = $inputs["inputDate"];
        $json = $inputs["inputJson"];
        $mediaFiles = $inputs["mediaFiles"];
        $categorys = $inputs["categorys"];
        $state = $inputs["inputState"] ? $inputs["inputState"] : 0;


        $sql = "UPDATE mod_sliders SET
        mod_sli_name = '" . $title . "',
        mod_sli_description = '" . $description . "',
        mod_sli_cls = '" . $cls . "',
        mod_sli_json = '" . $json . "',
        mod_sli_ent_id = '" . $entitieId . "',
        mod_sli_date = '" . $date . "',
        mod_sli_state = '" . $state . "'             
        WHERE mod_sli_id = '" . $inputId . "' ";
        $this->fmt->querys->consult($sql);


        $sql = "DELETE from mod_sliders_categorys where mod_sli_cat_sli_id='" . $inputId . "'";
        $this->fmt->querys->consult($sql, __METHOD__);

        $insert = 'mod_sli_cat_sli_id,mod_sli_cat_cat_id,mod_sli_cat_order';
        $countCategorys = count($categorys);
        for ($i = 0; $i < $countCategorys; $i++) {
            $values = "'" . $inputId . "','" . $categorys[$i] . "','" . $i . "'";
            $sql = "insert into mod_sliders_categorys (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        $sql = "DELETE from mod_sliders_files where mod_sli_file_sli_id='" . $inputId . "'";
        $this->fmt->querys->consult($sql, __METHOD__);

        $insert = 'mod_sli_file_sli_id,mod_sli_file_file_id,mod_sli_file_order';
        $countFiles = count($mediaFiles);
        for ($i = 0; $i < $countFiles; $i++) {
            $values = "'" . $inputId . "','" . $mediaFiles[$i] . "','" . $i . "'";
            $sql = "insert into mod_sliders_files (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        return 1;
    }

    public function relationsFiles($id = 0)
    {
        //return $var;
        $sql = "SELECT mod_sli_file_file_id, mod_sli_file_description, mod_sli_file_link, mod_sli_file_target FROM mod_sliders_files WHERE mod_sli_file_sli_id='" . $id . "' ORDER BY mod_sli_file_order ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_sli_file_file_id"];
                $return[$i]["id"] = $id;
                $return[$i]["description"] = $row["mod_sli_file_description"];
                $data = $this->fmt->files->dataItem($id);
                $return[$i]["data"] = $this->fmt->files->dataItem($id);
                $return[$i]["link"] = $row["mod_sli_file_link"];
                $return[$i]["target"] = $row["mod_sli_file_target"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function relationsCategorys(array $vars = null)
    {
        //return $var;

        $catId = $vars["catId"];
        //$postId = $vars["postId"];
        $entitieId = $vars["entitieId"] ?? 1;
        $order = $this->fmt->emptyReturn($vars["order"], "mod_sli_cat_order ASC");
        $limit = $vars["limit"];
        $and = $vars["and"];
        $filter = $vars["filter"] ?? "";
        $now = $this->fmt->data->dateFormat("", "Y-m-d");

        $avoidIds = $this->fmt->emptyReturn($vars["avoidIds"], "");

        if (!empty($limit)) {
            $limit = "LIMIT " . $limit;
        } else {
            $limit = "LIMIT 0,100";
        }

        if (!empty($and)) {
            $and = "AND " . $and;
        } else {
            $and = "";
        }

        if ($filter == "date<=") {
            $and = $and . " AND  DATE(slider_date) <= '" . $now . "' ";
        }

        if (!empty($avoidIds)) {
            $avoidIds = implode(",", $avoidIds);
            $and = $and . " AND  post_id NOT IN (" . $avoidIds . ") ";
        }


        $sql = "SELECT DISTINCT * FROM mod_sliders,mod_sliders_categorys WHERE mod_sli_cat_cat_id='" . $catId . "' AND mod_sli_cat_sli_id=mod_sli_id  AND mod_sli_state > 0 " . $and . "  ORDER BY " . $order . " " . $limit;

        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_sli_id"];
                $rtn[$i]["id"] = $id;
                $rtn[$i]["name"] = $row["cat_name"];
            }
            return $rtn;
        } else {
            return 0;
        }

    }

    public function relationCat($sliderId = null)
    {
        //return $var;

        $sql = "SELECT * FROM mod_sliders_categorys, categorys WHERE mod_sli_cat_sli_id='" . $sliderId . "' AND mod_sli_cat_cat_id=cat_id ORDER BY mod_sli_cat_order ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["cat_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["cat_name"];
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function delete(array $var = null)
    {
        //return $var;
        $sliderId = $var["vars"]["inputId"];

        $sql = "DELETE FROM mod_sliders WHERE  mod_sli_id = '" . $sliderId . "'";
        $this->fmt->querys->consult($sql, __METHOD__);
        $sql = "ALTER TABLE mod_sliders AUTO_INCREMENT = 1";
        $this->fmt->querys->consult($sql, __METHOD__);


        $sql = "DELETE FROM mod_sliders_categorys WHERE  mod_sli_cat_sli_id = '" . $sliderId . "'";
        $this->fmt->querys->consult($sql, __METHOD__);
        $sql = "ALTER TABLE mod_sliders_categorys AUTO_INCREMENT = 1";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "DELETE FROM mod_sliders_files WHERE  mod_sli_file_sli_id = '" . $sliderId . "'";
        $this->fmt->querys->consult($sql, __METHOD__);
        $sql = "ALTER TABLE mod_sliders_files AUTO_INCREMENT = 1";
        $this->fmt->querys->consult($sql, __METHOD__);

        return 1;

    }
}