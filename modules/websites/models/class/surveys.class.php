<?php
header('Content-Type: text/html; charset=utf-8');
class SURVEYS
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }


    public function responseSurveyId(int $surveyId = 0)
    {
        $options = "5,6,7";
        $return["options"] = $options;

        $sql = "SELECT r.response, COALESCE(SUM(CASE WHEN sr.mod_svy_svr_response IS NOT NULL THEN 1 ELSE 0 END), 0) AS total_responses FROM (SELECT 1 AS response UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4) AS r LEFT JOIN mod_surveys_responses sr ON r.response = sr.mod_svy_svr_response AND sr.mod_svy_svr_survey_id = '".$surveyId."' AND sr.mod_svy_svr_sq_id = 2 GROUP BY r.response ORDER BY r.response;";
            
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_svy_svr_id"];
                $return["data"][$i]["response"] = $row["response"];
                $return["data"][$i]["totalResponses"] = $row["total_responses"];
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function saveVoteSurvey(array $var = null)
    {
        $surveyId = $var["surveyId"];
        $questionId = $var["questionId"];
        $token = $var["token"];
        $voteValue = $var["voteValue"];

        $decode = base64_decode($token);
        $arrayUserDates = explode(".", $decode);
        $userId = $arrayUserDates[1];

        //primero revisar si el voto existe
        $sql = "SELECT * FROM mod_surveys_responses WHERE mod_svy_svr_atk_token  = '" . $token . "' AND mod_svy_svr_sq_id = '" . $questionId . "' AND mod_svy_svr_survey_id = '" . $surveyId . "';";
        $rs = $this->fmt->querys->consult($sql,__METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            return 0;
        } else {    
            //si no existe insertar el voto

            $now = $this->fmt->data->now();

            $insert = "mod_svy_svr_acu_id,mod_svy_svr_survey_id,mod_svy_svr_sq_id,mod_svy_svr_response,mod_svy_svr_atk_token,mod_svy_svr_date,mod_svy_svr_state";
            $values ="'" . $userId . "', '" . $surveyId . "', '" . $questionId . "', '" . $voteValue . "', '" . $token . "', '" . $now . "', '1'";
            $sql= "insert into  mod_surveys_responses (".$insert.") values (".$values.")";
            $this->fmt->querys->consult($sql,__METHOD__);

            $sql = "select max(mod_svy_svr_id) as id from mod_surveys_responses";
            $rs = $this->fmt->querys->consult($sql, __METHOD__);
            $row = $this->fmt->querys->row($rs);

            return $row["id"];
        }

        
        
    }

}