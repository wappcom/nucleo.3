<?php
header('Content-Type: text/html; charset=utf-8');
class MEDIA
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function getMediaList(array $var = null)
    {
        //return $var;
        $return = [];
        $entitieId = $var["entitieId"];
        $sql = "SELECT * FROM files WHERE file_ext IN ('jpeg','png','svg','gif','jpg') AND file_ent_id='" . $entitieId . "'";

        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);

        //return ['entitie'=>$entitieId, 'sql'=>$sql, 'num' =>$num];
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["file_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["file_name"];
                $return[$i]["description"] = $row["file_description"];
                $return[$i]["pathurl"] = $row["file_pathurl"];
                $return[$i]["thumb"] = $this->fmt->files->urlAdd($row["file_pathurl"], "-thumb");
                $return[$i]["embed"] = $row["file_embed"];
                $return[$i]["btnTitle"] = $row["file_btn_title"];
                $return[$i]["ext"] = $row["file_ext"];
                $return[$i]["alt"] = $row["file_alt"];
                $return[$i]["title"] = $row["file_title"];
                $return[$i]["filename_md5"] = $row["file_filename_md5"];
                $return[$i]["datatime"] = $row["file_datatime"];
                $return[$i]["state"] = $row["file_state"];
            }
            return $return;
        } else {
            return 0;
        }
        //$this->fmt->querys->leave($rs);
    }

    public function deleteMediaItem(array $var = null)
    {
        //return $var;

        $fileId = $var["vars"]["inputs"]["inputId"];


        $returnDelete = $this->fmt->files->deteleFileId($fileId);

        if ($returnDelete == 1) {
            $rtn["Error"] = 0;
            $rtn["status"] = "success";
            $rtn["message"] = "Eliminado con exito";
            return $rtn;
        } else {
            $rtn["Error"] = 1;
            $rtn["status"] = "error";
            $rtn["message"] = "Error al eliminar";
            return $rtn;
        }

    }

    public function deleteMediaItems(array $var = null)
    {
        //return $var;

        $fileIds = $var["vars"]["inputs"]["inputIds"];
        $count = 0;
        /* $fileIds.foreach(function($fileId){
            $returnDelete = $this->fmt->files->deteleFileId($fileId);
        }); */

        $file = [];

        foreach ($fileIds as $fileId) {
            //$count++;
            $rtnDelete = $this->fmt->files->deteleFileId($fileId);
            if ($rtnDelete != 1) {
                $count++;
            }
            //$file[$count] = $rtnDelete;
        }

        //return $file;

        if ($count == 0) {
            $rtn["Error"] = 0;
            $rtn["status"] = "success";
            $rtn["message"] = "Eliminado con exito";
            return $rtn;
        } else {
            $rtn["Error"] = 1;
            $rtn["status"] = "error";
            $rtn["message"] = "Error al eliminar";
            return $rtn;
        }

    }


}