<?php
header('Content-Type: text/html; charset=utf-8');
class TEMPLATES_ED
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function getTemplatesEd(array $var = null){
        //return $var;
        $entitieId = $var["entitieId"];
        $userId = $var["userId"];
        $active = $var["vars"]["active"] ? 1 : 0;
        $order = $this->fmt->emptyReturn($var["vars"]["orderBy"], "ORDER BY mod_edc_tpl_name DESC");

        $where = '';
        if ($active == 1){
             $where = " AND mod_edc_tpl_state >= 1";
        }
        
        //verificar-permisos
        
        $sql = "SELECT * FROM mod_external_docs_templates WHERE mod_edc_tpl_ent_id = '".$entitieId."'" . $where;
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for ($i=0; $i < $num; $i++) { 
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_edc_tpl_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_edc_tpl_name"];
                $return[$i]["description"] = $row["mod_edc_tpl_description"];
                $return[$i]["code"] = $row["mod_edc_tpl_code"];
                $return[$i]["tags"] = $row["mod_edc_tpl_tags"];
                $return[$i]["typeId"] = $row["mod_edc_tpl_type_id"];
                $return[$i]["pageDesignId"] = $row["mod_edc_tpl_pd_id"];
                
                $return[$i]["registerUserId"] = $row["mod_edc_tpl_register_user_id"];
                $return[$i]["registerDate"] = $row["mod_edc_tpl_register_date"];
                $return[$i]["json"] = $row["mod_edc_tpl_json"];
                $body = $row["mod_edc_tpl_body"];
                $numPages = 2;
                

                $return[$i]["body"] = $body;
                $return[$i]["numPages"] = $numPages;
                $return[$i]["state"] = $row["mod_edc_tpl_state"];
                
            }

            return $return;
            
        }else{
            return 0;
        }
    }

    public function getDataTemplates(array $var = null)
    {
        //return $var;

        require_once(_PATH_NUCLEO . "modules/externaldocuments/models/class/class.externaldocuments.php");
        $externalDocuments = new EXTERNALDOCUMENTS($this->fmt);
        
        $return["pageDesing"] = $this->getPageDesing($var);
        $return["typesEd"] = $externalDocuments->getTypesDocumentsEd($var);
        $return["templatesEd"] = $this->getTemplatesEd($var);

        return $return;
    }

    public function getPageDesing(array $var = null)
    {
        $entitieId = $var["entitieId"];

        $sql = "SELECT * FROM mod_external_docs_page_design WHERE mod_edc_pd_ent_id = $entitieId";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $id = $row["mod_edc_pd_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_edc_pd_name"];
                $return[$i]["url"] = $row["mod_edc_pd_url"];
                $return[$i]["json"] = $row["mod_edc_pd_json"];
                $return[$i]["state"] = $row["mod_edc_pd_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function addTemplateEd(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $userId = $var["user"]["userId"];
        $inputName = $var["vars"]["inputs"]["inputName"];
        $inputDescription = $var["vars"]["inputs"]["inputDescription"];
        $inputCode = $var["vars"]["inputs"]["inputCode"];
        $inputTags = $var["vars"]["inputs"]["inputTags"];
        $inputTypeEd = $var["vars"]["inputs"]["inputTypeEd"];
        $pd = $var["vars"]["inputs"]["inputPageDesing"];
        $inputPageDesing = $pd ? $pd : 0;
        $inputBody = $var["vars"]["inputs"]["inputBody"];
        $inputState = $var["vars"]["inputs"]["inputState"] ? 1 : 0;
        $inputJson = $var["vars"]["inputs"]["inputJson"];
        $now = $this->fmt->modules->dateFormat();

       
        if ($this->existInternalCode($inputCode) != 0) {
            $rtn["Error"] = 1;
            $rtn["status"] = "error";
            $rtn["message"] = "El código existe, por favor ingrese otro.";
            $rtn["data"] = 'error-code';
            return $rtn;
        }

        $insert = "mod_edc_tpl_name,mod_edc_tpl_description,mod_edc_tpl_code,mod_edc_tpl_tags,mod_edc_tpl_type_id,mod_edc_tpl_pd_id,mod_edc_tpl_body,mod_edc_tpl_register_user_id,mod_edc_tpl_register_date,mod_edc_tpl_json,mod_edc_tpl_ent_id,mod_edc_tpl_state";

        $values ="'" .$inputName. "','" .$inputDescription. "','" .$inputCode. "','" .$inputTags. "','" .$inputTypeEd. "','" .$inputPageDesing. "','".$inputBody."','".$userId."','".$now."','".$inputJson."','".$entitieId."','".$inputState."'";
        $sql= "insert into  mod_external_docs_templates(".$insert.") values (".$values.")";
        $this->fmt->querys->consult($sql,__METHOD__);

        $sql = "select max(mod_edc_tpl_id) as id from mod_external_docs_templates";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        $rtn["Error"] = 0;
        $rtn["status"] = "success";
        $rtn["message"] = "Template creado con éxito.";
        $rtn["data"] = $row["id"];

        return $rtn;
        
    }


    public function existInternalCode(string $code = "")
    {
        //return $var;
 
        $code = $this->fmt->clearString($code);
        
        $sql = "SELECT * FROM mod_external_docs_templates WHERE mod_edc_tpl_code = '".$code."'";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            return 1;
        } else {
            return 0;
        } 
        
    }

    public function deleteTemplateEd(array $var = null)
    {
        $entitieId = $var["entitieId"];
        $userId = $var["userId"];
        $itemId = $var["vars"]["item"];

        $sql = "DELETE FROM mod_external_docs_templates WHERE mod_edc_tpl_id = '".$itemId."'";
        $this->fmt->querys->consult($sql,__METHOD__);

        return 1;
    }

}