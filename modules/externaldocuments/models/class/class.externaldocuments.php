<?php
header('Content-Type: text/html; charset=utf-8');
class EXTERNALDOCUMENTS
{

    var $fmt;
    var $templatesEd;
    var $accounts;

    function __construct($fmt)
    {
        require_once(_PATH_NUCLEO . "modules/externaldocuments/models/class/class.templates-ed.php");
        require_once(_PATH_NUCLEO . "modules/accounts/models/class/class.accounts.php");
        $this->fmt = $fmt;
        $this->templatesEd = new TEMPLATES_ED($fmt);
        $this->accounts = new ACCOUNTS($fmt);
    }

    public function getDocuments(array $var = null){
        return $var;
        /*         $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        $orderBy = $this->fmt->emptyReturn($array["vars"]["orderBy"], "DESC");
        $limit = $this->fmt->emptyReturn($array["vars"]["limit"], "");

        
        if ($limit != "") {
            $limit = "LIMIT " . $limit;
        }

        $sql = "SELECT DISTINCT * FROM mod_customers_enterprises WHERE mod_cen_ent_id = '" . $entitieId . "' ORDER BY  mod_cen_register_date " . $orderBy  . $limit;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return["cen"][$i]["id"] = $row["mod_cen_id"];
                $return["cen"][$i]["name"] = $row["mod_cen_name"];
                $return["cen"][$i]["username"] = $row["mod_cen_username"];
                $return["cen"][$i]["code"] = $row["mod_cen_code"];
                $dataArray = $this->customersData($row["mod_cen_id"]);
                $return["cen"][$i]["businessName"] = $dataArray["businessName"];
                $return["cen"][$i]["nit"] = $dataArray["nit"];
                $return["cen"][$i]["priority"] = $dataArray["priority"];
                $return["cen"][$i]["typeContract"] = $dataArray["typeContract"];
                $return["cen"][$i]["description"] = $row["mod_cen_description"];
                
                if ($row["mod_cen_img"]) {
                    $img["id"] = $row["cont_img"];
                    $img["pathurl"] = $this->fmt->files->imgId($row["cont_img"], "");
                    $img["name"] = $this->fmt->files->name($row["cont_img"], "");
                } else {
                    $img = 0;
                }

                $return["cen"][$i]["img"] = $img;
                $return["cen"][$i]["account"]["acuId"] = $row["mod_cen_acu_id"];
                $return["cen"][$i]["registerDate"] = $row["mod_cen_register_date"];
                $return["cen"][$i]["state"] = $row["mod_cen_state"];
            }
        }else {
            $return["cen"] = 0;
        }
        $this->fmt->querys->leave($rs);
        
       
        
        return $return; */
        
        
    }

    public function getConfigEd(array $var = null){
        //return $var;
        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        $count = 0;

        $arrayUsersApproved = $this->getOptionValue(["entitieId" => $entitieId, "name"=>"usersApprovedForConfigurations"]);

        foreach ($arrayUsersApproved as $key => $value) {
            $userIdApproved =  $value["value"];

            if ($userId == $userIdApproved) {
                $count++;
            }

        }

        $return["sidebar"][0]["content"] = 'configEd';    
        $return["sidebar"][0]["label"] = 'Configuración';    
        $return["sidebar"][0]["icon"] = 'icon icon-conf';
        $return["configEd"]["content"] = "Config general por usuario";


        if ($count > 0) {
            $return["sidebar"][0]["content"] = 'configEd';    
            $return["sidebar"][0]["label"] = 'Configuración';    
            $return["sidebar"][0]["icon"] = 'icon icon-conf';
            $return["sidebar"][1]["content"] = 'typesDocumentsEd';
            $return["sidebar"][1]["label"] = 'Tipos de documentos';
            $return["sidebar"][1]["icon"] = 'icon icon-category-r';
            $return["configEd"]["content"] = "Config de ID configuradores";
        }

        return $return;

    }


    public function getOptions(array $var = null){
        //return $var;
        $entitieId = $var["entitieId"];
        $sql = "SELECT * FROM mod_external_docs_options WHERE mod_edc_opt_ent_id = '".$entitieId."'";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $id = $row["mod_inv_opt_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_inv_opt_name"];
                $return[$i]["value"] = $row["mod_inv_opt_value"];
                $return[$i]["autoload"] = $row["mod_inv_opt_autoload"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function getOptionValue(array $var = null){
        //return $var;
        $entitieId = $var["entitieId"];
        $name = $var["name"];

        $sql = "SELECT * FROM mod_external_docs_options WHERE mod_edc_opt_name = '".$name."' AND mod_edc_opt_ent_id = '".$entitieId."'";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
             for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $id = $row["mod_edc_opt_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_edc_opt_name"];
                $return[$i]["value"] = $row["mod_edc_opt_value"];
                $return[$i]["autoload"] = $row["mod_edc_opt_autoload"];
            }
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function getTypesDocumentsEd(array $var = null){
        //return $var;
        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        $count = 0;

        $arrayUsersApproved = $this->getOptionValue(["entitieId" => $entitieId, "name"=>"usersApprovedForConfigurations"]);

        foreach ($arrayUsersApproved as $key => $value) {
            $userIdApproved =  $value["value"];

            if ($userId == $userIdApproved) {
                $count++;
            }
        }

        if ($count > 0){
           $sql = "SELECT * FROM mod_external_docs_types WHERE mod_edc_type_ent_id = '".$entitieId."'";
           $rs =$this->fmt->querys->consult($sql,__METHOD__);
           $num=$this->fmt->querys->num($rs);
           if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $id = $row["mod_edc_type_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_edc_type_name"];
                $return[$i]["description"] = $row["mod_edc_type_description"];
                $return[$i]["color"] = $row["mod_edc_type_color"];
                $return[$i]["parentId"] = $row["mod_edc_type_parent_id"];
                $return[$i]["state"] = $row["mod_edc_type_state"];
            }
            return $return;
           } else {
            return 0;
           }
        }
    }



    public function getDataNewEd(array $var = null)
    {
        //return $var;
        //$rtn["var"] = $var;
        $rtn["entitieId"] = $var["entitieId"];
        $rtn["entitieData"] = $this->accounts->getEntitieData($var["entitieId"]);
        $rtn["userData"] = $var["user"];
        $now = $this->fmt->data->dateFormat();
        $rtn["timeData"]["now"] = $now;
        $rtn["timeData"]["literal"] = $this->fmt->data->formatDate(["date" => $now]);


        $var["vars"]["active"] = 1;
        $rtn["listsTemplatesEd"] = $this->templatesEd->getTemplatesEd($var);
        $rtn["pageDesigns"] = $this->templatesEd->getPageDesing($var);
        //$rtn["dataUser"];

        return $rtn;
    }
    
}