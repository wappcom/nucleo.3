import {
    empty,
    replacePath,
    replaceEssentials,
    replaceAll,
    loadView,
    stopLoadingBar,
    emptyReturn,
    loadingBtnIcon,
    btnLoadingRemove,
    unDisableId,
    accessToken,
    removeBtnLoading,
    alertMessageError,
    dataModule,
    alertPage,
    jointActions,
    addHtml
} from "../../components/functions.js";

import {
    editorText,
    errorInput,
    errorInputSelector,
    loadInitChargeMedia,
    loadItemMedia,
    loadFilesFormLoader,
    loadSingleFileFormLoader,
    loadChargeMediaInit,
    removalMediaItems,
    ckeckboxChecked
} from "../../components/forms.js";

import {
    categorysListTable,
    createTable,
    dataTable,
    removeItemTable,
    inactiveBtnStateItem,
    activeBtnStateItem,
    mountTable,
    colCheck,
    colId,
    colName,
    colTitle,
    colState,
    colActionsBase,
} from "../../components/tables.js";

import {
    renderBtnStateSwitch,
    renderSelectHoursStartEnd,
    renderSelect,
    renderSelectOptions
} from "../../components/renders/renderForms.js";

import {
    innerForm,
    removeInnerForm,
    renderDeleteModalFn
} from "../../components/renders/renderModals.js";


import {
    renderCol,
    renderColCheck,
    renderColId,
    renderColTitle,
    renderRowsTable,
    renderColState,
    renderColActions,
    renderNoDataTable
} from "../../components/renders/renderTables.js";



let module = "templatesEd";
let system = "externaldocuments";
let pathurl = "modules/externaldocuments/";
let formId = "formTemplateEd";
let tableId = "tableTemplateEd";

export const templatesEdIndex = (vars = []) => {
    //console.log('templatesEdIndex',vars);
    getData({
        task: "getDataTemplates",
        return: "returnArray",
    }).then((responseData) => {
        console.log('getDataTemplates', responseData);
        let data = responseData.data.templatesEd;
        let typesEd = responseData.data.typesEd;
        let pageDesing = responseData.data.pageDesing;

        loadView(_PATH_WEB_NUCLEO + "modules/externaldocuments/views/templatesEd.html?" + _VS).then((responseView) => {

            let content = replaceEssentials({
                str: responseView,
                module,
                system,
                pathurl,
                icon: dataModule(module, "icon"),
                color: dataModule(module, "color"),
                name: dataModule(module, "name"),
                btnName: 'Nueva Plantilla',
                btnIcon: 'icon icon-plus',
                btnId: 'btnNewTemplateEd',
                btnAttr: ``,
            });
            content = content.replace(/{{module}}/g, module);
            addHtml({
                selector: `.bodyModule[module='${module}']`,
                type: 'insert',
                content // insert, append, prepend, replace
            });

            if (data == 0) {
                addHtml({
                    selector: `.bodyModule[module="${module}"] .tbody`,
                    type: 'prepend',
                    content: renderNoDataTable()
                }) //type: html, append, prepend, before, after

                return false;
            }

            let rows = "";
            for (let i in data) {
                let item = data[i];
                let type = item.typeId;
                let dataType = typesEd.find((e) => e.id == type);
                //console.log("🚀 ~ file: templatesEd.js:130 ~ loadView ~ dataType:", dataType)

                rows += renderRowsTable({
                    id: item.id,
                    content: renderColCheck({
                            id: item.id
                        }) +
                        renderColId({
                            id: item.id
                        }) +
                        renderCol({
                            id: item.id,
                            data: item.name,
                        }) +
                        renderCol({
                            id: item.id,
                            data: item.code,
                        }) +
                        renderColTitle({
                            id: item.id,
                            title: item.description,
                        }) +
                        renderCol({
                            id: item.id,
                            data: dataType.id + ' : ' + dataType.name,
                        }) +
                        renderColState({
                            id: item.id,
                            state: item.state,
                            module,
                            system,
                        }) +
                        renderColActions({
                            id: item.id,
                            name: item.name,
                            type: "btnViewFn,btnEditFn,btnDeleteFn",
                            module,
                            system,
                        }),
                });
            }

            mountTable({
                id: tableId,
                columns: [
                    ...colCheck,
                    ...colId,
                    ...colName,
                    {
                        label: "Codigo Interno",
                        cls: "colName",
                    },
                    {
                        label: "Descripción",
                        cls: "colName",
                    },
                    {
                        label: "Tipo de documento",
                        cls: "colName",
                    },
                    ...colState,
                    ...colActionsBase,
                ],
                rows,
                module,
                system,
                container: ".bodyModule[module='" + module + "'] .tbody",
            });

        }).catch(console.warn());
    }).catch(console.warn());
}

export const newEdTemplateForm = (vars = []) => {
    //console.log('newEdTemplateForm',vars);
    getData({
            task: "getDataTemplates",
            return: "returnArray",
        })
        .then((responseData) => {
            console.log('getDataTemplates', responseData, responseData.data.pageDesing);

            let pageDesign = responseData.data.pageDesing;
            let typesEd = responseData.data.typesEd;

            loadView(_PATH_WEB_NUCLEO + "modules/externaldocuments/views/templatesForm.html?" + _VS)
                .then((responseView) => {

                    loadView(_PATH_WEB_NUCLEO + "modules/externaldocuments/views/infoTemplatesFormContent.html?" + _VS)
                        .then((responseInfo) => {
                            //console.log('loadView', responseView);
                            let content = responseView;
                            content = replaceAll(content, "{{_MODULE}}", module);
                            content = replaceAll(content, "{{_SYSTEM}}", system);
                            content = replaceAll(content, "{{_BTNS_TOP}}", "");
                            content = replaceAll(content, "{{_BTN_ACTION}}", "btnSaveTemplateEd");
                            content = replaceAll(
                                content,
                                "{{_BTN_NAME_ACTION}}",
                                "Guardar Nueva Plantilla"
                            );
                            content = replaceAll(content, "{{_FORM_ID}}", formId);
                            content = replaceAll(content, "{{_TITLE}}", "Nueva Plantilla");
                            content = replaceAll(content, "{{_INFO}}", responseInfo);
                            content = replaceAll(content, "{{_PAGE_DESING}}", renderSelectOptions({
                                options: pageDesign,
                                selectedDefaultValue: 1,
                                //selected: eventId,
                            }));

                            content = replaceAll(content, "{{_TYPES_DOCUMENTS}}", renderSelectOptions({
                                options: typesEd,
                                selectedDefaultValue: 0,
                                optionInitialLabel: "Seleccionar Tipo de Documento",
                                //selected: eventId,
                            }));

                            innerForm({
                                module,
                                body: content,
                            });

                            editorText("inputBody");
                        })
                        .catch(console.warn());
                })
                .catch(console.warn());
        })
        .catch(console.warn());

}

export const saveEdTemplateForm = (vars = []) => {
    console.log('saveEdTemplateForm', vars);
    let formId = vars.formId;
    //get name to form inputName value
    let inputName = document.getElementById(formId).querySelector('#inputName').value;
    let inputDescription = document.getElementById(formId).querySelector('#inputDescription').value;
    let inputCode = document.getElementById(formId).querySelector('#inputCode').value;
    let inputTags = document.getElementById(formId).querySelector('#inputTags').value;
    let inputState = document.getElementById(formId).querySelector('#inputState').value;
    let inputPageDesing = document.getElementById(formId).querySelector('#inputPageDesing').value;
    let inputTypeEd = document.getElementById(formId).querySelector('#inputTypeEd').value;
    let inputBody = document.getElementById(formId).querySelector('#inputBody').value;
    let inputJson = document.getElementById(formId).querySelector('#inputJson').value;
    let count = 0;

    if (!inputName) {
        errorInputSelector(`#${formId} #inputName`, 'El nombre es requerido');
        count++;
    }

    if (!inputCode) {
        errorInputSelector(`#${formId} #inputCode`, 'El código interno es requerido');
        count++;
    }

    if (inputTypeEd == 0) {
        errorInputSelector(`#${formId} #inputTypeEd`, 'El tipo de documento es requerido');
        count++;
    }

    if (count == 0) {
        getData({
            task: 'addTemplateEd',
            return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
            inputs: {
                inputName,
                inputDescription,
                inputCode,
                inputTags,
                inputState,
                inputPageDesing,
                inputTypeEd,
                inputBody,
                inputJson
            }
        }).then((response) => {
            console.log('getData addTemplateEd', response);
            if (response.status == 'success') {
                removeInnerForm();
                templatesEdIndex();
            }
            if (response.status == 'error') {

                if (response.data == 'error-code') {
                    document.getElementById(formId).querySelector('#inputCode').value = '';
                    errorInputSelector(`#${formId} #inputCode`, inputCode + ":" + response.message);
                }

                alertMessageError({
                    message: response.message
                })
            }
        }).catch(console.warn());
    }
}

export const editItemTableModal = (vars = []) => {
    console.log('editItemTableModal', vars);
    getData({
            task: "getDataTemplates",
            return: "returnArray",
        })
        .then((responseData) => {
            //console.log('getDataTemplates', responseData, responseData.data.pageDesing);

            let pageDesign = responseData.data.pageDesing;
            let typesEd = responseData.data.typesEd;
            let templatesEdArray = responseData.data.templatesEd;

            let item = templatesEdArray.find((e) => e.id == vars.id);
            console.log('item', item);

            loadView(_PATH_WEB_NUCLEO + "modules/externaldocuments/views/templatesForm.html?" + _VS)
                .then((responseView) => {

                    loadView(_PATH_WEB_NUCLEO + "modules/externaldocuments/views/infoTemplatesFormContent.html?" + _VS)
                        .then((responseInfo) => {
                            //console.log('loadView', responseView);


                            let content = responseView;
                            content = replaceAll(content, "{{_MODULE}}", module);
                            content = replaceAll(content, "{{_SYSTEM}}", system);
                            content = replaceAll(content, "{{_BTNS_TOP}}", "");
                            content = replaceAll(content, "{{_BTN_ACTION}}", "btnUpdateTemplateEd");
                            content = replaceAll(
                                content,
                                "{{_BTN_NAME_ACTION}}",
                                "Actualizar Plantilla"
                            );
                            content = replaceAll(content, "{{_FORM_ID}}", formId);
                            content = replaceAll(content, "{{_TITLE}}", "Editar Plantilla");
                            content = replaceAll(content, "{{_INFO}}", responseInfo);
                            content = replaceAll(content, "{{_PAGE_DESING}}", renderSelectOptions({
                                options: pageDesign,
                                selectedDefaultValue: 1,
                                selected: item.pageDesignId,
                            }));

                            content = replaceAll(content, "{{_TYPES_DOCUMENTS}}", renderSelectOptions({
                                options: typesEd,
                                selectedDefaultValue: 0,
                                optionInitialLabel: "Seleccionar Tipo de Documento",
                                selected: item.typeId,
                            }));

                            innerForm({
                                module,
                                body: content,
                            });



                            document.getElementById(formId).querySelector('#inputId').value = item.id;
                            document.getElementById(formId).querySelector('#inputName').value = item.name;
                            document.getElementById(formId).querySelector('#inputType').value = item.typeId;
                            document.getElementById(formId).querySelector('#inputDescription').value = item.description;
                            document.getElementById(formId).querySelector('#inputCode').value = item.code;
                            document.getElementById(formId).querySelector('#inputTags').value = item.tags;
                            document.getElementById(formId).querySelector('#inputBody').innerHTML = item.body;
                            document.getElementById(formId).querySelector('#inputJson').innerHTML = item.json;

                            document.getElementById(formId).querySelector('#inputState').value = item.state;


                            editorText("inputBody");


                        })
                        .catch(console.warn());
                })
                .catch(console.warn());
        })
        .catch(console.warn());
}

export const deleteItemTableModal = (vars = []) => {
    console.log('deleteItemTable', vars);
    addHtml({
        selector: `body`,
        type: "append", // insert, append, prepend, replace
        content: renderDeleteModalFn({
            name: vars.name,
            id: "modalDelete",
            module,
            attr: `data-module="${module}" data-id="${vars.id}"`,
        }),
    });
}

export const deleteItemTable = (vars = []) => {
    console.log('deleteItemTable', vars);

    getData({
        task: 'deleteTemplateEd',
        return: 'returnState', // returnId, returnState, returnArray, returnMessage, returnObject
        item: vars.id
    }).then((response) => {
        console.log('getData de', response);
        if (response.status == 'success') {

        } else {
            alertMessageError({
                message: response.message
            })
        }
    }).catch(console.warn());

}


export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/externaldocuments/controllers/apis/templates-ed.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        jointActions(res.data)
        return res.data
    } catch (error) {
        console.log("Error: getData ", error);
    }
}

document.addEventListener("click", function (e) {
    if (e.target.id == 'btnNewTemplateEd') {
        newEdTemplateForm();
    }

    if (e.target.id == 'btnSaveTemplateEd') {
        saveEdTemplateForm({
            formId: 'formTemplateEd'
        });
    }

    if (e.target.id == 'btnEditTemplateEd') {
        //btnDeleteFn
    }

    if ((/btnEditFn/).test(e.target.className)) {
        //obtener dataset 
        let data = e.target.dataset;
        //let data = document.getElementById('btnCampaigns').dataset;
        editItemTableModal(data);
    }

    if ((/btnDeleteFn/).test(e.target.className)) {
        //obtener dataset 
        let data = e.target.dataset;
        //let data = document.getElementById('btnCampaigns').dataset;
        deleteItemTableModal(data);
    }

    if ((/btnDeleteConfirmationTemplatesEd/).test(e.target.className)) {
        //obtener dataset 
        let data = e.target.dataset;
        //let data = document.getElementById('btnCampaigns').dataset;
        deleteItemTable(data);
    }
});