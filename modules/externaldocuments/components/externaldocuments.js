import {
	empty,
	replacePath,
	replaceEssentials,
	replaceAll,
	loadView,
	stopLoadingBar,
	emptyReturn,
	loadingBtnIcon,
	btnLoadingRemove,
	unDisableId,
	accessToken,
	removeBtnLoading,
	alertMessageError,
	dataModule,
	alertPage,
	addHtml
} from "../../components/functions.js";


import {
	editorText,
	errorInput,
	loadInitChargeMedia,
	loadItemMedia,
	loadFilesFormLoader,
	loadSingleFileFormLoader,
	loadChargeMediaInit,
	removalMediaItems,
	validateNum,
} from "../../components/forms.js";

import {
	deleteModalItem,
	removeModal,
	closeModal,
} from "../../components/modals.js";
import {
	categorysListTable,
	createTable,
	dataTable,
	removeItemTable,
	inactiveBtnStateItem,
	activeBtnStateItem,
	mountTable,
	colCheck,
	colId,
	colName,
	colTitle,
	colState,
	colActionsBase,
	deleteItemModule,
	renderEmpty,
	mountMacroTable,
	clearMacroTable
} from "../../components/tables.js";
import {
	renderCol,
	renderColCheck,
	renderColId,
	renderColTitle,
	renderRowsTable,
	renderColState,
	renderColActions,
	renderColCategorys,
} from "../../components/renders/renderTables.js";
import {
	innerForm,
	renderModalConfig,
	btnSidebarModalBtn,
	renderModalClean
} from "../../components/renders/renderModals.js";

import{
	renderPageEd,
} from "./renders/renderEd.js";

/* renderColTitle,
	renderColState,
	renderColActionsBase, */
let pathurl = "modules/externaldocuments/";
let module = "externaldocuments";
let system = "externaldocuments";
let tableId = "tableExternalDocuments";


export const externaldocumentsIndex = (vars = []) => {
	//let {module, system, moduleReturn} = vars;
	let systemReturn = vars.system ? vars.system : system;
	let moduleReturn = vars.moduleReturn ? vars.moduleReturn : module;

	//console.log("externaldocumentsIndex", module, system, pathurl);
	//console.log("externaldocumentsIndex", vars, module);

	loadView(_PATH_WEB_NUCLEO + pathurl + "views/externaldocuments.html?" + _VS).then(
		(htmlView) => {
			//console.log(htmlView, module, system);
			getData({
				task: 'getDocuments',
				return: 'returnArray', // returnId, returnState, returnArray
			}).then((response) => {
				console.log('getData getDocuments', response);
				if (response.status == 'success') {
					stopLoadingBar();

					let str = htmlView;


					$(".innerForm").remove();
					//console.log("str", module)
					addHtml({
						selector: `.bodyModule[module='${moduleReturn}']`,
						type: 'insert', // insert, append, prepend, replace
						content: replaceEssentials({
							str,
							module,
							system,
							pathurl,
							name: dataModule(module, "name"),
							fn: "formNewCustomerCompany",
							btnNameAction: "Nuevo Cliente Empresa",
							icon: dataModule(module, "icon"),
							color: dataModule(module, "color"),
						})
					})


				} else {
					alertMessageError({
						message: response.message
					})
				}
			}).catch(console.warn());
		}
	);
};

export const loadConfigEd = (vars = []) => {
	console.log('loadConfigEd', vars);
	getData({
		task: 'getConfigEd',
		return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
	}).then((response) => {
		console.log('getData getConfigEd', response);
		if (response.status == 'success') {
			let data = response.data;




			loadView(_PATH_WEB_NUCLEO + "modules/externaldocuments/views/config.html?" + _VS).then((responseView) => {
				//console.log('loadView', responseView);
				let modalId = 'modalConfigEd';
				let str = responseView;

				let sidebarArray = data.sidebar;
				let sidebar = '';
				let contentConfig = '';

				if (sidebarArray != 0) {
					sidebarArray.forEach((element, index) => {
						//console.log ("sidebarArray",index, element);
						const icon = element.icon;
						const label = element.label;
						const content = element.content;

						let state = ``;
						if (index == 0) {
							state = 'active';
						}

						sidebar += btnSidebarModalBtn({
							icon,
							label,
							dataContent: content,
							dataFor: 'contentConfigEd',
							modalId,
							module,
							state
						})

						contentConfig += `<div class="content" id="contentConfigEd">//aqui config</div>`;

					});
				}

				str = str.replace(/{{_MODULE}}/g, module);
				str = str.replace(/{{_SYSTEM}}/g, system);
				str = str.replace(/{{_MODAL_ID}}/g, modalId);
				str = str.replace(/{{_SIDEBAR_CONFIG}}/g, sidebar);
				str = str.replace(/{{_CONTENT_CONFIG}}/g, contentConfig);



				addHtml({
					selector: `body`,
					type: 'prepend',
					content: renderModalConfig({
						module,
						system,
						attr: ``,
						id: modalId,
						body: str
					}) // insert, append, prepend, replace
				})
			}).catch(console.warn());
		} else {
			alertMessageError({
				message: response.message
			})
		}
	}).catch(console.warn());



}

export const typesDocumentsEdIndex = (vars = []) => {
	console.log('typesDocumentsIndex', vars);
	getData({
		task: 'getTypesDocumentsEd',
		return: 'returnArray'
	}).then((responseData) => {
		console.log('getData getTypesDocumentsEd', responseData);
		loadView(_PATH_WEB_NUCLEO + "modules/externaldocuments/views/typesDocumentsEd.html?" + _VS).then((response) => {
			let content = response;

			let tableMacro = mountMacroTable({
				tableId: "tableTypesDocumentsEd",
				data: responseData.data,
				cols: [{
					label: "id",
					cls: 'colId',
					type: "id",
					render: 'id',
				}, {
					label: "*Nombre",
					cls: 'colName',
					type: "text",
					render: 'name',
					attrName: 'inputName',
					id: 'inputName',
				}, {
					label: "Descripción",
					cls: 'colDescription',
					type: "text",
					render: 'description',
					attrName: 'inputDescription',
					id: 'inputDescription',
				}, {
					label: "*Color Hex",
					cls: 'colorHex',
					type: "text",
					render: 'color',
					attrName: 'inputColor',
					id: 'inputColor',
				}, {
					label: "*Id Padre",
					cls: 'colParentId',
					type: "bit",
					render: 'parentId',
					attrName: 'inputParentId',
					id: 'inputParentId',
				}, {
					label: "Estado",
					cls: 'colState',
					type: "state",
					render: 'state',
					attrName: 'inputState',
					id: 'inputState',
				}]
			})
			content = content.replace(/\{{_BODY}}/g, tableMacro);
			addHtml({
				selector: "#boxContentConfigEd",
				type: 'insert',
				content
			}) //type: html, append, prepend, before, after
		}).catch(console.warn());
	}).catch(console.warn());
}

export const configEdIndex = (vars = []) => {
	console.log('configIndex', vars);

}

export const newExternalDocument = (vars = []) => {
	console.log('newExternalDocument', vars);
	getData({
		task: 'getDataNewEd',
		return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
	}).then((response) => {
		console.log('getData getDataNewEd', response);

		if (response.status == 'success') {
			let data = response.data;
			let content = '';
			let listsTemplatesEd = response.data.listsTemplatesEd;

			localStorage.setItem('getDataNewEd', JSON.stringify(data));

			content += `<div class="listTemplatesEd">`;
			listsTemplatesEd.forEach(elem => {
				content += `<button class="btn btnSelection btnTemplateEdSelected" data-id="${elem.id}">${elem.code} - ${elem.name}</button>`;
			});
			content += `</div>`;


			addHtml({
				selector: `body`,
				type: 'prepend',
				content: renderModalClean({
					id: 'modalNewExternalDocument',
					attr: '',
					title: 'Crear nuevo Documento',
					body: content
				}) // insert, append, prepend, replace
			})
		} else {
			alertMessageError({
				message: response.message
			})
		}
	}).catch(console.warn());

}

export const newFormExternalDocument = (vars = []) => {
	console.log('newFormExternalDocument', vars);
	document.getElementById('modalNewExternalDocument').remove();
	let templateId = vars.id;
	let dataFormEd = JSON.parse(localStorage.getItem('getDataNewEd'));
	let entitieData	= dataFormEd.entitieData;
	let timeData = dataFormEd.timeData;
	let dataTemplate = dataFormEd.listsTemplatesEd.find(elem => elem.id == templateId);
	let numPages = dataTemplate.numPages;
	let pageDesignId = dataTemplate.pageDesignId;
	let pageDesingData = dataFormEd.pageDesigns.find(elem => elem.id == pageDesignId);
	let jsonPageDesing = JSON.parse(pageDesingData.json);
	console.log('pageDesingData', pageDesingData, jsonPageDesing);

	loadView(_PATH_WEB_NUCLEO + "modules/externaldocuments/views/externalDocumentForm.html?" + _VS).then((responseView) => {
		console.log('loadView', responseView);

		let content = responseView;
		let pagesInner = '';
		content = replaceAll(content, "{{_TITLE}}", 'Nuevo Documento Externo');
		content = replaceAll(content, "{{_CLS_TEMPLATE}}", jsonPageDesing.cls);
		

		//for (let i = 0; i < numPages; i++) {
		//	let contentPage = '';
			pagesInner = `<div class="page" style="position: relative; " id="page">
				<div class = "page-inner" data-page="" id="pageInner"
				style = "z-index: 1; width: -webkit-fill-available; height: auto; display: inline-block;
				padding: 170px 70px 150px 100px; line-height: 1.7; font-size:14px; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', sans-serif">
					<p style="pointer-events: none">[[_LUGAR]], [[_FECHA_LITERAL]]</p><p class="sitePreview">[[_SITE]]</p>
					<br><br>
					<div class="editOnClick pageBody" id="pageBody">
						<div class="pageBody-inner containerPage">
							${dataTemplate.body}
						</div>
					</div>
				</div>
				<div style="position: absolute; z-index: 0;" class="bg">
					<img src="${pageDesingData.url}">
				</div>
			</div>`;
		//}

		content = content.replace(/{{_PAGES}}/g, pagesInner);
		content = content.replace(/{{_PATH_WEB}}/g, _PATH_WEB);

		//replace data document
		content = replaceAll(content, "[[_LUGAR]]", entitieData.code);
		content = replaceAll(content, "[[_FECHA_LITERAL]]", timeData.literal );

		innerForm({
			id: 'innerFormNewEd',
			module,
			body: content,
		});

		const contentPage = document.getElementById("pageBody");        
        let pageHeight = 710; // Altura máxima de cada página en píxeles
        
        function createPages() {
            let contentHeight = contentPage.clientHeight;
			let currentPage = 1;
			console.log ('contentHeight', contentHeight);
			const pageDesign =  pageDesingData.url.replace(/{{_PATH_WEB}}/g, _PATH_WEB);
            
            while (contentHeight > pageHeight) {
                //const newPage = document.createElement("div");
                //newPage.classList.add("page");
                //contentPage.parentNode.insertBefore(newPage, contentPage.nextSibling);
				//console.log("contentPage.nextSibling",contentPage.nextSibling);
				//console.log("next page", contentPage.parentNode.insertBefore(newPage, contentPage.nextSibling));
				
				$("#innerFormNewEd .innerPage").append(renderPageEd({
					pageDesign,
					index: currentPage,
					content: contentPage.innerHTML,
				})); 

                contentHeight -= pageHeight;
                //contentPage.style.height = contentHeight + "px";
                currentPage++;
            }
        }
        
        createPages(); 

		$(".pageBody").addClass("on");

	}).catch(console.warn());
}

//asycs  
export const getData = async (vars = []) => {
	//console.log('getData', vars);
	const url = _PATH_WEB_NUCLEO + "modules/externaldocuments/controllers/apis/externaldocuments.php";
	let data = JSON.stringify({
		accessToken: accessToken(),
		vars: JSON.stringify(vars)
	});

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: data
		})
		//console.log(res.data);
		return res.data
	} catch (error) {
		console.log("Error: getDataPlaces")
		console.log(error)
	}
}



document.addEventListener("DOMContentLoaded", function () {
	$("body").on("click",`.editOnClick`, function(e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let id = $(this).attr('id');
		console.log(`.btnEditOnClick`,data, id);
		//$('.pageBody').trumbowyg('destroy');
		editorText(id);
	});

	$("body").on("click",`.page-inner`, function(e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let id = $(this).attr('id');
		console.log(`.btnEditorOnclick leave`,data);
		$('#pageBody').trumbowyg('destroy');
	});


});

document.addEventListener('click', function (e) {
	//console.log("click",e);

	if (e.target.matches("#btnConfigEd")) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		loadConfigEd();
	}

	if (e.target.matches("#btnNewExternalDocument")) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		newExternalDocument();
	}

	if ((/btnTemplateEdSelected/.test(e.target.className))) {
		let data = e.target.dataset;
		newFormExternalDocument(data);
	}


});