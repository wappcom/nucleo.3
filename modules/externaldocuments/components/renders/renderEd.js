export const renderPageEd = (vars = []) => {
    console.log('renderPageEd', vars);
    const index = vars.index;
    const pageDesing = vars.pageDesign;
    let content = vars.content;
	const topPosition = index * 710;

    return `<div class="page" style="position: relative; " id="page${index}">
				<div class="page-inner" data-page="" id="pageInner${index}"
				    style="z-index: 1; width: -webkit-fill-available; 
                    height: auto; display: inline-block; position:relative;
                    overflow: hidden; position: relative; 
				    padding: 170px 70px 150px 100px; line-height: 1.7; 
                    font-size:14px; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', sans-serif">
					<div class="pageBody" style="height: 710px;  position: relative" id="pageBody${index}" >
						<div class="pageBody-inner" style="top: -${topPosition}px; position:absolute; width: -webkit-fill-available" >	
							${content}
						</div>
					</div>
				</div>
				<div style="position: absolute; z-index: 0; " class="bg">
					<img src="${pageDesing}">
				</div>
	        </div>`;
}