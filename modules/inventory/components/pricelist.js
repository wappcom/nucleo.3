import {
    stopLoadingBar,
    loadView,
    accessToken,
    alertPage,
    replacePath,
    createInnerForm,
    todaysDate,
    formatDate,
} from "../../components/functions.js"
import { resizeWorkspace } from "../../components/nav.js"
import {
    categorysListTable,
    createTable,
    dataTable,
    removeItemTable,
} from "../../components/tables.js"
import { errorInput } from "../../components/forms.js"

import { renderTrListPrice, renderListPriceEmpty } from "./renders/renderPricelist.js"
const module = "pricelist"
const system = "inventory"
const pathurl = "modules/inventory/"

//functions
export const pricelistIndex = () => {
    loadView(
        _PATH_WEB_NUCLEO + "modules/inventory/views/pricelist.html?vs" + _VS
    ).then((htmlView) => {
        getListPrice().then((response) => {
            console.log(response)
            if (response.Error == 0) {
                const tableId = "tablePriceList"
                const data = response.items
                let str = htmlView
                let strTable = ""
                let tbody = ""

                //console.log(data)

                for (let i = 0; i < data.length; i++) {
                    const elem = data[i]
                    elem["table"] = tableId
                    elem["module"] = module
                    elem["system"] = system
                    tbody += renderTrListPrice(elem)
                }
                strTable = createTable({
                    id: tableId,
                    thead: ":check,id:colId,Nombre,Lista Base,Factor, Metodo de Redondeo,Estado:colState,Acciones:colActions",
                    body: tbody,
                })

                str = str.replace(/{{_MODULE}}/g, module)
                str = str.replace(/{{_SYSTEM}}/g, system)
                str = str.replace(/{{_PATHURL}}/g, pathurl)
                str = replacePath(str)
                $(".bodyModule[module='" + module + "']").html(str)
                $(".bodyModule[module='" + module + "'] .tbody").html(strTable)
                dataTable({
                    elem: "#" + tableId,
                    orderCol: 1,
                })
            } else if (response == 0) {
                let str = htmlView
                str += renderListPriceEmpty()
                $(".bodyModule[module='" + module + "']").html(str)
                $(".bodyModule[module='" + module + "'] .tbody").html()
            } else {
                alertPage({
                    text:
                        "Error. por favor contactarse con soporte. " +
                        response.message,
                    icon: "icn icon-alert-warning",
                    animation_in: "bounceInRight",
                    animation_out: "bounceOutRight",
                    tipe: "danger",
                    time: "3500",
                    position: "top-left",
                })
            }
        })
    })
}

export const productsFormEdit = () =>{
    
}

//callBacks  
export const getListPrice = async (vars) => {
    const url =
        _PATH_WEB_NUCLEO +
        "modules/inventory/controllers/apis/v1/pricelist.php?" +
        _VS
    const dataForm = new FormData()
    dataForm.append("accessToken", JSON.stringify(accessToken))
    dataForm.append("action", "getListPrice")

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: dataForm,
        })
        console.log(res.data)
        return res.data
    } catch (error) {
        console.log("error getListPrice")
        console.log(error)
    }

}
