import {
    loadingBarIdReturn,
    loadView,
    addHtml,
    accessToken,
    alertMessageError,
    loadingBtnIcon,
    removeLoadingBtnIcon
} from "../../components/functions.js";

import {
    closeProfileMenu,
    closeSidebar
} from "../../components/nav.js";

import {
    mountMacroTable,
    clearMacroTable
} from "../../components/tables.js";

import {
    renderModal
} from "../../components/renders/renderModals.js";

import {
    focusInput
} from "../../components/forms.js";

import {
    productsIndex
} from "./products.js"


const module = "inventory";
const system = "inventory";
const modalConfigId = "modalInventory";
const tableProductsTypesId = "tableProductsTypes";
const tableColorsId = "tableColors";
const tableUnitsId = "tableUnits";
const tableWarehousesId = "tableWarehouses";

export function inventoryIndex() {
    //clearInterval(displayLoadingBar);

    //console.log("inventoryIndex");
    $(".bodyModule[system='" + system + "'][module='" + module + "']").html("Dashboard inventario");
    productsIndex();

}

export const warehousesIndex = (vars = []) => {
    console.log('warehousesIndex', vars);
    getData({
        task: 'getWarehouses',
        return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
        inputs: {
            state: 0
        }
    }).then((responseData) => {
        console.log('getData getWarehouses', responseData);
        if (responseData.status == 'success') {
            loadView(_PATH_WEB_NUCLEO + "modules/inventory/views/warehousesForm.html?" + _VS).then((response) => {
                let content = response;

                let tableMacro = mountMacroTable({
                    tableId: tableWarehousesId,
                    data: responseData.data,
                    cols: [{
                        label: "id",
                        cls: 'colId',
                        type: "id",
                        render: 'id',
                    }, {
                        label: "*Nombre",
                        cls: 'colName',
                        type: "text",
                        render: 'name',
                        attrName: 'inputName',
                        id: 'inputName',
                    }, {
                        label: "Detalle",
                        cls: '',
                        type: "text",
                        render: 'details',
                        attrName: 'inputDetails',
                        id: 'inputDetails',
                    }, {
                        label: "Primary",
                        cls: '',
                        type: "bit",
                        render: 'primary',
                        attrName: 'inputPrimary',
                        id: 'inputPrimary',
                    }, {
                        label: "Estado",
                        cls: 'colState',
                        type: "state",
                        render: 'state',
                        attrName: 'inputState',
                        id: 'inputState',
                    }]
                })
                content = content.replace(/\{{_BODY}}/g, tableMacro);
                addHtml({
                    selector: "#contentConfig",
                    type: 'insert',
                    content
                }) //type: html, append, prepend, before, after

            }).catch(console.warn());
        } else {
            alertMessageError({ message: response.message })
        }
    }).catch(console.warn());
}

export function configCreate(vars = []) {
    loadView(_PATH_WEB_NUCLEO + "modules/inventory/views/config.html?" + _VS).then((response) => {
        //console.log('loadView', response);
        let path = vars.path ? vars.path : "config";
        let content = response;
        content = content.replace(/\{{_MODULE}}/g, module);
        content = content.replace(/\{{_MODAL_ID}}/g, modalConfigId);

        closeProfileMenu();
        closeSidebar();

        addHtml({
            selector: "#root",
            type: 'prepend',
            content: renderModal({
                cls: "modalConfig animated fadeUp",
                btnClose: 1,
                body: loadingBarIdReturn(),
                id: modalConfigId,
            })
        }) //type: html, append, prepend, before, after

        addHtml({
            selector: "#" + modalConfigId + " .modalInner",
            type: 'insert',
            content
        }) //type: html, append, prepend, before, after

        getPageConfig({
            id: modalConfigId,
            path: path
        });

    }).catch(console.warn());

}

export function configIndex(vars = []) {
    loadView(_PATH_WEB_NUCLEO + "modules/inventory/views/configForm.html?" + _VS).then((responseView) => {
        //console.log('loadView', responseView);
        getData({
            task: 'getOptionsInventory',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
        }).then((response) => {
            console.log('getData getOptionsInventory', response);
            if (response.status == 'success') {
                let content = responseView;
                let data = response.data;

                //  find name "getOptionsInventory" in data 
                let optionsInventory = data.find((item) => {
                    return item.name == "routeCategoryId";
                });

                let optionsMultiType= data.find((item) => {
                    return item.name == "multiTypesActive";
                });
                console.log('optionsInventory', optionsInventory)
                // replace content whith variables
                content = content.replace(/\{{_CATEGORY_ID}}/g, optionsInventory.value);

                addHtml({
                    selector: "#contentConfig",
                    type: 'insert',
                    content
                }) //type: html, append, prepend, before, after

                $("#configForm #inputStateMultiTypeInventory").val(optionsMultiType.value);
                $("#configForm .btnStateSwitch[for='inputStateMultiTypeInventory']").attr("state",optionsMultiType.value);

            } else {
                alertMessageError({ message: response.message })
            }
        }).catch(console.warn());

    }).catch(console.warn());

}

export function typesProductsIndex(vars = []) {

    getData({
        task: 'getTypesProducts',
        return: 'returnArray'
    }).then((responseData) => {
        console.log('getData', responseData);
        loadView(_PATH_WEB_NUCLEO + "modules/inventory/views/typesProducts.html?" + _VS).then((response) => {
            let content = response;

            let tableMacro = mountMacroTable({
                tableId: tableProductsTypesId,
                data: responseData.data,
                cols: [{
                    label: "id",
                    cls: 'colId',
                    type: "id",
                    render: 'id',
                }, {
                    label: "*Nombre",
                    cls: 'colName',
                    type: "text",
                    render: 'name',
                    attrName: 'inputName',
                    id: 'inputName',
                }, {
                    label: "*Subfix",
                    cls: 'colSubfix',
                    type: "text",
                    render: 'subfix',
                    attrName: 'inputSubfix',
                    id: 'inputSubfix',
                }, {
                    label: "*SubfixDb",
                    cls: 'colSubfixDb',
                    type: "text",
                    render: 'subfixDb',
                    attrName: 'inputSubfixDb',
                    id: 'inputSubfixDb',
                }, {
                    label: "Primary",
                    cls: 'colPrimary',
                    type: "bit",
                    render: 'primary',
                    attrName: 'inputPrimary',
                    id: 'inputPrimary',
                }, {
                    label: "Json",
                    cls: 'colJson',
                    type: "text",
                    render: 'json',
                    attrName: 'inputJson',
                    id: 'inputJson',
                }, {
                    label: "Estado",
                    cls: 'colState',
                    type: "state",
                    render: 'state',
                    attrName: 'inputState',
                    id: 'inputState',
                }]
            })
            content = content.replace(/\{{_BODY}}/g, tableMacro);
            addHtml({
                selector: "#contentConfig",
                type: 'insert',
                content
            }) //type: html, append, prepend, before, after
        }).catch(console.warn());
    }).catch(console.warn());
}

export function colorsIndex(vars = []) {
    getData({
        task: 'getColors',
        return: 'returnArray'
    }).then((responseData) => {
        console.log('getData', responseData);
        loadView(_PATH_WEB_NUCLEO + "modules/inventory/views/colors.html?" + _VS).then((response) => {
            let content = response;

            let tableMacro = mountMacroTable({
                tableId: tableColorsId,
                data: responseData.data,
                cols: [{
                    label: "id",
                    cls: 'colId',
                    type: "id",
                    render: 'id',
                }, {
                    label: "*Nombre",
                    cls: 'colName',
                    type: "text",
                    render: 'name',
                    attrName: 'inputName',
                    id: 'inputName',
                }, {
                    label: "*Color Hex",
                    cls: 'colorHex',
                    type: "text",
                    render: 'color',
                    attrName: 'inputColor',
                    id: 'inputColor',
                }, {
                    label: "Json",
                    cls: 'colJson',
                    type: "text",
                    render: 'json',
                    attrName: 'inputJson',
                    id: 'inputJson',
                }, {
                    label: "Estado",
                    cls: 'colState',
                    type: "state",
                    render: 'state',
                    attrName: 'inputState',
                    id: 'inputState',
                }]
            })
            content = content.replace(/\{{_BODY}}/g, tableMacro);
            addHtml({
                selector: "#contentConfig",
                type: 'insert',
                content
            }) //type: html, append, prepend, before, after

        }).catch(console.warn());
    }).catch(console.warn());
}

export const unitsIndex = (vars = []) => {
    console.log('unitsIndex', vars);
    getData({
        task: 'getUnits',
        return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
    }).then((responseData) => {
        console.log('getData getUnits', responseData);
        if (responseData.status == 'success') {
            loadView(_PATH_WEB_NUCLEO + "modules/inventory/views/unitsForm.html?" + _VS).then((response) => {
                let content = response;

                let tableMacro = mountMacroTable({
                    tableId: tableUnitsId,
                    data: responseData.data,
                    cols: [{
                        label: "id",
                        cls: 'colId',
                        type: "id",
                        render: 'id',
                    }, {
                        label: "*Nombre",
                        cls: 'colName',
                        type: "text",
                        render: 'name',
                        attrName: 'inputName',
                        id: 'inputName',
                    }, {
                        label: "Detalle",
                        cls: '',
                        type: "text",
                        render: 'summary',
                        attrName: 'inputSummary',
                        id: 'inputSummary',
                    }, {
                        label: "Vars",
                        cls: 'colVars',
                        type: "text",
                        render: 'vars',
                        attrName: 'inputVars',
                        id: 'inputVars',
                    }, {
                        label: "Estado",
                        cls: 'colState',
                        type: "state",
                        render: 'state',
                        attrName: 'inputState',
                        id: 'inputState',
                    }]
                })
                content = content.replace(/\{{_BODY}}/g, tableMacro);
                addHtml({
                    selector: "#contentConfig",
                    type: 'insert',
                    content
                }) //type: html, append, prepend, before, after
                focusInput("inputName");
            }).catch(console.warn());
        } else {
            alertMessageError({ message: response.message })
        }
    }).catch(console.warn());
}

export function configUpdate(vars = []) { }

export const resizeConfig = (vars) => {
    //console.log('resizeConfig',vars);
    $("#" + modalConfigId).outerHeight($(window).height() - 84);
}

export const getPageConfig = (vars = []) => {
    //console.log('getPageConfig', vars);
    $(`#${modalConfigId} .btnMenuModal`).removeClass('active');
    $(`#${modalConfigId} .btnMenuModal[data-content='${vars.path}']`).addClass('active');

    if (vars.path != "config") {
        eval(`${vars.path}.${vars.path}Index(` + JSON.stringify(vars) + `)`);
    } else {
        configIndex(vars);
    }

}

export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/inventory/controllers/apis/v1/inventory.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("Error: getData")
        console.log(error)
    }
}

document.addEventListener('DOMContentLoaded', function () {
    $("body").on("click", `#btnConfigInventory`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data()
        //console.log('btnConfigInventory', data);
        configCreate();
    });

    $("body").on("keyup", `#${tableProductsTypesId} input`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        //console.log(e.key);
        let name = $(`#${tableProductsTypesId} #inputName`).val();
        let subfix = $(`#${tableProductsTypesId} #inputSubfix`).val();
        let subfixDb = $(`#${tableProductsTypesId} #inputSubfixDb`).val();
        let json = $(`#${tableProductsTypesId} #inputJson`).val();
        let state = $(`#${tableProductsTypesId} #inputState`).val();
        let cont = 0;
        $(`#${tableProductsTypesId} input`).removeClass("error");


        if (e.key == "Enter") {

            if (name == "") {
                cont++;
                $(`#${tableProductsTypesId} #inputName`).addClass('error');
            }

            if (subfix == "") {
                cont++;
                $(`#${tableProductsTypesId} #inputSubfix`).addClass('error');
            }

            if (cont == 0) {

                getData({
                    task: 'addTypeProducts',
                    return: 'returnId',
                    inputs: {
                        name,
                        subfix,
                        subfixDb,
                        json,
                        state
                    }
                }).then((responseData) => {
                    console.log('getData', responseData);
                    let id = responseData.data;
                    $(`#${tableProductsTypesId} table tbody`).append(`<tr rowId="${id}">
                        <td class="colCheck"><input type="checkbox" class="inputCheckBox" data-id="${id}"></td>
                        <td class="colId">${id}</td>
                        <td class="colName">${name}</td>
                        <td class="colSubfix">${subfix}</td>
                        <td class="colSubfixDb">${subfixDb}</td>
                        <td class="colJson">${json}</td>
                        <td class="colState">${state}</td>
                    </tr>`);
                }).catch(console.warn());
            }
        }

    })

    $("body").on("keyup", `#${tableColorsId} input`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        //console.log(e.key);
        let table = tableColorsId;
        let name = $(`#${table} #inputName`).val();
        let color = $(`#${table} #inputColor`).val();
        let json = $(`#${table} #inputJson`).val();
        let state = $(`#${table} #inputState`).val();
        let cont = 0;
        $(`#${table} input`).removeClass("error");


        if (e.key == "Enter") {

            if (name == "") {
                cont++;
                $(`#${table} #inputName`).addClass('error');
            }

            if (color == "") {
                cont++;
                $(`#${table} #inputColor`).addClass('error');
            }

            if (cont == 0) {

                getData({
                    task: 'addColor',
                    return: 'returnId',
                    inputs: {
                        name,
                        color,
                        json,
                        state
                    }
                }).then((responseData) => {
                    console.log('getData', responseData);
                    let id = responseData.data;
                    clearMacroTable("#tableColors");
                    $(`#${table} table tbody`).append(`<tr rowId="${id}">
                        <td class="colCheck"><input type="checkbox" class="inputCheckBox" data-id="${id}"></td>
                        <td class="colId">${id}</td>
                        <td class="colName">${name}</td>
                        <td class="colColor">${color}</td>
                        <td class="colJson">${json}</td>
                        <td class="colState">${state}</td>
                    </tr>`);
                }).catch(console.warn());
            }
        }

    })

    $("body").on("keyup", `#${tableWarehousesId} input`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let tableId = tableWarehousesId;
        let inputName = $(`#${tableId} #inputName`).val();
        let inputDetails = $(`#${tableId} #inputDetails`).val();
        let inputState = $(`#${tableId} #inputState`).val();
        let inputPrimary = $(`#${tableId} #inputPrimary`).val();
        let cont = 0;

        console.log(`#$tableWarehousesId input`, data);
        if (e.key == "Enter") {
        }
    });

    $("body").on("click",`.btnStateSwitch[for="inputStateMultiTypeInventory"]`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnStateSwitch[for="inputStateMultiTypeInventory"]`,data);
        
    });

    $("body").on("click", `#btnSaveConfigCategoryInventory`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`#btnSaveConfigCategoryInventory`, data);
        let inputCategoryId = $("#configForm #inputCategoryId").val();
        loadingBtnIcon("#btnSaveConfigCategoryInventory", 'Guardando...');
        getData({
            task: 'updateConfigInventory',
            return: 'returnState', // returnId, returnState, returnArray, returnMessage, returnObject
            input: {
                type: 'categories',
                inputCategoryId
            }
        }).then((response) => {
            console.log('getData updateConfigInventory', response);
            if (response.status == 'success') {
                setTimeout(() => {
                    removeLoadingBtnIcon("#btnSaveConfigCategoryInventory", 'Guardar');
                }, 1000);
            } else {
                alertMessageError({ message: response.message })
            }
        }).catch(console.warn());
    });
})