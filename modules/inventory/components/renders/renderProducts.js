import { empty, btnFn } from '../../../components/functions.js'
import { renderBtnState } from '../../../components/renders/render.js'
import { renderInputMedia } from '../../../components/renders/renderForms.js'

export function renderTrProduct(vars) {

    let code = empty(vars.code, '')
    return `
    <tr class="rowItem" item="${vars.id}">
        <td class="colCheck colId"><input name="inputCheck[]"  type="checkbox" value="${vars.id}"></td>
        <td >${vars.id}</td>
        <td class="colImg"><a>${vars.img}<span>${vars.name}<span></a></td>
        <td>${code}</td>
        <td> </td>
        <td class=""><div class="categorysList">${vars.categorysList}</div></td>
        <td class="colState">` + renderBtnState({ item: vars.id, fn: "changeStateItem", state: vars.state, module: vars.module, system: vars.system }) + `</td>
        <td class="colActions">
            <div class="btns">
                ` + btnFn({ btnType: 'btnEdit', clss: 'btnAddForm', item: vars.id, module: vars.module, system: vars.system, fn: 'productsFormEdit', vars: vars.name + '' }) + `
                ` + btnFn({ btnType: 'btnDelete', item: vars.id, module: vars.module, system: vars.system, fn: 'deleteFormProduct', vars: vars.name + '' }) + `
            </div>
        </td>
     </tr>
   `
}

export function renderTypesProducts(selectedId = 0) {
    let str = ''
    const array = _TYPES_PRODUCTS
    str += `<option value="0" lang="es">Seleccionar Tipo</option>`
    for (let i = 0; i < array.length; i++) {
        const element = array[i]
        let check = ''
        if (selectedId == 0) {
            if (element.active == 1) {
                check = 'selected'
            }
        } else {
            if (selectedId == element.id) {
                check = 'selected'
            }
        }
        str += `<option value="${element.id}" ${check}>${element.name}</option>`
    }
    return str
}

export const renderFormColor = (vars = []) => {
    let color = vars.color || '';
    let item = vars.item || '';
    let name = vars.name || '';
    let inputMedia = renderInputMedia(vars);
    return `
        <div class="item itemColor" data-name="${name}" data-color="${color}" data-id="${item}"> 
            ${inputMedia}
            <div class="actions">
                <a class="btn btnFull btnMini btnDeleteColorForm" data-name="${name}" data-id="${item}">
                    <i class="icon icon-trash"></i>
                    <span>Eliminar Color</span>
                </a>
            </div>
        </div>     
    `;
}

export const renderSelectColor = (vars = []) => {
    let listColor = JSON.parse(localStorage.getItem('colorsProducts'));
    let list = '';
    if (listColor != null && listColor.length > 0) {
        listColor.map(item => {
            list += `<a class="btnSelectorColor" data-name="${item.name}" data-id="${item.id}" data-color="${item.color}"> <i style="color:${item.color}" class="icon icon-checked-o"></i> <span>${item.name}</span></a>`
        });
    }

    return `    
        <label lang="es">Colores</label>
        <div class="selectorListColors">${list}</div>
    `;
}