import { empty, btnFn } from "../../../components/functions.js"
import { renderBtnState } from "../../../components/renders/render.js"

export function renderTrStock(vars) {
    console.log("renders vars", vars)
    let img = ""
    let code = vars.code != "" ? vars.code : "-";
    let quantity = vars.quantity != "" ? vars.quantity : "-";
    let state = renderBtnState(vars.state)
    if (vars.img != "" && vars.img != null) {
        img =
            '<img id="imgId' +
            vars.id +
            '" src="' +
            _PATH_FILES +
            vars.img +
            '">'
    } else {
        img =
            '<img id="imgId' +
            vars.id +
            '" src="' +
            _PATH_WEB +
            _IMG_DEFAULT +
            '">'
    }
    return `
    <tr class="rowItem" item="${vars.id}">
        <td class="colCheck colId"><input name="inputCheck[]"  type="checkbox" value="${vars.id}"></td>
        <td class="colId">${vars.id}</td>
        <td class="colImg"><a>${img}<span>${vars.name}</span></a></td>
        <td class="colCode">${code}</td>
        <td class="colUnavailable"> </td>
        <td class="colInto"></td>
        <td class="colQuantity">${quantity}</td>
        <td class="colState">${state}</td>
        <td class="colActions">
            <div class="btns">
                <button
                    class="btn btnSmall  btnAddStockItems btnFull"
                    item="${vars.id}"
                    fn = "addStockItems"
                    module="${vars.module}"
                    table = "${vars.table}"
                    name="${vars.name}"
                    system="${vars.system}"
                    for="inputItemAmount${vars.id}"
                    lang="es"
                >Añadir</button>                
                <!-- <button
                    class="btn btnSmall btnAddForm btnSetStockItems btnFull"
                    item="${vars.id}"
                    module="${vars.module}"
                    system="${vars.system}"
                    lang="es"
                >Establecer</button> -->
                <div class="boxAmount">
                    <input class="inputTable" placeholder="" id="inputItemAmount${vars.id}" value="0">
                    <div class="chevrons">
                        <button class="btnAddAmount" for="inputItemAmount${vars.id}"><i class="icon icon-chevron-up"></i></button>
                        <button class="btnMinusAmount" for="inputItemAmount${vars.id}"><i class="icon icon-chevron-down"></i></button>
                    </div>
                </div>
            </div>
        </td>
     </tr>
   `
}

export function renderStockEmpty() {
    return `
        <div class="inner">
            <div class="message">
                No hay productos registrados, Agrega un producto.
            </div>
        </div>
    `
}

export function renderItemProductStock(vars) {
    return `
        <div class="formItem">
            <div class="formControl formControlLineal">
                <div class="col colImg">
                    <img src="${vars.img}">
                </div>
                <div class="col colName">${vars.name}</div>
                <div class="col colActions">
                    <input type="hidden" value="${vars.item}" id="inputItem${vars.item}">
                    <input type="text" class="formInput input50w" name="inputItemAmount[]" id="inputItemAmount${vars.item}" value="${vars.value}">
                    <div class="btns">
                        <buttom class="btnDeleteItemStock" item="${vars.item}"><i class="icon icon-trash"></i></buttom>
                    </div>
                </div>
            </div>
            <div class="formControlLandscape">
                <div class="formControl  form25w formControlPrice">
                    <label lang="es">Precio Base</label>
                    <span class="money">${_COIN_DEFAULT}</span>
                    <input class="formInput" type="text" value="0,00" name="inputBasePrice" id="inputBasePrice${vars.item}">
                </div>                
                <div class="formControl  form25w formControlPrice">
                    <label lang="es">Desc.Cash</label>
                    <span class="money">${_COIN_DEFAULT}</span>
                    <input class="formInput" type="text" value="0,00" name="inputCashDiscount" id="inputCashDiscount${vars.item}">
                </div>                
                <div class="formControl  form20w">
                    <label lang="es">Desc.%</label>
                    <input class="formInput" type="text" value="" name="inputDiscount" id="inputDiscount${vars.item}">
                </div>
                <div class="formControl  form25w formControlPrice">
                    <label lang="es">Precio Base Final</label>
                    <span class="money disabled">${_COIN_DEFAULT}</span>
                    <input class="formInput disabled" type="text" value="0,00" name="inputBasePriceEnd" id="inputBasePriceEnd${vars.item}">
                </div>   
            </div>
        </div>
    `
}

export function renderCommentCronology(vars) {
    return `<div class="formItem" item="">
                <div class="comments">
                    <div class="perfil">{{_SIGLA}}</div>
                    <input lang="es" class="formInput" type="text" value="" placeholder="Deja aqui un comentario" name="inputComment" id="inputComment">
                    <button class="btn btnSuccess  btnSaveComment" item="">Guardar</button>
                </div>
                <div class="register">
                    <i class="icon icon-point"></i> ${vars.date}
                    <input type="hidden" class="formInput" id="inputdateComment">
                </div>
            </div>`
}
