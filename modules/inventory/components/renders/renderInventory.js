export function renderListWarehouses(selectedId = 0) {
    let str = '';
    const array = _WAREHOUSES;
    str += `<option value="0" lang="es">Seleccionar Almacén</option>`;
    for (let i = 0; i < array.length; i++) {
        const element = array[i];
        let check = '';
        if (element.id == selectedId) {
            check = 'selected';
        }
        str += `<option value="${element.id}" ${check}>${element.name}</option>`;
    }
    return str;
}

export function renderListProviders(id) {
    let str = '';
    const array = _PROVIDERS;

    for (let i = 0; i < array.length; i++) {
        const element = array[i];
        str += `<buttom class="btn item-${element.id} btnListSelect" value="${element.id}"><span>${element.id}.- ${element.name}</span><i class="icon icon-chevron-right"></i></buttom>`;
    }
    return str;
}