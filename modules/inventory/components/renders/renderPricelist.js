import { empty, btnFn } from "../../../components/functions.js"
import { renderBtnState } from "../../../components/renders/render.js"

export function renderTrListPrice(vars) {
    let baseList = ''
    let roundingMethod = ''
    if (vars.baseList==null){
        baseList = '='
    }
    if ( vars.roundingMethod==null || vars.roundingMethod==0){
        roundingMethod = "sin redondeo"
    }
    return /*html*/`
    <tr class="rowItem" item="${vars.id}">
        <td class="colCheck colId"><input name="inputCheck[]"  type="checkbox" value="${vars.id}"></td>
        <td class="colId">${vars.id}</td>
        <td class="colName"><a><span>${vars.name}</span></a></td>
        <td class="colName">${baseList}</td>
        <td class="colName">${vars.factor}</td>
        <td class="colName">${roundingMethod}</td>
        <td class="colState">` +
        renderBtnState({
            item: vars.id,
            fn: "changeStateItem",
            state: vars.state,
            module: vars.module,
            system: vars.system,
        }) +
        `</td>
        <td class="colActions">
            <div class="btns">
                ` +
        btnFn({
            btnType: "btnEdit",
            clss: "btnAddForm",
            item: vars.id,
            module: vars.module,
            system: vars.system,
            fn: "productsFormEdit",
            vars: vars.name + "",
        }) +
        `
                ` +
        btnFn({
            btnType: "btnDelete",
            item: vars.id,
            module: vars.module,
            system: vars.system,
            fn: "deleteFormProduct",
            vars: vars.name + "",
        }) +
        `
            </div>
        </td>
    </tr>
    `
}

export function renderListPriceEmpty() {
    return `
        <div class="inner">
            <div class="message">
                No hay listas registradas, Agrega una lista.
            </div>
        </div>
    `
}
