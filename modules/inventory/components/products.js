import {
    empty,
    alertMessageError,
    alertPage,
    replacePath,
    replaceAll,
    loadView,
    stopLoadingBar,
    emptyReturn,
    loadingBtnIcon,
    btnLoadingRemove,
    unDisableId,
    removeBtnLoading,
    accessToken,
    createBtnSlit,
    replaceEssentials,
    camelize,
    addHtml
} from '../../components/functions.js';
import {
    editorText,
    errorInput,
    loadItemMedia,
    loadInitChargeMedia,
    loadFilesFormLoader,
    loadSingleFileFormLoader,
    loadChargeMediaInit,
    removalMediaItems,
} from '../../components/forms.js';

import {
    checkCategorys,
    listCategorys
} from '../../websites/components/categorys.js';
import {
    deleteModalItem,
    removeModal,
    deleteModal,
    modalDelete,
    removeInnerForm
} from '../../components/modals.js';
import {
    categorysListTable,
    createTable,
    dataTable,
    removeItemTable,
    inactiveBtnStateItem,
    activeBtnStateItem,
    setupTable
} from '../../components/tables.js';
import {
    renderTrProduct,
    renderTypesProducts,
    renderSelectColor,
    renderFormColor
} from './renders/renderProducts.js';

import {
    renderListWarehouses
} from './renders/renderInventory.js';

import {
    innerForm,
    renderDeleteModal
} from "../../components/renders/renderModals.js";

import {
    renderNoDataTable
} from "../../components/renders/renderTables.js";

import {
    renderInputMedia,
    renderInputQuantity,
    renderInputFileMultiUpload
} from "../../components/renders/renderForms.js";

import {
    clothingStoreIndex
} from './types/clothingStore.js';

let numMod = 0;
let pathUrlApiInventory = "modules/inventory/controllers/apis/v1/";
let pathurl = "modules/inventory/";
let module = "products";
let system = "inventory";
let tableId = "tableProduct";
let formId = "formProduct";
//functions
export function btnchargeXlsProducts() {

}

export function productsIndex() {
    //console.log(module)
    //console.log("productsIndex");
    //rolUser({ module: module })

    removeInnerForm()

    getData({
        task: 'getParameters',
        return: 'returnArray', // returnId, returnState, returnArray
    }).then((responseTypesProducts) => {

        //console.log("responseTypesProducts: ", responseTypesProducts);

        localStorage.setItem("typesProducts", JSON.stringify(responseTypesProducts.data.typesProducts));
        localStorage.setItem("colorsProducts", JSON.stringify(responseTypesProducts.data.colorsProducts));
        localStorage.setItem("warehouses", JSON.stringify(responseTypesProducts.data.warehouses));

        let dataTypesProducts = responseTypesProducts.data.typesProducts;

        if (responseTypesProducts.status == 'success') {
            loadView(_PATH_WEB_NUCLEO + pathurl + "views/products.html?" + _VS).then((htmlView) => {
                stopLoadingBar()
                getData({
                    task: 'loadProducts',
                    return: 'returnArray'
                }).then((data) => {
                    //console.log('loadProducts', data)
                    if (data.Error == 0) {
                        let dataItems = data.data;
                        let str = htmlView;

                        let strTable = '';
                        let tbody = '';

                        str = str.replace(/{{_MODULE}}/g, module)
                        str = str.replace(/{{_SYSTEM}}/g, system)
                        str = str.replace(/{{_PATHURL}}/g, pathurl)
                        str = str.replace(/{{_BTN_NEW}}/g, createBtnSlit({
                            id: "btnsNews",
                            preLabel: "Nuevo Item",
                            data: dataTypesProducts
                        }))
                        str = replacePath(str);
                        //console.log("str", str)
                        $(".bodyModule[module='products']").html(str)

                        if (dataItems == 0) {
                            $(".bodyModule[module='products'] .tbody").html(renderNoDataTable())
                        }

                        if (dataItems != 0) {
                            //console.log(data.length)
                            for (let i = 0; i < dataItems.length; i++) {
                                const elem = dataItems[i];
                                let imagen = ''
                                let code = ''
                                let nx = elem.categorys;
                                let stock = elem.stock;

                                if ((elem.img != "") && (elem.img != null)) {
                                    imagen = '<img src="' + _PATH_FILES + elem.img + '">'
                                } else {
                                    imagen = '<img src="' + _IMG_NOIMAGE + '">'
                                }
                                // console.log("module", module);
                                elem["categorysList"] = categorysListTable(elem.categorys)
                                elem["img"] = imagen;
                                elem["stock"] = stock.quantity;
                                elem["module"] = module
                                elem["system"] = system
                                tbody += renderTrProduct(elem)

                            }

                            strTable = setupTable({
                                tableId,
                                cols: [{
                                    elem: "check",
                                }, {
                                    elem: "id",
                                    label: "Id",
                                }, {
                                    elem: "img",
                                    label: "Imagen",
                                }, {
                                    elem: "name",
                                    label: "Nombre",
                                }, {
                                    elem: "type",
                                    label: "Tipo",
                                }, {
                                    elem: "code",
                                    label: "Codigo",
                                }, {
                                    elem: "stock",
                                    label: "Stock",
                                }, {
                                    elem: "categorysList",
                                    label: "Categorias",
                                }, {
                                    elem: "state",
                                    label: "Estado",
                                }, {
                                    elem: "actions",
                                    label: "Acciones",
                                    btns: ["btnEdit", "btnDeleteFn"]
                                }],
                                data: dataItems,
                            });



                            $(".bodyModule[module='products'] .tbody").html(strTable)
                            dataTable({
                                elem: "#" + tableId,
                                orderCol: 1,
                            });

                        }

                        /*  strTable = createTable({
                             id: "tableProduct",
                             thead: ':check,id:colId,Nombre,Codigo,Stock,Categorias,Estado:colState,Acciones:colActions',
                             body: tbody
                         }) */


                    }
                    if (data.Error == 1) {
                        alertMessageError({
                            message: data.message,
                        });
                    }
                })
            })

        } else {
            alertMessageError({
                message: responseTypesProducts.message
            })
        }

    }).catch(console.warn());

    //Acciones
    $("body").on("click", "#btnSaveProduct", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        let data = $(this).data()
        let form = $(this).attr("form")
        loadingBtnIcon("#btnSaveProduct")
        saveProduct(form, data)
    })

    $("body").on("click", ".btnDelete[module='" + module + "']", function (e) {
        //console.log("btnDeleteAdvised")
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()

        let item = $(this).attr("item")
        let module = $(this).attr("module")
        let fn = $(this).attr("fn")
        let vari = $(this).attr("vars")
        let variArray = vari.split(',')

        deleteModalItem({
            name: variArray[0],
            attr: `item="${item}"`,
            vars: item,
            module: module,
            fn: fn
        })
    })

    $("body").on('click', '.btnDeleteConfirm[fn="deleteFormProduct"]', function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        /* Act on the event */
        let item = $(this).attr("vars")
        let fn = $(this).attr("fn")
        //console.log(item)
        //console.log("deleteFormProduct",fn);
        deleteProduct({
            item: item
        }).then((data) => {
            console.log('deleteProduct btnDeleteConfirm[fn="deleteFormProduct"]', data)
            if (data == 1) {
                removeModal()
                removeItemTable(item)
                //productsIndex()
            }
        })
    })

    $("body").on("click", ".btnEditProduct", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        let item = $(this).attr("item")
        let form = $(this).attr("form")
        //loadingBtnIcon(".btnEditProduct")
        updateProductFn(form, item)
    })




    $("body").on("keyup", "#inputName", function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        unDisableId("inputName")
    })
}

export const deleteFormProduct = (item = null) => {
    console.log('deleteFormProduct', item);

    deleteProduct({
        item
    }).then((data) => {
        console.log(' deleteProduct', data)
        if (data == 1) {
            removeModal()
            removeItemTable(item)
            //productsIndex()
        }
    })
}

export function updateProductFn(form, item) {
    let count = 0
    const name = $("#" + form + " #inputName").val()
    const description = $("#" + form + " #inputDescription").val()
    const code = $("#" + form + " #inputCode").val()
    const tags = $("#" + form + " #inputTags").val()
    const type = $("#" + form + " #inputType").val()
    const state = $("#" + form + " #inputProductStatus").val();
    const imageCharge = $("#" + form + " #inputImage").val()

    if (name == "") {
        errorInput("inputName", "Agrega un nombre valido de producto")
    }

    let categorys = []
    let countCategorys = 0
    $("#" + form + " input[name = 'inputCategorys[]']").each(function (e) {
        let valorCategorys = $(this).val()
        let checkCategorys = $(this).prop('checked')
        if (checkCategorys) {
            categorys[countCategorys] = valorCategorys
            countCategorys++
        }
    })

    let mediaFiles = []
    let countMediaFiles = 0
    $("#" + form + " input[name = 'inputFilesProducts[]']").each(function (e) {
        let values = $(this).val()
        mediaFiles[countMediaFiles] = values
        countMediaFiles++
    })

    if (count == 0) {
        let inputs = {
            item,
            name,
            description,
            code,
            tags,
            categorys,
            type,
            imageCharge,
            mediaFiles,
            state
        }
        /* updateProduct(vars).then((response) => {
            console.log("updateProduct", response)
            if (response.Error == 0) {
                productsIndex()
            } else {
                console.log(response.message)
            }
        }) */

        getData({
            task: 'updateProduct',
            return: 'returnState', // returnId, returnState, returnArray, returnMessage, returnObject
            inputs
        }).then((response) => {
            console.log('getData updateProduct', response);
            if (response.status == 'success') {
                productsIndex()
            } else {
                alertMessageError({
                    message: response.message
                })
            }
        }).catch(console.warn());
    }

}

export function saveProduct(form, data) {
    //console.log(form)
    let count = 0;

    const name = $("#" + form + " #inputName").val()
    const description = $("#" + form + " #inputDescription").val()
    const code = $("#" + form + " #inputCode").val()
    const tags = $("#" + form + " #inputTags").val();
    const imageCharge = $("#" + form + " #inputImage").val();
    const type = data.type || "";

    if (name == "") {
        errorInput("inputName", "Agrega un nombre valido de producto")
        btnLoadingRemove("btnSaveProduct")
        count++
    }

    let categorys = []
    let countCategorys = 0
    $("#" + form + " input[name = 'inputCategorys[]']").each(function (e) {
        let valorCategorys = $(this).val()
        let checkCategorys = $(this).prop('checked')
        if (checkCategorys) {
            categorys[countCategorys] = valorCategorys
            countCategorys++
        }
    })

    let mediaFiles = []
    let countMediaFiles = 0
    $("#" + form + " input[name = 'inputFilesProducts[]']").each(function (e) {
        let values = $(this).val()
        mediaFiles[countMediaFiles] = values
        countMediaFiles++
    })
    btnLoadingRemove("btnSaveProduct")
    if (count == 0) {
        let vars = {
            name,
            description,
            code,
            tags,
            type,
            imageCharge,
            categorys,
            mediaFiles,
            dataTypes: eval(data.subfix + `Data({form})`)
        }

        getData({
            task: 'createProduct',
            return: 'returnArray', // returnId, returnState, returnArray
            inputs: vars
        }).then((response) => {
            //console.log("createProduct", response);

            if (response.Error == 0) {
                productsIndex();
            } else {
                console.log(response.message)
                if (response.message == "exist-pathurl") {
                    errorInput("inputName")
                    alertPage({
                        text: 'El nombre esta repetido, por favor cambiar.',
                        icon: 'icn icon-alert-warning',
                        animation_in: 'bounceInRight',
                        animation_out: 'bounceOutRight',
                        tipe: 'danger',
                        time: '3500',
                        position: 'top-left'
                    });
                }
            }
        })
    }
}

export const foodData = (vars = []) => {
    console.log('foodData', vars);

}

export const clothingStoreData = (vars = []) => {
    //console.log('clothingStoreData', vars);
    let form = vars.form;

    let sizes = []
    let countSizes = 0
    $("#" + form + " input[name = 'inputSizes[]']").each(function (e) {
        let values = $(this).attr("size");
        //console.log("values", values);
        if ($(this).prop('checked') == true) {
            sizes[countSizes] = values
            countSizes++
        }
    });

    let colors = [];
    let countColors = 0
    $("#" + form + " #tbodyColor .itemColor").each(function (e) {
        let id = $(this).data("id");

        let mediaFiles = [];
        let countMediaFiles = 0;
        $(`#` + form + ` .itemColor[data-id="${id}"] .formChargeMedia .item`).each(function (e) {
            let values = $(this).attr("item");
            //console.log("values", values);
            mediaFiles[countMediaFiles] = values;
            countMediaFiles++;
        });
        colors[countColors] = {
            id,
            mediaFiles
        };
        countColors++
    })

    let data = {
        inputCodebar: $(`#${form} #inputCodebar`).val(),
        inputQuantity: $(`#${form} #inputStock`).val(),
        inputSizes: sizes,
        inputColors: colors,
        inputPriceUsd: $(`#${form} #inputPriceUsd`).val(),
        inputPreviousPriceUsd: $(`#${form} #inputPreviousPriceUsd`).val(),
        inputPriceBs: $(`#${form} #inputPriceBs`).val(),
        inputPreviousPriceBs: $(`#${form} #inputPreviousPriceBs`).val(),
        inputContents: $(`#${form} #inputContents`).val(),
        inputNotes: $(`#${form} #inputNotes`).val(),
    };

    return data;
}

export function productsFormNew(vars) {
    //console.log(vars)
    const typeProductId = vars.typeProductId;
    let arrayTypesProducts = JSON.parse(localStorage.getItem("typesProducts"));
    //console.log("arrayTypesProducts", arrayTypesProducts)
    let dataTypeProduct = arrayTypesProducts.find(item => item.id == typeProductId);
    const nameIndex = camelize(dataTypeProduct.name);

    var strHtml = ""
    numMod++

    loadView(_PATH_WEB_NUCLEO + "modules/inventory/views/productsForm.html?" + _VS + numMod).then((htmlView) => {

        loadView(_PATH_WEB_NUCLEO + "modules/inventory/views/" + nameIndex + "Form.html?" + _VS + numMod).then((htmlViewForm) => {
            strHtml = replacePath(htmlView)
            strHtml = replaceAll(strHtml, "{{_MODULE}}", module)
            strHtml = replaceAll(strHtml, "{{_TITLE}}", "Nuevo Producto")
            strHtml = replaceAll(strHtml, "{{_BTN_NAME_ACTION}}", "Guardar")
            strHtml = replaceAll(strHtml, "{{_BTN_ACTION}}", "btnSaveProduct")
            strHtml = replaceAll(strHtml, "{{_FORM_ID}}", "formNewProduct")
            strHtml = replaceAll(strHtml, "{{_ATTR}}", `data-type="${typeProductId}" data-subfix="${nameIndex}"`)
            strHtml = replaceAll(strHtml, "{{_SYSTEM}}", system)
            strHtml = replaceAll(strHtml, "{{_CATEGORYS}}", listCategorys(_CATEGORYS))
            strHtml = replaceAll(strHtml, "{{_PRODUCTS_TYPE}}", renderTypesProducts(_TYPES_PRODUCTS.active))

            strHtml = replaceAll(strHtml, "{{_LIST_WAREHOUSES}}", renderListWarehouses(_WAREHOUSE_DEFAULT))
            strHtml = replaceAll(strHtml, "{{_INPUT_FILE_MEDIA}}", renderInputFileMultiUpload({
                id: "inputMedia",
                folder: 'inventory',
                validfiles: 'jpeg,jpg,png,mp4',
                module,
                system
            }))
            //tipo de producto stantard

            /*  $("#inputName").focus()

             $(".bodyModule[module='" + module + "'] .innerForm").html(strHtml)
             $(".bodyModule[module='" + module + "'] .innerForm .typeProductForm").html(htmlViewForm)
             stopLoadingBar()
             //new EasyEditor('#inputDescription')
             editorText('inputDescription') */
            /*   }) */
            htmlViewForm = replacePath(htmlViewForm);
            htmlViewForm = htmlViewForm.replace(/\{{_COIN}}/g, _COIN_DEFAULT);

            innerForm({
                module,
                body: replaceEssentials({
                    str: strHtml,
                    module,
                    title: "Nuevo producto",
                    formId: formId + 'Add',
                    btnAction: "btnSaveProduct",
                    btnNameAction: "Guardar"
                })
            });

            $("#typeProductForm").html(htmlViewForm);
            editorText('inputDescription');

        }).catch((err) => {
            console.log(err)
        })

    }).catch((err) => {
        console.log(err)
    })

}

export function productsFormEdit(vars) {
    //console.log("productsFormEdit", vars)
    var strHtml = ""
    numMod++
    const systemLocal = vars["system"] || system;
    const moduleLocal = vars["module"] ? vars["module"] : module;
    const pathurl = 'modules/inventory/';


    loadView(_PATH_WEB_NUCLEO + pathurl + "views/productsForm.html?" + _VS + numMod).then((htmlView) => {

        getData({
            task: 'loadEditProduct',
            return: 'returnArray', // returnId, returnState, returnArray
            inputs: vars
        }).then((responseData) => {
            console.log("loadEditProduct", responseData);
            let response = responseData.data;
            let typeId = response.type;
            let pathType = nameTypeProduct(typeId);
            //console.log("type:" , response, nameTypeProduct(response.type));
            loadView(_PATH_WEB_NUCLEO + pathurl + "views/" + pathType + "Form.html?" + _VS + numMod).then((htmlViewForm) => {
                //console.log("route", htmlViewForm);
                if (responseData.Error == 0) {
                    let form = "#formEditProduct";
                    let img = response.img;
                    let files = response.files;

                    strHtml = replacePath(htmlView)
                    strHtml = replaceAll(strHtml, "{{_MODULE}}", moduleLocal)
                    strHtml = replaceAll(strHtml, "{{_TITLE}}", "Editar Producto")
                    strHtml = replaceAll(strHtml, "{{_BTN_NAME_ACTION}}", "Actualizar")
                    strHtml = replaceAll(strHtml, "{{_BTN_ACTION}}", "btnEditProduct")
                    strHtml = replaceAll(strHtml, "{{_FORM_ID}}", "formEditProduct")
                    strHtml = replaceAll(strHtml, "{{_SYSTEM}}", systemLocal)
                    strHtml = replaceAll(strHtml, "{{_ITEM}}", response.id)
                    strHtml = replaceAll(strHtml, "{{_CATEGORYS}}", listCategorys(_CATEGORYS))
                    strHtml = replaceAll(strHtml, "{{_PRODUCTS_TYPE}}", renderTypesProducts(response.type))
                    strHtml = replaceAll(strHtml, "{{_LIST_WAREHOUSES}}", renderListWarehouses(_WAREHOUSE_DEFAULT))
                    strHtml = replaceAll(strHtml, "{{_INPUT_FILE_MEDIA}}", renderInputFileMultiUpload({
                        id: "inputMedia",
                        folder: 'inventory',
                        validfiles: 'jpeg,jpg,png,mp4',
                        module,
                        system
                    }))
                    //tipo de producto stantard

                    stopLoadingBar()
                    //$(".bodyModule[module='" + module + "'] .innerForm").html(strHtml)

                    innerForm({
                        module,
                        body: strHtml,
                    });

                    $(form + " #inputId").val(response.id)
                    $(form + " #inputName").val(response.name)
                    $(form + " #inputDescription").html(response.description)
                    $(form + " .easyeditor").html(response.description)
                    $(form + " #inputCode").val(response.code)
                    $(form + " #inputTags").val(response.tags)
                    $(form + " #inputType").val(response.type)

                    editorText('inputDescription');

                    $(".boxModule[module='" + moduleLocal + "'] .innerForm #typeProductForm").html(htmlViewForm);

                    //console.log(img)

                    if (files != 0) {
                        loadInitChargeMedia('inputFilesProducts');
                        $(".formChargeMedia[for='inputFilesProducts']").attr("data-id", response.id)
                        for (let i = 0; i < files.length; i++) {
                            const element = files[i];
                            let index = i + 1;
                            loadItemMedia({
                                id: 'inputFilesProducts',
                                index: index,
                                img: _PATH_FILES + element.pathurl,
                                name: element.name,
                                item: element.id
                            })
                        }
                    }

                    loadSingleFileFormLoader({
                        id: "inputImageCharge",
                        file: img,
                    });

                    checkCategorys(response.categorys)
                    $(form + " div[for='prices']").hide()
                    $(form + " div[for='inputStock']").hide()

                    response["form"] = form;
                    eval(pathType + `FormEdit(response)`)

                } else {
                    alertPage({
                        text: 'Error. por favor contactarse con soporte. ' + response.message,
                        icon: 'icn icon-alert-warning',
                        animation_in: 'bounceInRight',
                        animation_out: 'bounceOutRight',
                        tipe: 'danger',
                        time: '3500',
                        position: 'top-left'
                    })
                }
            })

        })
    });

}

export const clothingStoreFormEdit = (vars = []) => {
    //console.log('clothingStoreFormEdit', vars, vars.dataTypes[0]);
    let data = vars.dataTypes[0];
    let form = vars.form;
    let codebar = vars.codebar ? vars.codebar : "";
    let contents = vars.contents ? vars.contents : "";
    let notes = vars.notes ? vars.notes : "";
    let size = 0;
    let colors = 0;
    if (data != undefined && data != 0) {
        size = data.size;
        colors = data.colors;
    }

    $(form + " #inputCodebar").val(codebar)
    $(form + " #inputContents").val(contents)
    $(form + " #inputNotes").val(notes);
    if (size != 0) {
        size.forEach(element => {
            //console.log("element", element)
            $(form + ` input[name="inputSizes[]"][size="${element}"]`).prop('checked', true);
        });
    }

    if (colors != 0) {
        colors.forEach(elem => {
            console.log("elem", elem);
            let color = elem.dataColor.color;
            let name = elem.dataColor.name;
            let id = elem.dataColor.id;
            let title = `<i style="color:${color}" class="icon icon-checked-o"></i> <span>${name}</span>`;
            let mediaFiles = elem.mediaFiles ? elem.mediaFiles : 0;
            addHtml({
                selector: `#tbodyColor`,
                type: 'prepend',
                content: renderFormColor({
                    title,
                    color,
                    name,
                    id: `inputColor${id}`,
                    item: id
                })
            }) //type: html, append, prepend, before, after

            if (mediaFiles != 0) {
                loadInitChargeMedia(`inputColor${id}`)
                for (let i = 0; i < mediaFiles.length; i++) {
                    const element = mediaFiles[i];
                    let index = i + 1;
                    loadItemMedia({
                        id: `inputColor${id}`,
                        index: index,
                        img: _PATH_FILES + element.pathurl,
                        name: element.name,
                        item: element.id
                    })
                }
            }
        });
    }
}

export const foodFormEdit = (vars = []) => {
    console.log("foodFormEdit", vars);

}

export function nameActiveTypeProduct() {
    let object = _TYPES_PRODUCTS
    for (const key in object) {
        if (Object.hasOwnProperty.call(object, key)) {
            const element = object[key];
            if (element.active == 1) {
                return element.name
            }
        }
    }
}

export function nameTypeProduct(item) {
    let object = localStorage.getItem('typesProducts');
    object = JSON.parse(object);
    //console.log("nameTypeProduct", object)
    /* for (const key in object) {
        if (Object.hasOwnProperty.call(object, key)) {
            const element = object[key];
            if (element.id == item) {
                return element.name
            }
        }
    } */
    let data = object.find((element) => {
        return element.id == item
    })

    //console.log("nameTypeProduct", item, data)

    return data.path;
}

export function changeStateItem(item, state) {
    changeStateProduct(item, state).then((response) => {
        if (response.Error == 0) {
            if (response.response == 1) {
                activeBtnStateItem(item)
            }
            if (response.response == 0) {
                inactiveBtnStateItem(item)
            }
        } else {
            console.log(response.message)
            alertPage({
                text: response.message,
                icon: 'icn icon-alert-warning',
                animation_in: 'bounceInRight',
                animation_out: 'bounceOutRight',
                tipe: 'danger',
                time: '3500',
                position: 'top-left'
            });
        }
    })
}

export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/inventory/controllers/apis/v1/products.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("Error: getDataPlaces", error);
    }
}

async function loadProducts(vars) {
    let url = _PATH_WEB_NUCLEO + "modules/inventory/controllers/apis/v1/products.php";
    const dataForm = new FormData()
    dataForm.append("access_token", localStorage.getItem("access_token"))
    dataForm.append("refresh_token", localStorage.getItem("refresh_token"))
    dataForm.append("entitieId", localStorage.getItem("idEntitie"))
    dataForm.append("page", _PATH_PAGE)

    dataForm.append("actions", "loadProducts")

    let req = new Request(url, {
        async: true,
        method: "POST",
        mode: "cors",
        body: dataForm,
    })
    try {
        let contentAwait = await fetch(req)
        let str = await contentAwait.json()
        //console.log(str)
        //console.log('loadProduct: ' + str)
        return str
    } catch (err) {
        console.log('loadProduct Error: ' + err)
    }
}

async function createProduct(vars) {
    var vari = JSON.stringify(vars)
    var url = _PATH_WEB_NUCLEO + pathUrlApiInventory + 'products.php'
    const dataForm = new FormData()
    dataForm.append("access_token", localStorage.getItem("access_token"))
    dataForm.append("refresh_token", localStorage.getItem("refresh_token"))
    dataForm.append("entitieId", localStorage.getItem("idEntitie"))
    dataForm.append("page", _PATH_PAGE)

    dataForm.append("actions", "createProduct")
    dataForm.append("vars", vari)

    let req = new Request(url, {
        async: true,
        method: "POST",
        mode: "cors",
        body: dataForm,
    })
    try {
        let contentAwait = await fetch(req)
        let str = await contentAwait.json()
        //console.log(str)
        //console.log('insertProduct: ' + str)
        return str
    } catch (err) {
        console.log('insertProduct Error ', err)
    }
}

async function updateProduct(vars) {
    var vari = JSON.stringify(vars)
    var url = _PATH_WEB_NUCLEO + pathUrlApiInventory + 'products.php'
    const dataForm = new FormData()
    dataForm.append("access_token", localStorage.getItem("access_token"))
    dataForm.append("refresh_token", localStorage.getItem("refresh_token"))
    dataForm.append("entitieId", localStorage.getItem("idEntitie"))
    dataForm.append("page", _PATH_PAGE)

    dataForm.append("actions", "updateProduct")
    dataForm.append("vars", vari)

    let req = new Request(url, {
        async: true,
        method: "POST",
        mode: "cors",
        body: dataForm,
    })
    try {
        let contentAwait = await fetch(req)
        let str = await contentAwait.json()
        //console.log(str)
        //console.log('insertProduct: ' + str)
        return str
    } catch (err) {
        console.log('updateProduct error ', err)
    }
}

async function deleteProduct(vars) {
    var url = _PATH_WEB_NUCLEO + pathUrlApiInventory + 'products.php'
    let access_token = localStorage.getItem("access_token")
    let refresh_token = localStorage.getItem("refresh_token")
    let idEntitie = localStorage.getItem("idEntitie")

    const dataForm = new FormData()
    dataForm.append("access_token", access_token)
    dataForm.append("refresh_token", refresh_token)
    dataForm.append("entitieId", idEntitie)
    dataForm.append("actions", "deleteProduct")
    dataForm.append("page", _PATH_PAGE)

    dataForm.append("vars", `{"item":"${vars.item}"}`);

    let req = new Request(url, {
        async: true,
        method: "POST",
        mode: "cors",
        body: dataForm,
    })

    try {
        let contentAwait = await fetch(req)
        let data = await contentAwait.json()
        //console.log("deleteProduct: " + data)
        return data
    } catch (err) {
        console.log("deleteProduct Error:" + err)
    }
}

/* async function changeStateProduct(item, state) {
    var url = _PATH_WEB_NUCLEO + pathUrlApiInventory + 'products.php'
    const dataForm = new FormData()
    dataForm.append("access_token", localStorage.getItem("access_token"))
    dataForm.append("refresh_token", localStorage.getItem("refresh_token"))
    dataForm.append("entitieId", localStorage.getItem("idEntitie"))
    dataForm.append("page", _PATH_PAGE)

    dataForm.append("actions", "changeStateProduct")
    dataForm.append("item", item)
    dataForm.append("state", state)

    let req = new Request(url, {
        async: true,
        method: "POST",
        mode: "cors",
        body: dataForm,
    })
    try {
        let contentAwait = await fetch(req)
        let str = await contentAwait.json()
        console.log(str)
        //console.log('changeStateProduct: ' + str)
        return str
    } catch (err) {
        console.log('changeStateProduct Error: ' + err)
    }
} */


export const deleteItemsMedia = async (vars, fnReturn, varsReturn) => {
    console.log('deleteItemsMedia', vars);
    const url = _PATH_WEB_NUCLEO + "modules/inventory/controllers/apis/v1/products.php";

    const array = vars.split(":");
    const items = array[0].split(",");

    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify({
            task: 'deleteItemsMedia',
            return: 'returnState',
            vars: {
                items,
                idItemForm: array[1]
            }
        })
    });

    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        console.log(res.data);
        let response = res.data;
        if (response.Error == "0") {
            //console.log(response, fnReturn, varsReturn);

            removalMediaItems({
                id: varsReturn,
                items: items,
            });
        }
        //return res.data
    } catch (error) {
        console.log("Error: deleteItemsMedia")
        console.log(error)
    }
}

document.addEventListener("DOMContentLoaded", function () {

    $("body").on("click", `.btnEdit[data-for="tableProduct"]`, function (e) {
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        let data = $(this).data();
        productsFormEdit(data);
    })

    $("body").on("click", `.btnDeleteFn[data-fn='deleteTableProduct']`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let id = $(this).attr("data-id");
        let name = $(this).attr("data-name");

        console.log(`.btnDeleteFn[data-fn='deleteTableProduct']`, data);

        deleteModal({
            id: "deleteModal",
            name,
            cls: "deleteModalTableProduct",
            attr: `data-name="${name}" data-id="${id}"`,
        })

    });

    $("body").on("click", `.btnRemoveItems`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let id = $(this).attr("for");

        $(".formChargeMedia[for='" + id + "'] .itemMedia[select='on']").each(function (e) {
            ////console.log(e);
            ////console.log($(this).attr("item"));
            const items = $(this).attr("item");
            $(`.formChargeMedia .itemMedia[item='${items}']`).remove();
        });

    });

    $("body").on("click", `.btnConfirmDelete[fn='deleteItemsMedia'][module='inventory']`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnConfirmDelete[fn='deleteItemsMedia']`, data);

        let array = data.array;
        let items = '';

        if (Array.isArray(array)) {
            items = data.array.split(",");
        } else {
            items = data.array;
        }


        getData({
            task: 'deleteItemsMedia',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
            inputs: {
                prodId: data.id,
                items
            }
        }).then((response) => {
            console.log('getData deleteItemsMedia', response);
            if (response.status == 'success') {
                removeModal();
                for (let i = 0; i < items.length; i++) {
                    $(`.formChargeMedia .itemMedia[item='${items[i]}']`).remove();
                }
            } else {
                alertMessageError({
                    message: response.message
                })
            }
        }).catch(console.warn());

    });

    $("body").on("click", `.deleteModalTableProduct`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let item = data.id;
        console.log(`.deleteModalTableProduct`, data);

        getData({
            task: 'deleteProduct',
            return: 'returnState', // returnId, returnState, returnArray
            item
        }).then((data) => {
            console.log("deleteProduct", data)
            if (data.Error == 0) {
                removeModal();
                removeItemTable(item);
                //productsIndex()
            }
        })
    });

    $("body").on("click", `.btnState[data-for="tableProduct"]`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let state = $(this).attr("state");
        console.log(`.btnState[for="tableProduct"]`, data);
        let id = data.id;

        changeStateProduct(id, state).then((data) => {
            console.log("changeStateProduct", data)
            if (data.Error == 0) {
                $(`.btnState[data-for="tableProduct"][data-id="${id}"]`).attr("state", data.response)
            }
        });
    });

    $("body").on("click", `#btnsNews .btnSlitItem`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        productsFormNew({
            typeProductId: data.id
        });
    });

    $("body").on("click", `#btnAddColor`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();

    });

    $("body").on("click", `#btnsNews0`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();

    });

    $("body").on("click", `#btnAddProductColor`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let numItem = $(`#${data.for} .itemColor`).length;
        $("#listProductsColors").addClass("on");
        $("#listProductsColors").html(renderSelectColor());
    });

    $("body").on("mouseleave", `#listProductsColors`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        $("#listProductsColors").removeClass("on");
    });

    $("body").on("click", `.btnSelectorColor`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();

        $("#listProductsColors").removeClass("on");
        let numItem = $(`#tbodyColor .itemColor[data-color='${data.color}']`).length;

        let title = `<i style="color:${data.color}" class="icon icon-checked-o"></i> <span>${data.name}</span>`;
        if (numItem == 0) {
            addHtml({
                selector: `#tbodyColor`,
                type: 'prepend',
                content: renderFormColor({
                    title,
                    color: data.color,
                    name: data.name,
                    id: `inputColor${data.id}`,
                    item: data.id
                })
            }) //type: html, append, prepend, before, after
        }
    });

    $("body").on("click", `.btnDeleteColorForm`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let name = data.name;
        let id = data.id;

        modalDelete({
            id: "deleteModal",
            name,
            cls: 'btnDeleteColorFormConfirm',
            attr: `data-name="${name}" data-id="${id}"`,
        })

    });

    $("body").on("click", `.btnDeleteColorFormConfirm`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();

        $(".itemColor[data-id='" + data.id + "']").remove();
        removeModal();

    });

})

/* document.addEventListener('click', function(event) {

    // If the clicked element doesn't have the right selector, bail
    if (!event.target.matches('#btnSaveProduct')) {
        //console.log(event)
        var element = event.target.offsetParent.id
        var form = document.getElementById(element).getAttribute("form")
        insertProduct(form)
    }

    // Don't follow the link
    event.preventDefault()
    event.stopPropagation()
    event.stopImmediatePropagation()

}, false); 
clicked element doesn 't have the right selector, bail
if (!event.target.matches('#btnSaveProduct')) {
    //console.log(event)
    var element = event.target.offsetParent.id
    var form = document.getElementById(element).getAttribute("form")
    insertProduct(form)
}

// Don't follow the link
event.preventDefault()
event.stopPropagation()
event.stopImmediatePropagation()

}, false); */