import { stopLoadingBar, loadView, accessToken, alertPage, replacePath, createInnerForm, todaysDate, formatDate, alertMessageError } from '../../components/functions.js';
import { resizeWorkspace } from '../../components/nav.js';
import { categorysListTable, createTable, dataTable, removeItemTable } from '../../components/tables.js'
import { errorInput } from '../../components/forms.js'
import { renderTrStock, renderStockEmpty, renderItemProductStock } from './renders/renderStock.js'
import { renderListWarehouses, renderListProviders } from './renders/renderInventory.js'
const module = "stock";
const system = "inventory";
const pathurl = "modules/inventory/";
const tableId = "tableStock";

export function stockIndex() {

    loadView(_PATH_WEB_NUCLEO + "modules/inventory/views/stock.html?" + _VS).then((htmlView) => {

        getData({
            task: 'getStock',
            return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
        }).then((response) => {
            console.log('getData getStock', response);
            if (response.status == 'success') {
                let data = response.data;
                if (data == 0) {
                    let str = htmlView;
                    str += renderStockEmpty();
                    $(".bodyModule[module='" + module + "']").html(str);
                    $(".bodyModule[module='" + module + "'] .tbody").html(strTable);
                } else {
                    let str = htmlView;
                    let strTable = '';
                    let tbody = ''; 

                    for (let i = 0; i < data.length; i++) {
                        const elem = data[i];
                        elem["table"] = tableId;
                        elem["module"] = module;
                        elem["system"] = system;
                        tbody += renderTrStock(elem);
                    }
                    strTable = createTable({
                        id: tableId,
                        thead: ':check,id:colId,Nombre,Codigo,Cuando se agote,Entrante,disponible,Estado:colState,Acciones:colActions',
                        body: tbody
                    })

                    str = str.replace(/{{_MODULE}}/g, module);
                    str = str.replace(/{{_SYSTEM}}/g, system);
                    str = str.replace(/{{_PATHURL}}/g, pathurl);
                    str = replacePath(str);
                    $(".bodyModule[module='" + module + "']").html(str);
                    $(".bodyModule[module='" + module + "'] .tbody").html(strTable);
                    dataTable({
                        elem: "#" + tableId,
                        orderCol: 1
                    });
                }

            } else {
                alertMessageError({ message: response.message })
            }
        }).catch(console.warn());

        /*loadStock().then((response) => {
            console.log(response);
            if (response.Error == 0) {
                const tableId = "tableStock";
                const data = response.items;
                let str = htmlView;
                let strTable = '';
                let tbody = '';

                //console.log(data)

                for (let i = 0; i < data.length; i++) {
                    const elem = data[i];
                    elem["table"] = tableId;
                    elem["module"] = module;
                    elem["system"] = system;
                    tbody += renderTrStock(elem);
                }
                strTable = createTable({
                    id: tableId,
                    thead: ':check,id:colId,Nombre,Codigo,Cuando se agote,Entrante,disponible:colState,Acciones:colActions',
                    body: tbody
                })

                str = str.replace(/{{_MODULE}}/g, module);
                str = str.replace(/{{_SYSTEM}}/g, system);
                str = str.replace(/{{_PATHURL}}/g, pathurl);
                str = replacePath(str);
                $(".bodyModule[module='" + module + "']").html(str);
                $(".bodyModule[module='" + module + "'] .tbody").html(strTable);
                dataTable({
                    elem: "#" + tableId,
                    orderCol: 1
                });

            } else if (response == 0) {
                let str = htmlView;
                str += renderStockEmpty();
                $(".bodyModule[module='" + module + "']").html(str);
                $(".bodyModule[module='" + module + "'] .tbody").html(strTable);
            } else {
                alertPage({
                    text: 'Error. por favor contactarse con soporte. ' + response.message,
                    icon: 'icn icon-alert-warning',
                    animation_in: 'bounceInRight',
                    animation_out: 'bounceOutRight',
                    tipe: 'danger',
                    time: '3500',
                    position: 'top-left'
                });
            }
        })*/
    }).catch(console.warn());

}

export function addStockItems(vars) {
    //console.log(vars);
    loadView(_PATH_WEB_NUCLEO + pathurl + "views/stockForm.html?vs" + _VS).then((htmlView) => {
        let str = htmlView;
        str = str.replace(/{{_TITLE}}/g, 'Stock Producto ' + vars.name);
        str = str.replace(/{{_BTN_NAME_ACTION}}/g, 'Añadir Stock');
        // str = str.replace(/{{_IMG}}/g, vars.img);
        str = str.replace(/{{_SIGLA}}/g, userData.initial);
        str = str.replace(/{{_COIN}}/g, _COIN_DEFAULT);
        str = str.replace(/{{_DATE_COMMENT}}/g, todaysDate('full'));
        str = str.replace(/{{_DATE_NOW}}/g, todaysDate());
        str = str.replace(/{{_DATE_NOW_DB}}/g, todaysDate('dbDateTime'));
        // str = str.replace(/{{_VALUE}}/g, vars.value);
        str = str.replace(/{{_MODULE}}/g, module);
        str = str.replace(/{{_SYSTEM}}/g, system);
        str = str.replace(/{{_PATHURL}}/g, pathurl);
        str = str.replace(/{{_FORM_ID}}/g, 'formAddItem');
        str = str.replace(/{{_BTN_ACTION}}/g, 'saveAddItemStock');
        str = str.replace(/{{_LIST_WAREHOUSES}}/g, renderListWarehouses(_WAREHOUSE_DEFAULT));
        str = str.replace(/{{_LIST_PROVIDERS}}/g, renderListProviders());
        str = str.replace(/{{_INITIAL_ITEM}}/g, renderItemProductStock({
            item: vars.item,
            name: vars.name,
            value: vars.value,
            img: vars.img,
            module: module,
            system: system
        }));
        str = replacePath(str);
        $(".bodyModule[module='" + module + "'] .innerForm").html(str)

        $('#inputReceptionLote').datetimepicker({
            timepicker: false,
            datepicker: true,
            format: 'd-m-Y H:i',
            lang: 'es'
        })

    })
}

//Callbacks
export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + 'modules/inventory/controllers/apis/v1/stock.php?' + _VS;
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars)
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data
        })
        //console.log(res.data);
        return res.data
    } catch (error) {
        console.log("Error: getDataPlaces", error);
    }
}

async function loadStock() {
    const url = _PATH_WEB_NUCLEO + 'modules/inventory/controllers/apis/v1/stock.php?' + _VS;
    const dataForm = new FormData()
    dataForm.append("accessToken", JSON.stringify(accessToken));
    dataForm.append("action", "loadStock");

    try {
        let res = await axios({
            async: true,
            method: 'post',
            responseType: 'json',
            url: url,
            headers: {},
            data: dataForm
        });
        //console.log(res.data);
        return res.data;
    } catch (error) {
        console.log(error);
    }
}

async function saveAddItemStock(vars) {
    const url = _PATH_WEB_NUCLEO + 'modules/inventory/controllers/apis/v1/stock.php?' + _VS;
    const dataForm = new FormData()
    dataForm.append("accessToken", JSON.stringify(accessToken));
    dataForm.append("action", "saveAddItem");
    dataForm.append("vars", JSON.stringify(vars));

    try {
        let res = await axios({
            async: true,
            method: 'post',
            responseType: 'json',
            url: url,
            headers: {},
            data: dataForm
        });
        console.log(res.data);
        return res.data;
    } catch (error) {
        console.log(error);
    }
}

document.addEventListener("DOMContentLoaded", function () {

    $("body").on("click", ".btnAddAmount", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        const id = $(this).attr("for");
        let value = $("#" + id).val();

        value = parseFloat(value) + 1;
        $("#" + id).val(value);
    })

    $("body").on("click", ".btnMinusAmount", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        const id = $(this).attr("for");
        let value = $("#" + id).val();
        if (value != 0) {
            value = parseFloat(value) - 1;
            $("#" + id).val(value);
        }
    })

    $("body").on("click", ".btnAddStockItems", function (e) {
        e.preventDefault();
        e.stopPropagation();
        let system = $(this).attr("system");
        let module = $(this).attr("module");
        let id = $(this).attr("for");
        let value = $("#" + id).val();
        let fn = $(this).attr("fn");
        let item = $(this).attr("item");
        let table = $(this).attr("table");
        let img = $("#" + table + " #imgId" + item).attr("src");
        let name = $(this).attr("name");
        let count = 0;

        if (value == 0) {
            count++
            errorInput(id)
            alertPage({
                text: 'Error. por favor añadir un valor diferente a  0. ',
                icon: 'icn icon-alert-warning',
                animation_in: 'bounceInRight',
                animation_out: 'bounceOutRight',
                tipe: 'danger',
                time: '3500',
                position: 'top-left'
            });
            $("#" + id).focus();
        }
        if (count == 0) {
            $("#" + id).val("0")
            createInnerForm({ system, module, fn });
            eval(fn + '({system:"' + system + '",module:"' + module + '",fn:"' + fn + '",item:"' + item + '",img:"' + img + '",name:"' + name + '","value":' + value + '})');
            resizeWorkspace();
        }

    })

    $("body").on("click", ".saveAddItemStock", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        //console.log("hello");
        let idForm = $(this).attr("form");
        const warehouseId = $("#" + idForm + " #inputWarehouse").val();
        const dateWarehouse = $("#" + idForm + " #inputDateWarehouse").val();
        const numInvoiceCreditor = $("#" + idForm + " #inputNumInvoiceCreditor").val();
        const numLote = $("#" + idForm + " #inputLote").val();
        const max = $("#" + idForm + " #inputMax").val();
        const min = $("#" + idForm + " #inputMin").val();
        const dat = $("#" + idForm + " #inputReceptionLote").val();
        const dateReceptionLote = formatDate({ date: dat, modal: 'db' });
        const providerId = $("#" + idForm + " #inputProvider").val();

        saveAddItemStock({
            warehouseId,
            dateWarehouse,
            numLote,
            dateReceptionLote,
            max,
            min,
            providerId,
            numInvoiceCreditor
        }).then((response) => {

        })
    })

})