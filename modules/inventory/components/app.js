var X = XLSX;
var actionFile = "";
var XW = {
    msg: 'xlsx',
    worker: './vendor/js/xlsx.full/xlsxworker.js'
};

var webworkers;

var ejecutar_webworkers = (function() {
    var OUT = document.getElementById('json');

    var crear_json = (function() {
        var fmt = document.getElementsByName("JSON");
        return function() {
            for (var i = 0; i < fmt.length; ++i)
                if (fmt[i].checked || fmt.length === 1) return fmt[i].value;
        };
    })();

    var to_json = function to_json(workbook) {
        var result = {};
        workbook.SheetNames.forEach(function(sheetName) {
            var roa = X.utils.sheet_to_json(workbook.Sheets[sheetName], { header: 1 });
            if (roa.length) result[sheetName] = roa;
        });
        return JSON.stringify(result, 2, 2);
    };

    return function ejecutar_webworkers(wb) {
        webworkers = wb;
        var output = "";
        switch (crear_json()) {
            default: output = to_json(wb);
        }
        //if(OUT.innerText === undefined){
        //OUT.textContent = output;
        //console.log("json:" + output);

        chargeXls(output);

        //}else {
        //OUT.innerText = output;
        //}
        // if(typeof console !== 'undefined'){
        // 	console.log("output", new Date());
        // 	console.log("json:"+output); 
        // }
    };
})();


async function chargeXls(vars) {

    let access_token = localStorage.getItem("access_token")
    let refresh_token = localStorage.getItem("refresh_token")
    let idEntitie = localStorage.getItem("idEntitie")
        //console.log(actionFile)

    const dataForm = new FormData();
    dataForm.append("objs", vars);
    dataForm.append("idEntitie", idEntitie)
    dataForm.append("access_token", access_token)
    dataForm.append("refresh_token", refresh_token)
    dataForm.append("action", actionFile)


    //console.log(vars);

    var init = {
        method: "POST",
        mode: "cors",
        cache: "default",
        body: dataForm,
    };
    //console.log(_PATH._PATH_WEB_NUCLEO);
    var url = _PATH_WEB + "controllers/api/v1/modules/importXls.php";
    try {
        let contentAwait = await fetch(url, init);
        let str = await contentAwait.text();
        console.log("chargeXls: " + str);
        //var dat = text.split(",");

        // var datArray = JSON.parse(text);

        // console.log("cargarExcels:" + datArray);
        // console.log("length:" + datArray.length);

    } catch (err) {
        console.log("Error:" + err);
        alert("Tu hoja de Excel tiene un conflicto, prueba eliminar espacios vacios o que tu primera hoja diga 'Hoja1'")
    }
}


var do_file = (function() {
    var rABS = typeof FileReader !== "undefined" && (FileReader.prototype || {}).readAsBinaryString;
    var domrabs = document.getElementsByName("userabs")[0];

    var use_worker = typeof Worker !== 'undefined';
    var domwork = document.getElementsByName("useworker")[0];

    var xw = function xw(data, cb) {
        var worker = new Worker(XW.worker);
        worker.onmessage = function(e) {
            switch (e.data.t) {
                case 'ready':
                    break;
                case 'e':
                    console.error(e.data.d);
                    break;
                case XW.msg:
                    cb(JSON.parse(e.data.d));
                    break;
            }
        };
        worker.postMessage({ d: data, b: rABS ? 'binary' : 'array' });
    };

    return function do_file(files) {

        var f = files[0];
        var reader = new FileReader();
        reader.onload = function(e) {
            if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
            var data = e.target.result;
            if (!rABS) data = new Uint8Array(data);
            if (use_worker) xw(data, ejecutar_webworkers);
            else ejecutar_webworkers(X.read(data, { type: rABS ? 'binary' : 'array' }));
        };
        if (rABS) reader.readAsBinaryString(f);
        else reader.readAsArrayBuffer(f);
    };
})();

(function() {
    var chargeFile = document.getElementById('fileXls');
    if (!chargeFile.addEventListener) return;
    actionFile = chargeFile.getAttribute("action");


    function handleFile(e) { do_file(e.target.files); }
    chargeFile.addEventListener('change', handleFile, false);
})();