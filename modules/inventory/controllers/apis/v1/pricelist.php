<?php
$access = json_decode($_POST["accessToken"], true);
require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $access["page"] . "config.php");
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

require_once(_PATH_NUCLEO . "modules/inventory/controllers/class/class.pricelist.php");
$pricelist = new PRICELIST($fmt);

header("Content-Type: application/json");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

switch ($_SERVER["REQUEST_METHOD"]) {
    case 'POST':
        $access_token = $access['access_token'];
        $refresh_token = $access['refresh_token'];
        $entitieId = $access['entitieId'];
        $action = $fmt->auth->getInput('action');

        $userId = $fmt->users->validateUserToken(array("access_token" => $access_token, "refresh_token" => $refresh_token, "idEntitie" => $entitieId));

        if ($userId) {
            $return["user"] = $fmt->users->userDataAuth(array("access_token" => $access_token, "idEntitie" => $entitieId));
            $userId = $return["user"]["userId"];
            $rolId = $return["user"]["rolId"];

            if ($action == "getListPrice") {
                $res = $pricelist->getListPrice(array("userId" => $userId, "rolId" => $rolId, "idEntitie" => $entitieId));

                $res["Error"] = 0;
                echo json_encode($res);
                //echo json_encode(0);
                exit(0);
            }
        } else {
            echo $fmt->errors->errorJson([
                "description" => "Access Auth. invalid user",
                "code" => "",
                "lang" => "es"
            ]);
            exit(0);
        }
        break;

    default:
        echo $fmt->errors->errorJson([
            "description" => "Access Auth. Metod request.",
            "code" => "",
            "lang" => "es"
        ]);
        exit(0);
        break;
}



$sql = "UPDATE  SET
        ='" . $$. "',
        ='" . $$. "'
        WHERE = '" .  . "' ";
$this->fmt->querys->consult($sql);