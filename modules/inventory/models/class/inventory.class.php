<?php
header('Content-Type: text/html; charset=utf-8');
class INVENTORY
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function listWarehouses()
    {
        $sql = "SELECT * FROM `mod_warehouses` WHERE mod_wrh_state > 0";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return[$i]["id"] = $row["mod_wrh_id"];
                $return[$i]["name"] = $row["mod_wrh_name"];
                $return[$i]["details"] = $row["mod_wrh_details"];
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function getWarehouses(array $var = [])
    {
        $state = $var["state"] ? $var["state"] : ">= 0";
        $sql = "SELECT * FROM `mod_warehouses` WHERE mod_wrh_state $state";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return[$i]["id"] = $row["mod_wrh_id"];
                $return[$i]["name"] = $row["mod_wrh_name"];
                $return[$i]["details"] = $row["mod_wrh_details"];
                $return[$i]["primary"] = $row["mod_wrh_primary"];
                $return[$i]["state"] = $row["mod_wrh_state"];
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function getTypesProducts(array $var = [])
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $state = $var["state"] ? $var["state"] : ">= 0";

        $sql = "SELECT * FROM mod_products_types WHERE mod_prod_type_ent_id = '" . $entitieId . "' AND mod_prod_type_state $state ORDER BY mod_prod_type_id DESC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return[$i]["id"] = $row["mod_prod_type_id"];
                $return[$i]["name"] = $row["mod_prod_type_name"];
                $return[$i]["subfix"] = $row["mod_prod_type_subfix"];
                $return[$i]["subfixDb"] = $row["mod_prod_type_subfix_db"];
                $return[$i]["json"] = $row["mod_prod_type_json"];
                $return[$i]["path"] = $row["mod_prod_type_path"];
                $return[$i]["primary"] = $row["mod_prod_type_primary"];
                $return[$i]["state"] = $row["mod_prod_type_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function getTypeProduct(int $id = 0)
    {
        $sql = "SELECT * FROM mod_products_types WHERE mod_prod_type_id = '" . $id . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $return["id"] = $row["mod_prod_type_id"];
            $return["name"] = $row["mod_prod_type_name"];
            $return["subfix"] = $row["mod_prod_type_subfix"];
            $return["subfixDb"] = $row["mod_prod_type_subfix_db"];
            $return["json"] = $row["mod_prod_type_json"];
            $return["path"] = $row["mod_prod_type_path"];
            $return["state"] = $row["mod_prod_type_state"];

            return $return;
        } else {
            return 0;
        }

    }

    public function getColorId(int $id = null, $entitieId = 1)
    {
        //return $id;

        $sql = "SELECT * FROM mod_products_colors WHERE mod_prod_color_id = '" . $id . "' AND mod_prod_color_ent_id = '" . $entitieId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $return["id"] = $row["mod_prod_color_id"];
            $return["name"] = $row["mod_prod_color_name"];
            $return["color"] = $row["mod_prod_color_hex"];
            $return["json"] = $row["mod_prod_color_json"];
            $return["state"] = $row["mod_prod_color_state"];

            return $return;
        } else {
            return 0;
        }
    }

    public function getColors(array $var = [])
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $state = $var["state"] ? $var["state"] : "> 0";
        $sql = "SELECT * FROM mod_products_colors WHERE mod_prod_color_ent_id = '" . $entitieId . "' AND mod_prod_color_state $state";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return[$i]["id"] = $row["mod_prod_color_id"];
                $return[$i]["name"] = $row["mod_prod_color_name"];
                $return[$i]["color"] = $row["mod_prod_color_hex"];
                $return[$i]["json"] = $row["mod_prod_color_json"];
                $return[$i]["state"] = $row["mod_prod_color_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function addTypeProducts(array $var = [])
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $inputs = $var["vars"]["inputs"];
        $name = $inputs["name"];
        $subfix = $inputs["subfix"];
        $subfixDb = $inputs["subfixDb"];
        $json = $inputs["json"];
        $state = $inputs["state"];
        $path = $inputs["path"];

        $insert = "mod_prod_type_name, mod_prod_type_subfix, mod_prod_type_subfix_db, mod_prod_type_json, mod_prod_type_path, mod_prod_type_ent_id, mod_prod_type_state";
        $values = "'" . $name . "','" . $subfix . "','" . $subfixDb . "','" . $json . "','" . $path . "','" . $entitieId . "','" . $state . "'";
        $sql = "insert into  mod_products_types (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_prod_type_id) as id from mod_products_types";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $id = $row["id"];

        return $id;

    }

    public function addColor(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $inputs = $var["vars"]["inputs"];
        $name = $inputs["name"];
        $color = $inputs["color"];
        $json = $inputs["json"];
        $state = $inputs["state"];

        $insert = "mod_prod_color_name,
        mod_prod_color_hex,
        mod_prod_color_json,
        mod_prod_color_ent_id,
        mod_prod_color_state";
        $values = "'" . $name . "','" . $color . "','" . $json . "','" . $entitieId . "','" . $state . "'";
        $sql = "insert into  mod_products_colors (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_prod_color_id) as id from mod_products_colors";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $id = $row["id"];

        return $id;

    }

    public function getOptionsInventory(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $sql = "SELECT * FROM mod_inventory_options WHERE mod_inv_opt_ent_id = '" . $entitieId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_inv_opt_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_inv_opt_name"];
                $return[$i]["value"] = $row["mod_inv_opt_value"];
                $return[$i]["autoload"] = $row["mod_inv_opt_autoload"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function getUnits(array $var = null)
    {
        $sql = "SELECT * FROM mod_inventory_units ORDER BY mod_inv_unt_id DESC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_inv_unt_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_inv_unt_name"];
                $return[$i]["summary"] = $row["mod_inv_unt_summary"];
                $return[$i]["vars"] = $row["mod_inv_unt_vars"];
                $return[$i]["state"] = $row["mod_inv_unt_state"];
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function updateConfigInventory(array $var = null)
    {
        //return $var;
        $inputs = $var["vars"]["input"];
        $type = $inputs["type"];
        $inputCategoryId = $inputs["inputCategoryId"];
        $entitieId = $var["entitieId"];

        if ($type == "categories") {
            $sql = "UPDATE mod_inventory_options SET
                    mod_inv_opt_value ='" . $inputCategoryId . "'
                    WHERE mod_inv_opt_name = 'routeCategoryId' AND mod_inv_opt_ent_id='" . $entitieId . "'";
            $this->fmt->querys->consult($sql);

            return 1;
        }

    }

}