<?php
header('Content-Type: text/html; charset=utf-8');
class STOCK 
{
    var $fmt;
    var $products;
    
    function __construct($fmt){
        $this->fmt = $fmt;
    } 

    function getStock(array $vars = null){
        //return $vars;
        $userId = $vars["user"]["userId"];
        $rolId = $vars["rolId"];
        $entitieId =  $vars["entitieId"];

        require_once("products.class.php");
        $this->products = new PRODUCTS($this->fmt);

        $sql = "SELECT DISTINCT * FROM mod_stock WHERE mod_stk_ent_id = '".$entitieId."' ORDER BY mod_stk_id DESC";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);

                $id = $row["mod_stk_id"];
                $prodId = $row["mod_stk_prod_id"];

                $return[$i]["id"] = $id;
                $return[$i]["sku"] = $row["mod_stk_sku"];
                $return[$i]["prodId"] = $prodId;
                
                $dataProd = $this->products->getDataId($prodId);
                $return[$i]["img"] = $dataProd["img"];
                $return[$i]["name"] = $dataProd["name"];
                $return[$i]["code"] = $dataProd["code"];
                
                /* if (!empty($dataProd["mod_prod_img"])){
                    $return[$i]["img"] = $this->fmt->files->urlAdd($this->fmt->files->urlFileId($row["mod_prod_img"]), "-thumb");
                }else {
                    $return[$i]["img"] = "";
                } */
                
                $return[$i]["quantity"] = $row["mod_stk_quantity"];
                $return[$i]["registerDate"] = $row["mod_stk_register_date"];
                $return[$i]["comment"] = $row["mod_stk_comment"];
                $return[$i]["invoiceId"] = $row["mod_stk_inv_id"];
                $return[$i]["max"] = $row["mod_stk_max"];
                $return[$i]["min"] = $row["mod_stk_min"];
                $return[$i]["alert"] = $row["mod_stk_alert"];
                $return[$i]["receiptDate"] = $row["mod_stk_receipt_date"];
                $return[$i]["lote"] = $row["mod_stk_lote"];
                $return[$i]["providerId"] = $row["mod_stk_prv_id"];
                $return[$i]["netPrice"] = $row["mod_stk_net_price"];
                $return[$i]["netPriceCoin"] = $row["mod_stk_net_price_coin"];
                $return[$i]["state"] = $row["mod_stk_state"];
            }
            return $return;
        } else {
            return 0;
        } 
        $this->fmt->querys->leave();
        
    }

     public function add(array $vars = null)
    {
        //return $vars;
        $inputQuantity = $vars["inputQuantity"] ? $vars["inputQuantity"] : 0;
        $inputStockMin = $vars["inputStockMin"] ? $vars["inputStockMin"] : 0;
        $inputStockMax = $vars["inputStockMax"] ? $vars["inputStockMax"] : 0;
        $inputSKU = $vars["inputSKU"] ? $vars["inputSKU"] : 0;
        $inputStockAlert = $vars["inputStockAlert"] ? $vars["inputStockAlert"] : 0;
        $inputStockAlertEmail = $vars["inputStockAlertEmail"] ? $vars["inputStockAlertEmail"] : 0;
        $inputComment = $vars["inputComment"];
        $inputInvoiceId = $vars["inputInvoiceId"];  
        $prodId = $vars["prodId"] ? $vars["prodId"] : 0;
        $inputReceipt = $vars["inputReceiptDate"]; 
        $inputLote = $vars["inputLote"] || 0;
        $inputProviderId = $vars["inputProviderId"];
        $inputNetPrice = $vars["inputNetPrice"] || 0;
        $inputNetPriceCoin = $vars["inputNetPriceCoin"] || $this->fmt->options->getValue('coin_default');
        $inputState = $vars["inputState"] ? $vars["inputState"] : 0;
        $now = $this->fmt->modules->dateFormat();

        if ($prodId == 0  || $inputQuantity == 0  ) {
            return 0;
        }
        
        $insert = "mod_stk_prod_id
        ,mod_stk_sku
        ,mod_stk_quantity
        ,mod_stk_register_date
        ,mod_stk_comment
        ,mod_stk_inv_id
        ,mod_stk_max
        ,mod_stk_min
        ,mod_stk_alert
        ,mod_stk_receipt_date
        ,mod_stk_lote
        ,mod_stk_prv_id
        ,mod_stk_net_price
        ,mod_stk_state";
        $values = "'" .$prodId. "','".$inputSKU."','".$inputQuantity."','".$now."','".$inputComment."','".$inputInvoiceId."','".$inputStockMax."','".$inputStockMin."','".$inputStockAlert."','".$inputReceipt."','".$inputLote."','".$inputProviderId."','".$inputNetPrice."','".$inputState."'";
        $sql="insert into mod_stock (".$insert.") values (".$values.")";
        $this->fmt->querys->consult($sql,__METHOD__);
 
        
        $sql = "select max(mod_stk_id) as id from mod_stock";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        
        return $row["id"];
        
    }

    public function getStockProdId(int $id = null){
        $sql = "SELECT * FROM mod_stock WHERE mod_stk_prod_id = '".$id."'";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
                $row=$this->fmt->querys->row($rs);
                $id = $row["mod_stk_id"];
                $return["id"] = $id;
                $return["quantity"] = $row["mod_stk_quantity"];
                $return["registerDate"] = $row["mod_stk_register_date"];
                $return["comment"] = $row["mod_stk_comment"];
                $return["invoiceId"] = $row["mod_stk_inv_id"];
                $return["max"] = $row["mod_stk_max"];
                $return["min"] = $row["mod_stk_min"];
                $return["alert"] = $row["mod_stk_alert"];
                $return["receiptDate"] = $row["mod_stk_receipt_date"];
                $return["lote"] = $row["mod_stk_lote"];
                $return["providerId"] = $row["mod_stk_prv_id"];
                $return["netPrice"] = $row["mod_stk_net_price"];
                $return["netPriceCoin"] = $row["mod_stk_net_price_coin"];
                $return["state"] = $row["mod_stk_state"];
            
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }
}