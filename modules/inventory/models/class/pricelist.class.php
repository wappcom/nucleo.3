<?php
header('Content-Type: text/html; charset=utf-8');
class PRICELIST
{

    var $fmt;
    var $products;
    var $stock;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
        require_once(_PATH_NUCLEO . "modules/inventory/controllers/class/products.class.php");
        $this->products = new PRODUCTS($fmt);
        require_once(_PATH_NUCLEO . "modules/inventory/controllers/class/stock.class.php");
        $this->stock = new STOCK($fmt);
    }

    public function getListPrice(array  $vars = null)
    {
        $usuId = $vars["usuId"];
        $rolId = $vars["rolId"];
        $entitieId = $vars["entitieId"];

        $sql = "SELECT DISTINCT * FROM mod_prices_list";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_prl_id"];
                $rw["items"][$i]["id"] = $id;
                $rw["items"][$i]["name"] = $row["mod_prl_name"];
                $rw["items"][$i]["baseList"] = $row["mod_prl_base_list"];
                $rw["items"][$i]["factor"] = $row["mod_prl_factor"];
                $rw["items"][$i]["roundingMethod"] = $row["mod_rounding_method"];
            }
            return $rw;
        } else {
            return 0;
        }
        $this->fmt->querys->leave();
    }
}