<?php
header('Content-Type: text/html; charset=utf-8');
class PRODUCTS
{

	var $fmt;
	var $pla;
	var $cat;
    var $inventory;
    var $stock;

	function __construct($fmt)
	{
		$this->fmt = $fmt;
        require_once("inventory.class.php");
        $this->inventory = new INVENTORY($fmt);
        
    }
    /* Products */
    public function dataId($idProd)
    {
        $sql = "SELECT * FROM mod_products WHERE mod_prod_id='".$idProd."'";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            return $this->fmt->querys->row($rs);
        }else{
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function getDataId(int $id = 0)
    {
        //return $id;
        $sql = "SELECT * FROM mod_products WHERE mod_prod_id='".$id."'";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            $row =  $this->fmt->querys->row($rs);
            $id = $row["mod_prod_id"];
            $return["id"] = $id;
            $return["name"] = $row["mod_prod_name"];
            $return["pathurl"] = $row["mod_prod_pathurl"];
            $return["description"] = $row["mod_prod_description"];
            $return["tags"] = $row["mod_prod_tags"];

            $type["id"] = $row["mod_prod_type"];
            $type["data"] = $this->inventory->getTypeProduct($row["mod_prod_type"]);
            $return["type"] = $type;
            $return["code"] = $row["mod_prod_code"];
            if (!empty($row["mod_prod_img"])){
                $return["img"] = $this->fmt->files->urlAdd($this->fmt->files->urlFileId($row["mod_prod_img"]), "-thumb");
                $return["imgPath"] = $this->fmt->files->urlAdd($this->fmt->files->urlFileId($row["mod_prod_img"]), "");
            }else {
                $return["img"] = "";
                $return["imgPath"] = "";
            }
            $return["categorys"] = $this->fmt->categorys->relationFrom(array("from" => "mod_products_categorys",
                "colId" => "mod_prod_cat_prod_id",
                "colCategory" => "mod_prod_cat_cat_id",
                "id" => $id,
                "orderBy" => "mod_prod_cat_order ASC"
            )); 

            $return["record_date"] = $row["mod_prod_record_date"];
            $return["state"] = $row["mod_prod_state"];
            return $return;   
            
        }else{
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function loadEditProduct($var = null)
    { 
        //return $var;
        $vars = $var["vars"]["inputs"];
        $userId = $vars["userId"];
        $item = $vars["id"];
        $entitieId = $var["entitieId"];

        $sql = "SELECT * FROM mod_products WHERE mod_prod_id='" . $item. "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_prod_id"];
            $type = $this->fmt->emptyReturn($row["mod_prod_type"],"1");
            $return["id"] = $id;
            $return["name"] = $row["mod_prod_name"];
            $return["pathurl"] = $row["mod_prod_pathurl"];
            $return["description"] = $row["mod_prod_description"];
            $return["tags"] = $row["mod_prod_tags"];

            if (!empty($row["mod_prod_img"])) {
                $img["id"] = $row["mod_prod_img"];
                $img["pathurl"] = $this->fmt->files->imgId($row["mod_prod_img"], "");
                $img["thumb"] = $this->fmt->files->urlAdd($this->fmt->files->urlFileId($row["mod_prod_img"]), "-thumb");
                $img["name"] = $this->fmt->files->name($row["mod_prod_img"], "");
                $return["img"] = $img;
            } else {
                $return["img"] = 0;
            }

            $return["code"] = $row["mod_prod_code"];
            $return["recordDate"] = $row["mod_prod_record_date"];
            $return["state"] = $row["mod_prod_state"];
            $return["type"] = $type;
            $return["categorys"] = $this->fmt->categorys->relationFrom(array("from"=>"mod_products_categorys",
                                                                            "colId" => "mod_prod_cat_prod_id", 
                                                                            "colCategory" => "mod_prod_cat_cat_id",
                                                                            "id" => $id,
                                                                            "orderBy" => "mod_prod_cat_order ASC"));
            $return["files"] = $this->fmt->files->relationFrom(array("from"=>"mod_products_files",
                                                                    "colItem"=>"mod_prod_file_prod_id",
                                                                    "colFiles"=>"mod_prod_file_file_id",
                                                                    "item" =>$id,
                                                                    "orderBy" =>"mod_prod_file_order ASC"));
            $return["dataTypes"] = $this->dataTypesProducts($id,$type,$entitieId);

            return $return;

        } else {
            return 0;
        }
        $this->fmt->querys->leave();
       
    }

    public function dataTypesProducts(int $id = null,int $type = null, $entitieId = 1)
    {
        //return $entitieId;
        $arrayType = $this->inventory->getTypeProduct($type);
        $pathType = $arrayType["path"];
        
        if ($type == 1){
            return 0;
        }

        if ($pathType=='clothingStore'){
            //clotingStore
            $sql = "SELECT * FROM mod_products_clothing_store WHERE mod_prod_cst_prod_id='".$id."'";
            $rs =$this->fmt->querys->consult($sql,__METHOD__);
            $num=$this->fmt->querys->num($rs);
            if($num>0){
                for($i=0;$i<$num;$i++){
                    $row=$this->fmt->querys->row($rs);
                    $id = $row["mod_prod_cst_id"];
                    $jsonColor = json_decode($row["mod_prod_cst_colors"],true);
                    $dataColor = [];

                    foreach ($jsonColor as $key => $value) {
                        $dataColor[$key]["id"] = $value["id"];
                        $dataColor[$key]["dataColor"] = $this->inventory->getColorId($value["id"], $entitieId); 
                        foreach ($value["mediaFiles"] as $keyMedia => $valueMedia) {
                            $dataColor[$key]["mediaFiles"][$keyMedia] = $this->fmt->files->dataBasicImg($valueMedia); 
                        }
                        
                    }
                    $return[$i]["id"] = $id;
                    $return[$i]["size"]=  json_decode($row["mod_prod_cst_size"],true);
                    $return[$i]["codebar"]= $row["mod_prod_cst_codebar"];
                    $return[$i]["contents"] = $row["mod_prod_cst_contents"];
                    $return[$i]["colors"] = $dataColor;
                    $return[$i]["notes"]= $row["mod_prop_cst_notes"];
                }
                return $return;
            } else {
                return 0;
            }
            $this->fmt->querys->leave($rs);
               
        }

        return 0;
        
    }

    public function dataIdDiscount($idDiscount)
    {
        $sql = "SELECT * FROM mod_discount WHERE mod_dis_id='".$idDiscount."'";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            return $this->fmt->querys->row($rs);
        }else{
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function loadProducts($vars)
    {
        $userId= $vars["userId"];
        $rolId= $vars["rolId"];
        $idEntitie =  $vars["entitieId"];

        require_once("stock.class.php");
        $this->stock = new STOCK($this->fmt);

        $sql = "SELECT DISTINCT * FROM mod_products";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $id = $row["mod_prod_id"];
                $rw[$i]["id"] = $id;
                $rw[$i]["name"] = $row["mod_prod_name"];
                $rw[$i]["description"] = $row["mod_prod_description"];
                $rw[$i]["tags"] = $row["mod_prod_tags"];
                $type["id"] = $row["mod_prod_type"];
                $type["data"] = $this->inventory->getTypeProduct($row["mod_prod_type"]);
                $rw[$i]["type"] = $type;
                $rw[$i]["code"] = $row["mod_prod_code"];
                if (!empty($row["mod_prod_img"])){
                    $rw[$i]["img"] = $this->fmt->files->urlAdd($this->fmt->files->urlFileId($row["mod_prod_img"]), "-thumb");
                }else {
                    $rw[$i]["img"] = "";
                }

                $rw[$i]["stock"] = $this->stock->getStockProdId($id);
                $rw[$i]["categorys"] = $this->fmt->categorys->relationFrom(array("from" => "mod_products_categorys",
                    "colId" => "mod_prod_cat_prod_id",
                    "colCategory" => "mod_prod_cat_cat_id",
                    "id" => $id,
                    "orderBy" => "mod_prod_cat_order ASC"
                )); 
                
                $rw[$i]["record_date"] = $row["mod_prod_record_date"];
                $rw[$i]["state"] = $row["mod_prod_state"];
            }
            return $rw;
        }else{
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function importXls($vars)
    {
        $obj = json_decode($vars["objsXls"],true);
        var_dump($obj);
        $entitieId = $vars["entitieId"];
        $userId = $vars["userId"];
        $action = $vars["action"];
        $numList = count($obj["Hoja1"]) - 1;

        switch ($action) {
            case 'chargeProducts':
                for ($i=1; $i < $numList; $i++) { 
                    $col = $obj["Hoja1"][$i];
                    $numCol = count ($col);
                    for ($j=0; $j < $numCol ; $j++) {
                        $id                 =$col[0];
                        $idTypeProduct      =$col[1];
                        $nameProduct        =$col[2];
                        $pathUrl            =$col[3];
                        $description        =$col[4];
                        $imgId              =$col[5];
                        $imgPathUrl         =$col[6];
                        $code               =$col[7];
                        $tags               =$col[8];
                        $idCat              =$col[9];
                        $nombreCat          =$col[10];
                        $price              =$col[11];
                        $listPrice          =$col[12];


                        return $idProd = $this->createProduct(array("name"=>$nameProduct,
                                                "pathUrl"=>$pathUrl,
                                                "description"=>$description,
                                                "tags"=>$tags,
                                                "imgId"=>$imgId,
                                                "imgPathUrl"=>$imgPathUrl,
                                                "code"=>$code,
                                                "price"=>$price,
                                                "listPrice"=>$listPrice
                                            ));
                        //$this->addStock()
                        //$this->addRelation()
                    }
                }
                break;
            
            default:
                # code...
                break;
        }
        
        //return $numList;
        //return $return;
        //var_dump($return);
        // return    
        //return $vars["objsXls"];
        //return $entitieId.":".$userId;
    }

    public function typesProducts()
    {
        $sql = "SELECT mod_prod_opt_id, mod_prod_opt_value,mod_prod_opt_autoload FROM mod_products_options WHERE mod_prod_opt_name='type'";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $return[$i]["id"] = $row["mod_prod_opt_id"];
                $return[$i]["name"] = $row["mod_prod_opt_value"];
                $return[$i]["active"] = $row["mod_prod_opt_autoload"];
            }
            return $return;
        }else{
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function getOptionsProducts($name="")
    {
        $sql = "SELECT  mod_prod_opt_value FROM mod_products_options WHERE mod_prod_opt_name='".$name."'";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            $row=$this->fmt->querys->row($rs);
            return $row["mod_prod_opt_value"];
        }else{
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function listProductsCatId($catId)
    {
        // $userId= $vars["userId"];
        // $rolId= $vars["rolId"];
        // $idEntitie =  $vars["entitieId"];

        $sql = "SELECT DISTINCT * FROM mod_products,mod_products_categorys WHERE mod_prod_cat_prod_id= mod_prod_id AND mod_prod_cat_cat_id='".$catId."' AND mod_prod_cat_ent_id='"._ID_ENTITIE."'  AND mod_prod_state > 0  ORDER BY mod_prod_cat_order ASC";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);

                $idRelation = $row["mod_prod_id_relation"];
                $idImg = $row["mod_prod_img"];
                $img = $this->fmt->files->urlFileId($idImg);

                $rw[$i]["id"] = $row["mod_prod_id"];
                $rw[$i]["name"] = $row["mod_prod_name"];
                $rw[$i]["description"] = $row["mod_prod_description"];
                $rw[$i]["tags"] = $row["mod_prod_tags"];
                $rw[$i]["code"] = $row["mod_prod_code"];
                $rw[$i]["img"] = $img;
                $rw[$i]["imgThumb"] = $this->fmt->files->urlAdd($img,'-thumb');
                $rw[$i]["recordDate"] = $row["mod_prod_record_date"];
                $rw[$i]["state"] = $row["mod_prod_state"];
                $rw[$i]["idRelation"] = $row["mod_prod_id_relation"];
                $rw[$i]["price"] = $row["mod_prod_price"];
                $rw[$i]["listPrice"] = $row["mod_prod_list_price"];

            }
            return $rw;
        }else{
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function discountProduct($idProd)
    {
        $arrayProd = $this->dataId($idProd);
        //$arrayDiscount = $this->dataIdDiscount($idProd);
        $price = number_format ($arrayProd["mod_prod_price"],2);

        $sql = "SELECT mod_dis_prod_dis_id FROM mod_discount_products,mod_discount WHERE 	mod_dis_prod_prod_id='".$idProd."' AND mod_dis_prod_dis_id=mod_dis_id AND mod_dis_state > 0 ";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            $row=$this->fmt->querys->row($rs);
            $disId = $row["mod_dis_prod_dis_id"];
            $arrayDiscount = $this->dataIdDiscount($disId);
            $rate = number_format (($arrayDiscount["mod_dis_rate"] / 100),2);
            
            //return   round ((number_format($arrayDiscount["mod_dis_rate"]) / 100 ),2,PHP_ROUND_HALF_UP);
            return    number_format ($price - ($price * $rate),2);
            //return   $arrayDiscount["mod_dis_rate"] ;
        }else{
            if (!empty($price)){
                return 0;
            }else{
                return $price;
            }
        }
        $this->fmt->querys->leave();
        
    }

    public function havePathUrl($pathUrl = null)
    {
        $sql = "SELECT * FROM mod_products  WHERE mod_prod_pathurl = '" . $pathUrl . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function createProduct(array $var = null )
    {
        //return $var;
        $vars= $var["vars"]["inputs"];
        $name = $vars["name"];
        $pathUrl = $vars["pathUrl"];
        $description = $vars["description"];
        $tags = $vars["tags"];
        $imgId = $vars["imgId"];
        $imgPathUrl = $vars["imgPathUrl"];
        $code = $vars["code"];
 
        $categorys = $vars["categorys"];
        $dataTypes = $vars["dataTypes"];
        $entitieId = $vars["entitieId"];
        $imageCharge = $vars["imageCharge"];

        $type = $vars["type"];
        $mediaFiles = $vars["mediaFiles"];
        $active = $this->fmt->emptyReturn($vars["active"],1);
        $now = $this->fmt->modules->dateFormat();
        
        if(empty($name)){
            $error[0] = "error";
            $error[1] = "No Name..";
            $error[2] = "code";
            return $error;
        }
        
        if (empty($pathUrl)){
            $pathUrl = $this->fmt->data->formatUri($name);
        }

        if($this->havePathUrl($pathUrl)){
            $error["Error"] = "error";
            $error["message"] = "exist-pathurl";
            $error["code"] = "code";

            return $error;
        }
        
        //$ingresar = "mod_prod_name,mod_prod_path_url,mod_prod_description,mod_prod_tags,mod_prod_img,mod_prod_code,mod_prod_record_date,mod_prod_id_relation,mod_prod_price,mod_prod_list_price,mod_prod_state";
        $ingresar = "mod_prod_name,mod_prod_pathurl,mod_prod_description,mod_prod_tags,mod_prod_img,mod_prod_code,mod_prod_record_date,mod_prod_type,mod_prod_state";
        $valores  = "'".$name."','".
                        $pathUrl."','".
                        $description."','".
                        $tags."','".
                        $imageCharge."','".
                        $code."','".
                        $now."','".
                        $type."','".
                        $active."'";
        $sql= "insert into mod_products (".$ingresar.") values (".$valores.")";
        $this->fmt->querys->consult($sql,__METHOD__);

        $sql = "select max(mod_prod_id) as id from mod_products";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $prodId = $row["id"];

        $insert = 'mod_prod_cat_prod_id,mod_prod_cat_cat_id,mod_prod_cat_ent_id,mod_prod_cat_order';
        $countCategorys = count($categorys);
        for ($i = 0; $i < $countCategorys; $i++) {
            $values = "'" . $prodId  . "','" . $categorys[$i] . "','" .$entitieId. "','". $i . "'";
            $sql = "insert into mod_products_categorys (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        $insert = 'mod_prod_file_prod_id,mod_prod_file_file_id,mod_prod_file_order';
        $countMediaFiles = count($mediaFiles);
        for ($i = 0; $i < $countMediaFiles ; $i++){
            $values = "'" . $prodId  . "','" . $mediaFiles[$i] . "','" . $i . "'";
            $sql = "insert into mod_products_files (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        if (!empty($type) && $dataTypes != "") {
            $dataTypeArray = $this->inventory->getTypeProduct($type);
            $fn = 'add'.$this->fmt->data->convertFn($dataTypeArray["subfixDb"]);
            $dataTypes["prodId"] = $prodId;
            $prodTypeId = $this->$fn($dataTypes);
            $stockId = $this->stock->add($dataTypes);
            
            $dataPriceBs["prodId"] = $prodId;
            $dataPriceBs["inputPrice"] = $dataTypes["inputPriceBs"];
            $dataPriceBs["inputPreviousPrice"] = $dataTypes["inputPreviousPriceBs"];
            $dataPriceBs["inputCoin"] = 'Bs';
            
            $dataPriceUsd["prodId"] = $prodId;
            $dataPriceUsd["inputPrice"] = $dataTypes["inputPriceUsd"];
            $dataPriceUsd["inputPreviousPrice"] = $dataTypes["inputPreviousPriceUsd"];
            $dataPriceUsd["inputCoin"] = 'Usd';
            
            $priceIdBs = $this->addPrice($dataPriceBs);
            $priceIdUsd = $this->addPrice($dataPriceUsd);

            $price["Bs"] = $priceIdBs;
            $price["Usd"] = $priceIdUsd; 
        }

        $return["Error"] = 0;
        $return["message"] = "success";
        $return["prodId"] =$prodId;
        $return["prodTypeId"] =$prodTypeId;
        $return["stockId"] = $stockId;
        $return["priceId"] = $price;
        
        return $return;
    }

    public function addClothingStore(array $var = null)
    {
        //return $var;
        $prodId = $var["prodId"];
        $codeBar = $var["inputCodebar"];
        $sizes = $var["inputSizes"];
        $colors = $var["inputColors"];
        $notes = $var["inputNotes"];
        $contents = $var["inputContents"];
        $priceBs = $var["inputPriceBs"] ? $var["inputPriceBs"] : 0;
        $priceUsd = $var["inputPriceUsd"] ? $var["inputPriceUsd"] : 0;
        $PreviousPriceBs = $var["inputPreviousPriceBs"] ? $var["inputPreviousPriceBs"] : 0;
        $PreviousPriceUsd = $var["inputPreviousPriceUsd"] ? $var["inputPreviousPriceUsd"] : 0;
        $stock = $var["inputStock"] ? $var["inputStock"] : 0;

        $insert = "mod_prod_cst_prod_id,mod_prod_cst_size,mod_prod_cst_codebar,mod_prod_cst_contents,mod_prod_cst_colors,mod_prop_cst_notes";

        $colors = json_encode($colors);
        $sizes = json_encode($sizes);
        $prices = json_encode(array(
            "priceBs" => $priceBs,
            "priceUsd" => $priceUsd,
            "PreviousPriceBs" => $PreviousPriceBs,
            "PreviousPriceUsd" => $PreviousPriceUsd,
        ));
        
        $values ="'" .$prodId. "','" .$sizes. "','" .$codeBar. "','" .$contents. "','" .$colors. "','" .$notes. "'";
        $sql= "insert into mod_products_clothing_store (".$insert.") values (".$values.")";
        $this->fmt->querys->consult($sql,__METHOD__);
                    
        $sql = "select max(mod_prod_cst_id) as id from mod_products_clothing_store";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        return  $row["id"];
    }

    public function updateProduct($vars = null)
    {
        //return $vars;
        $userId = $vars["user"]["userId"];
        $entitieId = $vars["entitieId"];
        $inputs = $vars["vars"]["inputs"];
        $prodId = $inputs["item"];
        $name = $inputs["name"];
        $pathUrl = $inputs["pathUrl"];
        $description = $inputs["description"];
        $tags = $inputs["tags"];
        $imgId = $inputs["imgId"];
        $imgPathUrl = $inputs["imgPathUrl"];
        $code = $inputs["code"];
        $price = $inputs["price"];
        $type = $inputs["type"];
        $listPrice = $inputs["listPrice"];
        $categorys = $inputs["categorys"];
        $mediaFiles = $inputs["mediaFiles"];
        $imageCharge = $inputs["imageCharge"];

        $active = $this->fmt->emptyReturn($inputs["active"], 1);
        $now = $this->fmt->modules->dateFormat();

        if (empty($pathUrl)) {
            $pathUrl = $this->fmt->data->formatUri($name);
        }
        //return $pathUrl;
        $sql = "UPDATE mod_products SET
            mod_prod_name='" . $name. "',
            mod_prod_description='" . $description. "',
            mod_prod_tags='" . $tags. "',
            mod_prod_pathurl='" . $pathUrl . "',
            mod_prod_code='" . $code . "',
            mod_prod_type='" . $type . "',
            mod_prod_img ='" . $imageCharge . "',
            mod_prod_state='" . $active . "',
            mod_prod_record_date='" . $now . "'
            WHERE mod_prod_id= '" . $prodId . "'";
        $this->fmt->querys->consult($sql);

        $this->fmt->modules->deleteItem(array("from" => "mod_products_categorys", 
                                                "column" => "mod_prod_cat_prod_id", 
                                                "item" => $prodId));

        $insert = 'mod_prod_cat_prod_id,mod_prod_cat_cat_id,mod_prod_cat_ent_id,mod_prod_cat_order';
        $countCategorys = count($categorys);
        for ($i = 0; $i < $countCategorys; $i++) {
            $values = "'" . $prodId  . "','" . $categorys[$i] . "','" . $entitieId . "','" . $i . "'";
            $sql = "insert into mod_products_categorys (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        $this->fmt->modules->deleteItem(array(
            "from" => "mod_products_files",
            "column" => "mod_prod_file_prod_id",
            "item" => $prodId
        ));

        $insert = 'mod_prod_file_prod_id,mod_prod_file_file_id,mod_prod_file_order';
        $countMediaFiles = count($mediaFiles);
        for ($i = 1; $i < $countMediaFiles; $i++) {
            $values = "'" . $prodId  . "','" . $mediaFiles[$i] . "','" . $i . "'";
            $sql = "insert into mod_products_files (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }


        return 1;
       
    }

    public function deleteProduct($var = null)
    { 
        //return $var;
        $vars = $var["vars"];
        $userId = $var["user"]["userId"];
        $item = $vars["item"];

        $sqle="DELETE FROM mod_products WHERE mod_prod_id='".$item."' ";
        $this->fmt->querys->consult($sqle, __METHOD__);
        $up_sqr6 = "ALTER TABLE mod_products AUTO_INCREMENT=1";
        $this->fmt->querys->consult($up_sqr6,__METHOD__);

        $sqle="DELETE FROM mod_products_categorys WHERE mod_prod_cat_prod_id='".$item."' ";
        $this->fmt->querys->consult($sqle, __METHOD__);

        $sql = "SELECT mod_prod_cst_colors FROM mod_products_clothing_store WHERE mod_prod_cst_prod_id='".$item."'";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $json = json_decode($row["mod_prod_cst_colors"], true);
                $countMedia = count($json);

                for ($j = 0; $j < $countMedia; $j++) {
                    $array = $json[$j]["mediaFiles"];
                    $count = count($array);
                    for ($k=0; $k < $count; $k++) { 
                        $id = $array[$k];
                        $path = _PATH_HOST_FILES.$this->fmt->files->urlFileId($id);
                        $pathThumb = $this->fmt->files->urlAdd($path,'-thumb');
                        $this->fmt->files->deleteFile($path);
                        $this->fmt->files->deleteFile($pathThumb);
                        $this->fmt->files->deleteId($id);
                    }
                    
                }
            } 
        }  
        $this->fmt->querys->leave($rs);
        
        $sqle= "DELETE FROM mod_products_clothing_store WHERE mod_prod_cst_prod_id='".$item."' ";
        $this->fmt->querys->consult($sqle, __METHOD__);
        $up_sqr6 = "ALTER TABLE mod_products_clothing_store AUTO_INCREMENT=1";
        $this->fmt->querys->consult($up_sqr6,__METHOD__);
        
        $sqle7= "DELETE FROM mod_products_prices_list WHERE mod_prod_prl_prod_id='".$item."' ";
        $this->fmt->querys->consult($sqle7, __METHOD__);
        $up_sqr67 = "ALTER TABLE mod_products_prices_list AUTO_INCREMENT=1";
        $this->fmt->querys->consult($up_sqr67,__METHOD__);

        $sql = "SELECT mod_prod_file_file_id FROM mod_products_files WHERE mod_prod_file_prod_id='".$item."'";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $id = $row["mod_prod_file_file_id"];
                $path = _PATH_HOST_FILES.$this->fmt->files->urlFileId($id);
                $pathThumb = $this->fmt->files->urlAdd($path,'-thumb');
                $this->fmt->files->deleteFile($path);
                $this->fmt->files->deleteFile($pathThumb);
                $this->fmt->files->deleteId($id);
            }   
        } 
        $this->fmt->querys->leave($rs);

        $sqle8= "DELETE FROM mod_products_files WHERE mod_prod_file_prod_id='".$item."' ";
        $this->fmt->querys->consult($sqle8, __METHOD__);
        $up_sqr68 = "ALTER TABLE mod_products_files AUTO_INCREMENT=1";
        $this->fmt->querys->consult($up_sqr68,__METHOD__);

        $sqle9= "DELETE FROM mod_stock WHERE mod_stk_prod_id='".$item."' ";
        $this->fmt->querys->consult($sqle9, __METHOD__);
        $up_sqr69 = "ALTER TABLE mod_stock AUTO_INCREMENT=1";
        $this->fmt->querys->consult($up_sqr69,__METHOD__);

        return 1;
       
    }

    public function changeStateProduct($item = null, $state=null)
    { 

        if($state==1){ $std=0; }
        if($state==0){ $std=1; }
       
        $sql = "UPDATE mod_products SET
           mod_prod_state='" . $std. "'
           WHERE mod_prod_id ='" . $item . "'";
        $this->fmt->querys->consult($sql);

        return $std;
       
    }

    /*Brands*/
    public function listBrands()
    {
        $sql = "SELECT * FROM mod_brands  WHERE mod_brd_state > 0";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $idImg = $row["mod_brd_img"];
                $img = $this->fmt->files->urlFileId($idImg);
                $rw[$i]["id"] = $row["mod_brd_id"];
                $rw[$i]["name"] = $row["mod_brd_name"];
                $rw[$i]["img"] = $img;
            }
            return $rw;
        } else {
            return 0;
        }
        $this->fmt->querys->leave();
    }

    /* Providers */
    public function listProviders()
    {
        $userId = $vars["userId"];
        $rolId = $vars["rolId"];
        $idEntitie =  $vars["entitieId"];

        $sql = "SELECT DISTINCT * FROM mod_providers WHERE mod_prv_state > 0";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_prv_id"];
                $rw[$i]["id"] = $id;
                $rw[$i]["name"] = $row["mod_prv_name"];
                $rw[$i]["description"] = $row["mod_prv_description"];
                if (!empty($row["mod_prv_img_id"])) {
                    $rw[$i]["img"] = $this->fmt->files->urlAdd($this->fmt->files->urlFileId($row["mod_prv_img_id"]), "-thumb");
                } else {
                    $rw[$i]["img"] = "";
                }
 
                $rw[$i]["state"] = $row["mod_prv_state"];
                $rw[$i]["type"] = $row["mod_prv_type"];
                $rw[$i]["registerDate"] = $row["mod_prv_register_date"];
            }
            return $rw;
        } else {
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function getParameters(array $var = null)
    {

        $rtn["typesProducts"]   = $this->inventory->getTypesProducts($var);
        $rtn["warehouses"] =$this->inventory->getWarehouses($var);
        $rtn["colorsProducts"] =$this->inventory->getColors($var);
        return $rtn;
        
    }

    public function deleteItemsMedia(array $var = null)
    {
        //return $var;
        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars =  $var["vars"];
        $items = $vars["inputs"]["items"];
        $itemId = $vars["inputs"]["prodId"];

        if (is_array($items)) {
            $count = count($items);

            for ($i = 0; $i < $count; $i++) {
                $id = $items[$i];
                $this->fmt->files->deteleFileId($id);

                if ($itemId != "undefined") {
                    $sql = "UPDATE mod_products SET
                        mod_prod_img ='0'
                    WHERE mod_prod_id = '" . $itemId . "' ";
                    $this->fmt->querys->consult($sql);
                }
                
                $sql = "DELETE FROM mod_products_files WHERE mod_prod_file_file_id = '" . $id . "'";
                $this->fmt->querys->consult($sql);
                $up_sqr6 = "ALTER TABLE mod_products_files AUTO_INCREMENT=1";
                $this->fmt->querys->consult($up_sqr6,__METHOD__);
            }
        }else{
            if ($itemId != "undefined") {
                $sql = "UPDATE mod_products SET
                    mod_prod_img ='0'
                WHERE mod_prod_id = '" . $itemId . "' ";
                $this->fmt->querys->consult($sql);
            }

            $sql = "DELETE FROM mod_products_files WHERE mod_prod_file_file_id = '" . $items . "'";
            $this->fmt->querys->consult($sql);
            $up_sqr6 = "ALTER TABLE mod_products_files AUTO_INCREMENT=1";
            $this->fmt->querys->consult($up_sqr6,__METHOD__);
        }
        
        return 1;
    }

    public function addPrice(array $var = null)
    {
        //return $var;
        $inputPrice = floatval($var["inputPrice"] ? $var["inputPrice"] : 0);
        $inputPreviousPrice = floatval($var["inputPreviousPrice"] ? $var["inputPreviousPrice"] : 0);
        $inputCoin = $var["inputCoin"] ? $var["inputCoin"]:  'Bs';
        $prodId = $var["prodId"] ? $var["prodId"] : 0;
        $now = $this->fmt->modules->dateFormat();

        if ($prodId == 0 ) {
            return 0;
        }

        $insert = "mod_prod_prl_prod_id,
        mod_prod_prl_price,
        mod_prod_prl_Previous_price,
        mod_prod_prl_coin,
        mod_prod_prl_register_date";
        $values ="'" .  $prodId . "','".$inputPrice."','".$inputPreviousPrice."','".$inputCoin."','".$now."'";
        $sql= "insert into  mod_products_prices_list (".$insert.") values (".$values.")";
        $this->fmt->querys->consult($sql,__METHOD__);

        $sql = "select max(mod_prod_prl_id) as id from mod_products_prices_list";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        
        return $row["id"];
        
    }

    public function restoreBd(array $var = null)
    {
        //return $var;
        $this->fmt->modules->truncate("files");
        $this->fmt->modules->truncate("mod_products");
        $this->fmt->modules->truncate("mod_products_categorys");
        $this->fmt->modules->truncate("mod_products_clothing_store");
        $this->fmt->modules->truncate("mod_products_files");
        $this->fmt->modules->truncate("mod_products_pharmacy");
        $this->fmt->modules->truncate("mod_products_pharmacy_stock");
        $this->fmt->modules->truncate("mod_products_prices_list");
        $this->fmt->modules->truncate("mod_products_food");
        $this->fmt->modules->truncate("mod_stock");
    }

}