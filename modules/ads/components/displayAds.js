import {
    loadView,
    addHtml
} from "../../components/functions.js";

const module = "displayAds";
const system = "ads";

export const displayAdsIndex = (vars = []) => {
    //console.log('displayAdsIndex', vars);

    loadView(_PATH_WEB_NUCLEO + "modules/ads/views/displayAds.html?" + "?" + _VS).then((responseView) => {
        //console.log('loadView', responseView);
        let content = responseView;

        content = content.replace(/{{_BTN_TITLE}}/g, "Guardar");
        content = content.replace(/{{_BTN_CLS}}/g, "btnPrimary");
        content = content.replace(/{{_BTN_ID}}/g, "btnAddDisplayAd");
        content = content.replace(/{{_FORM_ID}}/g, "formDisplayAd");

        addHtml({
            selector: `.bodyModule[module="${module}"]`,
            type: 'insert', //insert, append, prepend, replace
            content
        })

    }).catch(console.warn());

}