import {
    loadView,
    addHtml
} from "../../components/functions.js";

const module = "campaignsAds";
const system = "ads";

export const campaignsAdsIndex = (vars = []) => {
    console.log('campaignsAdsIndex', vars);

    loadView(_PATH_WEB_NUCLEO + "modules/ads/views/campaignsAds.html" + "?" + _VS).then((responseView) => {
        //console.log('loadView', responseView);
        let content = responseView;

        content = content.replace(/{{_BTN_TITLE}}/g, "Guardar");
        content = content.replace(/{{_BTN_CLS}}/g, "btnPrimary");
        content = content.replace(/{{_BTN_ID}}/g, "btnAddDisplayAd");
        content = content.replace(/{{_FORM_ID}}/g, "formDisplayAd");

        addHtml({
            selector: `.bodyModule[module="${module}"]`,
            type: 'insert', //insert, append, prepend, replace
            content
        })

    }).catch(console.warn());

}