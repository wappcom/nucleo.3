<?php
class SERVICES
{

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }


    public function getServicesAll(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];

        $sql = "SELECT * FROM mod_services WHERE mod_srv_ent_id = '".$entitieId."'";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_srv_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_srv_name"];
                $return[$i]["description"] = $row["mod_srv_description"];
                $return[$i]["duration"] = $row["mod_srv_duration"];
                $return[$i]["type"] = $row["mod_srv_type"];
                $return[$i]["price"] = $row["mod_srv_price"];
                $return[$i]["state"] = $row["mod_srv_state"];
                $return[$i]["active"] = $row["mod_srv_active"];
                $return[$i]["created"] = $row["mod_srv_created"];
                $return[$i]["modified"] = $row["mod_srv_modified"];
                $return[$i]["deleted"] = $row["mod_srv_deleted"];
            }
            return $return;
        } else {
            return 0;
        }
    }

}