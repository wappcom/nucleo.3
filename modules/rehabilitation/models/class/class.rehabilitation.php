<?php
header('Content-Type: text/html; charset=utf-8');
class REHABILITATION
{
    var $fmt;
    var $accounts;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
        require_once(_PATH_NUCLEO."modules/accounts/models/class/class.accounts.php");
        $this->accounts = new ACCOUNTS($fmt);
    }

    public function dataId(Int $id = null)
    {

        // cont_id,cont_title,cont_description,cont_tags,cont_pathurl,cont_img,cont_body,cont_author,cont_register_date,cont_notitle,cont_url,cont_target,cont_btn_title,cont_cls,cont_state
        $sql = "SELECT * FROM contents WHERE cont_id='" . $id . "' AND cont_state > 0";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["cont_id"];
                $return["id"] = $id;
                $return["title"] = $row["cont_title"];
                $return["summary"] = $row["cont_summary"];
                $return["pathurl"] = $row["cont_pathurl"];
                $return["tags"] = $row["cont_tags"];
                $return["body"] = $row["cont_body"];
                $return["author"] = $row["cont_author"];
                $return["registerDate"] = $row["cont_register_date"];
                $return["notitle"] = $row["cont_notitle"];
                $return["url"] = $row["cont_url"];
                $return["target"] = $row["cont_target"];
                $return["cls"] = $row["cont_cls"];
                $return["state"] = $row["cont_state"];
                $return["btnTitle"] = $row["cont_btn_title"];
                if ($row["cont_img"]) {
                    $img["id"] = $row["cont_img"];
                    $img["pathurl"] = $this->fmt->files->imgId($row["cont_img"], "");
                    $img["name"] = $this->fmt->files->name($row["cont_img"], "");
                } else {
                    $img = 0;
                }

                $return["img"] = $img;
                //$return["media"] = $this->relationFromFiles($id);
                $return["categorys"] = $this->fmt->categorys->relationFrom(array(
                    "from" => "contents_categorys",
                    "colId" => "cont_cat_cont_id",
                    "id" => $id,
                    "colCategory" => "cont_cat_cat_id",
                    "orderBy" => "cont_cat_order"
                ));
            }
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function dataTutotId(Int $id = 0){
       // return $var;
        $sql = "SELECT * FROM mod_rehabilitation_tutor WHERE mod_rhb_tutor_id='".$id."'";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $id = $row["mod_rhb_tutor_id"];
                $return[$i]["id"] = $id;
                $rtn["name"] = $row["mod_rhb_tutor_name"];
                $rtn["fathers_lastname"] = $row["mod_rhb_tutor_fathers_lastname"];
                $rtn["mothers_lastname"] = $row["mod_rhb_tutor_mothers_lastname"];
                $rtn["rhb_id"] = $row["mod_rhb_tutor_rhb_id"];
                $rtn["gender"] = $row["mod_rhb_tutor_gender"];
                $rtn["ci"] = $row["mod_rhb_tutor_ci"];
                $rtn["ext"] = $row["mod_rhb_tutor_ext"];
                $rtn["dial"] = $row["mod_rhb_tutor_dial"];
                $rtn["celular"] = $row["mod_rhb_tutor_celular"];
                $rtn["relationship"] = $row["mod_rhb_tutor_relationship"];
                $rtn["address"] = $row["mod_rhb_tutor_address"];
                $rtn["intern_ids"] = $row["mod_rhb_tutor_intern_ids"];
                $rtn["coordinates"] = $row["mod_rhb_tutor_coordinates"];
                $rtn["state"] = $row["mod_rhb_state"];
            }
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function loadRehabbeds(array $var = null)
    {
        //return $var;
        $sql = "SELECT * FROM mod_rehabilitation_user ORDER BY mod_rhb_register_date DESC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_rhb_id"];
                $acuId = $row["mod_rhb_acu_id"];
                $userId = $row["mod_rhb_user_id"];
                $dataAccount =  $this->accounts->dataId($acuId);

                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_rhb_name"];
                $return[$i]["fathersLastname"] = $dataAccount["mod_acu_fathers_lastname"];
                $return[$i]["mothersLastname"] = $dataAccount["mod_acu_mothers_lastname"];
                $return[$i]["internmentDate"] = $row["mod_rhb_internment_date"];
                $return[$i]["gradationDate"] = $row["mod_rhb_gradation_date"];
                $return[$i]["leavingDate"] = $row["mod_rhb_leaving_date"];
                $return[$i]["numInternment"] = $row["mod_rhb_num_internment"];
                $return[$i]["userRegister"] = $this->fmt->users->fullName($userId);
                $return[$i]["state"] = $row["mod_rhb_state"];
            }
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function loadDataRehabbed(array $var = null)
    {
        return $var;      
    }

    public function deleteRegistry(array $var = null)
    {
        $id = $var["vars"]["item"];

        $sql = "SELECT mod_rhb_tutor_id FROM mod_rehabilitation_tutor WHERE mod_rhb_tutor_rhb_id = '" .  $id . "'";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        $row=$this->fmt->querys->row($rs);
        $tutorId = $row["mod_rhb_tutor_id"];

        $sql = "SELECT mod_rhb_acu_id FROM mod_rehabilitation_user WHERE mod_rhb_id = '" .  $id . "'";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        $row=$this->fmt->querys->row($rs);
        $acuId = $row["mod_rhb_acu_id"];

        $sql = "DELETE FROM mod_accounts_users WHERE mod_acu_id = '" .  $acuId . "'";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "ALTER TABLE mod_accounts_users AUTO_INCREMENT = 1";
        $this->fmt->querys->consult($sql, __METHOD__);
        
        $sql = "DELETE FROM mod_rehabilitation_user WHERE mod_rhb_id = '" .  $id . "'";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "ALTER TABLE mod_rehabilitation_user AUTO_INCREMENT = 1";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "DELETE FROM mod_rehabilitation_tutor WHERE mod_rhb_tutor_id = '" .  $id . "'";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "ALTER TABLE mod_rehabilitation_tutor AUTO_INCREMENT = 1";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "DELETE FROM mod_accounts_services WHERE mod_acu_sv_acu_id = '" .  $acuId . "'";
        $this->fmt->querys->consult($sql, __METHOD__);


        return 1;
    }

    public function saveRehabbed(array $var = null)
    {
        return $var;

        $entitieId = $var["entitieId"];
        $userId = $var["user"]["userId"];
        $input = $var["vars"]["inputs"];
        $inputName = $input["inputName"];
        $inputFathersLastname = $input["inputFathersLastname"];
        $inputMothersLastname = $input["inputMothersLastname"];
        $inputGender = $input["inputGender"];
        $inputCI = $input["inputCI"];
        $inputExt = $input["inputExt"];
        $inputBirthday = $input["inputBirthday"];
        $inputNationality = $input["inputNationality"];
        $inputAgeFirstUse = $input["inputAgeFirstUse"];
        $inputTEPCYR = $input["inputTEPCYR"];
        $inputDateIntern = $input["inputDateIntern"];
        $inputPrimaryCompuption = $input["inputPrimaryCompuption"];
        $inputObservations = $input["inputObservations"];
        $inputNameTutor = $input["inputNameTutor"];
        $inputFathersLastnameTutor = $input["inputFathersLastnameTutor"];
        $inputMothersLastnameTutor = $input["inputMothersLastnameTutor"];
        $inputGenderTutor = $input["inputGenderTutor"];
        $inputCITutor = $input["inputCITutor"];
        $inputExtTutor = $input["inputExtTutor"];
        $inputCelularTutor = $input["inputCelularTutor"];
        $inputRelationship = $input["inputRelationship"];
        $inputEmailTutor = $input["inputEmailTutor"];
        $inputAddressTutor = $input["inputAddressTutor"];

        $rtn["entitieId"] = $entitieId;
        $rtn["vars"]  = [
            "email" => "no-email",
            "name" => $inputName,
            "fathersLastname" => $inputFathersLastname,
            "mothersLastname" => $inputMothersLastname,
            "gender" => $inputGender,
            "ci" => $inputCI,
            "ext" => $inputExt,
            "birthday" => $inputBirthday
        ];

        $today = $this->fmt->modules->dateFormat();

        //create account user
        $acuId = $this->accounts->add($rtn);

        if ($acuId["Error"] == 1) {
            $rtn["Error"] = 1;
            $rtn["status"] = 'success';
            $rtn["data"]["userData"] = $acuId["userData"];
            $rtn["data"]["userId"] = $acuId["userId"]; 
            $rtn["Message"] = "Error al crear la cuenta de usuario ya existe";

            return $rtn;
        }

        //create relation service
        $insert = "mod_acu_sv_sv_id,mod_acu_sv_acu_id,mod_acu_sv_order";
        $values ="'2','" . $acuId . "','1'";
        $sql= "insert into  mod_accounts_services (".$insert.") values (".$values.")";
        $this->fmt->querys->consult($sql,__METHOD__);

        //create rehab user
        $insert = "mod_rhb_name,mod_rhb_lastname,mod_rhb_acu_id,mod_rhb_tepcyr,mod_rhb_birthday,mod_rhb_nationality,mod_rhb_age_first_use,mod_rhb_register_date,mod_rhb_user_id,mod_rhb_state";
        $values ="'" .$inputName. "','" .$inputFathersLastname." ".$inputMothersLastname. "','" .$acuId. "','1','" .$inputBirthday. "','" .$inputNationality. "','" .$inputAgeFirstUse. "','" .$today. "','" .$userId. "','1'";
        $sql= "insert into  mod_rehabilitation_user (".$insert.") values (".$values.")";
        $this->fmt->querys->consult($sql,__METHOD__);

        $sql = "select max(mod_rhb_id) as id from mod_rehabilitation_user";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $rhbUserId = $row["id"];

        //create tutor
        $tutorId = $this->checkCITutor($inputCITutor, $inputExtTutor);
        if ($tutorId !=0){
            $rtn["data"]["tutor"] = $this->dataTutotId($tutorId);
            $rtn["data"]["tutorId"] = $tutorId; 
        }else{
            $insert = "mod_rhb_tutor_name,mod_rhb_tutor_fathers_lastname,mod_rhb_tutor_mothers_lastname,mod_rhb_tutor_rhb_id,mod_rhb_tutor_gender,mod_rhb_tutor_ci,mod_rhb_tutor_ext,mod_rhb_tutor_dial,mod_rhb_tutor_celular,mod_rhb_tutor_relationship,mod_rhb_tutor_address,mod_rhb_tutor_intern_ids,mod_rhb_tutor_coordinates,mod_rhb_tutor_state";
            $values ="'" . $inputNameTutor . "','" . $inputFathersLastnameTutor . "','" . $inputMothersLastnameTutor . "','" . $rhbUserId . "','" . $inputGenderTutor . "','" . $inputCITutor . "','" . $inputExtTutor . "','591','" . $inputCelularTutor . "','" . $inputRelationship . "','" . $inputAddressTutor . "','','','1'";
            $sql= "insert into mod_rehabilitation_tutor (".$insert.") values (".$values.")";
            $this->fmt->querys->consult($sql,__METHOD__);

            $sql = "select max(mod_rhb_tutor_id) as id from mod_rehabilitation_tutor";
            $rs = $this->fmt->querys->consult($sql, __METHOD__);
            $row = $this->fmt->querys->row($rs);
            $tutorId = $row["id"];

            $rtn["data"]["tutor"] = $this->dataTutotId($tutorId);
            $rtn["data"]["tutorId"] = $tutorId; 
        }

        $rtn["Error"] = 0;
        $rtn["status"]  = "success";
        $rtn["data"]["acuId"] = $acuId;
        $rtn["data"]["rhbUserId"] = $rhbUserId;

        return $rtn;

        
    }

    public function getDataRehabRegistry(array $var = null){
        
        $rtn["getPrimaryConsumption"] = $this->getPrimaryConsumption();
        $rtn["getRehabCenters"] = $this->getRehabCenters();

        return $rtn;
    }

    public function getPrimaryConsumption(array $var = null){
        //return $var;
        $sql = "SELECT * FROM mod_rehabilitation_primary_consumption";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $id = $row["mod_rhb_pcn_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_rhb_pcn_name"];
                $return[$i]["path"] = $row["mod_rhb_pcn_path"];
                $return[$i]["order"] = $row["mod_rhb_pcn_order"];
                $return[$i]["state"] = $row["mod_rhb_pcn_state"];
            }
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function getRehabCenters(array $var = null){
       //return $var;
        $sql = "SELECT * FROM mod_rehabilitation_centers";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $id = $row["mod_rhb_center_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_rhb_center_name"];
                $return[$i]["address"] = $row["mod_rhb_center_address"];
                $return[$i]["coord"] = $row["mod_rhb_center_coord"];
                $return[$i]["userId"] = $row["mod_rhb_center_user_id"];
                $return[$i]["entId"] = $row["mod_rhb_center_ent_id"];
                $return[$i]["state"] = $row["mod_rhb_center_state"];
            }
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function checkCITutor($ci = null, $ext = null)
    {
        if ($ci == null) {
            return 0;
        }

        if ($ext == null) {
            $ex = "AND mod_rhb_tutor_ext='" . $ext . "'";
        }else{
            $ex = "";
        }

        $sql = "SELECT * FROM mod_rehabilitation_tutor WHERE mod_rhb_tutor_ci='" . $ci . "' ".$ex;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_rhb_tutor_id"];
        } else {
            return 0;
        }
    }
}