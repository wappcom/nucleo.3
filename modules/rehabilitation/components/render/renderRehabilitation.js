import { capitalize } from "../../../components/functions.js";

export const renderSidebar = (vars = []) => {
  console.log("renderSidebar", vars);
  let contentId = "content" + capitalize(vars.id);
  return /*html*/ `
    <li>
        <a class="btn btnIconLeft btnMenuModal active" data-module="${vars.module}" data-modal-id="${vars.id}"
            data-for="${contentId}" data-content='primaryConsumption'>
            <i class="icon icon-alert-warning"></i>
            <span lang="es">Consumo Principal</span>
        </a>
    </li>
    <li>
        <a class="btn btnIconLeft btnMenuModal active" data-module="${vars.module}" data-modal-id="${vars.id}"
            data-for="${contentId}" data-content='rehabCenters'>
            <i class="icon icon-blocks"></i>
            <span lang="es">Centros de Rehabilitación</span>
        </a>
    </li>  
    `;
};
