import {
	empty,
	replacePath,
	replaceEssentials,
	replaceAll,
	loadView,
	stopLoadingBar,
	emptyReturn,
	listCategorys,
	checkCategorys,
	loadingBtnIcon,
	btnLoadingRemove,
	unDisableId,
	accessToken,
	removeBtnLoading,
	addHtml,
	capitalize,
	jointActions,
	alertMessageError,
	removeSelector
} from "../../components/functions.js";
import {
	dayLiteral
} from "../../components/dates.js";
import {
	editorText,
	errorInput,
	errorInputSelector,
	loadInitChargeMedia,
	loadItemMedia,
	loadFilesFormLoader,
	loadSingleFileFormLoader,
	mountForm
} from "../../components/forms.js";
import {
	deleteModalItem,
	removeModal,
	deleteModal
} from "../../components/modals.js";
import {
	categorysListTable,
	createTable,
	dataTable,
	removeItemTable,
	inactiveBtnStateItem,
	activeBtnStateItem,
	mountTable,
	colCheck,
	colId,
	colName,
	colTitle,
	colState,
	colActionsBase,
} from "../../components/tables.js";
import {
	renderCol,
	renderColCheck,
	renderColId,
	renderColTitle,
	renderRowsTable,
	renderColState,
	renderColActions,
	renderNoDataTable
} from "../../components/renders/renderTables.js";
import {
	innerForm,
	renderDeleteModal
} from "../../components/renders/renderModals.js";
import {
	renderSelect,
	renderRadioButton
} from "../../components/renders/renderForms.js";

const module = "rehabilitationRegistry";
const system = "rehabilitation";
let pathurl = "modules/rehabilitation/";
let tableId = "rehabilitationRegistryTable";
let formId = "rehabilitationRegistryForm";

export const rehabilitationRegistryIndex = (vars) => {
	//console.log('rehabilitationRegistryIndex',vars);
	loadView(_PATH_WEB_NUCLEO + pathurl + "views/register.html?" + _VS).then(
		(htmlView) => {
			getData({
				task: 'loadRehabbeds',
				return: 'returnArray', // returnId, returnState, returnArray
			}).then((response) => {

				if (response.status == "success") {
					let content = response;

					$(".bodyModule[module='" + module + "']").html(
						replaceEssentials({
							str: htmlView,
							fn: 'formNewRehabbed',
							module,
							system,
							pathurl
						})
					);

					if (response.data == 0) {
						addHtml({
							selector: `.bodyModule[module="${module}"] .tbody`,
							type: 'prepend',
							content: renderNoDataTable()
						}) //type: html, append, prepend, before, after

						return false;
					}

					let rows = "";
					for (let i in response.data) {
						let item = response.data[i];
						rows += renderRowsTable({
							id: item.id,
							content: renderColCheck({
									id: item.id
								}) +
								renderColId({
									id: item.id
								}) +
								renderCol({
									id: item.id,
									data: item.fathersLastname,
								}) +
								renderCol({
									id: item.id,
									data: item.mothersLastname,
								}) +
								renderColTitle({
									id: item.id,
									title: item.name,
								}) +
								renderCol({
									id: item.id,
									data: dayLiteral({
										dateInput: item.internmentDate,
										mode: "compact"
									}),
								}) +
								renderCol({
									id: item.id,
									data: dayLiteral({
										dateInput: item.gradationDate,
										mode: "compact"
									}),
								}) +
								renderCol({
									id: item.id,
									data: dayLiteral({
										dateInput: item.leavingDate,
										mode: "compact"
									}),
								}) +
								renderCol({
									id: item.id,
									data: item.numInternment,
								}) +
								renderColState({
									id: item.id,
									state: item.state,
									module,
									system,
								}) +
								renderColActions({
									id: item.id,
									name: item.name,
									type: "btnEditFn,btnDeleteFn",
									module,
									system,
								}),
						});
					}

					mountTable({
						id: tableId,
						columns: [
							...colCheck,
							...colId,
							{
								label: "Apellido Paterno",
								cls: "colName",
							},
							{
								label: "Apellido Materno",
								cls: "colName",
							},
							...colName,
							{
								label: "F.Internación",
								cls: "colName",
							},
							{
								label: "F.Graduación",
								cls: "colName",
							},
							{
								label: "F.Abandono",
								cls: "colName",
							},
							{
								label: "Num.Internación",
								cls: "colName",
							},
							...colState,
							...colActionsBase,
						],
						rows,
						module,
						system,
						container: ".bodyModule[module='" + module + "'] .tbody",
					});

				} else {
					alertMessageError({
						message: response.message
					});
				}

			}).catch(console.warn()); // loadRehabbeds
		}).catch(console.warn()); // loadView
};

export const loadRehabbeds = async (vars) => {
	//console.log("loadRehabbeds");
	const url =
		_PATH_WEB_NUCLEO +
		"modules/rehabilitation/controllers/apis/v1/rehabilitation.php";
	let data = JSON.stringify({
		accessToken: accessToken(),
		action: "loadRehabbeds",
	});

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "text",
			url: url,
			headers: {},
			data: data,
		});
		//console.log(res.data);
		return res.data;
	} catch (error) {
		console.log("Error: loadRehabbeds");
		console.log(error);
	}
};

export const loadDataRehabbed = async (vars) => {
	console.log('loadDataRehabbed', vars);
	const url = _PATH_WEB_NUCLEO + "";
	let data = JSON.stringify({
		accessToken: accessToken(),
		action: "loadDataRehabbed",
		vars: JSON.stringify(vars)
	});

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: data
		})
		//console.log(res.data);
		return res.data
	} catch (error) {
		console.log("Error: loadDataRehabbed")
		console.log(error)
	}
}

export const formNewRehabbed = (vars) => {
	loadView(
		_PATH_WEB_NUCLEO + "modules/rehabilitation/views/formRehabbed.html?" + _VS
	).then((htmlView) => {
		let str = replaceEssentials({
			str: htmlView,
			title: 'Nuevo Registro',
			btnNameAction: 'Dar de Alta',
			module,
			system,
			btnAction: 'btnSaveRehabbed',
			formId
		});

		let arrayCounteselors = [];

		getData({
			task: 'getDataRehabRegistry',
			return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject

		}).then((response) => {
			//console.log('getData getPrimaryConsumption', response);
			if (response.status == 'success') {
				let dataPrimaryConsumption = response.data.getPrimaryConsumption;
				let dataRehabCenters = response.data.getRehabCenters;

				str = str.replaceAll(/\{{_SELECT_PRIMARY_CONSUMPTION}}/g, renderSelect({
					array: dataPrimaryConsumption,
					id: "inputPrimaryCompuption",
					selected: ""
				}));

				str = str.replaceAll(/\{{_SELECT_CENTER}}/g, renderRadioButton({
					array: dataRehabCenters,
					id: "inputRehabCenters",
					checkedValue: 1
				}));

				innerForm({
					module,
					body: str,
				});

				$("#inputBirthday").datetimepicker({
					timepicker: false,
					datepicker: true,
					format: "Y-m-d",
					lang: "es",
				})
				$("#inputDateIntern").datetimepicker({
					timepicker: false,
					datepicker: true,
					format: "Y-m-d",
					lang: "es",
				})
			} else {
				alertMessageError({
					message: response.message
				})
			}
		}).catch(console.warn());


	});
}

export const getData = async (vars = []) => {
	//console.log('getData', vars);
	const url = _PATH_WEB_NUCLEO + "modules/rehabilitation/controllers/apis/v1/rehabilitation.php";
	let data = JSON.stringify({
		accessToken: accessToken(),
		vars: JSON.stringify(vars)
	});
	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: data
		})
		//console.log(res.data);
		jointActions(res.data);
		return res.data
	} catch (error) {
		console.log("Error: rehabilitation", error);
	}
}

document.addEventListener("DOMContentLoaded", function () {
	$("body").on("click", `.btnDeleteFn[data-module='${module}']`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let id = $(this).attr("data-id");
		let name = $(this).attr("data-name");

		console.log(`.btnDeleteFn[data-fn='deleteTableProduct']`, data);

		addHtml({
			selector: ".bodyModule[module='" + module + "']",
			type: 'prepend',
			content: renderDeleteModal({
				name: "El alumno " + name,
				id: "modalDelete" + tableId,
				cls: 'btnDelete' + capitalize(tableId),
				attr: `data-id="${id}"`,
			})
		})
	});

	$("body").on("click", `.btnDeleteRehabilitationRegistryTable`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		let id = $(this).attr("data-id");
		console.log(`.btnDeleteRehabilitationRegistryTable`, data);

		getData({
			task: 'deleteRegistry',
			return: 'returnState', // returnId, returnState, returnArray
			item: id
		}).then((response) => {
			console.log('getData deleteRegistry', response);
			if (response.status == 'success') {
				removeSelector(`#modalDeleterehabilitationRegistryTable`);
				removeItemTable(id, tableId);
			} else {
				alertMessageError({
					message: response.message
				})
			}
		}).catch(console.warn());
	});

	$("body").on("click", `.btnSaveRehabbed`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		console.log(`.btnSaveRehabbed`, data);

		let count = 0;
		let inputName = $(`#${formId} #inputName`).val();
		let inputFathersLastname = $(`#${formId} #inputFathersLastname`).val();
		let inputMothersLastname = $(`#${formId} #inputMothersLastname`).val();
		let inputGender = $(`#${formId} #inputGender`).val();
		let inputCI = $(`#${formId} #inputCI`).val();
		let inputExt = $(`#${formId} #inputExt`).val();
		let inputBirthday = $(`#${formId} #inputBirthday`).val();
		let inputNationality = $(`#${formId} #inputNationality`).val();
		let inputAgeFirstUse = $(`#${formId} #inputAgeFirstUse`).val();
		let inputTEPCYR = $(`#${formId} #inputTEPCYR`).val();
		let inputDateIntern = $(`#${formId} #inputDateIntern`).val();
		let inputPrimaryCompuption = $(`#${formId} #inputPrimaryCompuption`).val();
		let inputObservations = $(`#${formId} #inputObservations`).val();
		let inputNameTutor = $(`#${formId} #inputNameTutor`).val();
		let inputFathersLastnameTutor = $(`#${formId} #inputFathersLastnameTutor`).val();
		let inputMothersLastnameTutor = $(`#${formId} #inputMothersLastnameTutor`).val();
		let inputGenderTutor = $(`#${formId} #inputGenderTutor`).val();
		let inputCITutor = $(`#${formId} #inputCITutor`).val();
		let inputExtTutor = $(`#${formId} #inputExtTutor`).val();
		let inputCelularTutor = $(`#${formId} #inputCelularTutor`).val();
		let inputRelationship = $(`#${formId} #inputRelationship`).val();
		let inputEmailTutor = $(`#${formId} #inputEmailTutor`).val();
		let inputAddressTutor = $(`#${formId} #inputAddressTutor`).val();
		let inputCoordTutor = $(`#${formId} #inputCoordTutor`).val();

		if (inputName == '') {
			errorInputSelector(`#${formId} #inputName`, 'El nombre es requerido');
			count++;
		}
		if (inputFathersLastname == '') {
			errorInputSelector(`#${formId} #inputFathersLastname`, 'El apellido paterno es requerido');
			count++;
		}
		if (inputMothersLastname == '') {
			errorInputSelector(`#${formId} #inputMothersLastname`, 'El apellido materno es requerido');
			count++;
		}

		if (inputCI == '') {
			errorInputSelector(`#${formId} #inputCI`, 'El CI es requerido');
			count++;
		}

		if (inputBirthday == '') {
			errorInputSelector(`#${formId} #inputBirthday`, 'La fecha de nacimiento es requerida');
			count++;
		}

		if (inputDateIntern == '') {
			errorInputSelector(`#${formId} #inputDateIntern`, 'La fecha de internación es requerida');
			count++;
		}

		if (inputNameTutor == '') {
			errorInputSelector(`#${formId} #inputNameTutor`, 'El nombre del tutor es requerido');
			count++;
		}
		if (inputFathersLastnameTutor == '') {
			errorInputSelector(`#${formId} #inputFathersLastnameTutor`, 'El apellido paterno del tutor es requerido');
			count++;
		}
		if (inputMothersLastnameTutor == '') {
			errorInputSelector(`#${formId} #inputMothersLastnameTutor`, 'El apellido materno del tutor es requerido');
			count++;
		}
		if (inputCITutor == '') {
			errorInputSelector(`#${formId} #inputCITutor`, 'El CI es requerido');
			count++;
		}
		if (inputCelularTutor == '') {
			errorInputSelector(`#${formId} #inputCelularTutor`, 'El celular es requerido');
			count++;
		}

		let inputs = {
			inputName,
			inputFathersLastname,
			inputMothersLastname,
			inputGender,
			inputCI,
			inputExt,
			inputBirthday,
			inputNationality,
			inputAgeFirstUse,
			inputTEPCYR,
			inputDateIntern,
			inputPrimaryCompuption,
			inputObservations,
			inputNameTutor,
			inputFathersLastnameTutor,
			inputMothersLastnameTutor,
			inputGenderTutor,
			inputCITutor,
			inputExtTutor,
			inputCelularTutor,
			inputRelationship,
			inputEmailTutor,
			inputAddressTutor
		}

		if (count == 0) {
			getData({
				task: 'saveRehabbed',
				return: 'returnObject', // returnId, returnState, returnArray
				inputs
			}).then((response) => {
				console.log('getData saveRehabbed', response);
				if (response.status == 'success') {

				} else {
					alertMessageError({
						message: response.message
					})
				}
			}).catch(console.warn());
		}
	});

});