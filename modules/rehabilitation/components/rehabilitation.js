import { addHtml,jointActions,accessToken, loadView,capitalize } from "../../components/functions.js";
import { renderModalConf } from "../../components/renders/renderModals.js";
import { actionBtnModule } from "../../components/nav.js";
import { renderSidebar } from "./render/renderRehabilitation.js";
import {
    mountMacroTable,
    clearMacroTable
} from "../../components/tables.js";
import {
    focusInput
} from "../../components/forms.js";

const module = "rehabilitation";
const system = "rehabilitation";
const tablePrimaryConsumption = "tablePrimaryConsumption";

export const rehabilitationIndex = (vars) => {
    //console.log('rehabilitationRegistryIndex',vars);

    $(".bodyModule[system='" + system + "'][module='" + module + "']").html("Dashboard Rehabilitation");
    //actionBtnModule({module:"rehabilitationRegistry",system,pathurl:'rehabilitationRegistry'});
}

export const primaryConsumptionIndex = (vars =[]) => {
    console.log('primaryConsumptionIndex',vars);

    getData({
        task: 'getPrimaryConsumption',
        return: 'returnArray', // returnId, returnState, returnArray, returnMessage, returnObject
    }).then((response) => {
        //console.log('getData getPrimaryConsumption', response);
        if(response.status=='success'){
            loadView(_PATH_WEB_NUCLEO + "modules/rehabilitation/views/primaryConsumptionForm.html?" + _VS).then((responseView) => {
                //console.log('loadView', responseView);
                let content = responseView;

                let tableMacro = mountMacroTable({
                    tableId: tablePrimaryConsumption,
                    data: response.data,
                    cols: [{
                        label: "id",
                        cls: 'colId',
                        type: "id",
                        render: 'id',
                    }, {
                        label: "*Nombre",
                        cls: 'colName',
                        type: "text",
                        render: 'name',
                        attrName: 'inputName',
                        id: 'inputName',
                    }, {
                        label: "Detalle",
                        cls: '',
                        type: "text",
                        render: 'summary',
                        attrName: 'inputSummary',
                        id: 'inputSummary',
                    }, {
                        label: "Path",
                        cls: 'colPath',
                        type: "text",
                        render: 'path',
                        attrName: 'inputPath',
                        id: 'inputPath',
                    }, {
                        label: "Orden",
                        cls: 'colOrder',
                        type: "text",
                        render: 'order',
                        attrName: 'inputOrder',
                        id: 'inputOrder',
                    }, {
                        label: "Estado",
                        cls: 'colState',
                        type: "state",
                        render: 'state',
                        attrName: 'inputState',
                        id: 'inputState',
                    }]
                })
                content = content.replaceAll(/\{{_BODY}}/g, tableMacro);

                addHtml({
                    selector: '#'+vars.id, 
                    type: 'insert', 
                    content  
                }) //type: html, append, prepend, before, after

                focusInput("inputName");
                
            }).catch(console.warn());
        }else{
             alertMessageError({message: response.message})
        }
    }).catch(console.warn());

    
}

export const getData = async (vars = []) => {
   //console.log('getData', vars);
   const url = _PATH_WEB_NUCLEO + "modules/rehabilitation/controllers/apis/v1/rehabilitation.php";
   let data = JSON.stringify({
       accessToken: accessToken(),
       vars: JSON.stringify(vars)
   });
   try {
       let res = await axios({
           async: true,
           method: "post",
           responseType: "json",
           url: url,
           headers: {},
           data: data
        })
       //console.log(res.data);
       jointActions(res.data);
       return res.data
   } catch (error) {
      console.log("Error: getData ", error);
   }
}

document.addEventListener("DOMContentLoaded", function () {
    $("body").on("click",`#btnConfigRehabitation`, function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        let id = 'modalConfigRehabilitation';
        let contentId = "content" + capitalize(id);
        //console.log(`#btnConfigRehabitation`,data);
        
        addHtml({
            selector: 'body',
            type: 'prepend',
            content: renderModalConf({
                module,
                system,
                title: 'Configuración de rehabilitación',
                id,
                sidebar : renderSidebar({module,system, id}),
                body : ``
            })
        }) //type: html, append, prepend, before, after
        primaryConsumptionIndex({id:contentId});
    });
});
