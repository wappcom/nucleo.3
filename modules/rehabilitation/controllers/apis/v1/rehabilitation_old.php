<?php
/* $foo = file_get_contents("php://input"); echo json_encode($foo, true); exit(0);*/

$dataPost = json_decode(file_get_contents("php://input"), true);
//echo json_encode($dataPost); exit(0);
//echo json_encode($dataPost["accessToken"]["access_token"]); exit(0);

require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $dataPost["accessToken"]["page"] . "config.php");
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

require_once(_PATH_NUCLEO . "modules/rehabilitation/models/class/class.rehabilitation.php");
$rehabilitation = new REHABILITATION($fmt);

header("Content-Type: application/json");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

switch ($_SERVER["REQUEST_METHOD"]) {
    case 'POST':
        $access_token = $dataPost["accessToken"]['access_token'];
        $refresh_token = $dataPost["accessToken"]['refresh_token'];
        $entitieId = $dataPost["accessToken"]['entitieId'];
        $action = $dataPost["action"];
        //echo json_encode("1:". $action); exit(0);
        //echo json_encode("2:". $entitieId); exit(0);

        $userId = $fmt->users->validateUserToken(array("access_token" => $access_token, "refresh_token" => $refresh_token, "idEntitie" => $entitieId));

        //echo json_encode($userId); exit(0);
        if ($userId) {
            $return["user"] = $fmt->users->userDataAuth(array("access_token" => $access_token, "idEntitie" => $entitieId));
            $return["vars"] = json_decode($dataPost['vars'], true);
            $return["entitieId"] = $entitieId;
            $userId = $return["user"]["userId"];
            $rolId = $return["user"]["rolId"];

            if ($action == "loadRehabbeds") {

                $state = $rehabilitation->loadRehabbeds($return);

                //echo json_encode($state); exit(0);

                if (count($state) > 0) {
                    $resume["Error"] = 0;
                    $resume["items"] = $state;
                    echo json_encode($resume);
                } elseif ($state == 0) {
                    echo 0;
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Error, ",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
                exit(0);
            }

            if ($action == "loadDataRehabbed") {
                $state = $rehabilitation->loadDataRehabbed($return);
                echo json_encode($state); exit(0);
                
                if (count($state) > 0) {
                    $resume["Error"] = 0;
                    $resume["data"] = $state;
                    echo json_encode($resume);
                } elseif ($state == 0) {
                    echo 0;
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Error, ",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
                exit(0);
            }
        } else {
            echo $fmt->errors->errorJson([
                "description" => "Access Auth. invalid user",
                "code" => "",
                "lang" => "es"
            ]);
            exit(0);
        }


        break;

    default:
        echo $fmt->errors->errorJson([
            "description" => "Access Auth. Metod request.",
            "code" => "",
            "lang" => "es"
        ]);
        exit(0);
        break;
}
