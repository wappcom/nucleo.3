<?php
header('Content-Type: text/html; charset=utf-8');
class PLACES
{

    var $fmt;
    var $pla;
    var $cat;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function dataId(array $var = null)
    {
        $entitieId = $var['entitieId'];
        $vars = $var['vars'];
        $id = $vars['id'];

        $sql = "SELECT * FROM mod_places WHERE mod_plc_id = '" . $id . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_plc_id"];
            $return["id"] = $id;
            $return["name"] = $row["mod_plc_name"];
            $return["tags"] = $row["mod_plc_tags"];
            $return["address"] = $row["mod_plc_address"];
            $return["city"] = $row["mod_plc_city"];
            $return["country"] = $row["mod_plc_country"];
            $return["phone"] = $row["mod_plc_phone"];
            $return["celularWhatsapp"] = $row["mod_plc_celular_whatsapp"];
            $return["info"] = $row["mod_plc_info"];
            $return["img"] = $row["mod_plc_img"];
            $return["profileIcon"] = $row["mod_plc_profile_icon"];
            $return["mainCoord"] = $row["mod_plc_main_coord"];
            $return["perimeter"] = $row["mod_plc_perimeter"];
            $return["mapIcon"] = $row["mod_plc_map_icon"];
            $return["content"] = $row["mod_plc_content"];
            //$return["startHour"]=$row["mod_plc_start_hour"];
            //$return["startEnd"]=$row["mod_plc_start_end"];
            $return["acuId"] = $row["mod_plc_acu_id"];
            $return["billId"] = $row["mod_plc_bill_id"];
            $return["status"] = $row["mod_plc_status"];
            $return["ranking"] = $row["mod_plc_ranking"];
            $return["code"] = $row["mod_plc_code"];
            $return["mapUbication"] = $row["mod_plc_map_ubication"];
            $return["fullDay"] = $row["mod_plc_24h"];
            $return["entId"] = $row["mod_plc_ent_id"];
            $return["json"] = $row["mod_plc_json"];
            $return["categorys"] = $this->fmt->categorys->relationFrom(["from" => "mod_places_categorys", "colCategory" => "mod_plc_cat_cat_id", "colId" => "mod_plc_cat_plc_id", "id" => $id]);
            $return["state"] = $row["mod_plc_state"];

            return $return;
        } else {
            return 0;
        }
    }
    public function getPlace(array $var = null)
    {

        return $this->dataId($var);
    }

    public function getPlaces(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        $orderBy = $this->fmt->emptyReturn($array["vars"]["orderBy"], "DESC");
        $limit = $this->fmt->emptyReturn($array["vars"]["limit"], "");

        if ($limit != "") {
            $limit = "LIMIT " . $limit;
        }

        $sql = "SELECT * FROM mod_places WHERE mod_plc_ent_id = '" . $entitieId . "' ORDER BY  mod_plc_id " . $orderBy . $limit;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_plc_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_plc_name"];
                $return[$i]["tags"] = $row["mod_plc_tags"];
                $return[$i]["json"] = $row["mod_plc_json"];
                $return[$i]["address"] = $row["mod_plc_address"];
                $return[$i]["city"] = $row["mod_plc_city"];
                $return[$i]["country"] = $row["mod_plc_country"];
                $return[$i]["phone"] = $row["mod_plc_phone"];
                $return[$i]["celularWhatsapp"] = $row["mod_plc_celular_whatsapp"];
                $return[$i]["info"] = $row["mod_plc_info"];
                $return[$i]["img"] = $row["mod_plc_img"];
                $return[$i]["profileIcon"] = $row["mod_plc_profile_icon"];
                $return[$i]["mainCoord"] = $row["mod_plc_main_coord"];
                $return[$i]["perimeter"] = $row["mod_plc_perimeter"];
                $return[$i]["mapIcon"] = $row["mod_plc_map_icon"];
                $return[$i]["content"] = $row["mod_plc_content"];
                //$return[$i]["startHour"] = $row["mod_plc_start_hour"];
                //$return[$i]["startEnd"] = $row["mod_plc_start_end"];
                $return[$i]["acuId"] = $row["mod_plc_acu_id"];
                $return[$i]["contentId"] = $row["mod_plc_cont_id"];
                $return[$i]["status"] = $row["mod_plc_status"];
                $return[$i]["ranking"] = $row["mod_plc_ranking"];
                $return[$i]["code"] = $row["mod_plc_code"];
                $return[$i]["mapUbication"] = $row["mod_plc_map_ubication"];
                $return[$i]["json"] = $row["mod_plc_json"];
                $return[$i]["24h"] = $row["mod_plc_24h"];
                $return[$i]["state"] = $row["mod_plc_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function changeState(array $var = null)
    {
        //return $var;
        $vars = $var["vars"];
        $id = $vars["id"];
        $state = $vars["state"];

        $sql = "UPDATE mod_places SET
              mod_plc_state='" . $state . "'
              WHERE mod_plc_id = '" . $id . "' ";
        $this->fmt->querys->consult($sql);

        return 1;
    }

    public function delete(array $var = null)
    {
        //return $var;

        $vars = $var["vars"];
        $id = $vars["id"];

        $sql = "DELETE FROM mod_places WHERE mod_plc_id='" . $id . "'";
        $this->fmt->querys->consult($sql);
        $up_sqr6 = "ALTER TABLE mod_places AUTO_INCREMENT=1";
        $this->fmt->querys->consult($up_sqr6, __METHOD__);

        return 1;
    }

    public function getPlacesArray(array $var = null)
    {
        $places = $var["places"];
        $sql = "SELECT * FROM mod_places WHERE mod_plc_id IN (" . $places . ") ORDER BY  mod_plc_id " . $orderBy . $limit;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_plc_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_plc_name"];
                $return[$i]["tags"] = $row["mod_plc_tags"];
                $return[$i]["json"] = $row["mod_plc_json"];
                $return[$i]["address"] = $row["mod_plc_address"];
                $return[$i]["city"] = $row["mod_plc_city"];
                $return[$i]["country"] = $row["mod_plc_country"];
                $return[$i]["phone"] = $row["mod_plc_phone"];
                $return[$i]["celularWhatsapp"] = $row["mod_plc_celular_whatsapp"];
                $return[$i]["info"] = $row["mod_plc_info"];
                $return[$i]["img"] = $row["mod_plc_img"];
                $return[$i]["profileIcon"] = $row["mod_plc_profile_icon"];
                $return[$i]["mainCoord"] = $row["mod_plc_main_coord"];
                $return[$i]["perimeter"] = $row["mod_plc_perimeter"];
                $return[$i]["mapIcon"] = $row["mod_plc_map_icon"];
                $return[$i]["content"] = $row["mod_plc_content"];
                //$return[$i]["startHour"] = $row["mod_plc_start_hour"];
                //$return[$i]["startEnd"] = $row["mod_plc_start_end"];
                $return[$i]["acuId"] = $row["mod_plc_acu_id"];
                $return[$i]["contentId"] = $row["mod_plc_cont_id"];
                $return[$i]["status"] = $row["mod_plc_status"];
                $return[$i]["ranking"] = $row["mod_plc_ranking"];
                $return[$i]["code"] = $row["mod_plc_code"];
                $return[$i]["mapUbication"] = $row["mod_plc_map_ubication"];
                $return[$i]["24h"] = $row["mod_plc_24h"];
                $return[$i]["entId"] = $row["mod_plc_ent_id"];
                $return[$i]["state"] = $row["mod_plc_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function addPlace(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $userId = $var["user"]["userId"];
        $inputs = $var["vars"]["inputs"];

        $inputName = $inputs["inputName"];
        $inputTag = $inputs["inputTag"];
        $inputAddress = $inputs["inputAddress"];
        $inputCity = $inputs["inputCity"];
        $inputCountry = $inputs["inputCountry"];
        $inputPhone = $inputs["inputPhone"];
        $inputCelular = $inputs["inputCelular"];
        $inputImage = $inputs["inputImage"];
        $inputInfo = $inputs["inputInfo"];
        $inputJson = $inputs["inputJson"];
        $inputContent = $inputs["inputContent"];
        $inputProfileIcon = $inputs["inputProfileIcon"];
        $inputMapIcon = $inputs["inputMapIcon"];
        $input24h = $inputs["input24h"];
        $inputMainCoord = $inputs["inputMainCoord"];
        $inputPerimeter = $inputs["inputPerimeter"];

        $insert = "mod_plc_name,mod_plc_tags,mod_plc_address,mod_plc_city,mod_plc_country,mod_plc_phone, mod_plc_celular_whatsapp,mod_plc_img,mod_plc_info,mod_plc_json,mod_plc_content,mod_plc_profile_icon, mod_plc_map_icon,mod_plc_24h,mod_plc_main_coord,mod_plc_perimeter,mod_plc_ent_id,mod_plc_user_id,mod_plc_state";
        $values = "'" . $inputName . "','" . $inputTag . "','" . $inputAddress . "','" . $inputCity . "','" . $inputCountry . "','" . $inputPhone . "','" . $inputCelular . "','" . $inputImage . "','" . $inputInfo . "','" . $inputJson . "','" . $inputContent . "','" . $inputProfileIcon . "','" . $inputMapIcon . "','" . $input24h . "','" . $inputMainCoord . "','" . $inputPerimeter . "','" . $entitieId . "','" . $userId . "','1'";
        $sql = "insert into mod_places (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        //$sql = "SELECT LAST_INSERT_ID() as id";
        $sql = "select max(mod_plc_id) as id from mod_places";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $id = $row["id"];

        $rtn["Error"] = 0;
        $rtn["status"] = 'success';
        $rtn["data"] = $id;

        return $rtn;

    }

}