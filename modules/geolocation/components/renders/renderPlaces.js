let map;
export const renderMapaCoordenadas = async (vars = []) => {
    /* var html = "";
    let map;

    html += `
		<div class="form-control form-coordPrincipal">
			<input type="hidden" id="coordenadas" />
			<div id="map"></div>
		</div>
	`;
    //console.log(lugarActual);
    let dat = [];
    dat = lugarArrayDatos(lugarActual);
    console.log("coord:" + dat['lat'] + "," + dat['lng']);

    var lat = dat['lat'] ;  
    var lng = dat['lng'] ;
    

    $("#coordPrincipal").html(html);
    $("#coordPrincipal #coordenadas").val(lat + "," + lng);

    console.log ("lat + lng",lat, lng);

    const { Map } = await google.maps.importLibrary("maps");

    var mapOptions = {
        zoom: 18,
        center: new google.maps.LatLng(lat,lng),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById('map'),
        mapOptions);

    var infoWindow = new google.maps.InfoWindow({
        content: '',
        buttons: {
            close: { visible: false }
        }
    });

    $('<div id="windowInfo"/>').addClass('centerMarker').appendTo(map.getDiv())
        .click(function () {
            if (formattedAddress) {
                infoWindow.bindTo('position', map, 'center');
                infoWindow.open(map);
            }
        });


    var geocoder = new google.maps.Geocoder(); // create a geocoder object
    var formattedAddress = null;
    function updateAddress(lat, lng) {

        geocoder.geocode({
            'latLng': new google.maps.LatLng(lat, lng)
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {           // if geocode success
                formattedAddress = results[0].formatted_address;// if address found, pass to processing function
                //$('#address').html(formattedAddress);
                $('#coordenadas').val(lat + "," + lng);
            }
            infoWindow.setContent(formattedAddress);
        });
    }
    updateAddress(lat, lng);

    function gEventCallback(e) {
        updateAddress(this.getCenter().lat(), this.getCenter().lng());
    }

    google.maps.event.addListener(map, 'drag', gEventCallback);
    google.maps.event.addListener(map, 'dragend', gEventCallback);
    google.maps.event.addListener(map, 'zoom_changed', gEventCallback); */

    const { Map } = await google.maps.importLibrary("maps");

    let lat = vars.lat ? Number(vars.lat) : -17.783709;  
    let lng = vars.lng ? Number(vars.lng) : -63.182171;

    map = new Map(document.getElementById("coordPrincipal"), {
        center: { lat, lng },
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

 

    let infoWindow = new google.maps.InfoWindow({
        content: '',
        buttons: {
            close: { visible: false }
        }
    });

    $('<div id="windowInfo"/>').addClass('centerMarker').appendTo(map.getDiv())
        .click(function () {
            if (formattedAddress) {
                infoWindow.bindTo('position', map, 'center');
                infoWindow.open(map);
            }
        });

    var geocoder = new google.maps.Geocoder(); // create a geocoder object
    var formattedAddress = null;
    function updateAddress(lat, lng) {

        geocoder.geocode({
            'latLng': new google.maps.LatLng(lat, lng)
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {           // if geocode success
                formattedAddress = results[0].formatted_address;// if address found, pass to processing function
                //$('#address').html(formattedAddress);
                $('#coordenadas').val(lat + "," + lng);
            }
            infoWindow.setContent(formattedAddress);
        });
    }
    updateAddress(lat, lng);

    function gEventCallback(e) {
        updateAddress(this.getCenter().lat(), this.getCenter().lng());
    }

    google.maps.event.addListener(map, 'drag', gEventCallback);
    google.maps.event.addListener(map, 'dragend', gEventCallback);
    google.maps.event.addListener(map, 'zoom_changed', gEventCallback); 
}

export const lugarArrayDatos = (lugarActual) => {
    var lugarArray = lugarActual.split(",");
    var lat = "";
    var lng = "";
    var dat = new Array;

    if (lugarActual != "") {
        lat = lugarArray["0"].replace(/ /g, "");
        lng = lugarArray["1"].replace(/ /g, "");
    } else {
        // aquí va input
        lat = "-17.783456";
        lng = "-63.180269";
    }

    dat["lat"] = lat;
    dat["lng"] = lng;

    return dat;
}