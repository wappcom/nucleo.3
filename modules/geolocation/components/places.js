import {
    empty,
    replacePath,
    replaceEssentials,
    replaceAll,
    loadView,
    stopLoadingBar,
    emptyReturn,
    loadingBtnIcon,
    btnLoadingRemove,
    unDisableId,
    accessToken,
    removeBtnLoading,
    alertMessageError,
    dataModule,
    alertPage,
    addHtml,
    selectOptionsCountries,
    jointActions
} from "../../components/functions.js";

import {
    editorText,
    errorInput,
    loadInitChargeMedia,
    loadItemMedia,
    loadFilesFormLoader,
    loadSingleFileFormLoader,
    loadChargeMediaInit,
    removalMediaItems,
    addValueField,
} from "../../components/forms.js";

import {
    deleteModalItem,
    removeModal,
    closeModal,
    removeModalId,
} from "../../components/modals.js";
import {
    categorysListTable,
    createTable,
    dataTable,
    removeItemTable,
    inactiveBtnStateItem,
    activeBtnStateItem,
    mountTable,
    colCheck,
    colId,
    colName,
    colTitle,
    colState,
    colActionsBase,
    deleteItemModule,
    renderEmpty,
    setupTable,
} from "../../components/tables.js";
import {
    renderCol,
    renderColCheck,
    renderColId,
    renderColTitle,
    renderRowsTable,
    renderColState,
    renderColActions,
    renderColCategorys,
} from "../../components/renders/renderTables.js";
import {
    innerForm,
    renderDeleteModal,
} from "../../components/renders/renderModals.js";

import { renderMapaCoordenadas } from "./renders/renderPlaces.js";

import {
    listCategorys,
    checkCategorys,
} from "../../websites/components/categorys.js";

import {
    renderCountrySelect
} from "../../components/renders/renderForms.js";

/* renderColTitle,
    renderColState,
    renderColActionsBase, */
let pathurl = "modules/ads/";
var module = "places";
let system = "geolocation";
let tableId = "tablePlaces";
let myPolygon = "";
let perimeterInitial = "-17.78373,-63.18249:-17.78223,-63.18485:-17.78015,-63.18296:-17.78192,-63.18046";

export const placesIndex = (vars = []) => {
    //console.log("placesIndex", vars);
    loadView(_PATH_WEB_NUCLEO + "modules/geolocation/views/places.html?" + _VS)
        .then((htmlView) => {
            //console.log('loadView', htmlView);
            let content = htmlView;
            content = content.replace(/{{_MODULE}}/g, module);
            content = content.replace(/{{_SYSTEM}}/g, system);
            content = content.replace(/{{_ICON}}/g, dataModule(module, "icon"));
            content = content.replace(/{{_NAME}}/g, dataModule(module, "name"));
            content = content.replace(
                /{{_COLOR}}/g,
                dataModule(module, "color")
            );
            content = content.replace(/{{_BTN_LABEL_ACTION}}/g, "Nuevo Lugar");

            getData({
                task: "getPlaces",
                return: "returnArray",
            })
                .then((response) => {
                    //console.log("getData getPlaces", response);
                    if (response.status == "success") {
                        let data = response.data;
                        /* let table = createTable(tableId, [colId, colName, colTitle, colState, colActionsBase]);
                let render = renderRowsTable(data, table, [renderColId, renderColTitle, renderColState, renderColActionsBase]);
                mountTable(tableId, render); */

                        if (data == 0) {
                            content = content.replace(
                                /\{{_BODY}}/g,
                                renderEmpty()
                            );
                            addHtml({
                                selector:
                                    ".bodyModule[module='" + module + "']",
                                type: "insert",
                                content,
                            }); //type: html, append, prepend, before, after
                        }

                        if (data != 0) {
                            let table = setupTable({
                                tableId,
                                cols: [
                                    {
                                        elem: "check",
                                    },
                                    {
                                        elem: "id",
                                        label: "Id",
                                    },
                                    {
                                        elem: "name",
                                        label: "Nombre",
                                    },
                                    {
                                        elem: "state",
                                        label: "Estado",
                                    },
                                    {
                                        elem: "actions",
                                        label: "Acciones",
                                        btns: ["btnEdit", "btnDelete"],
                                    },
                                ],
                                data,
                            });

                            content = content.replace(/\{{_BODY}}/g, table);

                            addHtml({
                                selector:
                                    ".bodyModule[module='" + module + "'][system='" + system + "']",
                                type: "insert",
                                content,
                            }); //type: html, append, prepend, before, after

                            dataTable({
                                elem: "#" + tableId,
                                orderCol: 0,
                            });
                        }
                    } else {
                        alertMessageError({
                            message: response.message,
                        });
                    }
                })
                .catch(console.warn());
        })
        .catch(console.warn());
};

export const newFormPlace = (vars = []) => {
    console.log("newFormPlace", vars);
    loadView(
        _PATH_WEB_NUCLEO + "modules/geolocation/views/formPlaces.html?" + _VS
    ).then((responseView) => {
        //console.log('loadView', responseView);

        let str = responseView;

        str = str.replace(/{{_PERIMETER}}/g, perimeterInitial);
        str = str.replace(/{{_COUNTRY_SELECT}}/g, renderCountrySelect({id: "inputCountry"}));

        innerForm({
            module,
            body: replaceEssentials({
                str,
                module,
                fn: "",
                title: "Nuevo Lugar",
                formId: "formNewPlace",
                btnNameAction: "Guardar",
                btnAction: "btnSavePlace",
                categorys: listCategorys(_CATEGORYS),
            }),
        });

        editorText("inputInfo");

        addHtml({
            selector: "#coordPrincipal",
            type: "insert",
            content: renderMapaCoordenadas(),
        }); //type: html, append, prepend, before, after

        initializePerimeter({
            id: "inputPerimeter",
        });
    });
};

export const getFormEditPlace = (vars = []) => {
    //console.log('getFormEditPlace', vars);
    getDataPlaces({
        task: "getPlace",
        return: "returnArray",
        id: vars.id,
    })
        .then((responsePlace) => {
            console.log("getDataPlaces", responsePlace.data);
            loadView(
                _PATH_WEB_NUCLEO +
                    "modules/geolocation/views/formPlaces.html?" +
                    _VS
            )
                .then((responseView) => {
                    //console.log('loadView', responseView);

                    let str = responseView;
                    let dataPlace = responsePlace.data;
                    let img = responsePlace.data.img;
                    let mainCoordData = responsePlace.data.mainCoord;
                    let mainCoordArray = mainCoordData.split(",");

                    innerForm({
                        module,
                        body: replaceEssentials({
                            str,
                            module,
                            fn: "",
                            title: "Editar Lugar",
                            formId: "formEditPlace",
                            btnNameAction: "Actualizar",
                            btnAction: "btnUpdatePlace",
                            item: dataPlace.id,
                            categorys: listCategorys(_CATEGORYS),
                        }),
                    });

                    addValueField({
                        id: "inputName",
                        value: dataPlace.name,
                        type: "text",
                    });
                    addValueField({
                        id: "inputId",
                        value: dataPlace.id,
                        type: "hidden",
                    });
                    addValueField({
                        id: "inputTags",
                        value: dataPlace.tags,
                        type: "text",
                    });
                    addValueField({
                        id: "inputAddress",
                        value: dataPlace.address,
                        type: "text",
                    });
                    addValueField({
                        id: "inputCity",
                        value: dataPlace.city,
                        type: "text",
                    });
                    selectOptionsCountries({
                        id: "inputCountry",
                        selected: dataPlace.country,
                    });
                    addValueField({
                        id: "inputPhone",
                        value: dataPlace.phone,
                        type: "text",
                    });
                    addValueField({
                        id: "inputCelular",
                        value: dataPlace.celularWhatsapp,
                        type: "text",
                    });
                    addValueField({
                        id: "inputInfo",
                        content: dataPlace.info,
                        type: "textarea",
                    });
                    addValueField({
                        id: "inputContent",
                        content: dataPlace.content,
                        type: "textarea",
                    });
                    addValueField({
                        id: "coordenadas",
                        content: dataPlace.mainCoord,
                        type: "hidden",
                    });
                    addValueField({
                        id: "inputPerimeter",
                        content: dataPlace.coordinates,
                        type: "hidden",
                    });
                    addValueField({
                        id: "inputImg",
                        content: img,
                        type: "hidden",
                    });
                    addValueField({
                        id: "inputNotitle",
                        value: dataPlace.fullDay,
                        type: "hidden",
                    });

                    editorText("inputInfo");

                    loadSingleFileFormLoader({
                        id: "inputImageCharge",
                        file: img,
                    });

                    addHtml({
                        selector: "#coordPrincipal",
                        type: "insert",
                        content: renderMapaCoordenadas(mainCoordData),
                    }); //type: html, append, prepend, before, after

                    initializePerimeter({
                        lat: mainCoordArray[0],
                        lng: mainCoordArray[1],
                        perimeter: dataPlace.coordinates,
                        id: "inputPerimeter",
                    });

                    checkCategorys(dataPlace.categorys);
                })
                .catch(console.warn());
        })
        .catch(console.warn());
};

export const addPlace = async ({ form }) => {
    //console.log('savePlace', vars);
    let inputName = $(`#${form} #inputName`).val();
    let inputTag = $(`#${form} #inputTags`).val();
    let inputAddress = $(`#${form} #inputAddress`).val();
    let inputCity = $(`#${form} #inputCity`).val();
    let inputCountry = $(`#${form} #inputCountry`).val();
    let inputPhone = $(`#${form} #inputPhone`).val();
    let inputCelular = $(`#${form} #inputCelular`).val();
    let inputImage = $(`#${form} #inputImage`).val();
    let inputInfo = $(`#${form} #inputInfo`).val();
    let inputJson = $(`#${form} #inputJson`).val();
    let inputContent = $(`#${form} #inputContent`).val();
    let inputProfileIcon = $(`#${form} #inputProfileIcon`).val();
    let inputMapIcon = $(`#${form} #inputMapIcon`).val();
    let input24h = $(`#${form} #input24h`).val();

    let inputMainCoord = $(`#${form} #coordenadas`).val();
    let inputPerimeter = $(`#${form} #inputPerimeter`).val();

    if (inputName == "" || inputName == null) {
        errorInput("inputName", "Debe ingresar un titulo.");
        return;
    }

    if (inputMainCoord == "" || inputMainCoord == null) {
        errorInput("coordenadas", "Debe seleccionar una coordenada.");
        return;
    }

    getData({
        task: "addPlace",
        return: "returnObject", // returnId, returnState, returnArray, returnMessage, returnObject
        inputs: {
            inputName,
            inputTag,
            inputAddress,
            inputCity,
            inputCountry,
            inputPhone,
            inputCelular,
            inputImage,
            inputInfo,
            inputJson,
            inputContent,
            inputProfileIcon,
            inputMapIcon,
            input24h,
            inputMainCoord,
            inputPerimeter,
        },
    })
        .then((response) => {
            console.log("getData addPlace", response);
            if (response.status == "success") {
            } else {
                alertMessageError({
                    message: response.message,
                });
            }
        })
        .catch(console.warn());
};

export const getData = async (vars = []) => {
    //console.log('getData', vars);
    const url = _PATH_WEB_NUCLEO + "modules/geolocation/controllers/apis/v1/places.php";
    let data = JSON.stringify({
        accessToken: accessToken(),
        vars: JSON.stringify(vars),
    });
    try {
        let res = await axios({
            async: true,
            method: "post",
            responseType: "json",
            url: url,
            headers: {},
            data: data,
        });
        //console.log(res.data);
        jointActions(res.data);
        return res.data;
    } catch (error) {
        console.log("Error: getData ", error);
    }
};

export const initializePerimeter = async (vars = []) => {
    // Map Center
    let lat = vars.lat ? Number(vars.lat) : -17.783709;
    let lng = vars.lng ? Number(vars.lng) : -63.182171;
    let perimeterData =
        vars.perimeter ||
        perimeterInitial;
    let perimeterArray = perimeterData.split(":");

    const { Map } = await google.maps.importLibrary("maps");

    let mapOptions = new Map(document.getElementById("map-canvas"), {
        center: {
            lat,
            lng,
        },
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    });

    // Polygon Coordinates
    /*  var triangleCoords = [
         new google.maps.LatLng(-17.78373, -63.18249),
         new google.maps.LatLng(-17.78223, -63.18485),
         new google.maps.LatLng(-17.78015, -63.18296),
         new google.maps.LatLng(-17.78192, -63.18046),
         new google.maps.LatLng(-17.78373, -63.18249),
     ]; */

    var triangleCoords = perimeterArray.map(function (item) {
        let itemArray = item.split(",");
        return new google.maps.LatLng(itemArray[0], itemArray[1]);
    });

    // Styling & Controls
    myPolygon = new google.maps.Polygon({
        paths: triangleCoords,
        draggable: true, // turn off if it gets annoying
        editable: true,
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#FF0000",
        fillOpacity: 0.35,
    });

    myPolygon.setMap(mapOptions);
    //google.maps.event.addListener(myPolygon, "dragend", getPolygonCoords);
    google.maps.event.addListener(
        myPolygon.getPath(),
        "insert_at",
        getPolygonCoords
    );
    //google.maps.event.addListener(myPolygon.getPath(), "remove_at", getPolygonCoords);
    google.maps.event.addListener(
        myPolygon.getPath(),
        "set_at",
        getPolygonCoords
    );
};

//Display Coordinates below map
function getPolygonCoords() {
    var len = myPolygon.getPath().getLength();
    var htmlStr = "";
    var input = "";
    for (var i = 0; i < len; i++) {
        htmlStr +=
            "new google.maps.LatLng(" +
            myPolygon.getPath().getAt(i).toUrlValue(5) +
            "), ";
        let aux = ":";
        if (i == len - 1) {
            aux = "";
        }
        input += myPolygon.getPath().getAt(i).toUrlValue(5) + aux;
        //Use this one instead if you want to get rid of the wrap > new google.maps.LatLng(),
        //htmlStr += "" + myPolygon.getPath().getAt(i).toUrlValue(5);
    }
    document.getElementById("inputPerimeter").value = input;
    console.log(input);
}

function copyToClipboard(text) {
    window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
}

document.addEventListener("DOMContentLoaded", function (e) {
    $("body").on("click", `#btnNewFormPlace`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`#btnNewFormPlace`, data);
        newFormPlace();
    });

    $("body").on("click", `.btnSavePlace`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let data = $(this).data();
        console.log(`.btnSavePlace`, data);
        let form = data.form;
        addPlace({
            form,
        });
    });

    $("body").on("click", `.btnState[data-for='${tableId}']`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let id = $(this).attr("data-id");
        let state = $(this).attr("state") == "1" ? "0" : "1";
        $(this).attr("state", state);
        getDataPlaces({
            task: "changeState",
            return: "returnState",
            id,
            state,
        }).then((response) => {
            console.log("changeState", response);
        });
    });

    $("body").on("click",`.btnDelete[data-fn='deleteTablePlaces']`,function (e) {
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            let id = $(this).attr("data-id");
            let name = $(this).attr("data-name");

            console.log(`.btnDelete`);

            addHtml({
                selector: ".bodyModule[module='" + module + "']",
                type: "prepend",
                content: renderDeleteModal({
                    name,
                    id: "modalDeleteTablePlaces",
                    attr: `data-fn="deleteTablePlaces" data-id="${id}"`,
                }),
            }); //type: html, append, prepend, before, after
        }
    );

    $("body").on("click",`.btnDeleteConfirmation[data-fn='deleteTablePlaces']`,function (e) {
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            let id = $(this).attr("data-id");
            console.log(`.btnDeleteConfirmation[data-fn='deleteTablePlaces']`, id);

            getData({
                task: "delete",
                return: "returnState",
                id,
            }).then((response) => {
                console.log("delete", response);
                if (response.status == "success") {
                    removeItemTable(id, tableId);
                    removeModalId("modalDeleteTablePlaces");
                } else {
                    alertMessageError({
                        message: response.message,
                    });
                }
            })
            .catch(console.warn());
        }
    );

    $("body").on("click", `.btnEdit[data-for='${tableId}']`, function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let id = $(this).attr("data-id");
        let form = $(this).attr("data-for");
        //console.log(`.btnEdit[data-for='${tableId}']`,);
        getFormEditPlace({
            id,
            form,
        });
    });

    $("body").on("click",`.btnUpdatePlace[data-for='${tableId}']`,
        function (e) {
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            let id = $(this).attr("data-id");
            let form = $(this).attr("data-for");
            //console.log(`.btnSave[data-for='${tableId}']`,);

            saveFormEditPlace({
                id,
                form,
            });
        }
    );
});
