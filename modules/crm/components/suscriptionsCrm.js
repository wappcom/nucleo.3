import {
    empty,
    replacePath,
    replaceEssentials,
    replaceAll,
    loadView,
    stopLoadingBar,
    emptyReturn,
    loadingBtnIcon,
    btnLoadingRemove,
    unDisableId,
    accessToken,
    removeBtnLoading,
    alertMessageError,
    dataModule,
    addHtml,
    capitalize,
    alertPage
} from "../../components/functions.js";

import * as accountsUsers from "../../accounts/components/accountsUsers.js";

let module = "suscriptionsCrm";
let system = "crm";
let pathurl = "suscriptionsCrm";

export const suscriptionsCrmIndex = (vars = []) => {
    //console.log('suscriptionsCrmIndex', vars);

    accountsUsers.accountsUsersIndex({
        module,
        system
    });

    /* loadView(_PATH_WEB_NUCLEO + "modules/crm/views/suscriptionsCrm.html?" + _VS).then((responseView) => {
        //console.log('loadView', responseView);
        let content = replaceEssentials({
            str: responseView,
            module,
            system,
            pathurl,
            fn: "",
            icon: dataModule(module, "icon"),
            color: dataModule(module, "color"),
        })

        addHtml({
            selector: `.bodyModule[module="${module}"]`,
            type: 'insert',
            content
        })
    }).catch(console.warn()); */
}