import * as customers from "../../accounts/components/customers.js";

let module = "customersCrm";
let system = "crm";

export const customersCrmIndex = (vars = []) => {
    //console.log('customersCrmIndex', vars);
    customers.customersIndex({
        module: "customers",
        system: "accounts",
        moduleReturn: module
    });
}