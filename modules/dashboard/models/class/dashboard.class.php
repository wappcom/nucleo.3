<?php
header('Content-Type: text/html; charset=utf-8');
class DASHBOARD
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function removeItemTab(array $var = null){
        //return $var;
        $entitieId = $var['entitieId'];
        $userId = $var['user']['userId'];
        $inputs = $var['vars']['inputs'];
        $modId = $inputs["modId"];
        $sysId = $inputs["sysId"];

        $sql = "DELETE FROM users_tabs WHERE  user_tab_user_id= '" . $userId . "' AND user_tab_sys_id= '" . $sysId . "' AND user_tab_ent_id= '" . $entitieId . "' ";
        $this->fmt->querys->consult($sql, __METHOD__);

        return 1;
    }

    public function setItemTab(array $var = null){
        //return $var;
        $entitieId = $var['entitieId'];
        $userId = $var['user']['userId'];
        $inputs = $var['vars']['inputs'];
        $modId = $inputs["modId"]; // mod default
        $sysId = $inputs["sysId"];

        $sql = "SELECT * FROM users_tabs WHERE  user_tab_user_id= '" . $userId . "' AND user_tab_sys_id= '" . $sysId . "' AND user_tab_ent_id= '" . $entitieId . "' ";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            $sql = "UPDATE users_tabs SET
                    user_tab_sys_active='0'
                    WHERE user_tab_user_id= '" . $userId . "' AND user_tab_ent_id= '" . $entitieId . "' ";
            $this->fmt->querys->consult($sql);

            $sql = "UPDATE users_tabs SET
                    user_tab_sys_active='1',
                    user_tab_start_mod_id='" . $modId . "'
                    WHERE user_tab_user_id= '" . $userId . "' AND user_tab_sys_id= '" . $sysId . "' AND user_tab_ent_id= '" . $entitieId . "' ";
            $this->fmt->querys->consult($sql);
            
        } else {
            $numOrder = $this->numOrderTabs($userId, $entitieId);
            $numOrder++;

            $sql = "UPDATE users_tabs SET
                    user_tab_sys_active='0'
                    WHERE user_tab_user_id= '" . $userId . "' AND user_tab_ent_id= '" . $entitieId . "' ";
            $this->fmt->querys->consult($sql);
            
            $insert = "user_tab_user_id, user_tab_sys_id, user_tab_ent_id, user_tab_sys_active, user_tab_start_mod_id, user_tab_order"; 
            $values = "'" .$userId. "', '" .$sysId. "', '" .$entitieId. "', '1', '" .$modId. "', '" .$numOrder. "'";
            $sql= "insert into users_tabs (".$insert.") values (".$values.")";
            $this->fmt->querys->consult($sql,__METHOD__);

            return 1;
        }
        $this->fmt->querys->leave($rs);
    }

    public function setActiveTab(array $var = null){
        //return $var;
        $entitieId = $var['entitieId'];
        $userId = $var['user']['userId'];
        $inputs = $var['vars']['inputs'];
        $modId = $inputs["modId"];
        $sysId = $inputs["sysId"];

        $sql = "UPDATE users_tabs SET
                    user_tab_sys_active='0'
                    WHERE user_tab_user_id= '" . $userId . "' AND user_tab_ent_id= '" . $entitieId . "' ";
        $this->fmt->querys->consult($sql);

        $sql = "UPDATE users_tabs SET
            user_tab_sys_active='1',
            user_tab_start_mod_id='" . $modId. "'
            WHERE user_tab_user_id= '" . $userId . "' AND user_tab_sys_id= '" . $sysId . "' AND user_tab_ent_id= '" . $entitieId . "' ";
        $this->fmt->querys->consult($sql);

       return 1;
    }

    public function setModuleUserTab(array $var = null){
        //return $var;
        $entitieId = $var['entitieId'];
        $userId = $var['user']['userId'];
        $inputs = $var['vars']['inputs'];
        $modId = $inputs["modId"];
        $sysId = $inputs["sysId"];

        $sql = "UPDATE users_tabs SET
            user_tab_start_mod_id='" . $modId. "'
            WHERE user_tab_user_id= '" . $userId . "' AND user_tab_sys_id= '" . $sysId . "' AND user_tab_ent_id= '" . $entitieId . "' ";
        $this->fmt->querys->consult($sql);

       return 1;
    }

    public function numOrderTabs(int $userId = 0 , int $entitieId = 0){
        $sql = "SELECT * FROM users_tabs WHERE  user_tab_user_id= '" . $userId . "' AND user_tab_ent_id= '" . $entitieId . "' ";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        return $num;
        $this->fmt->querys->leave($rs);
        
    }
}