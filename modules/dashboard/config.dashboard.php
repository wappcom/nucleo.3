<?php
header('Content-Type: text/html; charset=utf8');
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_WARNING);
ini_set('display_errors', 'On');

define("_PATH_DASHBOARD_FAVICON", $fmt->options->getValue("dashboard_favicon"));
define("_PATH_DASHBOARD_BRAND", $fmt->options->getValue("dashboard_brand"));
define("_PATH_DASHBOARD", "modules/dashboard/");
define("_NAME_APP", _V);
