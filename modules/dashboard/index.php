<?php
header('Content-Type: text/html; charset=utf8');
header('Access-Control-Allow-Origin: *');
//aquí agregamos solo los metodos que necesitemos
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

define("_VS", $fmt->options->version());

require_once("config.dashboard.php");

//setcookie('same - site', 'strict');
header("Set-Cookie: key=value; path=/; HttpOnly; SameSite=Strict");

$index = file_get_contents(_PATH_NUCLEO . "modules/dashboard/views/index.html");
$html = '';
$html = str_replace("{{title}}", _NAME_APP, $index);
$html = str_replace("{{urlFavicon}}", _PATH_WEB . _PATH_DASHBOARD_FAVICON, $html);
$html = str_replace("{{_PATH_MODULE}}", _PATH_WEB . _PATH_DASHBOARD, $html);
$html = str_replace("{{_ICONS_LINK}}", _ICONS_LINK, $html);

$stateConnect = _STATE_CONNECT == 0 ? 0 : 1;

// Se estan cargando todos los scritps y style de modulos revisar por ROLES

$scriptConfig .= '    <script type="text/javascript" language="javascript"> 
        const _PATH_WEB="' . _PATH_WEB . '"; 
        const _PATH_WEB_NUCLEO="' . _PATH_WEB_NUCLEO . '"; 
        const _PATH_FILES="' . _PATH_FILES . '"; 
        const _PATH_DASHBOARD_BRAND="' . _PATH_DASHBOARD_BRAND . '"; 
        const _PATH_LOGOUT="' . _PATH_LOGOUT . '"; 
        const _PATH_DASHBOARD_FAVICON="' . _PATH_DASHBOARD_FAVICON . '"; 
        const _PATH_DASHBOARD="' . _PATH_DASHBOARD . '"; 
        const _PATH_PAGE="' . _PATH_PAGE . '"; 
        const _STATE_CONNECT = "' . $stateConnect . '";
        const _ICONS_LINK_FILES="' . _ICONS_LINK_FILES . '"; 
        const _PATH_ICONS_PUBS="' . $fmt->options->getValue("path_icons_pub") . '"; 
        const _BEARER_TOKEN="' . $fmt->options->getValue("bearer_token") . '";     
        const _CLIENT_ID="' . $fmt->options->getValue("client_id") . '"; 
        const _CLIENT_SECRET="' . $fmt->options->getValue("client_secret") . '"; 
        const _IMG_NOIMAGE="' . _PATH_WEB_NUCLEO . 'modules/assets/img/no-image.jpg"; 
        const _COIN_DEFAULT="' . $fmt->options->getValue("coin", "yes") . '"; 
        const _LOCALE="' . $fmt->options->getValue("locale") . '"; 
        const _TIMEZONE="' . $fmt->options->getValue("timezone") . '"; 
        const _KEY_GOOGLE = "' . _KEY_GOOGLE . '";
        let _ID_ENTITIE = 1;
        let _CATEGORYS; 
        let _VS = "v' . _VS . '";
        let userData = "";
        let displayLoadingBar = "";
        
        localStorage.setItem("_VS", ' . _VS . ');

        jQuery.datetimepicker.setLocale("es");
    </script>';
$scriptCss = "\n" . "    <!-- Modules Site Js/Css -->" . "\n";
$scriptCss .= $fmt->modules->loadHeaderIndex();

// $pathNav = _PATH_NUCLEO . "modules/components/nav.js";
// $navFile  = file_get_contents($pathNav);
// $navReturn =  str_replace("//{{_JS}}","//hola", $navFile);
// $pattern = "/\/[*]init[*]\/(.*)\/[*]end[*]\//";
// $navReturn =  preg_replace($pattern,"/*init*/hello7/*end*/", $navFile);

//chmod($pathNav, 0775);
//$dat = file_put_contents($pathNav, $navReturn); 
//$scriptConfig .= '<!-- '.$dat.' -->' ;
//$scriptConfig .= '<!-- '.$navReturn.'-->' ;

$html = str_replace("{{_CONFIGS}}", $scriptConfig, $html);
$html = str_replace("{{_KEY_GOOGLE}}", _KEY_GOOGLE, $html);
$html = str_replace("{{_SCRIPTS_CSS}}", $scriptCss, $html);
$html = str_replace("{{_PATH_DASHBOARD}}", _PATH_DASHBOARD, $html);
$html = str_replace("{{_VS}}", _VS, $html);
$html = $fmt->setUrlNucleo($html);

/* $fmt->tests->deleteFileTest("12");
$fmt->tests->deleteFileTest("13");  */

echo $html;