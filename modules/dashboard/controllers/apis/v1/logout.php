<?php
//require_once('../../../../config.php');
require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $_POST["page"] . "config.php");
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

header("Content-Type: application/json");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

switch ($_SERVER["REQUEST_METHOD"]) {
    case 'POST':

        $access_token = $fmt->auth->getInput('access_token');
        $refresh_token = $fmt->auth->getInput('refresh_token');
        $entitieId = $fmt->auth->getInput('idEntitie');

        $userId = $fmt->users->validateUserToken(array("access_token" => $access_token, "refresh_token" => $refresh_token, "idEntitie" => $entitieId));

        if ($userId) {
            //echo "'usario validado";
            $return = [];
            $return["user"] = $fmt->users->userDataAuth(array("access_token" => $access_token, "idEntitie" => $entitieId));
            $userId = $return["user"]["userId"];
            //echo json_encode($dateUser );
            if ($fmt->users->logout($userId)){
                $aux["Error"] = 0;
                echo json_encode($aux);
            }else{
                echo $fmt->errors->errorJson([
                    "description" => "Access Auth. Error user",
                    "code" => "",
                    "lang" => "es"
                ]);
            }
            
        } else {
            echo $fmt->errors->errorJson([
                "description" => "Access Auth. invalid user",
                "code" => "",
                "lang" => "es"
            ]);
        }
        break;

    default:
        echo $fmt->errors->errorJson([
            "description" => "Access Auth. Metod request.",
            "code" => "",
            "lang" => "es"
        ]);
        break;
}
