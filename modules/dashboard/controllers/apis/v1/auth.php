<?php
//echo json_encode($_POST["page"]); exit(0);
require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $_POST["page"] . "config.php");

require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

header("Content-Type: application/json");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

switch ($_SERVER["REQUEST_METHOD"]) {
    case 'POST':

        $grant_type = $fmt->auth->getInput('grant_type');
        $bearer_token = $fmt->auth->getInput('authentication');
        $email = $fmt->auth->getInput('email');

        $password = $fmt->auth->getInput('password');
        $clientId = $fmt->auth->getInput('client_id');
        $clientSecret = $fmt->auth->getInput('client_secret');
        $api = $fmt->auth->getApi();

        if (empty($email)) {
            echo $fmt->errors->errorJson([
                "description" => "Access Auth. Email empty field.",
                "code" => "",
                "lang" => "es"
            ]);
            exit(0);
        }
        if (empty($password)) {
            echo $fmt->errors->errorJson([
                "description" => "Access Auth. Password empty field.",
                "code" => "",
                "lang" => "es"
            ]);
            exit(0);
        }

        if ($grant_type == "token") {
            if ($bearer_token === "Bearer " . $fmt->auth->bearerToken()) {

                if (($api['client_id'] === $clientId) && ($api['client_secret'] === $clientSecret)) {

                    $userArray = $fmt->users->userData($email, $password);
                    //echo json_encode($userArray.":".$email.":". $password); exit(0);

                    if ($userArray != 0) {

                        $userId = $userArray["user_id"];

                        $accessToken = base64_encode($clientId . "." . $userId . "." . $userArray["user_rol"] . "." . uniqid(microtime(), true));
                        $refreshToken = md5(uniqid(microtime(), true));

                        /*if ($fmt->users->getTokenUser($userId, "access_token")) {
                            echo $fmt->errors->errorJson([
                                "description" => "Access Auth. Exist token.",
                                "code" => "",
                                "lang" => "es"
                            ]);

                            exit(0);
                        } else {*/


                        // echo json_encode([
                        //     "description"=>"Access Auth. crear token."
                        // ]);
                        // exit(0);
                        $userId = $userArray["user_id"];

                        //echo json_encode($userId.":".$accessToken.":".$refreshToken.":".$idEntitie); exit(0);

                        $returnCreateUserToken = $fmt->users->createUserToken($userId, "access_token", $accessToken);

                        $returnCreateRefreshToken = $fmt->users->createUserToken($userId, "refresh_token", $refreshToken);

                        $usersEntities = $fmt->users->idUserToEntities($userId);

                        //echo json_encode($usersEntities); exit(0);

                        if ($usersEntities == 0) {
                            $idEntitie = 0;
                        } else {
                            $idEntitie =  $usersEntities[0]["id"];
                        }

                        echo json_encode([
                            "Error" => 0,
                            "access_token" => $accessToken,
                            "token_type" => "Bearer",
                            "expires_in" => 3600,
                            "idEntitie" => $idEntitie,
                            "refresh_token" => $refreshToken
                        ]);


                        exit(0);
                        // }
                    } else {
                        echo $fmt->errors->errorJson([
                            "description" => "Access Auth. User.",
                            "message" => "Error de Accesos de Usuario, validar los datos",
                            "code" => "",
                            "lang" => "es"
                        ]);
                        exit(0);
                    }
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Access Auth. Client_id/client_secret.",
                        "code" => "",
                        "lang" => "es"
                    ]);
                    exit(0);
                }
            } else {
                echo $fmt->errors->errorJson([
                    "description" => "Access Auth. Bearer Token.",
                    "code" => "",
                    "lang" => "es"
                ]);
                exit(0);
            }
        } else {
            echo $fmt->errors->errorJson([
                "description" => "Access Auth. Grant_type.",
                "code" => "",
                "lang" => "es"
            ]);
            exit(0);
        }

        break;

    default:
        echo $fmt->errors->errorJson([
            "description" => "Access Auth. Metod request.",
            "code" => "",
            "lang" => "es"
        ]);
        break;
}