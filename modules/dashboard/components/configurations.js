import {
    replacePath,
    replaceAll,
    loadingBar,
    loadView,
    loadLoading,
    addHtml,
    loadingBarIdReturn
} from "../../components/functions.js"

import * as users from "../../users/components/users.js";
import * as roles from "../../users/components/roles.js";
import * as systems from "../../users/components/systems.js";
//import * as groups from "../../users/components/groups.js";

export const loadConfigAdmin = (vars = []) => {
    //console.log('loadConfigAdmin', vars);
    loadView(_PATH_WEB_NUCLEO + "modules/dashboard/views/configWindow.html?" + _VS).then((response) => {
        ////console.log('loadView', response);

        let content = response;

        addHtml({
            selector: "#" + vars.id + " .modalInner",
            type: 'insert',
            content
        }) //type: html, append, prepend, before, after

        getPageConfig({
             id: "contentConfigAdmin",
             path: vars.path
        });
    }).catch(console.warn());
}

export const getPageConfig = (vars = []) => {
    //console.log('getPageConfig', vars);
    $(`#modalConfigAdmin .btnConfAction`).removeClass('active');
    $(`#modalConfigAdmin .btnConfAction[content='${vars.path}']`).addClass('active');

    if (vars.path != "config") {
        eval(`${vars.path}.${vars.path}Index(` + JSON.stringify(vars) + `)`);
    } else {
        configIndex(vars);
    }

}


document.addEventListener("DOMContentLoaded", function () {
    $("body").on("click", ".btnConfAction", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        const id = $(this).attr('for');
        const content = $(this).attr('content');
        //console.log('.btnConfAction', id, content);
        getPageConfig({
            id,
            path: content
        });
    })
});