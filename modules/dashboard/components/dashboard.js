import {
    loadMenu,
    structureMenus
} from "../../components/nav.js"
import {
    alertMessageError,
    getScript
} from "../../components/functions.js"


import {
    verifySession
} from "./login.js"


let porc = 0

var img1 = new Image()
img1.src = _PATH_WEB_NUCLEO + "modules/assets/img/bend.svg"



document.addEventListener("DOMContentLoaded", function () {
    //console.log("dashboard")
    getScript(
        _PATH_WEB_NUCLEO + "modules/dashboard/components/login.js",
        "head"
    )
})

export function loadDashboard() {
    loadMenu().then((menusJson) => {
        console.log(menusJson);
        if (menusJson != 0) {

            if (menusJson.message != "Error") {
                //console.log("loadDashboard() loadMenu() structureMenus()", menusJson)
                structureMenus(menusJson); // of loadMenu
            } else {
                //console.log('clear');
                alertMessageError(menusJson.message);
                localStorage.clear()
                localStorage.setItem("bearerToken", _BEARER_TOKEN)
                localStorage.setItem("clientId", _CLIENT_ID)
                localStorage.setItem("clientSecret", _CLIENT_SECRET)
                verifySession();
            }
        } else {
            console.log("Error Menu Return.")
        }
    })
}

export const loadConfigAdmin = (vars = []) => {
    console.log('loadConfigAdmin', vars);

}

//files
export function returnBoxUpFileCircle(varId) {
    //console.log("rb:" + varId)
    $(".boxIcon[for='" + varId + "']").html(
        '<i class="icon icon-photo-camera"></i><span>Cargar Foto max. 500x500px</span>'
    )
}

export function returnBoxUpFile(varId) {
    //console.log("rb:" + varId)
    $(".boxIcon[for='" + varId + "']")
        .parent()
        .css("padding", "30px 10px")
    var text = $("#" + varId + "[returnaction='returnBoxUpFile']").attr(
        "textreturnaction"
    )
    $(".boxIcon[for='" + varId + "']").html(
        '<i class="icon icon-upfile"></i><span>' + text + "</span>"
    )
}

export function boxRol(rol) {
    return '<div class="boxRol">' + rol + "</div>"
}