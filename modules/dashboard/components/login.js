import {
    loadDashboard
} from "./dashboard.js"
import {
    alertMessageError,
    replacePath,
    changeBtn,
    convertBtn
} from "../../components/functions.js"
import {
    translate
} from "../../components/lang.js"


document.addEventListener("DOMContentLoaded", function () {})
verifySession()

$("body").on("click", "#btnSingIn", function () {
    var email = $("#inputEmail").val()
    var password = $("#inputPassword").val()
    convertBtn({
        selector: "#btnSingIn",
        icon: "icon icon-loading icon-refresh",
        text: 'Accediendo',
        cls: 'btn btnLg btnSignIn btnPrimary disabled btnIconLeft'
    })
    autentication({
        email: email,
        password: password
    })
})

document.addEventListener("keydown", function(event) {
    if (event.key === "Enter") {
      //console.log("Se presionó la tecla Enter");
      var email = $("#inputEmail").val();
      var password = $("#inputPassword").val();

      convertBtn({
          selector: "#btnSingIn",
          icon: "icon icon-loading icon-refresh",
          text: 'Accediendo',
          cls: 'btn btnLg btnSignIn btnPrimary disabled btnIconLeft'
      });
      
      autentication({
          email: email,
          password: password
      })

    }
});
  



export function verifySession() {
    //console.log("verifySession")
    var access_token = localStorage.getItem("access_token")
    //console.log("access_token: " + access_token)
    if (
        access_token != null &&
        access_token != undefined &&
        access_token != ""
    ) {
        //console.log("tiene session");
        loadDashboard()
    } else {
        //console.log("loginRoot")
        loadLoginRoot().then((data) => {
            //console.log(data);

            $("#root").html(data)
            translate()
        })
    }
}

async function loadLoginRoot() {
    var url = _PATH_WEB_NUCLEO + _PATH_DASHBOARD + "views/login.html"
    try {
        let contentAwait = await fetch(url)
        let str = await contentAwait.text()
        //console.log("loadLoginRoot:" + str);
        str = replacePath(str)
        str = str.replace("{{_PATH_BRAND}}", _PATH_WEB + _PATH_DASHBOARD_BRAND)
        str = str.replace("{{_VS}}", _VS)
        // $("#root").html(str);
        return str
    } catch (err) {
        console.log("loadLoginRoot Error:" + err)
    }
}

function autentication(vars) {
    //console.log(_PATH_WEB_NUCLEO + 'controllers/apis/v1/login/auth.php')
    let url =
        _PATH_WEB_NUCLEO + "modules/dashboard/controllers/apis/v1/auth.php"
    let token = localStorage.getItem("bearerToken")

    // let h = new Headers();
    // h.append('Authentication', `Bearer ${token}`);

    const dataForm = new FormData()
    dataForm.append("page", _PATH_PAGE)
    dataForm.append("authentication", `Bearer ${token}`)
    dataForm.append("grant_type", "token")
    dataForm.append("email", vars.email)
    dataForm.append("password", vars.password)
    dataForm.append("client_id", localStorage.getItem("clientId"))
    dataForm.append("client_secret", localStorage.getItem("clientSecret"))

    let req = new Request(url, {
        method: "POST",
        mode: "cors",
        body: dataForm,
    })
    fetch(req)
        .then((resp) => resp.json())
        .then((data) => {
            //console.log("autentication:" + data);

            if (data["Error"] == "0") {
                // console.log("autentication: e" + data);
                localStorage.setItem("access_token", data.access_token)
                localStorage.setItem("token_type", data.token_type)
                localStorage.setItem("refresh_token", data.refresh_token)
                localStorage.setItem("idEntitie", data.idEntitie)

                $(".pubLogin .pub-inner").addClass("animated fadeOut")
                setTimeout(function () {
                    loadDashboard()
                }, 1000)
            } else {
                //alertMessageError(data["message"])
                //console.log("Error" + data);

                $("#messageLogin").html('<div class="message messageDanger animated fadeUp">' + data["message"] + "</div>")
                //console.log(data);

                setTimeout(() => {
                    convertBtn({
                        selector: "#btnSingIn",
                        text: 'Ingresar',
                        icon: 'icon icon-arrow-line-right',
                        cls: 'btn btnPrimary btnLg btnSignIn btnIconRight'
                    });
                    $("#messageLogin").html('');
                }, 1500);
                //console.log(data["descripcion"]);
            }
        })
        .catch((err) => {
            console.error("Error Request ", err)
        })
}