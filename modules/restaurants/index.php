<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $_POST["page"] . "config.php");

require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

header('Content-Type: text/html; charset=utf8');
define("_VS", $fmt->options->version());

$html  = '';
$access_token = $fmt->auth->getInput('access_token');
$refresh_token = $fmt->auth->getInput('refresh_token');
$entitieId = $fmt->auth->getInput('idEntitie');
$rolId = $fmt->auth->getInput('idRol');
$pathSys = $fmt->auth->getInput('path');

$userId = $fmt->users->validateUserToken(array("access_token" => $access_token, "refresh_token" => $refresh_token, "idEntitie" => $entitieId));

if ($userId) {

    require_once(_PATH_NUCLEO . "modules/accounts/models/class/class.customers.php");
    $customers = new CUSTOMERS($fmt);
    

    $arraySys = $fmt->systems->pathToData($pathSys);
    $sysId = $arraySys["sys_id"];
    $name = $arraySys["sys_name"];
    $icon = $arraySys["sys_icon"];
    $color = $arraySys["sys_color"];
    $pathUrlSystem = $arraySys["sys_pathurl"];
    $pathSystem = $arraySys["sys_path"];
    //$str = "var _CATEGORYS=" . json_encode($fmt->categorys->nodes(0, $entitieId)) . "\n";
    $modArray = $fmt->modules->modulesRols($rolId, $sysId, $entitieId);
    $module =   $modArray[0]["mod_pathurl"];
    $modIdActive = $_POST["modIdActive"];

    //var_dump($modArray);
    
    if (!empty($modIdActive)) {
        $modActiveArray = $fmt->modules->dataId($modIdActive);
        $modIdActive =$modActiveArray["mod_pathurl"];
        $module = $modActiveArray["mod_pathurl"];
    } else {
        $modIdActive = $modArray[0]["mod_pathurl"];
    }
 
    $modArrayActive = $fmt->modules->dataId($_POST["modIdActive"]);
    $sibedarMod = $fmt->modules->sidebarMenuModulesHtml(array("sysId" => $sysId, "modules" => $modArray, "idActive" => $_POST["modIdActive"]));

    $index  = file_get_contents(_PATH_NUCLEO . $pathSystem . "views/index.html");

    if ($rolId==1 || $rolId==2 || $rolId==3 || $rolId==5) {
        $menuTopRight = '<div class="left"></div><div class="right">'.file_get_contents(_PATH_NUCLEO . $pathSystem . "views/menu-top-right.html")."</div>";
    
    } else {
        $menuTopRight =  "";
    }

    $html =  str_replace("{{_VS}}", _VS, $index);
    $html =  str_replace("{{_NAME_TOP}}", $name, $html);
    $html =  str_replace("{{_STATE}}", 1, $html);
    $html =  str_replace("{{_MENU_TOP_MEDIUM}}", "", $html);
    $html =  str_replace("{{_MENU_TOP_RIGHT}}", $menuTopRight, $html);
    // $html =  str_replace("{{_PATH_MODULE}}",_PATH_WEB.$pathModule,$html);
    $html =  str_replace("{{_MODULE}}", $module, $html);
    $html =  str_replace("{{_NAME}}", $name, $html);
    $html =  str_replace("{{_ICON}}", $icon, $html);
    $html =  str_replace("{{_COLOR}}", $color, $html);

    // $scripts = '<script type="text/javascript" language="javascript" src="modules/counseling/components/history.js"></script>';
    //$scripts = '<script type="text/javascript" language="javascript" src="' . _PATH_WEB_NUCLEO . 'modules/inventory/components/renderInventory.js"></script><script type="text/javascript" language="javascript" src="'._PATH_WEB_NUCLEO. 'modules/inventory/components/renderProducts.js"></script>';
    $scripts = '';
    $html =  str_replace("_MODULE_SCRIPT", $str, $html);
    $html =  str_replace("{{_SCRIPTS}}", $scripts, $html);
    $html =  str_replace("{{_SIDEBAR_MODULE}}", $sibedarMod, $html);
    $html =  str_replace("{{_SYSTEM}}", $pathUrlSystem, $html);

    echo $html;
} else {
    echo $fmt->errors->errorJson([
        "description" => "Access Auth. invalid user",
        "code" => "",
        "lang" => "es"
    ]);
}