

export const rrhhIndex = (vars = []) => {
    //console.log('rrhhIndex', vars);
    $(".bodyModule[module='rrhh']").html("Dashboard RRHH")
}

function rolUser(vars) {
    var dataUser = JSON.parse(userData);
    // console.log("kardexIndex" + JSON.parse(userData))
    let menuTopR = boxRol(dataUser.rolName) + '<a class="icons-action btnConfig btnMenuTop" module="' +
        vars.module + '"><i class="icon icon-conf"></i></a>'
    $(".menuTopModule[module='" + vars.module + "'] .menuTopRight").html(menuTopR)
}

function listRoles(vars) {
    var str = ''
    for (let i = 0; i < vars.length; i++) {
        const id = vars[i].rol_id;
        const name = vars[i].rol_name;
        // const url = vars[i].rol_redirection_url;

        str += `<div class="checkbox checkboxControl">
                    <input name="inputRol" id="inputRol${i}" type="radio" value="${id}">
					<span>${name}</span>
    	        </div>`;
    }
    // console.log(str)
    return str
}

function listGroups(vars) {
    var str = ''
    for (let i = 0; i < vars.length; i++) {
        const id = vars[i].group_id;
        const name = vars[i].group_name;
        // const url = vars[i].rol_redirection_url;

        str += `<div class="checkbox checkboxControl">
                    <span class="box"><input name="inputCheckGroup[]" id="inputGroup${id}" type="checkbox" value="${id}"></span>
                    <span class="name">${name}</span>
    	        </div>`;
    }
    // console.log(str)
    return str
}

function listJobTitles(vars) {
    var str = '';
    for (let i = 0; i < vars.length; i++) {
        const id = vars[i].mod_jbt_id
        const name = vars[i].mod_jbt_title
        const level = vars[i].level
        const children = vars[i].children
        str += `<div class="checkbox level level${level}">
                    <span class="box"><input name="inputTreeJobTitles[]" id="inputTree${id}" type="checkbox" value="${id}"></span>
					<span class="name">${name}</span>
    	        </div>`;
        if (children.length != 0) {
            //console.log("tiene hijos" + children.length)
            str += listJobTitlesChildren(children)
        }
        //console.log(str)
    }
    return str;
}

function listJobTitlesChildren(vars) {
    // console.log(vars)
    // console.log(vars.length)
    var str = '';
    for (let i = 0; i < vars.length; i++) {
        var levelMargin = ''
        const typeLine = ''
        const id = vars[i].mod_jbt_id
        const name = vars[i].mod_jbt_title
        const level = vars[i].level

        const children = vars[i].children

        var numLast = vars.length - 1;

        if (i == numLast) {
            lastItem = 'level-last'
        } else {
            lastItem = ''
        }

        var levelStatus = '';
        if (children.length >= 1) {
            levelStatus = 'childrenActive'
        } else {
            levelStatus = ''
        }
        if (children.length == 1) {
            levelStatus = 'childActive'
        }

        str += `<div class="checkbox level level${level}   ${lastItem}" id="item${id}" style ="margin-left:24px" >
                    <span class="box ${levelStatus}"><input name="inputTreeJobTitles[]" id="inputTree${id}" type="checkbox" value="${id}"></span>
                    <span class="name">${name}</span>`

        if (children != 0) {
            if (children.length != 0) {
                str += `<div class="children  ${levelStatus}" for="${id}" >`
                str += listJobTitlesChildren(children)
                str += '</div>'
            }
        }

        str += `</div>`;

    }
    return str;
}

function listDepartaments(vars) {
    var str = '';
    for (let i = 0; i < vars.length; i++) {
        const id = vars[i].mod_dep_id
        const name = vars[i].mod_dep_name
        const level = vars[i].level
        const children = vars[i].children
        str += `<div class="checkbox level level${level}">
                    <span class="box"><input name="inputTreeDepartaments[]" id="inputDepTree${id}" type="checkbox" value="${id}"></span>
					<span class="name">${name}</span>
    	        </div>`;
        if (children.length != 0) {
            //console.log("tiene hijos" + children.length)
            str += listDepartamentsChildren(children)
        }
        //console.log(str)
    }
    return str;
}

function listDepartamentsChildren(vars) {
    //console.log(vars)
    //console.log(vars.length)
    var str = '';
    for (let i = 0; i < vars.length; i++) {
        var levelMargin = ''
        const typeLine = ''
        const id = vars[i].mod_dep_id
        const name = vars[i].mod_dep_name
        const level = vars[i].level
        const children = vars[i].children

        var numLast = vars.length - 1;

        if (i == numLast) {
            lastItem = 'level-last'
        } else {
            lastItem = ''
        }

        var levelStatus = '';
        if (children.length >= 1) {
            levelStatus = 'childrenActive'
        } else {
            levelStatus = ''
        }
        if (children.length == 1) {
            levelStatus = 'childActive'
        }

        str += `<div class="checkbox level level${level}   ${lastItem}" id="item${id}" style ="margin-left:24px" >
                    <span class="box ${levelStatus}"><input name="inputTreeDepartaments[]" id="inputDepTree${id}" type="checkbox" value="${id}"></span>
                    <span class="name">${name}</span>`

        if (children != 0) {
            if (children.length != 0) {
                str += `<div class="children  ${levelStatus}" for="${id}" >`
                str += listDepartamentsChildren(children)
                str += '</div>'
            }
        }

        str += `</div>`;

    }
    return str;
}

document.addEventListener("DOMContentLoaded", function () {

    $("body").on("click", ".btnAddGroup", function (e) {
        e.preventDefault();
        e.stopPropagation();
        console.log("hola");

    });

})