var numModKardex = 0;
export function kardexIndex() {
    let module = "kardex"
    clearInterval(displayLoadingBar)
    rolUser({
        module: module
    })
    loadSubMenu(module)
    $('.bodyModule[module="' + module + '"]').html("Dashboard Kardex")
}

export const loadSubMenu = (module = "") => {
    console.log('loadSubMenu', module);
    let btns = $('.subModule[module="' + module + '"]').html();
    $('.menuTopModule[module="' + module + '"] .menuTopMedium').html('<ul>' + btns + '</ul>');
    resizeMenuTop()
}

//Discarge 
function dischargeIndex() {
    let module = "discharge"
    let system = "rrhh"
    let urlMod = "modules/rrhh/"
    loadView(_PATH_WEB + urlMod + "views/discharge.html?" + _VS).then((htmlView) => {
        loadDischarge(module, system).then((dataModule) => {
            clearInterval(displayLoadingBar)
            let str = replacePath(htmlView)
            str = str.replace("{{_MODULE}}", module)
            str = str.replace("{{_SYSTEM}}", system)
            $(".bodyModule[module='" + module + "']").html(str)
            $(".bodyModule[module='" + module + "'] .tbody").html(dataModule)
            dataTable({
                elem: "#tableDischarge",
                orderCol: "4"
            })
        })
    })
}

async function loadDischarge(module, system) {
    let url = _PATH_WEB + "controllers/api/rrhh/discharge.php";
    let access_token = localStorage.getItem("access_token")
    let refresh_token = localStorage.getItem("refresh_token")
    let idEntitie = localStorage.getItem("idEntitie")

    const dataForm = new FormData()
    dataForm.append("access_token", access_token)
    dataForm.append("refresh_token", refresh_token)
    dataForm.append("idEntitie", idEntitie)
    dataForm.append("action", "loadDischarge")

    let req = new Request(url, {
        method: "POST",
        mode: "cors",
        body: dataForm,
    })

    try {
        let contentAwait = await fetch(req)
        let jsonData = await contentAwait.json()
        //console.log(jsonData)
        //console.log(jsonData.length)
        let str = ''
        let tbody = ''

        let tDate = todaysDate()


        for (let i = 0; i < jsonData.length; i++) {
            const elem = jsonData[i]
            const titleJobsArray = elem.mod_kdx_jobtitle;
            let titleJobs = '';
            for (let j = 0; j < titleJobsArray.length; j++) {
                const elemTitleJobs = titleJobsArray[j]
                titleJobs += '<a class="btnTitleJob" item="' + elemTitleJobs.mod_kdx_jbt_jbt_id + '"> ' + elemTitleJobs.mod_jbt_title + '</a> '
            }
            tbody += '<tr>'
            tbody += '    <td>' + elem.mod_kdx_id + '</td>'
            tbody += '    <td>' + elem.mod_kdx_name + '</td>'
            tbody += '    <td>' + elem.mod_kdx_fathers_lastname + ' ' + elem.mod_kdx_mothers_lastname + '</td>'
            tbody += '    <td>' + titleJobs + '</td>'
            tbody += '    <td>' + elem.mod_kdx_entry_date + '</td>'
            tbody += '    <td> ' + btnFn({
                type: 'btnEdit',
                module: module,
                system: system
            }) + '</td>'
            tbody += '</tr>'
        }

        str = createTable({
            id: "tableDischarge",
            thead: 'id:col-id,Nombre,Apellidos,Cargo,Fecha de Ingreso, Acciones',
            body: tbody
        })
        return str
    } catch (err) {
        console.log("loadDischarge Error:" + err)
    }
}

function dischargeFormNewIndex() {
    let module = "discharge"
    let system = "rrhh"
    let action = "formNew"
    let urlMod = "modules/rrhh/"
    numModKardex++
    loadView(_PATH_WEB + urlMod + "views/dischargeNewForm.html?" + _VS + numModKardex).then((htmlView) => {
        clearInterval(displayLoadingBar)
        let strPag = replacePath(htmlView)
        let str = replaceAll(strPag, "{{_MODULE}}", module);
        str = replaceAll(str, "{{_ROLES}}", listRoles(_ROLES));
        str = replaceAll(str, "{{_SYSTEM}}", system);
        str = replaceAll(str, "{{_ACTION}}", action);
        str = replaceAll(str, "{{_JOBSTITLES}}", listJobTitles(_JOBSTITLES));
        str = replaceAll(str, "{{_DEPARTAMENTS}}", listDepartaments(_DEPARTAMENTS));
        str = replaceAll(str, "{{_GROUPS}}", listGroups(_GROUPS));

        $('.bodyModule[module="' + module + '"] .innerForm').html(str)
        resizeSteps()

        jQuery.datetimepicker.setLocale('es');

        $('#inputExpCI').datetimepicker({
            timepicker: false,
            datepicker: true,
            format: 'Y-m-d',
            lang: 'es'
        });

        $('#inputExpLic').datetimepicker({
            timepicker: false,
            datepicker: true,
            format: 'Y-m-d',
            lang: 'es'
        });
        let numHijo = 0;
        $("body").on("click", ".btnAgregarHijos", function (e) {
            numHijo++
            e.preventDefault();
            $(".formHijos").append(`
                <div class="formGroup formControlLandscape" id="inputGroup${numHijo}">
                    <div class="formControl form40w formControlDisplay">
                        <label for="inputNameKid${numHijo}" lang="es">Nombre Hija(o)</label>
                        <input class="formInput" name="inputNameKid[]" id="inputNameKid${numHijo}" type="text">
                    </div>
                    <div class="formControl form40w formControlDisplay">
                        <label for="inputNameKidA${numHijo}" lang="es">Apellidos Hija(o)</label>
                        <input class="formInput" name="inputNameKidA[]" id="inputNameKidA${numHijo}" type="text">
                    </div>
                    <div class="formControl form10w formControlDisplay">
                        <a class="btnCerrarInputGroup" item="${numHijo}" ><i class="icon icon-close"></i></a>
                    </div>
                </div>
            `)
        });
    })
}