<?php
header('Content-Type: text/html; charset=utf-8');
class KARDEX
{

	var $fmt;
	var $pla;
	var $cat;

	function __construct($fmt)
	{
		$this->fmt = $fmt;
    }

    public function dataId($id = null){
        $sql = "SELECT * FROM mod_kardex WHERE mod_kdx_user_id='".$id."'";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            $row=$this->fmt->querys->row($rs);
            $kardexId = $row["mod_kdx_id"];
            $return["id"] = $kardexId;
            $return["name"] = $row["name"];
            $return["fathersLastname"] = $row["mod_kdx_fathers_lastname"];
            $return["mothersLastname"] = $row["mod_kdx_mothers_lastname"];
            $return["state"] = $row["mod_kdx_state"];
            return $return;
        }else{
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function jobtitleData($id)
    {
        $sql = "SELECT * FROM mod_jobtitle WHERE mod_jbt_id='".$id."' AND mod_jbt_state='1'";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            return $row=$this->fmt->querys->row($rs);
        }else{
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function jobtitleKardexId($id,$entId)
    {
        $sql = "SELECT mod_kdx_jbt_jbt_id,mod_jbt_title FROM mod_kardex_jobtitle,mod_jobtitle  WHERE mod_kdx_jbt_kdx_id='".$id."' AND mod_kdx_jbt_jbt_id=mod_jbt_id AND mod_jbt_state='1' AND mod_kdx_jbt_ent_id='".$entId."' ORDER BY mod_kdx_jbt_order ASC";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for ($i=0; $i < $num; $i++) { 
                $row[$i]=$this->fmt->querys->row($rs);
            }
            return $row;
        }else{
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function listDischarge($vars)
    {
        $entId = $vars["idEntitie"];
        $sql = "SELECT DISTINCT * FROM mod_kardex WHERE mod_kdx_state='1' AND mod_kdx_ent_id='".$entId."' ";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for ($i=0; $i < $num; $i++) { 
                $row[$i] =$this->fmt->querys->row($rs);
                // $row[$i]["mod_kdx_entry_date"] = $this->fmt->modules->timeLeft(array('startDate'=> $row[$i]["mod_kdx_entry_date"],'endDate' => $dNow ));
                $row[$i]["mod_kdx_entry_date"] = $this->fmt->modules->formatDate(array('date'=> $row[$i]["mod_kdx_entry_date"],'mode' => 'linear' ));
                $row[$i]["mod_kdx_jobtitle"] = $this->jobtitleKardexId($row[$i]["mod_kdx_id"],$entId);
            }

            return $row;

        }else{
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function treeDepartaments($idEntitie)
    {
        $sql = "SELECT * FROM mod_departments WHERE mod_dep_state='1' AND mod_dep_ent_id='".$idEntitie."' AND mod_dep_parent_id='0'  ORDER BY mod_dep_order";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $row_id = $row["mod_dep_id"];
                $rowParentId = $row["mod_dep_parent_id"];

                $return[$i]['mod_dep_id'] = $row["mod_dep_id"];
                $return[$i]['mod_dep_name'] = $row["mod_dep_name"];
                $return[$i]['level'] = 0;
                if($this->haveChildrenTreeDepartaments($row_id,$idEntitie)){
                    $return[$i]["children"] = $this->childrenTreeDepartaments($row_id,$idEntitie,0);
                }else{
                    $return[$i]["children"] = 0;
                }
            }

            return $return;

        }else{
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function haveChildrenTreeDepartaments($parentId,$idEntitie)
    {
        $sql = "SELECT * FROM mod_departments WHERE mod_dep_state='1' AND mod_dep_ent_id='".$idEntitie."' AND mod_dep_parent_id='".$parentId."'";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            return true;
        }else{
            return false;
        }
        $this->fmt->querys->leave($rs);
    }

    public function childrenTreeDepartaments($parentId,$idEntitie,$level)
    {
        $level ++;
        $sql = "SELECT * FROM mod_departments WHERE mod_dep_state='1' AND mod_dep_ent_id='".$idEntitie."' AND mod_dep_parent_id='".$parentId."' ORDER BY mod_dep_order";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $row_id = $row["mod_dep_id"];
                $rowParentId = $row["mod_dep_parent_id"];
                $return[$i]['mod_dep_id'] = $row["mod_dep_id"];
                $return[$i]['mod_dep_name'] = $row["mod_dep_name"];
                $return[$i]['level'] = $level;
                if($this->haveChildrenTreeJobtitles($row_id,$idEntitie)){
                    $return[$i]["children"] = $this->childrenTreeDepartaments($row_id,$idEntitie,$level);
                }else{
                    $return[$i]["children"] = 0;
                }
            }
            return $return;

        }else{
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function treeJobtitles($idEntitie)
	{
		$sql = "SELECT * FROM mod_jobtitle WHERE mod_jbt_state='1' AND mod_jbt_ent_id='".$idEntitie."' AND mod_jbt_parent_id='0' ORDER BY mod_jbt_order";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $row_id = $row["mod_jbt_id"];
                $rowParentId = $row["mod_jbt_parent_id"];

                $return[$i]['mod_jbt_id'] = $row["mod_jbt_id"];
                $return[$i]['mod_jbt_title'] = $row["mod_jbt_title"];
                $return[$i]['level'] = 0;
                if($this->haveChildrenTreeJobtitles($row_id,$idEntitie)){
                    $return[$i]["children"] = $this->childrenTreeJobtitles($row_id,$idEntitie,0);
                }else{
                    $return[$i]["children"] = 0;
                }
            }

            return $return;

        }else{
            return 0;
        }
        $this->fmt->querys->leave($rs);
	}

    public function haveChildrenTreeJobtitles($parentId,$idEntitie)
    {
        $sql = "SELECT * FROM mod_jobtitle WHERE mod_jbt_state='1' AND mod_jbt_ent_id='".$idEntitie."' AND mod_jbt_parent_id='".$parentId."'";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            return true;
        }else{
            return false;
        }
        $this->fmt->querys->leave($rs);
    }

    public function childrenTreeJobtitles($parentId,$idEntitie,$level)
    {
        $level ++;
        $sql = "SELECT * FROM mod_jobtitle WHERE mod_jbt_state='1' AND mod_jbt_ent_id='".$idEntitie."' AND mod_jbt_parent_id='".$parentId."' ORDER BY mod_jbt_order";
        $rs =$this->fmt->querys->consult($sql);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $row_id = $row["mod_jbt_id"];
                $rowParentId = $row["mod_jbt_parent_id"];
                $return[$i]['mod_jbt_id'] = $row["mod_jbt_id"];
                $return[$i]['mod_jbt_title'] = $row["mod_jbt_title"];
                $return[$i]['level'] = $level;
                if($this->haveChildrenTreeJobtitles($row_id,$idEntitie)){
                    $return[$i]["children"] = $this->childrenTreeJobtitles($row_id,$idEntitie,$level);
                }else{
                    $return[$i]["children"] = 0;
                }
            }
            return $return;

        }else{
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }
}