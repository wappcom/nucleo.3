<?php
require_once('../../../../config.php');
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

require_once("../../../class/rrhh/class.kardex.php");
$kardex = new KARDEX($fmt);

header("Content-Type: application/json");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

switch ($_SERVER["REQUEST_METHOD"]) {
    case 'POST':
        
        $access_token = $fmt->auth->getInput('access_token');
        $refresh_token = $fmt->auth->getInput('refresh_token');
        $entitieId = $fmt->auth->getInput('idEntitie');
        $actions = $fmt->auth->getInput('action');

        if($fmt->users->validateUser(array("access_token"=>$access_token, "refresh_token"=> $refresh_token, "idEntitie" => $entitieId))){
            //echo "'usario validado";
            
            if($actions=="loadDischarge"){
                $return["user"] = $fmt->users->userDataAuthJson(array("access_token"=>$access_token,"idEntitie" => $entitieId));
                $dateUser = json_decode($return["user"]);
                $userId=$dateUser->userId;
                $rolId=$dateUser->rolId;
                echo json_encode($kardex->listDischarge(array("userId"=>$userId,"rolId"=>$rolId,"idEntitie" => $entitieId)));
            }
        }else{
            echo $fmt->errors->errorJson([
                "description"=>"Access Auth. invalid user",
                "code"=>"",
                "lang"=>"es"
            ]); 
        }
    break;

    default:
        echo $fmt->errors->errorJson([
            "description"=>"Access Auth. Metod request.",
            "code"=>"",
            "lang"=>"es"
        ]);
        break;
}