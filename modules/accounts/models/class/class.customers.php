<?php
header('Content-Type: text/html; charset=utf-8');
class CUSTOMERS
{

    var $fmt;
    var $pla;
    var $cat;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function getCustomersAll(array $var = null)
    {
        //return $var;
        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        $orderBy = $this->fmt->emptyReturn($var["vars"]["orderBy"], "DESC");
        $limit = $this->fmt->emptyReturn($var["vars"]["limit"], "");

        if ($limit != "") {
            $limit = "LIMIT " . $limit;
        }

        $sql = "SELECT DISTINCT * FROM mod_customers_enterprises WHERE mod_cen_ent_id = '" . $entitieId . "' ORDER BY  mod_cen_register_date " . $orderBy . $limit;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return["cen"][$i]["id"] = $row["mod_cen_id"];
                $return["cen"][$i]["name"] = $row["mod_cen_name"];
                $return["cen"][$i]["username"] = $row["mod_cen_username"];
                $return["cen"][$i]["code"] = $row["mod_cen_code"];
                $dataArray = $this->customersDataEnterprise($row["mod_cen_id"]);
                $return["cen"][$i]["x"] = $dataArray;
                $return["cen"][$i]["businessName"] = $dataArray["businessName"];
                $return["cen"][$i]["nit"] = $dataArray["nit"];
                $return["cen"][$i]["priority"] = $dataArray["priority"];
                $return["cen"][$i]["typeContract"] = $dataArray["typeContract"];
                $return["cen"][$i]["description"] = $row["mod_cen_description"];

                if ($row["mod_cen_img"]) {
                    $img["id"] = $row["cont_img"];
                    $img["pathurl"] = $this->fmt->files->imgId($row["cont_img"], "");
                    $img["name"] = $this->fmt->files->name($row["cont_img"], "");
                } else {
                    $img = 0;
                }

                $return["cen"][$i]["img"] = $img;
                $return["cen"][$i]["account"]["acuId"] = $row["mod_cen_acu_id"];
                $return["cen"][$i]["registerDate"] = $row["mod_cen_register_date"];
                $return["cen"][$i]["state"] = $row["mod_cen_state"];
            }
        } else {
            $return["cen"] = 0;
        }
        $this->fmt->querys->leave($rs);

        $sql = "SELECT DISTINCT * FROM mod_customers_persons WHERE mod_cpe_ent_id = '" . $entitieId . "' ORDER BY  mod_cpe_register_date " . $orderBy . $limit;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return["cpe"][$i]["id"] = $row["mod_cpe_id"];
                $return["cpe"][$i]["name"] = $row["mod_cpe_name"];
                $return["cpe"][$i]["lastname"] = $row["mod_cpe_lastname"];
                $return["cpe"][$i]["email"] = $row["mod_cpe_email"];
                $return["cpe"]["code"] = $row["mod_cpe_code"];
                $acuId = $row["mod_cpe_acu_id"];
                $return["cpe"][$i]["account"]["acuId"] = $acuId;

                /* if ($row["mod_cen_img"]) {
                    $img["id"] = $row["cont_img"];
                    $img["pathurl"] = $this->fmt->files->imgId($row["cont_img"], "");
                    $img["name"] = $this->fmt->files->name($row["cont_img"], "");
                } else {
                    $img = 0;
                }

                $return["cpe"][$i]["img"] = $img; */

                $return["cpe"][$i]["userId"] = $row["mod_cpe_acu_id"];
                $return["cpe"][$i]["registerDate"] = $row["mod_coo_register_date"];
                $return["cpe"][$i]["state"] = $row["mod_coo_state"];
            }

        } else {
            $return["cpe"] = 0;
        }


        return $return;


    }

    public function getCustomerPersonId(int $id = null)
    {
        $sql = "SELECT * FROM mod_customers_persons WHERE mod_cpe_id = '" . $id . "' ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {

            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_cpe_id"];
            $return["id"] = $id;
            $return["acuId"] = $row["mod_cpe_acu_id"];
            $return["name"] = $row["mod_cpe_name"];
            $return["lastname"] = $row["mod_cpe_lastname"];
            $return["email"] = $row["mod_cpe_email"];
            $return["code"] = $row["mod_cpe_code"];
            $return["phone"] = $row["mod_cpe_phone"];
            $return["ci"] = $row["mod_cpe_ci"];
            $return["adder"] = $row["mod_cpe_adder"];
            $return["registerDate"] = $row["mod_cpe_register_date"];
            $return["userId"] = $row["mod_cpe_user_id"];
            $return["entId"] = $row["mod_cpe_ent_id"];
            $return["state"] = $row["mod_cpe_state"];

            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);

    }

    public function getAccountCustomerPerson(int $acuId = null)
    {

        $sql = "SELECT * FROM mod_customers_persons WHERE mod_cpe_acu_id = '" . $acuId . "' ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);

            return $row["mod_cpe_id"];
        } else {
            return 0;
        }

    }


    public function customersDataPerson(int $id = 0)
    {
        //return $var;
        $sql = "SELECT * FROM mod_customers_persons_data WHERE mod_cpdt_cep_id = '" . $id . "' ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_cpdt_id"];
                $return["id"] = $id;
                $return["businessName"] = $row["mod_cpdt_cep_business_name"];
                $return["typeContribuyente"] = $row["mod_cpdt_cep_type_contribuyente"];
                $return["typeOficio"] = $row["mod_cpdt_cep_type_oficio"];
                $return["nit"] = $row["mod_cpdt_cep_nit"];
                $return["priority"] = $row["mod_cpdt_cep_priority"];
                $return["typeContract"] = $row["mod_cpdt_cep_type_contract"];
                $return["json"] = $row["mod_cpdt_cep_json"];
                $return["user"] = $row["mod_cpdt_cep_user"];
                $return["pw"] = $row["mod_cpdt_cep_pw"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function customersDataEnterprise(int $id = 0)
    {
        //return $var;
        $return = [];
        $sql = "SELECT * FROM mod_customers_enterprises_data WHERE mod_cen_data_cen_id = '" . $id . "' ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_cen_data_id"];
                $return = [
                    "id" => $id,
                    "businessName" => $row["mod_cen_data_business_name"],
                    "nit" => $row["mod_cen_data_nit"],
                    "priority" => $row["mod_cen_data_priority"],
                    "typeContract" => $row["mod_cen_data_type_contract"],
                    "json" => $row["mod_cen_data_json"]
                ];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function addCustomerCompany(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $userId = $var["user"]["userId"];
        $inputs = $var["vars"]["inputs"];
        
        $username = $inputs["username"];
        $code = $inputs["code"];
        $description = $inputs["description"];
        $name = $inputs["name"];
        $img = $inputs["img"] ? $inputs["img"] : 0;
        $cpeId = $inputs["cpeId"] || ""; // id de la persona que representa a la empresa
        $registerDate = $this->fmt->modules->dateFormat();
        $priority = $inputs["priority"];
        $state = $inputs["state"] ? $inputs["state"] : 0;
        $typeContract = $inputs["typeContract"];
        
        $businessName = $inputs["businessName"];
        $nit = $inputs["nit"];
        
        $json = $inputs["json"];

        $sql = "SELECT * FROM mod_customers_enterprises WHERE mod_cen_code = '" . $code . "' OR mod_cen_username = '" . $username . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $return["Error"] = 1;
            $return["status"] = "error";
            $return["message"] = "El nombre de usuario o el código ya existe";
            return $return;
        }
        $this->fmt->querys->leave($rs);

        $insert = "mod_cen_cpe_id,mod_cen_name,mod_cen_username,mod_cen_code,mod_cen_description,mod_cen_img,mod_cen_register_date,mod_cen_user_id,mod_cen_ent_id,mod_cen_state";
        $values = "'" . $cpeId . "','" . $name . "','" . $username . "','" . $code . "','" . $description . "','" . $img . "','" . $registerDate . "','" . $userId . "','" . $entitieId . "','" . $state . "'";
        $sql = "insert into mod_customers_enterprises (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_cen_id) as id from mod_customers_enterprises";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $id = $row["id"];

        $insert = "mod_cen_data_cen_id,mod_cen_data_business_name,mod_cen_data_nit,mod_cen_data_priority,mod_cen_data_type_contract,mod_cen_data_json";
        $values = "'".$id."','" . $businessName . "','" . $nit . "','" . $priority . "','" . $typeContract . "','" . $json . "'";
        $sql = "insert into mod_customers_enterprises_data (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_cen_data_id) as id from mod_customers_enterprises_data";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $idData = $row["id"];

        $sql = "UPDATE mod_customers_enterprises SET mod_cen_ced_id = '" . $idData . "' WHERE mod_cen_id = '" . $id . "'";
        $this->fmt->querys->consult($sql, __METHOD__);

        $rtn["Error"] = 0;
        $rtn["status"] = "success";
        $rtn["message"] = "Client added successfully";
        $rtn["data"]["id"] = $id;
        $rtn["data"]["name"] = $name;
        $rtn["data"]["username"] = $username;
        $rtn["data"]["code"] = $code;
        $rtn["data"]["businessName"] = $businessName;
        $rtn["data"]["nit"] = $nit;
        $rtn["data"]["priority"] = $priority;
        $rtn["data"]["typeContract"] = $typeContract;
        $rtn["data"]["state"] = $state;
        $rtn["data"]["description"] = $description;


        return $rtn;

    }

    public function addCustomerPerson(array $var = null)
    {

        //return $var;
        $userId = $var["user"]["userId"] ?? 0;
        $rolId = $var['user']['rolId'];
        $entitieId = $var['entitieId'];
        $input = $var['vars']['inputs'];

        $name = $input['name'];
        $lastname = $input['lastname'];
        $email = $input['email'];
        //crear codigo unico de cliente de 6 digitos
        $code = $this->fmt->data->createCode(['num' => 6, 'mode' => 'integer']);
        $code = $this->compareCodeCustomer($code);

        $code = $input['code'] ?? hash('sha256', $code);
        $phone = $input['phone'] ?? '';
        $ci = $input['ci'] ?? '';
        $complemento = $input['complemento'] ?? '';
        $planId = $input['planId'] ?? 1;
        $adder = $input['adder'] ?? '';
        $now = $this->fmt->modules->dateFormat();
        $acuId = $input['acuId'] ?? 0;

        //revisar si el email ya existe en customers
        if (!$this->existCustomerValue(["type" => "email", "value" => $email])) {
            $rtn["Error"] = "1";
            $rtn["status"] = "error";
            $rtn["message"] = "El email ya existe";
            return $rtn;
        }

        if (!$this->existCustomerValue(["type" => "phone", "value" => $phone])) {
            $rtn["Error"] = "1";
            $rtn["status"] = "error";
            $rtn["message"] = "El Celular ya existe";
            return $rtn;
        }

        if (!$this->existCustomerValue(["type" => "ci", "value" => $ci])) {
            $rtn["Error"] = "1";
            $rtn["status"] = "error";
            $rtn["message"] = "El CI ya existe";
            return $rtn;
        }

        $insert = "mod_cpe_acu_id,mod_cpe_name,mod_cpe_lastname,mod_cpe_email,mod_cpe_plan_id,mod_cpe_code,mod_cpe_phone,mod_cpe_ci,mod_cpe_ci_complemento,mod_cpe_adder,mod_cpe_register_date,mod_cpe_user_id,mod_cpe_ent_id,mod_cpe_state";
        $values = "'" . $acuId . "','" . $name . "','" . $lastname . "','" . $email . "','" . $planId . "','" . $code . "','" . $phone . "','" . $ci . "','" . $complemento . "','" . $adder . "','" . $now . "','" . $userId . "','" . $entitieId . "','1'";
        $sql = "insert into  mod_customers_persons (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_cpe_id) as id from mod_customers_persons";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        $rtn['Error'] = 0;
        $rtn['status'] = 'success';
        $rtn['data']['id'] = $row['id'];
        $rtn['data']['name'] = $name;
        $rtn['data']['lastname'] = $lastname;
        $rtn['data']['email'] = $email;
        $rtn['data']['code'] = $code;
        $rtn['data']['acuId'] = $acuId;
        return $rtn;
    }

    public function addCustomerPersonaData(array $var = null)
    {
        //return $var;

        $userId = $var["user"]["userId"] ?? 0;
        $rolId = $var['user']['rolId'];
        $entitieId = $var['entitieId'];
        $input = $var['vars']['inputs'];

        $bussinessName = $input['bussinessName'] ?? $input["name"] . " " . $input["lastname"];
        $priority = $input['priority'] ?? 0;
        $typeContract = $input['typeContract'] ?? 0;
        $typeContribuyente = $input['inputTypeContribuyente'] ?? 0;
        $typeOficio = $input['inputTypeOficio'] ?? 0;

        $cpeId = $input['cpeId'] ?? 0;
        $nit = $input['inputNit'] ?? 0;
        $json = $input['inputJson'] ?? "";
        $user = $input['inputUsername'] ?? "";
        $pw = $input['inputPassword'] ?? "";


        if ($cpeId == 0 || $nit == 0) {
            $rtn["Error"] = 1;
            $rtn["status"] = "error";
            $rtn["message"] = "El cpeId o el nit son requeridos";
            return $rtn;
        }

        //verifica si hay un addcustomer con el mismo cpeId y con el mismo nit
        $sql = "select * from mod_customers_persons_data where mod_cpdt_cep_id = '" . $cpeId . "' and mod_cpdt_cep_nit = '" . $nit."'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0){
            $rtn["Error"] = 1;
            $rtn["status"] = "error";
            $rtn["message"] = "El cliente ya existe";
            return $rtn;
        }

        $insert = "mod_cpdt_cep_business_name,mod_cpdt_cep_priority,mod_cpdt_cep_type_contract,mod_cpdt_cep_id,mod_cpdt_cep_nit,mod_cpdt_cep_json,mod_cpdt_cep_user,mod_cpdt_cep_pw,mod_cpdt_cep_type_contribuyente,mod_cpdt_cep_type_oficio";
        $values = "'" . $bussinessName . "','" . $priority . "','" . $typeContract . "','" . $cpeId . "','" . $nit . "','" . $json . "','" . $user . "','" . $pw . "','" . $typeContribuyente . "','" . $typeOficio . "'";
        $sql = "insert into mod_customers_persons_data (" . $insert . ") values (" . $values . ")";

        $this->fmt->querys->consult($sql,__METHOD__);
           
        $rtn["Error"] = 0;
        $rtn["status"] = "success";
        $rtn["message"]["es"] = "Cliente agregado";
        $rtn["message"]["en"] = "Customer added";

       
        return $rtn;

    }

    public function existCustomerValue(array $var = null)
    {
        //return $var;
        $type = $var['type'];
        $value = $var['value'];

        $sql = "SELECT * FROM mod_customers_persons WHERE mod_cpe_" . $type . "='" . $value . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function compareCodeCustomer(string $code = null)
    {

        $sql = "SELECT * FROM mod_customers_persons WHERE mod_cpe_code='" . $code . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $code = $this->fmt->data->createCode(['num' => 6, 'mode' => 'integer']);
            $code = $this->compareCodeCustomer($code);
            return $code;
        } else {
            return $code;
        }
    }

    public function searchCustomersPersons(array $var = null)
    {
        //return $var;

        $userId = $var['user']['userId'];
        $rolId = $var['user']['rolId'];
        $entitieId = $var['entitieId'];
        $input = $var['vars']['input'];

        //input length > 2
        if (strlen($input) <= 2) {
            return 0;
        }

        $sql = "SELECT * FROM mod_customers_persons WHERE mod_cpe_name LIKE '%" . $input . "%' OR mod_cpe_lastname LIKE '%" . $input . "%' OR mod_cpe_code LIKE '%" . $input . "%' OR mod_cpe_email LIKE '%" . $input . "%' OR mod_cpe_phone LIKE '%" . $input . "%' AND mod_cpe_ent_id = '" . $entitieId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_cpe_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["mod_cpe_name"];
                $return[$i]["lastname"] = $row["mod_cpe_lastname"];
                $return[$i]["username"] = $row["mod_cpe_username"];
                $return[$i]["code"] = $row["mod_cpe_code"];
                $return[$i]["email"] = $row["mod_cpe_email"];
                $return[$i]["phone"] = $row["mod_cpe_phone"];
                $return[$i]["ci"] = $row["mod_cpe_ci"];
                $return[$i]["adder"] = $row["mod_cpe_adder"]; //complemento
                $return[$i]["registerDate"] = $row["mod_cpe_register_date"];
                $return[$i]["state"] = $row["mod_cpe_state"];
                $return[$i]["userId"] = $row["mod_cpe_user_id"];
            }

            return $return;
        } else {
            return 0;
        }


    }

    public function validateNitCustomerPerson(string $nit = null)
    {
        //return $nit;
        $sql = "SELECT * FROM mod_customers_persons_data WHERE mod_cpdt_cep_nit = '" . $nit . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }

    }

    public function stateCustomerPerson(array $var = null)
    {
        //return $var;
        $acuId = $var["vars"]["inputs"]['acuId'];
        $email = $var['vars']['inputs']['email'];

        $sql = "SELECT * FROM mod_customers_persons WHERE mod_cpe_acu_id = '" . $acuId . "' AND mod_cpe_email = '" . $email . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return [
                "Error" => 0,
                "status" => "success",
                "data" => $row["mod_cpe_id"]
            ];
        } else {
            return [
                "Error" => 1,
                "status" => "error",
                "message" => "No record found."
            ];
        }
    }

}