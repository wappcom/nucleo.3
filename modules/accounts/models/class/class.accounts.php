<?php
header('Content-Type: text/html; charset=utf-8');
class ACCOUNTS
{

    var $fmt;
    var $pla;
    var $cat;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function dataId($userId = null)
    {
        $sql = "SELECT * FROM mod_accounts_users WHERE mod_acu_id='" . $userId . "' ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            return $this->fmt->querys->row($rs);
        } else {
            return 0;
        }
    }

    public function dataRol(int $rolId = null)
    {
        $sql = "SELECT * FROM mod_accounts_roles WHERE mod_acu_rol_id='" . $rolId . "' ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $rtn["id"] = $row["mod_acu_rol_id"];
            $rtn["name"] = $row["mod_acu_rol_name"];
            $rtn["description"] = $row["mod_acu_rol_description"];
            $rtn["parentId"] = $row["mod_acu_rol_parent_id"];
            $rtn["redirectionUrl"] = $row["mod_acu_rol_redirection_url"];
            $rtn["json"] = $row["mod_acu_rol_json"];
            $rtn["state"] = $row["mod_acu_rol_state"];

            return $rtn;
        } else {
            return 0;
        }
    }

    public function dataPlanId(int $planId = null)
    {
        $sql = "SELECT * FROM mod_accounts_plans WHERE mod_ap_id='" . $planId . "' ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_ap_id"];
            $return["id"] = $id;
            $return["name"] = $row["mod_ap_name"];
            $return["description"] = $row["mod_ap_description"];
            $return["code"] = $row["mod_ap_code"];
            $return["level"] = $row["mod_ap_level"];
            $return["details"] = $row["mod_ap_details"];
            $return["cost"] = $row["mod_ap_cost"];
            $return["mode"] = $row["mod_ap_mode"];
            $return["last"] = $row["mod_ap_last"];
            $return["coin"] = $row["mod_ap_coin"];
            $return["json"] = json_decode($row["mod_ap_json"], true);
            $return["entId"] = $row["mod_ap_ent_id"];
            $return["state"] = $row["mod_ap_state"];
            return $return;
        } else {
            return 0;
        }
    }

    public function dataAccount($userId = null)
    {
        $sql = "SELECT * FROM mod_accounts_users WHERE mod_acu_id='" . $userId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_acu_id"];
            $return["id"] = $id;
            $return["name"] = $row["mod_acu_name"];
            $return["lastname"] = $row["mod_acu_fathers_lastname"] . " " . $row["mod_acu_mothers_lastname"];
            $return["email"] = $row["mod_acu_email"];
            $birthdayDate = $row["mod_acu_birthday_date"];
            $return["ageRange"] = $this->ageRange($birthdayDate);
            $return["age"] = $this->age($birthdayDate);
            $return["stateBirthday"] = $this->stateBirthday($birthdayDate);
            $return["gender"] = $row["mod_acu_gender"];
            $return["dial"] = $row["mod_acu_dial"] ? $row["mod_acu_dial"] : $this->fmt->options->getValue("default_dial");
            $return["celular"] = $row["mod_acu_celular"];
            $return["recordDate"] = $row["mod_acu_record_date"];
            $return["referred"] = $row["mod_acu_referred"];
            $return["code"] = $row["mod_acu_code"];

            return $return;
        } else {
            return 0;
        }
    }

    public function dataAccountCode($code = null)
    {
        $sql = "SELECT * FROM mod_accounts_users WHERE mod_acu_code='" . $code . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_acu_id"];
            $return["id"] = $id;
            $return["name"] = $row["mod_acu_name"];
            $return["lastname"] = $row["mod_acu_fathers_lastname"] . " " . $row["mod_acu_mothers_lastname"];
            $return["email"] = $row["mod_acu_email"];
            $birthdayDate = $row["mod_acu_birthday_date"];
            $return["ageRange"] = $this->ageRange($birthdayDate);
            $return["age"] = $this->age($birthdayDate);
            $return["stateBirthday"] = $this->stateBirthday($birthdayDate);
            $return["gender"] = $row["mod_acu_gender"];
            $return["dial"] = $row["mod_acu_dial"] ? $row["mod_acu_dial"] : $this->fmt->options->getValue("default_dial");
            $return["celular"] = $row["mod_acu_celular"];
            $return["recordDate"] = $row["mod_acu_record_date"];
            $return["referred"] = $row["mod_acu_referred"];

            return $return;
        } else {
            return 0;
        }
    }

    public function dataAccountEmail($email = null)
    {
        $sql = "SELECT * FROM mod_accounts_users WHERE mod_acu_email='" . $email . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_acu_id"];
            return $this->dataAccount($id);
        } else {
            return 0;
        }
        
    }

    public function getEntitieData(int $entitieId = 0)
    {
        //return $var;
        $sql = "SELECT * FROM  entities WHERE ent_id='" . $entitieId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {

            $row = $this->fmt->querys->row($rs);
            $id = $row["ent_id"];
            $return["id"] = $id;
            $return["name"] = $row["ent_name"];
            $return["path"] = $row["ent_path"];
            $return["brand"] = $row["ent_brand"];
            $return["code"] = $row["ent_code"];
            $return["token"] = $row["ent_token"];
            $return["recordDate"] = $row["ent_record_date"];
            $return["type"] = $row["ent_type"];
            $return["notes"] = $row["ent_notes"];
            $return["state"] = $row["ent_state"];

            return $return;
        } else {
            return 0;
        }
    }

    public function dataEnterprise(int $cpeId = null)
    {
        //return $var;
        $sql = "SELECT * FROM mod_customers_enterprises WHERE mod_cen_cpe_id='" . $cpeId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_cen_id"];
            $return["id"] = $id;
            $return["name"] = $row["mod_cen_name"];
            $return["username"] = $row["mod_cen_username"];
            $return["code"] = $row["mod_cen_code"];
            $return["img"] = $this->fmt->files->imgReturn($row["mod_cen_img"]);
            $return["dateRegister"] = $row["mod_cen_register_date"];
            $return["state"] = $row["mod_cen_state"];

            return $return;
        } else {
            return 0;
        }

    }

    public function dataClient(int $acuId = null)
    {
        //return $var;
        $sql = "SELECT * FROM mod_customers_persons WHERE mod_cpe_acu_id='" . $acuId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["mod_cpe_id"];
            $return["id"] = $id;
            $return["name"] = $row["mod_cpe_name"];
            $return["lastname"] = $row["mod_cpe_lastname"];
            $return["email"] = $row["mod_cpe_email"];
            $return["code"] = $row["mod_cpe_code"];
            $return["dateRegister"] = $row["mod_cpe_register_date"];
            $return["state"] = $row["mod_cpe_state"];

            return $return;
        } else {
            return 0;
        }

    }

    public function stateBirthday($birthdayDate = null)
    {
        $today = $this->fmt->modules->dateFormat("", "m-d");

        $firstDate = date('m-d', strtotime($birthdayDate));
        if ($firstDate == $today) {
            return 1;
        } else {
            return 0;
        }
    }

    public function ageRange($birthdayDate = null)
    {
        //return $var;
        $today = $this->fmt->modules->dateFormat();

        $firstDate = new DateTime($birthdayDate);
        $secondDate = new DateTime($today);
        $dif = $firstDate->diff($secondDate);
        $years = $dif->y;

        if ($years < '17') {
            return '14-17';
        } elseif ($years < '24') {
            return '18-24';
        } elseif ($years < '34') {
            return '25-34';
        } elseif ($years < '44') {
            return '35-44';
        } elseif ($years < '54') {
            return '45-54';
        } elseif ($years < '64') {
            return '55-64';
        }

        return '65+';

    }

    public function age($birthdayDate = null)
    {
        //return $var;
        $today = $this->fmt->modules->dateFormat();

        $firstDate = new DateTime($birthdayDate);
        $secondDate = new DateTime($today);
        $dif = $firstDate->diff($secondDate);
        return $dif->y;
    }

    public function loadAccountsUsers($array = null)
    {
        $array;
        $userId = $array["user"]["userId"];
        $rolId = $array["user"]["rolId"];
        $entitieId = $array["entitieId"];
        $vars = $array["vars"];
        $orderBy = $this->fmt->emptyReturn($array["vars"]["orderBy"], "mod_acu_record_date DESC");
        $limit = $this->fmt->emptyReturn($array["vars"]["limit"], "");

        if ($orderBy == "recordDate ASC") {
            $orderBy = "mod_acu_record_date ASC";
        }
        if ($orderBy == "recordDate DESC") {
            $orderBy = "mod_acu_record_date DESC";
        }

        if ($limit != "") {
            $limit = "LIMIT " . $limit;
        }

        $sql = "SELECT DISTINCT * FROM mod_accounts_users WHERE mod_acu_ent_id = '" . $entitieId . "' ORDER BY " . $orderBy . $limit;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_acu_id"];
                $name = $row["mod_acu_name"];
                $lastname = $row["mod_acu_fathers_lastname"] . " " . $this->fmt->emptyReturn($row["mod_acu_mothers_lastname"], "");
                $return[$i]["id"] = $id;
                $return[$i]["nameFull"] = $name . " " . $lastname;
                $return[$i]["name"] = $name;
                $return[$i]["lastname"] = $lastname;
                $return[$i]["fathersLastname"] = $row["mod_acu_fathers_lastname"];
                $return[$i]["mothersLastname"] = $this->fmt->emptyReturn($row["mod_acu_mothers_lastname"], "");
                $return[$i]["ageRange"] = $row["mod_acu_age_range"];
                $return[$i]["email"] = $row["mod_acu_email"];
                $return[$i]["birthdayDate"] = $row["mod_acu_birthday_date"];
                //$return[$i]["mod_acu_password"] = $row["mod_acu_password"];
                $return[$i]["imagen"] = $row["mod_acu_imagen"];
                $return[$i]["level"] = $row["mod_acu_level"];
                $return[$i]["gender"] = $row["mod_acu_gender"];
                $return[$i]["city"] = $row["mod_acu_city"];
                $return[$i]["ci"] = $row["mod_acu_ci"];
                $return[$i]["ciExt"] = $row["mod_acu_ci_ext"];
                $return[$i]["celular"] = $row["mod_acu_celular"];
                $return[$i]["recordDate"] = $row["mod_acu_record_date"];
                $return[$i]["type"] = $row["mod_acu_type"];
                $return[$i]["timezone"] = $row["mod_acu_timezone"];
                $return[$i]["locale"] = $row["mod_acu_locale"];
                $return[$i]["token"] = $row["mod_acu_token"];
                $return[$i]["code"] = $row["mod_acu_code"];
                $return[$i]["referred"] = $row["mod_acu_referred"];
                //$return[$i]["ent_id"] = $row["mod_acu_ent_id"];
                $return[$i]["state"] = $row["mod_acu_state"];
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function add($vars = null)
    {
        //return $vars;
        $entitieId = $vars["entitieId"];
        $var = $vars["vars"];


        $user_name = trim(ucfirst(strtolower($var["name"])));
        $user_fathersLastname = trim(ucfirst(strtolower($var["fathersLastname"])));
        $user_mothersLastname = trim(ucfirst(strtolower($var["mothersLastname"])));
        $user_gender = $var["gender"];
        $user_ci = trim($var["ci"]);
        $user_ext = $var["ext"];
        $user_email = $var["email"];
        $user_birthday = $var["birthday"];
        $user_celular = trim($var["celular"] ? $var["celular"] : "");
        $user_timezone = $var["timezone"] ? $var["timezone"] : "America/La_Paz";
        $user_locale = $var["locale"] ? $var["locale"] : "es_BO";
        $typeAccount = $var["type"] ? $var["type"] : "app";
        $referred = $var["referred"] ? $var["referred"] : "";
        $city = $var["city"] ? $var["city"] : "";
        $today = $this->fmt->modules->dateFormat();
        $pw = $var["password"];
        $age = 0;
        $img = $var["img"];
        $level = "1";
        $token = "1";
        $code = "";



        if ($user_email != "no-email") {
            $returnEmail = $this->checkEmail($user_email);
            if ($returnEmail) {
                $return["Error"] = 1;
                $return["message"] = "error";
                $return["description"] = "account-exist";
                $return["code"] = "";
                $return["userId"] = $returnEmail;
                return $return;
            }
        }
        $returnCI = $this->checkCI($user_ci, $user_ext);
        if ($returnCI) {
            $return["Error"] = 1;
            $return["message"] = "error";
            $return["description"] = "account-exist";
            $return["code"] = "";
            $return["userId"] = $returnCI;
            $return["userData"] = $this->dataAccount($returnCI);
            return $return;
        }

        if (empty($code)) {
            $code = $this->codeAccount();

            if ($typeAccount == "app"){
                $code = $this->codeAccountToken();
            }
        }

        

        $ingresar = 'mod_acu_name,mod_acu_fathers_lastname,mod_acu_mothers_lastname,mod_acu_age_range,mod_acu_email,mod_acu_birthday_date,mod_acu_password,mod_acu_imagen,mod_acu_level,mod_acu_gender,mod_acu_ci,mod_acu_ci_ext,mod_acu_city,mod_acu_celular,mod_acu_record_date,mod_acu_type,mod_acu_timezone,mod_acu_locale,mod_acu_token,mod_acu_code,mod_acu_referred,mod_acu_ent_id,mod_acu_state';

        $valores = "'" . $user_name . "','" .
            $user_fathersLastname . "','" .
            $user_mothersLastname . "','" .
            $age . "','" .
            $user_email . "','" .
            $user_birthday . "','" .
            $pw . "','" .
            $img . "','" .
            $level . "','" .
            $user_gender . "','" .
            $user_ci . "','" .
            $user_ext . "','" .
            $city . "','" .
            $user_celular . "','" .
            $today . "','" .
            $typeAccount . "','" .
            $user_timezone . "','" .
            $user_locale . "','" .
            $token . "','" .
            $code . "','" .
            $referred . "','" .
            $entitieId . "','1'";

        $sql = "insert into mod_accounts_users (" . $ingresar . ") values (" . $valores . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_acu_id) as id from mod_accounts_users";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        return $row["id"];
    }

    public function addAccount($vars = null)
    {
        //return $vars["vars"];
        $var = $vars["vars"];

        $user_name = trim(ucfirst(strtolower($var["name"])));
        $user_fathersLastname = trim(ucfirst(strtolower($var["fathersLastname"])));
        $user_mothersLastname = trim(ucfirst(strtolower($var["mothersLastname"])));
        $user_gender = $var["gender"];
        $user_ci = trim($var["ci"] ? $var["ci"] : 0);
        $user_ext = $var["ext"] ? $var["ext"] : "";
        $user_email = $var["email"] ? $var["email"] : "";
        $user_birdaydate = $this->fmt->data->dbDate($var["birthday"]);
        $user_celular = trim($var["celular"] ? $var["celular"] : 0);
        $user_timezone = $var["timezone"] ? $var["timezone"] : "America/Lima";
        $user_locale = $var["locale"] ? $var["locale"] : "es_PE";
        $typeAccount = $var["type"] ? $var["type"] : "app";
        $referred = $var["referred"] ? $var["referred"] : "";
        $city = $var["city"] ? $var["city"] : "";
        $today = $this->fmt->modules->dateFormat();
        $pw = $var["password"];
        $age = 0;
        $img = $var["img"];
        $level = "1";
        $token = $var["token"] != "" ? $var["token"] : "1";
        $code = $var["code"] != "" ? $var["code"] : "";
        $entitieId = $vars["entitieId"] ? $vars["entitieId"] : 1;
        $state = $this->fmt->emptyReturn($var["state"], 0);


        $returnEmail = $this->checkEmail($user_email);


        if ($returnEmail) {
            $return["Error"] = "1";
            $return["status"] = "error";
            $return["description"] = "account-exist";
            $return["message"]["es"] = "Cuenta existente";
            $return["message"]["en"] = "Account existent";
            $return["code"] = "";
            $return["userId"] = $returnEmail;
            return $return;
        }


        if (empty($code)) {
            $code = uniqid() . $this->codeAccount("general", "12");
        }

        $ingresar = 'mod_acu_name,mod_acu_fathers_lastname,mod_acu_mothers_lastname,mod_acu_age_range,mod_acu_email,mod_acu_birthday_date,mod_acu_password,mod_acu_imagen,mod_acu_level,mod_acu_gender,mod_acu_ci,mod_acu_ci_ext, mod_acu_city,mod_acu_celular,mod_acu_record_date,mod_acu_type,mod_acu_timezone,mod_acu_locale,mod_acu_token,mod_acu_code,mod_acu_ent_id,mod_acu_referred,mod_acu_state';

        $valores = "'" . $user_name . "','" .
            $user_fathersLastname . "','" .
            $user_mothersLastname . "','" .
            $age . "','" .
            $user_email . "','" .
            $user_birdaydate . "','" .
            $pw . "','" .
            $img . "','" .
            $level . "','" .
            $user_gender . "','" .
            $user_ci . "','" .
            $user_ext . "','" .
            $city . "','" .
            $user_celular . "','" .
            $today . "','" .
            $typeAccount . "','" .
            $user_timezone . "','" .
            $user_locale . "','" .
            $token . "','" .
            $code . "','" .
            $entitieId . "','" . $referred . "','" . $state . "'";

        $sql = "insert into mod_accounts_users (" . $ingresar . ") values (" . $valores . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_acu_id) as id from mod_accounts_users";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        //return $row["id"];

        if ($typeAccount == "app") {
            $returnMail = $this->sendMailCode(
                array(
                    "name" => $user_name,
                    "lastname" => $user_fathersLastname,
                    "email" => $user_email,
                    "key" => $code
                )
            );

            if ($returnMail["status"] != "success") {
                $rtn["Error"] = "1";
                $rtn["status"] = "error";
                $rtn["message"]["es"] = "Error de envio.";
                $rtn["message"]["en"] = "Error sending.";
                $rtn["data"] = $returnMail;
                return $rtn;
            }
        }

        return $row["id"];
    }

    public function codeAccount($mode = "number", $quantity = 6)
    {
        $code = $this->fmt->modules->createCode(array("mode" => $mode, "quantity" => $quantity));

        if ($this->checkCode($code)) {
            $this->codeAccount($mode, $quantity);
        } else {
            return $code;
        }
    }


    public function codeAccountToken($mode = "number", $quantity = 6)
    {
        $code = $this->fmt->modules->createCode(array("mode" => $mode, "quantity" => $quantity));

        if ($this->checkCodeToken($code)) {
            $this->codeAccountToken($mode, $quantity);
        } else {
            return $code;
        }
    }


    public function codeCustomersPersons($mode = "number", $quantity = 6)
    {
        $code = $this->fmt->modules->createCode(array("mode" => $mode, "quantity" => $quantity));

        if ($this->checkCodeCustomersPersons($code)) {
            $this->codeCustomersPersons($mode, $quantity);
        } else {
            return $code;
        }
    }

    public function codeCustomersEnterprises($mode = "number", $quantity = 6)
    {
        $code = $this->fmt->modules->createCode(array("mode" => $mode, "quantity" => $quantity));

        if ($this->checkCodeCustomersEnterprises($code)) {
            $this->codeCustomersEnterprises($mode, $quantity);
        } else {
            return $code;
        }
    }

    public function update($var = null)
    {
        $userId = $var["userId"];
        $vars = $var["vars"];
        $user_byrthdate = $vars["birthday"];
        $user_ci = $vars["ci"];
        $user_ext = $vars["ext"];
        $user_pw = base64_encode($vars["password"]);

        $sql = "UPDATE mod_accounts_users SET
					mod_acu_birthday_date='" . $user_byrthdate . "',
					mod_acu_ci='" . $user_ci . "', 
					mod_acu_password='" . $user_pw . "', 
					mod_acu_ci_ext='" . $user_ext . "' 
					WHERE mod_acu_id ='" . $userId . "'";
        $this->fmt->querys->consult($sql);

        return true;
    }

    public function updateAccount($vars = null)
    {
        $var = $vars["vars"];
        $acuId = $var["acuId"];
        $name = ucwords(strtolower($var["name"]));
        $fathers_lastname = ucwords(strtolower($var["fathersLastname"]));
        $mothers_lastname = ucwords(strtolower($var["mothersLastname"]));
        $gender = $var["gender"];
        $ci = $var["ci"];
        $ext = $var["ext"];
        $email = $var["email"];
        $age_range = $var["ageRange"];
        $birthday_date = $var["birthday"];
        $celular = $var["celular"];
        $city = $var["city"];
        $referred = $var["referred"];


        $sql = "UPDATE mod_accounts_users SET
                    mod_acu_name ='" . $name . "',
                    mod_acu_fathers_lastname ='" . $fathers_lastname . "',
                    mod_acu_mothers_lastname ='" . $mothers_lastname . "',
                    mod_acu_age_range ='" . $age_range . "',
                    mod_acu_email ='" . $email . "',
                    mod_acu_birthday_date ='" . $birthday_date . "',
                    mod_acu_gender ='" . $gender . "',
                    mod_acu_ci ='" . $ci . "',
                    mod_acu_ci_ext ='" . $ext . "',
                    mod_acu_city ='" . $city . "',
                    mod_acu_celular ='" . $celular . "',
                    mod_acu_referred ='" . $referred . "'
        WHERE mod_acu_id ='" . $acuId . "'";
        $this->fmt->querys->consult($sql);

        return $acuId;
    }

    public function updateData(int $acuId = null, string $column = null, string $value = null)
    {
        $sql = "UPDATE mod_accounts_users SET
                    " . $column . " ='" . $value . "'
                WHERE mod_acu_id ='" . $acuId . "'";
        $this->fmt->querys->consult($sql);

        return 1;
    }

    public function deleteRelationServises($acuId = null)
    {
        $this->fmt->modules->deleteRelation(array("from" => "mod_accounts_services", "column" => "mod_acu_sv_acu_id", "item" => $acuId));
    }

    public function checkEmail($email = null)
    {
        if ($email == null) {
            return 0;
        }
        $sql = "SELECT * FROM mod_accounts_users WHERE mod_acu_email='" . $email . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_acu_id"];
        } else {
            return 0;
        }
    }

    public function sendMailActivation(array $var = null)
    {
        //return $var;
        $name = $var["name"];
        $lastname = $var["lastname"];
        $email = $var["email"];
        $key = base64_encode($email . "&" . $var["key"]);

        $subject = "Activación de cuenta";

        // building mail
        $indexMail = file_get_contents(_PATH_NUCLEO . "views/mail/mail-mensaje.htm");
        $reset = file_get_contents(_PATH_HOST . "sites/default/views/mail/mail-activation.html");

        $linkLogo = $this->fmt->setUrlNucleo($this->fmt->options->getItem("path_logo"));
        $pathActivation = $this->fmt->options->getItem("path_activation");

        $pathActivation = $this->fmt->setUrlNucleo($pathActivation);

        $logo = '<img width="200px" src="' . $linkLogo . '">';

        $link = $pathActivation . "?key=" . $key;

        $footer = $this->fmt->options->getItem("footer");


        $textoMail = str_replace("{{_NAME}}", $name . ' ' . $lastname, $reset);
        $textoMail = str_replace("{{_LINK}}", $link, $textoMail);


        $bodyMail = str_replace("#logo#", $logo, $indexMail);
        $bodyMail = str_replace("#footer#", $footer, $bodyMail);
        $bodyMail = str_replace("#body#", $textoMail, $bodyMail);


        return $this->fmt->mails->send(
            array(
                'email' => [$email],
                'name' => $name . " " . $lastname,
                'subject' => $subject,
                'setFromMail' => _EMAIL_USER,
                'setFromName' => _EMAIL_USER_NAME,
                'body' => $bodyMail
            )
        );
    }

    public function returnSendCode( array $var = null){
        //return $var;
        $return = $this->sendMailCode([
            "name" => $var["vars"]["name"],
            "lastname" => $var["vars"]["lastname"],
            "email" => $var["vars"]["email"],
            "key" => $var["vars"]["key"]
        ]);

        $rtn["Error"] = 0;
        $rtn["status"] = "success";
        $rtn["message"] = "Codigo enviado correctamente";
        $rtn["data"] = $return;
        return $rtn;
        
        
    }


    public function sendMailCode(array $var = null)
    {
        //return $var;
        $name = $var["name"];
        $lastname = $var["lastname"];
        $email = $var["email"];
        $key = base64_encode($email . "&" . $var["key"]);

        $subject = "Codigo activación de cuenta";

        // building mail
        $indexMail = file_get_contents(_PATH_NUCLEO . "views/mail/mail-mensaje.htm");
        $reset = file_get_contents(_PATH_HOST . "views/mail/mail-code.html");

        $linkLogo = $this->fmt->setUrlNucleo($this->fmt->options->getItem("path_logo"));
        $pathActivation = $this->fmt->options->getItem("path_activation");

        $pathActivation = $this->fmt->setUrlNucleo($pathActivation);

        $code = $this->fmt->modules->createCode(array("mode" => "number"));

        $logo = '<img width="200px" src="' . $linkLogo . '">';

        $link = $pathActivation . "?key=" . $key;

        $footer = $this->fmt->options->getItem("footer");

        $sql = "UPDATE mod_accounts_users SET
                mod_acu_token ='" . $code. "'
                WHERE mod_acu_email = '" . $email. "' ";
        $this->fmt->querys->consult($sql);


        $textoMail = str_replace("{{_NAME}}", $name . ' ' . $lastname, $reset);
        $textoMail = str_replace("{{_LINK}}", $link, $textoMail);
        $textoMail = str_replace("{{_CODE}}", $code, $textoMail);


        $bodyMail = str_replace("#logo#", $logo, $indexMail);
        $bodyMail = str_replace("#footer#", $footer, $bodyMail);
        $bodyMail = str_replace("#body#", $textoMail, $bodyMail);


        return $this->fmt->mails->send(
            array(
                'email' => [$email],
                'name' => $name . " " . $lastname,
                'subject' => $subject,
                'setFromMail' => _EMAIL_USER,
                'setFromName' => _EMAIL_USER_NAME,
                'body' => $bodyMail
            )
        );
    }

    public function checkCode($code = null)
    {
        if ($code == null) {
            return 0;
        }
        $sql = "SELECT * FROM mod_accounts_users WHERE mod_acu_code='" . $code . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function checkCodeToken($code = null)
    {
        if ($code == null) {
            return 0;
        }
        $sql = "SELECT * FROM mod_accounts_users WHERE mod_acu_token='" . $code . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function checkCodeCustomersPersons($code = null)
    {
        if ($code == null) {
            return 0;
        }
        $sql = "SELECT * FROM mod_customers_persons WHERE mod_cpe_code='" . $code . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function checkCodeCustomersEnterprises($code = null)
    {
        if ($code == null) {
            return 0;
        }
        $sql = "SELECT * FROM mod_customers_enterprises WHERE mod_cen_code='" . $code . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function checkBirthdate($userId = null)
    {
        $sql = "SELECT mod_acu_birthday_date FROM mod_accounts_users WHERE mod_acu_id='" . $userId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $bd = $row["mod_acu_birthday_date"];

            if ($bd != "0000-00-00") {
                return $row["mod_acu_birthday_date"];
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function checkCI($ci = null, $ext = null)
    {
        if ($ci == null) {
            return 0;
        }

        if ($ext == null) {
            $ex = "AND mod_acu_ci_ext='" . $ext . "'";
        } else {
            $ex = "";
        }

        $sql = "SELECT * FROM mod_accounts_users WHERE mod_acu_ci='" . $ci . "' " . $ex;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_acu_id"];
        } else {
            return 0;
        }
    }

    public function checkCodeId($userId = null)
    {
        if ($userId == null) {
            return 0;
        }
        $sql = "SELECT * FROM mod_accounts_users WHERE mod_acu_id='" . $userId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_acu_code"];
        } else {
            return 0;
        }
    }

    public function validateUser($vars = null)
    {
        //return $vars;
        $email = $vars['email'];
        $pw = $vars['password'];
        $password = base64_encode($pw);

        $sql = "SELECT mod_acu_id FROM mod_accounts_users WHERE mod_acu_email='" . $email . "' AND mod_acu_password='" . $password . "' AND mod_acu_state=1";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_acu_id"];
        } else {
            return 0;
        }
    }

    public function validateCI($ci = null, $complemento = null)
    {
        //return $ci.":".$complemento;

        if (empty($ci)) {
            return 0;
        }
        
        $sql = "SELECT mod_cpe_ci, mod_cpe_ci_complemento FROM mod_customers_persons WHERE mod_cpe_ci = '".$ci."'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        
        if ($num == 0) {
            return 0; // CI no existe
        }
        
        $row = $this->fmt->querys->fetch($rs);

        if ($num > 0){
            if ($row['mod_cpe_ci_complemento'] === $complemento && $row['mod_cpe_ci_complemento'] !== null) {
                return 1;
            }
        }
        
        // Si el CI existe y el complemento coincide exactamente o el complemento del CI es 0
       
        
        // CI existe pero con complemento diferente o vacío
        return 2;
    }

    public function userValidate($vars = null)
    {
        //return $vars;
        $email = $vars['email'];
        $pw = $vars['password'];
        $password = base64_encode($pw);

        require_once(_PATH_NUCLEO . "modules/sales/models/class/payments.class.php");
        $payments = new PAYMENTS($this->fmt);

        $sql = "SELECT mod_acu_id,mod_acu_code FROM mod_accounts_users WHERE mod_acu_email='" . $email . "'  AND mod_acu_state='1'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $sql2 = "SELECT mod_acu_id,mod_acu_code, mod_acu_name, mod_acu_fathers_lastname, mod_acu_imagen, mod_acu_level, mod_acu_json FROM mod_accounts_users WHERE mod_acu_email='" . $email . "' AND mod_acu_password='" . $password . "' AND mod_acu_state='1'";
            $rs2 = $this->fmt->querys->consult($sql2, __METHOD__);
            $num2 = $this->fmt->querys->num($rs2);
            if ($num2 > 0) {
                $row2 = $this->fmt->querys->row($rs2);
                $acuId = $row2["mod_acu_id"];
                $rolId = $this->accountRol($acuId, '1');
                $name = $row2["mod_acu_name"] . " " . $row2["mod_acu_fathers_lastname"];
                $rtn["Error"] = 0;
                $rtn["account"]["acuId"] = $row2["mod_acu_id"];
                $rtn["account"]["email"] = $email;
                $rtn["account"]["fullname"] = $name;
                $rtn["account"]["name"] = $row2["mod_acu_name"];
                $rtn["account"]["fathersLastname"] = $row2["mod_acu_fathers_lastname"];
                $rtn["account"]["mothersLastname"] = $row2["mod_acu_mothers_lastname"];
                $rtn["account"]["initial"] = $this->fmt->users->initial($name);
                $rtn["account"]["initialName"] = $row2["mod_acu_name"];
                $rtn["account"]["img"] = $this->fmt->files->imgReturn($row2["mod_acu_imagen"]);
                $rtn["account"]["level"] = $row2["mod_acu_level"];
                $dataAcu = $this->dataRol($rolId);
                $rtn["account"]["rol"] = $dataAcu;
                $rtn["account"]["json"] = $row2["mod_acu_json"];
                //$rtn["account"]["rol"] =  $this->dataRol($acuId);
                $rtn["account"]["plans"] = $this->plans($vars);

                $count = 0;

                //return $this->createToken(array('acuId' => $acuId, 'type' => 'app', 'token' => $code));

                $grant_type = $this->fmt->auth->getInput('grant_type');
                $clientId = $this->fmt->auth->getInput('client_id');

                $accessToken = base64_encode($clientId . "." . $acuId . "." . $grant_type . "." . uniqid(microtime(), true));
                $refreshToken = md5(uniqid(microtime(), true));

                if ($this->createToken(array('acuId' => $acuId, 'type' => 'accessToken', 'token' => $accessToken)) == 0) {
                    $count++;
                }
                if ($this->createToken(array('acuId' => $acuId, 'type' => 'refreshToken', 'token' => $refreshToken)) == 0) {
                    $count++;
                }

                $rtn["accountToken"]["accessToken"] = $accessToken;
                $rtn["accountToken"]["refreshToken"] = $refreshToken;
                $rtn["client"] = $this->dataClient($acuId);
                $arrayCpe = $this->dataClient($acuId);
                $rtn["enterprises"] = $this->dataEnterprise($arrayCpe["id"]);

                if ($count == 0) {
                    return $rtn;
                } else {
                    $rtn["Error"] = 1;
                    $rtn["type"] = 0;
                    $rtn["message"]['en'] = "access token create error";
                    $rtn["message"]['es'] = "Error al crear el token de acceso.";
                    return $rtn;
                }
            } else {
                $rtn["Error"] = 1;
                $rtn["type"] = 1;
                $rtn["message"]['en'] = "Password incorrect";
                $rtn["message"]['es'] = "Contraseña incorrecta.";
                return $rtn;
            }
        } else {
            $rtn["Error"] = 1;
            $rtn["type"] = 2;
            $rtn["message"]['en'] = "Email incorrect";
            $rtn["message"]['es'] = "Email incorrecto.";
            return $rtn;
        }
    }

    public function plans(array $var = null)
    {
        $entitieId = $var["entitieId"] || "1";

        $sql = "SELECT * FROM mod_accounts_plans WHERE mod_ap_ent_id='" . $entitieId . "' AND mod_ap_state='1'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_"];
                $return[$i]["id"] = $id;
                $return[$i]["id"] = $row["mod_ap_id"];
                $return[$i]["name"] = $row["mod_ap_name"];
                $return[$i]["description"] = $row["mod_ap_description"];
                $return[$i]["code"] = $row["mod_ap_code"];
                $return[$i]["level"] = $row["mod_ap_level"];
                $return[$i]["details"] = $row["mod_ap_details"];
                $return[$i]["cost"] = $row["mod_ap_cost"];
                $return[$i]["coin"] = $row["mod_ap_coin"];
                $return[$i]["json"] = $row["mod_ap_json"];
                $return[$i]["ent_id"] = $row["mod_ap_ent_id"];
                $return[$i]["status"] = $row["mod_ap_status"];
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function accountRol(int $acuId = null, int $entId = null)
    {

        $sql = "SELECT mod_acu_user_rol_rol_id FROM mod_accounts_users_roles WHERE mod_acu_user_rol_acu_id='" . $acuId . "' AND mod_acu_user_rol_ent_id='" . $entId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_acu_user_rol_rol_id"];
        } else {
            return 0;
        }


    }

    public function createUserToken($userId = null, $type = null, $token = null)
    {
        $ua = $this->fmt->users->getBrowser();
        if (($userId != null) && ($type != null) && ($token != null)) {
            $enter = "mod_atk_user_id,mod_atk_type,mod_atk_token,mod_atk_date,mod_atk_dates_browser"; // 1. local 2.fb 3.gl 4. in
            $values = "'" . $userId . "','" . $type . "','" . $token . "','" . $this->fmt->modules->dateFormat() . "','" . $ua['name'] . "," . $ua['version'] . "," . $ua["platform"] . ":" . $ua['userAgent'] . "'";
            $sql = "insert into mod_accounts_token (" . $enter . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
            return 1;
        } else {
            return 0;
        }
    }

    public function createToken($vars = null)
    {
        //return $vars;
        $acuId = $vars['acuId'];
        $type = $vars['type'];
        $token = $vars['token'];

        $ua = $this->fmt->users->getBrowser();
        if (($acuId != null) && ($type != null) && ($token != null)) {
            $enter = "mod_atk_user_id,mod_atk_type,mod_atk_token,mod_atk_date,mod_atk_dates_browser"; // 1. local 2.fb 3.gl 4. in
            $values = "'" . $acuId . "','" . $type . "','" . $token . "','" . $this->fmt->modules->dateFormat() . "','" . $ua['name'] . "," . $ua['version'] . "," . $ua["platform"] . ":" . $ua['userAgent'] . "'";
            $sql = "insert into mod_accounts_token (" . $enter . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
            return 1;
        } else {
            return 0;
        }
    }

    public function getTokenUser($userId, $type)
    {
        $sql = "SELECT mod_atk_token FROM mod_accounts_token WHERE mod_atk_type='" . $type . "' AND mod_atk_user_id='" . $userId . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_atk_token"];
        } else {
            return 0;
        }

    }

    public function getUserToken($token, $type)
    {
        $sql = "SELECT mod_atk_user_id FROM mod_accounts_token WHERE mod_atk_type='" . $type . "' AND mod_atk_token='" . $token . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_atk_user_id"];
        } else {
            return 0;
        }

    }

    public function addresses($userId = null)
    {
        require_once (_PATH_NUCLEO . "modules/logistics/controllers/class/class.logistics.php");
        $logistics = new LOGISTICS($this->fmt);
        $sql = "SELECT DISTINCT mod_add_id,mod_add_name,mod_add_alias,mod_add_coord FROM mod_accounts_addresses, mod_addresses WHERE mod_acu_add_acu_id='" . $userId . "' AND mod_add_state=1  ORDER BY mod_acu_add_order ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_add_id"];
                $return[$i]["id"] = $row["mod_add_id"];
                $return[$i]["name"] = $row["mod_add_name"];
                $return[$i]["alias"] = $row["mod_add_alias"];
                $return[$i]["coord"] = $row["mod_add_coord"];
                $perimeterId = $logistics->getRelationAddressDelivery($id);
                $dataPerimeter = $logistics->dataPerimeter($perimeterId);
                $return[$i]["perimeterId"] = $perimeterId;
                $return[$i]["perimeterCost"] = $dataPerimeter["cost"];
                $return[$i]["perimeterName"] = $dataPerimeter["name"];
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function invoiceData($userId = null)
    {

        $sql = "SELECT DISTINCT * FROM mod_accounts_invoices_data WHERE mod_acu_invd_acu_id='" . $userId . "' AND mod_acu_invd_state > 0  ORDER BY mod_acu_invd_order ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["mod_acu_invd_id"];
                $return[$i]["id"] = $id;
                $return[$i]["razonSocial"] = $row["mod_acu_invd_razon_social"];
                $return[$i]["nit"] = $row["mod_acu_invd_nit"];
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function validateUserToken($vars = null)
    {
        $access_token = $vars['access_token'];
        $refresh_token = $vars['refresh_token'];
        $idEntitie = $vars['idEntitie'];

        $decode = base64_decode($access_token);
        $arrayUserDates = explode(".", $decode);
        $userId = $arrayUserDates[1];
        $rolId = $arrayUserDates[2];

        $sql = "SELECT mod_atk_user_id FROM  mod_accounts_token WHERE mod_atk_user_id='" . $userId . "' AND  mod_atk_type='access_token' AND mod_atk_token='" . $access_token . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num == 0) {
            return 0;
        }


        $sql2 = "SELECT mod_atk_user_id FROM  mod_accounts_token WHERE mod_atk_user_id='" . $userId . "' AND  mod_atk_type='refresh_token' AND mod_atk_token='" . $refresh_token . "'";
        $rs2 = $this->fmt->querys->consult($sql2);
        $num2 = $this->fmt->querys->num($rs2);

        if ($num2 > 0) {
            return $userId;
        } else {
            return 0;
        }
    }

    public function validateTokenAccess($var = null)
    {
        //return $var;
        $access_token = $var['accessToken'];
        $refresh_token = $var['refreshToken'];
        $idEntitie = $var['idEntitie'];

        $decode = base64_decode($access_token);
        $arrayUserDates = explode(".", $decode);
        $userId = $arrayUserDates[1];


        $sql = "SELECT mod_atk_user_id FROM  mod_accounts_token WHERE mod_atk_user_id='" . $userId . "' AND  mod_atk_type='accessToken' AND mod_atk_token='" . $access_token . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num == 0) {
            return 0;
        }


        $sql2 = "SELECT mod_atk_user_id FROM  mod_accounts_token WHERE mod_atk_user_id='" . $userId . "' AND  mod_atk_type='refreshToken' AND mod_atk_token='" . $refresh_token . "'";
        $rs2 = $this->fmt->querys->consult($sql2);
        $num2 = $this->fmt->querys->num($rs2);

        if ($num2 > 0) {
            return $userId;
        } else {
            return 0;
        }
    }

    public function getTokerConnectId(array $var = null)
    {
        //return $var;
        $inputs = $var["vars"]["inputs"];
        $acuId = $this->createAccount($var);
        $clientId = $inputs["clientId"];
        $clientSecret = $inputs["clientSecret"];


        $accessToken = base64_encode($clientId . "." . $acuId . "." . $clientSecret . "." . uniqid(microtime(), true));
        $refreshToken = md5(uniqid(microtime(), true));

        $returnCreateUserToken = $this->createUserToken($acuId, "accessToken", $accessToken);
        $returnCreateRefreshToken = $this->createUserToken($acuId, "refreshToken", $refreshToken);

        $rtn["Error"] = 0;
        $rtn["status"] = "success";
        $rtn["data"]["connectId"]["accessToken"] = $accessToken;
        $rtn["data"]["connectId"]["refreshToken"] = $refreshToken;

        return $rtn;


    }

    public function createAccount(array $var = null)
    {
        //return $var;
        $vars = $var["vars"]["inputs"];
        $entitieId = $vars["entitieId"];

        $now = $this->fmt->modules->dateFormat();

        $name = $vars["name"] ? $vars["name"] : "temp";
        $fathers_lastname = $vars["fathersLastname"] ? $vars["fathersLastname"] : "";
        $mothers_lastname = $vars["mothersLastname"] ? $vars["mothersLastname"] : "";
        $age_range = $vars["age_range"] ? $vars["age_range"] : "";
        $json = $vars["json"] ? $vars["json"] : "";
        $email = $vars["email"] ? $vars["email"] : uniqid("temp_") . "@temp.com";
        $birthday_date = $vars["birthdayDate"] ? $this->fmt->data->dbDate($vars["birthdayDate"]) : "";
        $password = $vars["password"] ? $vars["password"] : "";
        $imagen = $vars["imagen"] ? $vars["imagen"] : "";
        $level = $vars["level"] ? $vars["level"] : "";
        $gender = $vars["gender"] ? $vars["gender"] : "";
        $ci = $vars["ci"] ? $vars["ci"] : uniqid("ci");
        $ciExt = $vars["ciExt"] ? $vars["ciExt"] : "";
        $dial = $vars["dial"] ? $vars["dial"] : "";
        $celular = $vars["celular"] ? $vars["celular"] : "";
        $record_date = $vars["recordDate"] ? $vars["recordDate"] : $now;
        $type = $vars["type"] ? $vars["type"] : "";
        $city = $vars["city"] ? $vars["city"] : "";
        $timezone = $vars["timezone"] ? $vars["timezone"] : "America/LaPaz";
        $locale = $vars["locale"] ? $vars["locale"] : "es:Es";
        $token = $vars["token"] ? $vars["token"] : "";
        $code = $vars["code"] ? $vars["code"] : "";
        $referred = $vars["referred"] ? $vars["referred"] : "referred";
        $ent_id = $vars["entitieId"] ? $vars["entitieId"] : 1;
        $state = $vars["state"] ? $vars["state"] : 0;

        $insert = "mod_acu_name,mod_acu_fathers_lastname,mod_acu_mothers_lastname,mod_acu_age_range,mod_acu_json,mod_acu_email,mod_acu_birthday_date,mod_acu_password,mod_acu_imagen,mod_acu_level,mod_acu_gender,mod_acu_ci,mod_acu_ci_ext,mod_acu_dial,mod_acu_celular,mod_acu_record_date,mod_acu_type,mod_acu_city,mod_acu_timezone,mod_acu_locale,mod_acu_token,mod_acu_code,mod_acu_referred,mod_acu_ent_id,mod_acu_state";
        $values = "'" . $name . "','" .
            $fathers_lastname . "','" .
            $mothers_lastname . "','" .
            $age_range . "','" .
            $json . "','" .
            $email . "','" .
            $birthday_date . "','" .
            $password . "','" .
            $imagen . "','" .
            $level . "','" .
            $gender . "','" .
            $ci . "','" .
            $ciExt . "','" .
            $dial . "','" .
            $celular . "','" .
            $record_date . "','" .
            $type . "','" .
            $city . "','" .
            $timezone . "','" .
            $locale . "','" .
            $token . "','" .
            $code . "','" .
            $referred . "','" .
            $entitieId . "','" .
            $state . "'";
        $sql = "insert into  mod_accounts_users (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_acu_id) as id from mod_accounts_users";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        return $row["id"];

    }

    public function decodeTokenAccess($token = null)
    {
        $arr = explode(".", base64_decode($token));

        $vars["client_id"] = $arr[0];
        $vars["userId"] = $arr[1];
        $vars["grant_type"] = $arr[2];

        return $vars;
    }

    public function encodeTokenAccess($vars = null)
    {
        $client_id = $vars["client_id"];
        $userId = $vars["userId"];
        $grant_type = $vars["grant_type"];
        return base64_encode($clientId . "." . $userId . "." . $grant_type . "." . uniqid(microtime(), true));
    }

    public function sendCode($vars = null)
    {

        $userId = $vars["userId"];
        $mailLogo = $vars["mailLogo"];
        $mailFooter = $vars["mailFooter"];
        $email = $vars["email"];
        $name = $vars["name"];
        $mailSubject = $vars["mailSubject"];
        $setFromName = $vars["setFromName"];

        $activationHtml = file_get_contents(_PATH_NUCLEO . "views/mail/mail-activation.htm");
        $code = $this->checkCodeId($userId);
        $body = str_replace("#CODE#", $code, $activationHtml);
        //$body = str_replace("#link#", _PATH_WEB."activation/user/". $accessToken,$activationHtml);

        $mailHtml = file_get_contents(_PATH_NUCLEO . "views/mail/mail-mensaje.htm");

        $mensajeMail = str_replace("#logo#", $mailLogo, $mailHtml);
        $mensajeMail = str_replace("#body#", $body, $mensajeMail);
        $mensajeMail = str_replace("#footer#", $mailFooter, $mensajeMail);



        $return = $this->fmt->mails->send(
            array(
                "Error" => 0,
                'email' => $email,
                'name' => $name,
                'subject' => $mailSubject,
                'setFromMail' => _EMAIL_USER,
                'setFromName' => $setFromName,
                'body' => $mensajeMail
            )
        );

        if ($return) {
            return 1;
        } else {
            return 0;
        }
    }

    public function realationAccountServices($acuId = null)
    {
        $sql = "SELECT mod_sv_id, mod_sv_name FROM mod_accounts_services,mod_services WHERE mod_acu_sv_acu_id='" . $acuId . "' AND mod_acu_sv_sv_id=mod_sv_id AND mod_sv_state > 0 ORDER BY mod_acu_sv_order ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $svId = $row["mod_sv_id"];
                $return[$i]["id"] = $svId;
                $return[$i]["name"] = $row["mod_sv_name"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    //invoices
    public function createInvoiceData(array $var = null)
    {
        $acuId = $var['acuId'];
        $razonSocial = $var['razonSocial'];
        $nit = $var['nit'];
        $invoiceData = $this->haveInvoiceData($acuId, $nit);
        if ($invoiceData == 0) {
            $insert = 'mod_acu_invd_acu_id,mod_acu_invd_razon_social,mod_acu_invd_nit,mod_acu_invd_order';
            //return $this->invoiceLastOrder($acuId);
            $values = "'" . $acuId . "','" . $razonSocial . "','" . $nit . "','" . $this->invoiceLastOrder($acuId) . "'";
            $sql = "insert into mod_accounts_invoices_data (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);

            $sql = "select max(mod_acu_invd_id) as id from mod_accounts_invoices_data";
            $rs = $this->fmt->querys->consult($sql, __METHOD__);
            $row = $this->fmt->querys->row($rs);
            return $row["id"];
        } else {
            return $invoiceData;
        }
    }

    public function haveInvoiceData(int $acuId = null, int $nit = null)
    {
        $sql = "SELECT mod_acu_invd_id FROM mod_accounts_invoices_data WHERE  mod_acu_invd_nit='" . $nit . "' AND mod_acu_invd_acu_id='" . $acuId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row['mod_acu_invd_id'];
        } else {
            return 0;
        }
    }

    public function invoiceLastOrder(int $acuId = null)
    {
        $sql = "SELECT mod_acu_invd_order FROM mod_accounts_invoices_data WHERE  mod_acu_invd_acu_id='" . $acuId . "' ORDER BY mod_acu_invd_order ASC LIMIT 0,1";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row['mod_acu_invd_order'];
        } else {
            return 0;
        }
    }

    public function fullName($acuId, $lastname = "0")
    {
        $sql = "SELECT mod_acu_name, mod_acu_fathers_lastname,mod_acu_mothers_lastname FROM mod_accounts_users WHERE mod_acu_id='" . $acuId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $last = $row["mod_acu_fathers_lastname"] . " " . $row["mod_acu_mothers_lastname"];
            if ($lastname == "1") {
                $lastname = $row["mod_acu_fathers_lastname"];
            } else {
                $lastname = $last;
            }
            return $row["mod_acu_name"] . " " . $lastname;
        } else {
            return "";
        }
    }

    public function registerSignUp(array $var = null)
    {
        //return $var;
        $vars = $var["vars"];
        $company = $vars["inputCompany"] ? $vars["inputCompany"] : "";
        $entitieId = $vars["entitieId"];
        $name = $vars["inputName"];
        $pw = base64_encode($vars["inputPassword"]);
        $arrayLastName = explode(" ", $vars["inputLastname"]);
        $fathersLastname = $arrayLastName[0];
        $mothersLastname = $arrayLastName[1];
        $gender = $vars["inputGender"] ? $vars["inputGender"] : "";
        $email = $vars["inputEmail"];
        $type = $vars["type"];
        $locale = $vars["locale"];
        $timezone = $vars["timezone"];
        $birdaydate = $vars["inputDay"] . "-" . $vars["inputMonth"] . "-" . $vars["inputYear"];

        if ($birdaydate == "0-0-0") {
            $birdaydate = "";
        }

        

        $vari["entitieId"] = $entitieId;
        $vari["vars"] = array(
            "name" => $name,
            "fathersLastname" => $fathersLastname,
            "mothersLastname" => $mothersLastname,
            "gender" => $gender,
            "email" => $email,
            "type" => $type,
            "password" => $pw,
            "locale" => $locale,
            "timezone" => $timezone,
            "birthday" => $birdaydate
        );

        //return $vari;

        $acuId = $this->addAccount($vari);

        if (!is_int($acuId) || $acuId == 0) {
            $rtn["Error"] = 1;
            $rtn["status"] = "error";
            $rtn["message"] = "Error al crear la cuenta. ".$acuId["description"];
            $rtn["data"] = $acuId;
            return $rtn;
        }
        //if ($acuId != 0){

        // return "acu:".$acuId ;

        /* $vars["acuId"] = $acuId;
        $vars["entitieId"] = $entitieId;
        /* $cpeId = $this->createCustomersPersons($vars);
        if (!is_int($cpeId))
            return 0; 

        $rol = $this->addRoleToAccount(["acuId" => $acuId, "rolId" => 1, "entitieId" => $entitieId]);
        //return "rol:".$rol;

        if (!is_int($rol))
            return 0;

        //$vars["cpeId"] = $cpeId;
        //$vars["inputUsername"] = $company;

        //return $cpeId."-".$company;

        if (!empty($company)) {
            $cenId = $this->createCustomersEnterprises($vars);
            if (!is_int($cenId))
                return 0;
        } */

        $code = $this->getCodeAccount($acuId);

        $rtn["Error"] = 0;
        $rtn["status"] = "success";
        $rtn["message"] = "send";
        $rtn["data"]["name"] = $name;
        $rtn["data"]["lastname"] = $fathersLastname." ".$mothersLastname;
        $rtn["data"]["email"] = $email;
        $rtn["data"]["code"] = $code;

        return $rtn;
    }

    public function getCodeAccount($acuId)
    {
        //return $var;
        $sql = "SELECT mod_acu_code FROM mod_accounts_users WHERE mod_acu_id='" . $acuId . "' ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_acu_code"];
        } else {
            return 0;
        }
        
    }

    public function createCustomersPersons(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $acuId = $var["acuId"];
        $name = $var["inputName"];
        $lastname = $var["inputLastname"];
        $email = $var["inputEmail"];
        $code = $var["code"] ? $var["code"] : 'MCP' . $this->codeCustomersPersons();

        $today = $this->fmt->modules->dateFormat();
        $userId = $var["userId"] ? $var["userId"] : 0;

        $insert = "mod_cpe_acu_id,
        mod_cpe_name,
        mod_cpe_lastname,
        mod_cpe_email,
        mod_cpe_code,
        mod_cpe_register_date,
        mod_cpe_user_id,
        mod_cpe_ent_id,
        mod_cpe_state";
        $values = "'" . $acuId . "','" . $name . "','" . $lastname . "','" . $email . "','" . $code . "','" . $today . "','" . $entitieId . "','" . $userId . "','1'";
        ;
        $sql = "insert into mod_customers_persons (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_cpe_id) as id from mod_customers_persons";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        return $row["id"];
    }

    public function createCustomersEnterprises(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"] ? $var["entitieId"] : 1;
        $cpeId = $var["cpeId"];
        $name = $var["inputName"];
        $username = $var["inputUsername"];
        $description = $var["inputDescription"] ? $var["inputDescription"] : "";
        $img = $var["inputImg"] ? $var["inputImg"] : "";
        $code = $var["code"] ? $var["code"] : 'N' . $this->codeCustomersPersons();

        $today = $this->fmt->modules->dateFormat();
        $userId = $var["userId"] ? $var["userId"] : 0;

        if ($username == "") {
            return 0;
        }

        $insert = "mod_cen_cpe_id,
        mod_cen_name,
        mod_cen_username,
        mod_cen_code,
        mod_cen_description,
        mod_cen_img,
        mod_cen_register_date,
        mod_cen_user_id,
        mod_cen_ent_id,
        mod_cen_state";
        $values = "'" . $cpeId . "','" . $name . "','" . $username . "','" . $code . "','" . $description . "','" . $img . "','" . $today . "','" . $entitieId . "','" . $userId . "','1'";
        ;
        $sql = "insert into mod_customers_enterprises (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(mod_cen_id) as id from mod_customers_enterprises";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        return $row["id"];
    }

    public function emailExists(string $email = null)
    {
        $sql = "SELECT mod_acu_id FROM mod_accounts_users WHERE mod_acu_email='" . $email . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function companyExists(array $var = null)
    {
        $username = $var["company"];

        if ($this->fmt->validations->username($username)) {
            $sql = "SELECT mod_cen_id FROM mod_customers_enterprises WHERE mod_cen_username='" . $username . "'";
            $rs = $this->fmt->querys->consult($sql, __METHOD__);
            $num = $this->fmt->querys->num($rs);
            if ($num > 0) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return "error";
        }


    }

    public function registerPassword(string $key = null, string $password = null)
    {
        //return $key.":".$password;

        $key = base64_decode($key);
        $array = explode("&", $key);
        $email = $array[0];
        $code = $array[1];
        $pw = base64_encode($password);

        if (empty($email) || empty($code)) {
            return 0;
        }

        $sql = "UPDATE mod_accounts_users  SET
               mod_acu_password='" . $pw . "'
               WHERE  mod_acu_email='" . $email . "' AND mod_acu_code='" . $code . "' AND mod_acu_state > 0";
        $this->fmt->querys->consult($sql);

        //register
        //$dataRegister = $this->userValidate(array("email"=>$email,"password"=>$password));
        $rtn["Error"] = 0;
        $rtn["email"] = $email;
        return $rtn;

    }

    public function activateAccountCode(string $key = null)
    {
        $key = base64_decode($key);
        $array = explode("&", $key);
        $email = $array[0];
        $code = $array[1];

        if (empty($email) || empty($code)) {
            return 0;
        }

        $sql = "SELECT mod_acu_id FROM mod_accounts_users WHERE mod_acu_email='" . $email . "' AND mod_acu_code='" . $code . "' AND mod_acu_state='0' ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $acuId = $row["mod_acu_id"];
            return $this->updateData($acuId, "mod_acu_state", "1");
        } else {
            return 0;
        }
    }

    public function loginSignIn(array $var = null)
    {
        $vars = $var["vars"];

        return $this->userValidate($vars);
    }

    public function setLevelAccount(array $var = null)
    {
        //return $var;
        $acuId = $var["acuId"];
        $level = $var["level"];
        //$entitieId = $var["entitieId"];

        $sql = "UPDATE mod_accounts_users SET
               mod_acu_level='" . $level . "'
               WHERE mod_acu_id='" . $acuId . "' AND mod_acu_state = '1'";
        $this->fmt->querys->consult($sql);

        return $level;
    }

    public function checkEmailType(string $email, string $type)
    {
        //return $email.",".$type;
        if ($email == null) {
            return "error";
        }
        $sql = "SELECT * FROM mod_accounts_users WHERE mod_acu_email='" . $email . "' AND mod_acu_type='" . $type . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_acu_id"];
        } else {
            return 0;
        }
    }

    public function checkType(string $email)
    {
        //return $email.",".$type;
        $sql = "SELECT mod_acu_type FROM mod_accounts_users WHERE mod_acu_email='" . $email . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_acu_type"];
        } else {
            return 0;
        }
    }

    public function emaintToId(string $email)
    {
        //return $email.",".$type;
        $sql = "SELECT mod_acu_id FROM mod_accounts_users WHERE mod_acu_email='" . $email . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_acu_id"];
        } else {
            return 0;
        }
    }

    public function checkCodeSocial(string $email)
    {
        //return $email.",".$type;
        $sql = "SELECT mod_acu_code FROM mod_accounts_users WHERE mod_acu_email='" . $email . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_acu_code"];
        } else {
            return 0;
        }
    }

    public function checkBirthday(string $email)
    {
        //return $var;
        $sql = "SELECT mod_acu_birthday_date,mod_acu_id FROM mod_accounts_users WHERE mod_acu_email='" . $email . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $date = $row["mod_acu_birthday_date"];

            if ($date == "" || $date == "0000-00-00") {
                return 0;
            } else {
                return $row["mod_acu_id"];
            }
        } else {
            return 0;
        }

    }

    public function createAccountSocial(array $var = null)
    {
        //return $var;

        $vars = $var["vars"];
        $email = $vars["email"];
        $type = $vars["type"];
        $code = $vars["code"];
        $token = $vars["token"];
        $img = $vars["img"];
        $locale = $vars["locale"];
        $timezone = $vars["timezone"];
        $mode = $vars["mode"];



        if ($email != null && !empty($email)) {
            //return "ingresamos mail";
            if (!$this->emailExists($email)) {
                //create account

                $name = $vars["name"];
                $pw = "";
                $arrayLastName = explode(" ", $vars["lastname"]);
                $fathersLastname = $arrayLastName[0];
                $mothersLastname = $arrayLastName[1];
                $gender = '';


                $birdaydate = '';
                $vari["entitieId"] = $vars["entitieId"];

                $vari["vars"] = array(
                    "name" => $name,
                    "fathersLastname" => $fathersLastname,
                    "mothersLastname" => $mothersLastname,
                    "gender" => $gender,
                    "email" => $email,
                    "type" => $type,
                    "code" => $code,
                    "password" => $pw,
                    "token" => $token,
                    "img" => $img,
                    "locale" => $locale,
                    "timezone" => $timezone,
                    "birdaydate" => $birdaydate,
                    "state" => 1
                );

                $stateAcu = $this->addAccount($vari);

                if ($mode == "business") {
                    $error = 3;
                    $message = 'error, ask for name bussiness';
                } else {
                    $error = 0;
                    $message = 'ok create bussiness' . $stateAcu;
                }

                if (!is_array($stateAcu)) {
                    $rtn["Error"] = $error;
                    $rtn["status"] = "success";
                    $rtn["message"] = $message . " message";

                    $arrayData = $this->dataId($stateAcu);
                    $response["email"] = $email;
                    $response["password"] = base64_decode($arrayData["mod_acu_password"]);
                    $response["entitieId"] = $vars["entitieId"] || 1;
                    return $this->userValidate($response);

                } else {
                    //Si hay algun error;
                    $rtn = $stateAcu;
                    $rtn["Error"] = 1;
                    $rtn["status"] = "success";
                    $rtn["id"] = $stateAcu;
                    $rtn["message"] = $message . " message";
                }

                return $rtn;

            } else {
                //account exist
                //return "account exist";
                $stateBirthday = $this->checkBirthday($email);
                $stateType = $this->checkType($email);
                $acuId = $this->emaintToId($email);
                $cpeId = $this->checkCustomerPerson($acuId);
                $cenId = $this->checkCustomerEnterprise($cpeId);

                //return $stateType." type:".$type." mode:".$mode;

                if ($stateType != $type) {
                    $sql = "UPDATE mod_accounts_users SET
                            mod_acu_type='" . $type . "',
                            mod_acu_imagen='" . $img . "',
                            mod_acu_token='" . $token . "',
                            mod_acu_code='" . $code . "'
                            WHERE mod_acu_id= '" . $acuId . "' ";
                    $this->fmt->querys->consult($sql);
                } else {
                    if (!$this->checkCodeSocial($email) == $code) {
                        $sql = "UPDATE mod_accounts_users SET
                            mod_acu_imagen='" . $img . "',
                            mod_acu_token='" . $token . "',
                            mod_acu_code='" . $code . "'
                        WHERE mod_acu_id= '" . $acuId . "' ";
                        $this->fmt->querys->consult($sql);
                    }
                }


                if ($mode == "business") {
                    if ($cenId) {
                        $arrayData = $this->dataId($acuId);
                        $response["email"] = $email;
                        $response["password"] = base64_decode($arrayData["mod_acu_password"]);
                        $response["entitieId"] = $vars["entitieId"] || 1;
                        return $this->userValidate($response);
                    } else {
                        $rtn["Error"] = 3;
                        $rtn["id"] = $acuId;
                        $rtn["message"] = "error, ask for name bussiness";
                        return $rtn;
                    }
                }

                if ($stateBirthday || $mode == "direct") {
                    //cuentaCompleta
                    //logearse
                    $arrayData = $this->dataId($acuId);
                    $response["email"] = $email;
                    $response["password"] = base64_decode($arrayData["mod_acu_password"]);
                    $response["entitieId"] = $vars["entitieId"] || 1;
                    return $this->userValidate($response);
                } else {
                    $rtn["Error"] = 2;
                    $rtn["status"] = "success";
                    $rtn["id"] = $acuId;
                    $rtn["message"] = "error, ask for birthday";
                    return $rtn;
                }


            }

        } else {
            $rtn["Error"] = 1;
            $rtn["message"] = "error, email no exist";
            return $rtn;
        }
    }

    public function createAccountUser(array $var = null)
    {
        //return $var;
        $vars = $var;
        $email = $vars["email"];
        $type = $vars["type"];
        $code = $vars["code"];
        $token = $vars["token"];
        $celular = $vars["celular"];
        $referred = $vars["referred"];
        $img = $vars["img"];
        $locale = $vars["locale"];
        $timezone = $vars["timezone"];
        $mode = $vars["mode"];
        $state = $vars["state"] ? $vars["state"] : 0;



        if ($email != null && !empty($email)) {
            //return "ingresamos mail";
            if (!$this->emailExists($email)) {
                //create account

                $name = $vars["name"];
                $pw = "";
                $arrayLastName = explode(" ", $vars["lastname"]);
                $fathersLastname = $arrayLastName[0];
                $mothersLastname = $arrayLastName[1];
                $gender = '';


                $birdaydate = '';
                $vari["entitieId"] = $vars["entitieId"];

                $vari["vars"] = array(
                    "name" => $name,
                    "fathersLastname" => $fathersLastname,
                    "mothersLastname" => $mothersLastname,
                    "gender" => $gender,
                    "email" => $email,
                    "type" => $type,
                    "celular" => $celular,
                    "referred" => $referred,
                    "code" => $code,
                    "password" => $pw,
                    "token" => $token,
                    "img" => $img,
                    "locale" => $locale,
                    "timezone" => $timezone,
                    "birdaydate" => $birdaydate,
                    "state" => $state
                );

                return $this->addAccount($vari);



            } else {
                $rtn["Error"] = 1;
                $rtn["message"] = "error, email exist";
                return $rtn;
                /*  //account exist
                 //return "account exist";
                 $stateBirthday = $this->checkBirthday($email);
                 $stateType = $this->checkType($email);
                 $acuId = $this->emaintToId($email);
                 $cpeId = $this->checkCustomerPerson($acuId);
                 $cenId = $this->checkCustomerEnterprise($cpeId);

                 //return $stateType.":".$type;

                 if ($stateType != $type) {
                     $sql = "UPDATE mod_accounts_users SET
                             mod_acu_type='" . $type . "',
                             mod_acu_imagen='" . $img . "',
                             mod_acu_token='" . $token . "',
                             mod_acu_code='" . $code . "'
                             WHERE mod_acu_id= '" . $acuId . "' ";
                     $this->fmt->querys->consult($sql);
                 } else {
                     if (!$this->checkCodeSocial($email) == $code) {
                         $sql = "UPDATE mod_accounts_users SET
                             mod_acu_imagen='" . $img . "',
                             mod_acu_token='" . $token . "',
                             mod_acu_code='" . $code . "'
                         WHERE mod_acu_id= '" . $acuId . "' ";
                         $this->fmt->querys->consult($sql);
                     }
                 }


                 if ($mode == "business") {
                     if ($cenId) {
                         $arrayData = $this->dataId($acuId);
                         $response["email"] = $email;
                         $response["password"] = base64_decode($arrayData["mod_acu_password"]);
                         return $this->userValidate($response);
                     } else {
                         $rtn["Error"] = 3;
                         $rtn["id"] = $acuId;
                         $rtn["message"] = "error, ask for name bussiness";
                         return $rtn;
                     }
                 }

                 if ($stateBirthday || $mode == "direct") {
                     //cuentaCompleta
                     //logearse
                     $arrayData = $this->dataId($acuId);
                     $response["email"] = $email;
                     $response["password"] = base64_decode($arrayData["mod_acu_password"]);
                     return $this->userValidate($response);
                 } else {
                     $rtn["Error"] = 2;
                     $rtn["id"] = $acuId;
                     $rtn["message"] = "error, ask for birthday";
                     return $rtn;
                 } */


            }


        } else {
            $rtn["Error"] = 1;
            $rtn["message"] = "error, email no exist";
            return $rtn;
        }
    }

    public function checkCustomerPerson(string $acuId = null)
    {
        $sql = "SELECT * FROM mod_customers_persons WHERE mod_cpe_acu_id='" . $acuId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }

    }

    public function checkCustomerEnterprise(string $cpeId = null)
    {
        //return $var;
        $sql = "SELECT * FROM mod_customers_enterprises WHERE mod_cen_cpe_id='" . $cpeId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }

    }

    public function sendForgot(array $var = null)
    {
        //return $var;
        $inputEmail = $var["inputEmail"];
        

        $sql = "SELECT * FROM mod_accounts_users WHERE mod_acu_email='" . $inputEmail . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);

            $user_name = $row["mod_acu_name"];
            $user_fathersLastname = $row["mod_acu_fathersLastname"];
            $user_email = $row["mod_acu_email"];
            $code = $row["mod_acu_code"];

            $this->sendMailCode(
                array(
                    "name" => $user_name,
                    "lastname" => $user_fathersLastname,
                    "email" => $user_email,
                    "key" => $code
                )
            );

            return 1;
        } else {
            return 0;
        }
    }

    public function registerSocialOthers(array $var = null)
    {

        $vars = $var["vars"];
        $item = $vars["item"];
        $birthday = $vars["birthday"];
        $gender = $vars["gender"];
        $genderCustom = $vars["genderCustom"];
        $email = $vars["email"];
        $code = $vars["code"];

        if (empty($gender) && !empty($genderCustom)) {
            $gender = $genderCustom;
        }

        $sql = "SELECT mod_acu_id FROM mod_accounts_users WHERE mod_acu_id='" . $item . "' AND mod_acu_email='" . $email . "' AND mod_acu_code='" . $code . "' ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {

            /* $sql = "UPDATE mod_accounts_users SET
                    mod_acu_birthday_date='" . $birthday. "',
                    mod_acu_gender = '".$gender."';  */
            $sql = "UPDATE mod_accounts_users SET
                   mod_acu_birthday_date='" . $birthday . "',
                   mod_acu_gender = '" . $gender . "'
                   WHERE mod_acu_id= '" . $item . "' ";
            $this->fmt->querys->consult($sql);

            $arrayData = $this->dataId($item);
            $response["email"] = $email;
            $response["password"] = base64_decode($arrayData["mod_acu_password"]);
            $response["entitieId"] = $vars["entitieId"];
            return $this->userValidate($response);

        } else {
            $rtn["Error"] = 1;
            $rtn["mensaje"] = "error, account not exist";
        }

        return $rtn;
    }

    public function registerSocialBusiness(array $var = null)
    {
        //return $var;
        $vars = $var["vars"];
        $company = $vars["company"];
        $item = $vars["item"];
        $birthday = $vars["birthday"];
        $gender = $vars["gender"];
        $genderCustom = $vars["genderCustom"];
        $email = $vars["email"];
        $code = $vars["code"];
        $entitieId = $vars["entitieId"];

        if (empty($gender) && !empty($genderCustom)) {
            $gender = $genderCustom;
        }

        $sql = "SELECT mod_acu_id FROM mod_accounts_users WHERE mod_acu_id='" . $item . "' AND mod_acu_email='" . $email . "' AND mod_acu_code='" . $code . "' ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {

            /* $sql = "UPDATE mod_accounts_users SET
                    mod_acu_birthday_date='" . $birthday. "',
                    mod_acu_gender = '".$gender."';  */
            $sql = "UPDATE mod_accounts_users SET
                    mod_acu_birthday_date='" . $birthday . "',
                    mod_acu_gender = '" . $gender . "'
                    WHERE mod_acu_id= '" . $item . "' ";
            $this->fmt->querys->consult($sql);

            $acuId = $item;

            if (is_int($acuId))
                return 0;
            //if ($acuId != 0){
            $arrayData = $this->dataId($item);

            $vari["acuId"] = $acuId;
            $vari["inputName"] = $arrayData["mod_acu_name"];
            $vari["inputLastname"] = $arrayData["mod_acu_fathers_lastname"] . " " . $arrayData["mod_acu_mothers_lastname"];
            $vari["inputEmail"] = $email;
            $vari["entitieId"] = $entitieId;

            $cpeId = $this->createCustomersPersons($vari);
            if (!is_int($cpeId))
                return 0;

            $vars["cpeId"] = $cpeId;
            $vars["inputUsername"] = $company;
            $vars["entitieId"] = $entitieId;
            $cenId = $this->createCustomersEnterprises($vars);
            if (!is_int($cenId))
                return 0;

            $rol = $this->addRoleToAccount(["acuId" => $acuId, "rolId" => 1, "entitieId" => $entitieId]);
            if (!is_int($rol))
                return 0;

            $newDb = _DATA_BASE_SUBFIX . $cenId;
            $this->fmt->querys->createDb($newDb);

            $querysDb = $this->fmt->querys->loadDbQuerys([
                "dbname" => $newDb,
                "path" => _DB_BASE_PATH . "db_base.sql"
            ]);

            if ($querysDb["Error"] == 1) {
                $rtn["Error"] = 1;
                $rtn["mensaje"] = "error, load query db_base.sql";
                return $rtn;
            }


            $response["email"] = $email;
            $response["password"] = base64_decode($arrayData["mod_acu_password"]);
            $response["entitieId"] = $vars["entitieId"];
            return $this->userValidate($response);

        } else {
            $rtn["Error"] = 1;
            $rtn["mensaje"] = "error, account not exist";
        }

        return $rtn;
    }

    public function addRoleToAccount(array $var = null)
    {
        //return $var;
        $acuId = $var["acuId"];
        $rolId = $var["rolId"];
        $entitieId = $var["entitieId"] ? $var["entitieId"] : 1;

        $insert = "mod_acu_user_rol_acu_id,
        mod_acu_user_rol_rol_id,
        mod_acu_user_rol_ent_id,
        mod_acu_user_rol_order";
        $values = "'" . $acuId . "','" . $rolId . "','" . $entitieId . "','0'";
        $sql = "insert into mod_accounts_users_roles (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        return 1;
    }

    public function checkActivation(array $var = null)
    {
        //return $var;
        $email = $var["email"];
        $code = $var["code"];

        $sql = "SELECT mod_acu_id FROM mod_accounts_users WHERE mod_acu_email='" . $email . "' AND mod_acu_code='" . $code . "' AND mod_acu_state='0' ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["mod_acu_id"];
        } else {
            return 0;
        }

    }

    public function activationUserUpdate(array $var = null)
    {
        //return $var;
        $email = $var["email"];
        $code = $var["code"];

        $sql = "SELECT mod_acu_id, mod_acu_name, mod_acu_fathers_lastname FROM mod_accounts_users WHERE mod_acu_email='" . $email . "' AND mod_acu_code='" . $code . "' AND mod_acu_state='0' ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $acuId = $row["mod_acu_id"];
            $sql1 = "UPDATE mod_accounts_users SET
                    mod_acu_state='1'
                    WHERE mod_acu_id = '" . $acuId . "' ";
            $this->fmt->querys->consult($sql1);

            $rtn["Error"] = 0;
            $rtn["status"] = "success";
            $rtn["message"] = "Cuenta activada";
            $rtn["data"]["id"] = $acuId;
            $rtn["data"]["name"] = $row["mod_acu_name"];
            $rtn["data"]["lastname"] = $row["mod_acu_fathers_lastname"];
            return $rtn;
        } else {
            return 0;
        }

    }

    public function reviewCode(array $var = null)
    {
        //return $var;

        $token = $var["tokenSession"]["clientId"];

        if ($token != "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9-nucleo") {
            return 0;
        }

        $code = $var["vars"]["inputs"]["code"];

        $sql = "SELECT * FROM mod_accounts_users WHERE mod_acu_token = '" . $code . "' ";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            $row = $this->fmt->querys->row($rs);

            $sql = "UPDATE mod_accounts_users SET
                    mod_acu_state ='1'
                    WHERE  mod_acu_token = '" . $code . "' ";
            $this->fmt->querys->consult($sql);


            $response["email"] = $row["mod_acu_email"];
            $response["password"] = base64_decode($row["mod_acu_password"]);
            $response["entitieId"] = 1;

            $rtn["Error"] = 0;
            $rtn["status"] = "success";
           // $rtn["response"]=  $response;
            $rtn["data"]= $this->userValidate($response);
            $rtn["message"] = "Cuenta activada";

            

            return $rtn;

        } else {
            return 0;
        }
        
    }

}