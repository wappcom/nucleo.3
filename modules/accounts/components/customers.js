import {
	empty,
	replacePath,
	replaceEssentials,
	replaceAll,
	loadView,
	stopLoadingBar,
	emptyReturn,
	loadingBtnIcon,
	btnLoadingRemove,
	unDisableId,
	accessToken,
	removeBtnLoading,
	alertMessageError,
	dataModule,
	alertPage,
	addHtml
} from "../../components/functions.js";


import {
	editorText,
	errorInput,
	loadInitChargeMedia,
	loadItemMedia,
	loadFilesFormLoader,
	loadSingleFileFormLoader,
	loadChargeMediaInit,
	removalMediaItems,
	validateNum,
} from "../../components/forms.js";

import {
	deleteModalItem,
	removeModal,
	closeModal,
} from "../../components/modals.js";
import {
	categorysListTable,
	createTable,
	dataTable,
	removeItemTable,
	inactiveBtnStateItem,
	activeBtnStateItem,
	mountTable,
	colCheck,
	colId,
	colName,
	colTitle,
	colState,
	colActionsBase,
	deleteItemModule,
	renderEmpty,
	mountMacroTable,
	clearMacroTable
} from "../../components/tables.js";
import {
	renderCol,
	renderColCheck,
	renderColId,
	renderColTitle,
	renderRowsTable,
	renderColState,
	renderColActions,
	renderColCategorys,
} from "../../components/renders/renderTables.js";

import {
    innerForm,
    renderDeleteModal,
    renderDeleteModalFn,
    renderModalConfig,
    renderModalConfigBody,
    renderModalClean,
    removeInnerForm
} from "../../components/renders/renderModals.js";

/* renderColTitle,
	renderColState,
	renderColActionsBase, */
let pathurl = "modules/accounts/";
let module = "customers";
let system = "accounts";
let tableId = "tableCustomers";


export const customersIndex = (vars = []) => {
	let moduleReturn = vars.moduleReturn ? vars.moduleReturn : module;
	//console.log("customersIndex", module, system, pathurl);
	loadView(_PATH_WEB_NUCLEO + pathurl + "views/customers.html?" + _VS).then(
		(htmlView) => {
			//console.log(htmlView, module, system);
			getData({
				task: 'getCustomersAll',
				return: 'returnArray', // returnId, returnState, returnArray
			}).then((response) => {
				//console.log('getData getCustomersAll', response);
				if (response.status == 'success') {
					stopLoadingBar();

					let str = htmlView;
					let tableEnterprises = mountMacroTable({
						tableId,
						module,
						data: response.data.cen,
						cols: [{
							label: "id",
							cls: 'colId',
							type: "id",
							render: 'id',
						}, {
							label: "*Nombre",
							cls: 'colName',
							type: "text",
							render: 'name',
							attrName: 'inputName',
							id: 'inputName',
						}, {
							label: "*Username",
							cls: 'colUsername',
							type: "text",
							render: 'username',
							attrName: 'inputUsername',
							id: 'inputUsername',
						}, {
							label: "Code",
							cls: 'colCode',
							type: "text",
							render: 'code',
							attrName: 'inputCode',
							id: 'inputCode',
						}, {
							label: "Razon Social",
							cls: 'colText',
							type: "text",
							render: 'businessName',
							attrName: 'inputBusinessName',
							id: 'inputBusinessName',
						}, {
							label: "Nit",
							cls: 'colText',
							type: "text",
							render: 'nit',
							attrName: 'inputNit',
							id: 'inputNit',
						}, {
							label: "Prioridad",
							cls: 'colBadgePoint',
							type: "select",
							render: 'priority',
							default: '2',
							out: 'label',
							options: [{
								value: '1',
								label: 'Alta',
								clsOut: 'high'
							}, {
								value: '2',
								label: 'Normal',
								clsOut: 'normal'
							}, {
								value: '3',
								label: 'Baja',
								clsOut: 'low'
							}],
							attrName: 'inputPriority',
							id: 'inputPriority',
						}, {
							label: "Tipo Contrato",
							cls: 'colBadgePoint',
							type: "select",
							render: 'typeContract',
							default: '1',
							out: 'label',
							options: [{
								value: '1',
								label: 'Proyecto',
								clsOut: 'proyect'
							}, {
								value: '2',
								label: 'Fee Mensual',
								clsOut: 'monthlyFee'
							}, {
								value: '3',
								label: 'Servicios',
								clsOut: 'services'
							}],
							attrName: 'inputTypeContract',
							id: 'inputTypeContract',
						}, {
							label: "Description",
							cls: 'colDescription',
							type: "text",
							render: 'description',
							attrName: 'inputDescription',
							id: 'inputDescription',
						}, {
							label: "Estado",
							cls: 'colState',
							type: "state",
							render: 'state',
							attrName: 'inputState',
							id: 'inputState',
						}]
					})
					str = replaceAll(str, "{{_TABLE_INNER_ENTERPRISES}}", tableEnterprises);

					$(".innerForm").remove();
					//console.log("str", module)
					addHtml({
						selector: `.bodyModule[module='${moduleReturn}']`,
						type: 'insert', // insert, append, prepend, replace
						content: replaceEssentials({
							str,
							module,
							system,
							pathurl,
							name: dataModule(module, "name"),
							fn: "formNewCustomerCompany",
							btnNameAction: "Nuevo Cliente Empresa",
							icon: dataModule(module, "icon"),
							color: dataModule(module, "color"),
						})
					})

					$(`#${tableId} #inputName`).focus();
				} else {
					alertMessageError({
						message: response.message
					})
				}
			}).catch(console.warn());
		}
	);
};

export const customersCrmIndex = (vars = []) => {
	//console.log('customersCrmIndex', vars);
	let module = "customersCrm";
	let system = "crm";

	customersIndex({
		module,
		system
	});
}

export const loadFormNewCustomerCompany = (vars = []) => {
	//console.log('loadFormNewCustomerCompany',vars);
}

export const delete_selected = (vars = []) => {
	console.log('delete_selected', vars);
	const tableId = vars.tableId;

	let inputs = [];
	let names = [];
	$(`#${tableId} input[type="checkbox"][data-table="${tableId}"][value!=0][data-action="checkselect"]`).each(function () {
		if ($(this).is(':checked')) {
			inputs.push($(this).val());
			names.push($(this).data('name'));
		}
	});
	console.log ('inputs', inputs, names);

	addHtml({
		selector: "body",
		type: "append",
		content: renderDeleteModalFn({
			name: names.join(", "),
			id: "modalDelete",
			module,
			attr: `data-module="${module}" data-id="${inputs.join(",")}"`,
		}),
	});
}
  
export const getData = async (vars = []) => {
	//console.log('getData', vars);
	const url = _PATH_WEB_NUCLEO + "modules/accounts/controllers/apis/v1/customers.php";
	let data = JSON.stringify({
		accessToken: accessToken(),
		vars: JSON.stringify(vars)
	});

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: data
		})
		//console.log(res.data);
		return res.data
	} catch (error) {
		console.log("Error: getDataPlaces")
		console.log(error)
	}
}

document.addEventListener("DOMContentLoaded", function () {
	$("body").on("keyup", `#${tableId} input`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		//console.log(e.key);

		if (e.key == "Enter") {
			let name = $(`#${tableId} #inputName`).val();
			let username = $(`#${tableId} #inputUsername`).val();
			let code = $(`#${tableId} #inputCode`).val();
			let businessName = $(`#${tableId} #inputBusinessName`).val();
			let nit = $(`#${tableId} #inputNit`).val();
			let description = $(`#${tableId} #inputDescription`).val() || "";
			let priority = $(`#${tableId} #inputPriority`).val();
			let typeContract = $(`#${tableId} #inputTypeContract`).val();
			let state = $(`#${tableId} #inputState`).is(':checked') ? 1 : 0;


			//let state = $(`#${tableId} #inputState`).val() ;
			let count = 0;

			if (name == "" && username == "" && code == "") {
				alertMessageError({
					message: "Debe ingresar al menos un criterio de busqueda"
				})
				count++;
			}

			if (nit) {
				if (!validateNum(nit)) {
					alertMessageError({
						message: "El nit debe ser númerico"
					})
					count++;
				}
			}

			if (count == 0) {
				let inputs = {
					name,
					username,
					code,
					businessName,
					nit,
					description,
					priority,
					typeContract,
					state
				}

				getData({
					task: 'addCustomerCompany',
					return: 'returnObject', // returnId, returnState, returnArray, returnMessage, returnObject
					inputs
				}).then((responseData) => {
					console.log('getData addCustomerCompany', responseData, tableId);
					if (responseData.status == 'success') {
						let data = responseData.data;
						let priorityLabel = $(`#${tableId} #inputPriority option:selected`).text();
						let typeContractLabel = $(`#${tableId} #inputTypeContract option:selected`).text();
						$(`#${tableId} table tbody:first tr:first`).after(`<tr rowId="${data.id}">
                            <td class="colCheck">
                                <input type="checkbox" class="inputCheckBox" data-id="${data.id}">
                            </td>
                            <td class="colId">${data.id}</td>
                            <td class="colName">${name}</td>
                            <td class="colText">${username}</td>
                            <td class="colText">${code}</td>
                            <td class="colText">${businessName}</td>
                            <td class="colText">${nit}</td>
                            <td class="colBadgePoint">${priorityLabel}</td>
                            <td class="colBadgePoint">${typeContractLabel}</td>
                            <td class="colText">${description}</td>
                            <td class="colState">
                                <input type="checkbox" class="tableInput" 
                                    data-name="state" 
                                    data-table="${tableId}" 
                                    data-module="${module}" 
                                    data-id="${data.id}" 
                                    value="1" 
                                    ${state === 1 ? 'checked' : ''}>
                            </td>
                        </tr>`);
						clearMacroTable('#' + tableId);
					} else {
						alertMessageError({
							message: response.message
						})
					}
				}).catch(console.warn());
			}
		}

	});

	/* $("body").on("click", `.btnDeleteItemPerson`, function (e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		let data = $(this).data();
		console.log(`.btnDeleteItemPerson`, data);
		addHtml({
			selector: "body",
			type: "append",
			content: renderDeleteModalFn({
				name: data.name,
				id: "modalDelete",
				module,
				attr: `data-module="${module}" data-id="${data.id}"`,
			}),
		});
	}); */
});

document.addEventListener('click', function (e) {

	if (e.target.matches(".btnAddCustomerCompany")) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		loadFormNewCustomerCompany();
	}
});