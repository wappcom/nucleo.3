import {
	empty,
	replacePath,
	replaceEssentials,
	replaceAll,
	loadView,
	stopLoadingBar,
	emptyReturn,
	loadingBtnIcon,
	btnLoadingRemove,
	unDisableId,
	accessToken,
	removeBtnLoading,
	alertMessageError,
	dataModule,
	alertPage,
} from "../../components/functions.js";


import {
	editorText,
	errorInput,
	loadInitChargeMedia,
	loadItemMedia,
	loadFilesFormLoader,
	loadSingleFileFormLoader,
	loadChargeMediaInit,
	removalMediaItems,
} from "../../components/forms.js";

import {
	deleteModalItem,
	removeModal,
	closeModal,
} from "../../components/modals.js";
import {
	categorysListTable,
	createTable,
	dataTable,
	removeItemTable,
	inactiveBtnStateItem,
	activeBtnStateItem,
	mountTable,
	colCheck,
	colId,
	colName,
	colTitle,
	colState,
	colActionsBase,
	deleteItemModule,
	renderEmpty
} from "../../components/tables.js";
import {
	renderCol,
	renderColCheck,
	renderColId,
	renderColTitle,
	renderRowsTable,
	renderColState,
	renderColActions,
	renderColCategorys,
} from "../../components/renders/renderTables.js";
import {
	innerForm
} from "../../components/renders/renderModals.js";

/* renderColTitle,
	renderColState,
	renderColActionsBase, */
let pathurl = "modules/accounts/";
var module = "accountsUsers";
let system = "accounts";
let tableId = "tableAccountsUsers";


export const accountsUsersIndex = (vars = []) => {

	const { module,system,pathurl} = vars || {};
	//console.log("accountsUsersIndex");
	loadView(_PATH_WEB_NUCLEO + "modules/accounts/views/accountsUsers.html?" + _VS).then(
		(htmlView) => {
			//console.log(htmlView, module, system);
			loadAccountsUsers()
				.then((response) => {
					//console.log("loadAccountsUsers", response);
					stopLoadingBar();

					$(".innerForm").remove();

					$(".bodyModule[module='" + module + "']").html(
						replaceEssentials({
							str: htmlView,
							module,
							system,
							pathurl,
							name: dataModule(module, "name"),
							fn: "formNewContent",
							color: dataModule(module, "color"),
							icon: dataModule(module, "icon"),
						})
					);

					if (response.Error == 0 && response.items != 0) {

						let rows = "";
						for (let i in response.items) {
							let item = response.items[i];
							//console.log("item", item);
							rows += renderRowsTable({
								id: item.id,
								content: renderColCheck({
										id: item.id,
									}) +
									renderColId({
										id: item.id,
									}) +
									renderColTitle({
										id: item.id,
										title: item.nameFull,
									}) +

									renderCol({
										id: item.id,
										cls: '',
										attr: '',
										data: item.gender,
									}) +
									renderCol({
										id: item.id,
										cls: '',
										attr: '',
										data: item.type,
									}) +
									renderCol({
										id: item.id,
										cls: '',
										attr: '',
										data: item.timezone,
									}) +
									renderCol({
										id: item.id,
										cls: '',
										attr: '',
										data: item.recordDate,
									}) +
									renderColState({
										id: item.id,
										state: item.state,
										module,
										system,
									}) +

									renderColActions({
										id: item.id,
										name: item.title,
										type: "btnEdit,btnDelete",
										fnType: "formEditContent,deleteModuleItem",
										module,
										system,
									}),
							});
						}

						mountTable({
							id: tableId,
							columns: [
								...colCheck,
								...colId,
								{
									label: "Nombre Completo",
									cls: "colNameFull",
								},

								{
									label: "Genero",
									cls: "",
								},
								{
									label: "Tipo/r",
									cls: "",
								},
								{
									label: "Timezone",
									cls: "",
								},
								{
									label: "Fecha Registro",
									cls: "colDareRegister",
								},
								...colState,
								...colActionsBase,
							],
							rows,
							module,
							system,
							container: ".bodyModule[module='" + module + "'] .tbody",
						});

					} else if (response === 0 || response.items == 0) {
						$(".bodyModule[module='" + module + "']  .tbody").html(renderEmpty())
					} else {
						alertPage({
							text: "Error. por favor contactarse con soporte. " +
								response.message,
							icon: "icn icon-alert-warning",
							animation_in: "bounceInRight",
							animation_out: "bounceOutRight",
							tipe: "danger",
							time: "3500",
							position: "top-left",
						})
					}
				})
				.catch(console.warn());
		}
	);
};

//asycs  
export const loadAccountsUsers = async (vars) => {
	//console.log('loadAccountsUsers', vars);
	const url = _PATH_WEB_NUCLEO + "modules/accounts/controllers/apis/v1/accountsUsers.php";
	let data = JSON.stringify({
		accessToken: accessToken(),
		action: "loadAccountsUsers",
		vars: JSON.stringify(vars)
	});

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: data
		})
		//console.log(res.data);
		return res.data
	} catch (error) {
		console.log("Error: loadAccountsUsers")
		console.log(error)
	}
}

export const add = async (vars) => {
	console.log('add', vars);
	const url = _PATH_WEB_NUCLEO + "modules/accounts/controllers/apis/v1/accountsUsers.php";
	let data = JSON.stringify({
		accessToken: accessToken(),
		action: "add",
		vars: JSON.stringify(vars)
	});

	try {
		let res = await axios({
			async: true,
			method: "post",
			responseType: "json",
			url: url,
			headers: {},
			data: data
		})
		console.log(res.data);
		return res.data
	} catch (error) {
		console.log("Error: add")
		console.log(error)
	}
}