<?php
$dataPost = json_decode(file_get_contents("php://input"), true);
//echo json_encode($dataPost); exit(0);
//echo json_encode($dataPost["tokenSession"]["page"]); exit(0);
require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $dataPost["tokenSession"]["page"] . "config.php");
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

require_once(_PATH_NUCLEO . "modules/accounts/models/class/class.accounts.php");
$accounts = new ACCOUNTS($fmt);

header("Content-Type: application/json");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");



switch ($_SERVER["REQUEST_METHOD"]) {

    case 'POST':

        if (!$fmt->auth->validateTokenSession($dataPost["tokenSession"])) {
            errorDefault($fmt, "Error: Token de sesión no válido");
        }
        //echo json_encode($dataPost["tokenAccess"]);exit(0);

        $stateAccount = intval($accounts->validateTokenAccess($dataPost["tokenAccess"]));
        //echo json_encode($stateAccount); exit(0);

        if ($stateAccount) {
            $action = $dataPost["action"];
            $vars["acuId"] = $stateAccount;
            $vars["entitieId"] = $dataPost["tokenAccess"]["entitieId"];

            if ($action == "setLevelAccount") {
                $vars["level"] = $dataPost['level'];
                $fmt->auth->getActionReturnId($accounts->setLevelAccount($vars));
            }

        } else {
            errorDefault($fmt, "Error: Account invalid");
        }

        break;
    default:
        errorDefault($fmt);
        break;
}

function errorDefault($fmt, $error = "Access Auth. Metod request.")
{
    echo $fmt->errors->errorJson([
        "description" => $error,
        "code" => "",
        "lang" => "es"
    ]);
    exit(0);
}
