<?php
header('Content-Type: text/html; charset=utf8');

$dataPost = json_decode(file_get_contents("php://input"), true);

//echo json_encode($dataPost); exit(0);
//echo json_encode($dataPost["tokenSession"]["page"]);exit(0);
$rootConfig = $_SERVER['DOCUMENT_ROOT'] . "/" . $dataPost["tokenSession"]["page"] . "config.php";
require_once ($rootConfig);
//echo json_encode($rootConfig); exit(0);

$configPath =  "/home/wappcom/www/nucleo/nucleo.3/models/class/class.constructor.php";
require_once($configPath);
$fmt = new CONSTRUCTOR();
   

require_once(_PATH_NUCLEO . "modules/accounts/models/class/class.accounts.php");
$accounts = new ACCOUNTS($fmt);

header("Content-Type: application/json");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");



switch ($_SERVER["REQUEST_METHOD"]) {

    case 'POST':
        $action = $dataPost["action"];

        //echo json_encode($action); exit(0);

        if (!$fmt->auth->validateTokenSession($dataPost["tokenSession"])) {
            errorDefault($fmt, "Error: Token de sesión no válido");
        }

        if ($action == "emailExists") {
            $state = $accounts->emailExists($dataPost["email"]);
            echo json_encode($state);
            exit(0);
        }        
        
        if ($action == "companyExists") {
            $state = $accounts->companyExists($dataPost);
            //echo json_encode($state);
            if ($state == "0") {
                $rtn["Error"] = 0;
                $rtn["status"] = "success";
                $rtn["data"] = $state;
                echo json_encode($rtn);
            } else {
                echo $fmt->errors->errorJson([
                    "description" =>  $state,
                    "message" =>  $state,
                    "code" => "",
                    "lang" => "es"
                ]);
            }
            exit(0);
        }

        if ($action == "createAccountSocial") {
            $state = $accounts->createAccountSocial($dataPost);
            echo json_encode($state);
            exit(0);
        }

        if ($action == "sendForgot") {
            //$var = json_decode($_POST['vars'], true);
            $response = $accounts->sendForgot($dataPost["vars"]);

            if ($response == 0) {
                $rtn["Error"] = 1;
                $rtn["status"] = "error";
                $rtn["data"] = $response;
                echo json_encode($rtn);
                exit(0); 
            } elseif ($response == 1 ) {
                $rtn["Error"] = 0;
                $rtn["status"] = "success";
                $rtn["data"] = $response;
                echo json_encode($rtn);
                exit(0); 
            }
            //echo json_encode($response);
            //exit(0); 
            //echo json_encode($action); exit(0);
        }

        if ($action == "registerSignUp") {
            $state = $accounts->registerSignUp($dataPost);
            echo json_encode($state); exit(0);

            /* if ($state != 0) {
                $rtn["Error"] = 0;
                $rtn["state"] = "send";
                $rtn["acuId"] = $state;
                echo json_encode($rtn);
            } else {
                errorDefault($fmt, $state);
            }
            exit(0); */
        }

        if ($action == "registerSocialOthers") {
            $state = $accounts->registerSocialOthers($dataPost);
            echo json_encode($state);
            exit(0);
        } 
        
        if ($action == "registerSocialBusiness") {
            $state = $accounts->registerSocialBusiness($dataPost);
            echo json_encode($state);
            exit(0);
        }

        if ($action == "loginSignIn") {
            $state = $accounts->loginSignIn($dataPost);
            echo json_encode($state);
            exit(0);
        }

        if ($action == "reviewCode") {
            $state = $accounts->reviewCode($dataPost);
            echo json_encode($state);
            exit(0);
        }

        if ($action == "returnSendCode") {
            $state = $accounts->returnSendCode($dataPost);
            echo json_encode($state);
            exit(0);
        }

        break;

    default:
        errorDefault($fmt);
        break;
}

function errorDefault($fmt, $error = "Access Auth. Metod request.")
{
    echo $fmt->errors->errorJson([
        "description" => $error,
        "message" => $error,
        "code" => "",
        "lang" => "es"
    ]);
    exit(0);
}