<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $_POST["page"] . "config.php");

require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

require_once(_PATH_NUCLEO . "modules/accounts/models/class/class.accounts.php");
$accounts = new ACCOUNTS($fmt);

require_once(_PATH_NUCLEO . "modules/logistics/models/class/class.logistics.php");
$logistics = new LOGISTICS($fmt);

header("Content-Type: application/json");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

switch ($_SERVER["REQUEST_METHOD"]) {
    case 'POST':

        $grant_type = $fmt->auth->getInput('grant_type');
        $bearer_token = $fmt->auth->getInput('authentication');
        $clientId = $fmt->auth->getInput('client_id');
        $clientSecret = $fmt->auth->getInput('client_secret');
        $entitieId = $fmt->auth->getInput('entitieId');

        $api = $fmt->auth->getApi();
        $accessToken = $fmt->auth->getInput('access_token');
        $refreshToken = $fmt->auth->getInput('refresh_token');
        $action = $fmt->auth->getInput('actions');

        if ($action == "loadUser") {
            $var = json_decode($_POST['vars'], true);
            $acuId = $accounts->validateUserToken(array("access_token" => $accessToken, "refresh_token" => $refreshToken, "entitieId" => $entitieId));
            $user = $accounts->dataId($acuId);

            if ($acuId) {
                $userName = $user["mod_acu_name"];
                $userLastname = $user["mod_acu_fathers_lastname"] . " " . $user["mod_acu_mothers_lastname"];
                $return["Error"] = 0;
                $return["userName"] = $userName;
                $return["userLastname"] = $userLastname;
                $return["initial"] = $fmt->users->initial($userName . " " . $userLastname);
                $return["userImg"] = $fmt->emptyReturn($user["mod_acu_imagen"], '');
                $addresses = $accounts->addresses($acuId);
                $return["addresses"] = $addresses;
                $return["celular"] = $user["mod_acu_celular"];
                $return["email"] = $user["mod_acu_email"];
                $return["ci"] = $user["mod_acu_ci"];
                $return["invoiceData"] = $accounts->invoiceData($acuId);

                echo json_encode($return);
                exit(0);
            } else {
                echo $fmt->errors->errorJson([
                    "description" => "Error return Load User Token",
                    "code" => "",
                    "lang" => "es"
                ]);
                exit(0);
            }
        }

        if ($grant_type != "token") {
            echo $fmt->errors->errorJson([
                "description" => "Access Auth. Grant_type.",
                "code" => "",
                "lang" => "es"
            ]);
            exit(0);
        }

        if ($bearer_token != "Bearer " . $fmt->auth->bearerToken()) {
            echo $fmt->errors->errorJson([
                "description" => "Access Auth. Bearer Token.",
                "code" => "",
                "lang" => "es"
            ]);
            exit(0);
        }

        if (($api['client_id'] != $clientId) && ($api['client_secret'] != $clientSecret)) {
            echo $fmt->errors->errorJson([
                "description" => "Access Auth. Client_id/client_secret.",
                "code" => "",
                "lang" => "es"
            ]);
            exit(0);
        }

        if ($action == "login") {
            $var = json_decode($_POST['vars'], true);
            $userId = $accounts->validateUser($var);
            $accessToken = $accounts->getTokenUser($userId, "access_token");
            $refreshToken = $accounts->getTokenUser($userId, "refresh_token");


            if ($userId) {
                echo json_encode([
                    "Error" => 0,
                    "access_token" => $accessToken,
                    "token_type" => "Bearer",
                    "expires_in" => 3600,
                    "idEntitie" => 1,
                    "refresh_token" => $refreshToken,
                    "type" => "email",
                    "birthdate" => true,
                    "account" => "active"
                ]);
            } else {
                echo $fmt->errors->errorJson([
                    "description" => "Error Login User",
                    "code" => "",
                    "lang" => "es"
                ]);
                exit(0);
            }
        }

        if ($action == "checkCodeAccount") {
            $var = json_decode($_POST['vars'], true);
            //echo json_encode($var);  exit(0);
            $user = $accounts->decodeTokenAccess($var["token"]);
            $codeId = $accounts->checkCodeId($user["userId"]);
            $refreshToken = $accounts->getTokenUser($user["userId"], "refresh_token");
            //echo json_encode($user."+". $codeId); exit(0);

            if ($codeId == $var["code"]) {
                echo json_encode([
                    "Error" => 0,
                    //"access_token" => $var["token"],
                    "token_type" => "Bearer",
                    "expires_in" => 3600,
                    //"idEntitie" => 1,
                    //"refresh_token" => $refreshToken,
                    "type" => "email",
                    "birthdate" => true,
                    "account" => "active"
                ]);
            } else {
                echo $fmt->errors->errorJson([
                    "description" => "error-code",
                    "code" => "",
                    "lang" => "es"
                ]);
            }

            // echo json_encode($userId); exit(0);
        }

        

        if ($action == "resendCode") {
            $var = json_decode($_POST['vars'], true);
            //echo json_encode($var);  exit(0);
            $user = $accounts->decodeTokenAccess($var["token"]);
            //echo json_encode($userId); exit(0);
            $var["userId"] = $user["userId"];

            $return = $accounts->sendCode($var);
            echo json_encode($return);
            exit(0);
            //echo json_encode($var); exit(0);
        }

        if ($action == "activateAccount") {
            $tokenAccount = $_POST['tokenAccount'];

            $user = $accounts->decodeTokenAccess($tokenAccount);
            $userId = $user["userId"];
            $accessToken = $accounts->getTokenUser($userId, "access_token");
            $refreshToken = $accounts->getTokenUser($userId, "refresh_token");
            //echo json_encode($user["userId"]); exit(0);

            $userIdReturn = $accounts->validateUserToken(array("access_token" => $accessToken, "refresh_token" => $refreshToken, "entitieId" => $entitieId));

            //echo json_encode($return);
            if ($userIdReturn) {
                $var["password"] = $_POST['password'];

                $arrayId = $accounts->dataId($userId);

                if (!empty($arrayId["mod_acu_password"])) {
                    echo $fmt->errors->errorJson([
                        "description" => "account-activate",
                        "code" => "",
                        "lang" => "es"
                    ]);
                    exit(0);
                }

                $return = $accounts->update(array("userId" => $userId, "vars" => $var, "entitieId" => $entitieId));
                if ($return) {
                    echo json_encode([
                        "Error" => 0,
                        "access_token" => $accessToken,
                        "token_type" => "Bearer",
                        "expires_in" => 3600,
                        "idEntitie" => 1,
                        "refresh_token" => $refreshToken,
                        "type" => "email",
                        "birthdate" => true,
                        "account" => "active"
                    ]);
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Error Update Activate User",
                        "code" => "",
                        "lang" => "es"
                    ]);
                    exit(0);
                }
            } else {
                echo $fmt->errors->errorJson([
                    "description" => "Error Activate User",
                    "code" => "",
                    "lang" => "es"
                ]);
                exit(0);
            }
        }

        if ($action == "updateAccountUser") {
            $var = json_decode($_POST['vars'], true);
            $userId = $accounts->validateUserToken($var);
            $accessToken = $accounts->getTokenUser($userId, "access_token");
            $refreshToken = $accounts->getTokenUser($userId, "refresh_token");

            //echo json_encode($userId); exit();
            if ($userId) {
                $return = $accounts->update(array("userId" => $userId, "vars" => $var, "entitieId" => $entitieId));
                //echo json_encode($return);
                if ($return) {
                    echo json_encode([
                        "Error" => 0,
                        "access_token" => $accessToken,
                        "token_type" => "Bearer",
                        "expires_in" => 3600,
                        "idEntitie" => 1,
                        "refresh_token" => $refreshToken,
                        "type" => $type,
                        "birthdate" => true,
                        "account" => "active"
                    ]);
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Error Update User",
                        "code" => "",
                        "lang" => "es"
                    ]);
                    exit(0);
                }
            } else {
                echo $fmt->errors->errorJson([
                    "description" => "Error return update User",
                    "code" => "",
                    "lang" => "es"
                ]);
                exit(0);
            }
        }

        if ($action == "createAccountUser") {
            $var = json_decode($_POST['vars'], true);
            $userId = $accounts->add(array("vars" => $var, "entitieId" => $entitieId));

            $email = $var["email"];
            $mailSubject = $var["mailSubject"];
            $setFromName = $var["setFromName"];
            $mailFooter = $var["mailFooter"];
            $mailLogo = $var["mailLogo"];
            $name = $var["name"] . " " . $var["fathersLastname"];
            $type = $var["type"];
            $token = $var["token"];
            //echo json_encode($type); exit(0);
            //echo json_encode($token); exit(0);

            if ($userId["message"] == "error") {
                if ($type == "email") {
                    echo $fmt->errors->errorJson([
                        "description" => "Error " . $userId["description"],
                        "code" => "",
                        "lang" => "es"
                    ]);
                    exit(0);
                }
                if ($userId["description"] == "account-exist") {


                    $accessToken = $accounts->getTokenUser($userId["userId"], "access_token");
                    $refreshToken = $accounts->getTokenUser($userId["userId"], "refresh_token");
                    $tokenTypeReturn = $accounts->getTokenUser($userId["userId"], $type);
                    $birthdate = $accounts->checkBirthdate($userId["userId"]);

                    if ($birthdate) {
                        $birthdateAux = "true";
                    } else {
                        $birthdateAux = "false";
                    }

                    echo json_encode([
                        "Error" => 0,
                        "access_token" => $accessToken,
                        "token_type" => "Bearer",
                        "expires_in" => 3600,
                        "idEntitie" => 1,
                        "refresh_token" => $refreshToken,
                        "type" => $type,
                        "birthdate" => $birthdateAux,
                        "token" => $tokenTypeReturn,
                        "account" => "active"
                    ]);

                    exit(0);
                }
            }

            $accessToken = base64_encode($clientId . "." . $userId . "." . $grant_type . "." . uniqid(microtime(), true));
            $refreshToken = md5(uniqid(microtime(), true));

            $returnCreateUserToken = $accounts->createUserToken($userId, "access_token", $accessToken);
            $returnCreateRefreshToken = $accounts->createUserToken($userId, "refresh_token", $refreshToken);

            if ($type != "email") {
                $accounts->createUserToken($userId, $type, $token);
            }


            $activationHtml = file_get_contents(_PATH_NUCLEO . "views/mail/mail-activation.htm");
            $code = $accounts->checkCodeId($userId);
            $body = str_replace("#CODE#", $code, $activationHtml);
            //$body = str_replace("#link#", _PATH_WEB."activation/user/". $accessToken,$activationHtml);

            $mailHtml = file_get_contents(_PATH_NUCLEO . "views/mail/mail-mensaje.htm");

            $mensajeMail = str_replace("#logo#", $mailLogo, $mailHtml);
            $mensajeMail = str_replace("#body#", $body, $mensajeMail);
            $mensajeMail = str_replace("#footer#", $mailFooter, $mensajeMail);


            if ($type == "email") {
                $fmt->mails->send(array(
                    "Error" => 0,
                    'email' => $email,
                    'name' => $name,
                    'subject' => $mailSubject,
                    'setFromMail' => _EMAIL_USER,
                    'setFromName' =>  $setFromName,
                    'body' => $mensajeMail
                ));
            }

            $birthdate = $accounts->checkBirthdate($userId["userId"]);

            if ($birthdate) {
                $birthdateAux = "true";
            } else {
                $birthdateAux = "false";
            }

            if ($type == 'email') {
                echo json_encode([
                    "Error" => 0,
                    "idEntitie" => 1,
                    "type" => "email",
                    "birthdate" => $birthdateAux,
                    "token" => $accessToken,
                    "account" => "active"
                ]);
            } else {

                echo json_encode([
                    "Error" => 0,
                    "access_token" => $accessToken,
                    "token_type" => "Bearer",
                    "expires_in" => 3600,
                    "idEntitie" => 1,
                    "refresh_token" => $refreshToken,
                    "type" => $type,
                    "birthdate" => $birthdateAux,
                    "token" => $accessToken,
                    "account" => "active"
                ]);
            }

            exit(0);
        }

        break;

    default:
        echo $fmt->errors->errorJson([
            "description" => "Access Auth. Metod request.",
            "code" => "",
            "lang" => "es"
        ]);
        break;
}