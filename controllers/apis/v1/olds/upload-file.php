<?php
$dataPost = json_decode($_POST["data"], true);
//echo json_encode($dataPost); exit(0);

require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $dataPost["accessToken"]["page"] . "config.php");
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

$folder = $dataPost["vars"]["folder"];
$numFiles = $dataPost["vars"]["numFiles"];
$files = $dataPost["vars"]["files"];
$entitieId = $dataPost["accessToken"]["entitieId"];
$replace = $dataPost["vars"]["replace"];
$replace = $replace ? $replace : 0;

$validfiles = $fmt->emptyReturn($dataPost["vars"]["validfiles"], 'jpg,jpeg,gif,png,mp3,mp4,quicktime,doc,docx,xls,xlsx,ppt,pptx,pdf,svg');

if ($numFiles == 0) {
    $return[0]["Error"] = 1;
    $return[0]["status"] = 'error';
    $return[0]["data"] = 'no-files';
    $return[0]["message"] = 'no-files';
    echo json_encode($return);
    exit(0);
}

if (!$fmt->files->createFolder(_PATH_HOST_FILES . "files/" . $folder . "/", "0777")) {
    $return[0]["Error"] = 1;
    $return[0]["status"] = 'error';
    $return[0]["data"] = 'create-folder';
    $return[0]["message"] = 'create-folder';
    echo json_encode($return);
    exit(0);
}

for ($i = 0; $i < $numFiles; $i++) {
    $file = $_FILES["file-" . $i];
    $path = _PATH_HOST_FILES . "files/" . $folder . "/";
    $name = strtolower($file["name"]);
    $size = $file["size"];
    $type = $file["type"];
    $pathTemp = $file["tmp_name"];
    $nameUrl = $fmt->convertPathUrl($name);
    $varTypeFile = array('.jpg', '.jpeg', '.gif', '.png', '.mp3', '.mp4', 'quicktime', '.doc', '.docx', '.xls', '.xlsx', '.ppt', '.pptx', '.pdf', '.svg');
    $varType = array('image/', 'audio/', 'video/', 'application/');
    $inputName = str_replace($varTypeFile, '', $fmt->convertPathUrl($name));
    // $inputType = str_replace($varType,'',$type);
    $inputType = $fmt->files->mimetypeToType($type);
    $inputSize = $fmt->files->sizeFile($size);
    $imageSizeArray = getimagesize($pathTemp);
    $width = $imageSizeArray[0];
    $height = $imageSizeArray[1];
    $imageSize = $width . "x" . $height;

    if ($type == 'video/mp4') {
        $width = 150;
        $height = 150;
    }
    if ($type == 'image/svg+xml') {
        $width = 150;
        $height = 150;
    }

    $validFilesArray = explode(",", $validfiles);
    $numValidFilesArray = count($validFilesArray);
    $numCount = 0;

    for ($j = 0; $j < $numValidFilesArray; $j++) {
        $mimetype = $fmt->files->typeToMimetype($validFilesArray[$j]);
        //echo "-validfile[".$validFilesArray[$j]."]mimetype[".$mimetype."]type[".$type."]";
        if ($mimetype == $type) {
            $numCount++;
        }
    }

    if ($inputType == "jpg" || $inputType == "jpeg" || $inputType == "png" || $inputType == "gif") {
        if ((($type != 'audio/mp3') && ($width < 10)) || (($type != 'audio/mp3') && ($height < 10))) {
            $return[0]["Error"] = 1;
            $return[0]["status"] = "error";
            $return[0]["data"] = "min-size";
            $return[0]["message"] = "la anchura y la altura mínima permitida es 10px";
            $return[0]["element"] = $name;
            echo json_encode($return);
            exit(0);
        }
    }

    if ($numCount == 0) {
        $return[0]["Error"] = 1;
        $return[0]["status"] = "error";
        $return[0]["data"] = "file-novalid";
        $return[0]["message"] = "El archivo no es valido (" . $validfiles . ")" . $inputType;
        echo json_encode($return);
        exit(0);
    }
    if ($size > 1024 * 1024 * 150) {
        $return[$i]["Error"] = 1;
        $return[$i]["status"] = "error";
        $return[$i]["data"] = "file-size-max-mb";
        $return[$i]["message"] = "El tamaño máximo permitido es un 150MB";
        echo json_encode($return);
        exit(0);
    }

    if ($width > 10000 || $height > 10000) {
        $return[$i]["Error"] = 1;
        $return[$i]["status"] = "error";
        $return[$i]["data"] = "file-sizemax";
        $return[$i]["message"] = "La anchura y la altura maxima permitida es 10000px";
        echo json_encode($return);
        exit(0);

    }

    chmod($path, "0755");
    //chmod($_FILES["file-".$i]["tmp_name"], 0777);
    //echo json_encode($fmt->files->existFile($path.$nameUrl));
    //exit(0);

    if ($fmt->files->existFile($path . $nameUrl) == true && $replace != 1) {
        $nameUrl = $fmt->files->nameFileNum($nameUrl);
        $inputName = $fmt->files->nameFile($nameUrl);
        $return[0]["Error"] = 1;
        $return[0]["status"] = "error";
        $return[0]["data"] = "exist-file";
        $return[0]["message"] = $name;
        echo json_encode($return);
        exit(0);
    }

    $moved = move_uploaded_file($_FILES["file-" . $i]["tmp_name"], $path . $nameUrl);

    if (!$moved) {
        $error[0]["Error"] = 1;
        $error[0]["status"] = "error";
        $error[0]["message"] = "error de movimiento a la carpeta quizas necesite permisos.";
        $error[0]["action1"] = $path . $nameUrl;
        $error[0]["resp"] = $moved;
        $error[0]["action2"] = $_FILES["file-" . $i]["tmp_name"];
        $error[0]["action3"] = $_FILES["file-" . $i]["error"];
        $error[0]["action5"] = substr(sprintf('%o', fileperms($_FILES["file-" . $i]["tmp_name"])), -4);
        echo json_encode($error);
        exit(0);
    }

    if (($type == 'image/jpg') || ($type == 'image/svg+xml') || ($type == 'image/jpeg') || ($type == 'image/png') || ($type == 'image/gif') || ($type == 'image/webp') || ($type == 'jpg') || ($type == 'svg+xml') || ($type == 'jpeg') || ($type == 'png') || ($type == 'gif') || ($type == 'webp') || ($type == 'avif') || ($type == 'image/avif')) {
        $nameThumb = $fmt->files->urlAdd($nameUrl, "-thumb");
        //$data = $fmt->files->createThumb($path.$nameUrl,$path.$nameThumb,250,250,0);

        $data = $fmt->files->createThumbImage($path . $nameUrl, $path . $nameThumb, 100);


        $return[$i]["resultThumb"] = $data;
        $return[$i]["typeFile"] = $inputType;
    }



    $return[$i]["Error"] = 0;
    $return[$i]["status"] = 'success';
    // $return[$i]["vars"] = $nameUrl.",".$fmt->files->insertFile(array("name"=>$inputName,"pathUrl"=>"files/".$folder."/".$nameUrl,"ext"=>$inputType)).",files/".$folder."/".$nameThumb.",".$inputType.",".$inputName.",";
    $return[$i]["name"] = $nameUrl;
    $return[$i]["type"] = $type;
    $return[$i]["pathurl"] = "files/" . $folder . "/" . $nameUrl;
    $return[$i]["fileId"] = $fmt->files->insertFile(array("name" => $inputName, "pathUrl" => "files/" . $folder . "/" . $nameUrl, "ext" => $inputType, "entitieId" => $entitieId));

    //echo "sin errores";

}

echo json_encode($return);