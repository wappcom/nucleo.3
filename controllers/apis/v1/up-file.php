<?php
require_once ($_SERVER['DOCUMENT_ROOT'] . "/" . $_POST["page"] . "config.php");
require_once (_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

// $output_dir = _RUTA_HOST."file/multimedia/";

$folder = $_POST["folder"];
$thumbOption = $_POST["thumb"] ? $_POST["thumb"] : "";
$last = $_POST["last"] ? "-" . $_POST["last"] : "";
$numFile = $_POST["numFile"];
$file = "";
$path = "";

$mode = $_POST["mode"];


if ($mode == "single" && $numFile > 1) {
    $return[0]["Error"] = 1;
    $return[0]["status"] = 'error';
    $return[0]["action"] = 'no-files';
    $return[0]["message"] = "Solo se puede subir un archivo";
    echo json_encode($return);
    exit(0);
}

$validFiles = $fmt->emptyReturn($_POST["validfiles"], 'jpg,jpeg,gif,png,mp3,mp4,quicktime,doc,docx,xls,xlsx,ppt,pptx,pdf,svg,webp,avif');

if ($numFile == 0) {
    $return[0]["Error"] = 1;
    $return[0]["status"] = 'error';
    $return[0]["action"] = 'no-files';
    $return[0]["message"] = "No hay archivos para subir";
    echo json_encode($return);
    exit(0);
}

if ($folder == "posts") {
    $folder = "posts/" . $fmt->data->dateFormat("America/La_Paz", "Y-m-d");
}


$createFolder = $fmt->files->createFolder(_PATH_HOST_FILES . "files/" . $folder . "/", 0777);



if ($createFolder["Error"] == 0) {

    for ($i = 0; $i < $numFile; $i++) {
        $file = $_FILES["file-" . $i];

        $path = _PATH_HOST_FILES . "files/" . $folder . "/";

        //echo json_encode($path); exit(0);
        //echo json_encode($file); exit(0);

        $name = strtolower($file["name"]);

        $size = $file["size"];
        $type = $file["type"];
        $pathTemp = $file["tmp_name"];

        $nameUrl = $fmt->convertPathUrl($name);



        $varTypeFile = array('.jpg', '.jpeg', '.gif', '.png', '.mp3', '.mp4', 'quicktime', '.doc', '.docx', '.xls', '.xlsx', '.ppt', '.pptx', '.pdf', '.svg', '.webp', '.avif');
        $varType = array('image/', 'audio/', 'video/', 'application/');

        $inputName = str_replace($varTypeFile, '', $fmt->convertPathUrl($name));
        // $inputType = str_replace($varType,'',$type);



        $arrayName = explode(".", $nameUrl);

        $nameUrl = $arrayName[0] . $last . "." . $arrayName[1];

        $inputName = $inputName . $last;

        $inputType = $fmt->files->mimetypeToType($type);

        //echo json_encode($inputType); exit(0);

        if ($inputType == null) {
            $return[$i]["Error"] = 1;
            $return[$i]["status"] = 'error';
            $return[$i]["action"] = "file-novalid";
            $return[$i]["message"] = "El archivo no es valido (" . $validFiles . ") type:" . $type;
            $return[$i]["otros"] = $file;
            echo json_encode($return);
            exit(0);
        }

        $inputSize = $fmt->files->sizeFile($size);
        $imageSizeArray = getimagesize($pathTemp);
        $width = $imageSizeArray[0];
        $height = $imageSizeArray[1];
        $imageSize = $width . "x" . $height;

        if ($inputSize == null || $inputSize == 0) {
            $return[$i]["Error"] = 1;
            $return[$i]["status"] = 'error';
            $return[$i]["action"] = "file-novalid";
            $return[$i]["message"] = "El archivo es 0 bytes o supera los 50MB";
            echo json_encode($return);
            exit(0);
        }



        if ($type == 'video/mp4') {
            $width = 150;
            $height = 150;
        }
        if ($type == 'image/svg+xml') {
            $width = 150;
            $height = 150;
        }



        $validFilesArray = explode(",", $validFiles);
        $numValidFilesArray = count($validFilesArray);
        $numCount = 0;

        for ($j = 0; $j < $numValidFilesArray; $j++) {
            $mimetype = $fmt->files->typeToMimetype($validFilesArray[$j]);
            //echo "-validfile[".$validFilesArray[$j]."]mimetype[".$mimetype."]type[".$type."]";
            if ($mimetype == $type) {
                $numCount++;
            }
        }



        if ($inputType == "jpg" || $inputType == "jpeg" || $inputType == "webp" || $inputType == "avif" || $inputType == "png" || $inputType == "gif") {
            if ((($type != 'audio/mp3') && ($width < 10)) || (($type != 'audio/mp3') && ($height < 10))) {
                $return[$i]["Error"] = 1;
                $return[$i]["status"] = 'error';
                $return[$i]["action"] = "min-size";
                $return[$i]["message"] = "la anchura y la altura mínima permitida es 10px";
            }
        }

        if ($numCount == 0) {
            $return[$i]["Error"] = 1;
            $return[$i]["status"] = 'error';
            $return[$i]["action"] = "file-novalid";
            $return[$i]["message"] = "3-El archivo no es valido (" . $validFiles . ") mimetype:" . $mimetype . " type:" . $type;

        } else if ($size > 1024 * 1024 * 150) {
            $return[$i]["Error"] = 1;
            $return[$i]["status"] = 'error';
            $return[$i]["action"] = "file-size-max-mb";
            $return[$i]["message"] = "El tamaño máximo permitido es un 150MB";

        } else if ($width > 10000 || $height > 10000) {
            $return[$i]["Error"] = 1;
            $return[$i]["status"] = 'error';
            $return[$i]["action"] = "file-sizemax";
            $return[$i]["message"] = "La anchura y la altura maxima permitida es 10000px";
        } else {

            //chmod($path, "0777");
            //chmod($_FILES["file-".$i]["tmp_name"], 0777);

            $existFile = 0;

            if (($fmt->files->existFile($path . $nameUrl)) && $folder != "docs") {
                $nameUrl = $fmt->files->nameFileNum($nameUrl);
                $inputName = $fmt->files->nameFile($nameUrl);
                $return[$i]["Error"] = 1;
                $return[$i]["status"] = 'error';
                $return[$i]["action"] = "exist-file";
                $return[$i]["massage"] = "El archivo ya existe. Se ha cambiado el nombre " . $nameUrl;
                $existFile++;
            }

            //echo json_encode($path); exit(0);

            $moved = move_uploaded_file($_FILES["file-" . $i]["tmp_name"], $path . $nameUrl);

            if (!$moved) {
                $error[0]["Error"] = 1;
                $error[0]["status"] = 'error';
                $error[0]["resp"] = $moved;
                $error[0]["message"] = "error de movimiento a la carpeta quizas necesite permisos. moved: " . $moved . "path:" . $path . $nameUrl . " file-name-temp:" . $_FILES["file-" . $i]["tmp_name"] . " error:" . $_FILES["file-" . $i]["error"] . " other" . substr(sprintf('%o', fileperms($_FILES["file-" . $i]["tmp_name"])), -4);
                echo json_encode($error);
                exit(0);
            }
            $fileId = $fmt->files->insertFile(array("name" => $inputName, "pathUrl" => "files/" . $folder . "/" . $nameUrl, "ext" => $inputType));

            /* if ($type == 'image/webp' || $type == 'image/avif' || $type == 'image/jpg' || $type == 'image/svg+xml' || $type == 'image/jpeg' || $type == 'image/png' || $type == 'image/gif') {
                $nameThumb = $fmt->files->urlAdd($nameUrl, "-thumb");
                $data = $fmt->files->createThumb($path . $nameUrl, $path . $nameThumb, 250, 250, 0);
                $return[$i]["return"]["data"]["thumb"] = $nameThumb;
                $return[$i]["return"]["data"]["typeFile"] = 'image';
            } */

            if (($type == 'image/jpg') || ($type == 'image/jpeg') || ($type == 'image/png') || ($type == 'image/gif') || ($type == 'image/webp') || ($type == 'jpg') || ($type == 'jpeg') || ($type == 'png') || ($type == 'gif') || ($type == 'webp') || ($type == 'avif') || ($type == 'image/avif')) {
                $nameThumb = $fmt->files->urlAdd($nameUrl, "-thumb");
                $nameMiddle = $fmt->files->urlAdd($nameUrl, "-middle");
                //$data = $fmt->files->createThumb($path.$nameUrl,$path.$nameThumb,250,250,0);

                if ($thumbOption != "no") {

                    $data = $fmt->files->createThumbImage($path . $nameUrl, $path . $nameThumb, 250, 250, 0);

                    if ($data != true) {
                        echo json_encode($data);
                    }

                    $dataMiddle = $fmt->files->createThumbImage($path . $nameUrl, $path . $nameMiddle, 450, 450, 0);

                    if ($dataMiddle != true) {
                        echo json_encode($data);
                    }


                    $imagen = _PATH_HOST_FILES . "files/" . $folder . "/" . $nameUrl;

                    $output = shell_exec("zbarimg --quiet --raw $imagen");

                    $return[$i]["return"]["data"]["QR"]["root"] = $imagen;
                    $return[$i]["return"]["data"]["QR"] = $output;
                    $return[$i]["return"]["data"]["resultThumb"] = $data;
                    $return[$i]["return"]["data"]["thumb"] = "files/" . $folder . "/" . $nameThumb;
                } else {
                    $return[$i]["return"]["data"]["thumb"] = "false";
                }
                $return[$i]["return"]["data"]["typeFile"] = 'image';

            }


            if ($type == 'application/pdf' || $type == 'pdf') {
                $name = str_replace(".pdf", ".jpg", $nameUrl);
                $nameThumb = $fmt->files->urlAdd($name, "-thumb");
                // echo json_encode($path . $nameUrl. " ".$path . $name); exit(0);
                if ($thumbOption != "no") {
                    $dataOrigin = $fmt->files->generateThumbnailFromPDF($path . $nameUrl, $path . $name, 1200, 1200);

                    //echo json_encode($dataOrigin); exit(0);

                    // if ($dataOrigin["Error"] == 1) {
                    //    echo json_encode($dataOrigin);
                    // }  
                    if ($dataOrigin["Error"] == 0) {
                        $dataThumb = $fmt->files->createThumbImage($path . $name, $path . $nameThumb, 100, 100);
                    }
                }

                // if ($dataThumb  != true) {
                //     echo json_encode($dataThumb);
                // }

                //echo json_encode($dataOrigin); exit(0);

                $return[$i]["return"]["data"]["thumbPdf"] = $dataOrigin;
                $return[$i]["return"]["data"]["typeFile"] = 'pdf';
                $return[$i]["return"]["data"]["data"] = $data;
                if ($thumbOption != "no") {
                    $return[$i]["return"]["data"]["img"] = "files/" . $folder . "/" . $name;
                    $return[$i]["return"]["data"]["thumb"] = "files/" . $folder . "/" . $nameThumb;
                } else {
                    $return[$i]["return"]["data"]["thumb"] = "false";
                }
            }

            if ($type == 'svg+xml' || $type == 'image/svg+xml' || $type == 'svg') {
                $return[$i]["return"]["data"]["typeFile"] = 'svg';
            }


            if ($type == 'audio/mp3') {
                $return[$i]["return"]["data"]["typeFile"] = 'audio';
            }

            if ($type == 'video/mp4') {
                $return[$i]["return"]["data"]["typeFile"] = 'video';
            }

            if ($existFile == 0) {
                $return[$i]["return"]["data"]["typeReturn"] = "return";
            }

            if ($type != 'image/webp' && $type != 'image/avif') {
                $nameEnd = $nameThumb;
            } else {
                $nameEnd = $nameUrl;
            }


            //$return[$i]["vars"] = $nameUrl . "," . $fileId . ",files/" . $folder . "/" . $nameEnd . "," . $inputType . "," . $inputName . ",";

            $return[$i]["return"]["Error"] = 0;
            $return[$i]["return"]["status"] = "success";
            $return[$i]["return"]["api"] = "v1.up-file";
            $return[$i]["return"]["message"] = "Se ha subido el archivo con exito.";
            $return[$i]["return"]["data"]["fileId"] = $fileId;
            $return[$i]["return"]["data"]["file"] = "files/" . $folder . "/" . $nameUrl;

            $return[$i]["return"]["data"]["folder"] = $folder;
            $return[$i]["return"]["data"]["type"] = $inputType;
            // $return[$i]["return"]["data"]["typeFile"] = $type;
            $return[$i]["return"]["data"]["name"] = $inputName;



            //echo "sin errores";
        }
    }

    echo json_encode($return);

} else {

    echo json_encode($createFolder);
}