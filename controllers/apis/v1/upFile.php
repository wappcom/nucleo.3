<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $_POST["page"] . "config.php");
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

// $output_dir = _RUTA_HOST."file/multimedia/";

$folder = $_POST["folder"];
$numFile = $_POST["numFile"];
$file = "";
$path = "";

$validFiles = $fmt->emptyReturn($_POST["validfiles"], 'jpg,jpeg,gif,png,mp3,mp4,quicktime,doc,docx,xls,xlsx,ppt,pptx,pdf,svg,webp,avif');

if ($numFile == 0) {
    $return[0]["type"] = 'error';
    $return[0]["action"] = 'no-files';
    echo json_encode($return);
    exit(0);
}

if ($fmt->files->createFolder(_PATH_HOST_FILES . "files/" . $folder . "/", "0777")) {
    for ($i = 0; $i < $numFile; $i++) {
        $file = $_FILES["file-" . $i];
        $path = _PATH_HOST_FILES . "files/" . $folder . "/";
        $name = strtolower($file["name"]);
        $size = $file["size"];
        $type = $file["type"];
        $pathTemp = $file["tmp_name"];
        $nameUrl = $fmt->convertPathUrl($name);
        $varTypeFile = array('.jpg', '.jpeg', '.gif', '.png', '.mp3', '.mp4', 'quicktime', '.doc', '.docx', '.xls', '.xlsx', '.ppt', '.pptx', '.pdf', '.svg', '.webp', '.avif');
        $varType = array('image/', 'audio/', 'video/', 'application/');
        $inputName = str_replace($varTypeFile, '', $fmt->convertPathUrl($name));
        // $inputType = str_replace($varType,'',$type);
        $inputType = $fmt->files->mimetypeToType($type);
        $inputSize = $fmt->files->sizeFile($size);
        $imageSizeArray = getimagesize($pathTemp);
        $width = $imageSizeArray[0];
        $height = $imageSizeArray[1];
        $imageSize = $width . "x" . $height;

        if ($type == 'video/mp4') {
            $width = 150;
            $height = 150;
        }
        if ($type == 'image/svg+xml') {
            $width = 150;
            $height = 150;
        }

        $validFilesArray = explode(",", $validFiles);
        $numValidFilesArray = count($validFilesArray);
        $numCount = 0;

        for ($j = 0; $j < $numValidFilesArray; $j++) {
            $mimetype = $fmt->files->typeToMimetype($validFilesArray[$j]);
            //echo "-validfile[".$validFilesArray[$j]."]mimetype[".$mimetype."]type[".$type."]";
            if ($mimetype == $type) {
                $numCount++;
            }
        }

        if ($inputType == "jpg" || $inputType == "jpeg" || $inputType == "webp" || $inputType == "avif" || $inputType == "png" || $inputType == "gif") {
            if ((($type != 'audio/mp3') && ($width < 10)) || (($type != 'audio/mp3') && ($height < 10))) {
                $return[$i]["type"] = "error";
                $return[$i]["action"] = "min-size";
                $return[$i]["vars"] = "la anchura y la altura mínima permitida es 10px";
            }
        }

        if ($numCount == 0) {
            $return[$i]["type"] = "error";
            $return[$i]["action"] = "file-novalid";
            $return[$i]["vars"] = "2-El archivo no es valido" . $inputType . " " . $mimetype . " " . $type . "(" . $validFiles . ")";

        } else if ($size > 1024 * 1024 * 150) {
            $return[$i]["type"] = "error";
            $return[$i]["action"] = "file-size-max-mb";
            $return[$i]["vars"] = "El tamaño máximo permitido es un 150MB";
        } else if ($width > 10000 || $height > 10000) {
            $return[$i]["type"] = "error";
            $return[$i]["action"] = "file-sizemax";
            $return[$i]["vars"] = "La anchura y la altura maxima permitida es 10000px";
        } else {

            chmod($path, "0755");
            //chmod($_FILES["file-".$i]["tmp_name"], 0777);

            $existFile = 0;

            if ($fmt->files->existFile($path . $nameUrl)) {
                $nameUrl = $fmt->files->nameFileNum($nameUrl);
                $inputName = $fmt->files->nameFile($nameUrl);
                $return[$i]["type"] = "error";
                $return[$i]["action"] = "exist-file";
                $return[$i]["vars"] = $nameUrl;
                $existFile++;
            }

            $moved = move_uploaded_file($_FILES["file-" . $i]["tmp_name"], $path . $nameUrl);

            if (!$moved) {
                $error[0]["resp"] = $moved;
                $error[0]["type"] = "error";
                $error[0]["message"] = "error de movimiento a la carpeta quizas necesite permisos.";
                $error[0]["action1"] = $path . $nameUrl;
                $error[0]["action2"] = $_FILES["file-" . $i]["tmp_name"];
                $error[0]["action3"] = $_FILES["file-" . $i]["error"];
                //$error[0]["action4"]  = substr(sprintf('%o', fileperms('/home/wappcom/www/upb/files/contents')), -4);
                $error[0]["action4"] = substr(sprintf('%o', fileperms($_FILES["file-" . $i]["tmp_name"])), -4);
                echo json_encode($error);
                exit(0);
            }

            if ($type == 'image/webp' || $type == 'image/avif' || $type == 'image/jpg' || $type == 'image/svg+xml' || $type == 'image/jpeg' || $type == 'image/png' || $type == 'image/gif') {
                $nameThumb = $fmt->files->urlAdd($nameUrl, "-thumb");
                $data = $fmt->files->createThumb($path . $nameUrl, $path . $nameThumb, 250, 250, 0);
                $return[$i]["resultThumb"] = $data;
                $return[$i]["typeFile"] = $inputType;
            }

            if ($existFile == 0) {
                $return[$i]["type"] = "return";
            }

            if ($type != 'image/webp' && $type != 'image/avif') {
                $nameEnd = $nameThumb;
            } else {
                $nameEnd = $nameUrl;
            }
            $fileId = $fmt->files->insertFile(array("name" => $inputName, "pathUrl" => "files/" . $folder . "/" . $nameUrl, "ext" => $inputType));

            $return[$i]["vars"] = $nameUrl . "," . $fileId . ",files/" . $folder . "/" . $nameEnd . "," . $inputType . "," . $inputName . ",";

            $return[$i]["return"]["Error"] = 0;
            $return[$i]["return"]["status"] = "success";
            $return[$i]["return"]["message"] = "Se ha subido el archivo con exito.";
            $return[$i]["return"]["data"]["fileId"] = $fileId;
            $return[$i]["return"]["api"] = "v1/upFile";
            $return[$i]["return"]["data"]["file"] = "files/" . $folder . "/" . $nameUrl;
            $return[$i]["return"]["data"]["folder"] = $folder;
            $return[$i]["return"]["data"]["type"] = $inputType;
            $return[$i]["return"]["data"]["name"] = $inputName;

            //echo "sin errores";
        }
    }

    echo json_encode($return);

} else {
    $error[0]["type"] = "error";
    $error[0]["action"] = "create-folder";
    echo json_encode($error);
}