<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $_POST["page"] . "config.php");
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

header("Content-Type: application/text");
header("Content-Type: application/text; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

switch ($_SERVER["REQUEST_METHOD"]) {
    case 'POST':
        
        $access_token = $fmt->auth->getInput('access_token');
        $refresh_token = $fmt->auth->getInput('refresh_token');
        $entitieId = $fmt->auth->getInput('idEntitie');
        $userId = $fmt->users->validateUserToken(array("access_token"=>$access_token, "refresh_token"=> $refresh_token, "idEntitie" => $entitieId));
        
        // echo json_encode($userId);
        // exit(0);

        if($userId){
            //echo json_encode("usario validado"); exit(0);
            
            $action = $fmt->auth->getInput('action');
            //echo json_encode( $action );  exit(0);

            switch ($action) {
                case 'chargeProducts':
                    //echo "chargeProduct:".$_POST["objs"];  exit(0);
                    require_once(_PATH_HOST."modules/inventory/controllers/class/class.products.php");
                    $products = new PRODUCTS($fmt);

                    echo  $products::importXls(array("objsXls"=>$_POST["objs"], "entitieId"=>$entitieId, "userId"=>$userId, "action" => $action ));

                    break;
                
                default:
                    echo $fmt->errors->errorJson([
                        "description"=>"Access Auth. No have action.",
                        "code"=>"",
                        "lang"=>"es"
                    ]); 
                    break;
            }
            //echo json_encode($obj.$action);

        }else{
            echo $fmt->errors->errorJson([
                "description"=>"Access Auth. invalid user",
                "code"=>"",
                "lang"=>"es"
            ]); 
        }
    break;

    default:
        echo $fmt->errors->errorJson([
            "description"=>"Access Auth. Metod request.",
            "code"=>"",
            "lang"=>"es"
        ]);
        break;
}