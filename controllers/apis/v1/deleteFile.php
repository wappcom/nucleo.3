<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $_POST["page"] . "config.php");
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

header("Content-Type: application/text");
header("Content-Type: application/text; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

switch ($_SERVER["REQUEST_METHOD"]) {
    case 'POST':
        
        $access_token = $fmt->auth->getInput('access_token');
        $refresh_token = $fmt->auth->getInput('refresh_token');
        $entitieId = $fmt->auth->getInput('idEntitie');
         

        if($fmt->users->validateUserToken(array("access_token"=>$access_token, "refresh_token"=> $refresh_token, "idEntitie" => $entitieId))){
            //echo "'usario validado";

            $idFile = $fmt->auth->getInput('idFile');
            echo  $fmt->files->deteleFileId($idFile);
             
        }else{
            echo $fmt->errors->errorJson([
                "description"=>"Access Auth. invalid user",
                "code"=>"",
                "lang"=>"es"
            ]); 
        }
    break;

    default:
        echo $fmt->errors->errorJson([
            "description"=>"Access Auth. Metod request.",
            "code"=>"",
            "lang"=>"es"
        ]);
        break;
}