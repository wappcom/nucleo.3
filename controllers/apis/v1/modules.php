<?php

$access = json_decode($_POST["accessToken"], true);
$page = $_POST["page"];
if (!empty($page) && $page != null) {
    $rootPage = $page;
} else {
    $rootPage = $access['page'];
}
require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $rootPage . "config.php");
require_once(_PATH_CONSTRUCTOR);
$fmt = new CONSTRUCTOR();

header("Content-Type: application/text");
header("Content-Type: application/text; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

switch ($_SERVER["REQUEST_METHOD"]) {
    case 'POST':

        $access_token = $fmt->auth->getInput('access_token');
        $refresh_token = $fmt->auth->getInput('refresh_token');
        $entitieId = $fmt->auth->getInput('idEntitie');

        if (!empty($page)) {
            if ($fmt->users->validateUser(array("access_token" => $access_token, "refresh_token" => $refresh_token, "idEntitie" => $entitieId))) {
                //echo "'usario validado";

                $action = $fmt->auth->getInput('action');


                switch ($action) {
                    case 'timeLeft':
                        $endDate = $fmt->auth->getInput('endDate');
                        $startDate = $fmt->auth->getInput('startDate');
                        $mode  = $fmt->auth->getInput('mode');
                        echo $fmt->modules->timeLeft(array('startDate' => $startDate, 'endDate' => $endDate));
                        exit(0);
                        break;

                    default:
                        # code...
                        break;
                }
            }
        }

        $access_token = $access['access_token'];
        $refresh_token = $access['refresh_token'];
        $entitieId = $access['entitieId'];
        $action = $fmt->auth->getInput('action');
        //echo json_encode("1:". $action); exit(0);
        //echo json_encode("2:". $entitieId); exit(0);

        $userId = $fmt->users->validateUserToken(array("access_token" => $access_token, "refresh_token" => $refresh_token, "idEntitie" => $entitieId));

        //echo json_encode($userId); exit(0);
        if ($userId) {
            $return["user"] = $fmt->users->userDataAuth(array("access_token" => $access_token, "idEntitie" => $entitieId));
            $userId = $return["user"]["userId"];
            $rolId = $return["user"]["rolId"];

            if ($action == "deleteItemModule") {
                //echo json_encode(1); exit(0);
                $vars =  json_decode($_POST['vars'], true);
                //echo json_encode($vars); exit(0);
                $state = $fmt->modules->deleteModuleItem(array(
                    "rolId" => $rolId,
                    "userId" => $userId,
                    "vars" => $vars,
                    "entitieId" => $entitieId
                ));
                //echo json_encode($state); exit(0);  
                if ($state) {
                    //echo json_encode($state);
                    $resume["Error"] = 0;
                    $resume["data"] = $state;
                    echo json_encode($resume);
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Error, delete Item.",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
            }
            if ($action == "changeStateModuleItem") {
                $vars =  json_decode($_POST['vars'], true);
                //echo json_encode($vars); exit(0);
                $state = $fmt->modules->changeStateModuleItem(array(
                    "rolId" => $rolId,
                    "userId" => $userId,
                    "vars" => $vars,
                    "entitieId" => $entitieId
                ));
                //echo json_encode($state); exit(0);  
                if (($state == 1) || ($state == 0)) {
                    //echo json_encode($state);
                    $resume["Error"] = 0;
                    $resume["response"] = $state;
                    echo json_encode($resume);
                } else {
                    echo $fmt->errors->errorJson([
                        "description" => "Error, delete Item.",
                        "code" => "",
                        "lang" => "es"
                    ]);
                }
            }
        } else {
            echo $fmt->errors->errorJson([
                "description" => "Access Auth. invalid user",
                "code" => "",
                "lang" => "es"
            ]);
        }
        break;

    default:
        echo $fmt->errors->errorJson([
            "description" => "Access Auth. Metod request.",
            "code" => "",
            "lang" => "es"
        ]);
        break;
}