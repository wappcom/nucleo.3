<?php
header('Content-Type: text/html; charset=utf-8');
class AUTH
{

	var $fmt;

	function __construct($fmt)
	{
		$this->fmt = $fmt;
	}

	public function index()
	{

		$rootIndex = $this->getArrayUrl();
		$dataEnd = end($rootIndex);

		$key = "userId";

		//var_dump($rootIndex); exit(0);
		//echo "root:".$rootIndex[0];
		//echo "root:".$rootIndex[1];

		//echo $dataEnd;
		//echo "hol:".strrpos($rootIndex[0],"activation?");

		$patron = '/key=([^&]+)/';


		if (preg_match($patron, $rootIndex[0], $coincidencias)) {
			//echo "activation:".$coincidencias[1]; exit(0);

			$tokenAuth = $coincidencias[1];
			require_once (_PATH_HOST . "ion/activation.php");
			exit(0);

		}

		$patron = explode("?", $rootIndex[0]);

		if ($patron[0] == 'booking') {
			$queryString = isset($patron[1]) ? $patron[1] : '';
			$queryString = html_entity_decode($queryString, ENT_QUOTES | ENT_HTML5);
			$queryString = str_replace('&amp;', '&', $queryString);
			$params = [];
			parse_str($queryString, $params);
			
			//var_dump($params); // Para ver el array resultante

			if ($params['version'] == 1){
				require_once(_PATH_HOST . "sites/default/controllers/apis/v1/booking.local.php");
			}
			exit(0);
		}


		switch ($rootIndex[0]) {
			case ('activation'):
				//echo ';activation;';
				if ($rootIndex[1] == "user") {
					$tokenAuth = $rootIndex[2];
					require_once (_PATH_HOST . "ion/activation.php");
					exit(0);
				}
				//require_once(_PATH_NUCLEO . "modules/login/login.local.php");
				break;
			case 'login':
				if ($this->fmt->sessions->getVar("keySession") == false) {
					require_once (_PATH_NUCLEO . "modules/login/login.local.php");
				} else {
					$path = $this->fmt->setUrlNucleo($this->fmt->users->idRolRedirectionUrl($this->fmt->sessions->getVar("userRol")));
					echo '<script type="text/javascript" >document.location.href="' . $path . '";</script>';
					exit(0);
				}
				break;

			case 'dashboard':
				require_once (_PATH_NUCLEO . "modules/dashboard/index.php");
				break;
			case 'logout':
				require_once (_PATH_NUCLEO . "modules/login/logout.local.php");
				break;

			case 'forgot':
				$this->validateSession(_PATH_NUCLEO . "modules/login/forgot.local.php", $key);
				break;

			case 'apis':
				require_once (_PATH_HOST . "apis/" . $rootIndex[1] . "/" . $rootIndex[2] . ".php");
				break;

			case 'booking':
				echo "booking/?"; 
				exit(0);
				break;

			default:
				$this->indexRoot();
				break;
		}

		//Worksheet = planilla / hoja de trabajo
	}

	public function indexReactive(string $mode = null)
	{
		//return $var;
		define("_ROOT_GET", "");
		define("_PATH_DASHBOARD", "modules/dashboard/");
		//define("_POST_ID", "");


		$rootIndex = $this->getArrayUrl();
		//var_dump($rootIndex);

		if (empty($rootIndex[0])) {
			define("_POST_ID", "");
			$cat = 1;
			$worksheet = 1;
			//echo "here emply";
			$body = $this->fmt->worksheets->loadBody($cat, $worksheet);
			echo $this->fmt->worksheets->htmlReactive($body);
		} else {
			//echo "here no emply";
			//echo $rootIndex[0];

			if (strrpos($rootIndex[0], "noticia.php?id") !== false) {
				$catId = intval($_GET["id_cat"]);
				$postId = intval($_GET["id"]);
				//echo "catId:".$catId . " postId:" . $postId;
				define("_POST_ID_OLD", $postId);

				$cat = 1;
				$worksheet = 3;
				//echo "cat:" . $cat;
				$body = $this->fmt->worksheets->loadBody($cat, $worksheet);
				echo $this->fmt->worksheets->htmlReactive($body);

				exit(0);
			}


			if (strrpos(end($rootIndex), "?m=react")) {
				$root = end($rootIndex);
				$root = str_replace("?m=react", "", $root);
				if ($this->matchCategory($root)) {
					$cat = $this->matchCategory($root);
					$worksheet = 1;
					echo $this->fmt->worksheets->loadBody($cat, $worksheet);
				} else {
					echo "error-404,";
				}
			} else {


				//echo "no react" . end($rootIndex);
				if ($this->matchCategory(end($rootIndex))) {
					$cat = $this->matchCategory(end($rootIndex));
					$worksheet = 1;
					//echo "cat:" . $cat;
					$body = $this->fmt->worksheets->loadBody($cat, $worksheet);
					echo $this->fmt->worksheets->htmlReactive($body);
				} else {
					$dataEnd = end($rootIndex);


					if (strrpos($dataEnd, ".html")) {
						//var_dump($rootIndex);

						$countCats = count($rootIndex);
						$catPath = $rootIndex[1];
						//$catPath = $rootIndex[1];
						//echo "catPath:" . $catPath;
						//echo "count catPath:" . $countCats;

						$cat = $this->matchCategory($catPath);

						//echo "post_id:" . _POST_ID;
						$worksheet = 2;


						$patronFB = '/fbclid=([^&]+)/';

						if (preg_match($patronFB, $dataEnd, $coincidencias)) {
							//echo "strrpos ?fbclid=";


							$strRoot = explode("?fbclid=", $dataEnd);
							//var_dump($dataEnd);
							//var_dump($rootIndex);
							//var_dump($strRoot);

							$path = str_replace(".html", "", $strRoot[0]);
							$postId = $this->fmt->posts->getPathPostToId($path);

							define("_POST_ID", $postId);
							//echo "cat:" . $cat . " postId:".$postId;
						} else {
							//echo "dataEnd:" . $dataEnd;
							//echo "cat:" . $cat . " postId:".$this->fmt->posts->getPathPostToId(str_replace(".html", "", $dataEnd));
							define("_POST_ID", $this->fmt->posts->getPathPostToId(str_replace(".html", "", $dataEnd)));
							define("_CAT_ID_2", $rootIndex[2]);
						}

						$body = $this->fmt->worksheets->loadBody($cat, $worksheet);
						echo $this->fmt->worksheets->htmlReactive($body);
						exit(0);
					}

					if (strpos($dataEnd, "-")) {
						//echo "- ".$dataEnd;

						$catPath = $rootIndex[1];
						//var_dump($rootIndex);

						if (count($rootIndex) > 2) {
							$catPath = $rootIndex[1];
							$worksheet = 2;
						} else {
							$catPath = $rootIndex[0];
							$worksheet = 3;
						}

						//echo "catPath:" . $catPath;
						//echo str_replace(":search", "", $dataEnd);
						define("_SEARCH", str_replace(":search", "", $dataEnd));

						//echo "search:" . _SEARCH;

						$cat = $this->matchCategory($catPath);


						$body = $this->fmt->worksheets->loadBody($cat, $worksheet);
						echo $this->fmt->worksheets->htmlReactive($body);
						exit(0);
					}

					/* if (strpos($dataEnd, "search?q=")) {
																																																																																																																																					  $catPath = $rootIndex[1];
																																																																																																																																					  $cat = $this->matchCategory($catPath);
																																																																																																																																					  $worksheet = 2;
																																																																																																																																					  $body = $this->fmt->worksheets->loadBody($cat, $worksheet);
																																																																																																																																					  echo $this->fmt->worksheets->htmlReactive($body);
																																																																																																																																					  exit(0);
																																																																																																																																				  } */


					if (strpos($dataEnd, "?")) {
						//echo "?". $dataEnd;
						$queryString = parse_url($url, PHP_URL_QUERY);
						$catPath = explode("?", $dataEnd);
						$variables = $catPath[1];
						// Parsear la cadena de consulta en un array asociativo
						$queryString = parse_url($dataEnd, PHP_URL_QUERY);

						// Obtener todas las variables de la cadena de consulta y dividirlas en un array
						$variables = [];
						parse_str($queryString, $variables);

						// Inicializar la matriz para almacenar los pares variable-valor
						$_VARS = [];

						// Recorrer todas las variables y almacenarlas en la matriz $_VARS
						foreach ($variables as $variable => $value) {
							$_VARS[] = [
								'var' => $variable,
								'value' => $value
							];
						}
						define("_VARS", $_VARS);
						// Mostrar la matriz resultante
						//var_dump($_VARS);


						$cat = $this->matchCategory($catPath[0]);
						$worksheet = 1;
						$body = $this->fmt->worksheets->loadBody($cat, $worksheet);
						echo $this->fmt->worksheets->htmlReactive($body);
						exit(0);
					}

					if ($rootIndex[0] == "tag" && (!strrpos($dataEnd, ".html"))) {
						//echo "tag";

						$cat = $this->matchCategory('tag');
						$worksheet = 1;

						$_VARS = [];

						// Recorrer todas las variables y almacenarlas en la matriz $_VARS
						 
						$_VARS[] = [
								'tag' => $rootIndex[1],
							];
						
						define("_VARS", $_VARS);

						$body = $this->fmt->worksheets->loadBody($cat, $worksheet);
						echo $this->fmt->worksheets->htmlReactive($body);
						exit(0);
					}

					echo "error-404";
				}
			}
		}
	}
	public function getArrayUrl($url = "")
	{
		if (empty($url)) {
			$link = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
			//return $_SERVER["HTTP_HOST"];
			//return $_SERVER["REQUEST_URI"];
			//return _TYPE_URI.$link;
			//return _PATH_WEB;
			$link = str_replace(_PATH_WEB, "", _TYPE_URI . $link);
			$escaped_link = htmlspecialchars($link, ENT_QUOTES, 'UTF-8');
			return explode("/", $escaped_link);
		} else {
			$url = str_replace(_PATH_WEB, "", $url);
			$escaped_link = htmlspecialchars($url, ENT_QUOTES, 'UTF-8');
			return explode("/", $escaped_link);
		}
	}
	public function indexRoot()
	{
		define("_ROOT_GET", "");
		define("_PATH_DASHBOARD", "modules/dashboard/");
		//echo "indexRoot"; exit(0);

		if (_MULTI_SITE == "on") {
			//multisite

		} else {
			$rootIndex = $this->getArrayUrl();
			//var_dump($rootIndex); exit(0);
			//echo "end:" . end($rootIndex);
			//echo _TYPE_URI.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; exit(0);
			if ($this->matchCategory(end($rootIndex))) {
				//echo 'mach' . $this->matchCategory(end($rootIndex));
				$cat = $this->matchCategory(end($rootIndex));
				$this->enterSite($cat, 1);

			} else {
				//echo "EnterSite"; exit(0);
				if (empty(end($rootIndex))) {
					//define("_PAGE", "");
					//echo "caso ultima categoria econtrada;
					//echo "vacio root"; exit(0);
					$this->enterSite(1, 1);
				} else {
					$str = end($rootIndex);
					$dataEnd = end($rootIndex);

					//echo "str:" . $str;
					//echo "dataEnd:" . $dataEnd;

					if (strpos($str, "?") !== false) {
						$rootIndex = $this->getArrayUrl();
						$rootAux = explode("?", $rootIndex[0]);
						//var_dump($rootAux);
						//var_dump(trim($rootAux[1]));
						define("_ROOT_GET", trim($rootAux[1]));

						$str = $rootAux[0];

						$queryString = parse_url($url, PHP_URL_QUERY);
						$catPath = explode("?", $dataEnd);
						$variables = $catPath[1];
						// Parsear la cadena de consulta en un array asociativo
						$queryString = parse_url($dataEnd, PHP_URL_QUERY);

						// Obtener todas las variables de la cadena de consulta y dividirlas en un array
						$variables = [];
						parse_str($queryString, $variables);

						// Inicializar la matriz para almacenar los pares variable-valor
						$_VARS = [];

						// Recorrer todas las variables y almacenarlas en la matriz $_VARS
						foreach ($variables as $variable => $value) {
							$_VARS[] = [
								'var' => $variable,
								'value' => $value
							];
						}
						define("_VARS", $_VARS);

						if ($this->matchCategory($str)) {
							$cat = $this->matchCategory($str);
							$worksheet = 1;
							//echo "mach category";
							$this->enterSite($cat, $worksheet);
						} else {
							$this->enterSite(1, 1);
							// echo "error-404";
						}
						exit(0);
					}


					if (strrpos($dataEnd, ".html")) {
						//echo "strrpos";
						$countCats = count($rootIndex);
						if ($countCats > 3) {
							$catPath = $rootIndex[1] . "/" . $rootIndex[2];
						} else {
							$catPath = $rootIndex[1];
						}
						$cat = trim($this->matchCategory($catPath));
						define("_POST_ID", $this->fmt->posts->getPathPostToId(str_replace(".html", "", $dataEnd)));
						$worksheet = 2;
						//$body = $this->fmt->worksheets->loadBody($cat, $worksheet);
						$this->enterSite($cat, $worksheet);
						exit(0);
					}

					//$patronFB = '/fbclid=([^&]+)/';

					$patronFB = '/fbclid=([^&]+)/';

					if (preg_match($patronFB, $dataEnd, $coincidencias)) {
						//echo "strrpos ?fbclid=";
						$catPath = $rootIndex[0];
						$cat = trim($this->matchCategory($catPath));
						$worksheet = 1;
						$this->enterSite($cat, $worksheet);
						exit(0);
					}

					if ($rootIndex[0] == "rolex" && $dataEnd != "rolex") {

						$cat = trim($this->matchCategory("rolex"));
						//define("_PAGE", $rootIndex[1]);
						//echo _PAGE;
						$worksheet = 1;
						$this->enterSite($cat, $worksheet);
						exit(0);
					}

					//if(strrpos($str))

					echo "error-404";
					$this->enterSite(1, 1);
					//require( _PATH_HOST . str_replace(_PATH_PAGE."", "",$_SERVER["REQUEST_URI"]));
					//require( _PATH_HOST . str_replace("/"._PATH_PAGE, "",$_SERVER["REQUEST_URI"]));
				}
			}
		}
	}
	public function matchCategory($str)
	{
		/*  */
		$sql = "SELECT cat_id FROM categorys WHERE  cat_pathurl='" . $str . "' or cat_path='" . $str . "'  and cat_state='1' ";
		$rs = $this->fmt->querys->consult($sql);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			$row = $this->fmt->querys->row($rs);
			return $row["cat_id"];
		} else {
			return 0;
		}
	}

	public function enterSite($cat, $worksheet)
	{
		//echo "$cat, $worksheet"; exit(0);

		echo $this->fmt->worksheets->loadHeader($cat, $worksheet);
		echo $this->fmt->worksheets->loadBody($cat, $worksheet);
		echo $this->fmt->worksheets->loadFooter($cat, $worksheet);
	}

	public function validateSession($root, $key)
	{
		if (!empty(_PATH_LOGOUT)) {
			$pathLogout = _PATH_LOGOUT;
		} else {
			$pathLogout = _PATH_WEB;
		}

		if ($this->fmt->sessions->getVar("keySession") == false) {
			echo '<script type="text/javascript" >document.location.href="' . $pathLogout . '";</script>';
			exit(0);
		}
		require_once ($root);
	}

	public function user($vars = null)
	{
		$jsonVars = json_decode($vars);
		$inputEmail = $jsonVars->inputEmail;
		$inputPassword = $jsonVars->inputPassword;
		$error = [];
		if (!$this->fmt->validations->email($inputEmail)) {
			array_push($error, 'email');
		}
		if (empty($inputPassword)) {
			array_push($error, 'password');
		}
		if (!$error) {
			$userData = $this->fmt->users->userData($inputEmail, $inputPassword);
			if ($userData != 0) {
				$userData["signIn"] += true;

				$this->fmt->sessions->setSession(
					array(
						'userId' => $userData["user_id"],
						'userName' => $userData["user_name"],
						'userEmail' => $inputEmail,
						'userRol' => $userData["user_rol"]
					)
				);
				return json_encode($userData);
			} else {
				return json_encode(array('return' => 'error', 'error' => 'no-exist-acount'));
			}
		} else {
			return json_encode(array('return' => 'error', 'error' => json_encode($error)));
		}
		//$this->ftm->user->rolUser()
	}

	public function getInput($index, $value = '')
	{
		if (isset($_POST[$index]) && !empty($_POST[$index])) {
			$value = $_POST[$index];
		} else if (isset($_GET[$index]) && !empty($_GET[$index])) {
			$value = $_GET[$index];
		}

		return $value;
	}

	public function bearerToken()
	{
		return $this->fmt->options->getValue("bearer_token");
	}

	public function getApi()
	{
		return [
			'client_id' => $this->fmt->options->getValue("client_id"),
			'client_secret' => $this->fmt->options->getValue("client_secret"),
		];
	}

	public function validationToken($vars = null)
	{
		$access_token = $vars["access_token"];
		$refresh_token = $vars["refresh_token"];
		$return = [];

		$var = base64_decode($access_token);
		$arrayUserDates = explode(".", $var);
		$return["userId"] = $arrayUserDates[1];
		$return["rolId"] = $arrayUserDates[2];

		$sql = "SELECT DISTINCT user_tk_user_id FROM users_tokens WHERE user_tk_type='access_token' AND user_tk_user_id='" . $return["userId"] . "' AND user_tk_token='" . $access_token . "'";
		$rs = $this->fmt->querys->consult($sql);
		$num = $this->fmt->querys->num($rs);
		if ($num == 0) {
			return 0;
		}
		$this->fmt->querys->leave();

		$sql = "SELECT DISTINCT user_tk_user_id FROM users_tokens WHERE user_tk_type='refresh_token' AND user_tk_user_id='" . $return["userId"] . "' AND user_tk_token='" . $refresh_token . "'";
		$rs = $this->fmt->querys->consult($sql);
		$num = $this->fmt->querys->num($rs);
		if ($num == 0) {
			return 0;
		}
		$this->fmt->querys->leave();

		return $return;
	}

	public function createCode($vars = null)
	{
		$num = $this->fmt->emptyReturn(intval($vars["num"]), 6);
		$prefix = $this->fmt->emptyReturn($vars["prefix"], "");
		$mode = $this->fmt->emptyReturn($vars["mode"], "");

		switch ($mode) {
			case 'number':
				$pattern = "1234567890";
				break;

			default:
				$pattern = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%&*-";
				break;
		}

		//return  $pattern.":".$num;
		//$pattern = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%&*-";
		// $pattern = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$key = '';
		//return strlen($pattern);

		/* 		for ($i = 0; $i < $num; $i++) {

																																																																																																																																																																																																																																									$key .= $pattern{rand(1, strlen($pattern))};
																																																																																																																																																																																																																																								} */
		$key = substr(str_shuffle($pattern), 0, $num);

		return $prefix . $key;
	}

	public function validateBearerToken($token = null)
	{
		if ($this->bearerToken() == $token) {
			return 1;
		} else {
			return 0;
		}
	}

	public function validateTokenSession(array $var = null)
	{
		//return $var;
		$bearerToken = $var["bearerToken"];
		$clientId = $var["clientId"];
		$clientSecret = $var["clientSecret"];

		$bearerTokenSystem = $this->fmt->options->getValue("bearer_token");
		$clientIdSystem = $this->fmt->options->getValue("client_id");
		$clientSecretSystem = $this->fmt->options->getValue("client_secret");

		if ($bearerToken == $bearerTokenSystem && $clientId == $clientIdSystem && $clientSecret == $clientSecretSystem) {
			return 1;
		} else {
			return 0;
		}
	}

	public function getActionState($state = null)
	{
		if ($state == 1) {
			$rtr["Error"] = 0;
			$rtr["status"] = "success";
			$rtr["state"] = $state;
			echo json_encode($rtr);
			exit(0);
		} else {
			$rtr["Error"] = 1;
			$rtr["status"] = 'error';
			$rtr["state"] = 0;
			$rtr["message"] = $state;
			echo json_encode($rtr);
			exit(0);
		}
	}

	public function getActionReturnId($state = null)
	{
		if ($state != 0 && is_int($state)) {
			$rtr["Error"] = 0;
			$rtr["status"] = "success";
			$rtr["data"] = $state;
			echo json_encode($rtr);
			exit(0);
		} else if ($state === 0) {
			$rtr["Error"] = 1;
			$rtr["status"] = "error";
			$rtr["data"] = $state;
			$rtr["message"] = $state;
			echo json_encode($rtr);
			exit(0);
		} else {
			$rtr["Error"] = 1;
			$rtr["status"] = "error";
			$rtr["message"] = $state;
			echo json_encode($rtr);
			exit(0);
		}
	}

	public function getActionReturnArray($state = null)
	{
		if ($state === 0 || $state == null) {
			$rtr["Error"] = 0;
			$rtr["status"] = "success";
			$rtr["data"] = $state;
			$rtr["message"] = 'data 0';
			echo json_encode($rtr);
			exit(0);
		}
		if (count($state) > 0) {
			$rtr["Error"] = 0;
			$rtr["status"] = "success";
			//$rtr["state"] = $state;
			$rtr["data"] = $state;
			echo json_encode($rtr);
			exit(0);
		}

		$rtr["Error"] = 1;
		$rtr["status"] = "error";
		$rtr["data"] = "";
		$rtr["message"] = $state;
		echo json_encode($rtr);
		exit(0);
	}

	public function getActionReturnMessage($state = null)
	{
		//echo json_encode($state); exit(0);
		if (($state != "0") && ($state != "error")) {
			$rtr["Error"] = 0;
			$rtr["status"] = "success";
			$rtr["message"] = $state;
			$rtr["data"] = $state;
			echo json_encode($rtr);
			exit(0);
		}
		if (!(is_string($state)) || ($state == "error") || ($state === 0)) {
			$rtr["Error"] = 1;
			$rtr["status"] = "error";
			$rtr["message"] = $state;
			echo json_encode($rtr);
			exit(0);
		}

	}

	public function getReturnObject($state = [])
	{
		echo json_encode($state);
		exit(0);
	}

	function urlExists($url = NULL)
	{

		//var_dump(@fopen($url,"r")); exit(0);

		$curl = curl_init();

		curl_setopt_array(
			$curl,
			array(
				CURLOPT_URL => $url,
				CURLOPT_HEADER => false,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_FOLLOWLOCATION => false,
				CURLOPT_SSL_VERIFYHOST => 0,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_NOBODY => false,
			)
		);

		$response = curl_exec($curl);

		if (curl_errno($curl)) {
			$err = curl_errno($curl);
			$errmsg = curl_error($curl);
			$header = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		}

		curl_close($curl);

		$elm = explode(",", $response);

		//var_dump($elm); exit(0);

		if ($elm[0] != "error-404") {
			return 1;
		} else {
			return 0;
		}
	}
	function getFileUrl($url = NULL, $vars = NULL)
	{

		if (empty($url)) {
			return false;
		}

		$curl = curl_init();

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $vars);


		$response = curl_exec($curl);

		$err = curl_errno();
		$errmsg = curl_error();
		$header = curl_getinfo();

		// Obtener el código de respuesta
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		//cerrar conexión
		curl_close($ch);


		$header['errno'] = $err;
		$header['errmsg'] = $errmsg;
		$header['content'] = $response;

		// Aceptar solo respuesta 200 (Ok), 301 (redirección permanente) o 302 (redirección temporal)
		$accepted_response = array(200, 301, 302);
		if (in_array($httpcode, $accepted_response)) {
			return $header;
		} else {
			return false;
		}
	}

	public function setData($url)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_FILETIME, true);
		curl_setopt($curl, CURLOPT_NOBODY, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$header["header"] = curl_exec($curl);
		$header["info"] = curl_getinfo($curl);
		curl_close($curl);

		$header['errno'] = $err;
		$header['errmsg'] = $errmsg;
		$header['content'] = $response;

		$accepted_response = array(200, 301, 302);
		if (in_array($httpcode, $accepted_response)) {
			return $header;
		} else {
			return false;
		}

	}

}