<?PHP
header('Content-Type: text/html; charset=utf-8');
class NOTIFICATIONS
{
    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function getNotificationsReceiver(array $var = null)
    {
        //return $var;
        $receiverId = $var["receiverId"] ? $var["receiverId"] : 1;
        $state = $var["state"] ? $var["state"] : "='1'";
        $type = $var["type"] ? $var["type"] : "";
        $aux = "";
        if ($type != "") {
            $aux = "AND ntf_recipient_type='" . $type . "' ";
        }

        $now = $this->fmt->data->dateFormat();

        $sql = "SELECT * FROM notifications WHERE ntf_recipient_id='" . $receiverId . "' " . $aux . " AND ntf_state" . $state . " ORDER BY ntf_sent_at DESC LIMIT 100";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["ntf_id"];
                $return[$i]["id"] = $id;
                $return[$i]["type"] = $row["ntf_recipient_type"];
                $return[$i]["entityId"] = $row["ntf_ent_id"];
                $return[$i]["receiverId"] = $row["ntf_recipient_id"];
                $return[$i]["message"] = $row["ntf_message"];
                $return[$i]["registerDate"] = $row["ntf_sent_at"];
                $return[$i]["dateCompact"] = $this->fmt->data->timeCompact(["dateInit" => $now, "dateEnd" => $row["ntf_sent_at"]]);
                $return[$i]["customData"] = $row["ntf_custom_data"];
                $return[$i]["class"] = $row["ntf_class"];
                $return[$i]["customId"] = $row["ntf_custom_id"];
                $return[$i]["status"] = $row["ntf_status"];
                $return[$i]["state"] = $row["ntf_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function create(array $var = null)
    {
        $type = $var['type'];
        $emitterId = $var['emitterId'];
        $receiverId = $var['receiverId'];
        $message = $var['message'];
        $registerDate = $var['registerDate'];
        $json = $var['json'];
        $state = $var['state'];

        $insert = "ntf_type,ntf_emitter_id,ntf_receiver_id,ntf_message,ntf_register_date,ntf_json,ntf_state";
        $values = "'" . $type . "','" . $emitterId . "','" . $receiverId . "','" . $message . "','" . $registerDate . "','" . $json . "','" . $state . "'";
        $sql = "insert into notifications (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(ntf_id) as id from notifications";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        return $row["id"];
    }

    public function notificationStateUpdate(array $var = null)
    {
        //return $var;
        //0 delete, 1 register, 2 recieved 3 read 4. no recived 5.cancel	

        $id = $var['notificationId'];
        $state = $var['state'];

        $sql = "UPDATE notifications SET ntf_state='" . $state . "' WHERE ntf_id='" . $id . "'";
        $this->fmt->querys->consult($sql, __METHOD__);

        return 1;

    }

}