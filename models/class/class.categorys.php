<?php
header('Content-Type: text/html; charset=utf-8');
class CATEGORYS
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function dataId($catId = null, $entitieId = 1, $state = "='1'")
    {
        //return $var;
        $sql = "SELECT * FROM categorys WHERE cat_state" . $state . " AND cat_id='" . $catId . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $row_id = $row["cat_id"];
            $rowParentId = $row["mod_jbt_parent_id"];
            $return['id'] = $row["cat_id"];
            $return['name'] = $row["cat_name"];
            $return['description'] = $row["cat_description"];
            $return['path'] = $row["cat_path"];
            $return['pathurl'] = $row["cat_pathurl"];
            $return['configpath'] = $row["cat_configpath"];
            $return['parentId'] = $row["cat_parent_id"];
            $return['icon'] = $row["cat_icon"];
            $return['json'] = $row["cat_json"];
            $return['level'] = 0;
            $return['img'] = $this->fmt->files->dataBasicImg($row["cat_img"]);
            $return['banner'] = $this->fmt->files->dataBasicImg($row["cat_banner"]);
            if ($this->haveChildrenNodes($row_id, $entitieId)) {
                $return["children"] = $this->childrenNodes($row_id, $entitieId, 0);
                //$return[$i]["children"] = 1;
            } else {
                $return["children"] = 0;
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function dataIdBasic($catId = null, $entitieId = 1, $state = "='1'")
    {
        //return $var;
        $sql = "SELECT * FROM categorys WHERE cat_state" . $state . " AND cat_id='" . $catId . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $row_id = $row["cat_id"];
            $rowParentId = $row["mod_jbt_parent_id"];
            $return['id'] = $row["cat_id"];
            $return['name'] = $row["cat_name"];
            $return['description'] = $row["cat_description"];
            $return['path'] = $row["cat_path"];
            $return['pathurl'] = $row["cat_pathurl"];
            $return['configpath'] = $row["cat_configpath"];
            $return['parentId'] = $row["cat_parent_id"];
            $return['color'] = $row["cat_color"];
            $return['icon'] = $row["cat_icon"];
            $return['json'] = $row["cat_json"];
            $return['level'] = 0;
            $return['img'] = $this->fmt->files->dataBasicImg($row["cat_img"]);
            $return['banner'] = $this->fmt->files->dataBasicImg($row["cat_banner"]);

            return $return;
        } else {
            return 0;
        }

    }
    public function dataIdReturn($catId = null)
    {
        //return $var;
        $sql = "SELECT * FROM categorys WHERE cat_id='" . $catId . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $row_id = $row["cat_id"];
            $return['id'] = $row["cat_id"];
            $return['name'] = $row["cat_name"];
            $return['description'] = $row["cat_description"];
            $return['path'] = $row["cat_path"];
            $return['pathurl'] = $row["cat_pathurl"];
            $return['configpath'] = $row["cat_configpath"];
            $return['parentId'] = $row["cat_parent_id"];
            $return['icon'] = $row["cat_icon"];
            $return['json'] = $row["cat_json"];
            $return['level'] = 0;
            $return['state'] = $row["cat_state"];
            $return['img'] = $this->fmt->files->dataBasicImg($row["cat_img"]);
            $return['banner'] = $this->fmt->files->dataBasicImg($row["cat_banner"]);

            return $return;
        } else {
            return 0;
        }

    }

    public function getPathFromCatId($catId = 0)
    {
        //return $catId;

        $sql = "SELECT cat_path FROM categorys WHERE cat_id='" . $catId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["cat_path"];
        } else {
            return 0;
        }

    }

    function nodes($idParent = 0, $idEntitie = 1)
    {
        $sql = "SELECT * FROM categorys WHERE  cat_ent_id='" . $idEntitie . "' AND cat_parent_id='" . $idParent . "' ORDER BY cat_order";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $row_id = $row["cat_id"];
                $rowParentId = $row["mod_jbt_parent_id"];
                $return[$i]['id'] = $row["cat_id"];
                $return[$i]['name'] = $row["cat_name"];
                $return[$i]['description'] = $row["cat_description"];
                $return[$i]['path'] = $row["cat_path"];
                $return[$i]['pathurl'] = $row["cat_pathurl"];
                $return[$i]['configpath'] = $row["cat_configpath"];
                $return[$i]['icon'] = $row["cat_icon"];
                $return[$i]['color'] = $row["cat_color"];
                $return[$i]['json'] = $row["cat_json"];
                $return[$i]['level'] = 0;
                $return[$i]['parentId'] = $row["cat_parent_id"];
                $banner = [];


                if ($row["cat_img"]) {
                    $img["id"] = $row["cat_img"];
                    $img["pathurl"] = $this->fmt->files->imgId($row["cat_img"], "");
                    $img["name"] = $this->fmt->files->name($row["cat_img"], "");
                } else {
                    $img = 0;
                }

                if ($row["cat_banner"]) {
                    $banner["id"] = $row["cat_banner"];
                    $banner["pathurl"] = $this->fmt->files->imgId($row["cat_banner"], "");
                    $banner["name"] = $this->fmt->files->name($row["cat_banner"], "");
                } else {
                    $banner = 0;
                }

                $return[$i]['img'] = $img;
                $return[$i]['banner'] = $banner;
                $return[$i]['related'] = $row["cat_related"];
                $return[$i]['cls'] = $row["cat_cls"];
                $return[$i]['state'] = $row["cat_state"];
                if ($this->haveChildrenNodes($row_id, $idEntitie)) {
                    $return[$i]["children"] = $this->childrenNodes($row_id, $idEntitie, 0);
                    //$return[$i]["children"] = 1;
                } else {
                    $return[$i]["children"] = 0;
                }
            }

            return $return;
        } else {
            return 0;
        }

    }

    public function haveChildrenNodes($idParent, $idEntitie)
    {
        //$sql = "SELECT * FROM categorys WHERE cat_state='1' AND cat_ent_id='" . $idEntitie . "' AND cat_parent_id='" . $idParent . "'";
        $sql = "SELECT * FROM categorys WHERE cat_ent_id='" . $idEntitie . "' AND cat_parent_id='" . $idParent . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function childrenNodes($parentId = null, $entitieId = null, $level = null)
    {
        //$sql = "SELECT * FROM categorys WHERE cat_state > 0 AND cat_ent_id='" . $entitieId . "' AND cat_parent_id='" . $parentId . "' ORDER BY cat_order";
        $sql = "SELECT * FROM categorys WHERE cat_ent_id='" . $entitieId . "' AND cat_parent_id='" . $parentId . "' ORDER BY cat_order";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $level++;
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $row_id = $row["cat_id"];
                $banner = [];
                //$rowParentId = $row["mod_jbt_parent_id"];
                $return[$i]['id'] = $row["cat_id"];
                $return[$i]['name'] = $row["cat_name"];
                $return[$i]['description'] = $row["cat_description"];
                $return[$i]['path'] = $row["cat_path"];
                $return[$i]['pathurl'] = $row["cat_pathurl"];

                $return[$i]['configpath'] = $row["cat_configpath"];
                $return[$i]['icon'] = $row["cat_icon"];
                $return[$i]['color'] = $row["cat_color"];
                $return[$i]['json'] = $row["cat_json"];

                $return[$i]['level'] = $level;
                $return[$i]['parentId'] = $row["cat_parent_id"];

                if ($row["cat_img"]) {
                    $img = [];
                    $img["id"] = $row["cat_img"];
                    $img["pathurl"] = $this->fmt->files->imgId($row["cat_img"], "");
                    $img["name"] = $this->fmt->files->name($row["cat_img"], "");
                } else {
                    $img = 0;
                }

                if ($row["cat_banner"]) {
                    $banner = [];
                    $banner["id"] = $row["cat_banner"];
                    $banner["pathurl"] = $this->fmt->files->imgId($row["cat_banner"], "");
                    $banner["name"] = $this->fmt->files->name($row["cat_banner"], "");
                } else {
                    $banner = 0;
                }

                //$return[$i]['cat_img'] = $banner;
                $return[$i]['img'] = $img;
                $return[$i]['banner'] = $banner;
                $return[$i]['banner2'] = $this->fmt->files->imgId($row["cat_banner"], "");
                //$return[$i]['cat_banner'] =$banner;
                $return[$i]['related'] = $row["cat_related"];
                $return[$i]['cls'] = $row["cat_cls"];
                $return[$i]['state'] = $row["cat_state"];
                $return[$i]['order'] = $row["cat_order"];
                if ($this->haveChildrenNodes($row_id, $entitieId)) {
                    $return[$i]["children"] = $this->childrenNodes($row_id, $entitieId, $level);
                } else {
                    $return[$i]["children"] = 0;
                }
            }

            return $return;
        } else {
            return 0;
        }

    }

    public function nodesHtml(array $var = null)
    {
        $arrayNav = $var;
        $count = count($arrayNav);
        $navHtml .= '<ul class="nodes">';

        for ($iNav = 0; $iNav < $count; $iNav++) {
            $id = $arrayNav[$iNav]["id"];
            $name = $arrayNav[$iNav]["name"];
            $pathUrl = $arrayNav[$iNav]["pathurl"];
            $target = $arrayNav[$iNav]["target"];
            $level = $arrayNav[$iNav]["level"];
            $level = $arrayNav[$iNav]["level"];
            $path = $arrayNav[$iNav]["path"];

            $navHtml .= '<li class="node node-' . $iNav . '"  pathurl="' . $pathUrl . '"  >';
            $navHtml .= '   <a class="nod"  pathurl="' . $pathUrl . '" target="' . $target . '" level="' . $level . '" >' . $name . '</a>';
            /*if ($arrayNav[$iNav]["children"] != 0) {
                $navHtml .= '<div class="subMenu">';
                $navHtml .= '   <div class="container">';
                $navHtml .= '       <div class="header"><a item="'. $id.'" pathurl="'.$pathUrl.'" ><h2 class="titleSection">'.$name.'</h2></a></div>';
                $navHtml .= '       <div class="tbody">';
                $arrayCat = $arrayNav[$iNav]["children"];
                $countCat = count($arrayCat);
                for ($i = 0; $i < $countCat; $i++) {
                    $elem = $arrayCat[$i];
                    //var_dump($elem);
                    $pathurl = $elem["pathurl"];
                    $childen = $elem["children"];
                    $path = $elem["path"];
                    //var_dump($childen);
                    
                    if ($pathurl == "#"){
                        $headerSub = '<label class="label label-'.$i.'" item="'. $elem["id"].'"  pathurl="'.$elem["pathurl"].'">'. $elem["name"].'</label>';
                    }else{
                        $headerSub = '<div class="header"><a class="nod" item="'. $elem["id"].'" pathurl="'.$elem["pathurl"].'" ><h2 class="titleSection">'.$elem["name"].'</h2></a></div>';
                    }

                    if ($childen != 0) {
                        $navHtml .= '<div class="subMenuElem">';
                        $navHtml .= $headerSub;
                        $navHtml .= '       <div class="tbody">';
                        $arrayChild = $elem["children"];
                        $countChild = count($arrayChild);
                        for ($j = 0; $j < $countChild; $j++) {
                            $elemChild = $arrayChild[$j];
                            $navHtml .= '<a class="cat cat-' . $j . '" item="' . $elemChild["id"] . '" path="'.$path.'"  pathurl="' . $elemChild["pathurl"] . '" level="' . $elemChild["level"] . '" >' . $elemChild["name"] . '</a>';
                        }
                        $navHtml .= '       </div>';
                        $navHtml .= '</div>';
                    }else{
                        $navHtml .= '<a class="cat cat-' . $i . '" item="' . $elem["id"] . '" path="'.$path.'"  pathurl="' . $elem["pathurl"] . '" level="' . $elem["level"] . '" >' . $elem["name"] . '</a>';
                    }
                }


                $navHtml .= '       </div>';
                $navHtml .= '   </div>';
                $navHtml .= '</div>';
            }*/

            if ($arrayNav[$iNav]["children"] != 0) {
                $navHtml .= '<div class="subMenu">';

                $arrayCat = $arrayNav[$iNav]["children"];
                $countCat = count($arrayCat);
                for ($i = 0; $i < $countCat; $i++) {
                    $elem = $arrayCat[$i];
                    //var_dump($elem);
                    $pathurl = $elem["pathurl"];
                    $childen = $elem["children"];
                    $path = $elem["path"];

                    if ($pathurl == "#") {
                        $headerSub = '<label class="nod" data-item="' . $elem["id"] . '"  data-pathurl="' . $elem["pathurl"] . '" level="1">' . $elem["name"] . '</label>';
                    } else {
                        $headerSub = '<a class="nod" data-item="' . $elem["id"] . '" data-pathurl="' . $elem["pathurl"] . '" level="1"><span>' . $elem["name"] . '</span></a>';
                    }

                    $navHtml .= $headerSub;
                }

                $navHtml .= '</div>';
            }
            $navHtml .= '</li>';
        }
        $navHtml .= '</ul>';

        return $navHtml;
    }

    public function relationFrom($vars = null, $actives = 1)
    {
        //return $actives;
        $from = $vars["from"];
        $colId = $vars["colId"];
        $colCategory = $vars["colCategory"];
        $id = $vars["id"];
        $actives = $vars["actives"];
        $orderBy = $this->fmt->emptyReturn($vars["orderBy"], " cat_id ASC");

        if ($actives == 1) {
            $aux = "> 0";
        } else {
            $aux = ">= 0";
        }

        $sql = "SELECT cat_id,cat_name, cat_pathurl, cat_state FROM categorys," . $from . " WHERE " . $colCategory . "=cat_id AND " . $colId . "=" . $id . " AND cat_state " . $aux . " ORDER BY " . $orderBy;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return[$i]["id"] = $row["cat_id"];
                $return[$i]["name"] = $row["cat_name"];
                $return[$i]["pathurl"] = $row["cat_pathurl"];
                $return[$i]["state"] = $row["cat_state"];
                // $return[$i]["sql"] = $sql ;
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function getCategorys(array $var = null)
    {
        $entitieId = $var["entitieId"];
        return $this->nodes(0, $entitieId);
    }

    public function existPath(array $var = null)
    {
        $entitieId = $var["entitieId"];
        $path = $var["path"];

        $sql = "SELECT cat_id FROM categorys WHERE cat_ent_id='" . $entitieId . "' AND cat_path='" . $path . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["cat_id"];
        } else {
            return 0;
        }


    }

    public function insertNode(array $var = null)
    {
        //return $var;

        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];

        $inputName = $vars["inputName"];
        $inputPath = $vars["inputPath"];
        $inputPathurl = $vars["inputPathurl"];
        $inputDescription = $vars["inputDescription"];
        $inputOrder = $vars["inputOrder"];
        $inputParentId = $vars["inputParentId"];
        $inputRelated = $vars["inputRelated"];
        $inputIcon = $vars["inputIcon"];
        $inputColor = $vars["inputColor"];
        $inputColor = $vars["inputColor"];
        $inputBanner = $vars["inputBanner"];
        $inputConfigPath = $vars["inputConfigPath"];
        $inputCls = $vars["inputCls"];
        $inputJson = $vars["inputJson"];
        $inputState = $vars["inputState"];

        $statePath = $this->existPath(array("entitieId" => $entitieId, "path" => $inputPath));
        if ($statePath) {
            return 'path_exist-' . $statePath;
        }

        //Validate inputPathUrl here


        $insert = "cat_name,cat_path,cat_pathurl,cat_description,cat_order,cat_parent_id,cat_related,cat_icon,cat_color,cat_img,cat_banner,cat_ent_id,cat_configpath,cat_cls,cat_json,cat_state";
        $values = "'" . $inputName . "','" .
            $inputPath . "','" .
            $inputPathurl . "','" .
            $inputDescription . "','" .
            $inputOrder . "','" .
            $inputParentId . "','" .
            $inputRelated . "','" .
            $inputIcon . "','" .
            $inputColor . "','" .
            $inputImg . "','" .
            $inputBanner . "','" .
            $entitieId . "','" .
            $inputConfigPath . "','" .
            $inputCls . "','" .
            $inputJson . "','" .
            $inputState . "'";
        $sql = "insert into categorys (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(cat_id) as id from categorys";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $catId = $row["id"];

        return $catId;

    }

    public function deleteNode(array $var = null)
    {
        //return $var;
        $vars = $var["vars"];
        $entitieId = $var["entitieId"];
        $parentId = $vars["parentId"];
        $inputNodes = $vars["inputNodes"];
        $numParentChildren = $vars["numParentChildren"];
        $item = $vars["item"];

        if ($inputNodes == 1) {
            $sql = "DELETE FROM categorys WHERE cat_ent_id='" . $entitieId . "' AND cat_parent_id='" . $item . "'";
            $this->fmt->querys->consult($sql);

            $sql = "DELETE FROM categorys WHERE cat_ent_id='" . $entitieId . "' AND cat_id='" . $item . "'";
            $this->fmt->querys->consult($sql);

            $up_sqr6 = "ALTER TABLE categorys AUTO_INCREMENT=1";
            $this->fmt->querys->consult($up_sqr6, __METHOD__);

            return 1;

        } else {
            //Search for children
            $sql = "SELECT cat_id FROM categorys WHERE cat_ent_id='" . $entitieId . "' AND cat_parent_id='" . $item . "'";
            $rs = $this->fmt->querys->consult($sql, __METHOD__);
            $num = $this->fmt->querys->num($rs);
            if ($num > 0) {
                for ($i = 0; $i < $num; $i++) {
                    $row = $this->fmt->querys->row($rs);
                    $id = $row["cat_id"];
                    $return[$i]["id"] = $id;
                }
            } else {
                $return = 0;
            }


            //return $return;

            // Update parent children
            if ($return != 0) {
                $count = count($return);
                for ($i = 0; $i < $count; $i++) {
                    $order = $numParentChildren++;
                    $sql = "UPDATE categorys SET
                       cat_parent_id ='" . $parentId . "',
                       cat_order ='" . $order . "' 
                       WHERE cat_id = '" . $return[$i]["id"] . "' ";
                    $this->fmt->querys->consult($sql);
                }
            }

            //Delete node
            $sql = "DELETE FROM categorys WHERE cat_ent_id='" . $entitieId . "' AND cat_id='" . $item . "'";
            $this->fmt->querys->consult($sql);

            $up_sqr6 = "ALTER TABLE categorys AUTO_INCREMENT=1";
            $this->fmt->querys->consult($up_sqr6, __METHOD__);

            return 1;
        }
    }

    public function updateNode(array $var = null)
    {
        //return $var;

        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];

        $inputId = $vars["inputId"];
        $inputName = $vars["inputName"];
        $inputPath = $vars["inputPath"];
        $inputPathurl = $vars["inputPathurl"];
        $inputDescription = $vars["inputDescription"];
        $inputOrder = $vars["inputOrder"];
        $inputParentId = $vars["inputParentId"];
        $inputRelated = $vars["inputRelated"];
        $inputIcon = $vars["inputIcon"];
        $inputColor = $vars["inputColor"];
        $inputImage = $vars["inputImage"];
        $inputBanner = $vars["inputBanner"];
        $inputConfigPath = $vars["inputConfigPath"];
        $inputCls = $vars["inputCls"];
        $inputJson = $vars["inputJson"];
        $inputState = $vars["inputState"];

        $statePath = $this->existPath(array("entitieId" => $entitieId, "path" => $inputPath));
        if (($statePath != $inputId) && ($statePath != 0)) {
            //return 'path_exist'."-".$statePath ."-".$inputId;
            return 'path_exist';
        }

        $sql = "UPDATE categorys SET
                    cat_name ='" . $inputName . "',
                    cat_path ='" . $inputPath . "',
                    cat_pathurl ='" . $inputPathurl . "',
                    cat_description ='" . $inputDescription . "',
                    cat_parent_id ='" . $inputParentId . "',
                    cat_related ='" . $inputRelated . "',
                    cat_icon ='" . $inputIcon . "',
                    cat_color ='" . $inputColor . "',
                    cat_img ='" . $inputImage . "',
                    cat_banner ='" . $inputBanner . "',
                    cat_configpath ='" . $inputConfigPath . "',
                    cat_cls ='" . $inputCls . "',
                    cat_json ='" . $inputJson . "',
                    cat_state ='" . $inputState . "'
                    WHERE cat_id = '" . $inputId . "' AND cat_ent_id = '" . $entitieId . "'";
        $this->fmt->querys->consult($sql);

        return 1;

    }

    public function changeStateNode(array $var = null)
    {
        //return $var;
        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];

        $inputId = $vars["item"];
        $inputState = $vars["state"];

        $sql = "UPDATE categorys SET
                    cat_state ='" . $inputState . "'
                    WHERE cat_id = '" . $inputId . "' AND cat_ent_id = '" . $entitieId . "'";
        $this->fmt->querys->consult($sql);

        return 1;
    }

    public function deleteMedia(array $var = null)
    {
        //return $var;
        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        $items = $vars["ids"];


        $count = count($items);
        for ($i = 0; $i < $count; $i++) {
            $id = $items[$i];
            $this->fmt->files->deteleFileId($id);
        }
        return 1;
    }

    public function reorderCategorys(array $var = null)
    {
        //return $var;

        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $input = $var["vars"]["input"];

        $nodeParent = $input["node"];
        $categorys = $input["categorys"];

        $count = count($categorys);

        for ($i = 0; $i < $count; $i++) {
            $id = $categorys[$i];
            $sql = "UPDATE categorys SET
                    cat_order ='" . $i . "'
                    WHERE cat_id = '" . $id . "' AND cat_ent_id = '" . $entitieId . "'";
            $this->fmt->querys->consult($sql);
        }

        return 1;


    }
}