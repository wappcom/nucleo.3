<?PHP
header('Content-Type: text/html; charset=utf-8');
class MODULES
{
	var $fmt;

	function __construct($fmt)
	{
		$this->fmt = $fmt;
	}

	public function dataId($modId)
	{
		$sql = "SELECT * FROM modules WHERE mod_id='" . $modId . "' AND mod_state='1'";
		$rs = $this->fmt->querys->consult($sql);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			return $this->fmt->querys->row($rs);
		} else {
			return 0;
		}
		$this->fmt->querys->leave();
	}

	public function dataPathurl($pathurl)
	{
		$sql = "SELECT * FROM modules WHERE mod_pathurl='" . $pathurl . "' AND mod_state='1'";
		$rs = $this->fmt->querys->consult($sql);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			return $this->fmt->querys->row($rs);
		} else {
			return 0;
		}
		$this->fmt->querys->leave();
	}

	public function deleteItemModule($vars = null)
	{
		$var = $vars["vars"];
		$idItem = $var["item"];
		$idMod = $var["idMod"];
		$arrayModulo = $this->dataId($idMod);
		$bd = $arrayModulo["mod_db"];
		$bd_prefijo = $arrayModulo["mod_prefix_db"];
		$bd_relaciones = $arrayModulo["mod_relations_db"];

		if (!empty($bd_relaciones)) {
			$relaciones = explode(",", $bd_relaciones);
			$c_r = count($relaciones);
			for ($i = 0; $i < $c_r; $i++) {
				$filax = explode(":", $relaciones[$i]);
				$sql1 = "DELETE FROM " . $filax[0] . " WHERE " . $filax[1] . "='" . $idItem . "'";
				$this->fmt->querys->consult($sql1, __METHOD__);
			}
		}
		$sqle = "DELETE FROM " . $bd . " WHERE " . $bd_prefijo . "id='" . $idItem . "'";
		$this->fmt->querys->consult($sqle, __METHOD__);
		return $idItem;
	}

	public function deleteModuleItem(array $vars = null)
	{
		//return $vars;
		$var = $vars["vars"];
		$idItem = $var["item"];
		$module = $var["module"];

		$arrayModulo = $this->dataPathurl($module);
		$bd = $arrayModulo["mod_db"];
		$bd_prefijo = $arrayModulo["mod_prefix_db"];
		$bd_relaciones = $arrayModulo["mod_relations_db"];

		if (!empty($bd_relaciones)) {
			$relaciones = explode(",", $bd_relaciones);
			$c_r = count($relaciones);
			for ($i = 0; $i < $c_r; $i++) {
				$filax = explode(":", $relaciones[$i]);
				$sql1 = "DELETE FROM " . $filax[0] . " WHERE " . $filax[1] . "='" . $idItem . "'";
				$this->fmt->querys->consult($sql1, __METHOD__);

				$up_sqr6 = "ALTER TABLE " . $filax[0] . " AUTO_INCREMENT=1";
				$this->fmt->querys->consult($up_sqr6, __METHOD__);
			}
		}

		$sqle = "DELETE FROM " . $bd . " WHERE " . $bd_prefijo . "id='" . $idItem . "'";
		$this->fmt->querys->consult($sqle, __METHOD__);

		$up_sqr6 = "ALTER TABLE " . $bd . " AUTO_INCREMENT=1";
		$this->fmt->querys->consult($up_sqr6, __METHOD__);

		return 1;
	}

	public function deleteItem($vars = null)
	{
		$from = $vars["from"];
		$column = $vars["column"];
		$item = $vars["item"];
		$query = $this->fmt->emptyReturn($vars["query"], "");

		$sqle = "DELETE FROM " . $from . " WHERE " . $column . "='" . $item . "' " . $query;
		$this->fmt->querys->consult($sqle, __METHOD__);

		$up_sqr6 = "ALTER TABLE " . $from . " AUTO_INCREMENT=1";
		$this->fmt->querys->consult($up_sqr6, __METHOD__);

		return $item;
	}

	public function deleteModuleId(array $vars = null)
	{
		//return $var;
		$from = $vars["from"];
		$prefix = $vars["prefix"];
		$id = $vars["id"];

		if (!empty($id) && is_numeric($id) && $id != 0 && $id != null) {
			$sql = "DELETE FROM " . $from . " WHERE " . $prefix . "id = '" . $id . "'";
			$this->fmt->querys->consult($sql);
			$up_sqr6 = "ALTER TABLE  " . $from . " AUTO_INCREMENT=1";
			$this->fmt->querys->consult($up_sqr6, __METHOD__);

			return 1;
		}

		return 0;

	}

	public function delete(array $var = null)
	{
		//return $var;
		$table = $var["table"];
		$where = $var["where"] ? $var["where"] : "";
		$foreignKey = $var["foreignKey"] ? $var["foreignKey"] : "";

		if ($foreignKey == 1) {
			$sql = "SET FOREIGN_KEY_CHECKS=0";
			$this->fmt->querys->consult($sql, __METHOD__);
		} else {
			$pre = "";
			$post = "";
		}


		$sql = "DELETE FROM " . $table . " WHERE " . $where;


		$this->fmt->querys->consult($sql, __METHOD__);

		$sql = "SET FOREIGN_KEY_CHECKS=1";
		$this->fmt->querys->consult($sql, __METHOD__);

		$sql2 = "ALTER TABLE " . $table . " AUTO_INCREMENT = 1";
		$this->fmt->querys->consult($sql2, __METHOD__);

		$rtn["Error"] = 0;
		$rtn["status"] = "success";
		$rtn["message"] = "delete successfully";

		return $rtn;
	}

	public function truncate(string $table = null)
	{

		$sql = "TRUNCATE TABLE " . $table;
		$this->fmt->querys->consult($sql, __METHOD__);

		$sql2 = "ALTER TABLE " . $table . " AUTO_INCREMENT = 1";
		$this->fmt->querys->consult($sql2, __METHOD__);

		$rtn["Error"] = 0;
		$rtn["status"] = "success";
		$rtn["message"] = "truncate successfully";

		return $rtn;

	}

	public function changeStateModuleItem(array $vars = null)
	{
		//return $vars;
		$var = $vars["vars"];
		$item = $var["item"];
		$module = $var["module"];
		$state = $var["state"];

		$arrayModulo = $this->dataPathurl($module);
		$bd = $arrayModulo["mod_db"];
		$bd_prefijo = $arrayModulo["mod_prefix_db"];

		if ($state == 1) {
			$std = 0;
		}
		if ($state == 0) {
			$std = 1;
		}

		$sql = "UPDATE " . $bd . " SET
           " . $bd_prefijo . "state='" . $std . "'
           WHERE " . $bd_prefijo . "id ='" . $item . "'";
		$this->fmt->querys->consult($sql);

		return $std;
	}

	public function deleteRelation($vars = null)
	{
		//return $vars;
		$from = $vars["from"];
		$column = $vars["column"];
		$item = $vars["item"];

		$sqle = "DELETE FROM " . $from . " WHERE " . $column . "='" . $item . "'";
		$this->fmt->querys->consult($sqle, __METHOD__);

		return $item;
	}

	public function loadHeaderIndex()
	{
		$str = "";
		$strJs = "";
		$strCss = "";
		//$strJs = '<script type="module">' . "\n";
		/*$arraySettings = $this->settingsPath(_PATH_HOST.'modules');
			  $countSettings = count($arraySettings);
			  
			  
			  for ($i=0; $i < $countSettings; $i++) { 
				  $root = $arraySettings[$i];
				  $index  = file_get_contents(_PATH_HOST.$root);
				  $obj = json_decode($index,true);
				  $path = $obj["data"]["path"];
				  $rootIndex = $obj["data"]["indexJs"];
				  $rootCss = $obj["data"]["css"];
				   
				  if (!empty($rootIndex)){
					  $str .= '    <link rel="stylesheet" type="text/css" href="'._PATH_WEB.$path.$rootCss.'">'."\n"."\n";
				  }
				  if (!empty($rootIndex)){
					  $str .= '    <script type="text/javascript" language="javascript" src="'._PATH_WEB.$path.$rootIndex.'"></script>'."\n";
				  } 
				  
			  }*/

		$sql = "SELECT * FROM systems WHERE sys_state='1'";
		$rs = $this->fmt->querys->consult($sql);
		$numSys = $this->fmt->querys->num($rs);
		if ($numSys > 0) {
			for ($j = 0; $j < $numSys; $j++) {
				$row = $this->fmt->querys->row($rs);
				$pathSys[$j] = $row["sys_path"];
				$indexjsSys[$j] = $row["sys_indexjs"];
				$cssSys[$j] = $row["sys_css"];

				if (!empty($cssSys[$j])) {
					$strCss .= '	<link rel="stylesheet" type="text/css" href="' . _PATH_WEB_NUCLEO . $pathSys[$j] . $cssSys[$j] . '">' . "\n";
				}
				if (!empty($indexjSys[$j])) {
					//$strJs .= '	<script type="module" language="javascript"  src="' . _PATH_WEB_NUCLEO  . $pathSys[$j] . $indexjsSys[$j] . '"></script>' . "\n";
				}
			}
		}
		

		$sql = "SELECT * FROM modules WHERE mod_state='1'";
		$rs = $this->fmt->querys->consult($sql);
		$numSys = $this->fmt->querys->num($rs);
		if ($numSys > 0) {
			for ($j = 0; $j < $numSys; $j++) {
				$row = $this->fmt->querys->row($rs);
				$module = $row["mod_pathurl"];
				$path = $row["mod_path"];
				$indexjs = $row["mod_indexjs"];
				$css = $row["mod_css"];

				if (!empty($css)) {
					$aux = 0;
					for ($i = 0; $i < $numSys; $i++) {
						if ($cssSys[$i] == $css) {
							$aux = 1;
						}
					}
					if ($aux == 0) {
						$strCss .= '    <link rel="stylesheet" type="text/css" href="' . _PATH_WEB_NUCLEO . $path . $css . '">' . "\n";
					}
				}
			}
		}

		

		return $strCss . "\n";
	}

	public function routerUpdate(array $var = null)
	{
		//return $var;
		$strJs = "";

		$sql = "SELECT * FROM modules WHERE mod_state='1'";
		$rs = $this->fmt->querys->consult($sql);
		$numSys = $this->fmt->querys->num($rs);
		if ($numSys > 0) {
			for ($j = 0; $j < $numSys; $j++) {
				$row = $this->fmt->querys->row($rs);
				$module = $row["mod_pathurl"];
				$path = $row["mod_path"];
				$indexjs = $row["mod_indexjs"];

				if (!empty($indexjs)) {
					$aux = 0;
					for ($i = 0; $i < $numSys; $i++) {
						if ($indexjs[$i] == $indexjs) {
							$aux = 1;
						}
					}
					if ($aux == 0) {
						$pathModule = str_replace("modules", "", $path);
						//$pathModule =  $path;
						$strJs .= 'import * as ' . $module . ' from "..' . $pathModule . $indexjs . '";' . PHP_EOL;
						$strJs .= 'export{' . $module . '};' . PHP_EOL;
						//$strJs .= '    <script type="module" language="javascript"  src="' . _PATH_WEB_NUCLEO . $path . $indexjs . '"></script>' . "\n";
					}
				}
			}
		}

		$filename = _PATH_NUCLEO . "modules/components/router.js";
		if (file_exists($filename)) {

			$navFile = file_get_contents($filename);
			// $navReturn =  str_replace("//{{_JS}}","//hola", $navFile);
			if ($navFile == "") {
				$navFile = "/*init*/" . PHP_EOL . "/*end*/";
			}
			$pattern = '/\/[*]init[*]\/(.*?(\n))+.*?\/[*]end[*]\//';
			$data = preg_replace($pattern, "/*init*/" . PHP_EOL . $strJs . "/*end*/", $navFile);
			//echo $navReturn;
			//$data = file_put_contents($pathNav, $navReturn);


			$file_handle = fopen($filename, 'w');
			if ($file_handle == false) {
				die("No se pudo escribir en el archivo ($filename)");
			}
			fwrite($file_handle, $data);
			fclose($file_handle);


			//die("No se pudo abrir el archivo. $data");

		} else {
			die("No existe el archivo de configuración de componentes router.js  contenido: /*init*//*end*/");
		}
	}

	public function settingsPath($path)
	{
		$result = [];
		$cont = 0;
		$cdir = scandir($path);
		foreach ($cdir as $key => $value) {
			if (!in_array($value, array(".", "..", ".DS_Store"))) {
				foreach (glob($path . '/' . $value . '/settings.json') as $file) {
					$result[$cont] = str_replace(_PATH_HOST, "", $file);
					$cont++;
				}
			}
		}

		return $result;
	}

	public function returnTextLang($vars)
	{
		$locale = $vars["locale"];
		$type = $vars["type"];
		$logType = $vars["logType"];
		$code = $vars["code"];

		$index = file_get_contents(_PATH_NUCLEO . "controllers/lang/" . $locale . ".json");
		$vars = (json_decode($index, true));

		switch ($type) {
			case 'code':
				$str = $vars[$type];
				$str = $str[$logType];
				$str = $str[$code];

				return $str;
				break;

			default:
				return "";
				break;
		}
	}

	public function listModulesSystem($userId, $pathSettings)
	{
		$html = '';
		$settings = file_get_contents($pathSettings);
		$obj = json_decode($settings, true);
		$array = $obj["modules"];
		$numArray = count($array);
		for ($i = 0; $i < $numArray; $i++) {
			$idMod = $array[$i]["id"];
			$html .= '<button class="itemModulo itemModulo-' . $i . ' itemModulo' . $idMod . '"  for="box-' . $idMod . '">
                        <i class="icon ' . $array[$i]["icon"] . '"></i>
                        <span>' . $array[$i]["name"] . '</span>
                        </button>';
			if ($array[$i]["subModules"]) {
				$html .= '<div class="boxSubModules" id="box-' . $idMod . '">';
				$arraySubModulos = $array[$i]["subModules"];
				$numArraySubModulos = count($arraySubModulos);
				for ($j = 0; $j < $numArraySubModulos; $j++) {
					$idSubMod = $arraySubModulos[$j]["id"];
					$html .= '<button class="itemSubModulo itemSubModulo-' . $j . ' itemSubModulo' . $idSubMod . '">
                        <i class="icon icon-chevron-right"></i>
                        <span>' . $arraySubModulos[$j]["name"] . '</span>
                    </button>';
				}

				$html .= '</div>';
			}
		}
		return $html;
	}

	public function menu($userId, $entitieId)
	{
		// $arraySettings = $this->settingsPath(_PATH_HOST.'modules');
		// $countSettings = count($arraySettings);
		$str = "";
		$sys = [];
		$tabs = [];

		//return var_dump($arraySettings);

		$rolId = $this->fmt->users->idRolUser($userId);

		// System Actives
		if ($rolId != 1) {
			$sqlx = "SELECT rol_sys_sys_id FROM roles_systems WHERE rol_sys_rol_id='" . $rolId . "' AND rol_sys_ent_id='" . $entitieId . "' ORDER BY rol_sys_order ASC";
			$rsx = $this->fmt->querys->consult($sqlx);
			$numSysRol = $this->fmt->querys->num($rsx);
			if ($numSysRol > 0) {
				for ($x = 0; $x < $numSysRol; $x++) {
					$row = $this->fmt->querys->row($rsx);
					$arraySysRol[$x] = $row["rol_sys_sys_id"];
				}
			}
			$this->fmt->querys->leave($rsx);
		} else {
			$sqlx = "SELECT sys_id FROM systems WHERE sys_state='1' ORDER BY sys_order ASC";
			$rsx = $this->fmt->querys->consult($sqlx);
			$numSysRol = $this->fmt->querys->num($rsx);
			if ($numSysRol > 0) {
				for ($x = 0; $x < $numSysRol; $x++) {
					$row = $this->fmt->querys->row($rsx);
					$arraySysRol[$x] = $row["sys_id"];
				}
			}
			$this->fmt->querys->leave($rsx);
		}

		for ($i = 0; $i < $numSysRol; $i++) {
			$sysId = $arraySysRol[$i];
			$obj = $this->fmt->systems->dataId($sysId);
			if ($obj != 0) {
				$sys[$i]["id"] = $obj["sys_id"];
				$sys[$i]["name"] = $obj["sys_name"];
				$sys[$i]["icon"] = $obj["sys_icon"];
				$sys[$i]["color"] = $obj["sys_color"];
				$sys[$i]["code"] = $obj["sys_code"];
				$sys[$i]["type"] = "system";
				$sys[$i]["path"] = $obj["sys_path"];
				$sys[$i]["modDefault"] = $obj["sys_mod_default"];
				$sys[$i]["pathUrl"] = $obj["sys_pathurl"];
			} else {
				$sys = 0;
			}
		}

		$sqlTab = "SELECT DISTINCT user_tab_sys_id,user_tab_sys_active,user_tab_start_mod_id,user_tab_order FROM users_tabs,users WHERE user_tab_user_id='" . $userId . "' AND user_tab_ent_id='" . $entitieId . "' ORDER BY user_tab_order  ASC";
		$rsTab = $this->fmt->querys->consult($sqlTab);
		$numTabs = $this->fmt->querys->num($rsTab);
		if ($numTabs > 0) {
			for ($j = 0; $j < $numTabs; $j++) {
				$row = $this->fmt->querys->row($rsTab);
				$sys_id = $row["user_tab_sys_id"];
				$tab_active = $row["user_tab_sys_active"];
				$tabs[$j]["id"] = $sys_id;

				$modId = $row["user_tab_start_mod_id"];

				$dataMod = $this->fmt->modules->dataId($modId);

				$tabs[$j]["modIdActive"] = $modId;
				$tabs[$j]["modPathUrlActive"] = $dataMod["mod_pathurl"];

				if ($tab_active == 1) {
					$tabs[$j]["active"] = "active";
				} else {
					$tabs[$j]["active"] = "";
				}
			}
		}
		$this->fmt->querys->leave($rsTab);

		$personal[0]["icon"] = "icon-p";
		$personal[0]["color"] = "#6ED059";
		$personal[0]["name"] = "Mi Panel";
		$personal[0]["path"] = "panel";
		$personal[0]["action"] = "panel";
		$personal[0]["type"] = "personal";

		$personal[1]["icon"] = "icon-a";
		$personal[1]["color"] = "#6F91E9";
		$personal[1]["name"] = "Mis Apps rapidas";
		$personal[1]["path"] = "apps-fast";
		$personal[1]["action"] = "apps-fast";
		$personal[1]["type"] = "personal";

		$personal[2]["icon"] = "icon-tool";
		$personal[2]["color"] = "#FEBF10";
		$personal[2]["name"] = "Herramientas";
		$personal[2]["path"] = "apps-tools";
		$personal[2]["action"] = "apps-tools";
		$personal[2]["type"] = "personal";


		$adm[0]["icon"] = "icon-conf";
		$adm[0]["color"] = "#444444";
		$adm[0]["name"] = "Configuración";
		$adm[0]["path"] = "config";
		$adm[0]["action"] = "config";
		$adm[0]["type"] = "admin";


		$adm[1]["icon"] = "icon-system";
		$adm[1]["color"] = "#8e43e7";
		$adm[1]["name"] = "Sistemas";
		$adm[1]["path"] = "systems";
		$adm[1]["action"] = "systems";
		$adm[1]["type"] = "admin";

		$adm[2]["icon"] = "icon-users";
		$adm[2]["color"] = "#6F91E9";
		$adm[2]["name"] = "Usuarios";
		$adm[2]["path"] = "users";
		$adm[2]["action"] = "users";
		$adm[2]["type"] = "admin";

		$adm[3]["icon"] = "icon-credential";
		$adm[3]["name"] = "Roles";
		$adm[3]["color"] = "#89C6FF";
		$adm[3]["path"] = "roles";
		$adm[3]["action"] = "roles";
		$adm[3]["type"] = "admin";

		$adm[6]["icon"] = "icon-group";
		$adm[6]["name"] = "Grupos";
		$adm[6]["color"] = "#FFC452";
		$adm[6]["path"] = "groups";
		$adm[6]["action"] = "groups";
		$adm[6]["type"] = "admin";

		$menu = [];

		// if ($numSysRol !=0 || $rolId==1 ){
		if ($numSysRol != 0) {
			$menu["sidebar"]["systems"]["label"] = "Sistemas Activos";
			$menu["sidebar"]["systems"]["data"] = $sys;
		} else {
			$menu["sidebar"]["systems"] = "0" . $rolId;
		}

		$menu["sidebar"]["personal"]["label"] = "Personal";
		$menu["sidebar"]["personal"]["data"] = $personal;

		if ($rolId == 1 || $rolId == 2) {
			$menu["sidebar"]["administrator"]["label"] = "Administrador";
			$menu["sidebar"]["administrator"]["data"] = $adm;
		} else {
			$menu["sidebar"]["administrator"] = 0;
		}

		if ($numTabs > 0) {
			$menu["tabs"] = $tabs;
		} else {
			$menu["tabs"] = 0;
		}

		//$menu["sql"] = $sqlTab; 
		//$menu["entitieId"] = $entitieId; 
		//$menu["userId"] = $userId; 
		//$menu["row"] = $sqlx; 
		//$menu["sys"] = $sys; 
		//$menu["numSysRol"] = $numSysRol; 


		return $menu;
	}

	public function dateFormat($zona = "America/La_Paz", $formato = "Y-m-d H:i:s", $setlocale = "es_ES")
	{
		setlocale(LC_TIME, $setlocale);
		date_default_timezone_set($zona);
		return date($formato);
	}

	public function dateFormate(array $var = null)
	{
		$zone = $var["zone"] ? $var["zone"] : "America/La_Paz";
		$format = $var["format"] ? $var["format"] : "Y-m-d H:i:s";
		$setlocale = $var["setlocale"] ? $var["setlocale"] : "es_ES";
		setlocale(LC_TIME, $setlocale);
		date_default_timezone_set($zone);
		return date($format);
	}

	public function formatDate($vars)
	{
		$fecha = $vars["date"];
		$modo = $this->fmt->emptyReturn($vars["mode"], "normal");
		$formato = $this->fmt->emptyReturn($vars["format"], "d,m,a,h,mi,s");
		$numDiaLiteral = $this->dayLiteralNum($fecha);
		$fechaHora = explode(" ", $fecha);
		$fechas = explode("-", $fechaHora[0]);
		$tiempo = explode(":", $fechaHora[1]);
		$ano = $fechas[0];
		$mes = (string) (int) $fechas[1];
		$dia = $fechas[2];
		$hora = $tiempo[0];
		$min = $tiempo[1];
		$seg = substr($tiempo[2], 0, 2);

		if ($modo == "mini") {
			$day = array(' ', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom');
			$month = array(' ', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');
		}
		if ($modo == "normal" || $modo = "lineal") {
			$day = array(' ', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');
			$month = array(' ', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
		}

		if ($modo == "number") {
			$day = array(' ', '01', '02', '03', '04', '05', '06', '07');
			$month = array(' ', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
		}

		$fx = explode(",", $formato);
		$nfx = count($fx);

		$F .= "<div class='block-date'>";

		$dl = "<span class='dayLiteral'>" . $day[$numDiaLiteral] . " </span>";
		$d = " <span class='day'>" . $dia . " </span>";
		$m = " <span class='month'>" . $month[$mes] . " </span>";
		$a = " <span class='year'>" . $ano . " </span>";
		$h = " <span class='hour'>" . $hora . "</span>";
		$mi = " <span class='min'>" . $min . "</span>";
		$s = " <span class='seg'>" . $seg . "</span>";

		for ($j = 0; $j < $nfx; $j++) {
			if ($fx[$j] == "dl") {
				$F .= $dl;
			}
			if ($fx[$j] == "d") {
				$F .= $d;
			}
			if ($fx[$j] == "m") {
				$F .= $m;
			}
			if ($fx[$j] == "a") {
				$F .= $a;
			}
			if ($fx[$j] == "h") {
				$F .= $h;
			}
			if ($fx[$j] == "mi") {
				$F .= $mi;
			}
			if ($fx[$j] == "s") {
				$F .= $s;
			}
		}

		$F .= "</div>";

		if ($formato == "d/m/a") {
			$F = "";
			if ($modo = "number") {
				$F .= $dia . "/" . $mes . "/" . $ano;
			} else {
				$F .= "<div class='block-date'>";
				$F .= $dia . "/" . $mes . "/" . $ano;
				$F .= "</div>";
			}
		}

		if ($modo = "linear") {
			$F = $dia . " de " . $month[$mes] . " de " . $ano;
		}

		return $F;
	}

	public function dayLiteralNum($date)
	{
		return strftime("%u", strtotime($date));
	}

	public function timeToDate($date, $format = "%H:%M")
	{
		return strftime($format, strtotime($date));
	}

	public function dayConverter($vars)
	{
		$day = $vars["day"];
		$type = $vars["type"];
		$lang = $vars["lang"];

		switch ($lang) {
			case 'es':
				$dayMin = array(' ', 'lun', 'mar', 'mie', 'jue', 'vie', 'sab', "dom");
				$dayNormal = array(' ', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'S&aacute;bado', "Domingo");
				break;
		}

		switch ($type) {
			case 'normal':
				for ($i = 0; $i < 8; $i++) {
					if ($dayMin[$i] == $day) {
						return $dayNormal[$i];
					}
				}
				break;

			case 'min':
				for ($i = 0; $i < 8; $i++) {
					if ($dayNormal[$i] == $day) {
						return $dayMin[$i];
					}
				}
				break;

			case 'numeral,min':
				return $dayMin[$day];
				break;

			case 'numeral,normal':
				return $dayNormal[$day];
				break;
		}
	}

	public function dateCompare($date1, $signalCompare = "=", $date2)
	{
		$date1 = strtotime($date1);
		$date2 = strtotime($date2);

		if ($signalCompare == ">") {
			if ($date1 > $date2) {
				return true;
			} else {
				return false;
			}
		}
		if ($signalCompare == "<") {
			if ($date1 < $date2) {
				return true;
			} else {
				return false;
			}
		}

		if ($signalCompare == "=") {
			if ($date1 == $date2) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function timeLeft($vars)
	{
		$desde = $vars["startDate"];
		$hasta = $vars["endDate"];
		$modo = $this->fmt->emptyReturn($vars["modo"], 'normal');

		$ini = explode(" ", $desde);
		$fIni = $ini[0];
		$hIni = $ini[1];
		$fIni = explode("-", $fIni);
		$hIni = explode(":", $hIni);

		$fin = explode(" ", $hasta);
		$fFin = $fin[0];
		$hFin = $fin[1];
		$fFin = explode("-", $fFin);
		$hFin = explode(":", $hFin);

		$anos = $fFin[0] - $fIni[0];
		$meses = $fFin[1] - $fIni[1];
		$dias = $fFin[2] - $fIni[2];

		$horas = $hFin[0] - $hIni[0];
		$minutos = $hFin[1] - $hIni[1];
		$segundos = $hFin[2] - $hIni[2];

		if ($segundos < 0) {
			$minutos--;
			$segundos = 60 + $segundos;
		}
		if ($minutos < 0) {
			$horas--;
			$minutos = 60 + $minutos;
		}

		if ($horas < 0) {
			if ($dias != 0) {
				//$dias--;
				$horas = 24 + $horas;
			}
		}

		//echo "d:".$dias;

		if ($dias << 0) {
			//echo "ingrese";
			if ($meses != 0) {
				//$meses--;

				switch ($fIni[1]) {
					case 1:
						$dias_mes_anterior = 30;
						break;
					case 2:
						if (checkdate(2, 29, $fIni[0])) {
							$dias_mes_anterior = 29;
							break;
						} else {
							$dias_mes_anterior = 28;
							break;
						}
					case 3:
						$dias_mes_anterior = 31;
						break;
					case 4:
						$dias_mes_anterior = 31;
						break;
					case 5:
						$dias_mes_anterior = 30;
						break;
					case 6:
						$dias_mes_anterior = 31;
						break;
					case 7:
						$dias_mes_anterior = 30;
						break;
					case 8:
						$dias_mes_anterior = 31;
						break;
					case 9:
						$dias_mes_anterior = 31;
						break;
					case 10:
						$dias_mes_anterior = 30;
						break;
					case 11:
						$dias_mes_anterior = 31;
						break;
					case 12:
						$dias_mes_anterior = 30;
						break;
				}
			}
		}

		if ($meses < 0) {
			--$anos;
			$meses = $meses + 12;
		}

		if ($anos == 0) {
			if ($meses == 0) {
				if ($dias == 0) {
					if ($horas == 0) {
						if ($minutos == 0) {
							if ($modo == "normal") {
								$tiempo = "Hace instantes";
							}
							if ($modo == "mini") {
								$tiempo = "5s";
							}
						} else {
							if ($modo == "normal") {
								$tiempo = "Hace " . $minutos . "min.";
							}
							if ($modo == "mini") {
								$tiempo = $minutos . "m.";
							}
						} // fin min
					} else {
						if ($horas < 0) {
							$tiempo .= $this->fmt->mensaje->programado();
							$tiempo .= $this->formatDate(array('fecha' => $desde));
						} else {
							if ($modo == "normal") {
								$tiempo = "Hace " . $horas . "hr.";
							}
							if ($modo == "mini") {
								$tiempo = $horas . "h.";
							}
						}
					} // fin horas
				} else {
					if ($dias < 0) {
						// menus de un día
						$tiempo .= $this->fmt->mensaje->programado();
						$tiempo .= $this->formatDate(array('fecha' => $desde));
					} else {
						// más de 1 día
						if ($dias == 1) {
							$tiempo = "Ayer";
						} else {
							if ($dias > 7) {
								$tiempo = $this->formatDate(array('fecha' => $desde));

								if ($modo == "mini") {
									$tiempo = $this->formatDate(array('fecha' => $desde, 'formato' => 'd,m,a', 'modo' => 'mini'));
								}
							} else {
								if ($modo == "normal") {
									$tiempo = "Hace " . $dias . " días.";
								}
								if ($modo == "mini") {
									$tiempo = $dias . "d.";
								}
							}
						}
					}
				} // fin +7 días
			} else {
				$d = $dias + $dias_mes_anterior;
				if ($d <= 7) {
					if ($modo == "normal") {
						$tiempo = "Hace " . $d . " días.";
					}
					if ($modo == "mini") {
						$tiempo = $d . "d.";
					}
				} else {
					//echo $meses;
					if ($meses < 0) {
						$tiempo = $this->fmt->mensaje->programado();
						$tiempo .= $this->formatDate(array('fecha' => $desde));
						if ($modo == "mini") {
							$tiempo = $this->fmt->mensaje->programado();
							$tiempo .= $this->formatDate(array('fecha' => $desde, 'formato' => 'd,m,a', 'modo' => 'mini'));
							//$tiempo .="hace menos de 1 mes";
						}
					} else {
						$tiempo = $this->formatDate(array('fecha' => $desde));
						if ($modo == "mini") {
							$tiempo = $this->formatDate(array('fecha' => $desde, 'formato' => 'd,m,a', 'modo' => 'mini'));
							//$tiempo .="hace mas de 1 mes";
						}
					}
				}
			} // fin meses
		} else {
			if ($modo == "normal") {
				$tiempo = $this->formatDate(array('fecha' => $desde));
			}
			if ($modo == "mini") {
				$tiempo = $this->formatDate(array('fecha' => $desde, 'formato' => 'd,m,a', 'modo' => 'mini'));
				//$tiempo .="hace";
			}
		} // fin años
		return $tiempo;
	}

	public function tablesBd()
	{
		$sql = "SELECT table_name AS name FROM information_schema.tables WHERE table_schema = '" . _DATA_BASE . "'";
		$rs = $this->fmt->querys->consult($sql);
		$num = $this->fmt->querys->num($rs);

		if ($num > 0) {
			for ($i = 0; $i < $num; $i++) {
				$row[$i] = $this->fmt->querys->row($rs);
			}
			return $row;
		} else {
			return 0;
		}
		$this->fmt->querys->leave();
	}

	public function modulesRols($rolId, $sysId, $entitieId)
	{
		if ($rolId == 1) {
			$sql = "SELECT mod_id,mod_pathurl FROM systems_modules,modules WHERE mod_state > 0 AND sys_mod_mod_id=mod_id AND sys_mod_sys_id='" . $sysId . "' AND sys_mod_ent_id='" . $entitieId . "' ORDER BY sys_mod_order ASC";
		} else {
			$sql = "SELECT mod_id,mod_pathurl FROM roles_modules,modules WHERE  rol_mod_mod_id=mod_id AND rol_mod_rol_id='" . $rolId . "' AND rol_mod_ent_id='" . $entitieId . "' AND mod_state > 0 ORDER BY rol_mod_order ASC";
		}

		$rs = $this->fmt->querys->consult($sql);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			for ($i = 0; $i < $num; $i++) {
				$row = $this->fmt->querys->row($rs);
				$return[$i]["mod_id"] = $row["mod_id"];
				$return[$i]["mod_pathurl"] = $row["mod_pathurl"];
			}
			return $return;
		} else {
			return 0;
		}
		$this->fmt->querys->leave();
	}

	public function sidebarMenuModulesHtml($vars)
	{
		$sysId = $vars["sysId"];
		$modules = $vars["modules"];
		$limitChars = $vars["limitChars"] ? $vars["limitChars"] : "21";
		//var_dump($modules);
		$idActive = $vars["idActive"];
		$count = count($modules);
		$objSys = $this->fmt->systems->dataId($sysId);
		$system = $objSys["sys_pathurl"];
		$str = '';

		for ($i = 0; $i < $count; $i++) {
			$obj = $this->fmt->modules->dataId($modules[$i]["mod_id"]);
			$id = $obj["mod_id"];
			$name = $obj["mod_name"];
			$icon = $obj["mod_icon"];
			$color = $obj["mod_color"];
			$code = $obj["mod_code"];
			$path = $obj["mod_path"];
			$pathUrl = $obj["mod_pathurl"];
			$indexJs = $obj["mod_indexjs"];
			$auxSubModules = '';
			$aux = "";

			if ($id == $idActive && $idActive != "") {
				$aux = "active";
			} else {
				if ($i == 0 && $idActive == "") {
					$aux = "active";
				} else {
					$aux = "";
				}
			}

			$str .= '<li class="">
						<a class="btn btnModule ' . $aux . '"  data-id="' . $id . '" data-sys="' . $sysId . '" name="' . $name . '" title="' . $name . '" path="' . $path . '" pathurl="' . $pathUrl . '" system="' . $system . '" module="' . $pathUrl . '" icon="' . $icon . '" color="' . $color . '">
							<i class="' . $icon . '"></i>
							<span>' . $this->fmt->data->limitText(array("text" => $name, "type" => "chars", "num" => $limitChars)) . '</span>
						</a>
						' . $auxSubModules . '
					</li>';
		}

		return $str;
	}

	function createCode($vars)
	{
		$quantity = $this->fmt->emptyReturn($vars["quantity"], 6);
		$prefix = $this->fmt->emptyReturn($vars["prefix"], "");
		$postfix = $this->fmt->emptyReturn($vars["postfix"], "");
		$mode = $this->fmt->emptyReturn($vars["mode"], "number");
		$key = "";

		switch ($mode) {
			case 'general':
				$pattern = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
				break;
			case 'number':
				$pattern = "123456789";
				break;
			default:
				$pattern = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
				break;
		}

		$key = substr(str_shuffle($pattern), 0, $quantity);

		$code = $prefix . $key . $postfix;

		return $code;
	}

	public function replaceRoot(string $module = null, string $system = null, string $str = null)
	{
		$response = str_replace("{{_MODULE}}", $module, $str);
		$response = str_replace("{{_SYSTEM}}", $system, $response);
		return $response;
	}

	public function getModules($vars)
	{
		$sql = "SELECT mod_id,mod_name,mod_icon,mod_color,mod_code,mod_path,mod_pathurl,mod_indexjs FROM modules WHERE mod_state > 0 ORDER BY mod_order ASC";
		$rs = $this->fmt->querys->consult($sql);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			for ($i = 0; $i < $num; $i++) {
				$row = $this->fmt->querys->row($rs);
				$return[$i]["mod_id"] = $row["mod_id"];
				$return[$i]["mod_name"] = $row["mod_name"];
				$return[$i]["mod_icon"] = $row["mod_icon"];
				$return[$i]["mod_color"] = $row["mod_color"];
				$return[$i]["mod_code"] = $row["mod_code"];
				$return[$i]["mod_path"] = $row["mod_path"];
				$return[$i]["mod_pathurl"] = $row["mod_pathurl"];
				$return[$i]["mod_indexjs"] = $row["mod_indexjs"];
			}
			return $return;
		} else {
			return 0;
		}
 
	}

	public function getSystemsModules(array $var = null)
	{
		//return $var;
		$sql = "SELECT * FROM systems_modules ";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			for ($i = 0; $i < $num; $i++) {
				$row = $this->fmt->querys->row($rs);
				$id = $row["mod_"];
				$return[$i]["id"] = $id;
				$return[$i][""] = $row["mod_"];
			}
			return $return;
		} else {
			return 0;
		}
		
	}

    /**
     * Obtiene el nombre de una entidad por su ID
     * @param int $entityId ID de la entidad
     * @return string Nombre de la entidad o cadena vacía si no existe
     */
    public function getEntityName(int $entityId)
    {
        $sql = "SELECT ent_name FROM entities WHERE ent_id = '" . $entityId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        
        if ($this->fmt->querys->num($rs) > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["ent_name"];
        }
        
        return "";
    }

    /**
     * Obtiene todos los datos de una entidad por su ID
     * @param int $entityId ID de la entidad
     * @return array|int Datos de la entidad o 0 si no existe
     */
    public function getEntitie(int $entityId)
    {
        $sql = "SELECT * FROM entities WHERE ent_id = '" . $entityId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        
        if ($this->fmt->querys->num($rs) > 0) {
            $row = $this->fmt->querys->row($rs);
            return [
                "id" => $row["ent_id"],
                "name" => $row["ent_name"],
                "path" => $row["ent_path"],
                "brand" => $row["ent_brand"],
                "code" => $row["ent_code"],
                "token" => $row["ent_token"],
                "recordDate" => $row["ent_record_date"],
                "type" => $row["ent_type"],
                "notes" => $row["ent_notes"],
                "state" => $row["ent_state"]
            ];
        }
        
        return 0;
    }

}