<?php
header('Content-Type: text/html; charset=utf-8');
class CONSTRUCTOR
{

	var $querys;
	var $sessions;

	var $auth;
	var $categorys;
	var $contents;
	var $data;
	var $docs;
	var $errors;
	var $files;
	var $forms;
	var $links;
	var $notifications;
	var $mails;
	var $menus;
	var $modules;
	var $options;
	var $systems;
	var $posts;
	var $pubs;

	var $tests;
	var $users;
	var $validations;
	var $worksheets;


	function __construct()
	{
		array_walk($_GET, array($this,'clearString'));
		array_walk($_POST, array($this,'clearString'));

		// ODB
		require_once("class.mysql.php");
		$querys = new MYSQL();
		$querys->connect();
		$this->querys = $querys;

		require_once("class.auth.php");
		require_once("class.categorys.php");
		require_once("class.contents.php");
		require_once("class.errors.php");
		require_once("class.data.php");
		require_once("class.docs.php");
		require_once("class.files.php");
		require_once("class.forms.php");
		require_once("class.links.php");
		require_once("class.mails.php");
		require_once("class.menus.php");
		require_once("class.modules.php");
		require_once("class.options.php");
		require_once("class.systems.php");
		require_once("class.posts.php");
		require_once("class.pubs.php");
		require_once("class.tests.php");
		require_once("class.users.php");
		require_once("class.validations.php");
		require_once("class.worksheets.php");
		require_once("class.notifications.php");


		$this->auth = new AUTH($this);
		$this->categorys = new CATEGORYS($this);
		$this->contents = new CONTENTS($this);
		$this->errors = new ERRORS($this);
		$this->data = new DATA($this);
		$this->docs = new DOCS($this);
		$this->files = new FILES($this);
		$this->forms = new FORMS($this);
		$this->links = new LINKS($this);
		$this->notifications = new NOTIFICATIONS($this);
		$this->mails = new MAILS($this);
		$this->menus = new MENUS($this);
		$this->modules = new MODULES($this);
		$this->options = new OPTIONS($this);
		$this->systems = new SYSTEMS($this);
		$this->posts = new POSTS($this);
		$this->pubs = new PUBS($this);
		$this->tests = new TESTS($this);
		$this->users = new USERS($this);
		$this->validations = new VALIDATIONS($this);
		$this->worksheets = new WORKSHEETS($this);

	}

	function emptyReturn($value, $return)
	{
		if (is_null($value)) {
			return $return;
		} else {
			if (empty($value)) {
				return $return;
			} else {
				return $value;
			}
		}
	}

	public function clearString(string $str = "")
	{
		$str = str_ireplace("SELECT", "", $str);
		$str = str_ireplace("COPY", "", $str);
		$str = str_ireplace("DELETE", "", $str);
		$str = str_ireplace("query", "", $str);
		$str = str_ireplace("QUERY", "", $str);
		$str = str_ireplace("DROP", "", $str);
		$str = str_ireplace("DUMP", "", $str);
		$str = str_ireplace(" OR ", "", $str);
		$str = str_ireplace(" AND ", "", $str);
		$str = str_ireplace("AND ", "", $str);
		$str = str_ireplace("%", "", $str);
		$str = str_ireplace("LIKE", "", $str);
		$str = str_ireplace("--", "", $str);
		$str = str_ireplace("^", "", $str);
		$str = str_ireplace("[", "", $str);
		$str = str_ireplace("]", "", $str);
		$str = str_ireplace("\'", "", $str);
		$str = str_ireplace("!", "", $str);
		$str = str_ireplace("¡", "", $str);
		$str = preg_replace("/'/", '"', $str);
		$str = str_replace('\'', '"', $str);
		$str = str_ireplace("`", '"', $str);
		$str = htmlspecialchars($str, ENT_QUOTES | ENT_HTML5, 'UTF-8');
		$str = stripslashes($str);

		return $str;
	}

	function convertPathUrl($str)
	{
		$str = utf8_decode($str);
		
		$str = strtolower($str);
		$str = str_replace(' ', '-', $str);
		$str = str_replace('?', '', $str);
		$str = str_replace('¿', '', $str);
		$str = str_replace('+', '', $str);
		$str = str_replace(':', '', $str);
		$str = str_replace('??', '', $str);
		$str = str_replace('`', '', $str);
		$str = str_replace('\'', '', $str);
		$str = str_replace('!', '', $str);
		$str = str_replace(',', '-', $str);
		$str = str_replace('(', '', $str);
		$str = str_replace(')', '', $str);
		$str = str_replace('"', '', $str);
		$str = str_replace(';', '', $str);
		$str = str_replace('%', '', $str);
		$str = str_replace('–', '-', $str);
		
		$origin = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿ?¿';
		$mod = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
		$str = strtr($str, utf8_decode($origin), $mod);
		$str = mb_convert_encoding( htmlspecialchars( $str, ENT_QUOTES, 'UTF-8' ), 'HTML-ENTITIES', 'UTF-8' );

		return $str;
	}

	public function setUrlNucleo($str)
	{
		if (strpos($str, '{{') !== false) {
			$return = str_replace('{{_PATH_WEB}}', _PATH_WEB, $str);
			$return = str_replace('{{_PATH_SERVER}}', _PATH_SERVER, $return);
			$return = str_replace('{{_PATH_HOST}}', _PATH_HOST, $return);
			$return = str_replace('{{_PATH_IMAGES}}', _PATH_IMAGES, $return);
			$return = str_replace('{{_PATH_DOCS}}', _PATH_DOCS, $return);
			$return = str_replace('{{_PATH_LOGOUT}}', _PATH_LOGOUT, $return);
			$return = str_replace('{{_PATH_FILES}}', _PATH_FILES, $return);
			$return = str_replace('{{_PATH_PAGE}}', _PATH_PAGE, $return);
			$return = str_replace('{{_PATH_WEB_NUCLEO}}', _PATH_WEB_NUCLEO, $return);
			$return = str_replace('{{_STATE_CONNECT}}', _STATE_CONNECT, $return);
			$return = str_replace('{{_ICONS_LINK}}', _ICONS_LINK, $return);

		} else {
			$return =  $str;
		}
		return $return;
	}

	public function active()
	{
		return "active";
	}

	public function varsKey(array $var = null)
	{
		//return $var;
		$rtn = [];
		foreach ($var as $key => $value) {
			$name = $var[$key]["name"];
			$rtn[$name] = $var[$key]["value"];
		}

		return $rtn;
	}

	public function jointActions(array $var = null){
		$access = $var['access'];

		$vs = $access['vs'];
		$vsDb = $this->options->version();

        if ($vs != $vsDb){
            $rtn["Error"] = 1;
            $rtn["status"] = 'version';
            $rtn["vs"] = $vsDb;
            echo json_encode($rtn);
			exit(0);
        }
		
	}

	public function test(string $var = null)
	{
		return $var;
		
	}
}