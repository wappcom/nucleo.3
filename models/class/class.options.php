<?PHP
header('Content-Type: text/html; charset=utf-8');
class OPTIONS
{

	var $fmt;

	function __construct($fmt)
	{
		$this->fmt = $fmt;
	}

	public function getValue($name, $autoload = "yes")
	{
		$sql = "SELECT option_value FROM options WHERE option_name='" . $name . "' AND autoload='" . $autoload . "'";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$row = $this->fmt->querys->row($rs);
		return $row["option_value"];
	}

	public function getItem($name, $autoload = "yes")
	{
		$sql = "SELECT option_value FROM options WHERE option_name='" . $name . "' AND autoload='" . $autoload . "'";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$row = $this->fmt->querys->row($rs);
		return $row["option_value"];
	}

	public function versionPlus()
	{
		$sql = "SELECT option_value FROM options WHERE option_name='site_version'";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$row = $this->fmt->querys->row($rs);

		$vs = $row["option_value"] + 1;
		$sql = "UPDATE options SET option_value='" . $vs . "' WHERE option_name='site_version'";
		$this->fmt->querys->consult($sql);

		return $vs;
	}

	public function version()
	{
		$sql = "SELECT option_value FROM options WHERE option_name='site_version'";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$row = $this->fmt->querys->row($rs);

		$vs = $row["option_value"];
		return $vs;
	}

	public function setValue ($name, $value, $autoload = "yes")
	{
		$sql = "UPDATE options SET option_value='" . $value . "' WHERE option_name='" . $name . "' AND autoload='" . $autoload . "'";
		$this->fmt->querys->consult($sql, __METHOD__);

		return 1;
	}

	public function createValue ($name, $value, $autoload = "yes")
	{
		$sql = "INSERT INTO options SET option_name='" . $name . "', option_value='" . $value . "', autoload='" . $autoload . "'";
		$this->fmt->querys->consult($sql, __METHOD__);

		return 1;
	}
}