<?php
header('Content-Type: text/html; charset=utf-8');

class MYSQL
{

	var $connection;
	var $fmt;

	var $fetchMode = PDO::FETCH_ASSOC;
	var $errorDB;
	var $sql_query;
	var $sql_array;
	var $sql_row;
	var $sqlQuery;

	function __construct()
	{
		$this->fmt = $fmt;
	}

	public function connect()
	{
		try {
			$this->connection = new PDO('mysql:host=' . _HOST . ';port=' . _PORT . ';dbname=' . _DATA_BASE . ';charset=' . _CHARSET, _USER, _PASSWORD, array(
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_PERSISTENT => true,
				PDO::ATTR_EMULATE_PREPARES => false,
				PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES " . _CHARSET,
			)
			);
		} catch (PDOException $e) {
			echo 'The connection to the database failed,  ' . $e->getMessage();
			exit(0);
		}
	}

	function consult($sql = "", $className = "", $params = [])
	{
		if (empty($sql)) {
			$this->errorDB = "Error, You have not specified a query SQL.";
			throw new Exception("A SQL query must be specified.");
			return [
				"Error" => 1,
				"status" => "error",
				"message" => "A SQL query must be specified."
			];
		}

		if ($className != "") {
			$className = ", ClassName: " . $className;
		}

		$consult = $this->connection->prepare($sql);
		$consult->setFetchMode($this->fetchMode);
		$success = $consult->execute($params);

		if (!$success) {
			$errorInfo = $consult->ErrorInfo();
			throw new Exception("Error with SQL query: " . $errorInfo[2], $errorInfo[1]);

			return [
				"Error" => 1,
				"status" => "error",
				"message" => "Error with SQL query: " . $errorInfo[2] . " " . $errorInfo[1],
				"errorInfo" => $errorInfo
			];
		}

		$this->sql_query = $consult;
		return $consult;
	}

	function request($query = '', $className = '', $params = [])
	{
		if (empty($query)) {
			throw new Exception("A SQL query must be specified.");
		}

		$statement = $this->connection->prepare($query);
		$statement->setFetchMode($this->fetchMode);
		$success = $statement->execute($params);

		if (!$success) {
			$errorInfo = $statement->errorInfo();
			throw new Exception("Error with SQL query: " . $errorInfo[2], $errorInfo[1]);
		}

		$this->sqlQuery = $statement;
		$result = $statement->fetchAll(PDO::FETCH_CLASS, $className);

		return [
			"error" => false,
			"status" => "success",
			"message" => "Query succeeded",
			"data" => $result
		];
	}

	function setFetchMode($type)
	{
		//FETCH_ASSOC o 	FETCH_NUM
		$this->fetchMode = $type;
	}

	function leave($rn)
	{
		$this->sql_query = null;
	}

	function row($rn)
	{
		return $rn->fetch();
	}

	function num($rn)
	{
		return $rn->rowCount();
	}

	function createDb($db)
	{
		$this->connection->exec("CREATE DATABASE IF NOT EXISTS $db");
		$rtn["Error"] = 0;
		$rtn["status"] = "success";
		$rtn["message"] = "Database create successfully";
		return $rtn;
	}

	function loadDbQuerys(array $vars = null)
	{
		$dbname = $vars["dbname"];
		$path = $vars["path"];

		try {
			$db = new PDO('mysql:host=' . _HOST . ';port=' . _PORT . ';dbname=' . $dbname . ';charset=' . _CHARSET, _USER, _PASSWORD);
			$sql = file_get_contents($path);
			$qr = $db->exec($sql);
			$rtn["Error"] = 0;
			$rtn["status"] = "success";
			$rtn["message"] = "Database loaded successfully";
			return $rtn;
		} catch (PDOException $e) {
			$rtn["Error"] = 1;
			$rtn["status"] = "error";
			$rtn["message"] = 'The connection to the database failed,  ' . $e->getMessage();
			return 0;
		}
	}

	public function truncate(array $var = null)
	{
		$sql = "TRUNCATE TABLE " . $var["table"];
		$this->consult($sql, __METHOD__);
		$sql = "ALTER TABLE " . $var["table"] . " AUTO_INCREMENT = 1";
		$this->consult($sql, __METHOD__);

		return 1;
	}

	public function fetch($rn)
	{
		if ($rn instanceof PDOStatement) {
			return $rn->fetch($this->fetchMode);
		}
		return false;
	}

	public function fetchAll($statement)
	{
		if ($statement instanceof PDOStatement) {
			return $statement->fetchAll($this->fetchMode);
		} else {
			throw new Exception("Invalid statement provided to fetchAll.");
		}
	}

}