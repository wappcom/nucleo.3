<?PHP
header('Content-Type: text/html; charset=utf-8');
class SESSIONS
{

	var $fmt;

	function __construct($fmt)
	{
		$this->fmt = $fmt;
	}

	public function start()
	{
		session_start();
	}

	public function setName($name)
	{
		session_name($name);
	}

	public function getName()
	{
		return session_name();
	}

	public function getId()
	{
		return session_id();
	}

	public function printSession()
	{
		return var_dump($_SESSION);
	}

	public function setVar($name, $value)
	{
		$_SESSION[$name] = $value;
	}

	public function getVar($name)
	{
		if ($this->existVar($name)) {
			return $_SESSION[$name];
		} else {
			return false;
		}
	}

	public function existVar($name)
	{
		if (isset($_SESSION[$name])) {
			return true;
		} else {
			return false;
		}
	}

	public function leave($name)
	{
		session_unset();
		if (isset($name)) {
			session_destroy();
		}
	}

	public function close()
	{
		session_destroy();
	}

	public function setSession($vars)
	{
		$this->setVar("userId", $vars["userId"]);
		$this->setVar("userName", $vars["userName"]);
		$this->setVar("userEmail", $vars["userEmail"]);
		$this->setVar("userRol", $vars["userRol"]);
		$idSession = uniqid();
		$this->setVar("keySession", $idSession);
	}
}