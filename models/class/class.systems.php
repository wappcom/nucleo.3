<?PHP
header('Content-Type: text/html; charset=utf-8');
class SYSTEMS
{

    var $fmt;
    var $modules;

    function __construct($fmt)
    {
        $this->fmt = $fmt;

        require_once("class.modules.php");
        $this->modules = new MODULES($fmt);
    }

    public function dataId($sysId)
    {
        $sql = "SELECT * FROM systems WHERE sys_id='" . $sysId . "' AND sys_state='1'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            return $this->fmt->querys->row($rs);
        } else {
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function pathToData($path)
    {
        $sql = "SELECT * FROM systems WHERE sys_path='" . $path . "' AND sys_state='1'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            return $this->fmt->querys->row($rs);
        } else {
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function getSystems(array $var = null)
    {
        //return $var;
        $sql = "SELECT * FROM systems WHERE sys_state > 0 ";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["sys_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["sys_name"];
                $return[$i]["description"] = $row["sys_description"];
                $return[$i]["pathurl"] = $row["sys_pathurl"];
                $return[$i]["path"] = $row["sys_path"];
                $return[$i]["code"] = $row["sys_code"];
                $return[$i]["icon"] = $row["sys_icon"];
                $return[$i]["color"] = $row["sys_color"];
                $return[$i]["parent_id"] = $row["sys_parent_id"];
                $return[$i]["indexjs"] = $row["sys_indexjs"];
                $return[$i]["css"] = $row["sys_css"];
                $return[$i]["order"] = $row["sys_order"];
                $return[$i]["state"] = $row["sys_state"];
                $return[$i]["modules"] = $this->modules->getModulesSystem($id);
            }
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }



}