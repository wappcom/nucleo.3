<?PHP
header('Content-Type: text/html; charset=utf-8');
class MENUS
{

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function items($menuId)
    {
        $sql = "SELECT * FROM menus_items WHERE menu_item_menu_id='" . $menuId . "'  ORDER BY menu_item_order ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["menu_item_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["menu_item_name"];

                $catId = $row["menu_item_cat_id"];
                $return[$i]["catId"] = $catId;

                $catActive = $row["menu_item_cat_active"];
                if ($catActive == "1") {
                    $return[$i]["cats"] = $this->fmt->categorys->nodes($catId);
                    $return[$i]["nodes"] = 0;
                } else {
                    $return[$i]["cats"] = 0;
                    $return[$i]["nodes"] = $this->nodes(array("id" => $id, "level" => 0));
                }

                $return[$i]["pathurl"] = $row["menu_item_pathurl"];
                $return[$i]["target"] = $row["menu_item_target"];
                $return[$i]["icon"] = $row["menu_item_icon"];
                $return[$i]["img"] = $row["menu_item_img"];
                $return[$i]["catActive"] = $row["menu_item_order"];
                $return[$i]["level"] = 0;
            }
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }


    public function nodes(array $var = null)
    {
        //return $var;
        $level = $var["level"] + 1;
        $sql = "SELECT * FROM menus_items WHERE menu_item_parent_id='" . $var["id"] . "' AND menu_item_state='1' ORDER BY menu_item_order ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["menu_item_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["menu_item_name"];
                $return[$i]["pathurl"] = $row["menu_item_pathurl"];
                $return[$i]["parentId"] = $row["menu_item_parent_id"];

                $catId = $row["menu_item_cat_id"];
                $return[$i]["catId"] = $catId;
                $catActive = $row["menu_item_cat_active"];
                if ($catActive == "1") {
                    $return[$i]["cats"] = $this->fmt->categorys->nodes($catId);
                    $return[$i]["nodes"] = 0;
                } else {
                    $return[$i]["cats"] = 0;
                    $return[$i]["nodes"] = $this->nodes(array("id" => $id, "level" => $level));
                }
                $return[$i]["catActive"] = $row["menu_item_cat_active"];
            }
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function nodesId($menuId)
    {
        $sql = "SELECT * FROM menus_items WHERE menu_id='" . $menuId . "'  WHERE menu_state='1'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["menu_id"];
            $return["id"] = $id;
            $return["name"] = $row["menu_name"];
            $return["description"] = $row["menu_description"];
            $return["items"] = $this->items($id);
            return $return;
        } else {
            return 0;
        }
    }

    public function nav(array $arrayNav = null, $catId = 0, $level = 0)
    {
        //return $arrayNav;
        $count = count($arrayNav);
        $navHtml .= '<ul class="navNodes">';

        for ($iNav = 0; $iNav < $count; $iNav++) {
            $id = $arrayNav[$iNav]["id"];
            $name = $arrayNav[$iNav]["name"];
            $pathUrl = $arrayNav[$iNav]["pathurl"];
            $target = $arrayNav[$iNav]["target"];
            $level = $arrayNav[$iNav]["level"];
            $level = $arrayNav[$iNav]["level"];
            $path = $arrayNav[$iNav]["path"];

            $navHtml .= '<li class="node node-' . $iNav . '"  pathurl="' . $pathUrl . '" level="' . $level . '" >';
            $navHtml .= '   <a class="nod"  pathurl="' . $pathUrl . '" target="' . $target . '" >' . $name . '</a>';
            if ($arrayNav[$iNav]["cats"] != 0) {
                $navHtml .= '<div class="subMenu">';
                $navHtml .= '   <div class="container">';
                $navHtml .= '       <div class="header"><a item="' . $id . '" pathurl="' . $pathUrl . '" ><h2 class="titleSection">' . $name . '</h2></a></div>';
                $navHtml .= '       <div class="tbody">';
                $arrayCat = $arrayNav[$iNav]["cats"];
                $countCat = count($arrayCat);
                for ($i = 0; $i < $countCat; $i++) {
                    $elem = $arrayCat[$i];
                    //var_dump($elem);
                    $pathurl = $elem["pathurl"];
                    $childen = $elem["children"];
                    $path = $elem["path"];
                    $state = $elem["state"];
                    //var_dump($childen);
                    if ($state == 1) {
                        if ($pathurl == "#") {
                            $headerSub = '<label class="label label-' . $i . '" item="' . $elem["id"] . '"  pathurl="' . $elem["pathurl"] . '">' . $elem["name"] . '</label>';
                        } else {
                            $headerSub = '<div class="header"><a class="nod" item="' . $elem["id"] . '" pathurl="' . $elem["pathurl"] . '" ><h2 class="titleSection">' . $elem["name"] . '</h2></a></div>';
                        }

                        if ($childen != 0) {
                            $navHtml .= '<div class="subMenuElem">';
                            $navHtml .= $headerSub;
                            $navHtml .= '       <div class="tbody">';
                            $arrayChild = $elem["children"];
                            $countChild = count($arrayChild);
                            if ($state == 1) {
                                for ($j = 0; $j < $countChild; $j++) {
                                    $elemChild = $arrayChild[$j];
                                    $navHtml .= '<a class="cat cat-' . $j . '" item="' . $elemChild["id"] . '" path="' . $path . '"  pathurl="' . $elemChild["pathurl"] . '" level="' . $elemChild["level"] . '" >' . $elemChild["name"] . '</a>';
                                }
                            }
                            $navHtml .= '       </div>';
                            $navHtml .= '</div>';
                        } else {
                            $navHtml .= '<a class="cat cat-' . $i . '" item="' . $elem["id"] . '" path="' . $path . '"  pathurl="' . $elem["pathurl"] . '" level="' . $elem["level"] . '" >' . $elem["name"] . '</a>';
                        }
                    }
                }


                $navHtml .= '       </div>';
                $navHtml .= '   </div>';
                $navHtml .= '</div>';
            }
            $navHtml .= '</li>';
        }
        $navHtml .= '</ul>';
        return $navHtml;
    }
}