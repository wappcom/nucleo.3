<?php
header('Content-Type: text/html; charset=utf-8');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class MAILS
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function send($vars, $attachments = [])
    {
        $email = $vars["email"] ?? null;
        $name = $vars["name"] ?? null;
        $subject = $vars["subject"] ?? null;
        $setFromMail = $vars["setFromMail"] ?? null;
        $setFromName = $vars["setFromName"] ?? null;
        $body = $vars["body"] ?? null;
        $eUser = $vars["confirmReadingTo"] ?? null;
    
        if (!$email || !$setFromMail || !$setFromName || !$subject || !$body) {
            return [
                "Error" => 1,
                "status" => "error",
                "message" => "Missing required parameters"
            ];
        }
    
        if (!is_array($email) || empty($email)) {
            return [
                "Error" => 1,
                "status" => "error",
                "message" => "Email list is empty or not an array"
            ];
        }
    
        require_once(_PATH_NUCLEO . "vendor/php/PHPMailer/src/Exception.php");
        require_once(_PATH_NUCLEO . "vendor/php/PHPMailer/src/PHPMailer.php");
        require_once(_PATH_NUCLEO . "vendor/php/PHPMailer/src/SMTP.php");
    
        $mail = new PHPMailer(true);
    
        try {
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = _EMAIL_SMTP;
            $mail->SMTPAuth = true;
            $mail->Username = _EMAIL_USER;
            $mail->Password = _EMAIL_PASSWORD;
            $mail->SMTPSecure = _EMAIL_SEGURITY;
            $mail->Port = _EMAIL_PORT;
    
            $mail->setFrom($setFromMail, $setFromName);
            foreach ($email as $destinatario) {
                $mail->addAddress($destinatario, $name);
            }
    
            if (!empty($attachments) && is_array($attachments)) {
                foreach ($attachments as $attachment) {
                    if (file_exists($attachment)) {
                        $mail->addAttachment($attachment, basename($attachment));
                    } else {
                        return [
                            "Error" => 1,
                            "status" => "error",
                            "message" => "Attachment not found: $attachment"
                        ];
                    }
                }
            }
    
            $mail->IsHTML(true);
            $mail->Subject = $subject;
            $mail->Body = $body;
    
            if (!empty($eUser)) {
                $mail->AddCustomHeader("X-Confirm-Reading-To: $eUser");
                $mail->AddCustomHeader("Return-Receipt-To: $eUser");
                $mail->AddCustomHeader("Disposition-Notification-To: $eUser");
                $mail->ConfirmReadingTo = $eUser;
            }
    
            $mail->CharSet = 'UTF-8';
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
    
            $send = $mail->send();
    
            return [
                "Error" => 0,
                "status" => "success",
                "message" => $send
            ];
        } catch (Exception $e) {
            return [
                "Error" => 1,
                "status" => "error",
                "message" => "Error: " . $e->getMessage(),
                "messageInfo" => $mail->ErrorInfo
            ];
        }
    }
}