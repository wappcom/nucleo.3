<?php
header('Content-Type: text/html; charset=utf-8');
class DOCS
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function dataId(int $id = null, int $state = 0, $catReturn = 1)
    {
        if ($state == 1) {
            $stateSql = 'AND doc_state > 0';
        } else {
            $stateSql = '';
        }

        $sql = "SELECT * FROM docs WHERE doc_id='" . $id . "' " . $stateSql;
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);

            $fileId = $row["doc_file_id"];

            $return["id"] = $id;
            $return["title"] = $row["doc_title"];
            $return["description"] = $row["doc_description"];
            $return["tags"] = $row["doc_tags"];

            $return["fileId"] = $row["doc_file_id"];
            $file = $this->fmt->files->dataItem($fileId);
            $return["file"]["id"] = $fileId;
            $return["file"]["name"] = $file["name"];
            $return["file"]["pathurl"] = $file["pathurl"];
            $return["file"]["ext"] = $file["ext"];

            $return["date"] = $row["doc_date"];

            if ($row["doc_img"]) {
                //saber si el elemento es un numero o una cadena

                //buscar si termina en .pdf
                if (strpos($file["pathurl"], ".pdf") !== false) {
                    $img = str_replace(".pdf", ".jpg", $file["pathurl"]);
                    $return["file"]["image"] = $img;
                    $return["file"]["thumb"] = str_replace(".jpg", "-thumb.jpg", $img);
                } else {
                    $img = $file["pathurl"];
                    $return["file"]["image"] = $img;
                    $return["file"]["thumb"] = str_replace(".jpg", "-thumb.jpg", $img);
                }
                

                 
                $imgId = $row["doc_img"];
                $img = [];
                $img["id"] = $imgId;
                $img["pathurl"] = $this->fmt->files->imgId($row["doc_img"], "");
                $img["thumb"] = $this->fmt->files->thumb($img["pathurl"]);
                $img["name"] = $this->fmt->files->name($row["post_img"], "");
                

            } else {
                $img = 0;
            }

            $return["img"] = $img;

            $userId = $row["doc_user_id"];
            $return["user"] = $this->fmt->users->fullName($userId, 1);
            $return["userId"] = $row["doc_user_id"];
            $return["json"] = $row["doc_json"];
            $return["state"] = $row["doc_state"];

            if ($catReturn == 1) {
                $return["categorys"] = $this->relationCats(["docId" => $id, "entitieId" => 1]);
            }

            return $return;
        } else {
            return 0;
        }

    }
    public function getDocs(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $state = $var["state"] ?? '>0';
        $limit = $var["limit"] ?? "LIMIT 0,100";
        $order = $var["order"] ?? "doc_date DESC";

        $sql = "SELECT * FROM docs WHERE doc_ent_id='" . $entitieId . "' AND doc_state " . $state . " ORDER BY " . $order . " " . $limit;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["doc_id"];
                $return[$i] = $this->dataId($id, 1, 0); //id, state 1, catReturn 0
            }
            return $return;
        } else {
            return 0;
        }
    }
    public function getDataDocs(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];

        $rtn["getDocs"] = $this->getDocs(array("entitieId" => $entitieId));
        $rtn["categorys"] = $this->fmt->categorys->getCategorys(array("entitieId" => $entitieId));
        $rtn["date"] = $this->fmt->modules->dateFormat("", "Y-m-d H:i");
        //$return["Autors"] = $this->getAutors(array("entitieId" => $entitieId));

        return $rtn;
    }

    public function getDataDoc(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $inputId = $var["vars"]["inputs"]["inputId"];

        $rtn["getDoc"] = $this->dataId($inputId, 0, 1);
        $rtn["categorys"] = $this->fmt->categorys->getCategorys(array("entitieId" => $entitieId));
        $rtn["date"] = $this->fmt->modules->dateFormat("", "Y-m-d H:i");
        //$return["Autors"] = $this->getAutors(array("entitieId" => $entitieId));
        return $rtn;
    }

    public function relationCat(array $vars = null)
    {
        $catId = $vars["catId"];
        //$docId = $vars["docId"];
        $entitieId = $vars["entitieId"] ?? 1;
        $order = $this->fmt->emptyReturn($vars["order"], "doc_cat_order ASC");
        $limit = $vars["limit"];
        $and = $vars["and"];

        if (!empty($limit)) {
            $limit = "LIMIT " . $limit;
        } else {
            $limit = "LIMIT 0,100";
        }

        if (!empty($and)) {
            $and = "AND " . $and;
        } else {
            $and = "";
        }


        $sql = "SELECT DISTINCT * FROM docs,docs_categorys WHERE doc_cat_cat_id='" . $catId . "' AND doc_cat_doc_id=doc_id AND doc_cat_ent_id='" . $entitieId . "' AND doc_state > 0 " . $and . "  ORDER BY " . $order . " " . $limit;
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["doc_id"];
                $rtn[$i] = $this->dataId($id, 1, 0); //id, state 1, catReturn 0
            }
            return $rtn;
        } else {
            return 0;
        }

    }

    public function relationCats(array $vars = null)
    {
        //return $vars;
        $docId = $vars["docId"];
        $entitieId = $vars["entitieId"];
        $order = $this->fmt->emptyReturn($vars["order"], " doc_cat_order ASC");
        $limit = $this->fmt->emptyReturn($vars["limit"], " LIMIT 0,100");

        $sql = "SELECT DISTINCT cat_id, cat_name, cat_pathurl FROM docs,docs_categorys, categorys WHERE doc_cat_doc_id='" . $docId . "' AND doc_cat_cat_id=cat_id AND     
        doc_cat_ent_id='" . $entitieId . "' AND doc_state > 0 ORDER BY " . $order . " " . $limit;
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return[$i]["id"] = $row["cat_id"];
                $return[$i]["name"] = $row["cat_name"];
                $return[$i]["pathurl"] = $row["cat_pathurl"];
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function addDoc(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $userId = $var["user"]["userId"];
        $inputTitle = $var['vars']['inputs']["inputTitle"];
        $inputDescription = $var['vars']['inputs']["inputDescription"];
        $inputDoc = $var['vars']['inputs']["inputDoc"];
        $inputDate = $var['vars']['inputs']["inputDate"];
        $inputImage = $var['vars']['inputs']["inputImage"];
        $inputTags = $var["vars"]["inputs"]["inputTags"];
        $inputJson = $var['vars']['inputs']["inputJson"];
        $categorys = $var['vars']['inputs']['categorys'];

        $now = $this->fmt->modules->dateFormat();

        $insert = "doc_file_id,doc_title,doc_description,doc_tags,doc_img,doc_date,doc_register_date,doc_user_id,doc_json,doc_ent_id,doc_state";
        $values = "'" . $inputDoc . "','" . $inputTitle . "','" . $inputDescription . "','" . $inputTags . "','" . $inputImage . "','" . $inputDate . "','" . $now . "','" . $userId . "','" . $inputJson . "','" . $entitieId . "','1'";
        $sql = "insert into docs  (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(doc_id) as id from docs";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $docId = $row["id"];

        $num = count($categorys);
        for ($i = 0; $i < $num; $i++) {
            $insert = "doc_cat_doc_id,doc_cat_cat_id,doc_cat_ent_id,doc_cat_order";
            $values = "'" . $docId . "','" . $categorys[$i] . "','" . $entitieId . "','" . $i . "'";
            $sql = "insert into docs_categorys (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        return $docId;

    }

    public function updateDoc(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $userId = $var["user"]["userId"];
        $inputId = $var['vars']['inputs']["inputId"];
        $inputTitle = $var['vars']['inputs']["inputTitle"];
        $inputDescription = $var['vars']['inputs']["inputDescription"];
        $inputTags = $var["vars"]["inputs"]["inputTags"];
        $inputDoc = $var['vars']['inputs']["inputDoc"];
        $inputDate = $var['vars']['inputs']["inputDate"];
        $inputImage = $var['vars']['inputs']["inputImage"];
        $inputJson = $var['vars']['inputs']["inputJson"];
        $categorys = $var['vars']['inputs']['categorys'];

        $sql = "UPDATE docs SET
            doc_title = '" . $inputTitle . "',
            doc_description = '" . $inputDescription . "',
            doc_tags = '" . $inputTags . "',
            doc_img = '" . $inputImage . "',
            doc_file_id = '" . $inputDoc . "',
            doc_date = '" . $inputDate . "',
            doc_json = '" . $inputJson . "'
        WHERE doc_id = '" . $inputId . "' ";
        $this->fmt->querys->consult($sql);

        $this->fmt->modules->deleteRelation(array("from" => "docs_categorys", "column" => "doc_cat_doc_id", "item" => $inputId));

        $num = count($categorys);
        for ($i = 0; $i < $num; $i++) {
            $insert = "doc_cat_doc_id,doc_cat_cat_id,doc_cat_ent_id,doc_cat_order";
            $values = "'" . $inputId . "','" . $categorys[$i] . "','" . $entitieId . "','" . $i . "'";
            $sql = "insert into docs_categorys (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        return 1;

    }

    public function deleteItemDoc(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $item = $var["vars"]["item"];

        $array = $this->dataId($item, 1, 0);

        //return $array;
        //return _PATH_HOST_FILES.$array["file"]["pathurl"];
        $file = $array["file"]["pathurl"];
        $img = str_replace(".pdf", ".jpg", $file);
        $thumb = str_replace(".jpg", "-thumb.jpg", $img);

        if ($array["img"] != 0) {
            $imgId = $array["img"]["id"];
            $imgCustom = _PATH_HOST_FILES . $array["img"]["pathurl"];
            $this->fmt->files->deleteId($imgId);
            $this->fmt->files->deleteFile($imgCustom);


        }

        $this->fmt->files->deleteFile(_PATH_HOST_FILES . $file);
        $this->fmt->files->deleteFile(_PATH_HOST_FILES . $img);
        $this->fmt->files->deleteFile(_PATH_HOST_FILES . $thumb);


        $sql = "DELETE FROM docs WHERE doc_id = '" . $item . "'";
        $this->fmt->querys->consult($sql, __METHOD__);
        $sql = "ALTER TABLE  docs AUTO_INCREMENT = 1";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "DELETE FROM docs_categorys WHERE  doc_cat_doc_id = '" . $item . "'";
        $this->fmt->querys->consult($sql, __METHOD__);



        return 1;

    }

    public function getLastDocCategory($catId = 0)
    {
        //return $catId;

        $sql = "SELECT * FROM docs,docs_categorys WHERE doc_cat_cat_id='" . $catId . "' AND doc_cat_doc_id=doc_id AND doc_state > 0 ORDER BY doc_date DESC LIMIT 1";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["doc_id"];
            $return = $this->dataId($id, 1, 0); //id, state 1, catReturn 0
            return $return;
        } else {
            return 0;
        }

    }

    public function getLastDocDateCategory(array $var = null)
    {
        //return $var;
        //$entitieId = $var["entitieId"];
        $catId = $var["catId"];
        $date = $var["date"];

        $sql = "SELECT doc_id FROM docs,docs_categorys WHERE doc_cat_cat_id='" . $catId . "' AND doc_cat_doc_id=doc_id AND doc_state > 0 AND DATE(doc_date) = '" . $date . "' ORDER BY doc_id DESC LIMIT 1";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["doc_id"];
            $return = $this->dataId($id, 1, 0); //id, state 1, catReturn 0
            return $return;
        } else {
            return 0;
        }
    }
}