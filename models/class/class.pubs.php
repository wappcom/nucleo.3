<?php
header( 'Content-Type: text/html; charset=utf-8' );

class PUBS {

    var $fmt;

    function __construct( $fmt ) {
        $this->fmt = $fmt;
    }

    public function dataId( int $id = null ) {
        //return $var;
        $sql = "SELECT * FROM publications WHERE pub_id='" . $id . "' ";
        $rs = $this->fmt->querys->consult( $sql, __METHOD__ );
        $num = $this->fmt->querys->num( $rs );
        if ( $num > 0 ) {

            $row = $this->fmt->querys->row( $rs );
            $id = $row[ 'pub_id' ];
            $return[ 'id' ] = $id;
            $return[ 'name' ] = $row[ 'pub_name' ];
            $return[ 'title' ] = $row[ 'pub_title' ];
            $return[ 'summary' ] = $row[ 'pub_summary' ];
            $return[ 'description' ] = $row[ 'pub_description' ];
            $return[ 'path' ] = $row[ 'pub_path' ];
            $return[ 'pathUI' ] = $row[ 'pub_path_ui' ];
            $return[ 'pathIcon' ] = $row[ 'pub_path_icon' ];
            $return[ 'icon' ] = $row[ 'pub_icon' ];
            $return[ 'type' ] = $row[ 'pub_type' ];
            $return[ 'cls' ] = $row[ 'pub_class' ];
            $return[ 'attrId' ] = $row[ 'pub_attr_id' ];
            $return[ 'attr' ] = $row[ 'pub_attr' ];
            $return[ 'count' ] = $row[ 'pub_count' ];
            $return[ 'json' ] = $row[ 'pub_json' ];
            $return[ 'html' ] = $row[ 'pub_html' ];
            $return[ 'state' ] = $row[ 'pub_state' ];

            return $return;
        } else {
            return 0;
        }

    }

    public function publicationDataId( int $id = null ) {
        //return $var;
        $sql = "SELECT * FROM publications WHERE pub_id='" . $id . "' ";
        $rs = $this->fmt->querys->consult( $sql, __METHOD__ );
        $num = $this->fmt->querys->num( $rs );
        if ( $num > 0 ) {

            $row = $this->fmt->querys->row( $rs );
            $id = $row[ 'pub_id' ];
            $return[ 'id' ] = $id;
            $return[ 'name' ] = $row[ 'pub_name' ];
            $return[ 'title' ] = $row[ 'pub_title' ];
            $return[ 'summary' ] = $row[ 'pub_summary' ];
            $return[ 'description' ] = $row[ 'pub_description' ];
            $return[ 'path' ] = $row[ 'pub_path' ];
            $return[ 'pathUI' ] = $row[ 'pub_path_ui' ];
            $return[ 'pathIcon' ] = $row[ 'pub_path_icon' ];
            $return[ 'icon' ] = $row[ 'pub_icon' ];
            $return[ 'type' ] = $row[ 'pub_type' ];
            $return[ 'cls' ] = $row[ 'pub_class' ];
            $return[ 'attrId' ] = $row[ 'pub_attr_id' ];
            $return[ 'attr' ] = $row[ 'pub_attr' ];
            $return[ 'count' ] = $row[ 'pub_count' ];
            $return[ 'json' ] = $row[ 'pub_json' ];
            $return[ 'html' ] = $row[ 'pub_html' ];
            $return[ 'state' ] = $row[ 'pub_state' ];

            return $return;
        } else {
            return 0;
        }

    }

    public function set( $str, $arrayPub ) {

        if ( strpos( $str, '{{' ) !== false ) {

            $return = $this->fmt->setUrlNucleo( $str );
            $return = str_replace( '{{_PUB_ID}}', $arrayPub[ 'id' ], $return );
            $return = str_replace( '{{_PUB_NAME}}', $arrayPub[ 'name' ], $return );
            $return = str_replace( '{{_PUB_TITLE}}', $arrayPub[ 'title' ], $return );
            $return = str_replace( '{{_PUB_DESCRIPTION}}', $arrayPub[ 'description' ], $return );
            $return = str_replace( '{{_PUB_PATH}}', $arrayPub[ 'path' ], $return );
            $return = str_replace( '{{_PUB_PATH_UI}}', $arrayPub[ 'pathUI' ], $return );
            $return = str_replace( '{{_PUB_PATH_ICON}}', $arrayPub[ 'pathIcon' ], $return );
            $return = str_replace( '{{_PUB_TYPE}}', $arrayPub[ 'type' ], $return );
            $return = str_replace( '{{_PUB_CLASS}}', $arrayPub[ 'cls' ], $return );
            $return = str_replace( '{{_PUB_CLS}}', $arrayPub[ 'cls' ], $return );
            $return = str_replace( '{{_PUB_ATTR_ID}}', $arrayPub[ 'attrId' ], $return );
            $return = str_replace( '{{_PUB_ATTR}}', $arrayPub[ 'attr' ], $return );
            $return = str_replace( '{{_PUB_COUNT}}', $arrayPub[ 'count' ], $return );
            $return = str_replace( '{{_PUB_JSON}}', $arrayPub[ 'attrJson' ], $return );
            $return = str_replace( '{{_PUB_HTML}}', $arrayPub[ 'html' ], $return );
            $return = str_replace( '{{_PUB_STATE}}', $arrayPub[ 'state' ], $return );

        } else {
            $return = $str;
        }
        return $return;
    }

    public function dataPublications( array $var = null ) {
        //return $var;
        $entitieId = $var[ 'entitieId' ];
        $vars = $var[ 'vars' ];
        $siteId = $vars[ 'siteId' ];
        $order = $vars[ 'order' ] ? $vars[ 'order' ] : 'pub_id DESC';

        $sql = 'SELECT * FROM publications WHERE pub_state > 0 ORDER BY ' . $order;
        $rs = $this->fmt->querys->consult( $sql, __METHOD__ );
        $num = $this->fmt->querys->num( $rs );
        if ( $num > 0 ) {
            for ( $i = 0; $i < $num; $i++ ) {
                $row = $this->fmt->querys->row( $rs );
                $id = $row[ 'pub_id' ];
                $return[ $i ][ 'id' ] = $id;
                $return[ $i ][ 'name' ] = $row[ 'pub_name' ];
                $return[ $i ][ 'title' ] = $row[ 'pub_title' ];
                $return[ $i ][ 'summary' ] = $row[ 'pub_summary' ];
                $return[ $i ][ 'description' ] = $row[ 'pub_description' ];
                $return[ $i ][ 'path' ] = $row[ 'pub_path' ];
                $return[ $i ][ 'pathUI' ] = $row[ 'pub_path_ui' ];
                $return[ $i ][ 'pathIcon' ] = $row[ 'pub_path_icon' ];
                $return[ $i ][ 'icon' ] = $row[ 'pub_icon' ];
                $return[ $i ][ 'type' ] = $row[ 'pub_type' ];
                $return[ $i ][ 'cls' ] = $row[ 'pub_class' ];
                $return[ $i ][ 'attrId' ] = $row[ 'pub_attr_id' ];
                $return[ $i ][ 'attr' ] = $row[ 'pub_attr' ];
                $return[ $i ][ 'count' ] = $row[ 'pub_count' ];
                $return[ $i ][ 'json' ] = $row[ 'pub_json' ];
                $return[ $i ][ 'html' ] = $row[ 'pub_html' ];
                $return[ $i ][ 'state' ] = $row[ 'pub_state' ];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function dataPOST($data)
    {
        // Verifica si existe $data["html"] antes de procesarlo
        if (empty($data)) {
            return [];
        }

        // Si es un string, decodifica normalmente 
        if (is_string($data)) {
            return unserialize(stripslashes(urldecode($data)));
        }

        // Almacena el HTML si existe, con valor predeterminado null
        $html = isset($data['html']) ? $data['html'] : null;

        // Crea una copia del array original
        $cleanedData = $data;

        // Procesa cada elemento excepto el HTML
        foreach ($cleanedData as $key => $value) {
            // No procesa la clave 'html'
            if ($key === 'html') continue;

            // Limpia otros campos de texto
            if (is_string($value)) {
                $cleanedData[$key] = stripslashes(urldecode($value));
            }
        }

        // Restaura el HTML si existía
        if ($html !== null) {
            $cleanedData['html'] = $html;
        }

        return unserialize(serialize($cleanedData));
    }




    public function getUI( array $var = null ) {
        //return $var;

        $entitieId = $var[ 'entitieId' ];
        $id = $var[ 'vars' ][ 'id' ];

        $sql = "SELECT * FROM publications WHERE pub_id='" . $id . "' ";
        $rs = $this->fmt->querys->consult( $sql, __METHOD__ );
        $num = $this->fmt->querys->num( $rs );
        if ( $num>0 ) {
            $row = $this->fmt->querys->row( $rs );
            $id = $row[ 'pub_id' ];
            $url = _PATH_WEB . $row[ 'pub_path_ui' ];

            $return[ 'id' ] = $id;
            $return[ 'name' ] = $row[ 'pub_name' ];
            $return[ 'pathUI' ] = $row[ 'pub_path_ui' ];
            //$return[ 'ui' ] = $content;

            return $return;
        } else {
            return 0;
        }
    }

    public function getPublications( array $var = null ) {
        //return $var;
        return $this->dataPublications( $var );
    }

    public function getPubsTypesSiteId( array $var = null ) {
        //return $var;
        $entitieId = $var[ 'entitieId' ];
        $vars = $var[ 'vars' ];
        $siteId = $vars[ 'siteId' ];

        $sql = "SELECT * FROM sites WHERE site_id='" . $siteId . "' ";
        $rs = $this->fmt->querys->consult( $sql, __METHOD__ );
        $num = $this->fmt->querys->num( $rs );
        if ( $num > 0 ) {
            $row = $this->fmt->querys->row( $rs );
            $id = $row[ 'site_id' ];
            $pathHost = $row[ 'site_path_host' ];

            $pathPubs = _PATH_HOST . $pathHost . 'controllers/pub/';

            /* $arrayPathsPubs = $this->fmt->files->getPathFilesRoot( $pathPubs, 'json' )[ 'files' ];
            //root, filter
            $aux = count( $arrayPathsPubs );
            $return[ 'pathHost' ] = $pathHost;
            for ( $i = 0; $i<$aux; $i++ ) {
                //$return[ $i ][ 'path' ] = file_get_contents( $pathPubs.$arrayPathsPubs[ $i ][ 'path' ] );

                $return[ 'data' ][ $i ] = json_decode( file_get_contents( $pathPubs.$arrayPathsPubs[ $i ][ 'path' ] ) );

            }
            */

            $arrayPathsPubs = $this->fmt->files->getPathFilesRoot( $pathPubs, 'php' )[ 'files' ];
            //root, filter
            $aux = count( $arrayPathsPubs );
            $return[ 'pathHost' ] = $pathHost;

            for ( $i = 0; $i < $aux; $i++ ) {
                $elem = $arrayPathsPubs[ $i ][ 'path' ];
                if ( $elem != 'meta.pub.php' ) {
                    if ( strpos( $elem, '.pub' ) !== false ) {
                        file_get_contents( _PATH_WEB . 'sites/default/controllers/pub/' . $elem );
                        //var_dump( $http_response_header );
                        $data = [];
                        $data[ 'path' ] = $pathHost . 'controllers/pub/' . $elem;
                        $contAux = count( $http_response_header );

                        for ( $j = 0; $j < $contAux; $j++ ) {
                            $dataElem = $http_response_header[ $j ];
                            if ( strpos( $dataElem, 'name:' ) !== false ) {
                                $data[ 'name' ] = str_replace( 'name: ', '', $dataElem );
                            }
                            if ( strpos( $dataElem, 'title:' ) !== false ) {
                                $data[ 'title' ] = str_replace( 'title: ', '', $dataElem );
                            }
                            if ( strpos( $dataElem, 'description:' ) !== false ) {
                                $data[ 'description' ] = str_replace( 'description: ', '', $dataElem );
                            }
                            if ( strpos( $dataElem, 'pathIcon:' ) !== false ) {
                                $data[ 'pathIcon' ] = str_replace( 'pathIcon: ', '', $dataElem );
                            }
                            if ( strpos( $dataElem, 'icon:' ) !== false ) {
                                $data[ 'icon' ] = str_replace( 'icon: ', '', $dataElem );
                            }
                            if ( strpos( $dataElem, 'pathUI:' ) !== false ) {
                                $data[ 'pathUI' ] = str_replace( 'pathUI: ', '', $dataElem );
                            }
                            if ( strpos( $dataElem, 'cls:' ) !== false ) {
                                $data[ 'cls' ] = str_replace( 'cls: ', '', $dataElem );
                            }
                            if ( strpos( $dataElem, 'type:' ) !== false ) {
                                $data[ 'type' ] = str_replace( 'type: ', '', $dataElem );
                            }
                        }

                        $return[ 'data' ][ $i ] = $data;
                    }
                }
            }

            return $return;
        } else {
            return 0;
        }
    }

    public function savePublication( array $var = null ) {
        //return $var;
        $entitieId = $var[ 'entitieId' ];
        //$inputs = $this->fmt->forms->serializeToArray( json_decode( $var[ 'vars' ][ 'inputs' ], true ) );
        $inputs = $var[ 'vars' ][ 'inputs' ];

        if ( empty( $inputs[ 'inputName' ] ) ) {
            $rtn[ 'Error' ] = 1;
            $rtn[ 'status' ] = 'error';
            $rtn[ 'message' ] = 'El nombre no puede estar vacío.';
            return $rtn;
        }

        if ( empty( $inputs[ 'inputPath' ] ) ) {
            $rtn[ 'Error' ] = 1;
            $rtn[ 'status' ] = 'error';
            $rtn[ 'message' ] = 'El path no puede estar vacío elige una publicación origen.';
            return $rtn;
        }

        if ( $this->existNamePub( $inputs[ 'inputName' ] ) ) {
            $rtn[ 'Error' ] = 1;
            $rtn[ 'status' ] = 'error';
            $rtn[ 'message' ] = 'El nombre ya existe.';
            return $rtn;
        }

        $insert = 'pub_name,pub_description,pub_title,pub_summary,pub_path,pub_path_ui,pub_path_icon,pub_icon,pub_type,pub_class,pub_attr_id,pub_attr,pub_count,pub_json,pub_html,pub_state';
        $values = "'" . $inputs[ 'inputName' ] . "','" .
        $inputs[ 'inputDescription' ] . "','" .
        $inputs[ 'inputTitle' ] . "','" .
        $inputs[ 'inputSummary' ] . "','" .
        $inputs[ 'inputPath' ] . "','" .
        $inputs[ 'inputPathUI' ] . "','" .
        $inputs[ 'inputPathIcon' ] . "','" .
        $inputs[ 'inputIcon' ] . "','" .
        $inputs[ 'inputType' ] . "','" .
        $inputs[ 'inputCls' ] . "','" .
        $inputs[ 'inputAttrId' ] . "','" .
        $inputs[ 'inputAttr' ] . "','" .
        $inputs[ 'inputCount' ] . "','" .
        $inputs[ 'inputJson' ] . "','" .
        $inputs[ 'inputHtml' ] . "','" .
        $inputs[ 'inputState' ] . "'";

        $sql = 'insert into publications (' . $insert . ') values (' . $values . ')';
        $this->fmt->querys->consult( $sql, __METHOD__ );

        $sql = 'select max(pub_id) as id from publications';
        $rs = $this->fmt->querys->consult( $sql, __METHOD__ );
        $row = $this->fmt->querys->row( $rs );

        //return $row[ 'id' ];
        $rtn[ 'Error' ] = 0;
        $rtn[ 'status' ] = 'success';
        $rtn[ 'message' ] = 'Guardado correctamente.';
        $rtn[ 'data' ][ 'pubId' ] = $row[ 'id' ];
        return $rtn;

    }

    public function existNamePub( $name ) {

        $sql = "SELECT pub_id FROM publications WHERE pub_name = '" . $name . "'";
        $rs = $this->fmt->querys->consult( $sql, __METHOD__ );
        $num = $this->fmt->querys->num( $rs );
        if ( $num > 0 ) {
            return 1;
        } else {
            return 0;
        }
    }

    public function add( array $var = null ) {
        //return $var;
        $entitieId = $var[ 'entitieId' ];
        //$inputs = $this->fmt->forms->serializeToArray( json_decode( $var[ 'vars' ][ 'inputs' ], true ) );
        $inputs = $var[ 'vars' ][ 'inputs' ];

        if ( empty( $inputs[ 'inputName' ] ) ) {
            $rtn[ 'Error' ] = 1;
            $rtn[ 'status' ] = 'error';
            $rtn[ 'message' ] = 'El nombre no puede estar vacío.';
            return $rtn;
        }

        if ( empty( $inputs[ 'inputPath' ] ) ) {
            $rtn[ 'Error' ] = 1;
            $rtn[ 'status' ] = 'error';
            $rtn[ 'message' ] = 'El path no puede estar vacío elige una publicación origen.';
            return $rtn;
        }

        if ( $this->existNamePub( $inputs[ 'inputName' ] ) ) {
            $rtn[ 'Error' ] = 1;
            $rtn[ 'status' ] = 'error';
            $rtn[ 'message' ] = 'El nombre ya existe.';
            return $rtn;
        }

        $insert = 'pub_name,pub_description,pub_title,pub_summary,pub_path,pub_path_ui,pub_path_icon,pub_icon,pub_type,pub_class,pub_attr_id,pub_attr,pub_count,pub_json,pub_html,pub_state';
        $values = "'" . $inputs[ 'inputName' ] . "','" .
        $inputs[ 'inputDescription' ] . "','" .
        $inputs[ 'inputTitle' ] . "','" .
        $inputs[ 'inputSummary' ] . "','" .
        $inputs[ 'inputPath' ] . "','" .
        $inputs[ 'inputPathUI' ] . "','" .
        $inputs[ 'inputPathIcon' ] . "','" .
        $inputs[ 'inputIcon' ] . "','" .
        $inputs[ 'inputType' ] . "','" .
        $inputs[ 'inputCls' ] . "','" .
        $inputs[ 'inputAttrId' ] . "','" .
        $inputs[ 'inputAttr' ] . "','" .
        $inputs[ 'inputCount' ] . "','" .
        $inputs[ 'inputJson' ] . "','" .
        $inputs[ 'inputHtml' ] . "','" .
        $inputs[ 'inputState' ] . "'";

        $sql = 'insert into publications (' . $insert . ') values (' . $values . ')';
        $this->fmt->querys->consult( $sql, __METHOD__ );

        $sql = 'select max(pub_id) as id from publications';
        $rs = $this->fmt->querys->consult( $sql, __METHOD__ );
        $row = $this->fmt->querys->row( $rs );

        //return $row[ 'id' ];
        $rtn[ 'Error' ] = 0;
        $rtn[ 'status' ] = 'success';
        $rtn[ 'message' ] = 'Guardado correctamente.';
        $rtn[ 'data' ][ 'pubId' ] = $row[ 'id' ];
        return $rtn;

    }

    public function update( array $var = null ) {
        //return $var;

        $entitieId = $var[ 'entitieId' ];
        $inputs = $var[ 'vars' ][ 'inputs' ];

        $id = $inputs[ 'inputId' ];
        $name = $inputs[ 'inputName' ];
        $description = $inputs[ 'inputDescription' ] ?: '';
        $title = $inputs[ 'inputTitle' ] ?: '';
        $summary = $inputs[ 'inputSummary' ] ?: '';
        $path = $inputs[ 'inputPath' ] ?: '';
        $path_ui = $inputs[ 'inputPathUI' ] ?: '';
        $path_icon = $inputs[ 'inputPathIcon' ] ?: '';
        $icon = $inputs[ 'inputIcon' ] ?: '';
        $type = $inputs[ 'inputType' ] ?: 'host';
        $class = $inputs[ 'inputCls' ] ?: '';
        $attr_id = $inputs[ 'inputAttrId' ] ?: '';
        $attr = $inputs[ 'inputAttr' ] ?: '';
        $count = $inputs[ 'inputCount' ] ?: 0;
        $json = $inputs[ 'inputJson' ] ?: '';
        $html = $inputs[ 'inputHtml' ] ?: '';
        $state = $inputs[ 'inputState' ] ?: 0;

        $sql = "UPDATE publications SET 
                pub_name ='" . $name . "',
                pub_description = '" . $description . "',
                pub_title = '" . $title . "',
                pub_summary = '" . $summary . "',
                pub_path = '" . $path . "',
                pub_path_ui = '" . $path_ui . "',
                pub_path_icon = '" . $path_icon . "',
                pub_icon = '" . $icon . "',
                pub_type = '" . $type . "',
                pub_class = '" . $class . "',
                pub_attr_id = '" . $attr_id . "',
                pub_attr = '" . $attr . "',
                pub_count = '" . $count . "',
                pub_json = '" . $json . "',
                pub_html = '" . $html . "',
                pub_state = '" . $state . "'
                WHERE pub_id = '" . $id . "' ";
        $this->fmt->querys->consult( $sql );

        return 1;

    }

    public function updateField( array $var = null ) {
        //return $var;
        $id = $var[ 'pubId' ];
        $field = $var[ 'field' ];
        $value = $var[ 'value' ];

        if ( $value == '' ) {
            return 0;
        }

        if ( $field == '' ) {
            return 0;
        }

        $sql = 'UPDATE publications SET ' . $field . " = '" . $value . "' WHERE pub_id = '" . $id . "'";
        $this->fmt->querys->consult( $sql );

        return 1;
    }

    public function delete( array $var = null ) {
        //return $var;
        $vars = $var[ 'vars' ];
        $id = $vars[ 'id' ];

        $sql = "DELETE FROM publications WHERE pub_id='" . $id . "'";
        $this->fmt->querys->consult( $sql );
        $up_sqr6 = 'ALTER TABLE publications AUTO_INCREMENT=1';
        $this->fmt->querys->consult( $up_sqr6, __METHOD__ );

        return 1;
    }

    public function duplicatePublication( array $var = null ) {
        //return $var;
        $vars = $var[ 'vars' ];
        $inputName = $vars[ 'inputName' ];
        $item = $vars[ 'itemOrigin' ];

        $sql = "SELECT * FROM publications WHERE pub_id='" . $item . "' ";
        $rs = $this->fmt->querys->consult( $sql, __METHOD__ );
        $num = $this->fmt->querys->num( $rs );

        if ( $num > 0 ) {
            $row = $this->fmt->querys->row( $rs );

            $insert = 'pub_name, pub_title, pub_description, pub_path, pub_path_ui, pub_path_icon, pub_icon, pub_type, pub_class, pub_attr_id, pub_attr, pub_count, pub_json, pub_html, pub_state';
            $values = "'" . $inputName . "','" . $row[ 'pub_title' ] . "','" . $row[ 'pub_description' ] . "','" . $row[ 'pub_path' ] . "','" . $row[ 'pub_path_ui' ] . "','" . $row[ 'pub_path_icon' ] . "','" . $row[ 'pub_icon' ] . "','" . $row[ 'pub_type' ] . "','" . $row[ 'pub_class' ] . "','" . $row[ 'pub_attr_id' ] . "','" . $row[ 'pub_attr' ] . "','" . $row[ 'pub_count' ] . "','" . $row[ 'pub_json' ] . "','" . $row[ 'pub_html' ] . "','" . $row[ 'pub_state' ] . "'";
            $sql = 'INSERT INTO publications (' . $insert . ') VALUES (' . $values . ')';
            $this->fmt->querys->consult( $sql, __METHOD__ );

            $sql = 'select max(pub_id) as id from publications';
            $rs = $this->fmt->querys->consult( $sql, __METHOD__ );
            $row = $this->fmt->querys->row( $rs );
            $id = $row[ 'id' ];

            return $id;

        } else {
            return 0;
        }

    }

    public function relationFrom( $vars = null, $actives = 1 ) {
        $from = $vars[ 'from' ];
        $colId = $vars[ 'colId' ];
        $colPub = $vars[ 'colPub' ];
        $id = $vars[ 'id' ];
        $actives = $vars[ 'actives' ] ? $vars[ 'actives' ] : 1;
        $orderBy = $this->fmt->emptyReturn( $vars[ 'orderBy' ], ' pub_id ASC' );

        if ( $actives == 1 ) {
            $aux = '> 0';
        } else {
            $aux = '>= 0';
        }

        $sql = 'SELECT pub_id, pub_name, pub_class FROM publications,' . $from . ' WHERE ' . $colPub . '=pub_id AND ' . $colId . '=' . $id . ' AND pub_state ' . $aux . ' ORDER BY ' . $orderBy;
        $rs = $this->fmt->querys->consult( $sql, __METHOD__ );
        $num = $this->fmt->querys->num( $rs );
        if ( $num > 0 ) {
            for ( $i = 0; $i < $num; $i++ ) {
                $row = $this->fmt->querys->row( $rs );
                $return[ $i ][ 'id' ] = $row[ 'pub_id' ];
                $return[ $i ][ 'name' ] = $row[ 'pub_name' ];
                $return[ $i ][ 'cls' ] = $row[ 'pub_class' ];
                // $return[ $i ][ 'sql' ] = $sql ;
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function updateStateRelations( array $var = null ) {
        //return $var;
        $vars = $var[ 'vars' ];
        $id = $vars[ 'item' ];
        $state = $vars[ 'state' ];
        $ws = $vars[ 'ws' ];
        $cat = $vars[ 'cat' ];
        $block = $vars[ 'block' ];

        $sql = "UPDATE publications_relations SET
                pub_rel_state='" . $state . "'
                WHERE pub_rel_pub_id= '" . $id . "' AND pub_rel_ws_id='" . $ws . "' AND pub_rel_cat_id='" . $cat . "' AND pub_rel_block_id='" . $block . "'";
        $this->fmt->querys->consult( $sql );

        return 1;
    }

    public function getPubsOrigin( array $var = null ) {

        $filesPubPhp = $this->fmt->files->searchFilesPubPhp( _PATH_HOST.'sites/default/controllers/pub' );
        $i = 0;

        if ( $filesPubPhp == 0 ) {
            return 0;
        }

        foreach ( $filesPubPhp as $file ) {

            $fp = fopen( $file, 'r' );

            $headers = [];

            while ( !feof( $fp ) ) {
                $linea = fgets( $fp );

                if ( strpos( $linea, ':' ) !== false ) {

                    list( $nombre, $valor ) = explode( ': ', $linea );

                    $nombre = trim( $nombre );
                    $nombre = str_replace( 'header("', '', $nombre );
                    $nombre = str_replace( 'header( \'', '', $nombre);
                    $valor = trim($valor, "\n");
                    $valor = preg_replace('/[ :\"]|\\);/', '', $valor);

                    if ($nombre == "pathIcon") {

                        if (!file_exists(_PATH_HOST . $valor)) {
                            $valor = '';
                        }
                    }

                    $headers[$nombre] = $valor;

                }
            }


            fclose($fp);

            $rtn['data'][$i]["headers"] = $headers;
            $rtn['data'][$i]["path"] = str_replace(_PATH_HOST, '', $file); //$file;

            $i++;

        }

        $rtn["Error"] = 0;
        $rtn["status"] = "success";
        $rtn["message"] = "proceso exitoso.";
        
        return $rtn;
    }
}