<?PHP
header('Content-Type: text/html; charset=utf-8');
class FILES
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function dataId($fileId)
    {
        $sql = "SELECT * FROM files WHERE file_id='" . $fileId . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            return $this->fmt->querys->row($rs);
        } else {
            return 0;
        }

    }

    public function read($path = null, $fmt, $vars = [])
    {
        $fmt = $fmt ?? $this->fmt;
        ob_start();
        extract($vars);
        include $path;
        $result = ob_get_clean();
        return $result;
    }

    public function dataItem($fileId = '')
    {
        //return $fileId;
        $sql = "SELECT * FROM files WHERE file_id='" . $fileId . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row['file_id'];
            $return["id"] = $id;
            $return["name"] = $row['file_name'];
            $return["descripcion"] = $row['file_descripcion'];
            $return["pathurl"] = $row['file_pathurl'];
            $return["thumb"] = $this->urlAdd($row['file_pathurl'], "-thumb");
            $return["btnTitle"] = $row['file_btn_title'];
            $return["ext"] = $row['file_ext'];
            $return["alt"] = $row['file_alt'];
            $return["title"] = $row['file_title'];
            $return["filename"] = $row['file_filename_md5'];
            $return["datatime"] = $row['file_datatime'];
            $return["state"] = $row['file_state'];

            return $return;
        } else {
            return 0;
        }

    }

    public function typeFile(string $ext = null)
    {
        if ($ext == null) {
            return 0;
        }

        if ($ext == "jpg" || $ext == "jpeg" || $ext == "png" || $ext == "svg" || $ext == "webp" || $ext == "gif" || $ext == "avif") {
            return "image";
        }

        if ($ext == "mp4" || $ext == "embed") {
            return "video";
        }

        if ($ext == "mp3") {
            return "audio";
        }

        if ($ext == "pdf" || $ext == "docs" || $ext == "docx" || $ext == "xls" || $ext == "xlsx" || $ext == "ppt" || $ext == "pptx") {
            return "document";
        }

    }

    public function getFiles(array $var = [])
    {
        //return $var;
        $entitieId = $var["entitieId"];
        //return $fileId;
        $sql = "SELECT * FROM files WHERE file_ent_id='" . $entitieId . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row['file_id'];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row['file_name'];
                $return[$i]["descripcion"] = $row['file_descripcion'];
                $return[$i]["pathurl"] = $row['file_pathurl'];
                $return[$i]["btnTitle"] = $row['file_btn_title'];
                $return[$i]["ext"] = $row['file_ext'];
                $return[$i]["alt"] = $row['file_alt'];
                $return[$i]["title"] = $row['file_title'];
                $return[$i]["filename"] = $row['file_filename_md5'];
                $return[$i]["datatime"] = $row['file_datatime'];
                $return[$i]["state"] = $row['file_state'];
                $return[$i]["thumb"] = $this->urlAdd($row['file_pathurl'], "-thumb");
                $return[$i]["typeFile"] = $this->typeFile($row['file_ext']);
            }
            $rtn["folders"] = $this->getDirectory(_PATH_HOST_FILES . 'files');
            $rtn["primaryFolder"] = $this->fmt->options->getValue("primery_folder_finder");
            $rtn["files"] = $return;
            return $rtn;
        } else {
            return 0;
        }

    }
    public function getDirectory(string $dir = null)
    {

        $result = array();
        $di = new DirectoryIterator($dir);
        foreach ($di as $folder) {
            if ($folder->isDir() && !$folder->isDot()) {
                $result[] = $folder->getFilename();
            }
        }
        return $result;
    }

    public function urlFileId($fileId)
    {
        $sql = "SELECT file_pathurl FROM files WHERE file_id='" . $fileId . "' and file_state > 0";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["file_pathurl"];
        } else {
            return 0;
        }

    }

    public function name($fileId)
    {
        $sql = "SELECT file_name FROM files WHERE file_id='" . $fileId . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["file_name"];
        } else {
            return 0;
        }

    }

    function createUserLogs($vars)
    {
        $userid = $vars["userId"];
        $type = $vars["type"];
        $logType = $vars["logType"];
        $code = $vars["code"];
        $ip = $_SERVER['REMOTE_ADDR'];
        $path = _PATH_HOST . "users/logs/user" . $userid;
        $locale = $this->fmt->options->getValue("locale");
        $tymezone = $this->fmt->options->getValue("timezone");
        setlocale(LC_TIME, $locale);
        date_default_timezone_set($tymezone);
        $date = date('Y-m-d H:i:s');
        $current_time = date('Y-m-d');
        if (!file_exists($path)) {
            mkdir($path, 0700);
        }

        $strText = $this->fmt->modules->returnTextLang(array("locale" => $locale, "type" => $type, "logType" => $logType, "code" => $code));

        $strData = '[' . $date . ']' . $strText;
        $strData = str_replace('{{userId}}', $userid, $strData);
        $strData = str_replace('{{IP}}', $ip, $strData);
        file_put_contents($path . '/log-user' . $userid . '-' . $current_time . '.txt', $strData . PHP_EOL, FILE_APPEND);
        return;
    }

    /* public function createFolder($path, $permits = '0655')
    {
        if (!file_exists($path)) {
            if (!mkdir($path, octdec($permits), true)) {
                return $path . ': Fallo al crear las carpetas...';
            }
            
            // Obtener el usuario y grupo que deben ser propietarios de la carpeta
            $user = posix_getpwuid(fileowner($path))['name'];
            $group = posix_getgrgid(filegroup($path))['name'];
            
            // Cambiar el propietario y grupo de la carpeta
            chown($path, $user);
            chgrp($path, $group);

            return true;
        } else {
            return true;
        }
        return false;
    } */

    public function createFolder($path, $permits = 0777)
    {
        if (!file_exists($path)) {

            ini_set('safe_mode', 'Off');

            if (!mkdir($path, $permits, true)) {
                $rtn["Error"] = 1;
                $rtn["status"] = 'error';
                $rtn["message"] = $path . ': Fallo al crear las carpetas...';
                return $rtn;
            }

            // Verificar los permisos de la carpeta
            $actualPermits = fileperms($path);
            if (($actualPermits & 0777) !== $permits) {
                $rtn["Error"] = 1;
                $rtn["status"] = 'error';
                $rtn["message"] = $path . ': Los permisos de la carpeta no coinciden con los esperados...';
                ini_set('safe_mode', 'On');
                return $rtn;
            }

            // Obtener el grupo que debe ser propietario de la carpeta
            $user = posix_getpwuid(fileowner($path))['name'];
            $group = posix_getgrgid(filegroup($path))['name'];

            // Cambiar el propietario y grupo de la carpeta
            if (!chown($path, $user) || !chgrp($path, $group)) {
                $rtn["Error"] = 1;
                $rtn["status"] = 'error';
                $rtn["message"] = $path . ': Error al cambiar el propietario o grupo de la carpeta...';
                ini_set('safe_mode', 'On');
                return $rtn;
            }

            // Establecer permisos especiales (sticky bit)
            if (!chmod($path, 01711)) {
                $rtn["Error"] = 1;
                $rtn["status"] = 'error';
                $rtn["message"] = $path . ': Error al establecer los permisos especiales...';
                ini_set('safe_mode', 'On');
                return $rtn;
            }

            $rtn["Error"] = 0;
            $rtn["status"] = 'success';
            $rtn["message"] = $path . ': Carpeta creada con éxito...';
            ini_set('safe_mode', 'On');

            return $rtn;
        } else {
            $rtn["Error"] = 0;
            $rtn["status"] = 'success';
            $rtn["message"] = "La carpeta ya existe";
            return $rtn;
        }
    }


    function sizeFile($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = round(number_format($bytes / 1073741824, 2), 0) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = round(number_format($bytes / 1048576, 2), 0) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = round(number_format($bytes / 1024, 2), 0) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    function typeToMimetype($type)
    {
        switch ($type) {
            case 'pdf':
                return 'application/pdf';
                break;
            case 'doc':
                return 'application/msword';
                break;
            case 'docx':
                return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
                break;
            case 'xls':
                return 'application/vnd.ms-excel';
                break;
            case 'xls':
                return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                break;
            case 'ppt':
                return 'application/vnd.ms-powerpoint';
                break;
            case 'pptx':
                return 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
                break;
            case 'pptx':
                return 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
                break;
            case 'jpg':
                return 'image/jpg';
                break;
            case 'jpeg':
                return 'image/jpeg';
                break;
            case 'webp':
                return 'image/webp';
            case 'avif':
                return 'image/avif';
                break;
            case 'png':
                return 'image/png';
                break;
            case 'svg':
                return 'image/svg+xml';
                break;
            case 'gif':
                return 'image/gif';
                break;
            case 'mp3':
                return 'audio/mp3';
                break;
            case 'mp4':
                return 'video/mp4';
                break;

            default:
                return "none";
                break;
        }
    }

    function mimetypeToType($mimetype)
    {
        switch ($mimetype) {
            case 'application/pdf':
                return 'pdf';
                break;
            case 'application/msword':
                return 'doc';
                break;
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                return 'docx';
                break;
            case 'application/vnd.ms-excel':
                return 'xls';
                break;
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                return 'xls';
                break;
            case 'application/vnd.ms-powerpoint':
                return 'ppt';
                break;
            case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
                return 'pptx';
                break;
            case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
                return 'pptx';
                break;
            case 'image/jpg':
                return 'jpg';
                break;
            case 'image/jpeg':
                return 'jpeg';
                break;
            case 'image/webp':
                return 'webp';
                break;
            case 'image/avif':
                return 'avif';
                break;
            case 'image/png':
                return 'png';
                break;
            case 'image/svg+xml':
                return 'svg';
                break;
            case 'image/gif':
                return 'gif';
                break;
            case 'audio/mp3':
                return 'mp3';
                break;
            case 'video/mp4':
                return 'mp4';
                break;
        }
    }

    public function thumb($path = null)
    {
        return $this->urlAdd($path, "-thumb");
    }

    function existFile($path)
    {
        if (file_exists($path)) {
            return true;
        } else {
            return false;
        }
    }

    function deleteFolder($folder)
    {
        if (!is_dir($folder)) {
            rmdir($folder);
        }
    }

    function deleteFile($route)
    {
        unlink($route);
    }

    public function nameFileNum($file)
    {
        $name = $this->nameFile($file);
        $ext = $this->extensionFile($file);
        $endName = explode("-", $name);
        $numCount = count($endName);
        $num = intval($endName[1]);

        $name = $name . "-1";

        if (is_numeric($num) && $numCount > 1) {
            $numAux = $num++;
            $name = $endName[0] . "-" . $numAux;
        }

        return $name . "." . $ext;
    }

    function urlAdd($url, $add)
    {
        if (empty($add)) {
            return $url;
        }
        $path = explode("/", $url);
        $name = end($path);
        $pathX = str_replace($name, '', $url);
        $namex = $this->nameFile($name);
        $extention = $this->extensionFile($url);
        return $pathX . $namex . $add . "." . $extention;
    }

    function extensionFile($file)
    {
        $array = pathinfo($file);
        return $array["extension"];
    }

    function nameFile($file)
    {
        $array = pathinfo($file);
        return $array["filename"];
    }

    function createImagenGmagick($src, $dest, $name)
    {
        $imagen = new Gmagick($src);
        $imagen->thumbnailImage(100, 0);
        $imagen->write($dest . $name);
    }

    function crear_imagen_imagick($src, $dest, $nombre, $width = null, $height = null, $valor_1 = null, $valor_2 = null)
    {

        $image = new Imagick($src);


        if ($width < $height) {
            $image->rotateImage(new ImagickPixel("#000000"), -90);
        } else {
            $image->thumbnailImage($width, $height, $valor_1, $valor_2);
        }

        //$image->flopImage();
        //$image->cropThumbnailImage( 100,100 );
        $image->writeImages($dest . $nombre, true);


        // if ($width > $height ) {
        //   $this->rotar_imagen($dest.$nombre,"#000000","-270");
        // }

        // }
    }

    function createThumb($src, $dst, $width, $height, $crop = 0)
    {

        //$tamano = getimagesize($src);

        if (!list($w, $h) = getimagesize($src))
            return "Unsupported picture type!";

        $type = strtolower(substr(strrchr($src, "."), 1));

        if ($type == 'jpeg')
            $type = 'jpg';
        if ($type == 'pjpeg"')
            $type = 'jpg'; //

        switch ($type) {
            case 'bmp':
                $img = imagecreatefromwbmp($src);
                break;
            case 'gif':
                $img = imagecreatefromgif($src);
                break;
            case 'jpg':
                $img = imagecreatefromjpeg($src);
                break;
            case 'png':
                $img = imagecreatefrompng($src);
                break;
            default:
                return "Unsupported picture type!";
        }
        $w_org = $w;
        $h_org = $h;
        // resize
        if ($crop) {

            if (($w < $width) and ($h < $height))
                return $rn['Error'] = 1;
            $rn['message'] = "Picture is too small crop!";
            $rn['message'] = 'picture-small';
            $ratio = max($width / $w, $height / $h);
            $h = $height / $ratio;
            $x = ($w - $width / $ratio) / 2;
            $w = $width / $ratio;
        } else {
            if ($w < $width and $h < $height)
                return $rn['Error'] = 1;
            $rn['message'] = "Picture is too small!";
            $rn['message'] = 'picture-small';
            $ratio = min($width / $w, $height / $h);
            $width = $w * $ratio;
            $height = $h * $ratio;
            $x = 0;
        }

        if ($crop == '2') {
            $ratio = $w_org / $h_org;
            if ($width / $height > $ratio) {
                $width = $height * $ratio;
            } else {
                $height = $width / $ratio;
            }
            $x = 0;
        }

        $new = imagecreatetruecolor($width, $height);

        // preserve transparency
        if ($type == "gif" or $type == "png") {
            imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
            imagealphablending($new, false);
            imagesavealpha($new, true);
            $color = imagecolorallocate($new, 0x00, 0x00, 127);
            imagefill($new, 0, 0, $color);
        }



        //imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);

        imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);
        $q = 7 / 100;
        $quality *= $q;

        switch ($type) {
            case 'bmp':
                imagewbmp($new, $dst, 98);
                break;
            case 'gif':
                imagegif($new, $dst, 98);
                break;
            case 'jpg':
                imagejpeg($new, $dst, 85);
                break;
            case 'png':
                imagepng($new, $dst, $quality);
                break;
        }
        return true;

        imagedestroy($ds);
        imagedestroy($src);
    }

    function createThumbImage($srcImage, $destImage, $desiredWidth)
    {
        // Lee la imagen
        $sourceImage = null;
        $ext = pathinfo($srcImage, PATHINFO_EXTENSION);
        switch (strtolower($ext)) {
            case 'jpg':
            case 'jpeg':
                $sourceImage = imagecreatefromjpeg($srcImage);
                break;
            case 'gif':
                $sourceImage = imagecreatefromgif($srcImage);
                break;
            case 'png':
                $sourceImage = imagecreatefrompng($srcImage);
                break;
            case 'avif':
                $sourceImage = imagecreatefromavif($srcImage);
                break;
            case 'webp':
                $sourceImage = imagecreatefromwebp($srcImage);
                break;
            default:
                throw new Exception("Unsupported image type");
        }

        $width = imagesx($sourceImage);
        $height = imagesy($sourceImage);

        // Encuentra la altura "destino" de la imagen para mantener su aspecto original
        $desiredHeight = floor($height * ($desiredWidth / $width));

        // Crea una nueva imagen en color verdadero (mejor calidad)
        $virtualImage = imagecreatetruecolor($desiredWidth, $desiredHeight);

        // Habilita el canal alfa para la imagen de destino
        imagealphablending($virtualImage, false);
        imagesavealpha($virtualImage, true);

        // Copia la imagen antigua a la nueva
        imagecopyresampled($virtualImage, $sourceImage, 0, 0, 0, 0, $desiredWidth, $desiredHeight, $width, $height);

        // Crea la imagen
        switch (strtolower($ext)) {
            case 'jpg':
            case 'jpeg':
                imagejpeg($virtualImage, $destImage);
                break;
            case 'gif':
                imagegif($virtualImage, $destImage);
                break;
            case 'png':
                imagepng($virtualImage, $destImage);
                break;
            case 'avif':
                imageavif($virtualImage, $destImage);
                break;
            case 'webp':
                imagewebp($virtualImage, $destImage);
                break;
        }

        // Libera la memoria
        imagedestroy($sourceImage);
        imagedestroy($virtualImage);

        return true;
    }

    function insertFile($vars)
    {
        $fileName = $vars["name"];
        $fileDescription = $vars["description"];
        $filePathUrl = $vars["pathUrl"];
        $fileExt = $vars["ext"];
        $filemd5 = hash('md5', $fileName);
        $entitieId = $vars["entitieId"] ? $vars["entitieId"] : 1;

        $day = $this->fmt->modules->dateFormat();

        $insert = "file_name,file_description,file_pathurl,file_ext,file_filename_md5,file_datatime,file_ent_id,file_state";
        $values = "'" . $fileName . "','" . $fileDescription . "','" . $filePathUrl . "','" . $fileExt . "','" . $filemd5 . "','" . $day . "','" . $entitieId . "','1'";
        $sql = "insert into files (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(file_id) as id from files";
        $rs = $this->fmt->querys->consult($sql);
        $rows = $this->fmt->querys->row($rs);

        return $rows["id"];
    }

    public function deteleFileId($fileId)
    {
        //return $fileId;
        $sql = "SELECT * FROM modules WHERE mod_state='1'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $relations = $row["mod_relations_db"];
                $db = $row["mod_db"];
                if (!empty($relations)) {
                    $relation = explode(",", $relations);
                    $count = count($relation);
                    for ($i = 0; $i < $count; $i++) {
                        $rowR = explode(":", $relation[$i]);
                        $sql1 = "DELETE FROM " . $rowR[0] . " WHERE " . $rowR[1] . "='" . $fileId . "'";
                        //$aux .= "[ " . $sql1 . " ]";
                        $this->fmt->querys->consult($sql1, __METHOD__);
                        $up_sqr6 = "ALTER TABLE " . $db . " AUTO_INCREMENT=1";
                        $this->fmt->querys->consult($up_sqr6, __METHOD__);
                    }
                }
            }
        }
        //$this->fmt->querys->leave($rs);

        $arrayId = $this->dataId($fileId);
        $path = $arrayId["file_pathurl"];
        $pathTb = $this->urlAdd($path, "-thumb");
        $this->deleteFile(_PATH_HOST . $path);
        $this->deleteFile(_PATH_HOST . $pathTb);
        $sql2 = "DELETE FROM files WHERE file_id='" . $fileId . "'";
        $this->fmt->querys->consult($sql2, __METHOD__);
        $up_sqr7 = "ALTER TABLE files AUTO_INCREMENT=1";
        $this->fmt->querys->consult($up_sqr7, __METHOD__);

        return 1;
    }

    public function deleteId($id = 0)
    {
        if (!is_numeric($id)) {
            return 0;
        }
        $sql = "DELETE FROM files WHERE file_id = '" . $id . "'";
        $this->fmt->querys->consult($sql, __METHOD__);
        $sql = "ALTER TABLE files AUTO_INCREMENT = 1";
        $this->fmt->querys->consult($sql, __METHOD__);

        return 1;
    }

    public function relationFrom($vars = null)
    {

        $from = $vars["from"];
        $colItem = $vars["colItem"];
        $colFiles = $vars["colFiles"];
        $item = $vars["item"];
        $orderBy = $this->fmt->emptyReturn($vars["orderBy"], "file_id");

        $sql = "SELECT file_id,file_name,file_pathurl FROM files," . $from . " WHERE " . $colFiles . "=file_id AND " . $colItem . "=" . $item . " AND file_state > 0 ORDER BY  " . $orderBy;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return[$i]["id"] = $row["file_id"];
                $return[$i]["name"] = $row["file_name"];
                $return[$i]["pathurl"] = $row["file_pathurl"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function imgId($id = null, $return = "", $add = "")
    {
        //return $this->fmt->files->urlFileId($id);
        if (!empty($id)) {
            return $this->fmt->files->urlAdd($this->fmt->files->urlFileId($id), $add);
        } else {
            return $return;
        }
    }

    public function dataBasicImg($imgId = null)
    {
        //return $var;
        if ($imgId) {
            $img["id"] = $imgId;
            $img["pathurl"] = $this->imgId($imgId, "");
            $img["thumb"] = $this->imgId($imgId, "", "-thumb");
            $img["name"] = $this->name($imgId, "");
        } else {
            $img = 0;
        }
        return $img;
    }

    public function imgPosHtml($url = null)
    {
        if (strpos($url, 'http') !== false) {
            return true;
        } else {
            return false;
        }
    }

    public function imgReturn($images = null)
    {
        if (empty($images)) {
            $img = 0;
        } else {
            if ($this->imgPosHtml($images)) {
                $img = $images;
            } else {
                if (is_numeric($images)) {
                    $img = $this->dataBasicImg($images);
                } else {
                    $img = _PATH_FILES . $images;
                }
            }
        }
        return $img;
    }

    public function getPathFilesRoot($root = null, $filterExt = null)
    {
        //return $root.":" . $level;
        if (is_dir($root)) {
            $return["Error"] = 0;
            $returnFilter["Error"] = 0;
            $gestor = opendir($root);
            $i = 0;
            $j = 0;
            while (($file = readdir($gestor)) !== false) {

                $rootEnd = $root . "/" . $file;

                // Se muestran todos los archivos y carpetas excepto "." y ".."
                if ($file != "." && $file != "..") {
                    // Si es un directorio se recorre recursivamente
                    $ext = $this->extensionFile($file);

                    $return["files"][$i]["path"] = $file;
                    $i++;

                    if ($filterExt != null && $filterExt == $ext) {
                        $returnFilter["files"][$j]["path"] = $file;
                        $j++;
                    }


                }
            }
            // Cierra el gestor de directorios
            closedir($gestor);

        } else {
            $return["Error"] = 1;
            $return["message"] = "No es una ruta de directorio valida";
        }

        if ($filterExt != null) {
            return $returnFilter;
        } else {
            return $return;
        }

    }

    function generateThumbnailFromPDF($pdfFilePath, $thumbnailFilePath, $thumbnailWidth = 400, $thumbnailHeight = 400)
    {
        // Intentar crear una miniatura desde el archivo PDF
        $archivo_generador = __FILE__; // Este es el archivo actual
        $permisos = fileperms($archivo_generador);
        $usuario = fileowner($archivo_generador);

        chmod($pdfFilePath, $permisos);
        chown($pdfFilePath, $usuario);

        try {
            // Instanciar un objeto Imagick con el archivo PDF
            $imagick = new Imagick($pdfFilePath . '[0]');
            $imagick->setResolution(100, 100); // Configurar la resolución

            // Leer el archivo PDF
            //$imagick->readImage($pdfFilePath);
            $imagick->setImageFormat("jpg");

            // Seleccionar solo la primera página del PDF
            $imagick->setIteratorIndex(0);

            // Redimensionar la imagen para crear la miniatura
            $imagick->thumbnailImage($thumbnailWidth, $thumbnailHeight, true); // Tamaño de la miniatura

            // Guardar la miniatura en un archivo
            $imagick->writeImage($thumbnailFilePath);

            // Liberar la memoria utilizada por Imagick
            $imagick->clear();
            $imagick->destroy();

            $rtn["Error"] = 0;
            $rtn["message"] = "Miniatura generada correctamente";
            $rtn["status"] = "success";

            return $rtn;

        } catch (ImagickException $e) {

            $rtn["Error"] = 1;
            $rtn["message"] = 'Error al generar la miniatura: ' . $e->getMessage();
            $rtn["status"] = "success";

            return $rtn;
        }
    }



    function searchFilesPubPhp($directory)
    {
        //return $directory;
        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory, FilesystemIterator::SKIP_DOTS));

        $files = [];

        foreach ($iterator as $file) {

            // Filtrar por extensión .php
            if ($file->getExtension() === 'php') {

                // Filtrar que contenga ".pub" y ".php" 
                if (strpos($file->getFilename(), '.pub.php') !== false) {

                    if (strpos($file->getFilename(), "._") !== 0) {

                        if (($file->getFilename() !== 'config.pub.php') && ($file->getFilename() !== 'meta.pub.php')) {
                            $files[] = $file->getPathname();
                        }
                    }

                }
            }
        }

        if (empty($files) || empty($directory)) {
            return 0;
        }

        return $files;
    }



}