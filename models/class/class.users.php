<?PHP
header('Content-Type: text/html; charset=utf-8');
class USERS
{

	var $fmt;

	function __construct($fmt)
	{
		$this->fmt = $fmt;
	}

	public function dataId($userId)
	{
		$sql = "SELECT * FROM users WHERE user_id='" . $userId . "' ";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			return $this->fmt->querys->row($rs);
		} else {
			return 0;
		}
		$this->fmt->query->leave($rs);
	}

	public function getDataId($userId = null)
	{
		$sql = "SELECT * FROM users WHERE user_id='" . $userId . "' ";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			$row = $this->fmt->querys->row($rs);
			$id = $row["user_id"];
			$return["id"] = $id;
			$return["name"] = $row["user_name"];
			$return["lastname"] = $row["user_lastname"];
			$return["nameFull"] = $row["user_name"] . " " . $row["user_lastname"];
			$return["initialName"] = $this->initialName($row["user_name"] . " " . $row["user_lastname"]);
			$return["email"] = $row["user_email"];

			if ($row["user_img"]) {
				$img["id"] = $row["user_img"];
				$img["pathurl"] = $this->fmt->files->imgId($row["user_img"], "");
				$img["name"] = $this->fmt->files->name($row["user_img"], "");
			} else {
				$img = 0;
			}
			$return["img"] = $img;

			$rolId = $this->idRolUser($id);
			$dataRol = $this->dataRolId($rolId);
			$return["rolId"] = $rolId;
			$return["rolName"] = $dataRol["name"];
			$return["user"] = $row["user_user"];
			$return["level"] = $row["user_level"];
			$return["state"] = $row["user_state"];

			return $return;
		} else {
			return 0;
		}
		$this->fmt->query->leave($rs);
	}

	public function dataRolId(int $rolId = null, int $state = null)
	{
		//return $var;
		if ($state != null) {
			$aux = ' AND rol_state=' . $state;
		} else {
			$aux = '';
		}
		$sql = "SELECT * FROM roles WHERE rol_id='" . $rolId . "'" . $aux;
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {

			$row = $this->fmt->querys->row($rs);
			$id = $row["rol_id"];
			$return["id"] = $id;
			$return["name"] = $row["rol_name"];
			$return["description"] = $row["rol_description"];
			$return["parentId"] = $row["rol_parent_id"];
			$return["redirectionUrl"] = $row["rol_redirection_url"];
			$return["state"] = $row["state"];

			return $return;
		} else {
			return 0;
		}
		$this->fmt->querys->leave($rs);
	}

	public function initialId($userId)
	{
		$name = $this->fullName($userId);
		$nombrex = explode(" ", $name);
		if (!empty($nombrex[1])) {
			$ap = substr($nombrex[1], 0, 1);
		} else {
			$ap = "";
		}
		if ($name != "") {
			return substr($nombrex[0], 0, 1) . $ap;
		} else {
			return 0;
		}
	}

	public function getUsers(array $var = null)
	{
		//return $var;
		$sql = "SELECT * FROM users";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			for ($i = 0; $i < $num; $i++) {
				$row = $this->fmt->querys->row($rs);
				$id = $row["user_id"];
				$return[$i]["id"] = $id;
				$return[$i]["name"] = $row["user_name"];
				$return[$i]["lastname"] = $row["user_lastname"];
				$return[$i]["nameFull"] = $row["user_name"] . " " . $row["user_lastname"];
				$return[$i]["initialName"] = $this->initialName($row["user_name"] . " " . $row["user_lastname"]);
				$return[$i]["email"] = $row["user_email"];

				if ($row["user_img"]) {
					$img["id"] = $row["user_img"];
					$img["pathurl"] = $this->fmt->files->imgId($row["user_img"], "");
					$img["name"] = $this->fmt->files->name($row["user_img"], "");
				} else {
					$img = 0;
				}
				$return[$i]["img"] = $img;

				$rolId = $this->idRolUser($id);
				$dataRol = $this->dataRolId($rolId);
				$return[$i]["rolId"] = $rolId;
				$return[$i]["rolName"] = $dataRol["name"];

				$return[$i]["user"] = $row["user_user"];
				$return[$i]["level"] = $row["user_level"];
				$return[$i]["state"] = $row["user_state"];
			}
			return $return;
		} else {
			return 0;
		}
		$this->fmt->querys->leave($rs);
	}

	public function validateUserToken($vars = null)
	{
		$access_token = $vars['access_token'];
		$refresh_token = $vars['refresh_token'];
		$idEntitie = $vars['idEntitie'];

		$decode = base64_decode($access_token);
		$arrayUserDates = explode(".", $decode);
		$userId = $arrayUserDates[1];
		$rolId = $arrayUserDates[2];

		$sql = "SELECT user_tk_user_id FROM  users_tokens WHERE user_tk_user_id='" . $userId . "' AND  user_tk_type='access_token' AND user_tk_token='" . $access_token . "'";
		$rs = $this->fmt->querys->consult($sql);
		$num = $this->fmt->querys->num($rs);
		if ($num == 0) {
			return 0;
		}
		$this->fmt->querys->leave($rs);

		$sql2 = "SELECT user_tk_user_id FROM  users_tokens WHERE user_tk_user_id='" . $userId . "' AND  user_tk_type='refresh_token' AND user_tk_token='" . $refresh_token . "'";
		$rs2 = $this->fmt->querys->consult($sql2);
		$num2 = $this->fmt->querys->num($rs2);

		if ($num2 > 0) {
			return $userId;
		} else {
			return 0;
		}
		$this->fmt->querys->leave($rs2);
	}

	public function initialName($name)
	{
		$nameAux = explode(" ", $name);
		if (!empty($nameAux[1])) {
			$lastname = substr($nameAux[1], 0, 1);
		} else {
			$lastname = "";
		}
		return substr($nameAux[0], 0, 1) . $lastname;
	}

	public function fullName($userId, $lastname = "0")
	{
		$sql = "SELECT user_name, user_lastname FROM users WHERE user_id='" . $userId . "'";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			$row = $this->fmt->querys->row($rs);
			if ($lastname == "1") {
				$lnx = explode(" ", $row["user_lastname"]);
				$lastname = $lnx[0];
			} else {
				$lastname = $row["user_lastname"];
			}
			return $row["user_name"] . " " . $lastname;
		} else {
			return "";
		}
	}

	public function create($vars)
	{
		$user_name = $vars["user_name"];
		$user_lastname = $vars["user_lastname"];
		$user_email = $vars["user_email"];
		$user_password = $this->fmt->emptyReturn(base64_encode($vars["user_password"]), "");
		$user_img = $this->fmt->emptyReturn($vars["user_img"], "");
		$user_user = $this->fmt->emptyReturn($vars["user_user"], "");
		$user_last_path_url = $this->fmt->emptyReturn($vars["user_last_path_url"], "");
		$user_level = $this->fmt->emptyReturn($vars["user_level"], "0");
		$user_state = $this->fmt->emptyReturn($vars["user_state"], "0");
		$rol_id = $vars["rol_id"];

		$inputs = "user_name,user_lastname,user_email,user_password,user_img,user_user,user_last_path_url,user_level,user_state";
		$values = "'" . $user_name . "','" .
			$user_lastname . "','" .
			$user_email . "','" .
			$user_password . "','" .
			$user_img . "','" .
			$user_user . "','" .
			$user_last_path_url . "','" .
			$user_level . "','" .
			$user_state . "'";

		$sql = "insert into users (" . $inputs . ") values (" . $values . ")";
		$this->fmt->querys->consult($sql, __METHOD__);

		$sql = "select max(user_id) as user_id from users";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$row = $this->fmt->querys->row($rs);
		$id = $row["user_id"];

		// ingresar rol
		$inputs1 = "user_rol_user_id, user_rol_rol_id";
		$values1 = "'" . $id . "','" . $rol_id . "'";
		$sql1 = "insert into users_roles (" . $inputs1 . ") values (" . $values1 . ")";
		$this->fmt->querys->consult($sql1, __METHOD__);

		return $id;
	}

	public function userData($email = null, $password = null)
	{
		$pw = base64_encode($password);
		$sql = "SELECT user_id,user_name,user_img,user_lastname,user_rol_rol_id FROM users LEFT JOIN users_roles ON user_rol_user_id=user_id WHERE user_email='" . $email . "'  AND user_password= '" . $pw . "' AND user_state > 0 ";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			$row = $this->fmt->querys->row($rs);
			$return["user_id"] = $row["user_id"];
			$name = $row["user_id"];
			$lastname = $row["user_lastname"];
			$return["name"] = $name;
			$return["img"] = $row["user_img"];
			$return["last_name"] = $lastname;
			$return["initial"] = $this->initial($name . " " . $lastname);
			$return["user_rol"] = $row["user_rol_rol_id"];
			$return["rol_redirection_url"] = $this->idRolRedirectionUrl($row["user_id"]);
			return $return;
		} else {
			return 0;
		}
		$this->fmt->query->leave($rs);
	}

	function usersRol($rol = 0, $entitieId = 0)
	{
		$sql = "SELECT user_id,user_name FROM users,users_roles WHERE user_rol_user_id=user_id AND user_rol_rol_id='" . $rol . "' AND user_state=1 AND user_rol_ent_id='" . $entitieId . "'";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			for ($i = 0; $i < $num; $i++) {
				$row = $this->fmt->querys->row($rs);
				$rt[$i] = $this->getDataId($row["user_id"]);
			}
			return $rt;
		} else {
			return 0;
		}
		$this->fmt->query->leave($rs);
	}

	public function usersRolEntitie($rolId, $entitieId)
	{

		$sql = "SELECT user_id,user_name,user_lastname FROM users,users_roles WHERE user_rol_user_id=user_id AND user_rol_rol_id='" . $rolId . "' AND  user_rol_ent_id='" . $entitieId . "'  AND user_state=1";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$num = $this->fmt->querys->num($rs);

		if ($num > 0) {
			for ($i = 0; $i < $num; $i++) {
				$row = $this->fmt->querys->row($rs);
				$rt[$i]["id"] = $row["user_id"];
				$rt[$i]["name"] = $row["user_name"];
				$rt[$i]["lastname"] = $row["user_lastname"];
			}
			return $rt;
		} else {
			return 0;
		}
		$this->fmt->query->leave($rs);
	}

	public function idRolData($idRol)
	{
		$sql = "SELECT * FROM roles WHERE rol_id='" . $idRol . "' AND rol_state = 1 ";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			return $this->fmt->querys->row($rs);
		} else {
			return 0;
		}
		$this->fmt->query->leave($rs);
	}

	public function roles($idEntitie)
	{
		$sql = "SELECT rol_id, rol_name,rol_redirection_url FROM roles,roles_entities WHERE rol_ent_rol_id=rol_id AND rol_state = '1' AND rol_ent_ent_id='" . $idEntitie . "' ";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			for ($i = 0; $i < $num; $i++) {
				$row[$i] = $this->fmt->querys->row($rs);
			}
			return $row;
		} else {
			return 0;
		}
		$this->fmt->query->leave($rs);
	}

	public function getRoles(array $vars = null)
	{
		//return $vars;
		$entitieId = $vars["entitieId"];
		$userId = $vars["user"];
		$rolId = $user["rolId"];

		$sql = "SELECT * FROM roles,roles_entities WHERE rol_ent_rol_id=rol_id AND rol_state = '1' AND rol_ent_ent_id='" . $entitieId . "' ORDER BY rol_id DESC";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			for ($i = 0; $i < $num; $i++) {
				$row[$i] = $this->fmt->querys->row($rs);
				$id = $row[$i]["rol_id"];
				$rtn[$i]["id"] = $id;
				$rtn[$i]["name"] = $row[$i]["rol_name"];
				$rtn[$i]["description"] = $row[$i]["rol_description"];
				$rtn[$i]["redirectionUrl"] = $row[$i]["rol_redirection_url"];
				$rtn[$i]["parentId"] = $row[$i]["rol_parent_id"];
				$rtn[$i]["json"] = $row[$i]["rol_json"];
				$rtn[$i]["state"] = $row[$i]["rol_state"];
				$rtn[$i]["cats"] = $this->relationCategoryRoles(array("rolId" => $id));
			}
			return $rtn;
		} else {
			return 0;
		}
		$this->fmt->query->leave($rs);
	}

	public function relationCategoryRoles(array $var = null)
	{
		//return $var;
		$rolId = $var["rolId"];
		$limit = $this->fmt->emptyReturn($var["limit"], "");
		$order = $this->fmt->emptyReturn($var["order"], "rol_cat_order ASC");

		if ($limit != "") {
			$limit = "LIMIT " . $limit;
		}

		$sql = "SELECT rol_cat_cat_id FROM roles_categorys, roles, categorys WHERE  rol_cat_cat_id = cat_id AND rol_cat_rol_id = '" . $rolId . "' AND rol_state>1 AND cat_state>1 ORDER BY " . $order . " " . $limit;
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			$return = array();
			for ($i = 0; $i < $num; $i++) {
				$row = $this->fmt->querys->row($rs);
				$id = $row["rol_cat_cat_id"];
				$return[$i] = $id;
			}
			return $return;
		} else {
			return 0;
		}
		$this->fmt->querys->leave($rs);
	}

	public function idRolRedirectionUrl($idRol)
	{
		$sql = "SELECT rol_redirection_url FROM roles WHERE rol_id='" . $idRol . "' AND rol_state = 1 ";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			$row = $this->fmt->querys->row($rs);
			return $row["rol_redirection_url"];
		} else {
			return 0;
		}
		$this->fmt->query->leave($rs);
	}

	public function idRolUser($userId)
	{
		$sql = "SELECT DISTINCT user_rol_rol_id FROM users,users_roles,roles WHERE user_rol_user_id='" . $userId . "' AND user_rol_rol_id=rol_id AND user_state = 1 ";
		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			$row = $this->fmt->querys->row($rs);
			return $row["user_rol_rol_id"];
		} else {
			return 0;
		}
		$this->fmt->query->leave($rs);
	}

	public function getIdbyEmailRol($email, $rol)
	{
		$sql = "SELECT user_id FROM users,users_roles WHERE user_email='" . $email . "' AND user_rol_user_id=user_id AND user_rol_rol_id='" . $rol . "'";
		$rs = $this->fmt->querys->consulta($sql);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			$row = $this->fmt->querys->row($rs);
			return $row["user_id"];
		} else {
			return 0;
		}
		$this->fmt->querys->leave();
	}

	public function getIdbyEmailToken($email, $token)
	{
		$sql = "SELECT user_id FROM users,users_tokens WHERE user_email='" . $email . "' AND user_tk_user_id=user_id AND user_tk_token='" . $token . "'";
		$rs = $this->fmt->querys->consult($sql);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			$row = $this->fmt->querys->row($rs);
			return $row["user_id"];
		} else {
			return 0;
		}
		$this->fmt->querys->leave();
	}

	public function createUserToken($userId = null, $type = null, $token = null)
	{
		$ua = $this->getBrowser();
		if (($userId != null) && ($type != null) && ($token != null)) {
			$enter = "user_tk_user_id,user_tk_type,user_tk_token,user_tk_date,user_tk_dates_browser"; // 1. local 2.fb 3.gl 4. in
			$values = "'" . $userId . "','" . $type . "','" . $token . "','" . $this->fmt->modules->dateFormat() . "','" . $ua['name'] . "," . $ua['version'] . "," . $ua["platform"] . ":" . $ua['userAgent'] . "'";
			$sql = "insert into users_tokens (" . $enter . ") values (" . $values . ")";
			$this->fmt->querys->consult($sql, __METHOD__);
			return 1;
		} else {
			return 0;
		}
	}

	public function getTokenUser($userId, $type)
	{
		$sql = "SELECT user_tk_token FROM users_tokens WHERE user_tk_type='" . $type . "' AND user_tk_user_id='" . $userId . "'";
		$rs = $this->fmt->querys->consult($sql);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			return 1;
		} else {
			return 0;
		}
		$this->fmt->querys->leave();
	}

	public function initial($name)
	{
		$nom = explode(" ", $name);
		if (!empty($nom[1])) {
			$ln = substr($nom[1], 0, 1);
		} else {
			$ln = "";
		}
		return substr($nom[0], 0, 1) . $ln;
	}

	public function idUserToEntities(int $idUser = null)
	{
		//$sql = "SELECT DISTINCT user_rol_ent_id,ent_name,ent_code,ent_path,ent_brand,user_rol_order FROM users_roles,users,entities WHERE user_rol_user_id='" . $idUser . "' AND user_rol_ent_id=ent_id AND user_state='1' AND ent_state='1' ORDER BY user_rol_order ASC";

		//[last] añidir comprovación de si usuario esta activo;

		$rolId = $this->idRolUser($idUser);

		if ($rolId == 0) {
			return 0;
		}

		if ($rolId = 1) {
			$sql = "SELECT DISTINCT ent_id,ent_name,ent_code,ent_path,ent_brand FROM  entities WHERE ent_state='1' ORDER BY ent_id ASC";
		} else {
			$sql = "SELECT DISTINCT ent_id,ent_name,ent_code,ent_path,ent_brand FROM users_roles,users,entities WHERE user_rol_rol_id='" . $rolId . "' AND user_rol_ent_id=ent_id AND user_state='1' AND ent_state='1' ORDER BY user_rol_order ASC";
		}


		$rs = $this->fmt->querys->consult($sql, __METHOD__);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			for ($i = 0; $i < $num; $i++) {
				$row = $this->fmt->querys->row($rs);
				$rows[$i]["id"] = $row["ent_id"];
				$rows[$i]["name"] = $row["ent_name"];
				$rows[$i]["path"] = $row["ent_path"];
				$rows[$i]["code"] = $row["ent_code"];
				$rows[$i]["brand"] = $row["ent_brand"];
			}
			return $rows;
		} else {
			return 0;
		}
		$this->fmt->query->leave($rs);
	}

	public function userDataAuth($vars)
	{
		$access_token = $vars['access_token'];
		$idEntitie = $vars['idEntitie'];

		$decode = base64_decode($access_token);
		$arrayUserDates = explode(".", $decode);
		$userId = $arrayUserDates[1];
		$return["userId"] = $userId;
		$rolId = $return["rolId"] = $arrayUserDates[2];
		$arrayUser = $this->dataId($userId);
		$userName = $arrayUser["user_name"];
		$userLastname = $arrayUser["user_lastname"];
		$return["userName"] = $userName;
		$return["userLastname"] = $userLastname;
		$return["userEmail"] = $arrayUser["user_email"];
		$return["initial"] = $this->initial($userName . " " . $userLastname);
		$return["userImg"] = $this->fmt->emptyReturn($arrayUser["user_img"], '');
		$usersEntities = $this->idUserToEntities($userId);
		if ($usersEntities == 0) {
			$return["entities"] = 0;
		} else {
			$return["entities"] = $usersEntities;
		}

		$arrayRol = $this->dataRolId($rolId);

		$return["rolName"] = $arrayRol["name"];

		return $return;
	}

	public function listGroups($entId)
	{
		$sql = "SELECT group_id,group_name,group_description FROM users_groups, groups WHERE user_group_ent_id='" . $entId . "' AND user_group_group_id=group_id AND group_state='1'";
		$rs = $this->fmt->querys->consult($sql);
		$num = $this->fmt->querys->num($rs);
		if ($num > 0) {
			for ($i = 0; $i < $num; $i++) {
				$row[$i] = $this->fmt->querys->row($rs);
			}
			return $row;
		} else {
			return 0;
		}
		$this->fmt->querys->leave();
	}

	public function getBrowser()
	{
		$u_agent = $_SERVER['HTTP_USER_AGENT'];
		$bname = 'Unknown';
		$platform = 'Unknown';
		$version = "";

		//First get the platform?
		if (preg_match('/linux/i', $u_agent)) {
			$platform = 'Linux';
		} elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
			$platform = 'Mac Os';
		} elseif (preg_match('/windows|win32/i', $u_agent)) {
			$platform = 'Windows';
		}

		// Next get the name of the useragent yes seperately and for good reason
		if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
			$bname = 'Internet Explorer';
			$ub = "MSIE";
		} elseif (preg_match('/Firefox/i', $u_agent)) {
			$bname = 'Mozilla Firefox';
			$ub = "Firefox";
		} elseif (preg_match('/OPR/i', $u_agent)) {
			$bname = 'Opera';
			$ub = "Opera";
		} elseif (preg_match('/Chrome/i', $u_agent) && !preg_match('/Edge/i', $u_agent) && !preg_match('/Edg/i', $u_agent)) {
			$bname = 'Google Chrome';
			$ub = "Chrome";
		} elseif (preg_match('/Safari/i', $u_agent) && !preg_match('/Edge/i', $u_agent) && !preg_match('/Edg/i', $u_agent)) {
			$bname = 'Apple Safari';
			$ub = "Safari";
		} elseif (preg_match('/Netscape/i', $u_agent)) {
			$bname = 'Netscape';
			$ub = "Netscape";
		} elseif (preg_match('/Edge/i', $u_agent)) {
			$bname = 'Edge';
			$ub = "Edge";
		} elseif (preg_match('/Edg/i', $u_agent)) {
			$bname = 'Edg';
			$ub = "Edg";
		} elseif (preg_match('/Trident/i', $u_agent)) {
			$bname = 'Internet Explorer';
			$ub = "MSIE";
		}

		// finally get the correct version number
		$known = array('Version', $ub, 'other');
		$pattern = '#(?<browser>' . join('|', $known) .
			')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
		if (!preg_match_all($pattern, $u_agent, $matches)) {
			// we have no matching number just continue
		}
		// see how many we have
		$i = count($matches['browser']);
		if ($i != 1) {
			//we will have two since we are not using 'other' argument yet
			//see if version is before or after the name
			if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
				$version = $matches['version'][0];
			} else {
				$version = $matches['version'][1];
			}
		} else {
			$version = $matches['version'][0];
		}

		// check if we have a number
		if ($version == null || $version == "") {
			$version = "?";
		}

		return array(
			'userAgent' => $u_agent,
			'name' => $bname,
			'version' => $version,
			'platform' => $platform,
			'pattern' => $pattern
		);
	}

	public function logout($userId = null)
	{

		$sqle = "DELETE FROM users_tokens WHERE user_tk_user_id='" . $userId . "' ";
		$this->fmt->querys->consult($sqle, __METHOD__);
		$up_sqr6 = "ALTER TABLE users_tokens AUTO_INCREMENT=1";
		$this->fmt->querys->consult($up_sqr6, __METHOD__);

		return 1;
	}

	public function changeStateId(array $var = null)
	{
		//return $var;
		$vars = $var["vars"];
		$id = $vars["id"];
		$state = $vars["state"];

		$sql = "UPDATE users SET
				user_state='" . $state . "'
				WHERE user_id='" . $id . "' ";
		$this->fmt->querys->consult($sql);

		return 1;
	}

	public function deleteItems(array $var = null)
	{
		//return $var;
		$vars = $var["vars"];
		$items = implode(",", $vars["items"]);

		$sql = "DELETE FROM users WHERE user_id IN (" . $items . ")";
		$this->fmt->querys->consult($sql);
		$up_sqr6 = "ALTER TABLE users AUTO_INCREMENT=1";
		$this->fmt->querys->consult($up_sqr6, __METHOD__);

		$sql = "DELETE FROM users_groups WHERE user_group_user_id IN (" . $items . ")";
		$this->fmt->querys->consult($sql);
		$up_sqr6 = "ALTER TABLE users_groups AUTO_INCREMENT=1";
		$this->fmt->querys->consult($up_sqr6, __METHOD__);

		$sql = "DELETE FROM users_path WHERE user_path_user_id IN (" . $items . ")";
		$this->fmt->querys->consult($sql);
		$up_sqr6 = "ALTER TABLE users_path AUTO_INCREMENT=1";
		$this->fmt->querys->consult($up_sqr6, __METHOD__);

		$sql = "DELETE FROM users_roles WHERE user_rol_user_id IN (" . $items . ")";
		$this->fmt->querys->consult($sql);
		$up_sqr6 = "ALTER TABLE users_roles AUTO_INCREMENT=1";
		$this->fmt->querys->consult($up_sqr6, __METHOD__);

		$sql = "DELETE FROM users_tabs WHERE user_tab_user_id IN (" . $items . ")";
		$this->fmt->querys->consult($sql);
		$up_sqr6 = "ALTER TABLE users_tabs AUTO_INCREMENT=1";
		$this->fmt->querys->consult($up_sqr6, __METHOD__);

		$sql = "DELETE FROM users_tokens WHERE user_tk_user_id IN (" . $items . ")";
		$this->fmt->querys->consult($sql);
		$up_sqr6 = "ALTER TABLE users_tokens AUTO_INCREMENT=1";
		$this->fmt->querys->consult($up_sqr6, __METHOD__);

		return 1;
	}

	public function deleteRol(array $var = null)
	{
		return $var;
	}


}