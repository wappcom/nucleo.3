<?PHP
header('Content-Type: text/html; charset=utf-8');
class FORMS
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    function serializeToArray($data)
    {
        foreach ($data as $d) {
            if (substr($d["name"], -1) == "]") {
                $d["name"] = explode("[", str_replace("]", "", $d["name"]));
                switch (sizeof($d["name"])) {
                    case 2:
                        $a[$d["name"][0]][$d["name"][1]] = $d["value"];
                        break;

                    case 3:
                        $a[$d["name"][0]][$d["name"][1]][$d["name"][2]] = $d["value"];
                        break;

                    case 4:
                        $a[$d["name"][0]][$d["name"][1]][$d["name"][2]][$d["name"][3]] = $d["value"];
                        break;
                }
            } else {
                $a[$d["name"]] = $d["value"];
            } // if
        } // foreach

        return $a;
    }

}