<?PHP
header('Content-Type: text/html; charset=utf-8');
class DATA
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function dateFormat($timezone = "America/La_Paz", $format = "Y-m-d H:i:s", $setlocale = "es_ES")
    {
        setlocale(LC_TIME, $setlocale);
        date_default_timezone_set($timezone);
        return date($format);
    }

    public function now(array $var = null)
    {
        $timezone = $var['timezone'] ?? "America/La_Paz";
        $setlocale = $var['setlocale'] ?? "es_ES";
        $format = $var['format'] ?? "Y-m-d H:i:s";
        setlocale(LC_TIME, $setlocale);
        date_default_timezone_set($timezone);
        return date($format);
    }

    public function dateCustom($options = [])
    {
        // Valores por defecto
        $defaults = [
            'modification' => '+0 day',
            'timezone' => 'America/La_Paz',
            'format' => 'Y-m-d H:i:s',
            'setlocale' => 'es_ES'
        ];

        // Combinar los valores por defecto con las opciones proporcionadas
        $settings = array_merge($defaults, $options);

        // Establecer la configuración regional y la zona horaria
        setlocale(LC_TIME, $settings['setlocale']);
        date_default_timezone_set($settings['timezone']);

        // Crear objeto DateTime con la fecha y hora actual en la zona horaria especificada
        $date = new DateTime("now", new DateTimeZone($settings['timezone']));

        // Modificar la fecha según el intervalo dado (por ejemplo, "+1 day", "-2 hours")
        $date->modify($settings['modification']);

        // Retornar la fecha en el formato especificado
        return $date->format($settings['format']);
    }

    public function markdownToHtml($markdown) {
        // Escapar caracteres HTML para evitar problemas de seguridad
        $markdown = htmlspecialchars($markdown, ENT_NOQUOTES);
    
        // Convertir encabezados
        $markdown = preg_replace('/\#\#\# (.+)/', '<h3>$1</h3>', $markdown);
        $markdown = preg_replace('/\#\# (.+)/', '<h2>$1</h2>', $markdown);
        $markdown = preg_replace('/\# (.+)/', '<h1>$1</h1>', $markdown);
    
        // Convertir negritas
        $markdown = preg_replace('/\*\*(.+?)\*\*/', '<strong>$1</strong>', $markdown);
    
        // Convertir itálicas
        $markdown = preg_replace('/\*(.+?)\*/', '<em>$1</em>', $markdown);
    
        // Convertir listas no ordenadas
        $markdown = preg_replace_callback(
            '/(?:^|\s)- (.+?)(?=$|\n| - )/',
            function ($matches) {
                return '<ul><li>' . $matches[1] . '</li></ul>';
            },
            $markdown
        );
    
        // Unir listas consecutivas
        $markdown = preg_replace('/<\/ul>\s*<ul>/', '', $markdown);
    
        // Convertir enlaces
        $markdown = preg_replace('/\[(.+?)\]\((.+?)\)/', '<a href="$2">$1</a>', $markdown);
    
        return $markdown;
    }
    
    

    public function htmlToMarkdown($html) {
        // Convertir encabezados
        $html = preg_replace('/<h1>(.+?)<\/h1>/', '# $1', $html);
        $html = preg_replace('/<h2>(.+?)<\/h2>/', '## $1', $html);
        $html = preg_replace('/<h3>(.+?)<\/h3>/', '### $1', $html);
    
        // Convertir negritas
        $html = preg_replace('/<(strong|b)>(.+?)<\/\1>/', '**$2**', $html);
    
        // Convertir itálicas
        $html = preg_replace('/<(em|i)>(.+?)<\/\1>/', '*$2*', $html);
    
        // Convertir listas ordenadas
        $html = preg_replace_callback(
            '/<ol>\s*(<li>.+?<\/li>)\s*<\/ol>/s',
            function ($matches) {
                return preg_replace('/<li>(.+?)<\/li>/', "1. $1", $matches[1]);
            },
            $html
        );
    
        // Convertir listas no ordenadas
        $html = preg_replace_callback(
            '/<ul>\s*(<li>.+?<\/li>)\s*<\/ul>/s',
            function ($matches) {
                return preg_replace('/<li>(.+?)<\/li>/', "* $1", $matches[1]);
            },
            $html
        );
    
        // Convertir enlaces
        $html = preg_replace('/<a href="(.+?)">(.+?)<\/a>/', '[$2]($1)', $html);
    
        // Eliminar cualquier etiqueta HTML restante
        $html = strip_tags($html);
    
        return $html;
    }
    


    function formatUri($string, $separator = '-')
    {
        $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|Grave|lig|orn|ring|slash|th|tilde|uml);~i';
        $special_cases = array('&' => 'and', "'" => '');
        $string = mb_strtolower(trim($string), 'UTF-8');
        $string = str_replace(array_keys($special_cases), array_values($special_cases), $string);
        $string = preg_replace($accents_regex, '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'));
        $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
        $string = preg_replace("/[$separator]+/u", "$separator", $string);
        return $string;
    }

    public function convertFn(string $str = null)
    {
        //return $str;
        $array = explode("_", $str);
        $cont = count($array);
        $string = '';
        for ($i = 0; $i < $cont; $i++) {
            $string .= ucfirst($array[$i]);
        }
        if ($cont == 0) {
            $string = $str;
        }

        return $string;
    }

    public function dbDate($date = null, $activeHours = 1)
    {
        $arrayDate = explode(' ', $date);
        $hours = $arrayDate[1] ? $arrayDate[1] : '00:00:00';
        $arrayDate[0] = str_replace('/', '-', $arrayDate[0]);
        $arrayDates = explode('-', $arrayDate[0]);
        $day = $arrayDates[0];
        $month = $arrayDates[1];
        $year = $arrayDates[2];

        if (strlen($day) == 1) {
            $day = '0' . $day;
        }
        if (strlen($month) == 1) {
            $month = '0' . $month;
        }

        if ($activeHours == 1) {
            return $year . '-' . $month . '-' . $day . ' ' . $hours;
        } else {
            return $year . '-' . $month . '-' . $day;
        }
    }

    public function limitText(array $var = null)
    {
        $text = $var['text'];
        $type = $var['type'];
        $num = $var['num'];
        $subfix = $var['subfix'] ? $var['subfix'] : '...';

        if ($type == 'chars') {
            if (strlen($text) > $num) {
                $str = substr($text, 0, $num) . $subfix;
            } else {
                $str = $text;
            }
        }
        return $str;
    }

    public function timeCompact(array $vars = null)
    {
        //return $vars;
        $dateInit = $vars['dateInit'];
        $dateEnd = $vars['dateEnd'];
        $mode = $vars['mode'];

        $ini = explode(" ", $dateInit);
        $fIni = $ini[0];
        $hIni = $ini[1];
        $fIni = explode("-", $fIni);
        $hIni = explode(":", $hIni);

        $hora = $hIni[0] . "" . $hIni[1];

        $date1 = date_create($dateInit);
        $date2 = date_create($dateEnd);
        $diff = date_diff($date1, $date2);

        $dias = $diff->format("%d%");
        $anos = $diff->format("%y%");
        $meses = $diff->format("%m%");
        $horas = $diff->format("%h%");
        $min = $diff->format("%i%");
        $seg = $diff->format("%s%");
        //echo $elapsed;

        //return $dias.":".$anos.":".$meses.":".$horas.":".$min .":".$seg;

        if ($anos != 0) {
            return $this->fecha_hora_compacta($dateInit);
        }
        if ($meses != 0) {
            return $this->fecha_hora_compacta($dateInit);
        }

        if ($dias != 0) {
            return $this->fecha_hora_compacta($dateInit);
        }

        if ($horas != 0) {
            return $horas . "h";
        }

        if ($min != 0) {
            return $min . "m";
        }

        if ($seg != 0) {
            return $seg . "s";
        } else {
            return "1s";
        }
    }

    function fecha_hora_compacta($fecha, $formato = "d,m,a,h,mi,s")
    {
        $fechaHora = explode(" ", $fecha);
        $fechas = explode("-", $fechaHora[0]);
        $tiempo = explode(":", $fechaHora[1]);
        $ano = $fechas[0];
        $mes = (string) (int) $fechas[1];
        $dia = $fechas[2];
        $hora = $tiempo[0];
        $min = $tiempo[1];
        $seg = substr($tiempo[2], 0, 2);
        // $F="";


        $day = array(' ', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie');
        $month = array(' ', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');
        $fx = explode(",", $formato);
        $nfx = count($fx);

        $F .= "<div class='block-date'>";

        $d = " <span class='dia'>" . $dia . " </span>";
        $m = " <span class='mes'>" . $month[$mes] . " </span>";
        $a = " <span class='ano'>" . $ano . " </span>";
        $h = " <span class='hora'>" . $hora . "</span>";
        $mi = " <span class='min'>" . $min . "</span>";
        $s = " <span class='seg'>" . $seg . "</span>";

        for ($j = 0; $j < $nfx; $j++) {
            if ($fx[$j] == "d") {
                $F .= $d;
            }
            if ($fx[$j] == "m") {
                $F .= $m;
            }
            if ($fx[$j] == "a") {
                $F .= $a;
            }
            if ($fx[$j] == "h") {
                $F .= $h;
            }
            if ($fx[$j] == "mi") {
                $F .= $mi;
            }
            if ($fx[$j] == "s") {
                $F .= $s;
            }
        }

        $F .= "</div>";

        return $F;
    }

    public function createCode($vars = null)
    {
        $num = $this->fmt->emptyReturn(intval($vars["num"]), 6);
        $prefix = $this->fmt->emptyReturn($vars["prefix"], "");
        $mode = $this->fmt->emptyReturn($vars["mode"], "");

        switch ($mode) {
            case 'number':
                $pattern = "1234567890";
                break;
            case 'integer':
                $pattern = "123456789";
                break;

            default:
                $pattern = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%&*-";
                break;
        }

        //return  $pattern.":".$num;
        //$pattern = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%&*-";
        // $pattern = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $key = '';
        //return strlen($pattern);

        $key = substr(str_shuffle($pattern), 0, $num);

        return $prefix . $key;
    }

    public function formatDate($vars)
    {
        //return json_encode($vars);
        $fecha = $vars["date"];
        $modo = $this->fmt->emptyReturn($vars["mode"], "normal");
        $formato = $this->fmt->emptyReturn($vars["format"], "d,m,a,h,mi,s");
        $numDiaLiteral = $this->dayLiteralNum($fecha);
        $fechaHora = explode(" ", $fecha);
        $fechas = explode("-", $fechaHora[0]);
        $tiempo = explode(":", $fechaHora[1]);
        $ano = $fechas[0];
        $mes = (string) (int) $fechas[1];
        $dia = $fechas[2];
        $hora = $tiempo[0];
        $min = $tiempo[1];
        $seg = substr($tiempo[2], 0, 2);

        if ($modo == "mini") {
            $day = array(' ', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom');
            $month = array(' ', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');
        }
        if ($modo == "normal" || $modo == "lineal" || $modo == "clean" || $modo == "cleanDate") {
            $day = array(' ', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');
            $month = array(' ', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
        }

        if ($modo == "number") {
            $day = array(' ', '01', '02', '03', '04', '05', '06', '07');
            $month = array(' ', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
        }

        $fx = explode(",", $formato);
        $nfx = count($fx);

        $F = "<div class='block-date'>";

        $dl = "<span class='dayLiteral'>" . $day[$numDiaLiteral] . " </span>";
        $d = " <span class='day'>" . $dia . " </span>";
        $m = " <span class='month'>" . $month[$mes] . " </span>";
        $a = " <span class='year'>" . $ano . " </span>";
        $h = " <span class='hour'>" . $hora . "</span>";
        $mi = " <span class='min'>:" . $min . "</span>";
        $s = " <span class='seg'>:" . $seg . "</span>";

        if ($modo == "clean") {
            $dl = $day[$numDiaLiteral] . " ";
            $d = $dia . " de ";
            $m = $month[$mes] . " de ";
            $a = $ano;
            $h = ", " . $hora . ":";
            $mi = $min;
            $s = ":" . $seg;
        }

        if ($modo == "cleanDate") {
            $dl = $day[$numDiaLiteral] . ", ";
            $d = $dia . " de ";
            $m = $month[$mes] . " de ";
            $a = $ano;
            $h = $hora . ":";
            $mi = $min;
            $s = ":" . $seg;
        }

        for ($j = 0; $j < $nfx; $j++) {
            if ($fx[$j] == "dl") {
                $F .= $dl;
            }
            if ($fx[$j] == "d") {
                $F .= $d;
            }
            if ($fx[$j] == "m") {
                $F .= $m;
            }
            if ($fx[$j] == "a") {
                $F .= $a;
            }
            if ($fx[$j] == "h") {
                $F .= $h;
            }
            if ($fx[$j] == "mi") {
                $F .= $mi;
            }
            if ($fx[$j] == "s") {
                $F .= $s;
            }
        }

        $F .= "</div>";

        if ($formato == "d/m/a") {
            $F = "";
            if ($modo = "number") {
                $F .= $dia . "/" . $mes . "/" . $ano;
            } else {
                $F .= "<div class='block-date'>";
                $F .= $dia . "/" . $mes . "/" . $ano;
                $F .= "</div>";
            }
        }

        if ($modo == "linear") {
            $F = $dia . " de " . $month[$mes] . " de " . $ano;
        }

        return $F;
    }

    public function dayLiteralNum($date)
    {
        return strftime("%u", strtotime($date));
    }

    public function timeToDate($date, $format = "%H:%M")
    {
        return strftime($format, strtotime($date));
    }

    public function dayConverter($vars)
    {
        $day = $vars["day"];
        $type = $vars["type"];
        $lang = $vars["lang"];

        switch ($lang) {
            case 'es':
                $dayMin = array(' ', 'lun', 'mar', 'mie', 'jue', 'vie', 'sab', "dom");
                $dayNormal = array(' ', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'S&aacute;bado', "Domingo");
                break;
        }

        switch ($type) {
            case 'normal':
                for ($i = 0; $i < 8; $i++) {
                    if ($dayMin[$i] == $day) {
                        return $dayNormal[$i];
                    }
                }
                break;

            case 'min':
                for ($i = 0; $i < 8; $i++) {
                    if ($dayNormal[$i] == $day) {
                        return $dayMin[$i];
                    }
                }
                break;

            case 'numeral,min':
                return $dayMin[$day];
                break;

            case 'numeral,normal':
                return $dayNormal[$day];
                break;
        }
    }

    function calculateTimeDifference($vars = null)
    {

        //return $vars;

        $baseDate =  $vars["dateDiff"];
        $compareDate = $vars["dateCompare"];
        $timezone = "America/La_Paz";
        $setlocale = "es_ES";
        $language = 'es';
        // Set timezone and locale
        date_default_timezone_set($timezone);
        setlocale(LC_TIME, $setlocale);

        $baseDateRef = DateTime::createFromFormat('Y-m-d H:i:s', $baseDate);
        if (!$baseDateRef) {
            return ($language == 'es') ? "Formato de fecha inválido" : "Invalid date format";
        }

        // Convert base date to a DateTime object if it's a string
        if (is_string($baseDate)) {
            $baseDate = new DateTime($baseDate);
        }

        // Convert compare date to a DateTime object if it's a string
        if (is_string($compareDate)) {
            $compareDate = new DateTime($compareDate);
        }

        // Calculate difference in seconds
        $difference = $compareDate->getTimestamp() - $baseDate->getTimestamp();

        // Calculate difference in minutes
        $minutes = floor($difference / 60);
        $hours = floor($minutes / 60);
        $days = floor($hours / 24);
        $weeks = floor($days / 7);
        $months = floor($days / 30);
        $years = floor($days / 365);

        //return $minutes." min" . ($minutes > 1 ? "s" : "") . " ago" . $hours . " h" . ($hours > 1 ? "s" : "") . " ago" . $days . " d" . ($days > 1 ? "s" : "") . " ago" . $weeks . " w" . ($weeks > 1 ? "s" : "") . " ago";

        $dato['minutes'] = $minutes;
        $dato['hours'] = $hours;
        $dato['days'] = $days;
        $dato['weeks'] = $weeks;

        $dato['baseDate'] = $baseDate->format('Y-m-d H:i:s');
        $dato['compareDate'] = $compareDate->format('Y-m-d H:i:s');
        //return $dato;

        // Define thresholds for different time units
        /*  $hourThreshold = 60; // 60 minutes
        $dayThreshold = 24 * $hourThreshold; // 24 hours
        $weekThreshold = 7 * $dayThreshold; // 7 days */

        // Compare with thresholds and return the result
        if ($minutes < 5) {
            return ($language == 'es') ? "Menos de 5 minutos " : "Less than 5 minutes ago";
        }

        if ($minutes < 60) {
            return ($language == 'es') ? "Hace $minutes minuto" . ($minutes > 1 ? "s" : "") . " " : "About $minutes minute" . ($minutes > 1 ? "s" : "") . " ago";
        }

        if ($hours < 24) {
            return ($language == 'es') ? "Hace $hours hora" . ($hours > 1 ? "s" : "") . " " : "About $hours hour" . ($hours > 1 ? "s" : "") . " ago";
        }

        if ($days < 7) {
            return ($language == 'es') ? "Hace $days dia" . ($days > 1 ? "s" : "") . " " : "About $days day" . ($days > 1 ? "s" : "") . " ago";
        }

        if ($weeks < 4) {
            return ($language == 'es') ? "Hace $weeks semana" . ($weeks > 1 ? "s" : "") . " " : "About $weeks week" . ($weeks > 1 ? "s" : "") . " ago";
        }

        if ($months < 12) {
            return ($language == 'es') ? "Hace $months mes" . ($months > 1 ? "es" : "") . " " : "About $months month" . ($months > 1 ? "s" : "") . " ago";
        }

        return ($language == 'es') ? "Hace $years año" . ($years > 1 ? "s" : "") . " " : "About $years year" . ($years > 1 ? "s" : "") . " ago";



        /* elseif ($minutes < 60) {
            return ($language == 'es') ? "Hace $minutes minuto" . ($minutes > 1 ? "s" : "") . " " : "About $minutes minute" . ($minutes > 1 ? "s" : "") . " ago";
        } elseif ($minutes < $dayThreshold) {
            $hours = floor($minutes / 60);
            return ($language == 'es') ? "Hace $hours hora" . ($hours > 1 ? "s" : "") . " " : "About $hours hour" . ($hours > 1 ? "s" : "") . " ago";
        } elseif ($minutes < $weekThreshold ) {
            $days = floor($minutes / $hourThreshold);
            return ($language == 'es') ? "Hace $days día" . ($days > 1 ? "s" : "") . " " : "About $days day" . ($days > 1 ? "s" : "") . " ago";
        } elseif ($minutes < $weekThreshold) {
            $weeks = floor($minutes / $dayThreshold);
            return ($language == 'es') ? "Hace $weeks semana" . ($weeks > 1 ? "s" : "") . " " : "About $weeks week" . ($weeks > 1 ? "s" : "") . " ago";
        } else {
            return ($language == 'es') ? "Hace más de una semana " : "More than a week ago";
        } */
    }
}