<?PHP
header('Content-Type: text/html; charset=utf-8');
class ERRORS
{

	var $fmt;

	function __construct($fmt)
	{
		$this->fmt = $fmt;
	}

	public function errorJson($vars)
	{
		/*
			  Codes:
			  300 = Error Email   
			  */
		return json_encode([
			'Error' => '1',
			'status' => 'error',
			'message' => 'Error,' . $vars['error'] . ' ' . $vars["description"] . '',
			'description' => $vars["description"],
			'code' => $vars["code"],
			'lang' => $vars["lang"]
		]);
	}
}