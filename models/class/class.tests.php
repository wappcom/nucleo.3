<?PHP
header('Content-Type: text/html; charset=utf-8');
class TESTS
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    function deleteFileTest($fileId)
    {
        $dataFile = $this->fmt->files->dataItem($fileId);
        $route = _PATH_HOST_FILES . $dataFile["pathurl"];
        $routeThumb = _PATH_HOST_FILES . $dataFile["thumb"];
        $this->fmt->files->deleteFile($route);
        $this->fmt->files->deleteFile($routeThumb);

        $sql = "DELETE FROM files WHERE file_id = '" . $fileId . "'";
        $this->fmt->querys->consult($sql, __METHOD__);
        $sql = "ALTER TABLE files AUTO_INCREMENT = 1";
        $this->fmt->querys->consult($sql, __METHOD__);
    }

    function deleteTeamId(array $var = null)
    {

        $teamId = $var["teamId"];
        $fileId = $var["fileId"];

        $this->deleteFileTest($fileId);

        $sql = "DELETE FROM mod_sports_teams WHERE mod_sp_tm_id = '" . $teamId . "'";
        $this->fmt->querys->consult($sql, __METHOD__);
        $sql = "ALTER TABLE mod_sports_teams AUTO_INCREMENT = 1";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "DELETE FROM mod_leagues_divisions_teams WHERE mod_lg_div_tm_tm_id = '" . $teamId . "'";
        $this->fmt->querys->consult($sql, __METHOD__);

    }

    public function deleteAllFiles(array $var = null)
    {
        //delete truncate table files
        $sql = "TRUNCATE TABLE files";
        $this->fmt->querys->consult($sql, __METHOD__);

        //delete all files in folder and sub folder files



    }

    public function truncateUserAccount(array $var = null)
    {
        //return $var;
        //truncate
        $sql = "TRUNCATE TABLE mod_accounts_users";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "TRUNCATE TABLE mod_accounts_users_plans";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "TRUNCATE TABLE mod_accounts_token";
        $this->fmt->querys->consult($sql, __METHOD__);
    }

    public function truncateCustomerPersona(array $var = null)
    {
        $sql = "TRUNCATE TABLE mod_customers_persons";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "TRUNCATE TABLE mod_customers_persons_data";
        $this->fmt->querys->consult($sql, __METHOD__);

        return $var;

    }

    public function truncatePayments(array $var = null)
    {
        //return $var;

        $sql = "TRUNCATE TABLE mod_pay_tx_temp";
        $this->fmt->querys->consult($sql, __METHOD__);

        return $var;
        
    }
}