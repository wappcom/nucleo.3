<?php
header('Content-Type: text/html; charset=utf-8');
class POSTS
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function dataId(int $id = null, int $state = 0)
    {

        if ($state == 1) {
            $stateSql = 'AND post_state > 0';
        }

        // post_id	post_title	post_pathurl	post_tags	post_review	post_img	post_body	post_place	post_author	post_cls	post_register_date	post_state	

        $sql = "SELECT * FROM posts WHERE post_id='" . $id . "' " . $stateSql;
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {

            $row = $this->fmt->querys->row($rs);
            $id = $row["post_id"];
            $authorId = $row["post_author"];
            $entitieId = $row["post_ent_id"];
            $return["id"] = $id;
            $return["title"] = $row["post_title"];
            $return["subtitle"] = $row["post_subtitle"];
            $return["review"] = $row["post_review"];
            $return["tags"] = $row["post_tags"];
            $return["body"] = $row["post_body"];
            $return["place"] = $row["post_place"];
            $return["author"] = $this->getAuthorId($authorId);
            $return["registerDate"] = $row["post_register_date"];
            $return["pathurl"] = $row["post_pathurl"];
            $return["cls"] = $row["post_cls"];
            $return["json"] = $row["post_json"];
            $return["date"] = $row["post_date"];
            $return["state"] = $row["post_state"];

            $date = explode(" ", $row["post_date"]);
            $category = $this->relationCats(["postId" => $id, "entitieId" => $entitieId]);

            $return["link"] = $date[0] . "/" . $category[0]['pathurl'] . "/" . $row["post_pathurl"] . ".html";

            if ($row["post_img"]) {
                //saber si el elemento es un numero o una cadena
                $imgId = $row["post_img"];
                $img = [];
                $img["id"] = $imgId;
                $img["pathurl"] = $this->fmt->files->imgId($row["post_img"], "");
                $img["thumb"] = $this->fmt->files->thumb($img["pathurl"]);
                $img["name"] = $this->fmt->files->name($row["post_img"], "");

                if (!is_numeric($imgId)) {
                    $img["id"] = $row["post_img"];
                    $img["pathurl"] = $row["post_img"];
                    $img["thumb"] = "";
                    $img["name"] = $row["post_pathurl"];
                }
            } else {
                $img = 0;
            }

            if ($row["post_video"]) {
                $video = [];
                $video["id"] = $row["post_video"];
                $arrayVideo = $this->fmt->files->dataItem($row["post_video"], "");
                $video["pathurl"] = $arrayVideo["pathurl"];
                $video["type"] = 'video';
                $video["name"] = $arrayVideo["name"];
            } else {
                $video = 0;
            }

            $return["video"] = $video;
            $return["previewVideo"] = $row["post_preview_video"];
            $return["internVideo"] = $row["post_intern_video"];

            $return["img"] = $img;
            $return["imgRef"] = $row["post_img_ref"];
            $return["embed"] = $row["post_embed"];
            $return["previewEmbed"] = $row["post_preview_embed"];
            $return["internEmbed"] = $row["post_intern_embed"];

            $return["media"] = $this->relationFromFiles($id);
            /* $return["categorys"] = $this->fmt->categorys->relationFrom(
                array(
                    "from" => "posts_categorys",
                    "colId" => "post_cat_post_id",
                    "id" => $id,
                    "colCategory" => "post_cat_cat_id",
                    "orderBy" => "post_cat_order",
                    "actives" => 0
                )
            ); */

            $return["categorys"] = $this->relationCats(["postId" => $id, "entitieId" => $entitieId]);

            return $return;
        } else {
            return 0;
        }
    }

    public function searchPost(array $var = null)
    {
        //return $var;

        $entitieId = $var["entitieId"] ?? 1;
        $search = $this->fmt->emptyReturn($var["vars"]["inputs"]["inputSearch"], "");

        //verificar el largo de caracteres de search tiene que ser mayor a 3 palabras

        if (strlen($search) < 3) {
            return 0;
        }

        $order = $this->fmt->emptyReturn($var["vars"]["inputs"]["inputOrder"], "post_date DESC");
        $limit = $this->fmt->emptyReturn($var["vars"]["inputs"]["inputLimit"], "LIMIT 0,150");

        $searchAuthor = explode("author:", $search);
        $author = $searchAuthor[1];

        $searchQuery = "post_title LIKE '%" . $search . "%' OR post_tags LIKE '%" . $search . "%' OR post_date LIKE '%" . $search . "%' OR post_pathurl LIKE '%" . $search . "%'";
        $sql = "SELECT post_id, post_title, post_author, post_tags, post_date, post_pathurl, post_state FROM posts WHERE post_ent_id='" . $entitieId . "' AND " . $searchQuery . " ORDER BY " . $order . " " . $limit;

        if (!empty($author)) {
            $sql = "SELECT post_id, post_title, post_author, post_tags, post_date, post_pathurl, post_state FROM posts, posts_authors WHERE post_au_name LIKE '%" . $author . "%' AND post_au_post_id=post_author AND post_ent_id='" . $entitieId . "'  ORDER BY " . $order . " " . $limit;
        }

        //return $sql;

        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);

                $id = $row["post_id"];
                $authorId = $row["post_author"];

                $return[$i]["id"] = $id;
                $return[$i]["title"] = $row["post_title"];
                $return[$i]["tags"] = $row["post_tags"];
                $return[$i]["date"] = $row["post_date"];
                $return[$i]["pathurl"] = $row["post_pathurl"];
                $rtn[$i]["author"] = $this->getAuthorId($authorId);
                $rtn[$i]["categorys"] = $this->relationCats(["postId" => $id, "entitieId" => $entitieId]);
                $return[$i]["state"] = $row["post_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function getPosts(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"] ?? 1;
        $order = $this->fmt->emptyReturn($var["order"], "post_id DESC");
        $limit = $this->fmt->emptyReturn($var["limit"], "LIMIT 0,150");
        $state = $this->fmt->emptyReturn($var["state"], "");

        $sql = "SELECT * FROM posts WHERE post_ent_id='" . $entitieId . "' " . $state . " ORDER BY " . $order . " " . $limit;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["post_id"];
                $authorId = $row["post_author"];
                $return[$i]["id"] = $id;
                $return[$i]["title"] = $row["post_title"];
                $return[$i]["pathurl"] = $row["post_pathurl"];
                $return[$i]["tags"] = $row["post_tags"];
                $return[$i]["review"] = $row["post_review"];

                if ($row["post_img"] != "") {
                    $img = [];
                    $img["id"] = $row["post_img"];
                    $pathUrl = $this->fmt->files->imgId($row["post_img"], "");
                    $thumb = $this->fmt->files->thumb($pathUrl);
                    $img["pathurl"] = $pathUrl;
                    $img["thumb"] = $thumb;
                    $img["name"] = $this->fmt->files->name($row["post_img"], "");
                } else {
                    $img = 0;
                }

                if ($row["post_video"] != "") {
                    $video = [];
                    $video["id"] = $row["post_video"];
                    $arrayVideo = $this->fmt->files->dataItem($row["post_video"], "");
                    $video["pathurl"] = $arrayVideo["pathurl"];
                    $video["type"] = 'video';
                    $video["name"] = $arrayVideo["name"];
                } else {
                    $video = 0;
                }

                $date = explode(" ", $row["post_date"]);
                $category = $this->relationCats(["postId" => $id, "entitieId" => $entitieId]);

                $return[$i]["img"] = $img;
                $return[$i]["link"] = $date[0] . "/" . $category[0]['pathurl'] . "/" . $row["post_pathurl"] . ".html";
                $return[$i]["imgRef"] = $row["post_img_ref"];
                $return[$i]["embed"] = $row["post_embed"];
                $return[$i]["previewEmbed"] = $row["post_preview_embed"];
                $return[$i]["internEmbed"] = $row["post_intern_embed"];
                $return[$i]["video"] = $video;
                $return[$i]["previewVideo"] = $row["post_preview_video"];
                $return[$i]["internVideo"] = $row["post_intern_video"];
                $return[$i]["body"] = $row["post_body"];
                $return[$i]["place"] = $row["post_place"];
                $return[$i]["author"] = $this->getAuthorId($authorId);
                $return[$i]["cls"] = $row["post_cls"];
                $return[$i]["json"] = $row["post_json"];
                $return[$i]["date"] = $row["post_date"];
                $return[$i]["categorys"] = $this->relationCats(["postId" => $id, "entitieId" => $entitieId]);
                $return[$i]["registerDate"] = $row["post_register_date"];
                $return[$i]["entId"] = $row["post_ent_id"];
                $return[$i]["state"] = $row["post_state"];
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function getPostsList(array $var = null)
    {
        return $var;
        
    }

    public function getPostsDateChildrenCategory(array $vars = null)
    {
        //return $vars;
        $parentCatId = $vars["parentCatId"];
        $entitieId = $vars["entitieId"];
        $now = $this->fmt->modules->dateFormate(['format' => 'Y-m-d']);

        $date = $vars["date"] ?? $now;

        //$limit = $limit = $this->fmt->emptyReturn($vars["limit"], "LIMIT 0,10");
        $limit = $vars["limit"];

        if (!empty($limit)) {
            $limit = "LIMIT " . $limit;
        } else {
            $limit = "LIMIT 0,10";
        }

        $sql = "SELECT * FROM categorys WHERE cat_parent_id = '" . $parentCatId . "' AND cat_ent_id = '" . $entitieId . "' AND cat_state > 0 ORDER BY cat_name DESC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["cat_id"];
                $children[$i] = $id;
                //$children[$i]["name"] = $row["cat_name"];
            }
            //return $children;
        } else {
            return 0;
        }

        $children = implode(",", $children);

        $remove = array("13", "14", "39");
        $elem = explode(",", $children);
        $elem_filter = array_diff($elem, $remove);
        $child = implode(",", $elem_filter);
        //var_dump($child); exit(0);
        //return $children;
        //var_dump ($children);
        if ($date != 0) {
            $date = "AND DATE(post_date) = '" . $date . "'";
        } else {
            $date = "";
        }

        $sql = "SELECT DISTINCT post_id, post_title, post_pathurl, post_date, post_cat_cat_id,post_author FROM posts, posts_categorys WHERE post_cat_cat_id IN (" . $child . ") AND post_state > 0 AND post_cat_ent_id = '" . $entitieId . "' AND post_cat_post_id = post_id " . $date . "  ORDER BY post_date DESC " . $limit;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $authorId = $row["post_author"];
                $return[$i]["id"] = $row["post_id"];
                $return[$i]["title"] = $row["post_title"];
                $return[$i]["pathurl"] = $row["post_pathurl"];
                $return[$i]["date"] = $row["post_date"];
                $return[$i]["post"] = $this->dataId($row["post_id"]);
                $return[$i]["author"] = $this->getAuthorId($authorId);
                $return[$i]["categorys"] = $this->fmt->categorys->dataIdBasic($row["post_cat_cat_id"]);
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function getPostHashtagCat($hashtag = null, $catId = null)
    {
        //return $hashtag." ".$catId;

        $sql = "SELECT post_id FROM posts, posts_categorys WHERE post_cat_cat_id='" . $catId . "' AND post_cat_post_id=post_id AND post_tags LIKE '%" . $hashtag . "%' AND post_state > 0 ORDER BY post_date DESC LIMIT 0,1";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["post_id"];
        } else {
            return 0;
        }
    }

    public function relationFromFiles($id = null)
    {
        $sql = "SELECT DISTINCT * FROM files, posts_files WHERE post_file_file_id=file_id AND post_file_post_id='" . $id . "' AND file_state > 0 ORDER BY post_file_order ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return[$i]["id"] = $row["post_file_file_id"];
                $return[$i]["title"] = $row["post_file_title"];
                $return[$i]["summary"] = $row["post_file_summary"];
                $return[$i]["url"] = $row["post_file_url"];
                $return[$i]["target"] = $row["post_file_target"];
                $return[$i]["pathurl"] = $row["file_pathurl"];
                $return[$i]["btnTitle"] = $row["file_btn_title"];
                $return[$i]["order"] = $i;
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function postLastDateId($date = "", $catId = "")
    {
        //return $var;
        $sql = "SELECT post_id FROM posts, posts_categorys WHERE DATE(post_date)='" . $date . "' AND post_cat_cat_id='" . $catId . "' post_cat_post_id=post_id ORDER BY post_data DESC LIMIT 0,1";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            return $row["post_id"];
        } else {
            return 0;
        }

    }

    public function relationCat(array $vars = null)
    {
        $catId = $vars["catId"];
        //$postId = $vars["postId"];
        $entitieId = $vars["entitieId"] ?? 1;
        $order = $this->fmt->emptyReturn($vars["order"], "post_cat_order ASC");
        $limit = $vars["limit"];
        $and = $vars["and"];
        $filter = $vars["filter"] ?? "";
        $now = $this->fmt->data->dateFormat("", "Y-m-d");

        $avoidIds = $this->fmt->emptyReturn($vars["avoidIds"], "");

        if (!empty($limit)) {
            $limit = "LIMIT " . $limit;
        } else {
            $limit = "LIMIT 0,100";
        }

        if (!empty($and)) {
            $and = "AND " . $and;
        } else {
            $and = "";
        }

        if ($filter == "date<=") {
            $and = $and . " AND  DATE(post_date) <= '" . $now . "' ";
        }

        if (!empty($avoidIds)) {
            //saber si $avoidIds es un array
            if (is_array($avoidIds)) {
                $avoidIds = implode(",", $avoidIds);
            }else{
                $avoidIds = $avoidIds;
            }
            $and = $and . " AND  post_id NOT IN (" . $avoidIds . ") ";
        }


        $sql = "SELECT DISTINCT * FROM posts,posts_categorys WHERE post_cat_cat_id='" . $catId . "' AND post_cat_post_id=post_id AND post_cat_ent_id='" . $entitieId . "' AND post_state > 0 " . $and . "  ORDER BY " . $order . " " . $limit;
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["post_id"];
                $authorId = $row["post_author"];

                $rtn[$i]["id"] = $id;

                $rtn[$i]["title"] = $row["post_title"];
                $rtn[$i]["review"] = $row["post_review"];
                // $rtn[$i]["url"] = $row["post_url"];
                $rtn[$i]["pathurl"] = $row["post_pathurl"];
                $rtn[$i]["target"] = $row["post_target"];
                $rtn[$i]["order"] = $row["post_cat_order"];

                if ($row["post_img"] != "") {
                    $img = [];
                    $img["id"] = $row["post_img"];
                    $pathUrl = $this->fmt->files->imgId($row["post_img"], "");
                    $thumb = $this->fmt->files->thumb($pathUrl);
                    $img["pathurl"] = $pathUrl;
                    $img["thumb"] = $thumb;
                    $img["name"] = $this->fmt->files->name($row["post_img"], "");

                    if (!is_numeric($row["post_img"])) {
                        $img["id"] = $row["post_img"];
                        $img["pathurl"] = $row["post_img"];
                        $img["thumb"] = "";
                        $img["name"] = $row["post_pathurl"];
                    }
                } else {
                    $img = 0;
                }

                $rtn[$i]["img"] = $img;
                $rtn[$i]["imgRef"] = $row["post_img_ref"];
                $rtn[$i]["author"] = $this->getAuthorId($authorId);
                $rtn[$i]["categorys"] = $this->relationCats(["postId" => $id, "entitieId" => $entitieId]);
                $rtn[$i]["embed"] = $row["post_embed"];
                $rtn[$i]["previewEmbed"] = $row["post_preview_embed"];
                $rtn[$i]["internEmbed"] = $row["post_intern_embed"];

                if ($row["post_video"]) {
                    $video = [];
                    $video["id"] = $row["post_video"];
                    $arrayVideo = $this->fmt->files->dataItem($row["post_video"], "");
                    $video["pathurl"] = $arrayVideo["pathurl"];
                    $video["type"] = 'video';
                    $video["name"] = $arrayVideo["name"];
                } else {
                    $video = 0;
                }

                $rtn[$i]["video"] = $video;
                $rtn[$i]["previewVideo"] = $row["post_preview_video"];
                $rtn[$i]["internVideo"] = $row["post_intern_video"];

                $rtn[$i]["cls"] = $row["post_cls"];
                $rtn[$i]["body"] = $row["post_body"];
                $rtn[$i]["json"] = $row["post_json"];
                $rtn[$i]["date"] = $row["post_date"];
            }
            return $rtn;
        } else {
            return 0;
        }

    }

    public function relationTag(array $vars = null)
    {
        $tag = $vars["tag"];
        //$postId = $vars["postId"];
        $entitieId = $vars["entitieId"] ?? 1;
        $order = $this->fmt->emptyReturn($vars["order"], "post_date ASC");
        $limit = $vars["limit"];
        $and = $vars["and"];
        $filter = $vars["filter"] ?? "";
        $now = $this->fmt->data->dateFormat("", "Y-m-d");

        $avoidIds = $this->fmt->emptyReturn($vars["avoidIds"], "");

        if (!empty($limit)) {
            $limit = "LIMIT " . $limit;
        } else {
            $limit = "LIMIT 0,100";
        }

        if (!empty($and)) {
            $and = "AND " . $and;
        } else {
            $and = "";
        }

        if ($filter == "date<=") {
            $and = $and . " AND  DATE(post_date) <= '" . $now . "' ";
        }

        if (!empty($avoidIds)) {
            $avoidIds = implode(",", $avoidIds);
            $and = $and . " AND  post_id NOT IN (" . $avoidIds . ") ";
        }


        //$sql = "SELECT DISTINCT * FROM posts  WHERE post_tags='" . $tag . "' AND post_ent_id='" . $entitieId . "' AND post_state > 0 " . $and . "  ORDER BY " . $order . " " . $limit;

        $sql = "SELECT DISTINCT * FROM posts 
        WHERE post_tags LIKE '%" . $tag . "%' 
        AND post_ent_id = '" . $entitieId . "' 
        AND post_state > 0 " . $and . " 
        ORDER BY " . $order . " " . $limit;

        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["post_id"];
                $authorId = $row["post_author"];

                $rtn[$i]["id"] = $id;

                $rtn[$i]["title"] = $row["post_title"];
                $rtn[$i]["review"] = $row["post_review"];
                // $rtn[$i]["url"] = $row["post_url"];
                $rtn[$i]["pathurl"] = $row["post_pathurl"];
                $rtn[$i]["target"] = $row["post_target"];
                $rtn[$i]["order"] = $row["post_cat_order"];

                if ($row["post_img"] != "") {
                    $img = [];
                    $img["id"] = $row["post_img"];
                    $pathUrl = $this->fmt->files->imgId($row["post_img"], "");
                    $thumb = $this->fmt->files->thumb($pathUrl);
                    $img["pathurl"] = $pathUrl;
                    $img["thumb"] = $thumb;
                    $img["name"] = $this->fmt->files->name($row["post_img"], "");

                    if (!is_numeric($row["post_img"])) {
                        $img["id"] = $row["post_img"];
                        $img["pathurl"] = $row["post_img"];
                        $img["thumb"] = "";
                        $img["name"] = $row["post_pathurl"];
                    }
                } else {
                    $img = 0;
                }

                $rtn[$i]["img"] = $img;
                $rtn[$i]["imgRef"] = $row["post_img_ref"];
                $rtn[$i]["author"] = $this->getAuthorId($authorId);
                $rtn[$i]["categorys"] = $this->relationCats(["postId" => $id, "entitieId" => $entitieId]);
                $rtn[$i]["embed"] = $row["post_embed"];
                $rtn[$i]["previewEmbed"] = $row["post_preview_embed"];
                $rtn[$i]["internEmbed"] = $row["post_intern_embed"];

                if ($row["post_video"]) {
                    $video = [];
                    $video["id"] = $row["post_video"];
                    $arrayVideo = $this->fmt->files->dataItem($row["post_video"], "");
                    $video["pathurl"] = $arrayVideo["pathurl"];
                    $video["type"] = 'video';
                    $video["name"] = $arrayVideo["name"];
                } else {
                    $video = 0;
                }

                $rtn[$i]["video"] = $video;
                $rtn[$i]["previewVideo"] = $row["post_preview_video"];
                $rtn[$i]["internVideo"] = $row["post_intern_video"];

                $rtn[$i]["cls"] = $row["post_cls"];
                $rtn[$i]["body"] = $row["post_body"];
                $rtn[$i]["json"] = $row["post_json"];
                $rtn[$i]["date"] = $row["post_date"];
            }
            return $rtn;
        } else {
            return 0;
        }

    }

    public function relationCatDate(array $vars = null)
    {
        $catId = $vars["catId"];
        $date = $vars["date"];
        //$postId = $vars["postId"];
        $entitieId = $vars["entitieId"] ?? 1;
        $order = $this->fmt->emptyReturn($vars["order"], "post_date ASC");
        $limit = $vars["limit"];
        $and = $vars["and"];

        if (!empty($limit)) {
            $limit = "LIMIT " . $limit;
        } else {
            $limit = "LIMIT 0,100";
        }

        if (!empty($and)) {
            $and = "AND " . $and;
        } else {
            $and = "";
        }


        $sql = "SELECT DISTINCT * FROM posts,posts_categorys WHERE post_cat_cat_id='" . $catId . "' AND post_cat_post_id=post_id AND post_cat_ent_id='" . $entitieId . "' AND DATE(post_date) = '" . $date . "' AND post_state > 0 " . $and . "  ORDER BY " . $order . " " . $limit;
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["post_id"];
                $authorId = $row["post_author"];

                $rtn[$i]["id"] = $id;

                $rtn[$i]["title"] = $row["post_title"];
                $rtn[$i]["review"] = $row["post_review"];
                // $rtn[$i]["url"] = $row["post_url"];
                $rtn[$i]["pathurl"] = $row["post_pathurl"];
                $rtn[$i]["target"] = $row["post_target"];
                $rtn[$i]["order"] = $row["post_cat_order"];

                if ($row["post_img"] != "") {
                    $img = [];
                    $img["id"] = $row["post_img"];
                    $pathUrl = $this->fmt->files->imgId($row["post_img"], "");
                    $thumb = $this->fmt->files->thumb($pathUrl);
                    $img["pathurl"] = $pathUrl;
                    $img["thumb"] = $thumb;
                    $img["name"] = $this->fmt->files->name($row["post_img"], "");

                    if (!is_numeric($row["post_img"])) {
                        $img["id"] = $row["post_img"];
                        $img["pathurl"] = $row["post_img"];
                        $img["thumb"] = "";
                        $img["name"] = $row["post_pathurl"];
                    }
                } else {
                    $img = 0;
                }

                $rtn[$i]["img"] = $img;
                $rtn[$i]["imgRef"] = $row["post_img_ref"];
                $rtn[$i]["author"] = $this->getAuthorId($authorId);
                $rtn[$i]["categorys"] = $this->relationCats(["postId" => $id, "entitieId" => $entitieId]);
                $rtn[$i]["embed"] = $row["post_embed"];
                $rtn[$i]["previewEmbed"] = $row["post_preview_embed"];
                $rtn[$i]["internEmbed"] = $row["post_intern_embed"];
                $rtn[$i]["cls"] = $row["post_cls"];
                $rtn[$i]["body"] = $row["post_body"];
                $rtn[$i]["json"] = $row["post_json"];
                $rtn[$i]["date"] = $row["post_date"];
            }
            return $rtn;
        } else {
            return 0;
        }

    }

    public function relationCats(array $vars = null)
    {
        //return $vars;
        $postId = $vars["postId"];
        $entitieId = $vars["entitieId"];
        $order = $this->fmt->emptyReturn($vars["order"], "post_cat_order, cat_name, cat_id ASC");
        $limit = $this->fmt->emptyReturn($vars["limit"], "LIMIT 0,100");

        $sql = "SELECT DISTINCT cat_name, cat_id, cat_pathurl,post_cat_order FROM posts,posts_categorys, categorys WHERE post_cat_post_id='" . $postId . "' AND post_cat_cat_id=cat_id AND post_cat_ent_id='" . $entitieId . "' AND post_state > 0 ORDER BY  " . $order . " " . $limit;
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return[$i]["id"] = $row["cat_id"];
                $return[$i]["name"] = $row["cat_name"];
                $return[$i]["pathurl"] = $row["cat_pathurl"];


            }
            return $return;
        } else {
            return 0;
        }

    }

    public function getDataPostId(array $var = null)
    {
        //return $var;

        $entitieId = $var["entitieId"];
        $item = $var["vars"]["item"];

        $rtn = $this->dataId($item);
        $rtn["listCategorys"] = $this->fmt->categorys->getCategorys(array("entitieId" => $entitieId));

        return $rtn;

    }

    public function getPathPostToId($path = null)
    {
        //return $var;

        $sql = "SELECT * FROM posts WHERE post_pathurl='" . $path . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["post_id"];

            return $id;
        } else {
            return 0;
        }

    }

    public function getPostMultiple(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $items = $var["vars"]["inputs"];

        if (!is_array($items)) {
            return 0;
        }
        $rtn = [];

        foreach ($items as $key => $value) {
            $rtn[$key] = $this->dataId($value);
        }

        return $rtn;

    }

    public function getDataPosts(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];

        $rtn["categorys"] = $this->fmt->categorys->getCategorys(array("entitieId" => $entitieId));
        $rtn["date"] = $this->fmt->modules->dateFormat();
        //$return["Autors"] = $this->getAutors(array("entitieId" => $entitieId));

        return $rtn;
    }

    public function getDataConfigPosts(array $var = null)
    {
        $entitieId = $var["entitieId"];
        $rtn["authors"] = $this->getAuthors(["entitieId" => $entitieId]);
        return $rtn;
    }

    public function updatePost(array $var = null)
    {
        //return $var;
        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        $input = $vars["inputs"];

        $inputId = $input["inputId"];
        $title = $input["inputTitle"];
        $pathurl = $input["inputPathurl"];
        $summary = $input["inputSummary"];
        $tags = $input["inputTags"];
        $body = $input["inputBody"];
        $authorId = $input["inputAuthor"];
        $img = $input["inputImg"];
        $imgRef = $input["inputImgRef"];
        $embed = $input["inputEmbed"] ? $input["inputEmbed"] : '';
        $previewEmbed = $input["inputPreviewEmbed"] ? $input["inputPreviewEmbed"] : 0;
        $internEmbed = $input["inputInternEmbed"] ? $input["inputInternEmbed"] : 0;
        $video = $input["inputVideo"] ? $input["inputVideo"] : '';
        $previewVideo = $input["inputPreviewVideo"] ? $input["inputPreviewVideo"] : 0;
        $internVideo = $input["inputInternVideo"] ? $input["inputInternVideo"] : 0;
        $place = '';
        $cls = $input["inputCls"];
        $topRelations = $input["inputTopRelations"];
        $middleRelations = $input["inputMiddleRelations"];
        $lastRelations = $input["inputLastRelations"];
        $date = $input["inputDate"] ? $input["inputDate"] : $this->fmt->modules->dateFormat();
        $json = $input["inputJson"];
        $categorys = $input["categorys"];
        $mediaFiles = $input["mediaFiles"];
        $state = $input["inputState"];


        $todaysDate = $this->fmt->modules->dateFormat();

        $sql = "UPDATE posts SET
                post_title='" . $title . "',
                post_pathurl='" . $pathurl . "',
                post_tags='" . $tags . "',
                post_review='" . $summary . "',
                post_img='" . $img . "',
                post_img_ref='" . $imgRef . "',
                post_embed='" . $embed . "',
                post_preview_embed='" . $previewEmbed . "',
                post_intern_embed='" . $internEmbed . "',
                post_video='" . $video . "',
                post_preview_video='" . $previewVideo . "',
                post_intern_video='" . $internVideo . "',
                post_body='" . $body . "',
                post_place='" . $place . "',
                post_author='" . $authorId . "',
                post_cls='" . $cls . "',
                post_top_relations='" . $topRelations . "',
                post_middle_relations='" . $middleRelations . "',
                post_last_relations='" . $lastRelations . "',
                post_json='" . $json . "',
                post_date='" . $date . "',
                post_register_date='" . $todaysDate . "',
                post_state='" . $state . "'
                WHERE post_id   = '" . $inputId . "' ";
        $this->fmt->querys->consult($sql);

        $this->fmt->modules->deleteRelation(array("from" => "posts_categorys", "column" => "post_cat_post_id", "item" => $inputId));

        $insert = 'post_cat_post_id,post_cat_cat_id,post_cat_ent_id,post_cat_order';
        $countCategorys = count($categorys);
        for ($i = 0; $i < $countCategorys; $i++) {
            $values = "'" . $inputId . "','" . $categorys[$i] . "','" . $entitieId . "','" . $i . "'";
            $sql = "insert into posts_categorys (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        $this->fmt->modules->deleteRelation(array("from" => "posts_files", "column" => "post_file_post_id", "item" => $inputId));

        $insert = 'post_file_post_id,post_file_file_id,post_file_order';
        $countFiles = count($mediaFiles);

        for ($i = 0; $i < $countFiles; $i++) {
            $values = "'" . $inputId . "','" . $mediaFiles[$i] . "','" . $i . "'";
            $sql = "insert into posts_files (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        return 1;

    }

    public function updateSelectionMultiplePost(array $var = null)
    {
        // return $var;
        $entitieId = $var["entitieId"];
        $inputs = $var["vars"]["inputs"];

        $pubId = $inputs["pubId"];
        $pubJson = $inputs["pubJson"];

        return $this->fmt->pubs->updateField(["pubId" => $pubId, "field" => "pub_json", "value" => $pubJson]);

    }

    public function deletePost(array $var = null)
    {
        //return $var;
        $postId = $var['vars']["item"];

        $sql = "DELETE FROM posts WHERE post_id = '" . $postId . "'";
        $this->fmt->querys->consult($sql, __METHOD__);
        $sql = "ALTER TABLE  posts AUTO_INCREMENT = 1";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "DELETE FROM posts_categorys WHERE  post_cat_post_id = '" . $postId . "'";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "DELETE FROM posts_files WHERE  post_file_post_id = '" . $postId . "'";
        $this->fmt->querys->consult($sql, __METHOD__);

        return 1;

    }

    public function changeState(array $var = null)
    {
        //return $var;

        $inputs = $var["vars"]["inputs"];
        $inputId = $inputs["inputId"];
        $state = $inputs["inputState"];


        $sql = "UPDATE posts SET post_state='" . $state . "' WHERE post_id='" . $inputId . "'";
        $this->fmt->querys->consult($sql, __METHOD__);

        return 1;

    }
    
    public function saveNewPost(array $var = null)
    {
        //return $var;
        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        $input = $vars["inputs"];

        $title = $input["inputTitle"];
        $pathurl = $input["inputPathurl"];
        $summary = $input["inputSummary"];
        $tags = $input["inputTags"];
        $body = $input["inputBody"];
        $authorId = $input["inputAuthor"];
        $img = $input["inputImg"];
        $imgRef = $input["inputImgRef"];
        $embed = $input["inputEmbed"] ? $input["inputEmbed"] : '';
        $previewEmbed = $input["inputPreviewEmbed"] ? $input["inputPreviewEmbed"] : '';
        $internEmbed = $input["inputInternEmbed"] ? $input["inputInternEmbed"] : '';
        $video = $input["inputVideo"] ? $input["inputVideo"] : '';
        $previewVideo = $input["inputPreviewVideo"] ? $input["inputPreviewVideo"] : '';
        $internVideo = $input["inputInternVideo"] ? $input["inputInternVideo"] : '';
        $place = '';
        $cls = $input["inputCls"];
        $topRelations = $input["inputTopRelations"];
        $middleRelations = $input["inputMiddleRelations"];
        $lastRelations = $input["inputLastRelations"];
        $date = $input["inputDate"] ? $input["inputDate"] : $this->fmt->modules->dateFormat();
        $json = $input["inputJson"];
        $categorys = $input["categorys"];
        $mediaFiles = $input["mediaFiles"];
        $state = $input["inputState"];


        $todaysDate = $this->fmt->modules->dateFormat();

        $insert = "post_title,post_pathurl,post_tags,post_review,post_img,post_img_ref,post_embed,post_preview_embed,post_intern_embed,post_video,post_preview_video,post_intern_video,post_body,post_place,post_author,post_cls,post_top_relations,post_middle_relations,post_last_relations,post_json,post_date,post_register_date,post_ent_id,post_user_id,post_state";
        $values = "'" . $title . "','" .
            $pathurl . "','" .
            $tags . "','" .
            $summary . "','" .
            $img . "','" .
            $imgRef . "','" .
            $embed . "','" .
            $previewEmbed . "','" .
            $internEmbed . "','" .
            $video . "','" .
            $previewVideo . "','" .
            $internVideo . "','" .
            $body . "','" .
            $place . "','" .
            $authorId . "','" .
            $cls . "','" .
            $topRelations . "','" .
            $middleRelations . "','" .
            $lastRelations . "','" .
            $json . "','" .
            $date . "','" .
            $todaysDate . "','" .
            $entitieId . "','" .
            $userId . "','" .
            $state . "'";
        $sql = "insert into posts (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = 'select max(post_id) as id from posts';
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        $id = $row["id"];

        $insert = 'post_cat_post_id,post_cat_cat_id,post_cat_ent_id,post_cat_order';
        $countCategorys = count($categorys);
        for ($i = 0; $i < $countCategorys; $i++) {
            $values = "'" . $id . "','" . $categorys[$i] . "','" . $entitieId . "','" . $i . "'";
            $sql = "insert into posts_categorys (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        $insert = 'post_file_post_id,post_file_file_id,post_file_order';
        $countFiles = count($mediaFiles);

        for ($i = 0; $i < $countFiles; $i++) {
            $values = "'" . $id . "','" . $mediaFiles[$i] . "','" . $i . "'";
            $sql = "insert into posts_files (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        return $id;

    }

    //Authors
    public function authorDataId(int $authorId = 0)
    {
        $sql = "SELECT * FROM posts_authors WHERE post_au_id='" . $authorId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {

            $row = $this->fmt->querys->row($rs);
            $id = $row["post_au_id"];
            $return["id"] = $id;
            $return["name"] = $row["post_au_name"];
            $return["summary"] = $row["post_au_summary"];

            if ($row["post_au_img"] != "") {
                $img = [];
                $img["id"] = $row["post_au_img"];
                $pathUrl = $this->fmt->files->imgId($row["post_au_img"], "");
                $thumb = $this->fmt->files->thumb($pathUrl);
                $img["pathurl"] = $pathUrl;
                $img["thumb"] = $thumb;
                $img["name"] = $this->fmt->files->name($row["post_au_img"], "");
            } else {
                $img = 0;
            }

            $return["img"] = $img;
            $return["path"] = $row["post_au_path"];
            $return["entId"] = $row["post_au_ent_id"];
            $return["contact"] = $row["post_au_contact"];
            $return["json"] = $row["post_au_json"];
            $return["state"] = $row["post_au_state"];

            return $return;
        } else {
            return 0;
        }

    }

    public function getAuthorId($authorId = null)
    {
        //return $var;
        $sql = "SELECT * FROM posts_authors WHERE post_au_id='" . $authorId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["post_au_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["post_au_name"];
                $return[$i]["summary"] = $row["post_au_summary"];

                if ($row["post_au_img"] != "" && $row["post_au_img"] != "0" && $row["post_au_img"] != "null") {
                    $img = [];
                    $img["id"] = $row["post_au_img"];
                    $pathUrl = $this->fmt->files->imgId($row["post_au_img"], "");
                    $thumb = $this->fmt->files->thumb($pathUrl);
                    $img["pathurl"] = $pathUrl;
                    $img["thumb"] = $thumb;
                    $img["name"] = $this->fmt->files->name($row["post_au_img"], "");
                } else {
                    $img = 0;
                }

                $return[$i]["img"] = $img;
                $return[$i]["path"] = $row["post_au_path"];
                $return[$i]["entId"] = $row["post_au_ent_id"];
                $return[$i]["contact"] = $row["post_au_contact"];
                $return[$i]["json"] = $row["post_au_json"];
                $return[$i]["state"] = $row["post_au_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function getDataAuthorId(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $item = $var["vars"]["item"];

        $rtn["data"] = $this->authorDataId($item);
        $rtn["Error"] = 0;
        $rtn["status"] = 'success';
        $rtn["message"] = 'Datos cargados correctamente';

        return $rtn;

    }

    public function getAuthors(array $var = null)
    {
        $entitieId = $var["entitieId"];
        $sql = "SELECT * FROM posts_authors WHERE post_au_ent_id='" . $entitieId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["post_au_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["post_au_name"];
                $return[$i]["summary"] = $row["post_au_summary"];
                if ($row["post_au_img"] != "") {
                    $img = [];
                    $img["id"] = $row["post_au_img"];
                    $pathUrl = $this->fmt->files->imgId($row["post_au_img"], "");
                    $thumb = $this->fmt->files->thumb($pathUrl);
                    $img["pathurl"] = $pathUrl;
                    $img["thumb"] = $thumb;
                    $img["name"] = $this->fmt->files->name($row["post_au_img"], "");
                } else {
                    $img = 0;
                }
                $return[$i]["img"] = $img;
                $return[$i]["path"] = $row["post_au_path"];
                $return[$i]["entId"] = $row["post_au_ent_id"];
                $return[$i]["contact"] = $row["post_au_contact"];
                $return[$i]["state"] = $row["post_au_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function addAuthorPosts(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $inputs = $var["vars"]["inputs"];

        $replacements = array(
            'á' => 'a',
            'é' => 'e',
            'í' => 'i',
            'ó' => 'o',
            'ú' => 'u',
            'Á' => 'A',
            'É' => 'E',
            'Í' => 'I',
            'Ó' => 'O',
            'Ú' => 'U'
        );

        $sql = "SELECT * FROM posts_authors WHERE post_au_ent_id='" . $entitieId . "' AND LOWER(post_au_name) = LOWER('" . $inputs["inputName"] . "')";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["post_au_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["post_au_name"];
            }
            return $return;
        }

        $sql = "INSERT INTO posts_authors (post_au_ent_id, post_au_name, post_au_summary, post_au_contact, post_au_img, post_au_path, post_au_state) VALUES ('" . $entitieId . "','" . $inputs["inputName"] . "','" . $inputs["inputSummary"] . "','" . $inputs["inputContact"] . "','" . $inputs["inputImage"] . "','" . $inputs["inputPath"] . "','1')";
        $rs = $this->fmt->querys->consult($sql);

        $sql = "select max(post_au_id) as id from posts_authors";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $id = $row["id"];

        return $id;
    }

    public function updateAuthorPosts(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"] || '1';

        $inputs = $var["vars"]["inputs"];
        $id = $inputs["inputId"];
        $name = $inputs["inputName"];
        $summary = $inputs["inputSummary"];
        $contact = $inputs["inputContact"];
        $image = $inputs["inputImage"];
        $path = $inputs["inputPath"];

        $sql = "UPDATE posts_authors SET
                post_au_name = '" . $name . "',
                post_au_summary = '" . $summary . "',
                post_au_img = '" . $image . "',
                post_au_ent_id = '" . $entitieId . "',
                post_au_contact = '" . $contact . "',
                post_au_path = '" . $path . "'
                WHERE post_au_id = '" . $id . "' ";
        $this->fmt->querys->consult($sql);

        return 1;
    }

    public function getAuthorSearch(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $inputs = $var["vars"]["inputs"];
        $inputName = $inputs["inputName"];
        $rtn = [];

        if (empty($inputName)) {
            $rtn["num"] = 0;
            $rtn["items"] = 0;
            return $rtn;
        }

        //calcular cantidad de palabras en $inputName
        $numCaracters = strlen($inputName);
        if ($numCaracters < 2) {
            $rtn["num"] = 0;
            $rtn["items"] = 0;
            return $rtn;
        }

        $sql = "SELECT * FROM posts_authors WHERE post_au_ent_id='" . $entitieId . "' AND post_au_name LIKE '%" . $inputName . "%'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["post_au_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["post_au_name"];
                $return[$i]["summary"] = $row["post_au_summary"];
                $return[$i]["contact"] = $row["post_au_contact"];
                $return[$i]["state"] = $row["post_au_state"];
            }
            $rtn["num"] = $num;
            $rtn["items"] = $return;

        } else {
            $rtn["num"] = 0;
            $rtn["items"] = 0;
        }

        return $rtn;
    }

}