<?php
header('Content-Type: text/html; charset=utf-8');
class CONTENTS
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function dataId(int $id = null, $entitieId = 1, $state = "=1")
    {
        // cont_id,cont_title,cont_description,cont_tags,cont_pathurl,cont_img,cont_body,cont_author,cont_register_date,cont_notitle,cont_url,cont_target,cont_btn_title,cont_cls,cont_state
        $sql = "SELECT * FROM contents WHERE cont_id='" . $id . "' AND cont_state " . $state;
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["cont_id"];
            $return["id"] = $id;
            $return["title"] = $row["cont_title"];
            $return["summary"] = $row["cont_summary"];
            $return["pathurl"] = $row["cont_pathurl"];
            $return["tags"] = $row["cont_tags"];
            //$return["body"] = preg_replace("!([\b\t\n\r\f\"\\'])!", "\\\\\\1", $row["cont_body"]);
            $return["body"] = $row["cont_body"];
            $return["author"] = $row["cont_author"];
            $return["registerDate"] = $row["cont_register_date"];
            $return["notitle"] = $row["cont_notitle"];
            $return["url"] = $row["cont_url"];
            $return["icon"] = $row["cont_icon"];
            $return["target"] = $row["cont_target"];
            $return["cls"] = $row["cont_cls"];
            $return["json"] = $row["cont_json"];
            $return["state"] = $row["cont_state"];
            $return["btnTitle"] = $row["cont_btn_title"];
            if ($row["cont_img"]) {
                $img["id"] = $row["cont_img"];
                $img["pathurl"] = $this->fmt->files->imgId($row["cont_img"], "");
                $img["name"] = $this->fmt->files->name($row["cont_img"], "");
            } else {
                $img = 0;
            }

            $return["img"] = $img;
            $return["media"] = $this->relationFromFiles($id);
            $return["categorys"] = $this->fmt->categorys->relationFrom(
                array(
                    "from" => "contents_categorys",
                    "colId" => "cont_cat_cont_id",
                    "id" => $id,
                    "colCategory" => "cont_cat_cat_id",
                    "orderBy" => "cont_cat_order",
                    "actives" => 0
                )
            ); // all until not active

            $return["pubs"] = $this->fmt->pubs->relationFrom(
                array(
                    "from" => "contents_pubs",
                    "colId" => "cont_pub_cont_id",
                    "id" => $id,
                    "colPub" => "cont_pub_pub_id",
                    "orderBy" => "cont_pub_order ASC",
                    "actives" => 0
                )
            ); // all until not active

            return $return;
        } else {
            return 0;
        }

    }

    public function list(int $entId = null)
    {

        define("_VS", $this->fmt->options->version());

        // cont_id,cont_title,cont_description,cont_tags,cont_pathurl,cont_img,cont_body,cont_author,cont_register_date,cont_notitle,cont_url,cont_target,cont_btn_title,cont_cls,cont_state
        $sql = "SELECT * FROM contents WHERE cont_ent_id='" . $entId . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["cont_id"];
                $return["id"] = $id;
                $return["title"] = $row["cont_title"];
                $return["summary"] = $row["cont_summary"];
                $return["pathurl"] = $row["cont_pathurl"];
                $return["tags"] = $row["cont_tags"];
                $return["body"] = $row["cont_body"];
                $return["author"] = $row["cont_author"];
                $return["registerDate"] = $row["cont_register_date"];
                $return["notitle"] = $row["cont_notitle"];
                $return["url"] = $row["cont_url"];
                $return["target"] = $row["cont_target"];
                $return["cls"] = $row["cont_cls"];
                $return["json"] = $row["cont_json"];
                $return["state"] = $row["cont_state"];
                $return["btnTitle"] = $row["cont_btn_title"];
                if ($row["cont_img"]) {
                    $img["id"] = $row["cont_img"];
                    $img["pathurl"] = $this->fmt->files->imgId($row["cont_img"], "");
                    $img["name"] = $this->fmt->files->name($row["cont_img"], "");
                } else {
                    $img = 0;
                }

                $return["img"] = $img;
                $return["media"] = $this->relationFromFiles($id);
                $return["categorys"] = $this->fmt->categorys->relationFrom(
                    array(
                        "from" => "contents_categorys",
                        "colId" => "cont_cat_cont_id",
                        "id" => $id,
                        "colCategory" => "cont_cat_cat_id",
                        "orderBy" => "cont_cat_order"
                    )
                );
                $return["pubs"] = $this->fmt->pubs->relationFrom(
                    array(
                        "from" => "contents_pubs",
                        "colId" => "cont_pub_cont_id",
                        "id" => $id,
                        "colPub" => "cont_pub_pub_id",
                        "orderBy" => "cont_pub_order ASC",
                        "actives" => 0
                    )
                ); // all until not active
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function relationFromFiles($id = null)
    {
        $sql = "SELECT * FROM files, contents_files WHERE cont_file_file_id=file_id AND cont_file_cont_id='" . $id . "' AND file_state > 0 ORDER BY cont_file_order ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return[$i]["id"] = $row["cont_file_file_id"];
                $return[$i]["title"] = $row["cont_file_title"];
                $return[$i]["summary"] = $row["cont_file_summary"];
                $return[$i]["url"] = $row["cont_file_url"];
                $return[$i]["target"] = $row["cont_file_target"];
                $return[$i]["pathurl"] = $row["file_pathurl"];
                $return[$i]["btnTitle"] = $row["file_btn_title"];
                $return[$i]["order"] = $i;
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function loadContents(array $var = null)
    {
        //return $var;
        $sql = "SELECT * FROM contents ORDER BY cont_register_date DESC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["cont_id"];
                $return[$i]["id"] = $id;
                $return[$i]["title"] = $row["cont_title"];
                $return[$i]["pathurl"] = $row["cont_pathurl"];
                $return[$i]["description"] = $row["cont_description"];
                $return[$i]["tags"] = $row["cont_tags"];
                $return[$i]["img"] = $row["cont_img"];
                $return[$i]["body"] = $row["cont_body"];
                $return[$i]["author"] = $row["cont_author"];
                $return[$i]["registerDate"] = $row["cont_register_date"];
                $return[$i]["userId"] = $row["cont_user_id"];
                $return[$i]["notitle"] = $row["cont_notitle"];
                $return[$i]["url"] = $row["cont_url"];
                $return[$i]["btnTitle"] = $row["cont_btn_title"];
                $return[$i]["target"] = $row["cont_target"];
                $return[$i]["cls"] = $row["cont_cls"];
                $return[$i]["json"] = $row["cont_json"];
                $return[$i]["state"] = $row["cont_state"];
                $return[$i]["media"] = $this->relationFromFiles($id);
                $return[$i]["categorys"] = $this->fmt->categorys->relationFrom(
                    array(
                        "from" => "contents_categorys",
                        "colId" => "cont_cat_cont_id",
                        "id" => $id,
                        "colCategory" => "cont_cat_cat_id",
                        "orderBy" => "cont_cat_order"
                    )
                );
                $return[$i]["pubs"] = $this->fmt->pubs->relationFrom(
                    array(
                        "from" => "contents_pubs",
                        "colId" => "cont_pub_cont_id",
                        "id" => $id,
                        "colPub" => "cont_pub_pub_id",
                        "orderBy" => "cont_pub_order ASC",
                        "actives" => 0
                    )
                ); // all until not active

            }
            return $return;
        } else {
            return 0;
        }

    }

    public function loadDataContent(array $var = null)
    {
        //return $var;
        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        //return array($userId,$rolId,$entitieId,$vars,$vars["id"]);

        $data = $this->dataId($vars["id"]);

        return $data;
    }

    public function addNewContent(array $var = null)
    {
        //return $var;
        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];

        $title = $vars["inputTitle"];
        $pathurl = $vars["inputPathurl"];
        $summary = $vars["inputSummary"];
        $tags = $vars["inputTags"];
        $img = $vars["inputImg"];
        $icon = $vars["inputIcon"];
        $body = $vars["inputBody"];
        $author = $vars["inputAuthor"];
        $notitle = $vars["inputNotitle"];
        $url = $vars["inputUrl"];
        $btnTitle = $vars["inputBtnTitle"];
        $target = $vars["inputTarget"];
        $cls = $vars["inputCls"];
        $json = $vars["inputJson"];
        $state = $vars["inputState"];
        $categorys = $vars["inputCategorys"];
        $mediaFiles = $vars["inputMediaFiles"];

        $todaysDate = $this->fmt->modules->dateFormat();

        $insert = "cont_title,cont_pathurl,cont_summary,cont_tags,cont_img,cont_icon,cont_body,cont_author,cont_register_date,cont_user_id,cont_notitle,cont_url,cont_btn_title,cont_target,cont_cls,cont_json,cont_state";
        $values = "'" . $title . "','" .
            $pathurl . "','" .
            $summary . "','" .
            $tags . "','" .
            $img . "','" .
            $icon . "','" .
            $body . "','" .
            $author . "','" .
            $todaysDate . "','" .
            $userId . "','" .
            $notitle . "','" .
            $url . "','" .
            $btnTitle . "','" .
            $target . "','" .
            $cls . "','" .
            $json . "','" .
            $state . "'";

        $sql = "insert into contents (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);


        $sql = 'select max(cont_id) as id from contents';
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        $id = $row["id"];

        $insert = 'cont_cat_cont_id,cont_cat_cat_id,cont_cat_ent_id,cont_cat_order';
        $countCategorys = count($categorys);
        for ($i = 0; $i < $countCategorys; $i++) {
            $values = "'" . $id . "','" . $categorys[$i] . "','" . $entitieId . "','" . $i . "'";
            $sql = "insert into contents_categorys (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        $insert = 'cont_file_cont_id,cont_file_file_id,cont_file_order';
        $countFiles = count($mediaFiles);

        for ($i = 0; $i < $countFiles; $i++) {
            $values = "'" . $id . "','" . $mediaFiles[$i] . "','" . $i . "'";
            $sql = "insert into contents_files (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        return $id;
    }

    public function updateContent(array $var = null)
    {
        //return $var;
        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        $id = $vars["inputId"];
        $categorys = $vars["inputCategorys"];
        $mediaFiles = $vars["inputMedia"];
        $pubs = $vars["inputPubs"];

        $todaysDate = $this->fmt->modules->dateFormat();

        $sql = "UPDATE contents SET
                cont_title='" . $vars["inputTitle"] . "',
                cont_pathurl='" . $vars["inputPathurl"] . "',
                cont_summary='" . $vars["inputSummary"] . "',
                cont_tags='" . $vars["inputTags"] . "',
                cont_img='" . $vars["inputImg"] . "',
                cont_icon='" . $vars["inputIcon"] . "',
                cont_body='" . $vars["inputBody"] . "',
                cont_author='" . $this->fmt->emptyReturn($vars["inputAuthor"], "") . "',
                cont_register_date='" . $todaysDate . "',
                cont_user_id='" . $userId . "',
                cont_notitle='" . $vars["inputNotitle"] . "',
                cont_url='" . $vars["inputUrl"] . "',
                cont_btn_title='" . $vars["inputBtnTitle"] . "',
                cont_target='" . $vars["inputTarget"] . "',
                cont_cls='" . $vars["inputClass"] . "',
                cont_json='" . $vars["inputJson"] . "',
                cont_state='" . $vars["inputState"] . "'
                WHERE cont_id = '" . $id . "' ";
        $this->fmt->querys->consult($sql);

        $this->fmt->modules->deleteRelation(array("from" => "contents_categorys", "column" => "cont_cat_cont_id", "item" => $id));

        $insert = 'cont_cat_cont_id,cont_cat_cat_id,cont_cat_ent_id,cont_cat_order';
        $countCategorys = count($categorys);
        for ($i = 0; $i < $countCategorys; $i++) {
            $values = "'" . $id . "','" . $categorys[$i] . "','" . $entitieId . "','" . $i . "'";
            $sql = "insert into contents_categorys (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        $this->fmt->modules->deleteRelation(array("from" => "contents_pubs", "column" => "cont_pub_cont_id", "item" => $id));

        $insert = 'cont_pub_cont_id,cont_pub_pub_id,cont_pub_order';
        $countPubs = count($pubs);
        for ($i = 0; $i < $countPubs; $i++) {
            $values = "'" . $id . "','" . $pubs[$i] . "','" . $i . "'";
            $sql = "insert into contents_pubs (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);
        }

        $this->fmt->modules->deleteRelation(array("from" => "contents_files", "column" => "cont_file_cont_id", "item" => $id));

        $insert = 'cont_file_cont_id,cont_file_file_id,cont_file_order';
        $countFiles = count($mediaFiles);

        for ($i = 0; $i < $countFiles; $i++) {

            $numCount = $i + 1;
            $values = "'" . $id . "','" . $mediaFiles[$i] . "','" . $numCount . "'";
            $sql = "insert into contents_files (" . $insert . ") values (" . $values . ")";
            $this->fmt->querys->consult($sql, __METHOD__);

        }

        return 1;
    }

    public function deleteContents(array $var = null)
    {
        //return $var;
        $ids = $var["vars"]["ids"];
        $auxIds = count($ids);
        $count = 0;

        for ($i = 0; $i < $auxIds; $i++) {
            $id = $ids[$i];
            if (($id != "") && ($id != null) && ($id != 0)) {
                $this->fmt->modules->deleteRelation(array("from" => "contents_categorys", "column" => "cont_cat_cont_id", "item" => $id));
                $this->fmt->modules->deleteRelation(array("from" => "contents_files", "column" => "cont_file_cont_id", "item" => $id));
                $state = $this->deleteId($id);
                if ($state == 0) {
                    $count++;
                }
            }
        }
        if ($count == 0) {
            return 1;
        } else {
            return 0;
        }

    }

    public function deleteId($id = null)
    {

        if (!empty($id) && is_numeric($id) && $id != 0 && $id != null) {
            $sql = "DELETE FROM contents WHERE cont_id = '" . $id . "'";
            $this->fmt->querys->consult($sql);
            $up_sqr6 = "ALTER TABLE contents AUTO_INCREMENT=1";
            $this->fmt->querys->consult($up_sqr6, __METHOD__);

            return 1;
        }

        return 0;
    }

    public function checkRelationFile(int $contentId = null, int $fileId = null)
    {
        //return $var;
        $sql = "SELECT * FROM contents_files WHERE cont_file_cont_id = '" . $contentId . "' AND cont_file_file_id = '" . $fileId . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function deleteItemsMedia(array $var = null)
    {
        //return $var;
        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        $items = $vars["items"];
        $contentId = $vars["contentId"];

        $items = explode(",", $items);

        $count = count($items);
        for ($i = 0; $i < $count; $i++) {
            $id = $items[$i];
            $this->fmt->files->deteleFileId($id);
            $sql = "DELETE FROM contents_files WHERE cont_file_file_id = '" . $id . "' AND cont_file_cont_id = '" . $contentId . "'";
            $this->fmt->querys->consult($sql);
            $up_sqr6 = "ALTER TABLE contents_files AUTO_INCREMENT=1";
            $this->fmt->querys->consult($up_sqr6, __METHOD__);
        }
        return 1;
    }

    public function loadDataItemMedia(array $var = null)
    {
        //return $var;
        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        $id = $vars["inputId"];
        $item = $vars["item"];

        $sql = "SELECT * FROM contents_files WHERE cont_file_cont_id = '" . $id . "' and cont_file_file_id = '" . $item . "'";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return["title"] = $row["cont_file_title"];
                $return["summary"] = $row["cont_file_summary"];
                $return["url"] = $row["cont_file_url"];
                $return["btnTitle"] = $row["cont_file_btn_title"];
                $return["icon"] = $row["cont_file_icon"];
                $return["target"] = $row["cont_file_target"];
                $return["order"] = $row["cont_file_order"];
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function updateItemMedia(array $var = null)
    {
        //return $var;
        $userId = $var["user"]["userId"];
        $rolId = $var["user"]["rolId"];
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        $idContent = $vars["inputIdContent"];
        $idMedia = $vars["inputIdMedia"];

        $sql = "UPDATE contents_files SET
                cont_file_title ='" . $vars["inputTitle"] . "',
                cont_file_summary ='" . $vars["inputSummary"] . "',
                cont_file_url ='" . $vars["inputUrl"] . "',
                cont_file_btn_title ='" . $vars["inputBtnTitle"] . "',
                cont_file_icon ='" . $vars["inputIcon"] . "',
                cont_file_target ='" . $vars["inputTarget"] . "'
               WHERE cont_file_cont_id = '" . $idContent . "' and cont_file_file_id = '" . $idMedia . "'";
        $this->fmt->querys->consult($sql);

        return 1;
    }

    public function relationCategory(array $var = null)
    {
        //return $var;
        $catId = $var["catId"];
        $limit = $this->fmt->emptyReturn($var["limit"], "");
        $order = $this->fmt->emptyReturn($var["order"], "cont_cat_order ASC");

        if ($limit != "") {
            $limit = "LIMIT " . $limit;
        }

        $sql = "SELECT * FROM contents_categorys, contents WHERE  cont_cat_cont_id = cont_id AND cont_cat_cat_id = '" . $catId . "' AND cont_state=1 ORDER BY " . $order . " " . $limit;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $return = array();
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["cont_cat_cont_id"];
                $array = $this->dataId($id);
                $return[$i]['id'] = $id;
                $return[$i]['title'] = $array['title'];
                $return[$i]['pathurl'] = $array['pathurl'];
                $return[$i]['summary'] = $array['summary'];
                $return[$i]['img'] = $array['img'];
                $return[$i]["media"] = $this->relationFromFiles($id);
                $return[$i]['body'] = $array['body'];
                $return[$i]['state'] = $array['state'];
                $return[$i]['url'] = $array['url'];
                $return[$i]['icon'] = $array['icon'];
                $return[$i]['target'] = $array['target'];
                $return[$i]['btnTitle'] = $array['btnTitle'];
                $return[$i]['cls'] = $array['cls'];
                $return[$i]['json'] = $array['json'];
            }
            return $return;
        } else {
            return 0;
        }


    }

    public function relationCategorys(array $var = null)
    {
        //return $var;  

        $cats = $var["cats"];
        $entitieId = $this->fmt->emptyReturn($var["entitieId"], 1);

        $arrayCats = explode(",", $cats);
        $contCats = count($arrayCats);

        $rtn = [];
        $rtn["cat0"] = $this->fmt->categorys->dataIdBasic($arrayCats[0], $entitieId, ">=0");
        $limit = $this->fmt->emptyReturn($var["limit"], "");
        $order = $this->fmt->emptyReturn($var["order"], "ASC");

        if ($limit != "") {
            $limit = "LIMIT " . $limit;
        }


        if ($contCats > 1) {
            $sql = "SELECT cont_cat_cont_id, GROUP_CONCAT(cont_cat_cat_id) AS cats, cont_cat_order FROM contents_categorys WHERE cont_cat_cat_id in (" . $cats . ") GROUP BY cont_cat_cont_id HAVING COUNT(*) > 1  " . $limit;
        } else {
            $sql = "SELECT cont_cat_cont_id, cont_cat_cat_id, cont_cat_order FROM contents_categorys WHERE cont_cat_cat_id ='" . $cats . "'      ORDER BY cont_cat_order " . $order . " " . $limit;
        }

        $sql;
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $this->dataId($row["cont_cat_cont_id"]);
                if ($id["state"] >= 1) {
                    $rtn["cont"][$i] = $id;
                }
            }

        } else {
            $rtn = 0;
        }

        return $rtn;


    }

    public function relationPub(int $catId = null, int $pubId = null)
    {
        //return $catId."-".$pubId;
        $sql = "SELECT cont_id FROM contents,contents_categorys,contents_pubs WHERE cont_cat_cont_id=cont_id AND cont_pub_cont_id=cont_id AND cont_cat_cat_id='" . $catId . "' AND cont_pub_pub_id='" . $pubId . "' ORDER BY cont_pub_order ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["cont_id"];
                $array = $this->dataId($id);
                $return[$i]['id'] = $id;
                $return[$i]['title'] = $array['title'];
                $return[$i]['pathurl'] = $array['pathurl'];
                $return[$i]['summary'] = $array['summary'];
                $return[$i]['img'] = $array['img'];
                $return[$i]['body'] = $array['body'];
                $return[$i]['state'] = $array['state'];
                $return[$i]['icon'] = $array['icon'];
                $return[$i]['target'] = $array['target'];
                $return[$i]['url'] = $array['url'];
                $return[$i]['cls'] = $array['cls'];
                $return[$i]['btnTitle'] = $array['btnTitle'];
                $return[$i]['media'] = $array['media'];
                $return[$i]['json'] = $array['json'];
            }
            return $return;
        } else {
            return 0;
        }

    }

    public function getRoles(array $var = null){
        //return $var; 
        $sql = "SELECT rol_id, rol_name FROM roles ORDER BY rol_name ASC"; // Ajusta la consulta según la BD
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["rol_id"];
                $return[$i]['id'] = $id;
                $return[$i]['name'] = $row["rol_name"];
            }
            return [
                ['id' => 1, 'name' => 'Admin'],
                ['id' => 2, 'name' => 'User']
            ];

        } else {
            return 0;
        }
    }


}