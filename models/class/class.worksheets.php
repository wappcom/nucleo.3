<?php
header('Content-Type: text/html; charset=utf-8');
class WORKSHEETS
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function loadHeader($cat = 1, $worksheet = 1)
    {

        $str = '';
        $metaHtml = '';
        #Luego revisar cual header usar
        $header = file_get_contents(_PATH_HOST . "sites/default/views/header.html");
        $header = str_replace("{{title}}", $this->fmt->options->getValue("site_title"), $header);
        $header = str_replace("{{urlFavicon}}", _PATH_WEB . $this->fmt->options->getValue("site_favicon"), $header);
        $header = str_replace("{{_PATH_MODULE}}", _PATH_WEB . _PATH_DASHBOARD, $header);
        $header = str_replace("{{_PATH_WEB_NUCLEO}}", _PATH_WEB_NUCLEO, $header);
        $header = str_replace("{{_ICONS_LINK}}", _ICONS_LINK, $header);
        $header = str_replace("{{_PATH_WEB}}", _PATH_WEB, $header);
        $header = str_replace("{{_PATH_PAGE}}", _PATH_PAGE, $header);
        $header = str_replace("{{_PATH_FILES}}", _PATH_FILES, $header);
        $header = str_replace("{{_PATH_FAVICON_APP}}", $this->fmt->options->getValue("site_favicon"), $header);
        $header = str_replace("{{_PATH_BRAND}}", $this->fmt->options->getValue("site_img"), $header);
        $header = str_replace("{{_VS}}", "v" . $this->fmt->options->version(), $header);
        // $header = str_replace("{{_VS}}", "v" . $this->fmt->options->versionPlus(), $header);
        $header = str_replace("{{_PATH_DASHBOARD}}", "sites/default/", $header);
        $header = str_replace("{{_BEARER_TOKEN}}", $this->fmt->options->getValue("bearer_token"), $header);
        $header = str_replace("{{_COIN_DEFAULT}}", $this->fmt->options->getValue("coin", "yes"), $header);
        $header = str_replace("{{_CLIENT_ID}}", $this->fmt->options->getValue("client_id"), $header);
        $header = str_replace("{{_CLIENT_SECRET}}", $this->fmt->options->getValue("client_secret"), $header);
        $header = str_replace("{{_ID_ENTITIE}}", $this->fmt->options->getValue("entitie_id_default"), $header);
        $header = str_replace("{{_LOCALE}}", $this->fmt->options->getValue("locale"), $header);
        $header = str_replace("{{_TIMEZONE}}", $this->fmt->options->getValue("timezone"), $header);
        $header = str_replace("{{_STATE_CONNECT}}", _STATE_CONNECT, $header);
        $header = str_replace("{{_PAGE_CAT}}", $this->fmt->categorys->getPathFromCatId($cat), $header);
        /*$options = array(
                    'http'=>array(
                    'method'=>"POST",
                    'content'=>"route="._PATH_HOST));
        $contexto = stream_context_create($options); 
        $metaHtml  = file_get_contents(_PATH_WEB . $this->fmt->options->getValue("site-meta"),false,$contexto);*/
        $metaHtml = '';
        $pathMeta = _PATH_HOST . $this->fmt->options->getValue("site-meta");
        if (file_exists($pathMeta)) {
            ob_start();
            include $pathMeta; // Incluir el archivo PHP
            $metaHtml = ob_get_clean();
        }
        $header = str_replace("{{_META}}", $metaHtml, $header);
        $str .= $header;
        return $str;
    }

    public function htmlReactive(string $body = null)
    {
        //return $var;

        $str = '';
        $str .= $this->loadHeader();
        $str .= $body;
        $str .= $this->loadFooter();

        return $str;
    }

    public function loadBody($catId, $worksheet)
    {
        $str = '';
        //$body = file_get_contents(_PATH_HOST . "sites/default/views/body.html");
        //$str .= $body;
        $str .= "\n" . "			<!--  Inicio Cuerpo  -->" . "\n";
        //return $str; exit(0);
        $sql = "SELECT DISTINCT ws_id,ws_class 
                FROM worksheets 
                WHERE ws_state=1 AND ws_id = '" . $worksheet . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $wsId = $row["ws_id"];
                $wsClass = $row["ws_class"];
                $str .= '<div class="ws ws-' . $wsId . ' ' . $wsClass . '">' . "\n";

                $sqlBlock = "SELECT block_id, block_name, block_class, block_order, block_parent_id, block_state  FROM worksheets_blocks,blocks WHERE ws_block_ws_id='" . $wsId . "' AND ws_block_block_id=block_id AND  block_state > 0 ORDER BY ws_block_order  ASC";
                $rsBlock = $this->fmt->querys->consult($sqlBlock);
                $numBlock = $this->fmt->querys->num($rsBlock);
                if ($numBlock > 0) {
                    for ($j = 0; $j < $numBlock; $j++) {
                        $rowBlock = $this->fmt->querys->row($rsBlock);
                        $blockId = $rowBlock["block_id"];
                        $blockName = $rowBlock["block_name"];
                        $blockClass = $rowBlock["block_class"];
                        $str .= '<div class="ws-block ws-block-' . $blockId . ' ' . $blockClass . '" name="' . $blockName . '" id="ws-block-' . $blockId . '">' . "\n";
                        if ($this->hasPublications($catId, $wsId, $blockId)) {
                            $str .= " " . $this->getPublications($catId, $wsId, $blockId);
                        }

                        if ($this->hasBlocks($blockId, $wsId, $catId)) {
                            $str .= " " . $this->getBlocks($blockId, $wsId, $catId);
                            //$str .= " hay contenido:" . $blockId;
                        }
                        $str .= '</div>' . "\n";
                    }
                } else {
                    return 0;
                }
                $this->fmt->querys->leave($rsBlock);
                $str .= '</div>';
            }
        } else {
            return 0;
        }

        return $str;
    }

    public function loadFooter($cat = 1, $worksheet = 1)
    {
        $str = '';
        $footer = file_get_contents(_PATH_HOST . "sites/default/views/footer.html");
        $str .= $footer;
        return $str;
    }

    public function hasBlocks($blockId = 0, $worksheet = 0, $cat = 0)
    {
        $sql = "SELECT DISTINCT block_id
                FROM blocks
                WHERE block_parent_id='" . $blockId . "' AND block_state > '0'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getBlocks($blockId = "NULL", $wsId = "NULL", $catId = "NULL")
    {
        $str = '';
        $sqlBlock = "SELECT block_id, block_name, block_class, block_order, block_parent_id, block_state  FROM blocks WHERE block_parent_id='" . $blockId . "' AND block_state > 0 ORDER BY block_order ASC";
        $rsBlock = $this->fmt->querys->consult($sqlBlock);
        $numBlock = $this->fmt->querys->num($rsBlock);
        if ($numBlock > 0) {
            for ($j = 0; $j < $numBlock; $j++) {
                $rowBlock = $this->fmt->querys->row($rsBlock);
                $blockIdHijo = $rowBlock["block_id"];
                $blockName = $rowBlock["block_name"];
                $blockClass = $rowBlock["block_class"];
                $str .= '<div class="ws-block ws-block-' . $blockIdHijo . ' ' . $blockClass . '" name="' . $blockName . '" id="ws-block-' . $blockIdHijo . '">' . "\n";
                if ($this->hasPublications($catId, $wsId, $blockIdHijo)) {
                    $str .= " " . $this->getPublications($catId, $wsId, $blockIdHijo);
                }

                if ($this->hasBlocks($blockIdHijo, $wsId, $catId)) {
                    $str .= " " . $this->getBlocks($blockIdHijo, $wsId, $catId);
                    //$str .= " hay contenido n2:" . $blockId;
                }
                $str .= '</div>' . "\n";
            }
            return $str;
        } else {
            return "";
        }
    }

    public function hasPublications($catId = "NULL", $wsId = "NULL", $blockId = "NULL")
    {
        if ($blockId == "NULL") {
            $sqlBlock = "";
        } else {
            $sqlBlock = " AND pub_rel_block_id='" . $blockId . "' ";
        }
        $sql = "SELECT pub_id FROM publications_relations,publications WHERE pub_rel_pub_id=pub_id AND pub_rel_state='1' AND pub_rel_ws_id = '" . $wsId . "' " . $sqlBlock . "  AND pub_rel_cat_id='" . $catId . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getPublications($catId = "NULL", $wsId = "NULL", $blockId = "NULL")
    {
        $returnHtml = '';
        //[fix] 
        $arrayPubCatId = $this->fmt->categorys->dataIdReturn($catId);

        if ($blockId == "NULL") {
            $sqlBlock = "";
        } else {
            $sqlBlock = " AND pub_rel_block_id='" . $blockId . "' ";
        }
        $sql = "SELECT DISTINCT pub_id AS id,
                                pub_name AS name,
                                pub_title AS title,
                                pub_summary AS summary,
                                pub_description AS description,
                                pub_path AS path,
                                pub_path_ui AS pathUI,
                                pub_icon AS icon,
                                pub_type AS type,
                                pub_class AS cls,
                                pub_attr_id AS attrId,
                                pub_attr AS attr,
                                pub_count AS count,
                                pub_html AS html,
                                pub_json AS json,
                                pub_rel_order
                                FROM publications_relations,publications 
                                WHERE pub_rel_pub_id=pub_id AND pub_rel_state='1' AND pub_rel_ws_id = '" . $wsId . "' " . $sqlBlock . "  AND pub_rel_cat_id='" . $catId . "' 
                                ORDER BY pub_rel_order ASC";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($inx = 0; $inx < $num; $inx++) {
                $arrayPub = $this->fmt->querys->row($rs);
                $pubId = $arrayPub["id"];
                $pubPath = $arrayPub["path"];
                $pubType = $arrayPub["type"];
                $pubName = $arrayPub["name"];
                $pubAttrId = $arrayPub["attrId"];
                $pubAttr = $arrayPub["attr"];
                $pubCount = $arrayPub["count"];
                $pubTitle = $arrayPub["title"];
                $pubSummary = $arrayPub["summary"];
                $pubCls = $arrayPub["cls"];
                $pubPathUI = $arrayPub["pathUI"];
                $pubJson = $arrayPub["json"];
                $pubHtml = $arrayPub["html"];
                $root = _PATH_HOST;


                if ($pubType == "host") {
                    $root = _PATH_WEB;
                    $rootAux = _PATH_HOST;
                }
                if ($pubType == "nucleo") {
                    $root = _PATH_WEB_NUCLEO;
                    $rootAux = _PATH_NUCLEO;
                }

                $dataPub['catId'] = $catId;
                $dataPub['dataCatId'] = $arrayPubCatId;
                $dataPub['pub'] = $arrayPub;
                $dataPub['root_get'] = _ROOT_GET;
                $arrayUrl = serialize($dataPub);
                $arrayUrl = urlencode($arrayUrl);
                //$data = http_build_query($dataPub);


                //$returnHtml .= $root . $pubPath;
                if (file_exists($rootAux . $pubPath)) {
                    //if ($this->fmt->auth->urlExists($root . $pubPath)) {
                    if (!empty(_ROOT_GET)) {
                        $aux = "?root_get=" . _ROOT_GET;
                    } else {
                        $aux = "";
                    }

                    $_POST['data'] = serialize($dataPub);

                    $url = $root . $pubPath;
                    $urlHost = "";
                    $urlHost = $rootAux . $pubPath;
                    $urlHtml = '';

                    ob_start();
                    include $urlHost; // Incluir el archivo PHP
                    $urlHtml = ob_get_clean(); // Obtener el contenido del buffer

                    /* $options = array (
                            'http' => array (
                                'method' => 'POST',
                                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                                'content' => 'data='.$arrayUrl,
                            ),
                            'ssl' => array(
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                            )
                            ); */

                    //$context = stream_context_create($options);
                    //$returnHtml .= file_get_contents($url,false, $context);
                    $returnHtml .= $urlHtml;
                    ob_end_flush();

                } else {
                    $returnHtml .= 'Error Worksheets. No File , $putType:' . $pubType . ', root: ' . $root . $pubPath;
                }
            }
            return $returnHtml . "\n";
        } else {
            return 0;
        }
    }

    public function getWorksheets(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];

        $sql = "SELECT * FROM worksheets WHERE ws_ent_id='" . $entitieId . "' ORDER BY ws_level ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["ws_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["ws_name"];
                $return[$i]["level"] = $row["ws_level"];
                $return[$i]["cls"] = $row["ws_class"];
                $return[$i]["state"] = $row["ws_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function getWorksheetsId(array $var = null)
    {
        //return $var;

        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        $worksheetId = $vars["wsId"];
        $catId = $vars["catId"];

        $sql = "SELECT DISTINCT block_id, block_name, block_class, block_order, block_parent_id, block_state, ws_block_order  FROM worksheets_blocks,blocks,worksheets WHERE ws_block_ws_id='" . $worksheetId . "' AND ws_block_block_id=block_id AND  block_state > 0 AND ws_ent_id='" . $entitieId . "' AND  block_ent_id='" . $entitieId . "' ORDER BY ws_block_order  ASC";

        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return[$i]["id"] = $row["block_id"];
                $return[$i]["name"] = $row["block_name"];
                $return[$i]["cls"] = $row["block_class"];
                $return[$i]["order"] = $row["block_order"];
                $return[$i]["parentId"] = $row["block_parent_id"];
                $return[$i]["state"] = $row["block_state"];
                $return[$i]["type"] = "block";
                $return[$i]["level"] = 1;
                $childrenNum = $this->hasBlocks($row["block_id"]);
                $pubNum = $this->hasPublications($catId, $worksheetId, $row["block_id"]);
                $return[$i]["blockChildrenNum"] = $childrenNum;
                $return[$i]["pubChildrenNum"] = $pubNum;

                if ($pubNum > 0) {
                    $return[$i]["pubChildren"] = $this->getPubElem(array("catId" => $catId, "blockId" => $row["block_id"], "entitieId" => $entitieId, "level" => 1, "wsId" => $worksheetId));
                }

                if ($childrenNum > 0) {
                    $return[$i]["blockChildren"] = $this->getBlockElem(array("blockId" => $row["block_id"], "entitieId" => $entitieId, "catId" => $catId, "level" => 1, "wsId" => $worksheetId));
                }

            }
        } else {
            $return = 0;
        }

        return $return;
    }

    public function getBlockElem(array $var = null)
    {
        //return $var;

        $entitieId = $var["entitieId"];
        $blockId = $var["blockId"];
        $catId = $var["catId"];
        $worksheetId = $var["wsId"];
        $level = intval($var["level"]) + 1;

        $sql = "SELECT block_id, block_name, block_class, block_order, block_parent_id, block_state  FROM blocks WHERE block_parent_id='" . $blockId . "' AND block_ent_id='" . $entitieId . "' AND block_state > 0 ORDER BY block_order ASC";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);

        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $return[$i]["id"] = $row["block_id"];
                $return[$i]["name"] = $row["block_name"];
                $return[$i]["cls"] = $row["block_class"];
                $return[$i]["order"] = $row["block_order"];
                $return[$i]["parentId"] = $row["block_parent_id"];
                $return[$i]["state"] = $row["block_state"];
                $return[$i]["type"] = "block";
                $return[$i]["level"] = $level;
                $childrenNum = $this->hasBlocks($row["block_id"]);
                $pubNum = $this->hasPublications($catId, $worksheetId, $row["block_id"]);
                $return[$i]["blockChildrenNum"] = $childrenNum;
                $return[$i]["pubChildrenNum"] = $pubNum;

                if ($pubNum > 0) {
                    $return[$i]["pubChildren"] = $this->getPubElem(array("catId" => $catId, "blockId" => $row["block_id"], "entitieId" => $entitieId, "level" => $level, "wsId" => $worksheetId));
                }

                if ($childrenNum > 0) {
                    $return[$i]["blockChildren"] = $this->getBlockElem(array("blockId" => $row["block_id"], "entitieId" => $entitieId, "level" => $level, "wsId" => $worksheetId));
                }
            }
        }

        return $return;
    }

    public function getPubElem(array $var = null)
    {
        //return $var;

        $entitieId = $var["entitieId"];
        $blockId = $var["blockId"];
        $catId = $var["catId"];
        $wsId = $var["wsId"];
        $level = intval($var["level"]) + 1;

        $returnHtml = '';

        if ($blockId == "NULL") {
            $sqlBlock = "";
        } else {
            $sqlBlock = " AND pub_rel_block_id='" . $blockId . "' ";
        }
        $sql = "SELECT DISTINCT pub_id AS id,
                                pub_name AS name,
                                pub_title AS title,
                                pub_description AS description,
                                pub_path AS path,
                                pub_path_ui AS pathUI,
                                pub_icon AS icon,
                                pub_type AS type,
                                pub_class AS cls,
                                pub_attr_id AS attrId,
                                pub_json AS json,
                                pub_html AS html,
                                pub_rel_state AS state,
                                pub_rel_order
                                FROM publications_relations,publications 
                                WHERE pub_rel_pub_id=pub_id  AND pub_rel_ws_id = '" . $wsId . "' " . $sqlBlock . "  AND pub_rel_cat_id='" . $catId . "' 
                                ORDER BY pub_rel_order ASC";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($inx = 0; $inx < $num; $inx++) {
                $row = $this->fmt->querys->row($rs);
                //$arrayPubCatId = $this->fmt->categorys->dataIdReturn($catId);
                $return[$inx]["id"] = $row["id"];
                $return[$inx]["name"] = $row["name"];
                $return[$inx]["state"] = $row["state"];
                $return[$inx]["type"] = "publication";
                $return[$inx]["data"] = $row;
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function updateWorksheet(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        $wsId = $vars["ws"];
        $catId = $vars["cat"];
        $pubs = $vars["pubs"];
        $countPubs = count($pubs);

        $sql = "DELETE FROM publications_relations WHERE  pub_rel_ws_id='" . $wsId . "' AND pub_rel_cat_id='" . $catId . "'";
        $this->fmt->querys->consult($sql);
        $up_sqr6 = "ALTER TABLE publications_relations AUTO_INCREMENT=1";
        $this->fmt->querys->consult($up_sqr6, __METHOD__);


        for ($i = 0; $i < $countPubs; $i++) {
            $pubId = $pubs[$i]["pub"];
            $blockId = $pubs[$i]["blockId"];
            $order = $pubs[$i]["order"];
            $state = $pubs[$i]["state"];

            $sql = "INSERT INTO publications_relations (pub_rel_cat_id,pub_rel_ws_id,pub_rel_block_id,pub_rel_pub_id,pub_rel_state,pub_rel_order) VALUES ('" . $catId . "','" . $wsId . "','" . $blockId . "','" . $pubId . "','" . $state . "','" . $order . "')";
            $this->fmt->querys->consult($sql);
        }

        return 1;

    }

    public function savePattern(array $var = null)
    {
        //return $var;
        $entitieId = $var["entitieId"];
        $vars = $var["vars"];
        $inputName = $vars["inputName"];
        $inputDescription = $vars["inputDescription"];
        $pubs = json_encode($vars["pubs"]);

        $insert = "pub_pat_name,pub_pat_description,pub_pat_json,pub_pat_ent_id,pub_pat_order,pub_pat_state";
        $values = "'" . $inputName . "','" . $inputDescription . "','" . $pubs . "','" . $entitieId . "','1','1'";
        $sql = "insert into publications_pattern (" . $insert . ") values (" . $values . ")";
        $this->fmt->querys->consult($sql, __METHOD__);

        $sql = "select max(pub_pat_id) as id from publications_pattern";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);

        return $row["id"];
    }

    public function getPatterns(array $var = null)
    {
        //return $var;

        $entitieId = $var["entitieId"];

        $sql = "SELECT * FROM publications_pattern WHERE pub_pat_ent_id='" . $entitieId . "' ORDER BY pub_pat_order ASC";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->querys->row($rs);
                $id = $row["pub_pat_id"];
                $return[$i]["id"] = $id;
                $return[$i]["name"] = $row["pub_pat_name"];
                $return[$i]["description"] = $row["pub_pat_description"];
                $return[$i]["json"] = $row["pub_pat_json"];
                $return[$i]["order"] = $row["pub_pat_order"];
                $return[$i]["state"] = $row["pub_pat_state"];
            }
            return $return;
        } else {
            return 0;
        }
    }

    public function deletePattern(array $var = null)
    {
        //return $var;
        $id = $var["vars"]["patternId"];

        $sql = "DELETE FROM publications_pattern WHERE pub_pat_id='" . $id . "'";
        $this->fmt->querys->consult($sql);
        $up_sqr6 = "ALTER TABLE publications_pattern AUTO_INCREMENT=1";
        $this->fmt->querys->consult($up_sqr6, __METHOD__);

        return 1;
    }

}