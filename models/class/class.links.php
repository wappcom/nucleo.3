<?php
header('Content-Type: text/html; charset=utf-8');
class LINKS
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function dataId(Int $id = 0){
        // lnk_id,lnk_title,lnk_description,lnk_tags,lnk_pathurl,lnk_img,lnk_body,lnk_author,lnk_register_date,lnk_notitle,lnk_url,lnk_target,lnk_btn_title,lnk_cls,lnk_state
        $sql = "SELECT * FROM links WHERE lnk_id='" . $id . "'";
        $rs = $this->fmt->querys->consult($sql);
        $num = $this->fmt->querys->num($rs);
        if ($num > 0) {
            $row = $this->fmt->querys->row($rs);
            $id = $row["lnk_id"];
            $return["id"] = $id;
            $return["title"] = $row["lnk_title"];
            $return["description"] = $row["lnk_description"];
            $return["tags"] = $row["lnk_tags"];
            $return["url"] = $row["lnk_url"];
            $return["target"] = $row["lnk_target"];
            $return["cls"] = $row["lnk_cls"];
            $return["attr"] = $row["lnk_attr"];
            $return["alt"] = $row["lnk_alt"];
            $return["json"] = $row["lnk_json"];
            $return["icon"] = $row["lnk_icon"];

            $img = [];
            if ($row["lnk_img"]) {
                $img["id"] = $row["lnk_img"];
                $img["pathurl"] = $this->fmt->files->imgId($row["lnk_img"], "");
            } else {
                $img = 0;
            }

            $return["categorys"] = $this->fmt->categorys->relationFrom(array("from" => "links_categorys",
                "colId" => "lnk_cat_lnk_id",
                "colCategory" => "lnk_cat_cat_id",
                "id" => $id,
                "orderBy" => "lnk_cat_order ASC"
            ));

            $return["pubs"] = $this->fmt->pubs->relationFrom(array(
                "from" => "links_pubs",
                "colId" => "lnk_pub_lnk_id",
                "id" => $id,
                "colPub" => "lnk_pub_pub_id",
                "orderBy" => "lnk_pub_order ASC",
                "actives" => 0
            )); // all until not active

            $return["img"] = $img;
            $return["state"] = $row["lnk_state"];
            
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave();
    }

    public function getDataId(array $var = null){
        //return $var;
        $id = $var["vars"]["id"];
        return $this->dataId($id);
    }

    public function set($str, $arrayPub){
    }

    public function getLinksRelatedPubs(Int $pubId = 0){
        //return $var;
        $sql = "SELECT * FROM links, links_pubs WHERE lnk_pub_pub_id='" . $pubId . "' AND lnk_pub_lnk_id=lnk_id";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->querys->row($rs);
                $id = $row["lnk_id"];
                $return[$i] = $this->dataId($id);
            }
            return $return;
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs);
    }

    public function add(array $var = null){
        //return $var;
        $entitieId = $var["entitieId"];
        $inputs = $var["vars"]["inputs"];
        $inputTitle = $inputs["inputTitle"];
        $inputDescription = $inputs["inputDescription"];
        $inputTags = $inputs["inputTags"];
        $inputUrl = $inputs["inputUrl"];
        $inputTarget = $inputs["inputTarget"] ? $inputs["inputTarget"] : "_self";
        $inputIcon = $inputs["inputIcon"];
        $inputAlt = $inputs["inputAlt"];
        $inputClass = $inputs["inputClass"];
        $inputAttr = $inputs["inputAttr"];
        $inputImage = $inputs["inputImage"];
        $inputJson = $inputs["inputJson"];
        $inputCategorys = $inputs["inputCategorys"];
        $count = count($inputCategorys);

        $inputPublications = $inputs["inputPublications"];
        $countPubs = count($inputPublications);

        $insert = "lnk_title,lnk_description,lnk_tags,lnk_url,lnk_target,lnk_icon,lnk_attr,lnk_cls,lnk_alt,lnk_img,lnk_json,lnk_ent_id,lnk_state";
        $values ="'" . $inputTitle . "','" . $inputDescription . "','" . $inputTags . "','" . $inputUrl . "','" . $inputTarget . "','" . $inputIcon . "','" . $inputAttr . "','" . $inputClass . "','" . $inputAlt . "','" . $inputImage . "','" . $inputJson . "','" . $entitieId . "','1'";
        $sql= "insert into  links (".$insert.") values (".$values.")";
        $this->fmt->querys->consult($sql,__METHOD__);

        $sql = "select max(lnk_id) as id from links";
        $rs = $this->fmt->querys->consult($sql, __METHOD__);
        $row = $this->fmt->querys->row($rs);
        $id = $row["id"];

        for ($i=0; $i < $count; $i++) { 
            $sql = "insert into links_categorys (lnk_cat_lnk_id,lnk_cat_cat_id, lnk_cat_order) values ('".$id."','".$inputCategorys[$i]."', '".$i."')";
            $this->fmt->querys->consult($sql,__METHOD__);
        }

        for ($i=0; $i < $countPubs; $i++) { 
            $sql = "insert into links_pubs (lnk_pub_lnk_id,lnk_pub_pub_id,lnk_pub_order) values ('".$id."','".$inputPublications[$i]."', '".$i."')";
            $this->fmt->querys->consult($sql,__METHOD__);
        }

        $rtn["Error"] = 0;
        $rtn["status"] = 'success';
        $rtn["message"] = "Save correctly";
        $rtn["id"] = $id;
        
        return $rtn;

    }

    public function update(array $var = null){
        //return $var;
        $entitieId = $var["entitieId"];
        $inputs = $var["vars"]["inputs"];
        $inputId = $inputs["inputId"];
        $inputTitle = $inputs["inputTitle"];
        $inputDescription = $inputs["inputDescription"];
        $inputTags = $inputs["inputTags"];
        $inputUrl = $inputs["inputUrl"];
        $inputTarget = $inputs["inputTarget"] ? $inputs["inputTarget"] : "_self";
        $inputIcon = $inputs["inputIcon"];
        $inputAlt = $inputs["inputAlt"];
        $inputClass = $inputs["inputClass"];
        $inputAttr = $inputs["inputAttr"];
        $inputImage = $inputs["inputImage"];
        $inputJson = $inputs["inputJson"];
        $inputState = $inputs["inputState"];

        $inputCategorys = $inputs["inputCategorys"];
        $count = count($inputCategorys);

        $inputPublications = $inputs["inputPublications"];
        $countPubs = count($inputPublications);

        $sql = "UPDATE links SET
                lnk_title = '" . $inputTitle . "',
                lnk_description = '" . $inputDescription . "',
                lnk_tags = '" . $inputTags . "',
                lnk_url = '" . $inputUrl . "',
                lnk_target = '" . $inputTarget . "',
                lnk_icon = '" . $inputIcon . "',
                lnk_attr = '" . $inputAttr . "',
                lnk_cls = '" . $inputClass . "',
                lnk_alt = '" . $inputAlt . "',
                lnk_img = '" . $inputImage . "',
                lnk_json = '" . $inputJson . "',        
                lnk_ent_id = '" . $entitieId . "',
                lnk_state = '".$inputState."'             
                WHERE lnk_id = '" .$inputId. "' ";
        $this->fmt->querys->consult($sql);

        $arrayCat = $this->fmt->categorys->relationFrom(["from" => "links_categorys",
                        "colId" => "lnk_cat_lnk_id", 
                        "colCategory" => "lnk_cat_cat_id", 
                        "id" => $inputId,
                        "orderBy" => "lnk_cat_order ASC"]);

        foreach ($arrayCat as $key => $value) {
            $arrayCatInts[$key] = $value["id"];
        }


        if ($arrayCat==0) {
            $sql = "delete from links_categorys where lnk_cat_lnk_id='".$inputId."'";
            $this->fmt->querys->consult($sql,__METHOD__);

            for ($i=0; $i < $count; $i++) { 
                $sql = "insert into links_categorys (lnk_cat_lnk_id,lnk_cat_cat_id, lnk_cat_order) values ('".$inputId."','".$inputCategorys[$i]."', '".$i."')";
                $this->fmt->querys->consult($sql,__METHOD__);
            }

        }else{
            $additional_elements = array_diff($inputCategorys,$arrayCatInts);
            $missing_elements = array_diff($arrayCatInts,$inputCategorys);

            foreach ($additional_elements as $element) {
                $sql = "insert into links_categorys (lnk_cat_lnk_id,lnk_cat_cat_id,lnk_cat_order) values ('".$inputId."','".$element."', '".$count."')";
                $this->fmt->querys->consult($sql,__METHOD__);
            }

            foreach ($missing_elements as $element) {
                $sql = "delete from links_categorys where lnk_cat_lnk_id='".$inputId."' and lnk_cat_cat_id='".$element."'";
                $this->fmt->querys->consult($sql,__METHOD__);
            }
        }

        $arrayPub = $this->fmt->pubs->relationFrom(["from" => "links_pubs",
                        "colId" => "lnk_pub_lnk_id", 
                        "colPub" => "lnk_pub_pub_id", 
                        "id" => $inputId,
                        "orderBy" => "lnk_pub_order ASC"]);

        foreach ($arrayPub as $key => $value) {
            $arrayPubInts[$key] = $value["id"];
        }

        if ($arrayPub==0) {
            $sql = "delete from links_pubs where lnk_pub_lnk_id='".$inputId."'";
            $this->fmt->querys->consult($sql,__METHOD__);

            for ($i=0; $i < $countPubs; $i++) { 
                $sql = "insert into links_pubs (lnk_pub_lnk_id,lnk_pub_pub_id,lnk_pub_order) values ('".$inputId."','".$inputPublications[$i]."', '".$i."')";
                $this->fmt->querys->consult($sql,__METHOD__);
            }

        }else{
            $additional_elements = array_diff($inputPublications,$arrayPubInts);
            $missing_elements = array_diff($arrayPubInts,$inputPublications);

            foreach ($additional_elements as $element) {
                $sql = "insert into links_pubs (lnk_pub_lnk_id,lnk_pub_pub_id,lnk_pub_order) values ('".$inputId."','".$element."', '".$count."')";
                $this->fmt->querys->consult($sql,__METHOD__);
            }

            foreach ($missing_elements as $element) {
                $sql = "delete from links_pubs where lnk_pub_lnk_id='".$inputId."' and lnk_pub_pub_id='".$element."'";
                $this->fmt->querys->consult($sql,__METHOD__);
            }
        }



        /* 

        foreach ($missing_elements as $element) {
            $sql = "delete from links_categorys where lnk_cat_lnk_id='".$inputId."' and lnk_cat_cat_id='".$element."'";
            $this->fmt->querys->consult($sql,__METHOD__);
        } */

        return 1;

    }

    public function getLinksList(array $var = null){
        //return $var;

        $entitieId = $var["entitieId"];
        $sql = "SELECT * FROM links WHERE lnk_ent_id='".$entitieId."'  ORDER BY lnk_id desc";
        $rs =$this->fmt->querys->consult($sql,__METHOD__);
        $num=$this->fmt->querys->num($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row = $this->fmt->querys->row($rs);
                $id = $row["lnk_id"];
                $return[$i]["id"] = $id;
                $return[$i]["title"] = $row["lnk_title"];
                $return[$i]["description"] = $row["lnk_description"];
                $return[$i]["tags"] = $row["lnk_tags"];
                $return[$i]["url"] = $row["lnk_url"];
                $return[$i]["target"] = $row["lnk_target"];
                $return[$i]["cls"] = $row["lnk_cls"];
                $return[$i]["attr"] = $row["lnk_attr"];
                $return[$i]["alt"] = $row["lnk_alt"];
                $return[$i]["json"] = $row["lnk_json"];
                $return[$i]["icon"] = $row["lnk_icon"];
                $imgId = $row["lnk_img"];

                $return[$i]["categorys"] = $this->fmt->categorys->relationFrom(array("from" => "links_categorys",
                    "colId" => "lnk_cat_lnk_id",
                    "colCategory" => "lnk_cat_cat_id",
                    "id" => $id,
                    "orderBy" => "lnk_cat_order ASC"
                )); 

             $img = [];
                if ($imgId != 0) {
                    $img["id"] = $imgId;
                    $img["pathurl"] = $this->fmt->files->imgId($row["lnk_img"], "");
                } else {
                    $img = 0;
                } 

                $return[$i]["img"] = $img;
                $return[$i]["state"] = $row["lnk_state"]; 
            }
            return $return; 
        } else {
            return 0;
        }
        $this->fmt->querys->leave($rs); 

        
    }
    
    public function deleteItems(array $var = null){
        //return $var;
        $ids = $var["vars"]["ids"];
        $auxIds = count($ids);
        $count = 0;

        for ($i = 0; $i < $auxIds; $i++) {
            $id  = $ids[$i];
            if ( ($id != "") && ($id!=null) && ($id!=0) ){
                $this->fmt->modules->deleteRelation(["from" => "links_categorys", "column" => "lnk_cat_lnk_id", "item" => $id]);
                //$this->fmt->modules->deleteRelation(array("from" => "links_files", "column" => "lnk_file_lnk_id", "item" => $id));
                $state = $this->fmt->modules->deleteModuleId(["id"=>$id,"from"=>"links", "prefix"=>"lnk_"]);
                if ($state==0){
                    $count++;
                }
            }
        }
        if ($count == 0){
            return 1;
        }else{
            return 0;
        } 
        
    }
   


}