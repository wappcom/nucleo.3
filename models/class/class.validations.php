<?PHP
header('Content-Type: text/html; charset=utf-8');
class VALIDATIONS
{

	var $fmt;

	function __construct($fmt)
	{
		$this->fmt = $fmt;
	}

	public function name($text)
	{
		$numCharacters = strlen($text);

		if (empty($text)) {
			return 0;
		}

		if ($numCharacters <= 2) {
			return 0;
		}

		return 1;
	}

	public function email($email, $required = 0)
	{


		if ($required == 1) {
			if (empty($email)) {
				return 1;
			}
			if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
				return 0;
			}
			return 1;
		} else {
			if (empty($email)) {
				return 0;
			} else {
				if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
					return 0;
				}
			}
			return 1;
		}
	}

	public function num($num = null)
	{
		if (is_int($num)) {
			return 1;
		} else {
			return 0;
		}
	}

	public function numLiteral(string $str = null)
	{
		if (empty($str)) {
			return 0;
		}
		//validate username
		if (preg_match('/^[0-9]+$/', $str)) {
			return 1;
		} else {
			return 0;
		}
	}

	public function username(string $str = null)
	{

		if (empty($str)) {
			return 0;
		}
		//validate username
		if (preg_match('/^[a-zA-Z0-9_]+$/', $str)) {
			return 1;
		} else {
			return 0;
		}
	}
}